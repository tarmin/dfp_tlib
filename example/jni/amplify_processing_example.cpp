#include <dfptl/core/core.hpp>
#include <dfptl/java/java.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>


using namespace dfp;
using namespace routing;
using namespace dfp::java;


auto amplify_processing_builder = [](std::size_t output_frame_length, std::vector<std::size_t> const& input_frame_lengths){
    (void) input_frame_lengths;
    return
        to_java(JINT) <<= adjust_frame_length(output_frame_length) <<= alu::multiply()  += to_single_sample() <<= from_java<0>(JINT)
                                                                                        |= 2.1
        ;
};

DFP_JAVA_ASSIGN_PROCESSING_TREE(dfp_example_Amplify, amplify_processing_builder);
