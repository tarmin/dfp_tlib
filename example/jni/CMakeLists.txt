cmake_minimum_required(VERSION 3.0.0)
project(dfp_jni_example VERSION 0.1.0)

find_package(JNI REQUIRED)
find_package(Boost REQUIRED)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


include_directories(
    ../../include
    ../../dep/span-lite/include
    ${JNI_INCLUDE_DIRS}
)

add_library(dfp_jni_example SHARED amplify_processing_example.cpp)

set(CMAKE_INSTALL_PREFIX ../../out)
install(TARGETS dfp_jni_example DESTINATION lib EXPORT dfp_jni_example)
install(EXPORT dfp_jni_example DESTINATION lib)
