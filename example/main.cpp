/**
* @file example/main.cpp
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <iostream>


extern void dfptl_overview_example();
extern void operators_overview_example();
extern void placeholder_creation_example();
extern void placeholder_preset_inputs_example();
extern void triangular_waveform_generator();
extern void const_sample_overview_example();


int main(int argc, char* argv[])
{
    std::cout << "dfptl_overview_example" << std::endl;
    std::cout << "---" << std::endl;
    dfptl_overview_example();
    std::cout  << std::endl;
    std::cout << "operators_overview_example" << std::endl;
    std::cout << "---" << std::endl;
    operators_overview_example();
    std::cout << std::endl;
    std::cout << "placeholder_creation_example" << std::endl;
    std::cout << "---" << std::endl;
    placeholder_creation_example();
    std::cout  << std::endl;
    std::cout << "placeholder_preset_inputs_example" << std::endl;
    std::cout << "---" << std::endl;
    placeholder_preset_inputs_example();
    std::cout << std::endl;
    std::cout << "triangular_waveform_generator" << std::endl;
    std::cout << "---" << std::endl;
    triangular_waveform_generator();
    std::cout << std::endl;
    std::cout << "const_sample_overview_example" << std::endl;
    std::cout << "---" << std::endl;
    const_sample_overview_example();
    std::cout << std::endl;
    return 0;
}
