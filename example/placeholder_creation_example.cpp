#include <dfptl/core/core.hpp>

#include <iostream>


using namespace dfp;


/*
 * A custom source implementation which produces value increment
 */
template<class Context>
struct my_counter_source
 :  source<Context, atomic_stream_specifier<int>, my_counter_source<Context>>
{
    my_counter_source(Context&& context)
     :  source<Context, atomic_stream_specifier<int>, my_counter_source<Context>>(std::forward<Context>(context))
    {}

    int acquire_sample() { return n++; }

private:
    int n;
};
/* end of struct my_counter_source */


/*
 * A custom node implementation which doubles input sample value
 */
template<class Input>
struct my_double_counting_node
 :  node<Input, atomic_stream_specifier<int>, my_double_counting_node<Input>>
{
    my_double_counting_node(Input&& input)
     :  node<Input, atomic_stream_specifier<int>, my_double_counting_node>(std::forward<Input>(input)) {}

    int process_sample(int const& in) const { return in * 2; }
}; /* end of struct my_double_counting_node */

/*
 * Helps to create the companion placeholder for my_double_counting_node
 */
decltype(make_placeholder<my_double_counting_node>()) my_double_counting()
{ return  make_placeholder<my_double_counting_node>(); }


decltype(make_placeholder<my_counter_source>()) my_counter()
{ return  make_placeholder<my_counter_source>(); }


void placeholder_creation_example()
{
    // 1st branch version
    // has implied usage of the make_placeholder() helper to create the placeholder of my_double_counting_node
    auto processing_branch1 = my_double_counting() <<= my_counter();

    // 2nd branch version without usage of make_placeholder().
    // As processing of my_double_counting_node is quite trivial, implicit conversion from function object to node or source can help here
    // and greatly reduces boilerplate - defining the struct my_double_counting_node or my_counter_source is actually not required -.
    // In such a case, no need for make_placeholder() as implicit conversion can apply to create the placeholder.
    int n;
    auto processing_branch2 = [](int const& in) { return in * 2; } <<= [&n](){ return n++; };

    // a custom node to display sample stream value
    auto print = [](int const& in) { std::cout << in << " "; return nullptr; };

    auto processing_tree1 = make_runnable(
        sample_releaser() <<= print <<= processing_branch1
    );
    auto processing_tree2 = make_runnable(
        sample_releaser() <<= print <<= processing_branch2
    );

    std::cout << "4 first samples from branch1 processing" << std::endl;
    processing_tree1.run();
    processing_tree1.run();
    processing_tree1.run();
    processing_tree1.run();
    std::cout << std::endl;

    std::cout << "4 first samples from branch2 processing"  << std::endl;
    processing_tree2.run();
    processing_tree2.run();
    processing_tree2.run();
    processing_tree2.run();
    std::cout << std::endl;
}

// expected output on placeholder_creation_example() call:
//
//4 first samples from branch1 processing
//0 2 4 6
//4 first samples from branch2 processing
//0 2 4 6
