/**
* @file example/triangular_waveform_generator.cpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/alu/alu.hpp>
#include <dfptl/core/core.hpp>
#include <dfptl/routing/looper.hpp>

#include <iostream>


void triangular_waveform_generator()
{
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::routing;

    const int PERIOD_LENGTH = 10; // period of the triangular function (shall be even)

    /* custom node printing values in CSV format */
    auto print = [](int const& in){ std::cout << in << ", "; return in; };

    looping(PERIOD_LENGTH*2) <<= print <<= accumulate() <<= subtract(1) <<= multiply(2) <<= less_than(PERIOD_LENGTH/2) <<= modulo(PERIOD_LENGTH) <<= counter(INT32_T)
    ;
}
// expected output on triangular_waveform_generator() call:
//
//1, 2, 3, 4, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0,
