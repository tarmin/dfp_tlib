/**
* @file example/dfptl_overview_example.cpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/routing/looper.hpp> // for routing::looping sink
#include <dfptl/io/write_csv.hpp> // for io::cout_csv node
#include <dfptl/math/sin.hpp> // for math::sin node
#include <dfptl/alu/multiply.hpp> // for alu::multiply node
#include <dfptl/alu/counter.hpp> // for alu::counter source

#include <iostream>
#include <cmath>


void dfptl_overview_example()
{
    { // Dataflow implementation with predefined processing cells
        using namespace dfp;

        // create a processing tree printing values of 1 sinewave period
        double frequency = 440;
        double time_step = 10;

        routing::looping(frequency/time_step) <<= io::cout_csv() += math::sin() <<= alu::multiply(2*M_PI/frequency) <<= alu::counter(time_step)
                                                                 |= false;
    }
    std::cout << std::endl;

    { // plain C++ implementation
        auto csv_print = [](double const& in){ std::cout << in << ","; return in; };
        struct counter_function{
            counter_function(double step) : cnt_(-step), step_(step) {}
            double operator()() { return cnt_ += step_; }
            double cnt_, step_;
        };

        // create a functions waterfall printing values of 1 sinewave period
        double frequency = 440;
        double time_step = 10;
        counter_function clock(time_step);

        for( int i = 0 ; i < frequency/time_step ; i++)
        {
            csv_print( std::sin( (2*M_PI/frequency)*clock() ) );
        }
    }
    std::cout << std::endl;

    { // Dataflow implementation with custom processing cells
        using namespace dfp;

        // create configurable single-input sink from lambda and with help of call forwarding
        auto looper = std::bind([](int iteration_count){
            return apply<run_for>([](int const&){ /*nothing to do*/ }, iteration_count);
        }, std::placeholders::_1);
        // create single-input node implicitly deduced from lambda
        auto csv_print = [](double const& in){ std::cout << in << ","; return in; };
        auto sin = [](double const& in){ return std::sin(in); };
        // create double-input node from lambda
        auto multiply2 = apply([](double const& in1, double const& in2){ return in1 * in2; });
        // create single-input node from double-input node with 1 preset input
        auto multiply = [multiply2](double in2){ return multiply2.preset_inputs(in2); };
        //create source from function-object
        auto clock = [](double step)
        { 
            struct counter_function
            {
                counter_function(double step) : cnt_(-step), step_(step) {}
                double operator()() { return cnt_ += step_; }
                double cnt_, step_;
            };

            return apply<counter_function>(step);
        };

        // create a processing tree printing values of 1 sinewave period
        double frequency = 440;
        double time_step = 10;

        auto processing_tree = make_runnable(
            looper(frequency/time_step) <<= csv_print <<= sin <<= multiply(2*M_PI/frequency) <<= clock(time_step)
        );
        processing_tree.run();
    }
    std::cout << std::endl;
}
