#include <dfptl/core/core.hpp>

#include <iostream>


using namespace dfp;


/*
 * A custom source implementation which produces value increment
 */
struct my_counter_function
{
    int operator()() { return n++; }

private:
    int n;
};
auto my_counter = apply<my_counter_function>();

/*
 * A custom node implementation which sums value on its both inputs
 */
auto my_add = apply([](int const& in0, int const& in1){ return in0 + in1; });


void placeholder_preset_inputs_example()
{
    int offset = 5;

    // Defines a counting processing starting from 5
    auto processing_branch1 = my_add    += offset
                                        |= my_counter
    ;

    // Same here but using preset_inputs()
    auto processing_branch2 = my_add.preset_inputs(offset) <<= my_counter;

    // a custom node to display stream sample value
    auto print = [] (int const& in) { std::cout << in << " "; };

    auto processing_tree1 = make_runnable(
        sample_releaser() <<= print <<= processing_branch1
    );
    auto processing_tree2 = make_runnable(
        sample_releaser() <<= print <<= processing_branch2
    );

    std::cout << "4 first samples from branch1 processing" << std::endl;
    processing_tree1.run();
    processing_tree1.run();
    processing_tree1.run();
    processing_tree1.run();
    std::cout << std::endl;

    std::cout << "4 first samples from branch2 processing"  << std::endl;
    processing_tree2.run();
    processing_tree2.run();
    processing_tree2.run();
    processing_tree2.run();
    std::cout << std::endl;
}

// expected output on placeholder_preset_inputs_example() call:
//
//4 first samples from branch1 processing
//5 6 7 8
//4 first samples from branch2 processing
//5 6 7 8
