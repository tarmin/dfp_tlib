#include <iostream>
#include <dfptl/core/core.hpp>
#include <dfptl/alu/alu.hpp>


using namespace dfp;


void const_sample_overview_example()
{
    int a = 2;
    auto processing_tree = make_runnable(
        apply<run_for>([&a](int const& in) { std::cout << in << ", "; a = 3; }, 2) <<= alu::negate() <<= const_sample(a)
    );
    processing_tree.run();

    // other example involving c-string processing
    std::cout << std::endl;
    char out[32];

    auto processing_tree2 = make_runnable(
        apply<run_once>([](const char* in) { std::cout << "Hello " << in << "!"; })
        <<= apply([&out](char const* in){ char* o = out; while(*in && (o-out)<(int) sizeof(out)) *o++ = toupper(*in++); return out; })
        <<= const_sample("world")
    );

    processing_tree2.run();
}

// expected output on const_sample_overview_example() call:
//
//-2, -2, 
//Hello WORLD! 
