#include <dfptl/core/core.hpp>
#include <dfptl/io/io.hpp>
#include <dfptl/routing/routing.hpp>
#include <dfptl/alu/alu.hpp>

#include <iostream>


using namespace dfp;


void operators_overview_example()
{
    /// @[Extend operator]
    // creating a processing tree that is ready for processing, i.e. all its sink, nodes and sources
    // have been built and linked together
                                    /// @[Implicitly converting lambda into apply node]
                                    // lambda is implicity converted into apply([](int const& in){ return in*2; }) node
    auto processing_tree =
        sample_releasing() <<= [](int const& in){ return in*2; } <<= alu::counter()
    ;
                                    /// @[Implicitly converting lambda into apply node]
    /// @[Extend operator]
    std::cout << *processing_tree << ","; // fires once the processing tree and returns a processed sample.
    std::cout << *++processing_tree << ",";
    std::cout << *++processing_tree << ",";
    std::cout << *++processing_tree;
    std::cout << std::endl << std::endl;

    const auto container = std::experimental::make_array(0, 1, 2, 3);
    const std::size_t container_size =sizeof(container) / sizeof(container[0]);

    /// @[Linking to multiple input cells]
    using namespace dfp::routing;
    using namespace dfp::alu;

    // creating a processing tree that is automatically run, i.e. all its sink, nodes and sources have been built, linked together
    // and processing has been launched.
                                    /// @[Implicitly converting input data into const_sample]
    looping(container_size*2) <<= io::cout_csv()    += equal_to(3) <<= modulo(4) <<= counter(1)
                                                    |= multiply(2) <<= from_iterator(container.cbegin());
                                    /// @[Implicitly converting input data into const_sample]
    /// @[Linking to multiple input cells]
    std::cout << std::endl;
}

// expected output on operators_overview_example() call:
//
//0,2,4,6
//
//0,2,4,6
//0,2,4,6
