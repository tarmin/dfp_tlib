/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/windowing/windowing.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/switch_if.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/greater_equal.hpp>
#include <dfptl/io/write_text_chart.hpp>

#include <iostream>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert(x, "")


BOOST_AUTO_TEST_CASE(hann_test)
{
    using namespace dfp;
    using namespace dfp::windowing;

    #if 0
    { // concept check
        // line below won't compile because taps_count argument has wrong type
        hann_frame(0.0);
        // line below won't compile because window_length argument has wrong type
        hann(0.0) <<= 1.0;
    }
    #endif
    { // type traits check
        size_t_const<20> WINDOW_LENGTH;

        STATIC_CHECK((dfp::detail::is_source<decltype(hann_frame(WINDOW_LENGTH).build())>{}));
        STATIC_CHECK((dfp::detail::source_produces_frame<decltype(hann_frame(WINDOW_LENGTH).build())>{}));
    }
    {
        ///@[hann_frame usage example with integral constant]
        size_t_const<20> WINDOW_LENGTH;

        std::cout << "Hann Frame - " << WINDOW_LENGTH << "-point length defined at compile-time" << std::endl;
        std::cout << "-------------------------------------------------------" << std::endl;

        routing::looping(WINDOW_LENGTH*1.5) <<= io::cout_text_chart() <<= routing::to_single_sample() <<= hann_frame(WINDOW_LENGTH);
        ///@[hann_frame usage example with integral constant]
    }

    {
        ///@[hann_frame usage example with std::size_t]
        std::size_t window_length = std::size_t(21 + std::rand()/(double)RAND_MAX);

        std::cout << "Hann Frame - " << window_length << "-point length defined at runtime" << std::endl;
        std::cout << "--------------------------------------------------" << std::endl;

        routing::looping(window_length) <<= io::cout_text_chart() <<= routing::to_single_sample() <<= hann_frame(window_length);
        ///@[hann_frame usage example with std::size_t]
    }
    {
        ///@[hann usage example with integral constant]
        size_t_const<20> WINDOW_LENGTH;

        std::cout << "Hann Windowing" << std::endl;
        std::cout << "--------------------------" << std::endl;

        routing::looping(WINDOW_LENGTH*2) <<= io::cout_text_chart({-1, 1, 80}) <<= hann(WINDOW_LENGTH)
            <<= routing::switch_if()    += -0.5 /*"else"-branch*/
                                        |= 1.0  /*"then"-branch*/
                                        |= alu::greater_equal(WINDOW_LENGTH) <<= alu::counter(1U);
        ///@[hann usage example with integral constant]
    }
}


BOOST_AUTO_TEST_CASE(blackman_nuttall_test)
{
    using namespace dfp;
    using namespace dfp::windowing;

    #if 0
    { // concept check
        // line below won't compile because taps_count argument has wrong type
        blackman_nuttall_frame(0.0);
        // line below won't compile because window_length argument has wrong type
        blackman_nuttall(0.0) <<= 1.0;
    }
    #endif
    { // type traits check
        size_t_const<20> WINDOW_LENGTH;

        STATIC_CHECK((dfp::detail::is_source<decltype(blackman_nuttall_frame(WINDOW_LENGTH).build())>{}));
        STATIC_CHECK((dfp::detail::source_produces_frame<decltype(blackman_nuttall_frame(WINDOW_LENGTH).build())>{}));
    }
    {
        ///@[blackman_nuttall_frame usage example with integral constant]
        size_t_const<20> WINDOW_LENGTH;

        std::cout << "Blackman-Nuttall Frame - " << WINDOW_LENGTH << "-point length defined at compile-time" << std::endl;
        std::cout << "-------------------------------------------------------" << std::endl;

        routing::looping(WINDOW_LENGTH*1.5) <<= io::cout_text_chart() <<= routing::to_single_sample() <<= blackman_nuttall_frame(WINDOW_LENGTH);
        ///@[blackman_nuttall_frame usage example with integral constant]
    }
    {
        ///@[blackman_nuttall_frame usage example with std::size_t]
        std::size_t window_length = std::size_t(21 + std::rand()/(double)RAND_MAX);

        std::cout << "Blackman-Nuttall Frame - " << window_length << "-point length defined at runtime" << std::endl;
        std::cout << "--------------------------------------------------" << std::endl;

        routing::looping(window_length) <<= io::cout_text_chart() <<= routing::to_single_sample() <<= blackman_nuttall_frame(window_length);
        ///@[blackman_nuttall_frame usage example with std::size_t]
    }
    {
        ///@[blackman_nuttall usage example with integral constant]
        size_t_const<20> WINDOW_LENGTH;

        std::cout << "Blackman-Nuttall Windowing" << std::endl;
        std::cout << "--------------------------" << std::endl;

        routing::looping(WINDOW_LENGTH*2) <<= io::cout_text_chart({-1, 1, 80}) <<= blackman_nuttall(WINDOW_LENGTH)
            <<= routing::switch_if()    += -0.5 /*"else"-branch*/
                                        |= 1.0  /*"then"-branch*/
                                        |= alu::greater_equal(WINDOW_LENGTH) <<= alu::counter(1U);
        ///@[blackman_nuttall usage example with integral constant]
    }
}

//todo: add triangular window
//---
//todo: add const test-cases
//todo: add ImplementationPolicy template with possible choices : SPEED_OPTIMIZED, MEMORY_OPTIMIZED, BALANCED_TRADEOFF
//todo: add test-cases for the 3 ImplementationPolicy variant (equality, mem and cpu consomption) - might need to dev a median node
//todo: implement new petrify node and make window sources with following pattern : petrify() <<= apply_f()
