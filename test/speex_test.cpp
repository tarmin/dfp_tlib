/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/speex/speex.hpp>
#include <dfptl/sndfile/sndfile.hpp>
#include <dfptl/math/sin.hpp>
#include <dfptl/math/cos.hpp>
#include <dfptl/math/random.hpp>
#include <dfptl/windowing/hann.hpp>
#include <dfptl/routing/delay.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/channelize.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/sample_static_cast.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>
#include <dfptl/alu/add.hpp>
#include <dfptl/alu/modulo.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/alu/equal_to.hpp>

#include <boost/test/unit_test.hpp>

#include <cmath>


BOOST_AUTO_TEST_CASE(speex_vad_test)
{
    ///@[speex::vad usage example]
    using namespace dfp;
    using namespace dfp::routing;

    SF_INFO infile_info;
    auto FRAME_LENGTH = size_t_const<512>{};

    SNDFILE* file_handle = sf_open("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", SFM_READ, &infile_info);

    // store the stream of voice detection status in .wav file
    looping(infile_info.frames / FRAME_LENGTH) <<= sndfile::write("output/speex_vad_out.wav", infile_info.samplerate / FRAME_LENGTH)
        <<= sample_static_cast<short>() <<= speex::vad(infile_info.samplerate) <<= sndfile::read(file_handle, INT16_T, FRAME_LENGTH)
    ;
    ///@[speex::vad usage example]
}


BOOST_AUTO_TEST_CASE(speex_aec_test)
{
    { ///@[speex::aec usage example]
        using namespace dfp;
        using namespace dfp::routing;
        using namespace dfp::shortname;

        size_t_const<16000>                     SAMPLING_RATE;      // sampling rate is 16kHz
        size_t_const<56*SAMPLING_RATE/1000>     LENGTH_OF_DELAY;    // delay introduced on audio composite record is 56ms
        size_t_const<20*SAMPLING_RATE/1000>     LENGTH_OF_20MS;     // sample count corresponding to 20ms
        size_t_const<10*SAMPLING_RATE-80>       LENGTH_OF_TEST;     // test length in sample count (10s - 80 samples)
        size_t_const<SAMPLING_RATE/440>         LENGTH_OF_440HZ;    // sample count corresponding to one period of signal at 440Hz
        size_t_const<SAMPLING_RATE/600>         LENGTH_OF_600HZ;    // sample count corresponding to one period of signal at 600Hz
        size_t_const<256>                       FRAME_LENGTH;       // processing frame length in sample count
        size_t_const<200*SAMPLING_RATE/1000>    LENGTH_OF_AEC_FILT; // sample count corresponding to the aec filter length (200ms)

        // branch to produce the reference audio signal
        auto const echo_ref  = windowing::hann(LENGTH_OF_20MS) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_440HZ) <<= alu::counter();

        // tree producing some reference audio file
       looping(LENGTH_OF_TEST) <<= sndfile::write("output/echo_ref.wav", SAMPLING_RATE) <<= echo_ref;

        // tree producing audio composite file in which is stored the signal of interest polluted with echo and noise
        looping(LENGTH_OF_TEST) <<= sndfile::write("output/audio_composite.wav", SAMPLING_RATE)
            // branch generating the audio composite
                           // branch generating the echo signal
        <<= alu::add()  += delay(LENGTH_OF_DELAY) <<= alu::multiply(0.3) <<= echo_ref
                                           // branch generating white noise
                        |= alu::add()   += alu::multiply(0.1) <<= math::random()
                                           // branch generating the audio of interest
                                        |= alu::multiply(0.6) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_600HZ) <<= alu::counter()
        ;

        // tree removing echo from the audio composite file
        SF_INFO reffile_info;
        SNDFILE* echoref_handle = sf_open("output/echo_ref.wav", SFM_READ, &reffile_info);

        looping(reffile_info.frames) <<= sndfile::write("output/cleaned_from_echo.wav", reffile_info.samplerate)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  += sndfile::read(echoref_handle, INT16_T, FRAME_LENGTH)
                                            |= sndfile::read("output/audio_composite.wav", INT16_T, FRAME_LENGTH)
        ;
        ///@[speex::aec usage example]
        looping(1024)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  +=  routing::const_frame(std::array<int16_t, 256>{})
                                            |=  speex::aec(LENGTH_OF_AEC_FILT)  += routing::const_frame(std::vector<int16_t>(256))
                                                                                |= routing::const_frame(std::vector<int16_t>(256))
        ;

        looping(1024)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  +=  routing::const_frame(std::array<int16_t, 256>{})
                                            |=  speex::aec(LENGTH_OF_AEC_FILT)  += routing::const_frame(std::array<int16_t const, 256>{})
                                                                                |= routing::const_frame(std::array<int16_t const, 256>{})
        ;
    }
    { ///@[speex::aec multi-channels usage example]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;
        using namespace dfp::shortname;

        size_t_const<16000>                     SAMPLING_RATE;      // sampling rate is 16kHz
//        size_t_const<56*SAMPLING_RATE/1000>     LENGTH_OF_DELAY;    // delay introduced on audio composite record is 56ms
        size_t_const<20*SAMPLING_RATE/1000>     LENGTH_OF_20MS;     // sample count corresponding to 20ms
        size_t_const<10*SAMPLING_RATE-80>       LENGTH_OF_TEST;     // test length in sample count (10s - 80 samples)
        size_t_const<SAMPLING_RATE/440>         LENGTH_OF_440HZ;    // sample count corresponding to one period of signal at 440Hz
        size_t_const<SAMPLING_RATE/550>         LENGTH_OF_550HZ;    // sample count corresponding to one period of signal at 550Hz
        size_t_const<SAMPLING_RATE/600>         LENGTH_OF_600HZ;    // sample count corresponding to one period of signal at 600Hz
//        size_t_const<SAMPLING_RATE/600>         LENGTH_OF_700HZ;    // sample count corresponding to one period of signal at 700Hz
        size_t_const<256>                       FRAME_LENGTH;       // processing frame length in sample count
        size_t_const<200*SAMPLING_RATE/1000>    LENGTH_OF_AEC_FILT; // sample count corresponding to the aec filter length (200ms)

        // branch to produce the reference audio signal
        auto const echo_ref =
            routing::channelize()
                += windowing::hann(LENGTH_OF_20MS) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_440HZ) <<= alu::counter()
                |= windowing::hann(LENGTH_OF_20MS) <<= math::cos() <<= alu::multiply(2*M_PI/LENGTH_OF_550HZ) <<= alu::counter()
        ;

        const auto sample_count = LENGTH_OF_TEST*echo_ref.output_stream_specifier().get_channel_count();

        // tree producing some reference audio file
        looping(sample_count)
            <<= sndfile::write("output/echo_ref_mc.wav", SAMPLING_RATE) <<= echo_ref;

        // tree producing audio composite file in which is stored the signal of interest polluted with echo and noise
        looping(sample_count) <<= sndfile::write("output/audio_composite.wav", SAMPLING_RATE)
            // branch generating the audio composite
                        // branch generating the echo signal
            <<= /*add()   += delay(LENGTH_OF_DELAY) <<= multiply(0.3) <<= echo_ref
                                    // branch generating white noise
                        |=*/ add()    += multiply(0.1) <<= math::random()
                                    // branch generating the audio of interest
//                                    |= channelize()
//                                                    += multiply(0.7) <<= math::sin() <<= multiply(2*M_PI/LENGTH_OF_700HZ) <<= counter()
                                                    |= multiply(0.6) <<= math::sin() <<= multiply(2*M_PI/LENGTH_OF_600HZ) <<= counter()
        ;

        // tree removing echo from the audio composite file
        SF_INFO reffile_info;
        SNDFILE* echoref_handle = sf_open("output/echo_ref.wav", SFM_READ, &reffile_info);

        looping(reffile_info.frames) <<= sndfile::write("output/cleaned_from_echo.wav", reffile_info.samplerate)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  += sndfile::read(echoref_handle, INT16_T, FRAME_LENGTH)
                                            |= sndfile::read("output/audio_composite.wav", INT16_T, FRAME_LENGTH)
        ;
        ///@[speex::aec multi-channels usage example]
        looping(1024)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  +=  routing::const_frame(std::array<int16_t, 256>{})
                                            |=  speex::aec(LENGTH_OF_AEC_FILT)  += routing::const_frame(std::vector<int16_t>(256))
                                                                                |= routing::const_frame(std::vector<int16_t>(256))
        ;

        looping(1024)
        <<= speex::aec(LENGTH_OF_AEC_FILT)  +=  routing::const_frame(std::array<int16_t, 256>{})
                                            |=  speex::aec(LENGTH_OF_AEC_FILT)  += routing::const_frame(std::array<int16_t const, 256>{})
                                                                                |= routing::const_frame(std::array<int16_t const, 256>{})
        ;
    }
}


BOOST_AUTO_TEST_CASE(speex_residual_echo_test)
{
    ///@[speex::reduce_residual_echo usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::routing;
    using namespace dfp::shortname;

    size_t_const<16000>                     SAMPLING_RATE;      // sampling rate is 16kHz
    size_t_const<56*SAMPLING_RATE/1000>     LENGTH_OF_DELAY;    // delay introduced on audio composite record is 56ms
    size_t_const<20*SAMPLING_RATE/1000>     LENGTH_OF_20MS;     // sample count corresponding to 20ms
    size_t_const<10*SAMPLING_RATE-80>       LENGTH_OF_TEST;     // test length in sample count (10s - 80 samples)
    size_t_const<SAMPLING_RATE/440>         LENGTH_OF_440HZ;    // sample count corresponding to one period of signal at 440Hz
    size_t_const<SAMPLING_RATE/1600>         LENGTH_OF_1600HZ;    // sample count corresponding to one period of signal at 1600Hz
    size_t_const<256>                       FRAME_LENGTH;       // processing frame length in sample count
    size_t_const<200*SAMPLING_RATE/1000>    LENGTH_OF_AEC_FILT; // sample count corresponding to the aec filter length (200ms)

    // branch to produce the reference audio signal
    auto const echo_ref  = windowing::hann(LENGTH_OF_20MS) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_440HZ) <<= alu::counter();

    // tree producing some reference audio file
   looping(LENGTH_OF_TEST) <<= sndfile::write("output/echo_ref.wav", SAMPLING_RATE) <<= echo_ref;

    // tree producing audio composite file in which is stored the signal of interest polluted with echo and noise
    looping(LENGTH_OF_TEST) <<= sndfile::write("output/audio_composite.wav", SAMPLING_RATE)
        // branch generating the audio composite
                       // branch generating the echo signal
    <<= alu::add()  += delay(LENGTH_OF_DELAY) <<= alu::multiply(0.3) <<= echo_ref
                                       // branch generating white noise
                    |= alu::add()   += alu::multiply(0.1) <<= math::random()
                                       // branch generating the audio of interest
                                    |= alu::multiply(0.6) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_1600HZ) <<= alu::counter()
    ;

    // tree removing echo from the audio composite file
    SF_INFO reffile_info;
    SNDFILE* echoref_handle = sf_open("output/echo_ref.wav", SFM_READ, &reffile_info);
    SpeexEchoState* echo_state = speex_echo_state_init(FRAME_LENGTH, LENGTH_OF_AEC_FILT);
    auto aec = speex::aec(echo_state);
    auto output_write = sndfile::write("output/cleaned_from_echo2.wav", reffile_info.samplerate);

    looping(reffile_info.frames) <<= output_write <<= speex::reduce_residual_echo(SAMPLING_RATE, echo_state)
        <<= aec += sndfile::read(echoref_handle, INT16_T, FRAME_LENGTH)
                |= sndfile::read("output/audio_composite.wav", INT16_T, FRAME_LENGTH)
    ;

    speex_echo_state_destroy(echo_state);
    ///@[speex::reduce_residual_echo usage example]
}


BOOST_AUTO_TEST_CASE(speex_audio_process_test)
{
    ///@[speex::audio_process usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::routing;
    using namespace dfp::shortname;

    size_t_const<16000>                     SAMPLING_RATE;      // sampling rate is 16kHz
    size_t_const<56*SAMPLING_RATE/1000>     LENGTH_OF_DELAY;    // delay introduced on audio composite record is 56ms
    size_t_const<20*SAMPLING_RATE/1000>     LENGTH_OF_20MS;     // sample count corresponding to 20ms
    size_t_const<10*SAMPLING_RATE-80>       LENGTH_OF_TEST;     // test length in sample count (10s - 80 samples)
    size_t_const<SAMPLING_RATE/440>         LENGTH_OF_440HZ;    // sample count corresponding to one period of signal at 440Hz
    size_t_const<SAMPLING_RATE/1600>         LENGTH_OF_1600HZ;    // sample count corresponding to one period of signal at 1600Hz
    size_t_const<256>                       FRAME_LENGTH;       // processing frame length in sample count
    size_t_const<200*SAMPLING_RATE/1000>    LENGTH_OF_AEC_FILT; // sample count corresponding to the aec filter length (200ms)

    // branch to produce the reference audio signal
    auto const echo_ref  = windowing::hann(LENGTH_OF_20MS) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_440HZ) <<= alu::counter();

    // tree producing some reference audio file
   looping(LENGTH_OF_TEST) <<= sndfile::write("output/echo_ref.wav", SAMPLING_RATE) <<= echo_ref;

    // tree producing audio composite file in which is stored the signal of interest polluted with echo and noise
    looping(LENGTH_OF_TEST) <<= sndfile::write("output/audio_composite.wav", SAMPLING_RATE)
        // branch generating the audio composite
                       // branch generating the echo signal
    <<= alu::add()  += delay(LENGTH_OF_DELAY) <<= alu::multiply(0.3) <<= echo_ref
                                       // branch generating white noise
                    |= alu::add()   += alu::multiply(0.1) <<= math::random()
                                       // branch generating the audio of interest
                                    |= alu::multiply(0.6) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_1600HZ) <<= alu::counter()
    ;

    // tree removing echo from the audio composite file
    SF_INFO reffile_info;
    SNDFILE* echoref_handle = sf_open("output/echo_ref.wav", SFM_READ, &reffile_info);
    SpeexEchoState* echo_state = speex_echo_state_init(FRAME_LENGTH, LENGTH_OF_AEC_FILT);
    auto aec = speex::aec(echo_state);
    auto output_write = sndfile::write("output/cleaned_from_echo3.wav", reffile_info.samplerate);

    looping(reffile_info.frames) <<= output_write
        <<= speex::make_audio_process_builder(SAMPLING_RATE)
            .enable_residual_echo_reduction(echo_state)
            .enable_denoise(-40)
            .build() <<= aec    += sndfile::read(echoref_handle, INT16_T, FRAME_LENGTH)
                                |= sndfile::read("output/audio_composite.wav", INT16_T, FRAME_LENGTH)
    ;

    speex_echo_state_destroy(echo_state);
    ///@[speex::audio_process usage example]

    {
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;
        using namespace dfp::shortname;

        size_t_const<16000>                     SAMPLING_RATE;      // sampling rate is 16kHz
        size_t_const<56*SAMPLING_RATE/1000>     LENGTH_OF_DELAY;    // delay introduced on audio composite record is 56ms
        size_t_const<20*SAMPLING_RATE/1000>     LENGTH_OF_20MS;     // sample count corresponding to 20ms
        size_t_const<10*SAMPLING_RATE-80>       LENGTH_OF_TEST;     // test length in sample count (10s - 80 samples)
        size_t_const<SAMPLING_RATE/440>         LENGTH_OF_440HZ;    // sample count corresponding to one period of signal at 440Hz
        size_t_const<SAMPLING_RATE/1600>         LENGTH_OF_1600HZ;    // sample count corresponding to one period of signal at 1600Hz
        size_t_const<256>                       FRAME_LENGTH;       // processing frame length in sample count
        size_t_const<200*SAMPLING_RATE/1000>    LENGTH_OF_AEC_FILT; // sample count corresponding to the aec filter length (200ms)

        // branch to produce the reference audio signal
        auto const echo_ref  = windowing::hann(LENGTH_OF_20MS) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_440HZ) <<= alu::counter();

        // tree producing some reference audio file
       looping(LENGTH_OF_TEST) <<= sndfile::write("output/echo_ref.wav", SAMPLING_RATE) <<= echo_ref;

        // tree producing audio composite file in which is stored the signal of interest polluted with echo and noise
        looping(LENGTH_OF_TEST) <<= sndfile::write("output/audio_composite.wav", SAMPLING_RATE)
            // branch generating the audio composite
                           // branch generating the echo signal
        <<= alu::add()  += delay(LENGTH_OF_DELAY) <<= alu::multiply(0.3) <<= echo_ref
                                           // branch generating white noise
                        |= alu::add()   += alu::multiply(0.1) <<= math::random()
                                           // branch generating the audio of interest
                                        |= alu::multiply(0.6) <<= math::sin() <<= alu::multiply(2*M_PI/LENGTH_OF_1600HZ) <<= alu::counter()
        ;

        // tree removing echo from the audio composite file
        SF_INFO reffile_info;
        SNDFILE* echoref_handle = sf_open("output/echo_ref.wav", SFM_READ, &reffile_info);
        SpeexEchoState* echo_state = speex_echo_state_init(FRAME_LENGTH, LENGTH_OF_AEC_FILT);
        auto aec = speex::aec(echo_state);
        auto output_write = sndfile::write("output/cleaned_from_echo4.wav", reffile_info.samplerate);
        SpeexPreprocessState* process_state = speex_preprocess_state_init(FRAME_LENGTH, SAMPLING_RATE);

        looping(reffile_info.frames) <<= output_write
            <<= speex::make_audio_process_builder(process_state)
                .enable_residual_echo_reduction(echo_state)
                .enable_denoise(-40)
                .build() <<= aec    += sndfile::read(echoref_handle, INT16_T, FRAME_LENGTH)
                                    |= sndfile::read("output/audio_composite.wav", INT16_T, FRAME_LENGTH)
        ;

        speex_echo_state_destroy(echo_state);
        speex_preprocess_state_destroy(process_state);
    }
}


BOOST_AUTO_TEST_CASE(speex_denoise_test)
{
    ///@[speex::denoise usage example]
    using namespace dfp;
    using namespace dfp::routing;

    SF_INFO infile_info;
    auto FRAME_LENGTH = size_t_const<512>{};

    SNDFILE* reading_handle = sf_open("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", SFM_READ, &infile_info);
    auto file_read = sndfile::read(reading_handle, INT16_T, FRAME_LENGTH);
    auto file_write = sndfile::write("output/speex_audio_denoise_out.wav", infile_info.samplerate);
    auto loop_over_read_file = looping(infile_info.frames);

    loop_over_read_file <<= file_write <<= speex::denoise(infile_info.samplerate, -30) <<= file_read;
    ///@[speex::denoise usage example]

    { // denoise with DYNAMIC_EXTENT frame in input
        looping(512) <<= speex::denoise(16000) <<= adjust_frame_length(512) <<= (int16_t) 0;
    }
}
