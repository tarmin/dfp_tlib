/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <iostream>

#include <dfptl/routing/routing.hpp>
#include <dfptl/core/core.hpp>
#include <dfptl/alu/add.hpp>
#include <dfptl/alu/divide.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/subtract.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/alu/less_than.hpp>
#include <dfptl/alu/greater_than.hpp>
#include <dfptl/math/cos.hpp>
#include <dfptl/math/sin.hpp>


#include <boost/test/unit_test.hpp>

#include <future>
#include <thread>
#include <numeric>


#define STATIC_CHECK(x) static_assert(x, "")
#define ENABLE_FAILURE_TEST 0


BOOST_AUTO_TEST_CASE(looper_test)
{
    using namespace dfp;

    std::cout << std::endl;
    {
        /// @[Draining in loop samples from processing branch]
        std::cout << "'looping sink' usage example draining sample-based processing branch" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::routing;

        int accumulator = 0;
        looping(5) <<= [&](int in){ accumulator += in; std::cout << accumulator << ","; } <<= alu::counter();

        // expected output:
        // 0, 1, 3, 6, 10,
        /// @[Draining in loop samples from processing branch]

        BOOST_CHECK_EQUAL(10, accumulator);
        std::cout << std::endl;
        std::cout << std::endl;
    }
    std::cout << std::endl;
    {
        /// @[Drain in loop samples from processing branch]
        std::cout << "'looper sink' usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::routing;

        auto processing_tree = make_runnable(
            routing::looper(5) <<= [](int in){ std::cout << in << ", "; } <<= alu::counter()
        );
        processing_tree.run();
        // expected output
        // 0, 1, 2, 3, 4,
        /// @[Drain in loop samples from processing branch]
        std::cout << std::endl;
    }
    {
        /// @[Draining in loop samples from frame-based processing branch]
        std::cout << "'looping sink' usage example draining frame-based processing branch" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::routing;

        int accumulator;
        looping(5) <<= execute_f([&](upg::span<const int> in){ accumulator = std::accumulate(in.cbegin(), in.cend(), 0); }) <<= const_frame({1, 3, 2, 4, 5});

        std::cout << accumulator << std::endl;
        //expected result: 15
        /// @[Draining in loop samples from frame-based processing branch]

        BOOST_CHECK_EQUAL(15, accumulator);
        std::cout << std::endl;
    }
    {
        /// @[Drain in loop with iteration count as result of the given function]
        std::cout << "'looping sink' usage example, samples count given per function" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::routing;

        int accumulator = 0;
        auto loop_count = [](int seed){ return seed * 2; };

        auto processing_tree = make_runnable(
            looper(loop_count(3)) <<= [&](int in){ accumulator += in; std::cout << accumulator << ","; } <<= alu::counter()
        );

        processing_tree.run();
        // expected output:
        // 0, 1, 3, 6, 10, 15
        /// @[Drain in loop with iteration count as result of the given function]

        BOOST_CHECK_EQUAL(15, accumulator);
        std::cout << std::endl;
        std::cout << std::endl;
    }
}


BOOST_AUTO_TEST_CASE(drainer_test)
{
    using namespace dfp;

    {
        /// @[Draining while given function is true]
        routing::draining([]{ static int count = 5; return count--; })
        <<= apply([](double const& in){ std::cout << in << ", "; return in; }) <<= alu::counter(10);
        // expected output
        // 0, 10, 20, 30, 40,
        /// @[Draining while given function is true]
        std::cout << std::endl;
        /// @[Drain while given function is true]
        auto processing_tree =
            routing::drainer([]{ static int count = 5; return count--; })
            <<= apply([](double const& in){ std::cout << in << ", "; return in; }) <<= alu::counter(10);
        processing_tree.run();
        // expected output
        // 0, 10, 20, 30, 40,
        /// @[Drain while given function is true]
        std::cout << std::endl;
    }
    {
        /// @[Draining while given variable is true]
        bool continue_condition = true;

        auto continue_task = std::async(std::launch::async, [&continue_condition]{ for(int i = 0 ; i < 5 ; i++) continue_condition = true; continue_condition = false; });

        routing::draining(continue_condition)
        <<= apply([](double const& in){ std::cout << in << ", "; return in; }) <<= alu::counter(-10);

        continue_task.wait();
        // expected output may print for some suite of integers with step of -10
        /// @[Draining while given variable is true]
        std::cout << std::endl;
    }
    {
        /// @[Drain while given variable is true]
        bool continue_condition = true;

        auto continue_task = std::async(std::launch::async, [&continue_condition]{ for(int i = 0 ; i < 5 ; i++) continue_condition = true; continue_condition = false; });

        auto processing_tree =
            routing::drainer(continue_condition)
            <<= apply([](double const& in){ std::cout << in << ", "; return in; }) <<= alu::counter(2);

        processing_tree.run();
        continue_task.wait();
        // expected output may print for some suite of integers with step of 2
        /// @[Drain while given variable is true]
        std::cout << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(adjust_frame_length_test)
{
    using namespace dfp;
    using namespace dfp::routing;

#if ENABLE_FAILURE_TEST
    { // concept check
        // output flow width shall be divisible by the input flow width
        STATIC_CHECK((adjust_frame_length(size_t_const<2>{}) <<= const_frame(std::experimental::make_array(1, 2, 3))));
        // only accept size_t or size_t_const<> argument type
        STATIC_CHECK((adjust_frame_length(2.1) <<= 0));
    }
#endif
    { // runtime failure
        // because of incompatible input frame_length
        BOOST_CHECK_THROW((adjust_frame_length(2) <<= const_frame(std::vector<int>(4))), frame_length_error);
        BOOST_CHECK_NO_THROW((adjust_frame_length(4) <<= const_frame(std::vector<int>(2))));
        BOOST_CHECK_THROW((adjust_frame_length(3) <<= const_frame(std::vector<int>(2))), frame_length_error);
    }
    { // convert atomic_stream into default-witdh stream
        auto processing_branch = adjust_frame_length(default_frame_length_const<>{}) <<= 0;

        STATIC_CHECK((core::detail::supports_process_stream_returning_frame<decltype(processing_branch), core::detail::cell_input_ports_t<decltype(processing_branch)>>{}));
        STATIC_CHECK((core::detail::node_produces_frame<decltype(processing_branch)>{}));

        auto frame = processing_branch.drain();

        // adjust_frame_length node does not imply memory allocation in processing branch
#if (__clang__ || (__GNUC__ > 7))
        STATIC_CHECK(sizeof(processing_branch) == sizeof(0));
#else
        STATIC_CHECK(sizeof(processing_branch) == sizeof(0) + sizeof(void*));
#endif
        BOOST_CHECK((frame.size() == default_frame_length_const<>{}));
    }
    {
        /// @[Convert atomic_stream into static 10-framelength stream]
        auto processing_branch = adjust_frame_length(10_c) <<= alu::counter();
        auto frame = processing_branch.drain();

        int error_count = 0;
        auto i = *frame.begin();
        for(auto sample : frame)
        {
            error_count += sample != i++;
            std::cout << sample << " ";
        }
        std::cout << std::endl;
        // expected output : 0 1 2 3 4 5 6 7 8 9
        /// @[Convert atomic_stream into static 10-framelength stream]

        BOOST_CHECK_EQUAL(error_count, 0);
    }
    {
        /// @[Convert atomic_stream into dynamic 10-framelength stream]
        auto processing_branch = adjust_frame_length(10) <<= alu::counter();
        auto frame = processing_branch.drain();

        int error_count = 0;
        auto i = *frame.begin();
        for(auto sample : frame)
        {
            error_count += sample != i++;
            std::cout << sample << " ";
        }
        std::cout << std::endl;
        // expected output : 0 1 2 3 4 5 6 7 8 9
        /// @[Convert atomic_stream into dynamic 10-framelength stream]

        BOOST_CHECK_EQUAL(error_count, 0);
    }
    {
        auto processing_branch = adjust_frame_length(10) <<= const_frame(std::experimental::make_array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        auto frame = processing_branch.drain();

        int error_count = 0;
        auto i = *frame.begin();
        for(auto sample : frame)
        {
            error_count += sample != i++;
            std::cout << sample << " ";
        }
        std::cout << std::endl;
        // expected output : 0 1 2 3 4 5 6 7 8 9

        BOOST_CHECK_EQUAL(error_count, 0);
    }
    { // using shortname
        using namespace dfp::routing::shortname;

        auto processing_branch = adjustfl(256_c) <<= alu::counter();
        processing_branch.drain();
    }
    { // convert into fixed-flow stream
        using namespace dfp::routing::shortname;

        auto processing_branch = adjustfl(256) <<= alu::counter();
        auto frame = processing_branch.drain();

        int error_count = 0;
        auto i = *frame.begin();
        for(auto sample : frame)
        { error_count += sample != i++; }

        BOOST_CHECK_EQUAL(error_count, 0);
    }
    { // constexpr support
        constexpr auto const processing_branch = adjust_frame_length(size_t_const<3>{}) <<= 1.0;
#if (__clang__ || (__GNUC__ > 7))
        constexpr
#endif
        auto const frame = processing_branch.get_downmost_cell().cdrain();

#if (__clang__ || (__GNUC__ > 7))
        STATIC_CHECK((frame[0] == 1.0));
        STATIC_CHECK((frame[1] == 1.0));
        STATIC_CHECK((frame[2] == 1.0));
#else
        BOOST_CHECK_EQUAL(frame[0], 1.0);
        BOOST_CHECK_EQUAL(frame[1], 1.0);
        BOOST_CHECK_EQUAL(frame[2], 1.0);
#endif
    }
    { // constexpr support only since C++17
#if (__cplusplus >= 201703L)
        constexpr auto frame = (adjust_frame_length(size_t_const<3>{}) <<= 1.0).get_downmost_cell().cdrain();

        STATIC_CHECK((frame[0] == 1.0));
        STATIC_CHECK((frame[1] == 1.0));
        STATIC_CHECK((frame[2] == 1.0));
#endif
    }
}


BOOST_AUTO_TEST_CASE(sample_static_cast_test)
{
    using namespace dfp;

    { // check type traits
        using namespace dfp::routing;
        using namespace dfp::core::detail;

        STATIC_CHECK((node_produces_sample<decltype(sample_static_cast<custom_tag>() <<= const_sample(1).tag(custom_tag{}))>{})); 
    }
    {
        using namespace dfp::routing;

        ///@[sample_static_cast usage example]
        auto processing_branch = sample_static_cast<int>() <<= 1.1;

        std::cout << *processing_branch.drain().begin() << std::endl;
        //expected output: 1
        ///@[sample_static_cast usage example]

        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)>, int>{}));
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
    {
        using namespace dfp::routing::shortname;

        DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
        auto processing_branch = sscast<int>() <<= 1.1;

        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)>, int>{}));
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 1));
#else
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
#endif
    }
}


BOOST_AUTO_TEST_CASE(to_single_sample_test)
{
    using namespace dfp;

    { // concept check
        std::array<int, 3> const arr{{1, 2, 3}};

        STATIC_CHECK((detail::is_node<decltype(routing::to_single_sample() <<= routing::const_frame(arr))>::value));
        STATIC_CHECK((detail::node_produces_sample<decltype(routing::to_single_sample() <<= routing::const_frame(arr))>::value));
    }
    {
        ///@[to_single_sample usage example]
        constexpr std::size_t ARRAYLEN = 3;
        std::array<int, ARRAYLEN> const arr{{1, 2, 3}};
        auto print_array = apply<run_for>([](int const& in){ std::cout << in << " "; }, ARRAYLEN);
        
        auto processing_tree = print_array <<= alu::multiply(2) <<= routing::to_single_sample() <<= routing::const_frame(arr);
        processing_tree.run();
        // expected output : 2 4 6
        ///@[to_single_sample usage example]
        std::cout << std::endl;
    }
    { // constexpr expression
        //TODO: should be enabled starting with C++14
        // constexpr auto processing_branch = routing::to_single_sample() <<= routing::const_frame({1, 2, 3});
        
    }
}


BOOST_AUTO_TEST_CASE(const_frame_test)
{
    using namespace dfp;
    using namespace std::experimental;

    constexpr std::array<int, 3> const_arr{{1, 2, 3}};
    constexpr float const_c_arr[] = {7, 8, 9};
    float c_arr[] = {10, 11, 12};
//TODO    std::vector<short> vect{{10, 11, 12}};
    std::vector<short> const const_vect{{13, 14, 15}};

    { // type traits
        using namespace routing;
        namespace detail = dfp::core::detail;

        STATIC_CHECK((detail::is_source<decltype(const_frame(std::array<int, 1>{{0}}).build())>{})); // move-construction of inner frame
        STATIC_CHECK((detail::is_source<decltype(const_frame(const_arr).build())>{}));
        STATIC_CHECK((detail::is_source<decltype(const_frame(const_vect).build())>{}));
        STATIC_CHECK((detail::is_source<decltype(const_frame(const_c_arr).build())>{}));
        STATIC_CHECK((detail::is_source<decltype(const_frame(c_array<int, 2>{30, 31}).build())>{}));
        STATIC_CHECK((detail::is_source<decltype(const_frame(c_arr).build())>{}));
        STATIC_CHECK((detail::is_source<decltype(const_frame(make_array(20, 21)).build())>{}));
//TODO        STATIC_CHECK((detail::is_source<decltype(routing::const_frame(arr))>{}));
        STATIC_CHECK((!detail::source_produces_sample<decltype(const_frame(const_arr).build())>{}));
        STATIC_CHECK((detail::source_produces_frame<decltype(const_frame(const_arr).build())>{}));
    }
    #if 0
    { // concept check
        // line below won't compile because the input argument is neither a sequence container nor a C-array
        routing::const_frame(30);
    }
    #endif
    { // check for constexpr support
        STATIC_CHECK((const_arr[0] == 1));
        STATIC_CHECK((const_c_arr[0] == 7));
        STATIC_CHECK((c_array<int, 2>{30, 31}[0] == 30));  // c_array is a workaround for C++11 as std::initializer has poor constexpr support
        //TODO: add support for std::initializer_list in const_frame
//        auto constexpr pb2 = routing::const_frame({30, 31}); // TODO: but constexpr will likely not work in C++11
#if (__cplusplus >= 201703L)
        {
            auto constexpr pb = routing::const_frame(c_array<int, 2>{30, 31}).build();
            auto constexpr is_ok = [pb](){
                auto frame_it = pb.cdrain().cbegin(); return (frame_it[0] == 30) && (frame_it[1] == 31);
            };
            STATIC_CHECK((is_ok()));
        }
        {
            
            // need to initialize const_frame with r-value reference for constexpr support
            auto constexpr pb = routing::const_frame(std::experimental::to_array(const_c_arr)).build();
            auto constexpr is_ok = [pb](){
                auto frame_it = pb.cdrain().cbegin();
                return (frame_it[0] == 7) && (frame_it[1] == 8) && (frame_it[2] == 9);
            };
            STATIC_CHECK((is_ok()));
        }
#endif
    }
    { // compile-time failure because of negative channel count
#if ENABLE_FAILURE_TEST
            using namespace routing;
            using namespace std::experimental;

            auto frame_wrapper = frame_releasing() <<= const_frame(make_array(10, -2, 20, -2, 30, -2), -2_c);

#endif
    }
    {
        ///@[mono const_frame from vector]
        using namespace routing;

        auto fired_tree {
            frame_releasing() <<= const_frame(std::vector<int>{{30, 31}})
        };
        auto const& frame = fired_tree.get_result();

        BOOST_CHECK_EQUAL(fired_tree.input_stream_specifier().get_channel_count(), 1);
        BOOST_CHECK_EQUAL(frame[0], 30);
        BOOST_CHECK_EQUAL(frame[1], 31);
        ///@[mono const_frame from vector]
    }
    {
        ///@[2-channels const_frame from array]
        using namespace routing;
        using namespace std::experimental;

        auto fired_tree {
            frame_releasing() <<= const_frame(make_array(10, -2, 20, -2, 30, -2), 2_c)
        };
        auto const& frame = fired_tree.get_result();

        BOOST_CHECK_EQUAL(fired_tree.input_stream_specifier().get_channel_count(), 2);
        BOOST_CHECK_EQUAL(frame[0], 10);
        BOOST_CHECK_EQUAL(frame[1], -2);
        ///@[2-channels const_frame from array]
    }
    {
        ///@[2-channels const_frame specified at runtime]
        using namespace routing;
        using namespace std::experimental;

        std::size_t channel_count = 2;
        auto fired_tree {
            frame_releasing() <<= const_frame(make_array(10, -2, 20, -2, 30, -2), channel_count)
        };
        auto const& frame = fired_tree.get_result();

        BOOST_CHECK_EQUAL(fired_tree.input_stream_specifier().get_channel_count(), channel_count);
        BOOST_CHECK_EQUAL(frame[0], 10);
        BOOST_CHECK_EQUAL(frame[1], -2);
        ///@[2-channels const_frame specified at runtime]
    }
    { // check support on rvalue reference on c-array
        auto fired_tree  {
            frame_releasing() <<= routing::const_frame(c_array<int, 2>{30, 31})
        };

        auto const& frame = fired_tree.get_result();
        BOOST_CHECK_EQUAL(frame[0], 30);
        BOOST_CHECK_EQUAL(frame[1], 31);
    }
    {
        ///@[2-channels const_frame from c-array specified at runtime]
        using namespace routing;
        using namespace std::experimental;

        std::size_t channel_count = 2;
        auto fired_tree {
            frame_releasing() <<= const_frame(c_array<int, 6>{10, -2, 20, -2, 30, -2}, channel_count)
        };

        auto const& frame = fired_tree.get_result();

        BOOST_CHECK_EQUAL(fired_tree.input_stream_specifier().get_channel_count(), channel_count);
        BOOST_CHECK_EQUAL(frame[0], 10);
        BOOST_CHECK_EQUAL(frame[1], -2);
        ///@[2-channels const_frame from c-array specified at runtime]
    }
    {
        ///@[2-channels const_frame from const lvalue ref c-array specified at runtime]
        using namespace routing;
        using namespace std::experimental;

        std::array<int, 6> const in{10, -2, 20, -2, 30, -2};
        std::size_t channel_count = 2;
        auto fired_tree {
            frame_releasing() <<= const_frame(in, channel_count)
        };
        auto const& frame = fired_tree.get_result();

        BOOST_CHECK_EQUAL(fired_tree.input_stream_specifier().get_channel_count(), channel_count);
        BOOST_CHECK_EQUAL(frame[0], 10);
        BOOST_CHECK_EQUAL(frame[1], -2);
        ///@[2-channels const_frame from const lvalue ref c-array specified at runtime]
    }
}


BOOST_AUTO_TEST_CASE(iterator_test)
{
    {
        ///@[from_iterator usage example]
        using namespace dfp;
        using namespace dfp::routing;

        std::vector<int> const container = {1, 3, 5};
        std::size_t i = 0;
        auto check = [&i, &container](int const& val){ BOOST_CHECK_EQUAL(container[i++]*2, val); };

        looping(container.size()) <<= check <<= alu::add()  += from_iterator(container.begin())
                                                            |= from_iterator(container.begin());
        ///@[from_iterator usage example]
    }
    {
        ///@[from_iterator usage example with ref]
        using namespace dfp;
        using namespace dfp::routing;

        std::vector<int> const container = {1, 2, 3, 5, 7};
        auto iterator = container.begin();
        std::size_t i = 0;
        std::size_t j = 0;
        auto check = [&](int const& val){ BOOST_CHECK_EQUAL(container[i] + container[j], val); i=j+1; j=i+1; };

        looping(container.size() / 2) <<= check <<= alu::add()  += from_iterator(std::ref(iterator))
                                                                |= from_iterator(std::ref(iterator));
        ///@[from_iterator usage example with ref]
    }
}


BOOST_AUTO_TEST_CASE(mutable_frame_test)
{
    using namespace dfp;

    std::array<int, 3> arr{{4, 5, 6}};
    float c_arr[] = {10, 11, 12};
    std::vector<int> vect{{10, 11, 12}};

    { // type traits check
        STATIC_CHECK((detail::is_source<decltype(routing::mutable_frame(arr).build())>{}));
        STATIC_CHECK((output_stream_specifier_t<decltype(routing::mutable_frame(arr).build())>::FRAME_EXTENT != DYNAMIC_EXTENT));
        STATIC_CHECK((detail::is_source<decltype(routing::mutable_frame(vect).build())>{}));
        STATIC_CHECK((output_stream_specifier_t<decltype(routing::mutable_frame(vect).build())>::FRAME_EXTENT == DYNAMIC_EXTENT));
        STATIC_CHECK((detail::is_source<decltype(routing::mutable_frame(c_arr).build())>{}));
        STATIC_CHECK((output_stream_specifier_t<decltype(routing::mutable_frame(c_arr).build())>::FRAME_EXTENT != DYNAMIC_EXTENT));
    }
    { // check error message
#if ENABLE_FAILURE_TEST
        routing::mutable_frame(std::experimental::make_array(1, 2));
        routing::mutable_frame(std::vector<int>(2));
        routing::mutable_frame(c_array<int, 2>{30, 31});
#endif
    }
    {
        ///@[mutable_frame usage example with std::array as input]
        using namespace dfp;
        using namespace dfp::routing;

        std::array<int, 2> arr{{4, 5}};

        auto processing_tree = sample_releasing() <<= to_single_sample() <<= mutable_frame(arr);

        std::cout << *processing_tree << " " << *++processing_tree << " ";
        arr[0] = 1; arr[1] = 2;
        std::cout << *++processing_tree << " " <<  *++processing_tree;
        std::cout << std::endl;
        // expected output : 4 5 1 2
        ///@[mutable_frame usage example with std::array as input]
        BOOST_CHECK_EQUAL(*++processing_tree, 1);
        BOOST_CHECK_EQUAL(*++processing_tree, 2);
        arr[0] = 3; arr[1] = 4;
        BOOST_CHECK_EQUAL(*++processing_tree, 3);
        BOOST_CHECK_EQUAL(*++processing_tree, 4);
    }
    {
        ///@[mutable_frame usage example with std::vector as input]
        using namespace dfp;
        using namespace dfp::routing;

        std::vector<int> vect{{4, 5}};

        auto processing_tree = sample_releasing() <<= to_single_sample() <<= mutable_frame(vect);

        std::cout << *processing_tree << " " << *++processing_tree << " ";
        vect[0] = 1; vect[1] = 2;
        std::cout << *++processing_tree << " " <<  *++processing_tree;
        std::cout << std::endl;
        // expected output : 4 5 1 2
        ///@[mutable_frame usage example with std::vector as input]
        BOOST_CHECK_EQUAL(*++processing_tree, 1);
        BOOST_CHECK_EQUAL(*++processing_tree, 2);
        vect[0] = 3; vect[1] = 4;
        BOOST_CHECK_EQUAL(*++processing_tree, 3);
        BOOST_CHECK_EQUAL(*++processing_tree, 4);
    }
    {
        ///@[mutable_frame usage example with C-array as input]
        using namespace dfp;
        using namespace dfp::routing;

        int arr[] = {4, 5};

        auto processing_tree = sample_releasing() <<= to_single_sample() <<= mutable_frame(arr);

        std::cout << *processing_tree << " " << *++processing_tree << " ";
        arr[0] = 1; arr[1] = 2;
        std::cout << *++processing_tree << " " <<  *++processing_tree;
        std::cout << std::endl;
        // expected output : 4 5 1 2
        ///@[mutable_frame usage example with C-array as input]
        BOOST_CHECK_EQUAL(*++processing_tree, 1);
        BOOST_CHECK_EQUAL(*++processing_tree, 2);
        arr[0] = 3; arr[1] = 4;
        BOOST_CHECK_EQUAL(*++processing_tree, 3);
        BOOST_CHECK_EQUAL(*++processing_tree, 4);
    }
    {
        ///@[mutable_frame usage example with 2-channels C-array as input]
        using namespace dfp;
        using namespace dfp::routing;

        int arr[] = {4, 3, 2, 1};

        auto runnable_tree = make_runnable(
            frame_releaser() <<= mutable_frame(arr, 2_c)
        );

        auto frame = runnable_tree.run();
        std::cout << frame[0] << " " << frame[1] << " " << frame[2] << " " <<  frame[3] << " ";
        arr[0] = 5; arr[1] = 6;
        frame = runnable_tree.run();
        std::cout << frame[0] << " " << frame[1] << " " << frame[2] << " " <<  frame[3];
        std::cout << std::endl;
        // expected output : 4 3 2 1 5 6 2 1
        ///@[mutable_frame usage example with 2-channels C-array as input]
        BOOST_CHECK_EQUAL(runnable_tree.input_stream_specifier().get_channel_count(), 2);
        auto frame_iterator = runnable_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*frame_iterator++, 5);
        BOOST_CHECK_EQUAL(*frame_iterator++, 6);
        BOOST_CHECK_EQUAL(*frame_iterator++, 2);
        BOOST_CHECK_EQUAL(*frame_iterator++, 1);
        arr[0] = 4; arr[1] = 3;
        frame_iterator = runnable_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*frame_iterator++, 4);
        BOOST_CHECK_EQUAL(*frame_iterator++, 3);
        BOOST_CHECK_EQUAL(*frame_iterator++, 2);
        BOOST_CHECK_EQUAL(*frame_iterator++, 1);
    }
    {
        ///@[mutable_frame usage example with std::span as input]
        using namespace dfp;
        using namespace dfp::routing;

        int arr1[] = {4, 5, 6};
        int arr2[] = {1, 2, 3};
        auto frame = upg::make_span(arr1, 2);

        auto processing_tree = sample_releasing() <<= to_single_sample() <<= mutable_frame(frame);

        std::cout << *processing_tree << " " << *++processing_tree << " ";
        frame = upg::make_span(arr2, 2);
        std::cout << *++processing_tree << " " <<  *++processing_tree;
        std::cout << std::endl;
        // expected output : 4 5 1 2
        ///@[mutable_frame usage example with std::span as input]
        BOOST_CHECK_EQUAL(*++processing_tree, 1);
        BOOST_CHECK_EQUAL(*++processing_tree, 2);
        frame = upg::make_span(arr1, 2);
        BOOST_CHECK_EQUAL(*++processing_tree, 4);
        BOOST_CHECK_EQUAL(*++processing_tree, 5);
    }
    {
        ///@[2-channels mutable_frame from array]
        using namespace routing;
        using namespace std::experimental;

        auto mutable_array = make_array(10, -2, 20, -2, 30, -2);
        auto runnable_tree = make_runnable(
            frame_releaser() <<= mutable_frame(mutable_array, 2_c)
        );
        auto frame = runnable_tree.run();

        BOOST_CHECK_EQUAL(runnable_tree.input_stream_specifier().get_channel_count(), 2);
        BOOST_CHECK_EQUAL(frame.data()[0], 10);
        BOOST_CHECK_EQUAL(frame.data()[1], -2);
        mutable_array = make_array(1, -20, 20, -2, 30, -2);
        frame = runnable_tree.run();
        BOOST_CHECK_EQUAL(frame.data()[0], 1);
        BOOST_CHECK_EQUAL(frame.data()[1], -20);
        ///@[2-channels mutable_frame from array]
    }
    {
        ///@[2-channels mutable_frame specified at runtime]
        using namespace routing;
        using namespace std::experimental;

        auto mutable_array = make_array(10, -2, 20, -2, 30, -2);
        std::size_t channel_count = 2;
        auto processing_tree = frame_releasing() <<= mutable_frame(mutable_array, channel_count);
        auto const& frame = processing_tree.get_result();

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_channel_count(), channel_count);
        BOOST_CHECK_EQUAL(frame.data()[0], 10);
        BOOST_CHECK_EQUAL(frame.data()[1], -2);
        ///@[2-channels mutable_frame specified at runtime]
    }
    {
        ///@[2-channels mutable_frame from c-array specified at runtime]
        using namespace routing;
        using namespace std::experimental;

        int c_array[]{10, -2, 20, -2, 30, -2};
        std::size_t channel_count = 2;
        auto processing_tree = frame_releasing() <<= mutable_frame(c_array, channel_count);
        auto const& frame = processing_tree.get_result();

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_channel_count(), channel_count);
        BOOST_CHECK_EQUAL(frame.data()[0], 10);
        BOOST_CHECK_EQUAL(frame.data()[1], -2);
        ///@[2-channels mutable_frame from c-array specified at runtime]
    }
    {
        #if DFP_CPP17_SUPPORT
        using namespace alu;
        using namespace routing;
        using namespace std::experimental;

        int c_array0[]{1, 2, 3, 5, 5, 6};
        int c_array1[]{10, -2, 20, -2, 30, -2};
        std::size_t channel_count = 2;
        auto processing_tree =
            frame_releasing() <<= apply_f([](auto const& in0, auto const& in1){ return in0; })  += mutable_frame(c_array1, channel_count)
                                                                                                |= mutable_frame(c_array0, channel_count)
            ;
        auto const& frame = processing_tree.get_result();

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_channel_count(), 1);
        BOOST_CHECK_EQUAL(frame.data()[0], 1);
        BOOST_CHECK_EQUAL(frame.data()[1], 2);
        #endif
    }
}


BOOST_AUTO_TEST_CASE(tag_test)
{
    {
        ///@[tag usage example]
        using namespace dfp;
        using namespace dfp::routing;

        struct my_tag : custom_tag {};

        auto processing_tree =
            sample_releasing() <<= sample_static_cast(my_tag{}) <<= alu::multiply(1.1f) <<= const_sample(20).tag(my_tag{})
        ;
        static_assert(std::is_same<decltype(processing_tree.get_result()), int const&>{}, "output sample of the processing is same as type of value 20");

        auto processing_tree2 =
            sample_releasing() <<= sample_static_cast(my_tag{}) <<= alu::multiply(2.0) <<= alu::multiply(1.1f).tag(my_tag{}) <<= 20
        ;
        static_assert(std::is_same<decltype(processing_tree2.get_result()), float const&>{}, "output sample of the processing is same as type of value 20*1.1f");
        ///@[tag usage example]

        BOOST_CHECK_EQUAL(processing_tree.get_result(), (int)(20*1.1f));
        BOOST_CHECK_EQUAL(processing_tree2.get_result(), (float)(20*1.1f*2.0));
    }
}


BOOST_AUTO_TEST_CASE(switch_if_test)
{
    //TODO switch_if().then()
    //TODO constexpr

    using namespace dfp;
    using namespace dfp::routing;

    #if 0
    { // type input consistency
        // line below won't compile because nullptr and double are incompatible
        auto processing_branch = switch_if()  += nullptr
                                            |= 0.0
                                            |= false;
        *processing_branch.drain().begin();
    }
    #endif
    {
        auto processing_branch = sample_releasing() <<= switch_if() += 0
                                                                    |= 2.1
                                                                    |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 0.0);
        BOOST_CHECK_EQUAL(*++processing_branch, 0.0);
        BOOST_CHECK_EQUAL(*++processing_branch, 2.1);
        BOOST_CHECK_EQUAL(*++processing_branch, 2.1);
    }
    {
        auto processing_branch = sample_releasing() <<= switch_if() += "zero"
                                                                    |= "one"
                                                                    |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK(!strcmp(*processing_branch, "zero"));
        BOOST_CHECK(!strcmp(*++processing_branch, "zero"));
        BOOST_CHECK(!strcmp(*++processing_branch, "one"));
        BOOST_CHECK(!strcmp(*++processing_branch, "one"));
    }
    #if !defined(__GNUC__) || defined(__clang__)
    // test case disable for gcc because
    // if fails to apply array to pointer conversion for rvalue reference
    // (c.f. https://stackoverflow.com/questions/32941846/c-error-taking-address-of-temporary-array)
    {
        auto processing_branch = sample_releasing() <<= switch_if() += c_array<int16_t, 3>{0x3031, 0x3233, 0}
                                                                    |= c_array<int16_t, 2>{0x3435, 0}
                                                                    |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK(!strcmp((const char*)*processing_branch, (const char*)c_array<int16_t, 3>{0x3031, 0x3233, 0}));
        BOOST_CHECK(!strcmp((const char*)*++processing_branch, (const char*)c_array<int16_t, 3>{0x3031, 0x3233, 0}));
        BOOST_CHECK(!strcmp((const char*)*++processing_branch, (const char*)c_array<int16_t, 2>{0x3435, 0}));
        BOOST_CHECK(!strcmp((const char*)*++processing_branch, (const char*)c_array<int16_t, 2>{0x3435, 0}));
    }
    #endif
    {
        auto then_branch = const_sample(2.1);
        auto else_branch = alu::multiply(2) <<= then_branch;
        auto processing_branch =
            sample_releasing() <<= switch_if()  += std::move(else_branch)
                                                |= std::move(then_branch)
                                                |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 2.1*2);
        BOOST_CHECK_EQUAL(*++processing_branch, 2.1*2);
        BOOST_CHECK_EQUAL(*++processing_branch, 2.1);
        BOOST_CHECK_EQUAL(*++processing_branch, 2.1);
    }
    {
        ///@[switch_if usage example]
        using namespace dfp::routing;

        auto processing_branch = sample_releasing() <<= switch_if()   += 'E'
                                                                      |= alu::add(1) <<= 'S'
                                                                      |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'T');
        ///@[switch_if usage example]
    }
    {
        ///@[switch_if with ELSE branch as parameter]
        using namespace dfp::routing;

        auto processing_branch = sample_releasing() <<= switch_if('E')  += alu::add(1) <<= 'S'
                                                                        |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'T');
        ///@[switch_if with ELSE branch as parameter]
    }
    {
        ///@[switch_if with THEN and ELSE branches as parameters]
        using namespace dfp::routing;

        auto processing_branch =
            sample_releasing() <<= switch_if(alu::add(1) <<= 'S', 'E') <<= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'T');
        ///@[switch_if with THEN and ELSE branches as parameters]
    }
    {
        using namespace dfp::routing;
        auto processing_branch = sample_releasing() <<= switch_if().pi('T', 'E') <<= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'T');
    }
    {
        // implicit conversion of "else" arguments to input stream
        using namespace dfp::routing;
        auto processing_branch = sample_releasing() <<= switch_if('E')  += 'T'
                                                                        |= alu::greater_than(1) <<= alu::counter();

        BOOST_CHECK_EQUAL(*processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'E');
        BOOST_CHECK_EQUAL(*++processing_branch, 'T');
    }
}


BOOST_AUTO_TEST_CASE(delay_test)
{
    using namespace dfp;
    using namespace dfp::routing;

    #if ENABLE_FAILURE_TEST
    {
        // line below will assert because delay node is a single input node
        delay().build(0, 0);

        // line below will assert because of invalid type for delay length
        delay("1") <<= 1;

        // line below will assert because flow width different from 1 not yet supported
        delay() <<= const_frame(std::experimental::make_array(1, 2));
    }
    #endif
    {
        ///@[delay node usage with delay length as std::size_t]
        auto const processing_branch = delay(2) <<= const_sample(1, 2, 3, 4);

        looping(6) <<= [](int const& i){ std::cout << i << ','; } <<= processing_branch;
        //expected output: 0,0,1,2,3,4
        ///@[delay node usage with delay length as std::size_t]

        std::cout << std::endl;
        auto tree = sample_releasing() <<= processing_branch;
        BOOST_CHECK_EQUAL(*tree, 0);
        BOOST_CHECK_EQUAL(*++tree, 0);
        BOOST_CHECK_EQUAL(*++tree, 1);
        BOOST_CHECK_EQUAL(*++tree, 2);
        BOOST_CHECK_EQUAL(*++tree, 3);
        BOOST_CHECK_EQUAL(*++tree, 4);
        BOOST_CHECK_EQUAL(*++tree, 1);
        BOOST_CHECK_EQUAL(*++tree, 2);
        BOOST_CHECK_EQUAL(*++tree, 3);
    }
    {
        // initial value check
        auto const processing_branch = delay(2, -1) <<= 1;

        looping(4) <<= [](int const& i){ std::cout << i << ','; } <<= processing_branch;

        std::cout << std::endl;
        auto tree = sample_releasing() <<= processing_branch;
        BOOST_CHECK_EQUAL(*tree++, -1);
        BOOST_CHECK_EQUAL(*tree++, -1);
        BOOST_CHECK_EQUAL(*tree++, 1);
        BOOST_CHECK_EQUAL(*tree++, 1);
    }
    { // special case where delay length is 0
        BOOST_REQUIRE_THROW(delay(0) <<= 1, std::invalid_argument);
    }
    {
        ///@[delay node usage with delay length as size_t integral constant]
        auto const processing_branch = delay(3_c) <<= 1.1;

        looping(4) <<= [](double const& i){ std::cout << i << ','; } <<= processing_branch;
        //expected output: 0,0,0,1.1
        ///@[delay node usage with delay length as size_t integral constant]
     
        auto pb = processing_branch;
        BOOST_CHECK_EQUAL(*pb.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*pb.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*pb.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*pb.drain().begin(), 1.1);
        BOOST_CHECK_EQUAL(*pb.drain().begin(), 1.1);
    }
    {
        //invoke optimized version of delay node (for const delay)
        auto processing_branch = delay(2_c) <<= alu::counter(1, 5);
     
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 5);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 6);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 7);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 8);
    }
    #if 0 //TODO
    { // set type of initial value
        auto processing_branch = delay<as_input0_tag>(2) <<= alu::counter(1, 5);

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 5);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 5);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 5);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 6);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 7);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 8);
    }
    #endif
    #if 0 // TODO
    {
        // verify behaviour with const input
        constexpr int value = 1;
        constexpr auto processing_branch = delay() <<= value;

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
    #endif
}

BOOST_AUTO_TEST_CASE(synced_buffer_test)
{
    {
        ///@[Synced buffer usage]
        using namespace dfp;
        using namespace routing;

        synced_buffer::share<int, 20> shared_buffer;

        std::cout << std::endl;
        std::cout << "Synced buffer usage with non-static frame-length input" << std::endl;

        auto producer = make_runnable(
            looper(shared_buffer.max_length()) <<= synced_buffer::write(shared_buffer)
            <<= adjust_frame_length(5) <<= alu::counter()
        );

        auto consumer = std::async(std::launch::async, [&shared_buffer]{
            looping(shared_buffer.max_length()) <<= [](int const& in){ std::cout << in << ", "; }
            <<= to_single_sample() <<= synced_buffer::read(shared_buffer, 4_c);
        });

        std::this_thread::yield(); //allow async task to start
        producer.run(); // feed sync_buffer with samples
        consumer.wait(); // wait for async task to complete
        ///@[Synced buffer usage]
    }
    {   // constant-flow support
        using namespace dfp;
        using namespace routing;

        synced_buffer::share<int, 20> shared_buffer;

        std::cout << std::endl;
        std::cout << "Synced buffer usage with static frame-length input" << std::endl;

        auto producer = make_runnable(
            looper(shared_buffer.max_length()) <<= synced_buffer::write(shared_buffer) <<= adjust_frame_length(5_c) <<= alu::counter()
        );

        auto consumer = std::async(std::launch::async, [&shared_buffer]{
            looping(shared_buffer.max_length()) <<= [](int const& in){ std::cout << in << ", "; return in; }
            <<= to_single_sample() <<= synced_buffer::read(shared_buffer, 4_c);
        });

        std::this_thread::yield();
        producer.run();
        consumer.wait();
    }
    {   // check runtime failure
        using namespace dfp;
        using namespace routing;

        synced_buffer::share<int, 20> shared_buffer;

        BOOST_CHECK_THROW(
            (looping(shared_buffer.max_length()) <<= synced_buffer::write(shared_buffer) <<= adjust_frame_length(512_c) <<= alu::counter()),
            frame_length_error
        );
    }
}


#if DFP_CPP14_SUPPORT
BOOST_AUTO_TEST_CASE(hold_test) {
    {
        std::cout << std::endl;

        ///@[hold usage example]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        const double threshold = -0.4;

        auto sinewave = math::sin() <<= multiply(2*M_PI/20) <<= counter(INT32_T);
        output_sample_t<decltype(sinewave)> last_sample = 0;
        constexpr struct : custom_tag {} SINEWAVE;
        constexpr struct : custom_tag {} SINEWAVE_REF;
    
        // equivalent implementation without usage of hold node
        auto reference =
            switch_if() += from(SINEWAVE_REF)
                        |= [&last_sample]() { return last_sample; }
                        |= [&last_sample](bool const& c, decltype(last_sample) const& u){ if (!c) last_sample = u; return c; } 
                                    += from(SINEWAVE_REF)
                                    |= less_than(threshold) <<= unsafe_probe(SINEWAVE_REF) <<= sinewave
        ;

        std::cout << std::endl << std::endl;
        // check processing implementation based on "hold" node against reference implementation
        looping(40) <<= [](double const& in) { BOOST_CHECK_EQUAL(0, in); return nullptr; }
        <<= subtract()  += reference
                        |= hold() += from(SINEWAVE)
                                  |= less_than(threshold) <<= unsafe_probe(SINEWAVE) <<= sinewave
        ;
        ///@[hold usage example]
    }
}
#endif // #if DFP_CPP14_SUPPORT


BOOST_AUTO_TEST_CASE(extract_channel_test) {
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= extract_channel(0) <<= const_frame({1, 3, 2, 4, 5, 6}, 2_c);
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(get_length(frame), 3);
        BOOST_CHECK_EQUAL(*it++, 1);
        BOOST_CHECK_EQUAL(*it++, 2);
        BOOST_CHECK_EQUAL(*it++, 5);
        BOOST_CHECK_THROW(frame_releasing() <<= extract_channel(2) <<= const_frame({1, 3, 2, 4, 5, 6}, 2_c), std::invalid_argument);
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= extract_channel(1) <<= const_frame({1, 3, 2, 4, 5, 6}, 2_c);
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(*it++, 3);
        BOOST_CHECK_EQUAL(*it++, 4);
        BOOST_CHECK_EQUAL(*it++, 6);
    }
    {
        ///@[extract-channel node usage]
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= extract_channel(1) <<= const_frame({1, 3, 2, 4, 5, 6}, 2);
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(get_length(frame), 3);
        BOOST_CHECK_EQUAL(*it++, 3);
        BOOST_CHECK_EQUAL(*it++, 4);
        BOOST_CHECK_EQUAL(*it++, 6);
        BOOST_CHECK_THROW(frame_releasing() <<= extract_channel(2) <<= const_frame({1, 3, 2, 4, 5, 6}, 2), std::invalid_argument);
        ///@[extract-channel node usage]
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        int input[] {1, 3, 2, 4, 5, 6};
        auto frame_wrapper = frame_releasing() <<= extract_channel(1) <<= mutable_frame(input, 2_c);
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(*it++, 3);
        BOOST_CHECK_EQUAL(*it++, 4);
        BOOST_CHECK_EQUAL(*it++, 6);
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        int input[] {1, 3, 2, 4, 5, 6};
        auto frame_wrapper = frame_releasing() <<= extract_channel(1) <<= mutable_frame(input, 2);
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(*it++, 3);
        BOOST_CHECK_EQUAL(*it++, 4);
        BOOST_CHECK_EQUAL(*it++, 6);
    }
}


BOOST_AUTO_TEST_CASE(channelize_test) {
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= channelize() += const_frame({15, 16, 25, 26, 35, 36}, 2_c)
                                                                |= const_frame({14, 24, 34})
                                                                |= const_frame({11, 12, 13, 21, 22, 23, 31, 32, 33}, 3_c)
        ;
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(get_length(frame), 18);
        BOOST_CHECK_EQUAL(*it++, 11); BOOST_CHECK_EQUAL(*it++, 12); BOOST_CHECK_EQUAL(*it++, 13); BOOST_CHECK_EQUAL(*it++, 14);
            BOOST_CHECK_EQUAL(*it++, 15); BOOST_CHECK_EQUAL(*it++, 16);
        BOOST_CHECK_EQUAL(*it++, 21); BOOST_CHECK_EQUAL(*it++, 22); BOOST_CHECK_EQUAL(*it++, 23); BOOST_CHECK_EQUAL(*it++, 24);
            BOOST_CHECK_EQUAL(*it++, 25); BOOST_CHECK_EQUAL(*it++, 26);
        BOOST_CHECK_EQUAL(*it++, 31); BOOST_CHECK_EQUAL(*it++, 32); BOOST_CHECK_EQUAL(*it++, 33); BOOST_CHECK_EQUAL(*it++, 34);
            BOOST_CHECK_EQUAL(*it++, 35); BOOST_CHECK_EQUAL(*it++, 36);
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto processing_tree = frame_releaser() <<= channelize() <<= const_frame(std::vector<int>{15, 16, 25, 26, 35, 36}, 2_c);

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_frame_length(), 6);
        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_channel_count(), 2);
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto processing_tree = frame_releaser() <<= channelize() <<= const_frame({15, 16, 25, 26, 35, 36}, 2_c);

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_frame_length(), 6);
        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_channel_count(), 2);
    }
    {
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= channelize() += const_frame({15, 16, 25, 26, 35, 36}, 2_c)
                                                                |= const_frame(std::vector<int>{14, 24, 34})
                                                                |= const_frame({11, 12, 13, 21, 22, 23, 31, 32, 33}, 3_c)
        ;

        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(get_length(frame), 18);
        BOOST_CHECK_EQUAL(*it++, 11); BOOST_CHECK_EQUAL(*it++, 12); BOOST_CHECK_EQUAL(*it++, 13); BOOST_CHECK_EQUAL(*it++, 14);
            BOOST_CHECK_EQUAL(*it++, 15); BOOST_CHECK_EQUAL(*it++, 16);
        BOOST_CHECK_EQUAL(*it++, 21); BOOST_CHECK_EQUAL(*it++, 22); BOOST_CHECK_EQUAL(*it++, 23); BOOST_CHECK_EQUAL(*it++, 24);
            BOOST_CHECK_EQUAL(*it++, 25); BOOST_CHECK_EQUAL(*it++, 26);
        BOOST_CHECK_EQUAL(*it++, 31); BOOST_CHECK_EQUAL(*it++, 32); BOOST_CHECK_EQUAL(*it++, 33); BOOST_CHECK_EQUAL(*it++, 34);
            BOOST_CHECK_EQUAL(*it++, 35); BOOST_CHECK_EQUAL(*it++, 36);

    }
    {
        ///@[channelize usage example]
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper = frame_releasing() <<= channelize() += const_frame({15, 16, 25, 26, 35, 36}, 2)
                                                                |= const_frame({14, 24, 34})
        ;
        auto const& frame = frame_wrapper.get_result();
        auto it = std::begin(frame);

        BOOST_CHECK_EQUAL(get_length(frame), 9);
        BOOST_CHECK_EQUAL(*it++, 14); BOOST_CHECK_EQUAL(*it++, 15); BOOST_CHECK_EQUAL(*it++, 16);
        BOOST_CHECK_EQUAL(*it++, 24); BOOST_CHECK_EQUAL(*it++, 25); BOOST_CHECK_EQUAL(*it++, 26);
        BOOST_CHECK_EQUAL(*it++, 34); BOOST_CHECK_EQUAL(*it++, 35); BOOST_CHECK_EQUAL(*it++, 36);
        ///@[channelize usage example]
    }
}


BOOST_AUTO_TEST_CASE(convey_if_test) {
#if ENABLE_FAILURE_TEST
    {
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        // invalid sample type
        sample_releaser() <<= routing::convey()  += std::vector<int>{}
                                                |= counter(INT16_T)
                                                |= add(100) <<= counter(INT16_T)
        ;

        // invalid sample type
        sample_releaser() <<= routing::convey()  += true
                                                |= counter(DOUBLE)
                                                |= add(100) <<= counter(INT16_T)
        ;

        // invalid input count
        sample_releaser() <<= routing::convey()  += counter(INT16_T)
                                                |= add(100) <<= counter(INT16_T)
        ;

        // invalid selector value
        sample_releaser() <<= routing::convey()  += -1_c
                                                |= counter(INT16_T)
                                                |= add(100) <<= counter(INT16_T)
        ;
    }
#endif
    {
        std::cout << std::endl;

        ///@[convey usage example]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        std::cout << "convey usage example:" << std::endl;

        auto processing =
            sample_releaser() <<= routing::convey()  += to_single_sample() <<= const_frame({true, false, true, false})
                                                    |= add(100) <<= counter(INT16_T)
                                                    |= counter(INT16_T)
        ;

        std::cout << processing.run() << " " << processing.run() << " " << processing.run() << " " << processing.run();
        // expected result
        // 100 1 102 3
        ///@[convey usage example]
        BOOST_CHECK_EQUAL(processing.run(), 104);
        BOOST_CHECK_EQUAL(processing.run(), 5);
        BOOST_CHECK_EQUAL(processing.run(), 106);
        BOOST_CHECK_EQUAL(processing.run(), 7);
        
    }
    {
        std::cout << std::endl;

        ///@[convey usage example with preset]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        std::cout << "convey usage example with preset:" << std::endl;

        int16_t alt = -1;

        auto processing =
            sample_releaser()
            <<= routing::convey(2_c)    += add(100) <<= counter(INT16_T)
                                        |= -1
                                        |= [&](int16_t const& in){ alt = in; } <<= counter(INT16_T)
        ;

        std::cout   << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " "
                    << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " ";
        // expected result
        // 100/0 101/1 102/2 103/3
        ///@[convey usage example with preset]
        BOOST_CHECK_EQUAL(processing.run(), 104);
        BOOST_CHECK_EQUAL(alt, 4);
        BOOST_CHECK_EQUAL(processing.run(), 105);
        BOOST_CHECK_EQUAL(alt, 5);
        BOOST_CHECK_EQUAL(processing.run(), 106);
        BOOST_CHECK_EQUAL(alt, 6);
        BOOST_CHECK_EQUAL(processing.run(), 107);
        BOOST_CHECK_EQUAL(alt, 7);        
    }
    {
        std::cout << std::endl;

        ///@[convey1st usage example]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        std::cout << "convey1st usage example:" << std::endl;

        int16_t alt = -1;

        auto processing =
            sample_releaser()
            <<= routing::convey1st()    += [&](int16_t const& in){ alt = in; } <<= counter(INT16_T)
                                        |= add(100) <<= counter(INT16_T)
        ;

        std::cout   << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " "
                    << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " ";
        // expected result
        // 100/0 101/1 102/2 103/3
        ///@[convey1st usage example]
        BOOST_CHECK_EQUAL(processing.run(), 104);
        BOOST_CHECK_EQUAL(alt, 4);
        BOOST_CHECK_EQUAL(processing.run(), 105);
        BOOST_CHECK_EQUAL(alt, 5);
        BOOST_CHECK_EQUAL(processing.run(), 106);
        BOOST_CHECK_EQUAL(alt, 6);
        BOOST_CHECK_EQUAL(processing.run(), 107);
        BOOST_CHECK_EQUAL(alt, 7);        
    }
    {
        std::cout << std::endl;

        ///@[convey2nd usage example]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::routing;

        std::cout << "convey2nd usage example:" << std::endl;

        int16_t alt = -1;

        auto processing =
            sample_releaser()
            <<= routing::convey2nd()    += add(100) <<= counter(INT16_T)
                                        |= [&](int16_t const& in){ alt = in; } <<= counter(INT16_T)
        ;

        std::cout   << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " "
                    << processing.run() << "/" << alt << " " << processing.run() << "/" << alt << " ";
        // expected result
        // 100/0 101/1 102/2 103/3
        ///@[convey2nd usage example]
        BOOST_CHECK_EQUAL(processing.run(), 104);
        BOOST_CHECK_EQUAL(alt, 4);
        BOOST_CHECK_EQUAL(processing.run(), 105);
        BOOST_CHECK_EQUAL(alt, 5);
        BOOST_CHECK_EQUAL(processing.run(), 106);
        BOOST_CHECK_EQUAL(alt, 6);
        BOOST_CHECK_EQUAL(processing.run(), 107);
        BOOST_CHECK_EQUAL(alt, 7);        
    }
}
