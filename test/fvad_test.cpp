/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/fvad/vad.hpp>
#include <dfptl/sndfile/sndfile.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/routing/sample_static_cast.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>

#include <boost/test/unit_test.hpp>


#include <dfptl/io/write_text_chart.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#define CHECK_FAILURE 0


BOOST_AUTO_TEST_CASE(fvad_test)
{
    { // check concept failures
#if CHECK_FAILURE
        using namespace dfp;
        using namespace dfp::fvad;
        // will fail because input stream has invalid sample type
        vad(0) <<= routing::const_frame(std::array<int, 1>{});

        // will fail because input stream has invalid flow width
        vad(int_const<1>{}) <<= routing::const_frame(std::array<int16_t, 2>{});
#endif
    }

    ///@[fvad usage example]
    using namespace dfp;
    using namespace dfp::fvad;
    using namespace dfp::routing;

    SF_INFO sf_info;
    SNDFILE* file_handle = sf_open("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", SFM_READ, &sf_info);
    auto FRAME_LENGTH = size_t_const<960>{};

    // storing vad detection in wav file
    looping(sf_info.frames / FRAME_LENGTH) <<= sndfile::write("output/fvad_out.wav", sf_info.samplerate / FRAME_LENGTH)
        <<= sample_static_cast<short>() <<= vad(sf_info.samplerate, 3)
        <<= sndfile::read(file_handle, INT16_T, FRAME_LENGTH)
    ;

    ///@[fvad usage example]

    { // check for failure of input frame length
        BOOST_REQUIRE_THROW(sample_releasing() <<= vad(48000, 3) <<= const_frame(std::array<int16_t, 959>{}), frame_length_error);
    }

    // using integral constant for sampling rate parameter
    looping(sf_info.frames / FRAME_LENGTH) <<= vad(48000_c, 3)
    <<= sndfile::read("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", INT16_T, FRAME_LENGTH);
#if 0
    // will fail to compile because input frame length is not supported
    looping(sf_info.frames / FRAME_LENGTH) <<= vad(48001_c, 3) <<= const_frame(std::array<int16_t, FRAME_LENGTH>{});
    looping(sf_info.frames / FRAME_LENGTH) <<= vad(44000_c, 3) <<= const_frame(std::array<int16_t, FRAME_LENGTH>{});
#endif
    {
        ///@[fvad usage example2]
        using namespace dfp;
        using namespace dfp::fvad;
        using namespace dfp::routing;

        auto FRAME_LENGTH = size_t_const<960>{};
        Fvad* vad_state = fvad_new();

        SF_INFO sf_info;
        SNDFILE* file_handle = sf_open("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", SFM_READ, &sf_info);

        fvad_set_mode(vad_state, 3);
        fvad_set_sample_rate(vad_state, sf_info.samplerate);

        // prefetch the Fvad node with 30S of "silence"
        looping(sf_info.samplerate / FRAME_LENGTH * 30) <<= vad(std::ref(*vad_state)) <<= adjust_frame_length(FRAME_LENGTH)
        <<= sample_static_cast<int16_t>() <<= 0;

        // storing vad detection in wav file
        looping(sf_info.frames / FRAME_LENGTH) <<= sndfile::write("output/fvad_out2.wav", sf_info.samplerate / FRAME_LENGTH)
        <<= sample_static_cast<int16_t>() <<= vad(std::ref(*vad_state)) <<= sndfile::read(file_handle, INT16_T, FRAME_LENGTH);

        fvad_free(vad_state);
        ///@[fvad usage example2]
    }
}
