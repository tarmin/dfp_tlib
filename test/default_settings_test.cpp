/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/default_settings.hpp>


// user-defined settings shall be defined before including any dfptl header file except default_settings.hpp
struct my_settings : public dfp::predefined_default_settings
{ typedef int sample_type; };

DFPTL_SET_DEFAULT_SETTINGS(my_settings);


#include <dfptl/core/core.hpp>
#include <dfptl/alu/alu.hpp>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert(x, "")


BOOST_AUTO_TEST_CASE(sample_type_test)
{
    using namespace dfp;

    auto processing_branch = alu::counter();

    STATIC_CHECK((
        std::is_same<std::remove_const_t<output_sample_t<decltype(processing_branch)>>, my_settings::sample_type>::value
    ));
}
