/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <iostream>

#include <dfptl/core/core.hpp>
#include <dfptl/routing/const_frame.hpp> //TODO: investigate how to get rid of this include
#include <dfptl/routing/to_single_sample.hpp> //TODO: investigate how to get rid of this include
#include <dfptl/routing/mutable_frame.hpp> //TODO: investigate how to get rid of this include
#include <dfptl/routing/adjust_frame_length.hpp> //TODO: investigate how to get rid of this include
#include <dfptl/alu/counter.hpp>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert((x), "")


BOOST_AUTO_TEST_SUITE(CoreCellTestSuite)


using namespace dfp;


BOOST_AUTO_TEST_CASE(from_test)
{
    struct fb_source : dfp::source<initial_context, dfp::static_stream_specifier<long, 3>, fb_source>
    {
        std::array<long, 3> acquire_frame()
        { return std::array<long, 3>{666}; }
    };
    {
        ///@[core::from usage example]
        using namespace dfp;

        int result = -1;
        int cnt = 0;
        auto minus = [](int in0, int in1){ return in0-in1; };
        auto counter = [&cnt](){ return cnt += 1; };
        auto release = apply<run_once>([&result](int in){ result = in; });
        struct : custom_tag {} const IN;

        std::cout << std::endl << std::endl << "\"from\" source test" << std::endl;

        auto processing =
            release <<= [](int in){ std::cout << in << ";"; }
            <<= minus   += from(IN)
                        |= [](int in){ std::cout << in << ":"; } <<= unsafe_probe(IN) <<= counter
        ;

        auto runnable = make_runnable(processing);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        std::cout << std::endl;
        //shall display: 1:0;2:0;3:0;
        ///@[core::from usage example]

        std::cout << std::endl;
        {
            // frame-based source providing frame as const lvalue ref
            using namespace dfp;

            struct : custom_tag {} const IN;

            frame_releasing() <<= unsafe_probe(IN) <<= fb_source{};
        }
    }
    {
        using namespace dfp;

        int result = -1;
        int cnt = 0;
        auto minus = [](int in0, int in1){ return in0-in1; };
        auto counter = [&cnt](){ return cnt += 1; };
        auto add =  [](int a, int b, int c){ return a+b+c; };
        auto release = apply<run_once>([&result](int in){ result = in; });
        struct : custom_tag {} const IN;

        std::cout << std::endl << std::endl << "nested \"from\" source complicated test" << std::endl;

        auto upstream =     unsafe_probe(IN) <<= counter
                        |=  0
        ;
        auto processing =
            release <<= [](int in){ std::cout << in << ";"; }
            <<= minus   += from(IN)
                        |= [](int in){ std::cout << in << ":"; } <<= add    += const_sample(0)
                                                                            |= upstream
        ;

        auto runnable = make_runnable(processing);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        runnable.run();
        BOOST_CHECK_EQUAL(result, 0);
        std::cout << std::endl;
        //shall display: 1:0;2:0;3:0;

        std::cout << std::endl;
        {
            // frame-based source providing frame as const lvalue ref
            using namespace dfp;

            struct : custom_tag {} const IN;

            frame_releasing() <<= unsafe_probe(IN) <<= fb_source{};
        }
    }
}


BOOST_AUTO_TEST_CASE(sample_from_test)
{
    using namespace dfp::routing;

    {
        ///@['core::sample_from' example usage given values]
        using namespace dfp;

        std::cout << "'sample_from' example usage given values" << std::endl;
        std::cout << "---" << std::endl;

        auto running_tree =
            sample_releasing() <<= [](int in0, int in1){ return in0*in1; }  += const_sample(1, 2, 3)
                                                                            |= sample_from(1, 2, 3)
        ;

        std::cout << running_tree.get_result() << ", " << running_tree.run() << ", " << running_tree.run() << std::endl;
        // expected result: 1, 4, 9

        ///@['core::sample_from' example usage given values]
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(1, running_tree.run());
        BOOST_CHECK_EQUAL(4, running_tree.run());
        BOOST_CHECK_EQUAL(9, running_tree.run());
    }
    {
        ///@[example of implicit conversion from value to 'core::sample_from']
        using namespace dfp;

        std::cout << "example of implicit conversion from value to 'core::sample_from'" << std::endl;
        std::cout << "---" << std::endl;

        auto running_tree =
            sample_releasing() <<= [](int in0, int in1){ return in0==in1; } += sample_from(42)
                                                                            |= 42
        ;

        std::cout << running_tree.get_result() << std::endl;
        // expected result: 1

        ///@[example of implicit conversion from value to 'core::sample_from']
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(true, running_tree.run());
    }
    {
        ///@['core::sample_from' example usage given a value reference]
        using namespace dfp;

        std::cout << "'sample_from' example usage given a value reference" << std::endl;
        std::cout << "---" << std::endl;

        int value = 1;

        auto running_tree =
            sample_releasing() <<= [](int in0, int in1){ return in0*in1; }  += mutable_sample(value)
                                                                            |= sample_from(std::cref(value))
        ;

        std::cout << running_tree.get_result() << ", ";
        value = 2;
        std::cout << running_tree.run() << ", ";
        value = 3;
        std::cout << running_tree.run() << std::endl;
        // expected result: 1, 4, 9

        ///@['core::sample_from' example usage given a value reference]
        std::cout << "---" << std::endl;

        value = 1;
        BOOST_CHECK_EQUAL(1, running_tree.run());
        value = 2;
        BOOST_CHECK_EQUAL(4, running_tree.run());
        value = 3;
        BOOST_CHECK_EQUAL(9, running_tree.run());
    }
    {
        ///@[example of implicit conversion from reference to 'core::sample_from']
        using namespace dfp;

        std::cout << "example of implicit conversion from reference to 'core::sample_from'" << std::endl;
        std::cout << "---" << std::endl;

        int x = 0;

        auto running_tree =
            sample_releasing()
            <<= [](int in0, int in1){ return in0==in1; }
                += [](int in){ return in*in; } <<= std::cref(x)
                |= [](int in0, int in1){ return in0*in1; }
                    += std::cref(x)
                    |= sample_from(std::cref(x))
        ;

        x = 10;

        std::cout << running_tree.get_result() << ", " << running_tree.run() << std::endl;
        // expected result: 0, 100

        ///@[example of implicit conversion from reference to 'core::sample_from']
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(true, running_tree.run());
    }
    {
        ///@['core::sample_from' example usage given a custom_tag]
        using namespace dfp;

        std::cout << "'sample_from' example usage given a custom_tag" << std::endl;
        std::cout << "---" << std::endl;

        struct : custom_tag {} VALUE;

        auto running_tree =
            sample_releasing() <<= [](int in0, int in1){ return in0*in1; }  += sample_from(VALUE)
                                                                            |= probe(VALUE) <<= const_sample(1, 2, 3)
        ;

        std::cout << running_tree.get_result() << ", " << running_tree.run() << ", " << running_tree.run() << std::endl;
        // expected result: 1, 4, 9

        ///@['core::sample_from' example usage given a custom_tag]
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(1, running_tree.run());
        BOOST_CHECK_EQUAL(4, running_tree.run());
        BOOST_CHECK_EQUAL(9, running_tree.run());
    }
    {
        ///@[example of implicit conversion from custom_tag to 'core::sample_from']
        using namespace dfp;

        std::cout << "example of implicit conversion from custom_tag to 'core::sample_from'" << std::endl;
        std::cout << "---" << std::endl;

        struct : custom_tag {} VALUE;

        auto running_tree =
            sample_releasing() <<= [](int in0, int in1){ return in0==in1; } += VALUE
                                                                            |= probe(VALUE) <<= const_sample(42)
        ;

        std::cout << running_tree.get_result() << std::endl;
        // expected result: 1

        ///@[example of implicit conversion from custom_tag to 'core::sample_from']
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(true, running_tree.run());
    }
}


BOOST_AUTO_TEST_CASE(frame_releaser_test)
{
    { // check type traits
        using namespace dfp;
       using namespace dfp::core::detail;

        STATIC_CHECK((is_sink<decltype(frame_releaser() <<= 0)>{}));
    }
    {
        std::cout << "frame_releaser usage example" << std::endl;
        ///@[frame_releaser usage example]
        using namespace dfp;

        int arr[] = { 3, 2, 1 };
        auto processing_tree = frame_releaser() <<= routing::mutable_frame(arr);

        for (auto i : processing_tree.run())
            std::cout << i << " ";
        arr[1] = 0; arr[2] = -1;
        for (auto i : processing_tree.run())
            std::cout << i << " ";
        std::cout << std::endl;
        // expected output: 3 2 1 3 0 -1
        ///@[frame_releaser usage example]
        int j = 0;
        for(auto i : processing_tree.run())
        {
            BOOST_CHECK_EQUAL(i, arr[j++]);
        }   

        std::cout << "frame_releasing usage example" << std::endl;
        ///@[frame_releasing usage example]
        using namespace dfp;
        using namespace dfp::routing;

        auto frame_wrapper =
            frame_releasing() <<= adjust_frame_length(10) <<= [](int const& i0){ return i0*i0; } <<= alu::counter(INT32_T);

        for(auto i : frame_wrapper.get_result())
            std::cout << i << " ";
        std::cout << std::endl;
        // expected output: 0 1 2 4 9 16 25 36 49 64 81
        ///@[frame_releasing usage example]
        j = 0;
        for(auto i : frame_wrapper.get_result())
        {
            BOOST_CHECK_EQUAL(i, j*j);
            j++;
        }   
    }
    //TODO: add test for constant flow input
    { //TODO: check constexpr support
    }
}


BOOST_AUTO_TEST_CASE(sample_releaser_test)
{
    using namespace dfp;

    { // check type traits
        using namespace dfp::core::detail;

        STATIC_CHECK((is_sink<decltype(sample_releaser() <<= 0)>{}));
    }
    #if ENABLE_FAILURE_TEST
    { // check failure
        //will fail because input is not a atomic stream
        auto x = sample_releasing() <<= routing::const_frame(std::experimental::make_array(0, 1));
        std::cout << x;
    }
    #endif
    {
        std::cout << "sample_releaser usage example" << std::endl;
        ///@[sample_releaser usage example]
        using namespace dfp;

        int arr[] = { 3, 2, 1 };
        auto processing_tree = sample_releaser() <<= routing::to_single_sample() <<= routing::mutable_frame(arr);

        std::cout
        << processing_tree.run() << " " << processing_tree.run() << " "
        << processing_tree.run() << " " << processing_tree.run() << " ";
        arr[1] = 0; arr[2] = -1;
        std::cout << processing_tree.run() << " " << processing_tree.run() << std::endl;
        // expected output: 3 2 1 3 0 -1
        ///@[sample_releaser usage example]

        BOOST_CHECK_EQUAL(processing_tree.run(), 3);
        BOOST_CHECK_EQUAL(processing_tree.run(), 0);
        BOOST_CHECK_EQUAL(processing_tree.run(), -1);

        std::cout << "sample_releasing usage example" << std::endl;
        ///@[sample_releasing usage example]
        using namespace dfp;

        struct A
        {
            static int my_square(int const& in)
            { return (sample_releasing() <<= apply([](int const& i0){ return i0*i0; }) <<= in).get_result(); }
        };

        std::cout << A::my_square(2) << std::endl;
        // expected output: 4
        ///@[sample_releasing usage example]
        BOOST_CHECK_EQUAL(A::my_square(3), 9);
    }
    { //TODO: check constexpr support
#if 0
        using namespace dfp;
        using namespace std::experimental;

        constexpr auto processing_tree = sample_releaser() <<= 0;

        std::cout << processing_tree.run() << " " << processing_tree.run() << " " << processing_tree.run() << " ";
        std::cout << processing_tree.run() << " " << processing_tree.run() << std::endl;
#endif
    }
    { // sample_releasing as iterator
        std::cout << "'sample_releasing' usage example as iterator" << std::endl;
        ///@['sample_releasing' usage example as iterator]
        using namespace dfp;

        auto stream_iterator = sample_releasing() <<= apply([](int const& in){ return in*in; }) <<= alu::counter();
        std::cout << *stream_iterator << " " << *++stream_iterator << " "  << *++stream_iterator  << " " << *++stream_iterator << std::endl;
        // expected output : 0 1 4 9
        ///@['sample_releasing' usage example as iterator]

        BOOST_CHECK_EQUAL(*++stream_iterator, 16);
        BOOST_CHECK_EQUAL(*++stream_iterator, 25);
        BOOST_CHECK_EQUAL(*++stream_iterator, 36);
        BOOST_CHECK_EQUAL(*++stream_iterator, 49);
    }
}


BOOST_AUTO_TEST_SUITE_END()
