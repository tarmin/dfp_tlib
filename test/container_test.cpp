/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/container/container.hpp>

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <stl/experimental/array.hpp>


#define STATIC_CHECK(x) static_assert((x), "")
#define ENABLE_FAILURE_TEST 0


BOOST_AUTO_TEST_SUITE(ContainerTestSuite)


using namespace dfp;


BOOST_AUTO_TEST_CASE(iterate_test)
{
    using namespace std::experimental;

    #if ENABLE_FAILURE_TEST
    {
        // failure because of wrong input count
        routing::looping(1) <<= container::iterate();
        // failure because of wrong input count
        routing::looping(1) <<= container::iterate()  += make_array(1)
                                                      |= 1;
        // failure because of invalid input stream type
        routing::looping(1) <<= container::iterate() <<= routing::const_frame(make_array(1));
    }
    #endif // ENABLE_FAILURE_TEST

    ///@[Iterate over container]
    auto const expectation = std::experimental::make_array(1, 2, 3, 1, 2);
    auto it = expectation.cbegin();
    auto check = [&it](int const& in){ BOOST_CHECK_EQUAL(*it++, in); return in; };
    auto display = [](int const& in){ std::cout << in << ", "; return in; };

    routing::looping(5) <<= check <<= display <<= container::iterate() <<= std::experimental::make_array(1, 2, 3);
//TODO: rename into tour
//    routing::looping(5) <<= check <<= display <<= tour() <<= std::experimental::make_array(1, 2, 3);
//    routing::observer_for(std::experimental::make_array(1, 2, 3), 2) <<= check <<= display <<= element(INT);
//    routing::drainer() <<= observe_for(std::experimental::make_array(1, 2, 3), 3) <<= check <<= display <<= element(INT);
    // expected output:
    //1, 2, 3, 1, 2,
    ///@[Iterate over container]
}


BOOST_AUTO_TEST_SUITE_END()
