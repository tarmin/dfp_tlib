/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/kissfft/fft.hpp>
#include <dfptl/routing/to_single_sample.hpp> //TODO: rename into to_atomic_stream()
#include <dfptl/routing/adjust_frame_length.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/math/sin.hpp>
#include <dfptl/math/abs.hpp>
#include <dfptl/math/real.hpp>
#include <dfptl/math/complex.hpp>
#include <dfptl/windowing/hann.hpp>
#include <dfptl/io/write_text_chart.hpp>

#include <boost/test/unit_test.hpp>

#include <cmath>


#define STATIC_CHECK(x) static_assert(x, "")


BOOST_AUTO_TEST_CASE(complex_fft_test)
{
    ///@[fft usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::math;
    using namespace dfp::routing;
    using namespace dfp::kissfft;
    using namespace dfp::windowing;

    constexpr auto FFT_LENGTH = 256_c;
    constexpr auto SAMPLING_RATE = 16000_c; // sampling rate is 16kHz

    constexpr struct : custom_tag {} CLOCK;

    looping(FFT_LENGTH) <<= io::cout_text_chart({-16, 64, 80}) <<= abs() <<= to_single_sample() <<= fft(FFT_LENGTH)
    <<= hann(FFT_LENGTH) <<= complex()  += sin() <<= multiply(2*M_PI*600)   <<= from(CLOCK)
                                        |= sin() <<= multiply(2*M_PI*7000)  <<= probe(CLOCK) <<= counter(1.0/SAMPLING_RATE)
    ;
    ///@[fft usage example]
    //TODO:
    // ifft
    // 1. rename to_single_sample() to to_atomic_stream()
    // -> release
    // 2. implicit cast to atomic_stream (and back)
    // -> release
    // 3. in-place processing support ?
    {  // no implicit frame_length conversion
        constexpr struct : custom_tag {} CLOCK;

        looping(FFT_LENGTH) <<= abs() <<= to_single_sample() <<= fft(FFT_LENGTH) <<= adjust_frame_length(FFT_LENGTH)
        <<= hann(FFT_LENGTH) <<= complex()  += sin() <<= multiply(2*M_PI*600)   <<= from(CLOCK)
                                            |= sin() <<= multiply(2*M_PI*7000)  <<= probe(CLOCK) <<= counter(1.0/SAMPLING_RATE)
        ;
    }
}


BOOST_AUTO_TEST_CASE(complex_ifft_test)
{
    std::cout << std::endl << std::endl;
    ///@[ifft usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::math;
    using namespace dfp::routing;
    using namespace dfp::kissfft;
    using namespace dfp::windowing;

    constexpr auto FFT_LENGTH = 256_c;
    constexpr auto SAMPLING_RATE = 16000_c; // sampling rate is 16kHz

    std::cout << "complex_ifft_test" << std::endl << std::endl;

    looping(FFT_LENGTH) <<= io::cout_text_chart({-129, 129, 80}) <<= real() <<= to_single_sample() <<= ifft(FFT_LENGTH/2_c) <<= to_single_sample() <<= fft(FFT_LENGTH)
    <<= hann(FFT_LENGTH) <<= complex()  += 0.0
                                        |= sin() <<= multiply(2*M_PI*220) <<= counter(1.0/SAMPLING_RATE)
    ;
    ///@[ifft usage example]
}


BOOST_AUTO_TEST_CASE(real_fft_test)
{
}

BOOST_AUTO_TEST_CASE(real_ifft_test)
{
}
