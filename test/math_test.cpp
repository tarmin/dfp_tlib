/*
* Copyright (C) 2018 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <iostream>

#include <dfptl/core/core.hpp>
#include <dfptl/math/math.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>
#include <dfptl/routing/from_iterator.hpp>
#include <dfptl/alu/subtract.hpp>
#include <dfptl/alu/add.hpp>

#include <map>
#include <iomanip>


#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert((x), "")


BOOST_AUTO_TEST_CASE(sin_test)
{
    ///@[sin usage example]
    std::cout << "sin usage example" << std::endl;
    std::cout << "---" << std::endl;

    using namespace dfp;
    using namespace dfp::math;

    auto tree {
        sample_releasing() <<= sin() <<= 0.1f
    };

    std::cout << *tree << std::endl;
    //expected result: 0.0998334
    ///@[sin usage example]

    BOOST_CHECK_EQUAL(*tree, std::sin(0.1f));
    BOOST_CHECK_EQUAL(*++tree, std::sin(0.1f));
    std::cout << std::endl;
}


BOOST_AUTO_TEST_CASE(cos_test)
{
    {
        ///@[cos usage example]
        std::cout << "cos usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::math;

        auto tree {
            sample_releasing() <<= cos() <<= const_sample( 0.1, 0.0, M_PI/2)
        };

        std::cout << *tree << ", " << *++tree << ", " << *++tree << std::endl;
        //expected result: 0.995004, 1, 6.12323e-17
        ///@[cos usage example]

        BOOST_CHECK_EQUAL(*++tree, std::cos(0.1));
        BOOST_CHECK_EQUAL(*++tree, std::cos(0.0));
        BOOST_CHECK_EQUAL(*++tree, std::cos(M_PI/2));
        std::cout << std::endl;
    }
    {
        using namespace dfp;
        using namespace dfp::math;

        auto tree = make_runnable(
            sample_releaser() <<= cos() <<= true
        );

        BOOST_CHECK_EQUAL(tree.run(), std::cos(true));
        STATIC_CHECK((std::is_same<std::remove_cvref_t<decltype(tree.run())>, double>{}));
    }
}


BOOST_AUTO_TEST_CASE(atan2_test)
{
    ///@[atan2 usage example]
    using namespace dfp;
    using namespace dfp::math;

    {
        auto result =
            sample_releasing() <<= atan2()  += const_sample(0.0, 1.0, 0.0, -1.0)
                                            |= const_sample(1.0, 0.0, -1.0, 0.0)
        ;

        BOOST_CHECK_EQUAL(*result, M_PI/2);
        BOOST_CHECK_EQUAL(*++result, 0);
        BOOST_CHECK_EQUAL(*++result, -M_PI/2);
        BOOST_CHECK_EQUAL(*++result, M_PI);
    }
    ///@[atan2 usage example]
}


BOOST_AUTO_TEST_CASE(random_test)
{
    {
        using namespace dfp;
        using namespace dfp::math;

        auto tree {
            /* acc() <<= alu::logical_not() <<= alu::greater_equal(xx) <<= routing::get_frame_length() <<= hoffman::encode() <<= */
            frame_releasing() <<= routing::adjust_frame_length(size_t_const<1024>{}) <<= random<>()
        };

        auto const& frame = tree.get_result();
        std::map<typename decltype(tree)::value_type::value_type, int> hist;

        for (std::size_t i = 0 ; i < frame.size() ; i++)
            ++hist[std::floor(frame[i]*10)];

        for (auto p : hist) {
            std::cout << std::fixed << std::setprecision(1) << std::setw(2)
                      << p.first << ' ' << std::string(p.second/5, '*') << '\n';
        }
    }
    {
        ///@[random source usage example]
        std::cout << "random source usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::math;

        // creates a processing tree simply producing some random number with the std::knuth_b generator
        auto tree {
            sample_releasing() <<= random<std::knuth_b>()
        };

        // creates container for histogram of the random number distribution 
        std::map<typename decltype(tree)::value_type, int> hist;
        for (int i = 0 ; i < 1024 ; i++) {
            ++hist[std::floor(*tree * 10)];
            ++tree;
        }
        // displays histogram on std::cout
        for (auto p : hist) {
            std::cout << std::fixed << std::setprecision(1) << std::setw(2)
                      << p.first << ' ' << std::string(p.second/5, '*') << '\n';
        }
        ///@[random source usage example]
        std::cout  << std::endl;
    }
    {
        using namespace dfp;
        using namespace dfp::math;

        auto tree =
            sample_releasing() <<= random(uniform_random_distribution_t<short>(0, 3))
        ;

        std::map<short, int> hist;
        for (int i = 0 ; i < 1024 ; i++) {
            ++hist[tree.get_result()];
            tree.run();
        }
        for (auto p : hist) {
            std::cout << std::fixed << std::setprecision(1) << std::setw(2)
                      << p.first << ' ' << std::string(p.second/5, '*') << '\n';
        }
    }
    {
        using namespace dfp;
        using namespace dfp::math;

        auto tree =
            sample_releasing() <<= random<std::ranlux24>(std::ranlux24{})
        ;

        std::map<bool, int> hist;
        for (int i = 0 ; i < 256 ; i++) {
            ++hist[tree.get_result()];
            tree.run();
        }
        for (auto p : hist) {
            std::cout << std::fixed << std::setprecision(1) << std::setw(2)
                      << p.first << ' ' << std::string(p.second/5, '*') << '\n';
        }
    }
    {
        using namespace dfp;
        using namespace dfp::math;

        auto tree =
            sample_releasing() <<= random(1, uniform_random_distribution_t<bool>())
        ;

        std::map<bool, int> hist;
        for (int i = 0 ; i < 256 ; i++) {
            ++hist[tree.get_result()];
            tree.run();
        }
        for (auto p : hist) {
            std::cout << std::fixed << std::setprecision(1) << std::setw(2)
                      << p.first << ' ' << std::string(p.second/5, '*') << '\n';
        }
    }
}


BOOST_AUTO_TEST_CASE(log_test)
{
    ///@[log usage example]
    using namespace dfp;
    using namespace dfp::math;

    auto tree =
        sample_releasing() <<= log() <<= const_sample(1.0, M_E, M_E*M_E)
    ;

    BOOST_CHECK_EQUAL(tree.get_result(), 0);
    BOOST_CHECK_EQUAL(tree.run(), 1);
    BOOST_CHECK_EQUAL(tree.run(), 2);
    ///@[log usage example]
}


BOOST_AUTO_TEST_CASE(complex_test)
{
    ///@[complex usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::math;
    using namespace dfp::routing;

    auto complex_double =
        sample_releasing() <<= complex()    += const_sample(0.0, 2.0, 4.0)
                                            |= const_sample(1.0, 3.0, 5.0)
    ;

    BOOST_CHECK_EQUAL(*complex_double, std::complex<double>(1, 0));

    auto complex_float =
        sample_releasing() <<= add()    += complex<float>() <<= from_iterator(std::ref(complex_double))
                                        |= std::complex<float>(0.1, 0.2)
    ;

    BOOST_CHECK_EQUAL(*complex_double, std::complex<double>(3, 2));
    BOOST_CHECK_EQUAL(*complex_float, std::complex<float>(1.1, 0.2));
    BOOST_CHECK_EQUAL(*++complex_float, std::complex<float>(3.1, 2.2));
    BOOST_CHECK_EQUAL(*complex_double, std::complex<double>(5, 4));
    BOOST_CHECK_EQUAL(*++complex_float, std::complex<float>(5.1, 4.2));
    BOOST_CHECK_EQUAL(*complex_double, std::complex<double>(1, 0));

#if DFP_CPP14_SUPPORT
    using namespace std::literals::complex_literals;

    auto cplx_double =
        sample_releasing() <<= const_sample(1.0+0.0i, 3.0+2.0i, 5.0+4.0i)
    ;

    BOOST_CHECK_EQUAL(*cplx_double,     1.0 +0.0i);

    auto cplx_float =
        sample_releasing() <<= add()    += complex<float>() <<= from_iterator(std::ref(cplx_double))
                                        |= 0.1f+0.2if
    ;

    BOOST_CHECK_EQUAL(*cplx_float,    1.1f+0.2if);
    BOOST_CHECK_EQUAL(*cplx_double,     3.0 +2.0i);
    BOOST_CHECK_EQUAL(*++cplx_float,    3.1f+2.2if);
    BOOST_CHECK_EQUAL(*cplx_double,     5.0 +4.0i);
    BOOST_CHECK_EQUAL(*++cplx_float,    5.1f+4.2if);
#endif
    ///@[complex usage example]
    {
        using namespace dfp;
        using namespace dfp::math;
        using namespace dfp::routing;

        auto cplx = sample_releasing() <<= cos() <<= complex()  += 2.0
                                                                |= const_sample(1.0)
        ;
        BOOST_CHECK_EQUAL(*cplx, std::cos(std::complex<double>(1.0, 2.0)));
    }
}


BOOST_AUTO_TEST_CASE(abs_test)
{
    ///@[abs usage example]
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::math;
    using namespace dfp::shortname;

#if DFP_CPP14_SUPPORT
    using namespace std::literals::complex_literals;

    auto tree =
        sample_releasing() <<= subtract()   += abs() <<= const_s(4.0 + 3.0i, 0.0 + 4.0i)
                                            |= add()    += abs() <<= const_s(2.0, -2.0)
                                                        |= abs() <<= const_s(-3, 2)
    ;
#else
    auto tree =
        sample_releasing() <<= subtract()   += abs() <<= const_s(std::complex<double>(4, 3), std::complex<double>(0, 4))
                                            |= add()    += abs() <<= const_s(2.0, -2.0)
                                                        |= abs() <<= const_s(-3, 2)
    ;
#endif

    BOOST_CHECK_EQUAL(*tree, 0);
    BOOST_CHECK_EQUAL(*++tree, 0);
    ///@[abs usage example]
#if 0
    // introduce =, the "assignement" operator specifically to nop() operator
    #define z nop()
    using namespace std::literals::complex_literals;

    auto tree =
        sample_releasing() <<= subtract()   += abs() <<=z={2.0 + 2.0i, 0.0 + 4.0i}
                                            |= add()    += abs() <<=z={ a, -2.0 }
                                                        |= abs() <<=z={ -1, 2 }
    ;
#endif
#if (__GNUC__ >= 11) // issue with integral constant with older gcc version
    {
        auto processing_branch =
            sample_releasing() <<= subtract()   += abs() <<= -1_c
                                                |= abs() <<= 1_c
        ;

        BOOST_CHECK_EQUAL(*processing_branch, 0);
    }
#endif
}


BOOST_AUTO_TEST_CASE(real_test)
{
    ///@[real usage example]
    using namespace dfp;
    using namespace dfp::math;
    using namespace dfp::shortname;

    {
        auto tree =
            sample_releasing() <<= real() <<= complex() += const_s(0.0, 2.0, 4.0)
                                                        |= const_s(1.0, 3.0, 5.0)
        ;

        BOOST_CHECK_EQUAL(*tree, 1.0);
        BOOST_CHECK_EQUAL(*++tree, 3.0);
        BOOST_CHECK_EQUAL(*++tree, 5.0);
    }
    ///@[real usage example]
    {
        auto tree =
            sample_releaser() <<= real() <<= true
        ;

        BOOST_CHECK_EQUAL(tree.run(), std::real(true));
        STATIC_CHECK((std::is_same<std::remove_cvref_t<decltype(tree.run())>, double>{}));
    }
}


BOOST_AUTO_TEST_CASE(imag_test)
{
    ///@[imag usage example]
    using namespace dfp;
    using namespace dfp::math;
    using namespace dfp::shortname;

    {
        auto tree =
            sample_releasing() <<= imag() <<= complex() += const_s(0.0, 2.0, 4.0)
                                                        |= const_s(1.0, 3.0, 5.0)
        ;

        BOOST_CHECK_EQUAL(*tree, 0.0);
        BOOST_CHECK_EQUAL(*++tree, 2.0);
        BOOST_CHECK_EQUAL(*++tree, 4.0);
    }
    ///@[imag usage example]
    {
        auto tree =
            sample_releasing() <<= imag() <<= true;

        STATIC_CHECK((std::is_same<input_sample_t<typename std::remove_reference_t<decltype(tree)>::terminal_cell_type>, double>{}));
        BOOST_CHECK_EQUAL(tree.get_result(), std::imag(true));
    }
}


BOOST_AUTO_TEST_CASE(arg_test)
{
    ///@[arg usage example]
    using namespace dfp;
    using namespace dfp::math;
    using namespace dfp::shortname;

    {
        auto processing_branch =
            sample_releasing() <<= arg() <<= const_s(std::complex<double>(cos(M_PI/2), sin(M_PI/2)), std::complex<double>(2.0*cos(M_PI/4), 2.0*sin(M_PI/4)))
        ;

        BOOST_CHECK_EQUAL(*processing_branch, M_PI/2);
        BOOST_CHECK_EQUAL(*++processing_branch, M_PI/4);
    }
    ///@[arg usage example]
}


BOOST_AUTO_TEST_CASE(max_test)
{
    ///@[max usage example]
    using namespace dfp;
    using namespace dfp::shortname;
    using namespace dfp::math;

    auto tree =
        sample_releasing() <<= max()    += const_s(1, 2, -3, 3)
                                        |= const_s(0, 1, 3, 4 )
    ;

    BOOST_CHECK_EQUAL(*tree, 1);
    BOOST_CHECK_EQUAL(*++tree, 2);
    BOOST_CHECK_EQUAL(*++tree, 3);
    BOOST_CHECK_EQUAL(*++tree, 4);
    ///@[max usage example]
}


BOOST_AUTO_TEST_CASE(cummax_test)
{
    ///@[cummax usage example]
    using namespace dfp;
    using namespace dfp::math;

    auto tree =
        sample_releasing() <<= cumulative_max() <<= const_sample(1, 2, -3, 3)
    ;

    BOOST_CHECK_EQUAL(*tree, 1);
    BOOST_CHECK_EQUAL(*++tree, 2);
    BOOST_CHECK_EQUAL(*++tree, 2);
    BOOST_CHECK_EQUAL(*++tree, 3);
    ///@[cummax usage example]
    {
        using namespace dfp::math::shortname;

        auto tree =
            sample_releasing() <<= [](int x){ std::cout << x; } <<= cummax() <<= [](int x){ std::cout << std::endl << x << "//"; } <<= const_sample(1, 2, -3, 3)
        ;

        BOOST_CHECK_EQUAL(*tree, 1);
        BOOST_CHECK_EQUAL(*++tree, 2);
        BOOST_CHECK_EQUAL(*++tree, 2);
        BOOST_CHECK_EQUAL(*++tree, 3);
    }
}
