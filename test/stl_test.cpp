/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <stl/span.hpp>
#include <stl/tuple.hpp>
#include <stl/utility.hpp>
#include <stl/optional.hpp>
#include <stl/functional.hpp>
#include <stl/type_traits.hpp>
#include <stl/experimental/type_traits.hpp>

#include <boost/test/unit_test.hpp>

#include <tuple>


BOOST_AUTO_TEST_SUITE(StlTestSuite)


#define STATIC_CHECK(x) static_assert(x, "")


BOOST_AUTO_TEST_CASE(type_traits_test)
{
    using namespace std;

    struct foo { long operator()(int) { return 0L; } };

    STATIC_CHECK((is_same<add_const_t<int>,                  typename add_const<int>::type>::value));
    STATIC_CHECK((is_same<add_cv_t<int>,                     typename add_cv<int>::type>::value));
    STATIC_CHECK((is_same<add_lvalue_reference_t<int>,       typename add_lvalue_reference<int>::type>::value));
    STATIC_CHECK((is_same<add_rvalue_reference_t<int>,       typename add_rvalue_reference<int>::type>::value));
    STATIC_CHECK((is_same<add_volatile_t<int>,               typename add_volatile<int>::type>::value));
    STATIC_CHECK((is_same<conditional_t<true, int, long>,    typename conditional<true, int, long>::type>::value));
    STATIC_CHECK((is_same<make_signed_t<unsigned>,           typename make_signed<unsigned>::type>::value));
    STATIC_CHECK((is_same<make_unsigned_t<int>,              typename make_unsigned<int>::type>::value));
    STATIC_CHECK((is_same<remove_const_t<const int>,         typename remove_const<const int>::type>::value));
    STATIC_CHECK((is_same<remove_reference_t<int&>,          typename remove_reference<int&>::type>::value));
    STATIC_CHECK((is_same<result_of_t<foo(int)>,             typename result_of<foo(int)>::type>::value));
    STATIC_CHECK((is_same<bool_constant<false>,              typename integral_constant<bool, false>::type>::value));
    STATIC_CHECK((is_same<invoke_result_t<decltype(&std::srand), unsigned>, void>::value));
}


BOOST_AUTO_TEST_CASE(tuple_test)
{
    using namespace std;
    using tupl = tuple<int, long>;

    STATIC_CHECK((is_same<tuple_element_t<1, tupl>,          typename tuple_element<1, tupl>::type>::value));
}


BOOST_AUTO_TEST_CASE(functional_test)
{
    using namespace upg;

    BOOST_CHECK_EQUAL(multiplies<>()(INT_MAX, 2L),      INT_MAX * 2L);
    BOOST_CHECK_EQUAL(divides<>()(1.2L, 2),             1.2L / 2);
    BOOST_CHECK_EQUAL(modulus<>()(LONG_MAX, INT_MAX),   LONG_MAX % INT_MAX);
    BOOST_CHECK_EQUAL(plus<>()(2.1, 2),                 2.1 + 2);
    BOOST_CHECK_EQUAL(minus<>()(2.1, 2),                2.1 - 2);
    BOOST_CHECK_EQUAL(negate<>()(1.1f),                 -1.1f);
    BOOST_CHECK_EQUAL(bit_and<>()(LONG_MAX, INT_MAX),   LONG_MAX & INT_MAX);
    BOOST_CHECK_EQUAL(bit_or<>()(INT_MAX, LONG_MIN),    INT_MAX | LONG_MIN);
    BOOST_CHECK_EQUAL(bit_xor<>()(INT_MAX, LONG_MIN),   INT_MAX ^ LONG_MIN);
    BOOST_CHECK_EQUAL(logical_not<>()(false),           true);
    BOOST_CHECK_EQUAL(greater<>()(2.1, 2),              2.1 > 2);
    BOOST_CHECK_EQUAL(greater_equal<>()(2.1, 2),        2.1 >= 2);
    BOOST_CHECK_EQUAL(not_equal_to<>()(2, 2),           2 != 2);
    BOOST_CHECK_EQUAL(equal_to<>()(2, 2),               2 == 2);
    BOOST_CHECK_EQUAL(logical_and<>()(true, false),     true && false);
}


struct ressources
{
    template<class T>
    using first_element_t = std::tuple_element_t<0, T>;
};
BOOST_AUTO_TEST_CASE(experimental_type_traits_test)
{
    using namespace std;
    using namespace std::experimental;

    STATIC_CHECK((is_detected<vector, int>::value));
    STATIC_CHECK((is_detected<bit_and, int>::value));
    STATIC_CHECK((is_detected<ressources::first_element_t, tuple<int>>::value));
    STATIC_CHECK((is_same<detected_t<ressources::first_element_t, tuple<int>>, int>::value));
    STATIC_CHECK((!is_detected<ressources::first_element_t, int>::value));
    STATIC_CHECK((is_same<detected_t<ressources::first_element_t, int>, nonesuch>::value));
    STATIC_CHECK((is_same<typename detected_or<bool, ressources::first_element_t, int>::type, bool>::value));
}


BOOST_AUTO_TEST_CASE(span_test)
{
    using namespace std;

    STATIC_CHECK((tuple_size<tuple<int, char>>::value == 2));
    STATIC_CHECK((tuple_size<upg::span<int const, 3>>::value == 3));
}


BOOST_AUTO_TEST_CASE(optional_test)
{
    using namespace upg;

    optional<int> o1;
    optional<int> o2(2);

    BOOST_CHECK_EQUAL(o1.has_value(), false);
    BOOST_CHECK_EQUAL(o2.has_value(), true);
    BOOST_CHECK_EQUAL(o1.value_or(1), 1);
    BOOST_CHECK_EQUAL(o2.value_or(2), 2);
}


BOOST_AUTO_TEST_SUITE_END()
