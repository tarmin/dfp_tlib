/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/esphome/esphome.hpp>
#include <dfptl/alu/counter.hpp>

#include <string>

#undef F //arduino F macro conflicts with boost/function/function_base.hpp
#include <boost/test/unit_test.hpp>
#define F(string_literal) (FPSTR(PSTR(string_literal))) // restore F macro definition


#define STATIC_CHECK(x) static_assert(x, "")

#define ENABLE_FAILURE_TEST 0

#define VERSION_CODE(major, minor, patch) ((major) << 16 | (minor) << 8 | (patch))


// esphome stubs
namespace esphome
{
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
    EntityBase::EntityBase(std::string name){ (void) name; }
#endif

namespace sensor
{
    Sensor::Sensor() {}
    bool Sensor::has_state() const { return has_state_; }
    float Sensor::get_state() const { return state; }
    std::string Sensor::unique_id() { return ""; }
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
    std::string Sensor::unit_of_measurement() { return {}; }
    int8_t Sensor::accuracy_decimals() { return {}; }
    std::string Sensor::device_class()  { return {}; }
    StateClass Sensor::state_class() { return {}; }
#endif
    void Sensor::publish_state(float state) { this->state = state; has_state_ = true; callback_.call(state); }
    void Sensor::add_on_state_callback(std::function<void(float)> &&callback) { callback_.add(std::move(callback)); }
}

namespace binary_sensor
{
    BinarySensor::BinarySensor() {}
    bool BinarySensor::has_state() const { return has_state_; }
    bool BinarySensor::is_status_binary_sensor() const { return {}; }
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
    std::string BinarySensor::device_class()  { return {}; }
#endif
    void BinarySensor::publish_state(bool state) { this->state = state; has_state_ = true; state_callback_.call(state); }
    void BinarySensor::add_on_state_callback(std::function<void(bool)> &&callback) { state_callback_.add(std::move(callback)); }
}

namespace text_sensor
{
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
    TextSensor::TextSensor() {}
#endif
    bool TextSensor::has_state() { return true; }
    std::string TextSensor::get_state() const { return state; }
    std::string TextSensor::unique_id() { return {}; }
    void TextSensor::publish_state(const std::string& state) { this->state = state; callback_.call(state); }
    void TextSensor::add_on_state_callback(std::function<void(std::string)> callback) { callback_.add(std::move(callback)); }
}

namespace select
{
    void Select::publish_state(const std::string& state) { this->state = state; this->has_state_ = true; state_callback_.call(state, std::stoul(state)); }
    void Select::add_on_state_callback(std::function<void(std::string, std::size_t)>&& callback) { state_callback_.add(std::move(callback)); }
    optional<std::size_t> Select::active_index() const { return std::stoul(state); }
}

namespace climate
{
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    Climate::Climate() {}
#pragma GCC diagnostic pop
#endif
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2024, 4, 1))
    void Climate::publish_state() { this->mode = CLIMATE_MODE_HEAT; state_callback_.call(*this); }
    void Climate::add_on_state_callback(std::function<void(Climate&)>&& callback) { state_callback_.add(std::move(callback)); }
#else
    void Climate::publish_state() { this->mode = CLIMATE_MODE_HEAT; state_callback_.call(); }
    void Climate::add_on_state_callback(std::function<void()>&& callback) { state_callback_.add(std::move(callback)); }
#endif
    ClimateTraits Climate::get_traits() { return ClimateTraits{}; }
}

namespace number
{
    void Number::publish_state(float state) { this->state = state; this->has_state_ = true; state_callback_.call(state); }
    void Number::add_on_state_callback(std::function<void(float)>&& callback) { state_callback_.add(std::move(callback)); }
}

} // namespace esphome


BOOST_AUTO_TEST_CASE(state_test)
{
    {
        ///@[esphome::state usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;

        auto runnable_tree = make_runnable(
            sample_releaser() <<= state(dummy_sensor)
        );

        BOOST_CHECK(!runnable_tree.run().has_value());
        dummy_sensor.publish_state(3.1415926f);
        auto optional_val = runnable_tree.run();
        BOOST_CHECK(optional_val.has_value());
        BOOST_CHECK_EQUAL(*optional_val, 3.1415926f);
        ///@[esphome::state usage example]

        STATIC_CHECK((core::detail::is_upstream<decltype(state(dummy_sensor).build())>{}));
        STATIC_CHECK((core::detail::is_source<decltype(state(dummy_sensor).build())>{}));
    }
    {
        ///@[esphome::state for binary sensor usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::binary_sensor::BinarySensor dummy_sensor;
        dummy_sensor.publish_state(true);

        auto sample_wrapper =
            sample_releasing() <<= [](std::optional<bool> const& input){ std::cout << *input << std::endl; }
            <<= state(dummy_sensor)
        ;
        BOOST_CHECK(sample_wrapper.get_result().has_value());
        BOOST_CHECK_EQUAL(*sample_wrapper.get_result(), true);
        ///@[esphome::state for binary sensor usage example]
    }
    {
        ///@[esphome::state for text sensor usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::text_sensor::TextSensor dummy_sensor;
        dummy_sensor.publish_state("hello");

        auto run_tree {
            sample_releasing() <<= execute([](std::optional<std::string> const& input){ std::cout << *input << std::endl; })
            <<= state(dummy_sensor)
        };
        BOOST_CHECK_EQUAL(*run_tree.get_result(), "hello");
        ///@[esphome::state for text sensor usage example]
    }
    {
        ///@[esphome::state for "select" usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;
        
        struct my_dummy_select : ::esphome::select::Select
        {
            void control(const std::string& value) override {}
        } dummy_sensor;
        dummy_sensor.publish_state("5");

        auto run_tree {
            sample_releasing() <<= [](std::optional<std::size_t> const& input){ std::cout << *input << std::endl; }
            <<= state(dummy_sensor)
        };
        BOOST_CHECK(run_tree.get_result().has_value());
        BOOST_CHECK_EQUAL(*run_tree.get_result(), 5);
        ///@[esphome::state for "select" usage example]
    }
    {
        ///@[esphome::state for "climate" usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;
        using namespace ::esphome::climate;
        
        struct my_dummy_climate : ::esphome::climate::Climate
        {
            void control(const ClimateCall &call)  {}
            ClimateTraits traits() { return ClimateTraits{}; }
        } dummy_sensor;
        dummy_sensor.publish_state();

        auto run_tree {
            sample_releasing() <<= [](std::optional<ClimateDeviceRestoreState> const& input){ std::cout << std::endl; } <<= state(dummy_sensor)
        };
        BOOST_CHECK(run_tree.get_result().has_value());
        ///@[esphome::state for "climate" usage example]
    }
    {
        ///@[esphome::state for "number" usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;
        
        struct my_dummy_number : ::esphome::number::Number
        {
            void control(float value) override {}
        } dummy_entity;
        dummy_entity.publish_state(5.0f);

        auto run_tree {
            sample_releasing() <<= [](std::optional<float> const& input){ std::cout << *input << std::endl; } <<= state(dummy_entity)
        };
        BOOST_CHECK(run_tree.get_result().has_value());
        BOOST_CHECK_EQUAL(*run_tree.get_result(), 5.0f);
        ///@[esphome::state for "number" usage example]
    }
}


BOOST_AUTO_TEST_CASE(observer_test)
{
    {
        ///@[esphome::observer_for usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;

        auto tree = make_runnable(
            observer_for(dummy_sensor, 3) <<= [&](std::optional<float> const& input){value = *input; } <<= state(dummy_sensor)
        );

        tree.run();

        // check observed values over published states
        //---
        BOOST_CHECK_EQUAL(value, -1); // no state captured as even if "obs" processing tree is already running nothing has been pusblished
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1); // state published has been captured
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0); // no state update as iteration count has been reached
        ///@[esphome::observer_for usage example]
    }
    {
        ///@[esphome::observer_for usage example with explicit "run"]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;

        auto observer_tree = make_runnable(
            observer_for(dummy_sensor, 3) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_sensor)
        );

        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, -1); // no state captured as "observer_tree" processing tree is not running

        observer_tree.run();
    
        BOOST_CHECK_EQUAL(value, 1); // state published before "run" are captured
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0); // no state update as iteration count has been reached
        ///@[esphome::observer_for usage example with explicit "run"]
    }
    {
        ///@[esphome::observer_for on select usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        struct my_dummy_select : ::esphome::select::Select
        {
            void control(const std::string& value) override {}
        } dummy_sensor;
        std::size_t value = -1;

        dummy_sensor.publish_state("3");

        auto tree = make_runnable(
            observer_for(dummy_sensor, 3) <<= [&](std::optional<std::size_t> const& input){ value = *input; } <<= state(dummy_sensor)
        );

        tree.run();

        BOOST_CHECK_EQUAL(value, 3); // even if published before observer initialization, current state (if exists) is immediately captured when subscribing to esphome entity
        dummy_sensor.publish_state("2");
        BOOST_CHECK_EQUAL(value, 2); 
        dummy_sensor.publish_state("1");
        BOOST_CHECK_EQUAL(value, 1); 
        dummy_sensor.publish_state("0");
        BOOST_CHECK_EQUAL(value, 1); // no state update as iteration count has been reached
        ///@[esphome::observer_for on select usage example]
    }
    {
        ///@[esphome::observer_for on binary_sensor usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::binary_sensor::BinarySensor dummy_sensor;
        bool value = false;

        auto tree_builder = [&](::esphome::binary_sensor::BinarySensor& sensor){ return
            observer_for(sensor, 3) <<= [&](std::optional<bool> const& input){ value = *input; } <<= state(sensor)
        ; };

        runnable_tree<decltype(tree_builder(dummy_sensor))> tree { tree_builder(dummy_sensor) };

        tree.run();

        BOOST_CHECK_EQUAL(value, false);
        dummy_sensor.publish_state(true);
        BOOST_CHECK_EQUAL(value, true);
        dummy_sensor.publish_state(true);
        BOOST_CHECK_EQUAL(value, true);
        dummy_sensor.publish_state(false);
        BOOST_CHECK_EQUAL(value, false);
        dummy_sensor.publish_state(true);
        BOOST_CHECK_EQUAL(value, false);  // no state update as iteration count has been reached
        ///@[esphome::observer_for on binary_sensor usage example]
    }
    {
        ///@[esphome::observer_for on number usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        struct my_dummy_number : ::esphome::number::Number
        {
            void control(float value) override {}
        } dummy_entity;
        float value = -1.0f;

        dummy_entity.publish_state(3);

        auto tree = make_runnable(
            observer_for(dummy_entity, 3) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_entity)
        );

        tree.run();

        BOOST_CHECK_EQUAL(value, 3.0f); // even if published before observer initialization, current state (if exists) is immediately captured when subscribing to esphome entity
        dummy_entity.publish_state(2.0f);
        BOOST_CHECK_EQUAL(value, 2.0f); 
        dummy_entity.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1.0f); 
        dummy_entity.publish_state(0.0f);
        BOOST_CHECK_EQUAL(value, 1.0f); // no state update as iteration count has been reached
        ///@[esphome::observer_for on number usage example]
    }
    { // 2 observers on same sensor
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;
        float valu2 = -1;

        auto obs1 = make_runnable(
            observer_for(dummy_sensor, 3) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_sensor)
        );
        auto obs2 = make_runnable(
            observer_for(dummy_sensor, 5) <<= [&](std::optional<float> const& input){ valu2 = *input; } <<= state(dummy_sensor)
        );


        obs1.run();
        obs2.run();

        BOOST_CHECK_EQUAL(value, -1);
        BOOST_CHECK_EQUAL(valu2, -1);
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1);
        BOOST_CHECK_EQUAL(valu2, 1);
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        BOOST_CHECK_EQUAL(valu2, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, 0);
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -1);
        dummy_sensor.publish_state(-2);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -2);
        dummy_sensor.publish_state(-3);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -2);

        obs1.run();
        obs2.run();

        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1);
        BOOST_CHECK_EQUAL(valu2, 1);
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        BOOST_CHECK_EQUAL(valu2, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, 0);
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -1);
        dummy_sensor.publish_state(-2);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -2);
        dummy_sensor.publish_state(-3);
        BOOST_CHECK_EQUAL(value, 0);
        BOOST_CHECK_EQUAL(valu2, -2);
    }
    {
        ///@[esphome::observer_while usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;
        bool enabled = true;

        auto tree = make_runnable(
            observer_while(dummy_sensor, [&](){ return enabled; }) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_sensor)
        );

        tree.run();

        BOOST_CHECK_EQUAL(value, -1); // no state captured as even if "obs" processing tree is already running nothing has been pusblished
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1); // state published has been captured
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        enabled = false;
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0); // no state update as predicate is now invalid
        ///@[esphome::observer_while usage example]
    }
    {
        ///@[esphome::observer_while usage example with explicit "run"]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;
        bool enabled = true;

        auto observer_tree = make_runnable(
            observer_while(dummy_sensor, [&](){ return enabled; }) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_sensor)
        );

        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, -1); // no state captured as "obs" processing tree is not running

        observer_tree.run();

        BOOST_CHECK_EQUAL(value, 1); // state published before "run" but after initialization are captured
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        enabled = false;
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0); // no state update as predicate is now invalid
        ///@[esphome::observer_while usage example with explicit "run"]
    }
    {
        ///@[esphome::observer_while with variable usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is required to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;
        bool enabled = true;

        auto tree = make_runnable(
            observer_while(dummy_sensor, enabled) <<= [&](std::optional<float> const& input){ value = *input; } <<= state(dummy_sensor)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, -1); // no state captured as even if "obs" processing tree is already running nothing has been pusblished
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1); // state published has been captured
        dummy_sensor.publish_state(3.1415926f);
        BOOST_CHECK_EQUAL(value, 3.1415926f);
        dummy_sensor.publish_state(0);
        BOOST_CHECK_EQUAL(value, 0);
        enabled = false;
        dummy_sensor.publish_state(-1);
        BOOST_CHECK_EQUAL(value, 0); // no state update as predicate is now invalid
        ///@[esphome::observer_while with variable usage example]
    }
}


BOOST_AUTO_TEST_CASE(actual_state_test)
{
#if ENABLE_FAILURE_TEST
    {
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;

        auto run_tree {
            sample_releasing() <<= actual_state(FLOAT)
        }; // fails to compile because sink is not an esphome observer
    }
#endif
    {
        ///@[esphome::actual_state usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;
        bool enabled = true;

        auto tree = make_runnable(
            observer_while(dummy_sensor, enabled) <<= [&](float const& input){ value = input; } <<= actual_state(FLOAT)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, -1);
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1);
        ///@[esphome::actual_state usage example]
    }
    {
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::sensor::Sensor dummy_sensor;
        float value = -1;

        auto tree = make_runnable(
            observer_for(dummy_sensor, 2) <<= [&](float const& input){ value = input; } <<= actual_state(FLOAT)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, -1);
        dummy_sensor.publish_state(1);
        BOOST_CHECK_EQUAL(value, 1);
    }
    {
        ///@[esphome::actual_state for binary sensor usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::binary_sensor::BinarySensor dummy_sensor;
        dummy_sensor.state = true;
        bool value = false;

        auto tree = make_runnable(
            observer_forever(dummy_sensor) <<= [&](bool const& input){ std::cout << (value = input) << std::endl; } <<= actual_state(BOOL)
        );

        tree.run();
        dummy_sensor.publish_state(true);
        BOOST_CHECK_EQUAL(value, true);
        dummy_sensor.publish_state(false);
        BOOST_CHECK_EQUAL(value, false);        
        ///@[esphome::actual_state for binary sensor usage example]
    }
    {
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::binary_sensor::BinarySensor dummy_sensor;
        dummy_sensor.state = true;
        bool value = false;

        auto tree = make_runnable(
            observer_for(dummy_sensor, 2) <<= [&](bool const& input){ std::cout << (value = input) << std::endl; } <<= actual_state(BOOL)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, false);
        dummy_sensor.publish_state(true);
        BOOST_CHECK_EQUAL(value, true);
        dummy_sensor.publish_state(false);
        BOOST_CHECK_EQUAL(value, false);        
    }
    {
        ///@[esphome::observer_forever on text_sensor usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::text_sensor::TextSensor dummy_sensor;
        dummy_sensor.state = "hello";
        std::string value = "";

        auto tree = make_runnable(
            observer_forever(dummy_sensor) <<= [&](std::string const& input){ std::cout << (value = input) << std::endl; } <<= actual_state<std::string>()
        );

        tree.run();
        dummy_sensor.publish_state("goodbye");
        BOOST_CHECK_EQUAL(value, "goodbye");
        ///@[esphome::observer_forever on text_sensor usage example]
    }
    {
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;

        ::esphome::text_sensor::TextSensor dummy_sensor;
        dummy_sensor.state = "hello";
        std::string value = "";

        auto tree = make_runnable(
            observer_for(dummy_sensor, 2) <<= [&](std::string const& input){ std::cout << (value = input) << std::endl; } <<= actual_state(STRING)
        );

        tree.run();
        dummy_sensor.publish_state("goodbye");
        BOOST_CHECK_EQUAL(value, "goodbye");
    }
    {
        ///@[esphome::actual_state for "select" usage example]
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;
        
        struct my_dummy_select : ::esphome::select::Select
        {
            void control(const std::string& value) override {}
        } dummy_sensor;
        dummy_sensor.publish_state("5");
        std::size_t value = -1;

        auto tree = make_runnable(
            observer_forever(dummy_sensor) <<= [&](std::size_t const& input){ value = input; } <<= actual_state(SIZE_T)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, 5);
        dummy_sensor.publish_state("3");
        BOOST_CHECK_EQUAL(value, 3);
        ///@[esphome::actual_state for "select" usage example]
    }
    {
        using namespace dfp;
        namespace esph = dfp::esphome; // alias is recommended to avoid namespace clashing between esphome and dfp
        using namespace esph;
        
        struct my_dummy_select : ::esphome::select::Select
        {
            void control(const std::string& value) override {}
        } dummy_sensor;
        dummy_sensor.publish_state("5");
        std::size_t value = -1;

        auto tree = make_runnable(
            observer_for(dummy_sensor, 2) <<= [&](std::size_t const& input){ value = input; } <<= actual_state(SIZE_T)
        );

        tree.run();
        BOOST_CHECK_EQUAL(value, 5);
        dummy_sensor.publish_state("3");
        BOOST_CHECK_EQUAL(value, 3);
    }
}

