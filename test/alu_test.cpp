/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <chrono>
#include <climits>
#include <iostream>
#include <functional>
 
#include <dfptl/core/core.hpp>
#include <dfptl/alu/alu.hpp>
#include <dfptl/math/max.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/routing/to_single_sample.hpp>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert((x), "")


BOOST_AUTO_TEST_CASE(cell_traits_test)
{
    using namespace dfp;
    using namespace dfp::alu::nodes;

    multiply().preset_inputs(0) <<= 0;
    STATIC_CHECK((detail::is_node<decltype(multiply(0) <<= 0)>{}));
    STATIC_CHECK((output_stream_t<decltype(multiply(0) <<= 0)>::IS_ATOMIC));
}


BOOST_AUTO_TEST_CASE(multiply_test)
{
    using namespace dfp;
    using namespace dfp::alu::nodes;

    {
        auto tree {
            sample_releasing() <<= multiply().preset_inputs(2) <<= 0.1f
        };

        BOOST_CHECK_EQUAL(tree.get_result(), 0.1f*2);
    }
    {
        auto tree {
            sample_releasing() <<= multiply(2) <<= 0.1f
        };

        BOOST_CHECK_EQUAL(tree.get_result(), 0.1f*2);
    }
    { // constexpr support
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST && DFP_CPP14_SUPPORT
        constexpr auto tree {
            sample_releasing() <<= multiply(2) <<= 0.1f
        };
        STATIC_CHECK(tree.get_result() == 0.1f*2);
#endif
    }
    { // integral_const support
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST && DFP_CPP14_SUPPORT
        using namespace dfp::alu::shortname;

        STATIC_CHECK((2 == (sample_releasing() <<= mult(2) <<= 1).get_result()));
        STATIC_CHECK((1*2 == (sample_releasing() <<= mult() += const_sample(2) |= 1).get_result()));
#endif
    }
    {
        auto tree =
            sample_releasing() <<= multiply()   += const_sample(0.1f)
                                                |= const_sample(2)
        ;

        BOOST_CHECK_EQUAL(*tree, 2*0.1f);
    }
    {
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        constexpr auto processing_branch = multiply()   += const_sample(0.1f)
                                                        |= const_sample(2);

        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 2*0.1f));
#endif
    }
}


BOOST_AUTO_TEST_CASE(divide_test)
{
    using namespace dfp;
    using namespace dfp::alu::shortname;

    {
        auto processing_branch = dfp::alu::knots::divide(1.1f) <<= 20;

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 20/1.1f);
    }
    {
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        constexpr auto processing_branch = div(1.1f) <<= 20;

        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 20/1.1f));
#endif
    }
    {
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        constexpr auto processing_branch = div()    += const_sample(1.1f)
                                                    |= const_sample(20);

        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 20/1.1f));
#endif
    }
}


BOOST_AUTO_TEST_CASE(add_test)
{
    auto processing_branch = dfp::alu::add(1.1f) <<= 20;

    BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 20+1.1f);
}


BOOST_AUTO_TEST_CASE(subtract_test)
{
    auto processing_branch = dfp::alu::subtract(1.1f) <<= 20;

    BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 20-1.1f);
}


BOOST_AUTO_TEST_CASE(negate_test)
{
    ///@ [negate usage example]
    std::cout << "'negate' usage example" << std::endl;
    std::cout << "---" << std::endl;

    using namespace dfp;

    auto processing_tree =
        sample_releasing() <<= alu::negate() <<= 1;

    std::cout << processing_tree.get_result() << std::endl;
    // expected result : -1
    ///@ [negate usage example]
    std::cout << std::endl;

    BOOST_CHECK_EQUAL(processing_tree.get_result(), -1);
}

BOOST_AUTO_TEST_CASE(accumulate_test)
{
    {   ///@[accumulate usage example]
        std::cout << "'accumulate' usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        auto processing_tree =
            sample_releasing() <<= accumulate() <<= const_sample(2.0, 1.1, 3.0)
        ;

        std::cout << processing_tree.get_result() << " ; ";
        processing_tree.run();
        std::cout << processing_tree.get_result() << " ; ";
        processing_tree.run();
        std::cout << processing_tree.get_result() << std::endl;
        // expected result : 2.0 ; 3.1 ; 6.1
        ///@[accumulate usage example]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.get_result(), 2 + 1.1 + 3);
        BOOST_CHECK_EQUAL(processing_tree.run(), 2 + 1.1 + 3 + 2.0);
        BOOST_CHECK_EQUAL(processing_tree.run(), 2 + 1.1 + 3 + 2 + 1.1);
        BOOST_CHECK_EQUAL(processing_tree.run(), 2 + 1.1 + 3 + 2 + 1.1 + 3);
    }
    {   ///@[accumulate with max usage example]
        std::cout << "'accumulate' with max usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        auto processing_tree =
            sample_releaser() <<= accumulate<math::functions::max>(std::numeric_limits<double>::min()) <<= const_sample(1.1, 3.0, -4.0, 0.1, 3.1)
        ;

        auto runnable_tree = make_runnable(processing_tree);

        std::cout << runnable_tree.run() << " ; ";
        std::cout << runnable_tree.run() << " ; ";
        std::cout << runnable_tree.run() << " ; ";
        std::cout << runnable_tree.run() << " ; ";
        std::cout << runnable_tree.run() << std::endl;
        // expected result : 1.1 ; 3 ; 3 ; 3 ; 3.1
        ///@[accumulate with max usage example]

        std::cout << std::endl;

        // rebuid runnable tree
        auto runnable_tree2 = make_runnable(decltype(processing_tree){ std::move(processing_tree) });
        BOOST_CHECK_EQUAL(runnable_tree2.run(), 1.1);
        BOOST_CHECK_EQUAL(runnable_tree2.run(), 3.0);
        BOOST_CHECK_EQUAL(runnable_tree2.run(), 3.0);
        BOOST_CHECK_EQUAL(runnable_tree2.run(), 3.0);
        BOOST_CHECK_EQUAL(runnable_tree2.run(), 3.1);

        std::cout << std::endl;
    }
    using namespace dfp;
    using namespace dfp::alu;

    {
        auto processing_branch = accumulate() <<= 1.1;

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1.1 + 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1.1 + 1.1 + 1.1);
    }
    {
        auto processing_branch = accumulate(2) <<= 1.1;

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1 + 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1 + 1.1 + 1.1);
    }
    {
        auto processing_branch = subtract(1.1) <<= accumulate(2) <<= 1.1;

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1 - 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1 + 1.1 - 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2 + 1.1 + 1.1 + 1.1 - 1.1);
    }
    {
        auto processing_branch = accumulate(std::numeric_limits<double>::min(), math::functions::max{}) <<= const_sample(1.1, 3.0, -4.0, 0.1, 3.1);

        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1.1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3.0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3.0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3.0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3.1);
    }
    {   ///@[acc usage example]
        std::cout << "'acc' usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu::shortname;

        auto running_tree =
            sample_releasing() <<= acc() <<= const_sample(1.1, 3.0, -4.0, 0.1, -(1.1+3.0-4.0+0.1))
        ;

        std::cout << running_tree.get_result() << ", ";
        std::cout << running_tree.run() << ", ";
        std::cout << running_tree.run() << ", ";
        std::cout << running_tree.run() << ", ";
        std::cout << running_tree.run() << std::endl;
        // expected result: 1.1, 4.1, 0.1, 0.2, 0
        ///@[acc usage example]

        BOOST_CHECK_EQUAL(running_tree.run(), 1.1);
        BOOST_CHECK_EQUAL(running_tree.run(), 1.1+3.0);
        BOOST_CHECK_EQUAL(running_tree.run(), 1.1+3.0-4.0);
        BOOST_CHECK_EQUAL(running_tree.run(), 1.1+3.0-4.0+0.1);

        std::cout << std::endl;
    }
    //TODO: constexpr ?
}


BOOST_AUTO_TEST_CASE(accumulator_test)
{
    {
        ///@['accumulating sink' usage example with sample-based stream]
        std::cout << "'accumulating sink' usage example with sample-based stream" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        //implicit conversion to result
        double cumul = accumulating(3) <<= const_sample(2.0, 1.1, 3.0);

        std::cout << cumul << std::endl;
        // expected result : 6.1
        ///@['accumulating sink' usage example with sample-based stream]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(cumul, 2 + 1.1 + 3);
    }
    {
        ///@['accumulator sink' usage example with frame-based stream]
        std::cout << "'accumulator sink' usage example with frame-based stream" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        auto processing_tree = make_runnable(
            accumulator(3) <<= routing::const_frame({2.0, 1.1, 3.0})
        );

        std::cout << processing_tree.run() << std::endl;
        // expected result : 6.1
        ///@['accumulator sink' usage example with frame-based stream]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.run(), 2*(2 + 1.1 + 3));
    }
    {
        ///@['accumulator sink' usage example with custom cumul operation]
        std::cout << "''accumulator sink' usage example with custom cumul operation" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        auto processing_tree = make_runnable(
            accumulator<upg::multiplies<>>(3, 1) <<= const_sample(2.0, 1.1, 3.0)
        );

        std::cout << processing_tree.run() << std::endl;
        // expected result : 6.6
        ///@['accumulator sink' usage example with custom cumul operation]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.run(), (2 * 1.1 * 3)*(2 * 1.1 * 3));
    }
}

class my_int {
    constexpr int clip(int val) const
    { return (val > 3) ? 0 : val; }

public:
    constexpr my_int()
    {}

    constexpr my_int(int const& value) : value_(clip(value))
    {}

    constexpr operator int() const
    { return value_; }

    my_int operator+(my_int const& i) const
    { return value_+i.value_; }

    my_int operator-(my_int const& i) const
    { return value_-i.value_; }

private:
    int value_{};
};

template<>
struct upg::plus<my_int>
{ my_int operator()(my_int const& a, my_int const& b){ return a+b; } };

template<class = void>
struct my_int_meta : dfp::type_meta<my_int, my_int_meta<>> {
    static constexpr auto NAME      = "my_int";
};
template<> struct dfp::resolve_type_meta<my_int> { typedef my_int_meta<> type; };
DFP_INLINE_CONSTEXPR my_int_meta<> MY_INT;


BOOST_AUTO_TEST_CASE(counter_test)
{
    using namespace dfp;

    #if 0
    { // type traits which does not work because counter has not been implemented as a source (even if used as if it was a source)
      // is it really a problem ?
        STATIC_CHECK((detail::is_source<decltype(               alu::counter()   )>::value));
        STATIC_CHECK((detail::source_produces_sample<decltype(  alu::counter()   )>::value));
    }
    #endif
    {
        ///@[counter usage example]
        std::cout << "'counter' usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp::alu;
        auto print = [](double in) { std::cout << in << ", "; };
        auto print_last = [](double in) { std::cout << in << std::endl; };

        auto processing_tree =
            sample_releasing() <<= add()    += print_last <<= counter(0.1, 1.0)
                                            |= subtract()   += multiply(2) <<= print <<= counter()
                                                            |= print <<= counter(2)
        ;

        processing_tree.run();
        processing_tree.run();

        // expected result :
        //0, 0, 1
        //2, 1, 1.1
        //4, 2, 1.2
        ///@[counter usage example]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.run(), 1.0 + 0.1 + 0.1 + 0.1);
        BOOST_CHECK_EQUAL(processing_tree.run(), 1.0 + 0.1 + 0.1 + 0.1 + 0.1);
        BOOST_CHECK_EQUAL(processing_tree.run(), 1.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1);
        std::cout << std::endl;
    }
    {
        ///@[counter type_meta usage example]
        std::cout << "'counter' usage example with type meta parameter" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp::alu;
        auto print = [](double in) { std::cout << in << ", "; };

        auto processing_tree
            = sample_releasing() <<= print <<= alu::counter(FLOAT, 0.5)
        ;

        processing_tree.run();
        processing_tree.run();

        // expected result :
        //0, 0.5, 1.0,
        ///@[counter type_meta usage example]
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.run(), 1.5f);
        BOOST_CHECK_EQUAL(processing_tree.run(), 2.0f);
        BOOST_CHECK_EQUAL(processing_tree.run(), 2.5f);
        std::cout << std::endl << std::endl;
    }
    {
        std::cout << "usage example of 'counter' node with custom type_meta" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp::alu;
        auto print = [](my_int in) { std::cout << (int) in << ", "; };

        auto processing_tree
            = sample_releasing() <<= print <<= alu::counter(MY_INT, 1, 2)
        ;

        processing_tree.run();
        processing_tree.run();
        processing_tree.run();

        // expected result :
        //2, 3, 0, 1
        std::cout << std::endl;

        BOOST_CHECK_EQUAL(processing_tree.run(), 2);
        BOOST_CHECK_EQUAL(processing_tree.run(), 3);
        BOOST_CHECK_EQUAL(processing_tree.run(), 0);
        std::cout << std::endl << std::endl;
    }
}


BOOST_AUTO_TEST_CASE(modulo_test)
{
    auto processing_branch = dfp::alu::modulo(3) <<= 20L;

    BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 20L%3);
}


// memory layout is compiler and platform dependent so only enabling test for compilers and plaforms well-known
BOOST_AUTO_TEST_CASE(sizeof_test)
{
    using namespace dfp;
    using namespace dfp::alu;

#if __x86_64__
#if __clang__
    STATIC_CHECK(sizeof(multiply(2) <<= 0.1f)   == (sizeof(2) + sizeof(0.1f))   );
    STATIC_CHECK(sizeof(divide(1.1f) <<= 20)    == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(add(1.1f) <<= 20)       == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(subtract(1.1f) <<= 20)  == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(negate() <<= 1)         == sizeof(1)                    );
    STATIC_CHECK(sizeof(accumulate(2) <<= 1.1)  == 2*sizeof(1.1)                );
    STATIC_CHECK(sizeof(counter(2))             == 2*sizeof(2)                  );
    STATIC_CHECK(sizeof(modulo(3) <<= 20L)      == sizeof(3) + sizeof(20L) + 4  );
  
    STATIC_CHECK(sizeof(multiply(2) <<= int_const<1>())                 == 8);
    STATIC_CHECK(sizeof(multiply(int_const<2>()) <<= int_const<1>())    == 2);
#endif // __clang__
#if !defined(__clang__) && defined(__GNUC__)
    STATIC_CHECK(sizeof(multiply(2) <<= 0.1f)   == (sizeof(2) + sizeof(0.1f))   );
    STATIC_CHECK(sizeof(divide(1.1f) <<= 20)    == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(add(1.1f) <<= 20)       == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(subtract(1.1f) <<= 20)  == (sizeof(1.1f) + sizeof(20))  );
    STATIC_CHECK(sizeof(negate() <<= 1)         == sizeof(1)                    );
    STATIC_CHECK(sizeof(accumulate(2) <<= 1.1)  == 2*sizeof(1.1)                );
    STATIC_CHECK(sizeof(counter(2))             == 2*sizeof(2)                  );
    STATIC_CHECK(sizeof(modulo(3) <<= 20L)      == sizeof(3) + sizeof(20L) + 4  );

#if DFP_CPP11_SUPPORT && !DFP_CPP14_SUPPORT // weird optimization applied in c++11 !?
    STATIC_CHECK(sizeof(multiply(2) <<= int_const<1>())                 == 8);
#else
    STATIC_CHECK(sizeof(multiply(2) <<= int_const<1>())                 == sizeof(2)+sizeof(int));
#endif
    STATIC_CHECK(sizeof(multiply(int_const<2>()) <<= int_const<1>())    == 2);
#endif // !__clang__
#endif // __x86_64__
}


BOOST_AUTO_TEST_CASE(constexpr_test)
{
    using namespace dfp::alu::nodes;

    {
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        //TODO: investigate issue in compilation below
//        constexpr auto processing_branch = modulo(3L) <<= negate() <<= subtract<int>(LONG_MIN)
//                                                <<= add(3.1) <<= divide(11) <<= multiply(1.1f) <<= 20;
        constexpr auto processing_branch = modulo(3L) <<= negate() <<= subtract<int>(3)
                                                <<= add(3) <<= divide(11) <<= multiply(1) <<= 20;

        STATIC_CHECK((*processing_branch.cdrain().cbegin() != 0));
#endif
    }
    {
        //TODO
//        constexpr int const input = 20;
//        constexpr auto processing_branch = modulo(3L) <<= negate() <<= subtract<int>(LONG_MIN)
//                                                <<= add(3.1) <<= divide(11) <<= multiply(1.1f) <<= input;
//
//        BOOST_CHECK_EQUAL(sizeof(*processing_branch.begin()), sizeof(long));
    }
}


#if 0
const size_t SIZE = 10000000;
int result1[SIZE] = {1,}, result2[SIZE] = {1,};
BOOST_AUTO_TEST_CASE(other_test)
{
    using namespace dfp;
    using namespace dfp::basic;
    using namespace dfp::arithmetic;
    using namespace std::chrono;

    /*-- constexpr test --*/
    constexpr auto processing_branch =
        bit_and(-3) <<= bit_xor<long>(0x1234567812345678L) <<= rightshift(3) <<= leftshift(4) <<= 11;
    static_assert(
        *processing_branch.drain().begin() == ((((11 << 4) >> 3) ^ 0x1234567812345678L) & -3)
    , "");

//TODO: frame_releaser to be redesigned to allow constexpr
//    constexpr auto graph = frame_releaser() <<= processing_branch;

    /*-- test of processing time optimization (requires at least O2 flag) --*/
    high_resolution_clock::time_point t1, t2;

    auto processing_branch1 = negate() <<= bit_and(-1) <<= bit_xor(1) <<= rightshift(1) <<= leftshift(1)
                            <<= sources::random();

    t1 = high_resolution_clock::now();
    for(size_t i = 1 ; i < SIZE ; i++)
        result1[i] = (unsigned) processing_branch1;

    auto bb = sources::random();

    for (int i = 0 ; i < 500 ; i++)
        std::cout << *bb.drain().begin() << " ";

    std::cout << std::endl;
    t2 = high_resolution_clock::now();

    auto duration1 = duration_cast<microseconds>( t2 - t1 ).count();

    auto processing_branch2 = leftshift(1) <<= leftshift(1) <<= leftshift(1) <<= leftshift(1) <<= 10;

    t1 = high_resolution_clock::now();
    for(size_t i = 1 ; i < SIZE ; i++)
        result2[i] = processing_branch2;

    t2 = high_resolution_clock::now();
    auto duration2 = duration_cast<microseconds>( t2 - t1 ).count();

    std::cout << result1[1] << ": processing duration with mutable input : " << duration1 << std::endl;
    std::cout << result2[SIZE-1] << ": processing duration with unmutable input : " << duration2 << std::endl;
}
#endif


BOOST_AUTO_TEST_CASE(trait_test)
{
    using namespace dfp::alu;
    using namespace dfp::detail;


    static_assert(is_node<decltype(                 negate()    <<= 0   )>::value, "");
    static_assert(is_node<decltype(                 subtract(0) <<= 0   )>::value, "");
    static_assert(is_node<decltype(                 multiply(0) <<= 0   )>::value, "");
    static_assert(is_node<decltype(                 divide(1)   <<= 0   )>::value, "");
    static_assert(is_node<decltype(                 modulo(1)   <<= 0   )>::value, "");
    static_assert(is_node<decltype(                 add(1)      <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    negate()    <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    subtract(0) <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    multiply(0) <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    divide(1)   <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    modulo(1)   <<= 0   )>::value, "");
    static_assert(node_produces_sample<decltype(    add(1)      <<= 0   )>::value, "");
}


BOOST_AUTO_TEST_CASE(equal_to_test)
{
    using namespace dfp;
    using namespace dfp::alu::shortname;

    {
        ///@[equal_to usage example]
        std::cout << "'equal_to' usage example" << std::endl;
        std::cout << "---" << std::endl;

        DFP_IF(DFP_TREE_CONSTEXPR_SUPPORT)(constexpr,) auto processing_tree =
            sample_releasing() <<= alu::equal_to()  += 1.1
                                                    |= const_sample(1.1)
        ;

        std::cout << processing_tree.get_result() << std::endl;
        //expected output : 1
        ///@[equal_to usage example]
        std::cout << std::endl;

#if DFP_TREE_CONSTEXPR_SUPPORT
        STATIC_CHECK((processing_tree.get_result() == (1.1 == 1.1)));
#endif
        BOOST_CHECK_EQUAL(processing_tree.get_result(), true);
    }
    {
        using namespace dfp::alu::shortname;

        ///@[equal_to usage example with preset]
        constexpr auto processing_branch =  alu::equal_to(eq(2) <<= 2) <<= true;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[equal_to usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (true == (2 == 2))));
#endif
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
    }
}


BOOST_AUTO_TEST_CASE(not_equal_test)
{
    using namespace dfp;
    using namespace dfp::alu::shortname;

    {
        ///@[not_equal usage example]
        constexpr auto processing_branch =  alu::not_equal()    += 1.1
                                                                |= const_sample(1.1)
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 0
        ///@[not_equal usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1.1 != 1.1)));
#endif
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false);
    }
    {
        using namespace dfp::alu::shortname;

        ///@[not_equal usage example with preset]
        constexpr auto processing_branch =  alu::not_equal(ne(2) <<= 2) <<= true;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[not_equal usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (true != (2 != 2))));
#endif
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
    }
}


BOOST_AUTO_TEST_CASE(logical_not_test)
{
    using namespace dfp;
    using namespace dfp::alu;
    using namespace dfp::alu::shortname;

    constexpr auto processing_branch = lnot() <<= logical_not() <<= true;

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    STATIC_CHECK((*processing_branch.cdrain().cbegin() == !!true));
#endif
    BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
}


BOOST_AUTO_TEST_CASE(less_than_test)
{
    using namespace dfp;

    {
        ///@[less_than usage example]
        constexpr auto processing_branch =  alu::less_than()    += 1.1
                                                                |= 1_c
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[less_than usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 < 1.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
    {
        ///@[less_than usage example with preset]
        constexpr auto processing_branch =  alu::less_than(alu::add(0.1) <<= 1) <<= 1;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[less_than usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 < 1 + 0.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
}


BOOST_AUTO_TEST_CASE(less_equal_test)
{
    using namespace dfp;

    {
        ///@[less_equal usage example]
        constexpr auto processing_branch =  alu::less_equal()   += 1.1
                                                                |= 1_c
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[less_equal usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 <= 1.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
    {
        ///@[less_equal usage example with preset]
        constexpr auto processing_branch =  alu::less_equal(alu::add(0.1) <<= 1) <<= 1;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[less_equal usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 <= 1 + 0.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
}


BOOST_AUTO_TEST_CASE(greater_equal_test)
{
    using namespace dfp;

    {
        ///@[greater_equal usage example]
        constexpr auto processing_branch =  alu::greater_equal()    += 1.1
                                                                    |= 1_c
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 0
        ///@[greater_equal usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 >= 1.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false);
#endif
    }
    {
        ///@[greater_equal usage example with preset]
        constexpr auto processing_branch =  alu::greater_equal(alu::add(0.1) <<= 1) <<= 1;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 0
        ///@[greater_equal usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 >= 1 + 0.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false);
#endif
    }
}


BOOST_AUTO_TEST_CASE(greater_than_test)
{
    using namespace dfp;

    {
        ///@[greater_than usage example]
        constexpr auto processing_branch =  alu::greater_than() += 1.1
                                                                |= 1_c
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 0
        ///@[greater_than usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 > 1.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false);
#endif
    }
    {
        ///@[greater_than usage example with preset]
        constexpr auto processing_branch =  alu::greater_than(alu::add(0.1) <<= 1) <<= 1;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 0
        ///@[greater_than usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (1 > 1 + 0.1)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false);
#endif
    }
}


BOOST_AUTO_TEST_CASE(logical_and_test)
{
    using namespace dfp;
    using namespace dfp::alu::shortname;

    {
        ///@[logical_and usage example]
        constexpr auto processing_branch =  alu::logical_and()  += true
                                                                |= true_c
        ;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[logical_and usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (true && true)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
    {
        ///@[logical_and usage example with preset]
        constexpr auto processing_branch =  alu::logical_and(land(true) <<= true) <<= true;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[logical_and usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (true && (true && true))));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
}


BOOST_AUTO_TEST_CASE(logical_or_test)
{
    using namespace dfp;
    using namespace dfp::alu::shortname;

    {
        ///@[logical_or usage example]
        constexpr auto processing_branch = 
            alu::logical_or()   += true
                                |= false
                                |= false_c
        ;
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), false || false || true);
        ///@[logical_or usage example]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (false || false || true)));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
    {
        ///@[logical_or usage example with preset]
        constexpr auto processing_branch =  alu::logical_or(lor(false) <<= false) <<= true;
        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        //expected output : 1
        ///@[logical_or usage example with preset]

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (true || (false || false))));
#else
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), true);
#endif
    }
}


BOOST_AUTO_TEST_CASE(average_channels_test)
{
    {
        ///@[average_channels usage example]
        std::cout << "average_channels usage example" << std::endl;
        std::cout << "---" << std::endl;
        
        using namespace dfp;

        auto tree =
            sample_releasing() <<= routing::to_single_sample() <<= alu::average_channels() <<= routing::const_frame({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 5)
        ;

        std::cout << tree.get_result() << " " << tree.run() << std::endl;
        //expected output : 3 8
        ///@[average_channels usage example]

        std::cout << std::endl;

        BOOST_CHECK_EQUAL(tree.run(), 3);
        BOOST_CHECK_EQUAL(tree.run(), 8);
    }
    {
        using namespace dfp;

        auto sample_wrapper =
            sample_releasing() <<= alu::average_channels() <<= routing::const_frame({1, 2, 3, 4, 5}, 5_c)
        ;

        BOOST_CHECK_EQUAL(sample_wrapper.get_result(), 3);
    }
    {
        using namespace dfp;

        auto running_tree =
            sample_releasing() <<= alu::average_channels() <<= routing::const_frame({1.0, 2.0, 3.0, 4.0, 5.0}, 5_c)
        ;

        BOOST_CHECK_EQUAL(running_tree.get_result(), 3);
    }
    {
        using namespace dfp;

        auto tree =
            sample_releasing() <<= routing::to_single_sample() <<= alu::average_channels()
            <<= routing::const_frame({1.0l, 2.0l, 3.0l, 4.0l, 5.0l, 6.0l, 7.0l, 8.0l, 9.0l, 10.0l}, 5_c)
        ;

        BOOST_CHECK_EQUAL(*tree, 3);
        BOOST_CHECK_EQUAL(*++tree, 8);
    }
#if 0
    { //TODO: good example to go deeper in constexpr support
        using namespace dfp;

        constexpr auto running_tree =
            sample_releasing() <<= alu::average_channels() <<= routing::const_frame({1, 2, 3, 4, 5}, 5_c)
        ;

        std::cout << running_tree.get_result() << std::endl;

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST && DFP_CPP14_SUPPORT
        STATIC_CHECK((running_tree.get_result() == 3));
#else
        BOOST_CHECK_EQUAL(running_tree.get_result(), 3);
#endif
    }
#endif
}


BOOST_AUTO_TEST_CASE(schmitt_trigger_test)
{
    {
        using namespace dfp;
        using namespace dfp::alu;

        STATIC_CHECK((core::detail::is_node<decltype(             schmitt_trigger(1.3f) <<= 0  )>{}));
        STATIC_CHECK((core::detail::node_produces_sample<decltype(schmitt_trigger(1.3f) <<= 0  )>{}));
    }
    {
        ///@[schmitt_trigger usage example]
        std::cout << "schmitt_trigger usage example" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;
        using namespace dfp::alu;

        auto print = execute([](int32_t const& in){ std::cout << in << " "; });
        auto new_line = execute([](bool const&){ std::cout << std::endl; });

#if DFP_GUARANTEED_COPY_ELISION
        auto runnable_tree = make_runnable(
#else
        // Before c++17 copy-elision may not be guaranteed with "auto" however runnable tree defined below is not move-constructible because of probe node.
        // Consequently r-value reference on runnable tree is used.
        auto runnable_tree = make_runnable(
#endif
            sample_releaser() <<= new_line <<= print <<= schmitt_trigger()  += const_sample(2)
                                                                            |= print <<= subtract(2) <<= modulo(6) <<= counter(1)
        );

        // expected output
        //1 0 0 0
        ///@[schmitt_trigger usage example]
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);

        std::cout << std::endl;
    }
    {
        ///@[schmitt_trigger usage example with preset]
        using namespace dfp;
        using namespace dfp::alu;
        using namespace dfp::alu::shortname;

        auto print = execute([](int32_t const& in){ std::cout << in << " "; });
        auto new_line = execute([](bool const&){ std::cout << std::endl; });
        struct : custom_tag {} TRIANGLE;
        auto triangle =
            print <<= accumulate() <<= subtract(1) <<= multiply(2) <<= less_than(12/2) <<= modulo(12) <<= counter(INT32_T);

        auto runnable_tree = make_runnable(
            sample_releaser() <<= new_line  <<= logical_or()    += print <<= schmitt_trigger(1) <<= sub(4) <<= from(TRIANGLE)
                                                                |= print <<= schmitt_trigger(2) <<= sub(3) <<= from(TRIANGLE)
                                                                |= print <<= schmitt_trigger(2) <<= sub(2) <<= probe(TRIANGLE) <<= triangle
         );

        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        runnable_tree.run();
        // expected output
        //1 0 0 0
        //3 0 0 0
        //2 0 0 0
        //4 1 0 0
        //5 1 1 1
        //6 1 1 1
        //5 1 1 1
        //4 1 1 1
        //3 1 1 0
        //2 1 1 0
        //1 1 0 0
        //0 0 0 0
        //1 0 0 0
        ///@[schmitt_trigger usage example with preset]

        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), true);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);
        BOOST_CHECK_EQUAL(runnable_tree.run(), false);

        std::cout << std::endl;

        auto running_tree {
            sample_releasing() <<= new_line  <<= logical_or()   += print <<= schmitt_trigger(1_c) <<= sub(4) <<= from(TRIANGLE)
                                                                |= print <<= schmitt_trigger(2_c) <<= sub(2) <<= from(TRIANGLE)
                                                                |= print <<= schmitt_trigger(2_c) <<= sub(1) <<= probe(TRIANGLE) <<= triangle
        };
        BOOST_CHECK_EQUAL(running_tree.get_result(), false);
    }
#if DFP_TREE_CONSTEXPR_SUPPORT
    {
        using namespace dfp;
        using namespace dfp::alu;

        constexprauto const tree = sample_releasing() <<= schmitt_trigger(2) <<= subtract(2) <<= modulo(6) <<= 1;

        STATIC_CHECK((tree.get_result() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == true));
        STATIC_CHECK((tree.run() == true));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == false));
        STATIC_CHECK((tree.run() == true));
    }
#endif
}
