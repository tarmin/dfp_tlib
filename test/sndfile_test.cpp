/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/sndfile/sndfile.hpp>
#include <dfptl/core/core.hpp>
#include <dfptl/math/sin.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/sample_static_cast.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>
#include <dfptl/alu/modulo.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/equal_to.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/alu/accumulate.hpp>
#include <dfptl/alu/logical_not.hpp>

#include <chrono>
#include <cstdint>
#include <iostream>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert(x, "")


using namespace dfp;
using namespace dfp::math;
using namespace dfp::routing;
using namespace dfp::alu::shortname;


constexpr double const PI = 3.1415926535898;
constexpr const int SAMPLING_RATE = 44100; //hertz
constexpr auto FRAME_LENGTH = size_t_const<441>{};
constexpr const int DURATION = 3; //seconds


BOOST_AUTO_TEST_CASE(sndfile_write_test)
{
    { // sample-based writing
        std::cout << std::endl << "sample-based writing";
        auto t1 = std::chrono::high_resolution_clock::now();

        ///@[sndfile::write usage example]
        looping(SAMPLING_RATE*DURATION) <<= sndfile::write("record1.wav") <<= sin() <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE)
            <<= counter(1);
        ///@[sndfile::write usage example]

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }

    { // frame-based writing
        std::cout << std::endl << "frame-based writing";
        auto t1 = std::chrono::high_resolution_clock::now();

        looping(SAMPLING_RATE*DURATION) <<= sndfile::write("fb_record.wav") <<= adjust_frame_length(FRAME_LENGTH) <<= sin()
            <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE) <<= counter(1);

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }
}


BOOST_AUTO_TEST_CASE(sndfile_read_test)
{
    STATIC_CHECK((dfp::detail::is_source<decltype(sndfile::read<double>("").build())>{}));
    STATIC_CHECK((dfp::detail::is_source<decltype(sndfile::read<125>("").build())>{}));
    STATIC_CHECK((dfp::detail::is_source<decltype(sndfile::read("").build())>{}));
    STATIC_CHECK((dfp::detail::source_produces_frame<decltype(sndfile::read<double>("").build())>{}));

    { // sample-based reading given sndfile handle
        std::cout << std::endl << "sample-based reading given sndfile handle";
        auto t1 = std::chrono::high_resolution_clock::now();

        ///@[sndfile::reading_handle and sndfile::get_info usage example]
        using namespace dfp;
        using namespace dfp::routing;

        SF_INFO sndfile_info; sndfile_info.format = 0;
        SNDFILE* sndfile_handle = sf_open("record1.wav", SFM_READ, &sndfile_info);

        looping(sndfile_info.frames) <<= sndfile::read(sndfile_handle)
        ;
        ///@[sndfile::reading_handle and sndfile::get_info usage example]

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }
    { //sample-based reading
        std::cout << std::endl << "sample-based reading";
        auto t1 = std::chrono::high_resolution_clock::now();

        ///@[sndfile::read usage example]
        using namespace dfp;
        using namespace dfp::routing;
        using namespace dfp::alu::shortname;

        constexpr double const PI = 3.1415926535898;
        constexpr const int SAMPLING_RATE = 44100; //hertz
        constexpr const int DURATION = 3; //seconds

        auto check_true = execute([](bool const& in){ BOOST_CHECK(in); });

        looping(SAMPLING_RATE*DURATION) <<= check_true <<= lnot() <<= acc() <<= lnot()
            <<= eq()    +=  sndfile::read("record1.wav", FLOAT)
                        |=  sample_static_cast<float>() <<= math::sin() <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE)
                                <<= counter(1)
        ;
        ///@[sndfile::read usage example]

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }
    { // test frame-based reading
        std::cout << std::endl << "frame-based reading";
        auto t1 = std::chrono::high_resolution_clock::now();

        auto check_true = execute([](bool const& in){ BOOST_CHECK(in); });

        looping((SAMPLING_RATE*DURATION / FRAME_LENGTH) * FRAME_LENGTH) <<= check_true <<= lnot() <<= acc() <<= lnot()
            <<= eq()    +=  to_single_sample() <<= sndfile::read("record1.wav", FLOAT, FRAME_LENGTH)
                        |=  sample_static_cast<float>() <<= math::sin() <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE)
                                <<= counter(1)
        ;

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }
    { // deferred test frame-based reading
        std::cout << std::endl << "deferred frame-based reading";
        auto t1 = std::chrono::high_resolution_clock::now();

        auto check_true = execute([](bool const& in){ BOOST_CHECK(in); });

        auto reader = make_runnable(
            looper((SAMPLING_RATE*DURATION / FRAME_LENGTH) * FRAME_LENGTH) <<= check_true <<= lnot() <<= acc() <<= lnot()
            <<= eq()    +=  to_single_sample() <<= sndfile::read("record1.wav", FLOAT, FRAME_LENGTH)
                        |=  sample_static_cast<float>() <<= math::sin() <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE)
                                <<= counter(1)
        );

        reader.run();
        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        std::cout << " done in " << duration << "ms" << std::endl; 
    }
    { // test channel_chunk_length as long integral constant
//        sndfile::read<float>("record1.wav", 2_c);
    }

#if 0
    { // test referencing of read source
        auto file_reader = sndfile::read<float>("record1.wav");
        // line below would fail because sndfile::read source is not copyable
        auto processing_branch = acc() <<= file_reader;
        looping(SAMPLING_RATE*DURATION) <<= file::csv_cout().pi(true) <<= processing_branch;
//      looping(SAMPLING_RATE*DURATION) <<= probe_sample(test_status) <<= lnot() <<= acc() <<= lnot() <<= eq().build
//       (
//            file_reader,
//            sconv<float>() <<= math::sin() <<= mult(2*PI/SAMPLING_RATE) <<= mod(SAMPLING_RATE) <<= counter(1)
//        );
    }
#endif
}
