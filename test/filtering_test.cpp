/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/filtering/finite_difference.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/alu/counter.hpp>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert(x, "")


BOOST_AUTO_TEST_CASE(backward_difference_test)
{
    ///@[backward_difference usage example]
    using namespace dfp;
    using namespace dfp::filtering::shortname;

    {
        routing::looping(6) <<= [](int const& in){BOOST_CHECK_EQUAL(1, in); }
        <<= bdiff(-1) <<= alu::counter(INT32_T);
    }
    ///@[backward_difference usage example]

    { // double chaining
        routing::looping(6) <<= [](int const& in){BOOST_CHECK_EQUAL(0, in); }
        <<= bdiff(1) <<= bdiff(-1) <<= alu::counter(INT32_T);
    }
}
