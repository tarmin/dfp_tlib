/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/core/core.hpp>
#include <dfptl/io/io.hpp>
#include <dfptl/alu/modulo.hpp>
#include <dfptl/alu/negate.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/subtract.hpp>
#include <dfptl/alu/logical_not.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>

#include <boost/test/unit_test.hpp>

#include <iostream>


#define STATIC_CHECK(x) static_assert((x), "")
#define CHECK_FAILURE 0


BOOST_AUTO_TEST_CASE(text_chart_write_test)
{
    using namespace dfp;
    using namespace dfp::io;

    { // type traits check
        STATIC_CHECK((dfp::detail::is_node<decltype(write_text_chart(std::cout) <<= 0)>{}));
    }
    #if CHECK_FAILURE
    { // concept check
        // line below will fail because write_text_chart node is a SINGLE input node
        write_text_chart(std::cout).build(0, 0);

        // line below will fail because write_text_chart node only accepts ARITHMETIC input sample
        write_text_chart(std::cout) <<= nullptr;
    }
    #endif
    {
        std::cout << std::endl;
        std::cout << "text_char_write(std::cout) test" << std::endl;
        std::cout << "---" << std::endl;

        ///@[write_text_chart usage example]
        routing::looping(25) <<= write_text_chart(std::cout, {0, 10, 80}) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        ///@[write_text_chart usage example]

        // max signal value above display range
        routing::looping(25) <<= write_text_chart(std::cout, {0, 8, 80}) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // min signal value below display range
        routing::looping(25) <<= write_text_chart(std::cout, {0, 10, 80}) <<= alu::subtract(2) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // min of display range is below zero
        routing::looping(25) <<= write_text_chart(std::cout, {-3, 10, 80}) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // min of display range is below zero and min signal can be below zero
        routing::looping(25) <<= write_text_chart(std::cout, {-3, 10, 80}) <<= alu::subtract(2) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // min of display range is above zero
        routing::looping(25) <<= write_text_chart(std::cout, {3, 10, 80}) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // signal below x-axis
        routing::looping(25) <<= write_text_chart(std::cout, {-10, 0, 80}) <<= alu::negate() <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // max signal value above display range
        routing::looping(25) <<= write_text_chart(std::cout, {-10, 0, 80}) <<= alu::negate() <<= alu::subtract(2) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // min signal value below display range
        routing::looping(25) <<= write_text_chart(std::cout, {-10, 0, 80}) <<= alu::subtract(2) <<= alu::negate() <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // max of display range is above zero
        routing::looping(25) <<= write_text_chart(std::cout, {-10, 3, 80}) <<= alu::negate() <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // max of display range is above zero and signal can be above zero
        routing::looping(25) <<= write_text_chart(std::cout, {-10, 3, 80}) <<= alu::negate() <<= alu::subtract(2) <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;

        // max of display range is below zero
        routing::looping(25) <<= write_text_chart(std::cout, {-10, -3, 80}) <<= alu::negate() <<= alu::modulo(10) <<= alu::counter(INT32_T);
        std::cout << std::endl;
    }
}


#include <functional>
BOOST_AUTO_TEST_CASE(write_csv_test)
{
    using namespace dfp;
    using namespace dfp::io;

    { // failure test
#if CHECK_FAILURE
        //line below won't compile because write_csv is a double input node
        write_csv(std::cout) <<= 0;

        write_csv(std::cout)    += 1
                                |= 2
                                |= const_sample(0);

        //line below won't compile because write_csv only accepts input streams with same granularity
        cout_csv()  += routing::adjust_frame_length(size_t_const<2>{}) <<= 0
                    |= const_sample(0);
#endif

    }

    STATIC_CHECK((core::detail::is_node<decltype(cout_csv() += const_sample(0) |= const_sample(0))>{}));

    routing::looping(10) <<= cout_csv<flush_policy::on_eol>()   += false
                                                                |= alu::counter(INT32_T);
    std::cout << std::endl;

    ///@[write_csv usage example]
    { // cout_csv node fed with 2 atomic streams
        auto line_feed_control = alu::logical_not() <<= alu::subtract(4) <<= alu::modulo(5) <<= alu::counter(INT32_T);
        constexpr auto cache_flush = mec<flush_policy::on_eol | flush_policy::on_write>();

        routing::looping(12) <<= cout_csv(':', cache_flush) += line_feed_control
                                                            |= alu::counter(INT32_T)
        ;
        // expected output:
        //0:1:2:3:4
        //5:6:7:8:9
        //10:11
        std::cout << std::endl;
    }
    { // cout_csv node fed with 2 static streams with equal frame length
        using namespace dfp::shortname;
        constexpr auto cache_flush = mec<flush_policy::on_eol | flush_policy::on_write>();

        routing::looping(12) <<= cout_csv('-', cache_flush) += routing::const_frame({0, 0, 0, 0, 1, 0, 0, 0, 0, 1,  0,  0})
                                                            |= routing::const_frame({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11})
        ;
        // expected output:
        //0-1-2-3-4
        //5-6-7-8-9
        //10-11
        std::cout << std::endl;
    }
    { // cout_csv node fed with 1 single stream
        routing::looping(15) <<= cout_csv(';') <<= routing::adjust_frame_length(5_c) <<= alu::counter(INT16_T)
        ;
        // expected output:
        //0;1;2;3;4
        //5;6;7;8;9
        //10;11;12;13;14
        std::cout << std::endl;
    }
    { // cout_csv node fed with 1 single stream of dynamic extent
        routing::looping(15) <<= cout_csv(';') <<= routing::adjust_frame_length(3) <<= alu::counter(INT16_T)
        ;
        // expected output:
        //0;1;2
        //3;4;5
        //6;7;8
        //9;10;11
        //12;13;14
        std::cout << std::endl;
    }
    ///@[write_csv usage example]
}
