/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/

#include <iostream>

#include <dfptl/core/core.hpp>
#include <dfptl/routing/const_frame.hpp> //TODO: investigate how to get rid of this include
#include <dfptl/routing/to_single_sample.hpp> //TODO: investigate how to get rid of this include

#include <boost/test/unit_test.hpp>

#include <list>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <stl/experimental/array.hpp>
#include <boost/fusion/include/at_key.hpp>


#define STATIC_CHECK(x) static_assert((x), "")
#define ENABLE_FAILURE_TEST 0


BOOST_AUTO_TEST_SUITE(CoreTestSuite)


using namespace dfp;


template<class Cell>
struct dummy_stream : stream<stream_specifier<>, Cell> {};


template<class Context>
struct dummy_source
 :  public  source<Context, atomic_stream_specifier<int>, dummy_source<Context>>
{
    typedef source<Context, atomic_stream_specifier<int>, dummy_source<Context>> source_type;

    using source_type::source_type;

    constexpr int acquire_sample() const
    { return 1; }
};


template<class Input> struct dummy_sink : sink<Input, run_once, dummy_sink<Input>>
{
    typedef sink<Input, run_once, dummy_sink> sink_type;

    using sink_type::sink_type;

    void release_sample(input_sample_t<sink_type> const&)
    {}
};


template<class Input> struct dummy_node
 :  public  node<Input, atomic_stream_specifier<int>, dummy_node<Input>>
{
    typedef node<Input, atomic_stream_specifier<int>, dummy_node> node_type;

    using node_type::node_type;

    int process_sample(input_sample_t<node_type> const& in)
    { return in; }
};


template<class Input> class constexpr_node : public node<Input, atomic_stream_specifier<int>, constexpr_node<Input>>
{
    typedef node<Input, atomic_stream_specifier<int>, constexpr_node> node_type;

public:
    constexpr constexpr_node(Input input) : node_type(std::forward<Input>(input)) {}

    constexpr output_sample_t<node_type> process_sample(input_sample_t<node_type> const& in) const
    { return in + 2; }
};


template<class Context>
struct my_frame_based_source
 :  public  source<Context, static_stream_specifier<int, 3>, my_frame_based_source<Context>>
{
    typedef source<Context, static_stream_specifier<int, 3>, my_frame_based_source> source_type;

    output_frame_t<source_type> acquire_frame() const
    { return output_frame_t<source_type>{{0, 1, 2}}; }
};


template<class T>
constexpr dfp::detail::processing_graph<T> make_graph(T&& cell)
{ return dfp::detail::processing_graph<T>{ std::forward<T>(cell) }; };

BOOST_AUTO_TEST_CASE(type_meta_test)
{
    using namespace dfp;

    std::cout << BOOL.name() << " parent count: " << BOOL.parent_count() << std::endl;
    std::cout << INT16_T.name() << " parent count: " << INT16_T.parent_count() << ", " << INT16_T.fully_qualified_name();
    std::cout << std::endl;
}


void display_as_function(int const& in) { std::cout << in << std::endl; }
BOOST_AUTO_TEST_CASE(type_traits_test)
{
    using namespace dfp;

    using numeric_types = std::tuple<char, int, long, long long, float, double>;

    struct sum
    { int operator()(int lhs, int rhs) const { return lhs+rhs; } }; // struct sum

    
    struct my_fct
    { int operator()(int const& in) { return prev_ = in ^ prev_; } int prev_; }; // struct my_fct

    struct display
    { void operator()(int const& in) { std::cout << in << std::endl; } ; }; // struct display

    auto display_as_lambda = [](int const& in) { std::cout << in << std::endl; };
    auto nonvoid_lambda = [](int const& in) { return in; };

    STATIC_CHECK((always_true<void>::value));
    STATIC_CHECK((!always_false<void>::value));
    STATIC_CHECK((is_specialization<numeric_types, std::tuple>::value));
    STATIC_CHECK((!is_specialization<int, std::tuple>::value));

    STATIC_CHECK((is_bool<bool>::value));
    STATIC_CHECK((is_bool<bool const>::value));
    STATIC_CHECK((is_bool<bool volatile>::value));
    STATIC_CHECK((is_bool<bool&>::value));
    STATIC_CHECK((is_bool<const bool&>::value));
    STATIC_CHECK((!is_bool<int>::value));

    STATIC_CHECK((is_char<char>::value));
    STATIC_CHECK((is_char<char const>::value));
    STATIC_CHECK((is_char<char volatile>::value));
    STATIC_CHECK((is_char<char&>::value));
    STATIC_CHECK((is_char<const char&>::value));
    STATIC_CHECK((!is_char<int>::value));

    STATIC_CHECK((is_size_t<std::size_t>::value));
    STATIC_CHECK((!is_size_t<int>::value));

    STATIC_CHECK((!is_int<unsigned>::value));
    STATIC_CHECK((is_int<unsigned, false>::value));
    STATIC_CHECK((!is_int<char>::value));

    STATIC_CHECK(!supports_iterator<int>::value);
    STATIC_CHECK(supports_iterator<std::vector<int>>::value);
    // line below would fail because 'int' does not provide any 'iterator' type
//    STATIC_CHECK(is_random_access_iterator<iterator_t<int>>::value);
    STATIC_CHECK(is_random_access_iterator<iterator_t<std::vector<int>>>::value);
    STATIC_CHECK(!is_random_access_iterator<iterator_t<std::list<int>>>::value);
    STATIC_CHECK(is_bidirectional_iterator<iterator_t<std::vector<int>>>::value);
    STATIC_CHECK(is_bidirectional_iterator<iterator_t<std::list<int>>>::value);
    STATIC_CHECK(!is_bidirectional_iterator<iterator_t<std::istreambuf_iterator<char>>>::value);
    STATIC_CHECK(is_input_iterator<iterator_t<std::list<int>>>::value);
    STATIC_CHECK(is_input_iterator<iterator_t<std::istreambuf_iterator<char>>>::value);
    STATIC_CHECK(!is_input_iterator<iterator_t<std::ostreambuf_iterator<char>>>::value);
    STATIC_CHECK(is_output_iterator<iterator_t<std::list<int>>>::value);
    STATIC_CHECK(!is_output_iterator<iterator_t<std::istreambuf_iterator<char>>>::value);
    STATIC_CHECK(is_output_iterator<iterator_t<std::ostreambuf_iterator<char>>>::value);
    STATIC_CHECK((detail::supports_const_call_operator<sum, int, int>::value));
    STATIC_CHECK((detail::supports_call_operator<sum, int, int>::value));
    STATIC_CHECK((!detail::supports_const_call_operator<my_fct, int const&>::value));
    STATIC_CHECK((detail::supports_call_operator<my_fct, int const&>::value));
    STATIC_CHECK((!detail::returns_void<my_fct, int const&>{}));
    STATIC_CHECK((!detail::returns_void<display>{}));
    STATIC_CHECK((detail::returns_void<display, char const&>{}));
    STATIC_CHECK((detail::returns_void<display, int const&>{}));
    STATIC_CHECK((detail::returns_void<decltype(display_as_lambda), int const&>{}));
    STATIC_CHECK((detail::returns_void<decltype(display_as_lambda), char const&>{}));
    STATIC_CHECK((!detail::returns_void<decltype(display_as_lambda)>{}));
    STATIC_CHECK((detail::returns_void<decltype(display_as_function), int const&>{}));
    STATIC_CHECK((detail::returns_void<decltype(display_as_function), char const&>{}));
    STATIC_CHECK((!detail::returns_void<decltype(display_as_function)>{}));
    STATIC_CHECK((!detail::returns_void<decltype(nonvoid_lambda), int const&>{}));
    STATIC_CHECK((is_std_array<std::array<int, 0>>{}));
    STATIC_CHECK((is_std_array<std::array<int, 0> &>{}));
}


BOOST_AUTO_TEST_CASE(frame_traits_test)
{
    using namespace dfp;
    using namespace dfp::detail;

    STATIC_CHECK((!is_frame_traits<int>{}));
    STATIC_CHECK((is_frame_traits<frame_traits<std::array<int, 5>>>{}));
    STATIC_CHECK((is_frame_traits<frame_traits<std::vector<int>>>{}));
    STATIC_CHECK((is_frame_traits<frame_traits<single_sample_frame<int>>>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<std::array<int, 5>>::sample_type, int>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<std::vector<int>>::sample_type, int>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<single_sample_frame<int>>::sample_type, int>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<std::array<int, 5>>::template set_sample<double>::type, std::array<double, 5>>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<std::vector<int>>::template set_sample<double>::type, std::vector<double>>{}));
    STATIC_CHECK((std::is_same<typename frame_traits<single_sample_frame<int>>::template set_sample<double>::type, single_sample_frame<double>>{}));
    STATIC_CHECK((std::is_same<set_sample_t<double, frame_traits<std::array<int, 5>>>, std::array<double, 5>>{}));
    STATIC_CHECK((std::is_same<set_sample_t<double, frame_traits<std::vector<int>>>, std::vector<double>>{}));
    STATIC_CHECK((std::is_same<set_sample_t<double, frame_traits<single_sample_frame<int>>>, single_sample_frame<double>>{}));
    //TODO: Need for some sample_t<> helper specialization for frame_traits ?
}


BOOST_AUTO_TEST_CASE(placeholder_traits_test)
{
    using namespace dfp;
    using namespace dfp::detail;

    auto placeholder = make_placeholder<dummy_node>();

    STATIC_CHECK((is_cell_builder<decltype(placeholder)>{}));
    STATIC_CHECK((std::is_same<cell_t<decltype(placeholder), initial_context>, dummy_node<initial_context>>{}));
}


BOOST_AUTO_TEST_CASE(stream_traits_test)
{
    STATIC_CHECK((is_stream<dummy_stream<int>>{}));
    STATIC_CHECK((std::is_same<frame_t<dummy_stream<int>>, frame_t<stream_specifier<>>>{}));
    STATIC_CHECK((std::is_same<sample_t<dummy_stream<int>>, sample_t<stream_specifier<>>>{}));
}


BOOST_AUTO_TEST_CASE(stream_specifier_test)
{
    using namespace dfp;

    {
#if 0
    // line below will raise a static assertion failure because if SampleBased is true, no extra parameter are allowed
//    stream_specifier<int, true, void, void> tt;
    // line below will raise a static assertion failure because if SampleBased is false,
    // at least one extra parameter shall be provided
//    stream_specifier<int, false, void> tt;
    // line below will raise a static assertion failure because if SampleBased is false,
    // the first extra parameter shall be a flow_constant specialized type
//    stream_specifier<int, false, void, void> tt;
        // will fail because given Sample template argument is const
        STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<const double>>{}));
#endif
    }
    STATIC_CHECK((std::is_same<sample_t<atomic_stream_specifier<int>>, int>{}));
    STATIC_CHECK((std::is_same<sample_t<stream_specifier<>>, default_sample_t<>>{}));
    STATIC_CHECK((std::is_same<sample_t<dynamic_stream_specifier<int>>, int>{}));
    STATIC_CHECK((stream_specifier<>::set_frame_extent_t<3>{}.get_frame_length() == 3));
    STATIC_CHECK((!is_stream_specifier<int>{}));
    STATIC_CHECK((is_stream_specifier<stream_specifier<2>>{}));
    STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<int>>{}));
    STATIC_CHECK((is_stream_specifier<stream_specifier<DYNAMIC_EXTENT>>{}));
    STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<double*>>{}));
    STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<double const*>>{}));
#ifdef ENABLE_WARNING_TEST
    STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<int>::set_sample_clue_t<const double>>{}));
    STATIC_CHECK((is_stream_specifier<set_sample_t<const double, atomic_stream_specifier<int>>>{}));
#endif
    STATIC_CHECK((is_stream_specifier<atomic_stream_specifier<int>>{}));
    STATIC_CHECK((atomic_stream_specifier<int>::get_frame_length() == 1));
    STATIC_CHECK((atomic_stream_specifier<std::nullptr_t>{}.set_sample_type(FLOAT).get_sample_type() == FLOAT));
    stream_specifier<>::set_sample_clue_t<float>();
    set_sample<float, atomic_stream_specifier<detail::null_tag>>::type();
    typedef static_stream_specifier<int, 12>::set_tag_clue_t<custom_tag>::set_channel_extent_t<2> x;
    STATIC_CHECK((x{}.get_channel_count() == 2));
    STATIC_CHECK((static_stream_specifier<int, 12>::IS_CHANNEL_COUNT_STATIC == true));
    STATIC_CHECK((static_stream_specifier<int, 12, DYNAMIC_EXTENT>::IS_CHANNEL_COUNT_STATIC == false));
    STATIC_CHECK((static_stream_specifier<int, DYNAMIC_EXTENT, DYNAMIC_EXTENT>{}.set_channel_count(DYNAMIC_EXTENT).IS_CHANNEL_COUNT_STATIC == false));
    STATIC_CHECK((static_stream_specifier<int, 12, 2>{}.set_channel_count(size_t_const<2>{}).IS_CHANNEL_COUNT_STATIC == true));
    auto z = static_stream_specifier<int, 12, DYNAMIC_EXTENT>{}.set_channel_count(2).is_channel_count_specified();
    BOOST_CHECK_EQUAL(z, true);
    STATIC_CHECK((static_stream_specifier<int, 12>::IS_FRAME_LENGTH_STATIC == true));
    STATIC_CHECK((static_stream_specifier<int, DYNAMIC_EXTENT>::IS_FRAME_LENGTH_STATIC == false));
    STATIC_CHECK((static_stream_specifier<int, DYNAMIC_EXTENT>{}.set_frame_length(DYNAMIC_EXTENT).IS_FRAME_LENGTH_STATIC == false));
    STATIC_CHECK((static_stream_specifier<int, 12>{}.set_frame_length(size_t_const<12>{}).IS_FRAME_LENGTH_STATIC == true));
    auto y = static_stream_specifier<int, DYNAMIC_EXTENT>{}.set_frame_length(12).is_frame_length_specified();
    BOOST_CHECK_EQUAL(y, true);
}


BOOST_AUTO_TEST_CASE(static_stream_specifier_test)
{
    constexpr stream_specifier<>::set_sample_clue_t<float>::set_frame_extent_t<5> x{};
    STATIC_CHECK((x.get_frame_length() == 5));
}


BOOST_AUTO_TEST_CASE(single_sample_frame_test)
{
    using namespace dfp;
    using namespace dfp::detail;

    { // default constructor for unmutable frame
        constexpr auto frame = single_sample_frame<int>();

        STATIC_CHECK((*frame.cbegin() == int{}));
        STATIC_CHECK((frame.cbegin() != frame.cend()));
        STATIC_CHECK((frame.begin() != frame.end()));
        STATIC_CHECK((frame.size() == 1));
        STATIC_CHECK((!frame.empty()));
    }
    { // default constructor for mutable frame
        auto frame = single_sample_frame<int>();

        *frame.begin() = 1;
        BOOST_CHECK_EQUAL(*frame.cbegin(), 1);
        BOOST_CHECK_EQUAL(frame.cbegin() != frame.cend(), true);
        BOOST_CHECK_EQUAL(*frame.begin(), 1);
        BOOST_CHECK_EQUAL(frame.begin() != frame.end(), true);
        STATIC_CHECK((frame.size() == 1));
        STATIC_CHECK((!frame.empty()));
        for(auto s : frame) BOOST_CHECK_EQUAL(s, 1);

        auto it = frame.begin();
        BOOST_CHECK_EQUAL(*it++, 1);
        BOOST_CHECK_EQUAL(it == frame.end(), true);
    }
    { // explicit constructor for unmutable frame
        constexpr auto frame = single_sample_frame<int>(2);

        STATIC_CHECK((*frame.cbegin() == 2));
        STATIC_CHECK((frame.cbegin() != frame.cend()));
        STATIC_CHECK((frame.begin() != frame.end()));
        STATIC_CHECK((frame.size() == 1));
        STATIC_CHECK((!frame.empty()));
    }
    { // explicit constructor for mutable frame
        auto frame = single_sample_frame<int>(3);

        BOOST_CHECK_EQUAL(*frame.cbegin(), 3);
        BOOST_CHECK_EQUAL(frame.cbegin() != frame.cend(), true);
        BOOST_CHECK_EQUAL(*frame.begin(), 3);
        BOOST_CHECK_EQUAL(frame.begin() != frame.end(), true);
        STATIC_CHECK((frame.size() == 1));
        STATIC_CHECK((!frame.empty()));
        for(auto s : frame) BOOST_CHECK_EQUAL(s, 3);
    }
    { // list-initializer constructor for unmutable frame
        constexpr single_sample_frame<int> frame = { 4 };

        STATIC_CHECK((*frame.cbegin() == 4));
        STATIC_CHECK((frame.cbegin() != frame.cend()));
        STATIC_CHECK((frame.begin() != frame.end()));
        STATIC_CHECK((frame.size() == 1));
        STATIC_CHECK((!frame.empty()));
    }
}


BOOST_AUTO_TEST_CASE(single_sample_stream_test)
{
    // check concept
    STATIC_CHECK((is_stream<stream<atomic_stream_specifier<int>, dummy_source<initial_context>>>{}));
    STATIC_CHECK((is_stream<detail::constify_t<stream<atomic_stream_specifier<int>, dummy_source<initial_context>>>>{}));
    STATIC_CHECK((stream_specifier<1, 1, int>::IS_STATIC));
    STATIC_CHECK((static_stream_specifier<int, 1, 1>::IS_STATIC));
    STATIC_CHECK((atomic_stream_specifier<int>::IS_STATIC));

    auto frame =dummy_source<initial_context>().drain();

    BOOST_CHECK_EQUAL(*frame.begin(), 1);

#if   (__clang__ || (__GNUC__ > 5))
    constexpr auto cst_frame = detail::constify_t<dummy_source<initial_context>>().cdrain();
    STATIC_CHECK((*cst_frame.cbegin(), 1));
    STATIC_CHECK((*cst_frame.begin(), 1));
#endif
}


BOOST_AUTO_TEST_CASE(port_traits_test)
{
    typedef dummy_source<initial_context> dummy_source;

    STATIC_CHECK((!detail::is_input_port<int>{}));
    STATIC_CHECK((!detail::is_input_port<dummy_source::output_port_type>{}));
    STATIC_CHECK((detail::is_input_port<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>{}));
    STATIC_CHECK((detail::is_input_port<dummy_node<processing_context<void, dummy_source>>::template input_port_type<0> const>{}));
    STATIC_CHECK((!detail::is_output_port<int const>::value));
    STATIC_CHECK((!detail::is_output_port<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>{}));
    STATIC_CHECK((detail::is_output_port<dummy_source::output_port_type const>{}));
    STATIC_CHECK((detail::is_output_port<dummy_node<processing_context<void, dummy_source>>::output_port_type>{}));

    STATIC_CHECK((std::is_same<detail::port_stream_t<dummy_source::output_port_type>, stream<atomic_stream_specifier<int>, dummy_source>>{}));
    STATIC_CHECK((std::is_same<detail::port_stream_t<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>, stream<atomic_stream_specifier<int>, dummy_source>>{}));
    STATIC_CHECK((std::is_same<detail::port_stream_t<dummy_node<processing_context<void, dummy_source>>::template input_port_type<0>>, stream<atomic_stream_specifier<int>, dummy_source>>{}));
    STATIC_CHECK((std::is_same<detail::port_sample_t<dummy_source::output_port_type>, int>{}));
    STATIC_CHECK((std::is_same<detail::port_sample_t<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>, int>{}));
    STATIC_CHECK((std::is_same<detail::port_sample_t<dummy_node<processing_context<void, dummy_source>>::template input_port_type<0>>, int>{}));
    STATIC_CHECK((std::is_same<detail::port_frame_t<dummy_source::output_port_type>, detail::single_sample_frame<int>>{}));
    STATIC_CHECK((std::is_same<detail::port_frame_t<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>, detail::single_sample_frame<int>>{}));
    STATIC_CHECK((std::is_same<detail::port_frame_t<dummy_node<processing_context<void, dummy_source>>::template input_port_type<0>>, detail::single_sample_frame<int>>{}));

    STATIC_CHECK((std::is_same<detail::input_cell_t<dummy_node<processing_context<void, dummy_source>>::template input_port_type<0>>, dummy_source>{}));
    STATIC_CHECK((std::is_same<detail::input_cell_t<dummy_sink<processing_context<void, dummy_source>>::template input_port_type<0>>, dummy_source>{}));
}


BOOST_AUTO_TEST_CASE(input_port_test)
{
    #if ENABLE_FAILURE_TEST
    {
        // will fail because sample-based node cannot be linked to an input frame-based source
        auto processing_branch = apply([](int i0){ return i0*2; }) <<= my_frame_based_source();
        processing_branch.begin();
    }
    #endif
}


BOOST_AUTO_TEST_CASE(cell_traits_test)
{
    using namespace dfp::detail;

    typedef dummy_source<initial_context> dummy_src;

    STATIC_CHECK((!is_upstream<int>::value));
    STATIC_CHECK((!is_upstream<dummy_sink<core::processing_context<void, dummy_src>>>::value));
    STATIC_CHECK((is_upstream<dummy_node<core::processing_context<void, dummy_src>>>::value));
    STATIC_CHECK((is_upstream<dummy_src>::value));
    STATIC_CHECK((is_upstream<dummy_src&>::value));

    STATIC_CHECK((!is_downstream<int>::value));
    STATIC_CHECK((!is_downstream<dummy_src>::value));
    STATIC_CHECK((is_downstream<dummy_sink<core::processing_context<void, dummy_src>>>::value));
    STATIC_CHECK((is_downstream<dummy_node<core::processing_context<void, dummy_src>>>::value));
    STATIC_CHECK((is_downstream<dummy_sink<core::processing_context<void, dummy_src>>&>::value));

    STATIC_CHECK((!is_sink<int>{}));
    STATIC_CHECK((!is_sink<dummy_src>{}));
    STATIC_CHECK((is_sink<dummy_sink<core::processing_context<void, dummy_src>>>{}));
    STATIC_CHECK((is_sink<decltype(make_placeholder<dummy_sink>().implicit_run() <<= make_placeholder<dummy_source>())>{}));
    STATIC_CHECK((!is_cell<int>::value));
    STATIC_CHECK((is_cell<dummy_src>::value));
    STATIC_CHECK((is_cell<dummy_sink<core::processing_context<void, dummy_src>>>::value));
    STATIC_CHECK((is_cell<dummy_node<core::processing_context<void, dummy_src>>>::value));

    STATIC_CHECK((std::is_same<cell_output_port_t<dummy_src>, dummy_src::output_port_type>::value));
    STATIC_CHECK((
        std::is_same<cell_input_port_t< dummy_sink<core::processing_context<void, dummy_src>>>, dummy_sink<core::processing_context<void, dummy_src>>::template input_port_type<0>>::value
    ));

    STATIC_CHECK((source_produces_sample<dummy_src>{}));
    STATIC_CHECK((node_produces_sample<decltype(dfp::make_placeholder<dummy_node>() <<= make_placeholder<dummy_source>())>{}));
    STATIC_CHECK((!node_produces_frame<decltype(dfp::make_placeholder<dummy_node>() <<= make_placeholder<dummy_source>())>{}));
}


BOOST_AUTO_TEST_CASE(extend_operator_test)
{
    using namespace dfp;

    typedef dummy_source<initial_context> dummy_source;

    auto placeholder = make_placeholder<dummy_node>();

    #if ENABLE_FAILURE_TEST
    { // check assertion failure
        // line below will raise a static assertion failure
        // because of unresolved processing graph on right side of <<=
        placeholder <<= placeholder;
        // line below will raise a static assertion failure
        // because of unexpected sink on right side of <<=
        placeholder <<= make_placeholder<dummy_sink>() <<= const_sample(0);
    }
    #endif

    { // link source to placeholder
        STATIC_CHECK((detail::is_upstream<decltype(dummy_source())>{}));
        STATIC_CHECK((detail::is_cell_builder<decltype(placeholder)&>{}));
        STATIC_CHECK((detail::is_convertible_to_cell_builder<decltype(placeholder), std::tuple<>>{}));

        placeholder <<= dummy_source();
    }

    { // link a single source placeholder and build a node
        struct counter { int count;  int operator()(){ return count++; } };

        auto processing_branch = apply([](int in1){ return in1*2; }) <<= apply<counter>();

        apply<counter>().build().acquire_sample();

        STATIC_CHECK((detail::node_produces_sample<decltype(processing_branch)>{}));
        STATIC_CHECK((!detail::node_produces_frame<decltype(processing_branch)>{}));

        auto tree = make_runnable(
            sample_releaser() <<= processing_branch
        );

        BOOST_CHECK_EQUAL(tree.run(), 0);
        BOOST_CHECK_EQUAL(tree.run(), 2);
        BOOST_CHECK_EQUAL(tree.run(), 4);
    }
    // const_sample implicit conversion of reference wrapper
    {
        int x = 0;

        auto processsing = make_placeholder<dummy_node>() <<= std::cref(x);

        x = 1;
        BOOST_CHECK_EQUAL(*processsing.drain().begin(), 1);
    }
    // non-copyable source
    {
        struct nocopy_source
         :  source<initial_context, atomic_stream_specifier<int>, nocopy_source>
        {
            int counter;

            nocopy_source(nocopy_source const&) = delete;
            nocopy_source(nocopy_source&&) : counter(-2) {}
            nocopy_source() : counter(1) {}

            int acquire_sample()
            { return counter++; }

        } in;

        auto processing = make_placeholder<dummy_node>() <<= std::move(in);

        BOOST_CHECK_EQUAL(*processing.drain().begin(), -2);
    }
}


BOOST_AUTO_TEST_CASE(expand_operator_test)
{
    using namespace dfp;

}


template<class Context>
struct increment_source
 :  public  dfp::source<Context, dfp::atomic_stream_specifier<long>, increment_source<Context>>
{
    typedef dfp::source<Context, dfp::atomic_stream_specifier<long>, increment_source<Context>> source_type;

    using source_type::source_type;

    long acquire_sample() { return n++; }

private:
    long n{};
};
decltype(make_placeholder<increment_source>()) constexpr increment()
{ return make_placeholder<increment_source>(); }

struct inc_2ch_source
 :  public  dfp::source<initial_context, dfp::static_stream_specifier<int, 2, 2>, inc_2ch_source>
{
    typedef dfp::source<initial_context, dfp::static_stream_specifier<int, 2, 2>, inc_2ch_source> source_type;

    output_frame_t<source_type> acquire_frame() { return {{n, n++}}; }

private:
    int n;
};

struct fb_source : dfp::source<initial_context, dfp::static_stream_specifier<long, 3>, fb_source>
{
    std::array<long, 3> const& acquire_frame()
    {
        std::for_each(frame.begin(), frame.end(), [this](long& i){ i = n++; });
        return frame;
    }

private:
    long n;
    std::array<long, 3> frame;
};

template <class Inputs>
struct sb_node : dfp::node<Inputs, dfp::atomic_stream_specifier<int>, sb_node<Inputs>>
{
    typedef dfp::node<Inputs, dfp::atomic_stream_specifier<int>, sb_node> node_type;
public:
    using node_type::node_type;
    constexpr output_sample_t<node_type> process_sample(input_sample_t<node_type> const& in) const { return 2*in; }
};

template <class Inputs>
struct add_2in_node
 :          dfp::node<Inputs, dfp::atomic_stream_specifier<int>, add_2in_node<Inputs>>
{
    typedef dfp::node<Inputs, dfp::atomic_stream_specifier<int>, add_2in_node> node_type;

    using node_type::node_type;

    output_sample_t<node_type> process_sample(input_sample_t<node_type, 0> const& in1, input_sample_t<node_type, 1> const& in2) const
    { return in1 + in2; }
};


template<class Inputs>
struct weigh_channel_node
 :          dfp::node<Inputs, dfp::static_stream_specifier<int, 2, 2>, weigh_channel_node<Inputs>>
{
    typedef dfp::node<Inputs, dfp::static_stream_specifier<int, 2, 2>, weigh_channel_node> node_type;

    using node_type::node_type;

    output_frame_t<node_type> process_frame(input_frame_t<node_type> const& in) const
    {
        output_frame_t<node_type> out;
        const std::size_t in_frame_length = in.size();
        const std::size_t in_channel_count = this->input_stream_specifier().get_channel_count();
        auto out_it = out.begin();
        auto in_it = in.cbegin();

        for(std::size_t i = 0 ; i < in_frame_length ; i+= in_channel_count)
            for(std::size_t j = 1 ; j <= in_channel_count ; j++)
                *out_it++ = *(in_it++) * j;

        return out;
    }
};


BOOST_AUTO_TEST_CASE(downstream_traits_test)
{
    using namespace dfp;

    STATIC_CHECK((std::is_same<input_sample_t<sb_node<processing_context<void, increment_source<initial_context>>>>, long>::value));
    STATIC_CHECK((detail::is_node<sb_node<processing_context<void, increment_source<initial_context>>>>::value));
    STATIC_CHECK((detail::is_node<sb_node<processing_context<void, increment_source<initial_context>>>&>::value));
    STATIC_CHECK((detail::is_node<sb_node<processing_context<void, increment_source<initial_context>>> const&>::value));
}


BOOST_AUTO_TEST_CASE(source_test)
{
    using namespace dfp::shortname;
    using namespace std;

    #if ENABLE_FAILURE_TEST
    {
        struct invalid_source : source<atomic_stream_specifier<int>, invalid_source> {};

        auto x = invalid_source(); // misses one of the acquire_xxx method
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        struct frame_source : source<static_stream_specifier<int, 3>, frame_source>
        {
            typedef source<static_stream_specifier<int, 3>, frame_source> source_type;

            output_frame_t<source_type> acquire_frame()
            { return output_frame_t<source_type>{}; }
        };

        auto const x = frame_source();
        x.drain(); // acquire_frame is not const
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        struct frame_source : source<static_stream_specifier<int, 3>, frame_source>
        {
            typedef source<static_stream_specifier<int, 3>, frame_source> source_type;

            output_frame_t<source_type> acquire_frame()
            { return output_frame_t<source_type>{}; }

            output_sample_t<source_type> acquire_sample() const
            { return output_sample_t<source_type>{}; }
        };

        auto const x = frame_source(); // both acquire_frame and acquire_sample are implemented in same source
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = increment_source();
        x.cdrain(); // fails because increment_source does not implement const acquire_sample()
    }
    #endif

    STATIC_CHECK((dfp::detail::supports_acquire_sample<increment_source<initial_context>>{}));
    STATIC_CHECK((!dfp::detail::supports_const_acquire_sample<increment_source<initial_context>>{}));
    STATIC_CHECK((!dfp::detail::source_produces_frame<increment_source<initial_context>>{}));
    STATIC_CHECK((!dfp::detail::source_produces_stream<increment_source<initial_context>>{}));
    STATIC_CHECK((dfp::detail::source_produces_sample<increment_source<initial_context>>{}));
    STATIC_CHECK((dfp::detail::is_source<increment_source<initial_context>>{}));

    typedef decltype(mph<sb_node>() <<= increment()) enclosed_branch_type;

    class source_producing_stream
     :  public  source<initial_context, output_stream_specifier_t<enclosed_branch_type>, source_producing_stream>
                    ::template set_emitter_class_t<enclosed_branch_type>
    {
        typedef source<initial_context, output_stream_specifier_t<enclosed_branch_type>, source_producing_stream>
                    ::template set_emitter_class_t<enclosed_branch_type>
                source_type;

        enclosed_branch_type emitter_;

    public:
        source_producing_stream() : emitter_(mph<sb_node>() <<= increment())
        {}

        output_stream_t<source_type>
        acquire_stream()
        { return output_stream_t<source_type>{ emitter_ }; }
    };

    #if ENABLE_FAILURE_TEST
    {
        auto const x = source_producing_stream();
        x.cdrain(); // fails because source_producing_stream does not implement const acquire_stream()
    }
    #endif

    STATIC_CHECK((dfp::detail::supports_acquire_stream<source_producing_stream>{}));
    STATIC_CHECK((!dfp::detail::supports_const_acquire_stream<source_producing_stream>{}));
    STATIC_CHECK((!dfp::detail::source_produces_frame<source_producing_stream>{}));
    STATIC_CHECK((dfp::detail::source_produces_stream<source_producing_stream>{}));
    STATIC_CHECK((!dfp::detail::source_produces_sample<source_producing_stream>{}));
    STATIC_CHECK((dfp::detail::is_source<source_producing_stream>{}));

    auto processing_branch = source_producing_stream{};
    BOOST_CHECK_EQUAL(*processing_branch.drain().cbegin(), 0);
    BOOST_CHECK_EQUAL(*processing_branch.drain().cbegin(), 2);
    BOOST_CHECK_EQUAL(*processing_branch.drain().cbegin(), 4);
    BOOST_CHECK_EQUAL(*processing_branch.drain().cbegin(), 6);
}


template<class Context>
struct node_producing_stream
 :                      node<Context, as_input0_tag, node_producing_stream<Context>>
                            ::template set_emitter_class_t<context_upstream_t<0, Context>>
{
    typedef typename    node<Context, as_input0_tag, node_producing_stream<Context>>
                            ::template set_emitter_class_t<context_upstream_t<0, Context>>
                        node_type;

    using node_type::node_type;

    output_stream_t<node_type> process_stream(input_stream_t<node_type>&& input) const
    {
        std::cout << std::endl << "node_producing_stream fired" << std::endl;

        return input;
    }
}; // struct node_producing_stream


template<class Input>
struct invalid_node : node<Input, as_input0_tag, invalid_node<Input>>
{
    typedef node<Input, as_input0_tag, invalid_node<Input>> node_type;
    
    using node_type::node_type;
};


template<class Input>
struct unregular_fb_node : node<Input, as_input0_tag, unregular_fb_node<Input>>
{
    typedef node<Input, as_input0_tag, unregular_fb_node<Input>> node_type;
    
    using node_type::node_type;
    
    output_frame_t<node_type> process_frame(input_frame_t<node_type> const& in)
    { std::cout << "unregular frame-based node fired" << std::endl; return in; }
};


template<class Input>
struct unregular_fb_node2 : node<Input, as_input0_tag, unregular_fb_node2<Input>>
{
    typedef node<Input, as_input0_tag, unregular_fb_node2<Input>> node_type;
    
    using node_type::node_type;
    
    output_frame_t<node_type> process_stream(input_stream_t<node_type>&& in)
    { std::cout << "unregular frame-based node 2 fired" << std::endl; return in.drain(); }
};

template<class Input>
struct unregular_sb_node : node<Input, as_input0_tag, unregular_sb_node<Input>>
{
    typedef node<Input, as_input0_tag, unregular_sb_node<Input>> node_type;
    
    using node_type::node_type;
    
    output_sample_t<node_type> process_sample(input_sample_t<node_type> const& in)
    { std::cout << "unregular sample-based node fired" << std::endl; return in; }
};

template<class Input>
struct unregular_sb_node2 : node<Input, as_input0_tag, unregular_sb_node2<Input>>
{
    typedef node<Input, as_input0_tag, unregular_sb_node2<Input>> node_type;
    
    using node_type::node_type;
    
    output_sample_t<node_type> process_stream(input_stream_t<node_type>&& in)
    { std::cout << "unregular sample-based node 2 fired" << std::endl; return *in.drain().begin(); }
};

template <class Inputs>
struct forward_sample_node
 :          dfp::node<Inputs, dfp::atomic_stream_specifier<as_input1_tag>, forward_sample_node<Inputs>>
{
    typedef dfp::node<Inputs, dfp::atomic_stream_specifier<as_input1_tag>, forward_sample_node> node_type;

    using node_type::node_type;

    decltype(1_uc) forward_sample(input_sample_t<node_type, 0> const&, input_sample_t<node_type, 1> const&)
    { return 1_uc; }
};


template <class Inputs>
struct regular_forward_sample_node
 :          dfp::node<Inputs, dfp::atomic_stream_specifier<as_input1_tag>, regular_forward_sample_node<Inputs>>
{
    typedef dfp::node<Inputs, dfp::atomic_stream_specifier<as_input1_tag>, regular_forward_sample_node> node_type;

    using node_type::node_type;

    decltype(1_uc) forward_sample(input_sample_t<node_type, 0> const&, input_sample_t<node_type, 1> const&) const
    { return 1_uc; }
};

template <class Inputs>
struct forward_frame_node
 :          dfp::node<Inputs, as_input0_tag, forward_frame_node<Inputs>>
{
    typedef dfp::node<Inputs, as_input0_tag, forward_frame_node> node_type;

    using node_type::node_type;

    decltype(0_uc) forward_frame(input_frame_t<node_type, 0> const&, input_frame_t<node_type, 1> const&)
    { return 0_uc; }
};

template <class Inputs>
struct regular_forward_frame_node
 :          dfp::node<Inputs, as_input0_tag, regular_forward_frame_node<Inputs>>
{
    typedef dfp::node<Inputs, as_input0_tag, regular_forward_frame_node> node_type;

    using node_type::node_type;

    decltype(0_uc) forward_frame(input_frame_t<node_type, 0> const&, input_frame_t<node_type, 1> const&) const
    { return 0_uc; }
};

template<class Input>
struct regular_sb_node : node<Input, atomic_stream_specifier<double>, regular_sb_node<Input>>
{
    typedef node<Input, atomic_stream_specifier<double>, regular_sb_node<Input>> node_type;
    
    using node_type::node_type;
    
    double const& process_sample(int const& in) const
    { return lookup_table_[in%3]; }

    const double lookup_table_[3] { -3.3, -2.2, -1.1 };
};

template<class Input>
struct regular_fb_node : node<Input, static_stream_specifier<as_input0_tag, 2/*todo: as_input0_tag*/>, regular_fb_node<Input>>
{
    typedef node<Input, static_stream_specifier<as_input0_tag, 2>, regular_fb_node<Input>> node_type;
    
    using node_type::node_type;
    
    output_frame_t<node_type> const& process_frame(input_frame_t<node_type> const&) const
    { return out_; }

    output_frame_t<node_type> const out_ {3, 4};
};

BOOST_AUTO_TEST_CASE(node_test)
{
    using namespace dfp::shortname;
    using namespace std;

    #if ENABLE_FAILURE_TEST
    { // concept check
        // won't compile because there is no trivial binding between sample-based cell and frame-based cell (need to insert a routing::to_single_sample())
        (mph<sb_node>() <<= fb_source()).drain();
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {

        auto x = mph<invalid_node>() <<= 1; // misses one of the process_xxx method
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = mph<unregular_fb_node>() <<= { 1, 2 };
        x.drain(); // process_frame is not const
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = mph<unregular_fb_node2>() <<= { 1, 2 };
        x.drain(); // process_stream is not const
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = mph<unregular_sb_node>() <<= { 1, 2 };
        x.drain(); // process_sample is not const
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = mph<unregular_sb_node2>() <<= { 1, 2 };
        x.drain(); // process_stream is not const
    }
    #endif

    #if ENABLE_FAILURE_TEST
    {
        auto const x = mph<node_producing_stream>() <<= 1;
        x.drain(); // process_stream is not const
    }
    #endif

    STATIC_CHECK((dfp::detail::is_source<decltype(increment().build())>{}));
    STATIC_CHECK((dfp::detail::is_node<decltype(mph<sb_node>() <<= increment())>{}));
    STATIC_CHECK((dfp::detail::is_node<decltype(mph<add_2in_node>().pi(increment()) <<= increment())>{}));
    STATIC_CHECK((dfp::detail::is_node<decltype(mph<add_2in_node>().pi(increment()) <<= mph<sb_node>() <<= increment())>{}));

    { // sample-based node bound to sample-based source
        auto processing_tree = sample_releaser() <<= mph<add_2in_node>().pi(increment()) <<= mph<sb_node>() <<= increment();
        BOOST_CHECK_EQUAL(processing_tree.run(), 0);
        BOOST_CHECK_EQUAL(processing_tree.run(), 3);
    }
    { // 2-channels node
        auto processing_tree = frame_releaser() <<= mph<weigh_channel_node>() <<= inc_2ch_source();
        auto out_frame_it = processing_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*out_frame_it++, 0);
        BOOST_CHECK_EQUAL(*out_frame_it, 0);
        out_frame_it = processing_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*out_frame_it++, 1);
        BOOST_CHECK_EQUAL(*out_frame_it, 2);
        out_frame_it = processing_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*out_frame_it++, 2);
        BOOST_CHECK_EQUAL(*out_frame_it, 4);
        out_frame_it = processing_tree.run().cbegin();
        BOOST_CHECK_EQUAL(*out_frame_it++, 3);
        BOOST_CHECK_EQUAL(*out_frame_it, 6);
    }
    { // frame-based node bound to sample-based source
    }
    { // node producing stream
        auto x = mph<node_producing_stream>() <<= 1;

        STATIC_CHECK((dfp::detail::node_produces_stream<decltype(x)>{}));
        STATIC_CHECK((dfp::detail::supports_const_process_stream<decltype(x)>{}));
        STATIC_CHECK((!dfp::detail::node_produces_sample<decltype(x)>{}));
        STATIC_CHECK((!dfp::detail::node_produces_frame<decltype(x)>{}));
        STATIC_CHECK((dfp::detail::supports_renew_stream<typename decltype(x)::output_port_type::derived>{}));

        auto processing_tree = make_runnable(
            sample_releaser() <<= x
        );

        BOOST_CHECK_EQUAL(processing_tree.run(), 1);
        BOOST_CHECK_EQUAL(processing_tree.run(), 1);
    }
    { // forward_sample
        auto x = mph<forward_sample_node>() += const_sample(-1L)
                                            |= const_sample(false)
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 1));
        STATIC_CHECK((dfp::detail::supports_forward_sample<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_sample_t<>, long>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), -1L);
    }
#if ENABLE_FAILURE_TEST
    { // missing const forward_sample
        auto const x = mph<forward_sample_node>()   += const_sample(-1L)
                                                    |= const_sample(false)
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 1));
        STATIC_CHECK((dfp::detail::supports_forward_sample<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_sample_t<>, long>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), -1L); // const forward_sample missing
    }
#endif
    { // regular forward_sample
        DFP_IF(DFP_CPP14_SUPPORT)(constexpr,)
        auto const x = mph<regular_forward_sample_node>()   += const_sample(-1L)
                                                            |= const_sample(false)
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 1));
        STATIC_CHECK((dfp::detail::supports_const_forward_sample<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_sample_t<>, long>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), -1L);
    }
    { // forward_sample
        auto x = mph<regular_forward_sample_node>() += mph<regular_sb_node>() <<= const_sample(1)
                                                    |= const_sample(false)
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 1));
        //TODO:
//        typename decltype(x)::output_port_type::result_sample_t<> y = "";
        //STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_sample_t<>, long const&>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), -2.2);
    }
    { // forward_frame
        auto x = mph<forward_frame_node>()  += routing::const_frame({-1L, -2L})
                                            |= routing::const_frame({false, true})
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 0));
        STATIC_CHECK((dfp::detail::supports_forward_frame<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_frame_t<>, std::array<bool, 2>>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), false);
    }
#if ENABLE_FAILURE_TEST
    { // missing const forward_frame
        auto const x = mph<forward_frame_node>()    += routing::const_frame({-1L, -2L})
                                                    |= routing::const_frame({false, true})
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 0));
        STATIC_CHECK((dfp::detail::supports_forward_frame<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_frame_t<>, std::array<bool, 2>>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), false); // const forward_frame missing
    }
#endif
    { // regular forward_frame
        DFP_IF(DFP_CPP14_SUPPORT)(constexpr,)
        auto const x = mph<regular_forward_frame_node>()    += routing::const_frame({-1L, -2L})
                                                            |= routing::const_frame({false, true})
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 0));
        STATIC_CHECK((dfp::detail::supports_const_forward_frame<decltype(x)>{}));
        STATIC_CHECK((std::is_same<typename decltype(x)::output_port_type::result_frame_t<>, std::array<bool, 2>>{}));
        BOOST_CHECK_EQUAL(*x.drain().begin(), false);
    }
    { // forward_frame
        DFP_IF(DFP_CPP14_SUPPORT)(constexpr,)
        auto const x = mph<regular_forward_frame_node>()    += mph<regular_fb_node>() <<= routing::const_frame({1, 2})
                                                            |= routing::const_frame({-1.0f, -2.0f})
        ;

        STATIC_CHECK((typename decltype(x)::output_port_type::get_forwarded_input<>{} == 0));
        BOOST_CHECK_EQUAL(*x.drain().begin(), -1.0f);
    }
    #if 0
    { // node producing stream connected to sample-based node
        auto const x = mph<add_2in_node>()    += 2.1
                                        |= mph<node_producing_stream>() <<= 1;

        BOOST_CHECK_EQUAL(*x.drain().begin(), 3.1);
        BOOST_CHECK_EQUAL(*x.drain().begin(), 3.1);
    }
    #endif
}


BOOST_AUTO_TEST_CASE(graph_designer_test)
{
    struct random
    { double operator()() const { return std::rand(); } };

    struct add
    { long operator()(long const& in0, long const& in1){ return in0 + in1; } };

    auto zero = const_sample(0);
    auto sample_source = zero.build();

    graph_designer<>::instance().extend(1, make_placeholder<dummy_sink>());
    make_placeholder<dummy_sink>() <<= 1;

    graph_designer<>::instance().expand(detail::processing_graph<decltype(zero.build())>(zero.build()), zero);
    zero |= detail::processing_graph<decltype(zero.build())>(zero.build());
    graph_designer<>::instance().expand(zero, zero);
    zero |= zero.build();
    graph_designer<>::instance().expand(detail::processing_graph<decltype(sample_source) const&>(sample_source), apply<random>());
    apply<random>() |= detail::processing_graph<decltype(sample_source) const&>(sample_source);
    graph_designer<>::instance().expand(detail::processing_graph<decltype(sample_source) const&>(sample_source), random{});
    random{} |= detail::processing_graph<decltype(sample_source) const&>(sample_source);
    apply<random>() |= apply<random>().build();
    STATIC_CHECK((detail::is_cell_builder<decltype(apply<random>())>{}));
    graph_designer<>::instance().expand(apply<random>().build(), apply<random>());
    graph_designer<>::instance().expand(apply<random>(), apply<random>());
    apply<random>() |= apply<random>();
    graph_designer<>::instance().expand(random{}, random{});
    random{} |= random{};
    graph_designer<>::instance().expand(1, zero);
    zero |= 1;

    (const_sample(-1L) |= const_sample(false)).collect(apply<add>());
    graph_designer<>::instance().collect(const_sample(-1L) |= const_sample(false), apply<add>());
    apply<add>() += const_sample(-1L) |= const_sample(false);

    // implicit cast from functor to node with extend operator
    graph_designer<>::instance().extend(const_sample(1), [](int x){ return -x; });

    // implicit cast from functor to node with collect operator
    graph_designer<>::instance().collect(const_sample(1) |= const_sample(0), add{});

    using namespace dfp::routing;
    {
        // implicit cast from std::reference_wrapper_t<> to mutable_sample
        using namespace dfp;

        int x = 20;

        auto running_tree = graph_designer<>::instance().extend(std::cref(x), sample_releasing());

        BOOST_CHECK_EQUAL(x, running_tree.run());
    }
}


BOOST_AUTO_TEST_CASE(apply_source_test)
{
    using namespace dfp;
    using namespace dfp::shortname;

    {
        struct ramp
        {
            constexpr ramp(int period) : period_(period), current_(0) {}

            DFP_MUTABLE_CONSTEXPR int operator()() { return current_++ % period_; }

        private:
            const int   period_;
            int         current_;
        };

        auto processing_branch = apply<ramp>(3).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);

#if(__cplusplus >= 201703L) //TODO: should work also in C++14
#if !defined(__GNUC__) || defined(__clang__)
        { //gcc has some problem to compile line below
            STATIC_CHECK((*apply<ramp>(3).build().drain().begin() == 0));
        }
#endif
#endif
    }
    { // create a frame-based "apply source" given a lambda
        auto processing_branch = apply_f([]{ return std::experimental::make_array(1, 2, 3); });
        BOOST_CHECK_EQUAL(processing_branch.build().cdrain()[0], 1);
        BOOST_CHECK_EQUAL(processing_branch.build().cdrain()[1], 2);
        BOOST_CHECK_EQUAL(processing_branch.build().cdrain()[2], 3);
    }
    { // create a sample-based "apply source" given a lambda
        auto processing_branch = apply([]{ return 4; });
        BOOST_CHECK_EQUAL(*processing_branch.build().cdrain().begin(), 4);
    }
    { // create a frame-based "apply source" given a mutable function-object
        struct my_frame_based_producer
        {
            std::array<int, 3> operator()() { arr_[0] -= 1; return arr_; }
            private: std::array<int, 3> arr_;
        };
        auto processing_source = apply_f(my_frame_based_producer{}).build();
        BOOST_CHECK_EQUAL(processing_source.drain()[2], 0);
        BOOST_CHECK_EQUAL(processing_source.drain()[1], 0);
        BOOST_CHECK_EQUAL(processing_source.drain()[0], -3);

        //OR
        auto processing_sourc2 = apply_f<my_frame_based_producer>().build();
        BOOST_CHECK_EQUAL(processing_sourc2.drain()[2], 0);
        BOOST_CHECK_EQUAL(processing_sourc2.drain()[1], 0);
        BOOST_CHECK_EQUAL(processing_sourc2.drain()[0], -3);
    }
    {
        struct random
        { double operator()() const { return std::rand(); } };

        auto processing_branch = apply<random>().build();

        STATIC_CHECK((detail::is_source<decltype(processing_branch)>::value));
        STATIC_CHECK((output_stream_t<decltype(processing_branch)>::IS_ATOMIC));

        BOOST_CHECK_GT(*processing_branch.drain().begin(), -1);
    }
    {
        struct three
        { constexpr int operator()() const { return 3; } };

        auto processing_branch = apply<three>().build();

        STATIC_CHECK((detail::is_source<decltype(processing_branch)>::value));
        STATIC_CHECK((output_stream_t<decltype(processing_branch)>::IS_ATOMIC));

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 3));
#endif // #if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST

#if(__cplusplus >= 201703L && (defined(__clang__) || (__GNUC__ > 9)))
        STATIC_CHECK((*processing_branch.drain().begin() == 3));
#endif
    }
}


BOOST_AUTO_TEST_CASE(const_sample_test)
{
    using namespace dfp;

    { // wrapping value
        auto processing_branch = const_sample(1).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    { // constexpr support
        constexpr auto const processing_branch = const_sample(2).build();
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == 2));
        STATIC_CHECK((*const_sample(0).build().cdrain().cbegin() == 0));
    }
#endif //#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    { // constexpr support only since C++17
#if (__cplusplus >= 201703L)
        constexpr auto processing_branch = const_sample(0).build();

        STATIC_CHECK((*processing_branch.drain().begin() == 0));
        STATIC_CHECK((*const_sample(0).build().drain().begin() == 0));
#endif
    }
    // wrapping const reference
    {
        double value = 2.2;
        const double& ref = value;
        auto processing_branch = const_sample(ref).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2.2);

        value = 1.1;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2.2); // no change as const_sample shall copy its input
    }
    // wrapping reference
    {
        double value = 2.2;
        auto processing_branch = const_sample(value).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2.2);

        value = 1.1;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2.2);
    }
    // r-value reference
    {
        auto processing_branch = const_sample(std::sin(2)).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), std::sin(2));
    }
    // initializer list
    {
        auto processing_branch = const_sample(1, 3, 2).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
    // initializer list of reference
    {
        int one = 1; int two = 2; int three = 3;
        auto processing_branch = const_sample(one, two, three).build();
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 2);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 3);
        one = 0;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
    // l-value ref implicit conversion
    {
        double const value = 2.2;
        auto processing_branch = detail::cellify(value);
        STATIC_CHECK((std::is_same<decltype(processing_branch), detail::sample_source<initial_context, double const>>{}));
    }
    // integral_constant
    { // TODO: as it is compiler dependent, this test should only be enabled for some given compilers
#if (__cplusplus >= 201402L) // auto in lambda is only supported since C++14
        auto processing_branch1 = apply([](auto const& in){ return in + 1; }) <<= const_sample(size_t_const<3>{});
#if   (__clang__ || (__GNUC__ > 5))
        STATIC_CHECK((sizeof(processing_branch1) == 1));
#elif (__GNUC__ <= 5)
        STATIC_CHECK((sizeof(processing_branch1) == 2));
#endif
#endif //(__cplusplus >= 201402L)
    }
}


BOOST_AUTO_TEST_CASE(running_tree_test)
{
    {
        std::cout << "post-increment usage on iterable 'running_tree'" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        auto tree = make_running(
            sample_releaser() <<= sample_from(1, 2, 3, 4)
        );

        std::cout << *tree++ << ", " << *tree++ << ", " << *tree++ << ", " << *tree++ << std::endl;
        // expected result: 1, 2, 3, 4

        BOOST_CHECK_EQUAL(1, *tree++);
        BOOST_CHECK_EQUAL(2, *tree++);
        BOOST_CHECK_EQUAL(3, *tree++);
        BOOST_CHECK_EQUAL(4, *tree++);
        std::cout << std::endl;
    }
}


class product_by_int
{
    int m_;

public:
    constexpr product_by_int(int m) : m_(m) {}

    template<class T>
    constexpr auto operator()(T in) const -> decltype(in * m_) const { return in * m_; }
};


// TODO: for sake of completeness some OutFrame type should be defined in sort-operator()
// class OutFrame = typename detail::frame_traits<InFrame>::template set_sample<
//     std::remove_const_t<typename detail::frame_traits<InFrame>::sample_type>
// >::type
// TODO: replace with some helper remove_const_on_sample<>
///@ [frame-based "apply node & source" given a function-object type and lambda]
struct median
{
    struct sort
    {
        template<class Frame>
        Frame operator()(Frame const& in) const
        {
            Frame out = in; int n = in.size(); const int N = n;
            for(int& s : out) for (int i = N - n--; i < N; i++) if (s > out[i]) std::swap(s, out[i]);
            return out;
        }
    };
    struct get_middle
    {
        template<class Frame>
        std::array<int, 1> operator()(Frame const& in) const
        { return std::experimental::make_array(in[(in.size()-1) / 2]); }
    };
    template<class Frame>
    std::array<int, 1> operator()(Frame const& in) const
    { return (apply_f<get_middle>() <<= apply_f<sort>() <<= routing::const_frame(in)).cdrain(); }

    template<class T = median>
    static void example()
    {
        using namespace std::experimental;

        auto processing_branch = apply_f<T>() <<= apply_f([]{ return make_array(10, 4, 1, 1, 2, 2, 3); });

        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        // expected result : 2
    }
};
///@ [frame-based "apply node & source" given a function-object type and lambda]
struct acc
{
    template<class Frame>
    std::array<int, 1> operator()(Frame const& in)
    { for(int a : in) acc_ += a; return std::experimental::make_array(acc_); }
private:
    int acc_;
};

int foo(int in)
{ return in*in; }

BOOST_AUTO_TEST_CASE(apply_test)
{
    using namespace dfp;

    struct negate
    { constexpr int operator()(int const& in) const { return -in; } };

    struct check_equal
    {
        check_equal(int expectation) : expectation_(expectation) {}
        void operator()(int const& in) { BOOST_CHECK_EQUAL(in, expectation_); }
    private:
        int expectation_;
    };

#if !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    {
        ///@ [sample-based "apply node" given a function-object type]
        // create a sample-based "apply node" given a function-object type
        struct negate
        { constexpr int operator()(int in) const { return -in; } };

        constexpr auto processing_branch = apply<negate>() <<= 1;

        std::cout << *processing_branch.cdrain().cbegin() << std::endl;
        // expected result : -1
        ///@ [sample-based "apply node" given a function-object type]

        processing_branch.cdrain();
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == -1));
    }
    {
        ///@ [sample-based "apply sink" given a function-object type]
        // create a sample-based "apply sink" given a function-object type
        struct acc
        {
            acc(int initial) : acc_(initial) {}
            void operator()(int const& in) { acc_ += in ; std::cout << acc_ << ", "; }
        private:
            int acc_;
        };

        auto processing_tree = apply<run_for, acc>(run_for_args{3}, -1) <<= 1;
        processing_tree.run();
        // expected result : 0, 1, 2, 
        ///@ [sample-based "apply sink" given a function-object type]
        std::cout << std::endl;
    }
    {
        //TODO: replace tuple with proxy : aka apply<check_equal>().run_for(1) / running_for(1) -> auto start
        auto processing_tree = apply<run_for, check_equal>(run_for_args{1}, -1) <<= apply<negate>() <<= 1;
        processing_tree.run();
    }
    { // create "apply node" given a function-object as callable
        constexpr auto processing_branch = apply(negate{}) <<= 1;
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == -1));
    }
    {
        struct
        {
            std::vector<int> operator()(){ return {3, 1, 2}; }
            std::size_t output_frame_length() const { return 3; }
        } input;
                
        auto processing_tree = make_runnable(
            frame_releaser() <<= apply_f<median::sort>()
                <<= apply_f(input)
//TODO          <<= apply_f([](){ return std::vector<int>{{3, 1, 2}}; }).do_on_init([](auto it){ return it.stream_spec.set_output_frame_length(3); })
        );

        BOOST_CHECK_EQUAL(processing_tree.input_stream_specifier().get_frame_length(), 3);

        auto frame = processing_tree.run();
        auto frame_it = frame.cbegin();

        BOOST_CHECK_EQUAL(*frame_it++, 1);
        BOOST_CHECK_EQUAL(*frame_it++, 2);
        BOOST_CHECK_EQUAL(*frame_it++, 3);
    }
    {
        // create a frame-based "apply node" given a regular function-object type
        auto processing_branch = apply_f<median>() <<= apply_f([](){ return std::experimental::make_array(2, 1, 3); });

        processing_branch.cdrain();
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), 2);
        median::example();
    }
    {
        // create a frame-based "apply node" given a regular function-object type and a dynamic extent frame as input
        struct {
            std::vector<int> operator()() const
            { return {1, 2, 3}; }
            
            std::size_t output_frame_length() const
            { return 3; }

        } input;

        auto processing_branch = apply_f<median>() <<= apply_f(input);

        processing_branch.cdrain();
        BOOST_CHECK_EQUAL(*processing_branch.cdrain().cbegin(), 2);
    }
    { // create a frame-based "apply node" given a mutable function-object type
        auto processing_branch = apply_f<acc>() <<= apply_f([](){ return std::experimental::make_array(5, 2, 3); });
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 5+2+3);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), (5+2+3)*2);
    }
    { // create "apply sink" given a function-object as callable
        auto processing_tree = apply<run_for>(check_equal{-1}, 1) <<= apply<negate>() <<= 1;
        processing_tree.run();
    }
    { // return type auto detection and function-object construction forwarding
        constexpr auto processing_branch = apply<product_by_int>(2) <<= 1.1;
        STATIC_CHECK((*processing_branch.cdrain().cbegin() == (2 * 1.1)));
    }
#endif // !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    { // same as previously but using lambda as callable rather than function object
      // constexpr won't work here in C++11
      //TODO: try with later C++ version
        auto processing_branch = apply([](int in){ return -in;}) <<= 1;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), -1);
    }
    /* TODO
    { // implicit conversion of lambda into apply node
        auto processing_branch = [](int in){ return -in; } <<= apply([](){ return 1; });
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), -1);
    }
    { // implicit conversion of lambda into apply node before "collect" operator
                auto processing_branch =
                    make_placeholder<forward_sample_node>() += [](int in){ return -in; } <<= 1
                                                            |= apply([](){ return 1; });
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), -1);
    }
     */
    /*TODO
    { // double implicit conversion of lambda into apply node
        auto processing_branch = [](int in){ return -in;} <<= [](int in){ return -in;} <<= apply([](){ return 1; });
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
    }
    */
    {
        // frame-based apply node
//TODO: investigate why auto does not work as expected
#if 0 && DFP_CPP14_SUPPORT
            apply_f<run_for>([](auto const&){}, 1)
            <<= apply_f([](auto const& in){ for(auto i : in) std::cout << i << ", "; std::cout << std::endl; return in; })
            <<= apply_f([](){ return std::experimental::make_array(1, 5, 2, 3); });
        });
#endif

    }
    {
        using namespace dfp;

        std::cout << "implicit conversion from C-function to apply node" << std::endl;
        std::cout << "---" << std::endl;

        auto tree = sample_releasing() <<= foo <<= sample_from(2);

        std::cout << tree.get_result() << std::endl;
        // expected result: 4
    }
}


BOOST_AUTO_TEST_CASE(execute_test)
{
    using namespace dfp::routing;

    {
        ///@ [sample-based "execute node" given a function-object type]
        std::cout << "sample-based 'execute node' given a function-object type" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        // some callable
        struct accumulate
        {
            accumulate(int& accumulator) : accumulator_(accumulator)
            {}

            void operator()(int in)
            { accumulator_ += in; }

        private:
            int& accumulator_;
        };

        int accumulator = 0;
        auto tree = sample_releasing() <<= execute<accumulate>(std::ref(accumulator)) <<= const_sample(1, 2, 3, 4);

        std::cout << tree.get_result() << ", " << accumulator << std::endl;
        // expected result : 1, 1
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 2, 3
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 3, 6
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 4, 10
        ///@ [sample-based "execute node" given a function-object type]

        BOOST_CHECK_EQUAL(accumulator, 10);

        BOOST_CHECK_EQUAL(tree.run(), 1);
        BOOST_CHECK_EQUAL(tree.run(), 2);
        BOOST_CHECK_EQUAL(tree.run(), 3);
        BOOST_CHECK_EQUAL(tree.run(), 4);

        std::cout << std::endl;
    }
    {
        ///@ [frame-based "execute node" given a function-object type]
        std::cout << "frame-based 'execute node' given a function-object type" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        // some callable
        struct accumulate
        {
            accumulate(int& accumulator) : accumulator_(accumulator)
            {}

            void operator()(upg::span<const int> const& in_frame)
            { std::for_each(in_frame.cbegin(), in_frame.cend(), [&](int in_sample){ accumulator_ += in_sample; }); }

        private:
            int& accumulator_;
        };

        int accumulator = 0;
        frame_releasing() <<= execute_f<accumulate>(std::ref(accumulator)) <<= const_frame({1, 2, 3, 4});

        std::cout << accumulator << std::endl;
        // expected result : 10
        ///@ [frame-based "execute node" given a function-object type]

        BOOST_CHECK_EQUAL(accumulator, 10);
        std::cout << std::endl;
    }
    {
        ///@ [sample-based 'execute node' given a lambda]
        std::cout << "sample-based 'execute node' given a lambda" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        int accumulator = 0;
        auto tree = sample_releasing() <<= execute([&](int in){ accumulator += in; }) <<= const_sample(1, 2, 3, 4);

        std::cout << tree.get_result() << ", " << accumulator << std::endl;
        // expected result : 1, 1
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 2, 3
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 3, 6
        std::cout << tree.run() << ", " << accumulator << std::endl;
        // expected result : 4, 10
        ///@ [sample-based 'execute node' given a lambda]

        BOOST_CHECK_EQUAL(accumulator, 10);

        BOOST_CHECK_EQUAL(tree.run(), 1);
        BOOST_CHECK_EQUAL(tree.run(), 2);
        BOOST_CHECK_EQUAL(tree.run(), 3);
        BOOST_CHECK_EQUAL(tree.run(), 4);

        std::cout << std::endl;
    }
    {
        ///@ [frame-based 'execute node' given a lambada]
        std::cout << "frame-based 'execute node' given a lambda" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        int accumulator = 0;

        frame_releasing()
        <<= execute_f([&](upg::span<const int> const& in_frame){
            std::for_each(in_frame.cbegin(), in_frame.cend(), [&](int in_sample){ accumulator += in_sample; });
        })
        <<= const_frame({1, 2, 3, 4});

        std::cout << accumulator << std::endl;
        // expected result : 10
        ///@ [frame-based 'execute node' given a lambada]

        BOOST_CHECK_EQUAL(accumulator, 10);
        std::cout << std::endl;
    }
    {
        ///@[callable implicit conversion to 'execute' node]
        std::cout << "callable implicit conversion to 'execute' node" << std::endl;
        std::cout << "---" << std::endl;

        using namespace dfp;

        // some callable
        struct accumulate
        {
            accumulate(int& accumulator) : accumulator_(accumulator)
            {}

            void operator()(int in)
            { accumulator_ += in; }

        private:
            int& accumulator_;
        };

        int accumulator = 0;
        auto tree = sample_releasing() <<= accumulate{accumulator} <<= [&](int){ std::cout << "cumul: " << accumulator << ", "; } <<= const_sample(1);
        // is same as
        // auto tree = sample_releasing() <<= execute(accumulate{accumulator}) <<= execute([&](int){ std::cout << "cumul: " << accumulator << ", "; }) <<= const_sample(1);

        tree.run(); tree.run(), tree.run();
        // expected result: cumul: 0, cumul: 1, cumul: 2, cumul: 3, 

        ///@[callable implicit conversion to 'execute' node]
        std::cout << std::endl;
        BOOST_CHECK_EQUAL(tree.get_result(), 1);
        BOOST_CHECK_EQUAL(accumulator, 4);
        BOOST_CHECK_EQUAL(tree.run(), 1);
        BOOST_CHECK_EQUAL(accumulator, 5);
        BOOST_CHECK_EQUAL(tree.run(), 1);
        BOOST_CHECK_EQUAL(accumulator, 6);

        std::cout << std::endl << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(impure_function_node_test)
{
    using namespace dfp;

    struct xor_previous
    {
        bool operator()(bool in) { bool prev = previous_; previous_ = in; return prev ^ in; }
    private:
        bool previous_;
    };

    { // type traits
        STATIC_CHECK((detail::is_upstream<decltype(apply<xor_previous>() <<= 1)>{}));
    }
    { // 1st way to create "function node" with help of apply
        auto processing_branch = apply<xor_previous>() <<= 1;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
    }
    { // 2nd way to create "function node" using callable as parameter in apply
        auto processing_branch = apply(xor_previous{}) <<= 1;
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 1);
        BOOST_CHECK_EQUAL(*processing_branch.drain().begin(), 0);
    }
}


template<class Input>
struct tagged_node
    :       node<Input, atomic_stream_specifier<as_input0_tag>, tagged_node<Input>>
{
    typedef node<Input, atomic_stream_specifier<as_input0_tag>, tagged_node> node_type;
    using node_type::node_type;

    output_sample_t<node_type> process_sample(input_sample_t<node_type> const&)
    { return 0; }
};
static auto tagged() ->
decltype(make_placeholder<tagged_node>())
{ return make_placeholder<tagged_node>(); }

template<class Context, class Tag>
struct ramp_source
 :  public  source<Context, atomic_stream_specifier<output_sample_t<typename Context::template at_tag_t<Tag>>>, ramp_source<Context, Tag>>
{
    typedef source<Context, atomic_stream_specifier<output_sample_t<typename Context::template at_tag_t<Tag>>>, ramp_source> source_type;
    using source_type::source_type;

    output_sample_t<source_type> acquire_sample()
    { return counter++; }

private:
    output_sample_t<source_type> counter;
};
template<class Tag>
auto ramp() ->
decltype(make_placeholder<std::tuple<Tag>, ramp_source>())
{ return make_placeholder<std::tuple<Tag>, ramp_source>(); }

BOOST_AUTO_TEST_CASE(stream_tag_test)
{
    { // check retrieval of sample type identified by tag
        struct node0_tag : custom_tag {} NODE0;
        struct node1_tag : custom_tag {} NODE1;
        struct node2_tag : custom_tag {} NODE2;
        struct node3_tag : custom_tag {} NODE3;
        auto to_double = [](int in) -> double { return (double) in; };
        auto add = [](double in0, double in1){ return in0 + in1; };

        auto processing_branch =
            add += tagged().tag(NODE3) <<= ramp<node2_tag>()
                |= add  += tagged().tag(NODE2) <<= to_double <<= tagged().tag(NODE1) <<= ramp<node0_tag>()
                        |= tagged().tag(NODE0) <<= 3
        ;

        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)::at_tag_t<node0_tag>>,     int>{}));
        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)::at_tag_t<node1_tag>>,     int>{}));
        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)::at_tag_t<node2_tag>>,     double>{}));
        STATIC_CHECK((std::is_same<output_sample_t<decltype(processing_branch)::at_tag_t<node3_tag>>,     double>{}));
    }
}


BOOST_AUTO_TEST_CASE(frame_concept_test)
{
    using namespace dfp::detail;

    STATIC_CHECK((!models_frame_concept<int>{}));
    STATIC_CHECK((models_frame_concept<single_sample_frame<int>>{}));
    STATIC_CHECK((models_frame_concept<std::vector<int>>{}));
    STATIC_CHECK((models_frame_concept<std::array<int, 0>>{}));
    STATIC_CHECK((models_frame_concept<upg::span<int>>{}));
}


template<class UpstreamGraph>
struct noncopyable_source
 :  public  source<UpstreamGraph, atomic_stream_specifier<int>, noncopyable_source<UpstreamGraph>>
{
    typedef source<UpstreamGraph, atomic_stream_specifier<int>, noncopyable_source> source_type;

    noncopyable_source(noncopyable_source const&) = delete;
    noncopyable_source(noncopyable_source&&) = default;

    using source_type::source_type;
    constexpr int acquire_sample() const { return 1; }
};

BOOST_AUTO_TEST_CASE(processing_graph_test)
{
    using namespace dfp;
    using namespace dfp::detail;

    { // downmost cell is a rvalue reference source
        constexpr auto graph = processing_graph<decltype(const_sample(0).build())>(const_sample(0).build());

        STATIC_CHECK((is_source<decltype(graph)>{}));
        STATIC_CHECK((std::is_same<
            std::remove_cvref_t<decltype(const_sample(0).build())>,
            std::remove_cvref_t<decltype(std::get<0>(graph.get_downmost_cells()))>
        >{}));

#if (__clang__ || (__GNUC__ > 7))
        STATIC_CHECK((*std::get<0>(graph.get_downmost_cells()).cdrain().cbegin() == 0));
#endif
        BOOST_CHECK_EQUAL(*std::get<0>(graph.get_downmost_cells()).cdrain().cbegin(), 0);

#if (__clang__ || (__GNUC__ > 7))
        STATIC_CHECK((*graph.cdrain().cbegin() == 0));
#endif
        BOOST_CHECK_EQUAL(*graph.cdrain().cbegin(), 0);

        // extend initial source with some node
        constexpr auto graph2 = graph.extend(make_placeholder<constexpr_node>()); // call processing_graph#1::extend()#1
        STATIC_CHECK((is_node<decltype(graph2)>{}));

        // extend initial source with some node
//        graph.collect(make_placeholder<constexpr_node>()); // call processing_graph#1::extend()#1
        graph.extend(make_placeholder<constexpr_node>()); // call processing_graph#1::extend()#1

#if (__clang__ || (__GNUC__ > 7))
        STATIC_CHECK((*graph2.get_downmost_cell().cdrain().cbegin() == 2));
#endif
        BOOST_CHECK_EQUAL(*graph2.get_downmost_cell().cdrain().cbegin(), 2);

        // extend branch with some other node
        constexpr auto graph3 = graph2
            .extend(make_placeholder<constexpr_node>()) // processing_graph#2::call extend()#1
            .collect(make_placeholder<constexpr_node>())
        ;
        STATIC_CHECK((is_node<decltype(graph3)>{}));

// some issue here with gcc and constexpr support
#if (__clang__ /*|| (__GNUC__ > 7)*/)
        STATIC_CHECK((*graph3.cdrain().cbegin() == 6));
#endif
        BOOST_CHECK_EQUAL(*graph3.cdrain().cbegin(), 6);
    }
    { // downmost cell is a non-copyable source
        constexpr auto noncopyable = make_placeholder<std::tuple<>, noncopyable_source>();

        STATIC_CHECK((is_source<decltype(processing_graph<decltype(noncopyable.build())>(noncopyable.build()))>{}));
        STATIC_CHECK((std::is_same<
            std::remove_cvref_t<decltype(noncopyable.build())>,
            std::remove_cvref_t<decltype(std::get<0>(processing_graph<decltype(noncopyable.build())>(noncopyable.build()).get_downmost_cells()))>
        >{}));
    }
    { // "collecting" non-copyable graph
        constexpr auto noncopyable = make_placeholder<std::tuple<>, noncopyable_source>();
        auto && graph = processing_graph<decltype(const_sample(0).build())>(const_sample(0).build());

        std::move(graph)
            .expand(noncopyable)
            .collect(apply([](int const& i0, int const& i1){ return i0+i1; }));
    }
    { // terminal is a const-lvalue reference source
        constexpr auto source = const_sample(0).build();
        constexpr auto graph = processing_graph<decltype(source) const&>(source);

        auto graph2 = graph
            .expand(const_sample(1))
            // extend 1st branch with some node
            .extend(apply([](int const& i){ return i+1; }))
            // expand graph with a 2nd branch
            .expand(const_sample(2))
            // extend 2nd branch with some node
            .extend(apply([](int const& i){ return i+3; }))
            // expand graph with a 3rd branch
            .expand(const_sample(3))
            // extend 3rd branch with some node
            .extend(apply([](int const& i){ return i+3; }))
            .extend(apply([](int const& i){ return i*5; }))
            .collect(apply([](int const& i0, int const& i1, int const& i2, int const& i3){ return i0+i1+i2+i3; }))
        ;

        BOOST_CHECK_EQUAL(*graph2.cdrain().cbegin(), 0+(1+1)+(2+3)+((3+3)*5));
    }

    { // terminal is a lvalue reference source
        auto source = const_sample(0).build();
        auto graph = detail::processing_graph<decltype(source) const&>(source);

        STATIC_CHECK((is_source<decltype(graph)>{}));

        BOOST_CHECK_EQUAL(*graph.cdrain().cbegin(), 0);

        // extend initial source with some node
        auto graph2 = graph.extend(make_placeholder<constexpr_node>()); // call processing_graph#1::extend()#1
        STATIC_CHECK((is_node<decltype(graph2)>{}));
    }

    { // terminal is a mutable lvalue reference source not copy-constructible
        struct no_copy_construct : decltype(const_sample(0).build())
        {
            typedef decltype(const_sample(0).build()) base;
            no_copy_construct() : base(const_sample(0).build()) {}
            no_copy_construct(no_copy_construct const&) = delete;
            no_copy_construct(no_copy_construct&&) = default;
        };

        no_copy_construct source{};
        // line below will raise a static error
        //auto graph = processing_graph<decltype(source) const&>(source);
        auto graph = processing_graph<decltype(source)>(std::move(source));

        STATIC_CHECK((is_source<decltype(graph)>{}));

        BOOST_CHECK_EQUAL(*graph.drain().begin(), 0);

        // extend initial source with some node
    #if ENABLE_FAILURE_TEST
        /// will fail because graph is not copyable
        auto graph2 = graph.extend(make_placeholder<constexpr_node>());
    #else
        auto graph2 = std::move(graph).extend(make_placeholder<constexpr_node>());
    #endif
        STATIC_CHECK((is_node<decltype(graph2)>{}));
    }
    { // terminal is a value
        constexpr auto graph = make_graph(const_sample(-1).build());

        STATIC_CHECK((is_source<decltype(graph)>{}));

        BOOST_CHECK_EQUAL(*graph.cdrain().cbegin(), -1);

        // extend initial source with some node
        auto graph2 = graph.extend(make_placeholder<constexpr_node>());
        STATIC_CHECK((is_node<decltype(graph2)>{}));
    }
    { // terminal is a callable
        auto graph = make_graph( apply([]{ return 1; }).build() );

        STATIC_CHECK((is_source<decltype(graph)>{}));

        BOOST_CHECK_EQUAL(*graph.cdrain().cbegin(), 1);

        // extend initial source with some node
        auto graph2 = graph.extend(make_placeholder<constexpr_node>());
        STATIC_CHECK((is_node<decltype(graph2)>{}));
    }
    { // terminal is an "apply source"
        auto graph = make_graph(apply([]{ return 2; }).build());

        STATIC_CHECK((is_source<decltype(graph)>{}));

        BOOST_CHECK_EQUAL(*graph.cdrain().cbegin(), 2);

        // extend initial source with some node
        auto graph2 = graph.extend(make_placeholder<constexpr_node>());
        STATIC_CHECK((is_node<decltype(graph2)>{}));
    }
}

BOOST_AUTO_TEST_CASE(operators_test)
{
        using namespace dfp;

        auto mult = [](int const& in0, int const& in1){ return in0*in1; };
        auto square = [](int const& in) { return in*in; };
        int cnt = 1;
        auto clock = [&cnt](){ return cnt++; };
    {
        auto branch = mult  += 3
                            |= mult += 2
                                    |= square <<= apply(clock);

        BOOST_CHECK_EQUAL(*branch.drain().begin(), 6);
        BOOST_CHECK_EQUAL(*branch.drain().begin(), 24);
    }
#if 0
    auto do_nothing = [](int const&) {};
    {
        auto branch = mult  += 3
                            |= mult += do_nothing <<= const_sample(2)
                                    |= square <<= apply(clock);

        BOOST_CHECK_EQUAL(*branch.drain().begin(), 6);
        BOOST_CHECK_EQUAL(*branch.drain().begin(), 24);
    }
#endif
    {
        auto tree =
            sample_releasing() <<= mult += const_sample(3)
                                        |= const_sample(1, 2, 3)
        ;

        BOOST_CHECK_EQUAL(*tree++, 3);
        BOOST_CHECK_EQUAL(*tree++, 6);
        BOOST_CHECK_EQUAL(*tree++, 9);
    }
    cnt = 1;
    #if 0 //TODO
    {
        auto branch = mult  += mult += 3
                                    |= 2
                            ^= square <<= apply(clock).build();

        BOOST_CHECK_EQUAL(*branch.drain().begin(), 6);
        BOOST_CHECK_EQUAL(*branch.drain().begin(), 24);
    }
    #endif
}


BOOST_AUTO_TEST_CASE(integral_const_test)
{
    STATIC_CHECK((302_c == 302));
    STATIC_CHECK((-302_c == -302));
    STATIC_CHECK((-1_c + 3_c * 5_c == 14));
}


///@[subprocessing_node subpart example]
struct POWER{};
#if DFP_CPP14_SUPPORT
struct square_processing
{
    template<class TreeConfig, DFP_F_REQUIRES((functions::contains<TreeConfig, POWER>{}))>
    void initialize_upwards(TreeConfig tree_config)
    { boost::fusion::at_key<POWER>(tree_config) = 2; }

    template<class Input>
    auto operator()(Input in) const
    { return [](output_sample_t<Input> const& a) {return a*a;} <<= in; }
};
#else // #if DFP_CPP14_SUPPORT
static const auto square_function = [](int const& a) {return a*a;};

struct square_processing
{
    template<class TreeConfig, DFP_F_REQUIRES((functions::contains<TreeConfig, POWER>{}))>
    void initialize_upwards(TreeConfig tree_config)
    { boost::fusion::at_key<POWER>(tree_config) = 2; }

    template<class Input>
    auto operator()(Input in) const ->
    decltype(square_function <<= in)
    { return square_function <<= in; }
};
#endif

static const auto product_function = [](int i0, int i1){ return i0 * i1; };

struct product_processing
{
    template<class Input0, class Input1>
    auto operator()(Input0 in0, Input1 in1) const ->
    decltype(
        product_function    += in1
                            |= in0
    )
    { return
        product_function    += in1
                            |= in0
    ; }
};


#if DFP_CPP14_SUPPORT
template<class Sample>
struct back_product_processing
{
    static const auto product()
    { return [](Sample const& a, Sample const& b) {return a*b;}; }

    struct delay
    {
        delay(): backup_(0)
        {}

        auto operator()(Sample const& a)
        { Sample val = backup_; backup_ = a; return val; }

        Sample backup_;
    }; // struct delay

    template<class Input>
    auto operator()(Input in) const
    {
        const struct : custom_tag {} IN;


        return
            product()   += delay{} <<= from(IN)
                        |= unsafe_probe(IN) <<= in
        ;
    }

}; //struct back_product_processing

template<class Inputs>
struct back_product_processing_node
 :  detail::subprocessing_node<Inputs, back_product_processing<output_sample_t<std::tuple_element_t<0, Inputs>>>>
{
    typedef back_product_processing<output_sample_t<std::tuple_element_t<0, Inputs>>> processing_function;

    back_product_processing_node(Inputs&& inputs)
     :  detail::subprocessing_node<Inputs, processing_function>::subprocessing_node(std::forward<Inputs>(inputs), processing_function{})
    {}
}; //struct back_product_processing_node
#endif //#if DFP_CPP14_SUPPORT
///@[subprocessing_node subpart example]

template<class Input>
struct circle_node
 :          node<Input, atomic_stream_specifier<std::pair<double, double>>, circle_node<Input>>
{
    typedef node<Input, atomic_stream_specifier<std::pair<double, double>>, circle_node<Input>> node_type;

    circle_node(Input&& input) : node_type(std::forward<Input>(input)) {}

    output_sample_t<node_type> const& process_sample(input_sample_t<node_type> const& in)
    {
        double angle = M_PI * (in%100) / 100;
        point_.first = std::cos(angle); point_.second = std::sin(angle);
        return point_;
    }

private:
    std::pair<double, double> point_;
};
struct circle_processing
{
    template<class Input>
    auto operator()(Input in) const ->
    decltype(make_placeholder<circle_node>() <<= in)
    { return make_placeholder<circle_node>() <<= in; }
};


static const auto mult_function = [](int const& gain, int const& a) {
    return gain*a;
};


struct stepper_processing
{
private:
    int power_;

public:
    using initialization_storage_keyvalue = boost::fusion::pair<POWER, int>;

    template<class InitStorageView>
    void initialize_upwards(InitStorageView init_storage_view)
    { power_ = boost::fusion::at_key<POWER>(init_storage_view); }

    auto operator()() const ->
    decltype(
        mult_function   += increment()
                        |= mutable_sample(this->power_)
    )
    { return
        mult_function   += increment()
                        |= mutable_sample(this->power_)
    ;}

};
struct tagged_stepper_processing
{
    struct tag_type : custom_tag {};

    constexpr auto operator()() const ->
    // TODO: workaround. build() should not be invoked and Context should be deduced from inputs of the subprocessing node
    decltype(increment().build())
    { return increment().build(); }

};
BOOST_AUTO_TEST_CASE(subprocessing_cell_test)
{
    ///@[subprocessing_node upperpart example]
    {
        using namespace dfp;
        using namespace dfp::shortname;

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc(square_processing{}) <<= increment()
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), 1);
        BOOST_CHECK_EQUAL(square_serie.run(), 4);
        BOOST_CHECK_EQUAL(square_serie.run(), 9);
        BOOST_CHECK_EQUAL(square_serie.run(), 16);
    }
    ///@[subprocessing_node upperpart example]
    ///@[subprocessing_source upperpart example]
    {
        using namespace dfp;
        using namespace dfp::shortname;

        static const auto negate_function = [](int const& a) {return -a;};
        struct decrement_processing
        {
            auto operator()() const ->
            decltype(negate_function <<= increment())
            { return negate_function <<= increment(); }
        };

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc(square_processing{}) <<= subproc(decrement_processing{})
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), 1);
        BOOST_CHECK_EQUAL(square_serie.run(), 4);
        BOOST_CHECK_EQUAL(square_serie.run(), 9);
        BOOST_CHECK_EQUAL(square_serie.run(), 16);
    }
    ///@[subprocessing_source upperpart example]
    ///subprocessing_source and init storage
    {
        using namespace dfp;
        using namespace dfp::shortname;

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc(square_processing{}) <<= subproc(stepper_processing{})
        );
        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), 4);
        BOOST_CHECK_EQUAL(square_serie.run(), 16);
        BOOST_CHECK_EQUAL(square_serie.run(), 36);
        BOOST_CHECK_EQUAL(square_serie.run(), 64);
    }
    ///subprocessing_source and tag
    {
        using namespace dfp;
        using namespace dfp::shortname;

#if DFP_CPP14_SUPPORT
        STATIC_CHECK((subproc(tagged_stepper_processing{}).build().output_stream_specifier().get_tag_type() == type_meta<typename tagged_stepper_processing::tag_type>{}));
#endif //DFP_CPP14_SUPPORT

        BOOST_CHECK_EQUAL(subproc(stepper_processing{}).build().output_stream_specifier().get_tag_type(), type_meta<detail::null_tag>{});
        BOOST_CHECK_EQUAL(subproc(tagged_stepper_processing{}).build().output_stream_specifier().get_tag_type(), type_meta<typename tagged_stepper_processing::tag_type>{});
    }
#if DFP_CPP17_SUPPORT
    {
        using namespace dfp;
        using namespace dfp::shortname;

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc([](auto in){ return [](output_sample_t<decltype(in)> const& a){return a*a;} <<= in; }) <<= increment()
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), 1);
        BOOST_CHECK_EQUAL(square_serie.run(), 4);
        BOOST_CHECK_EQUAL(square_serie.run(), 9);
        BOOST_CHECK_EQUAL(square_serie.run(), 16);
    }
#endif // DFP_CPP17_SUPPORT
#if DFP_CPP14_SUPPORT
    {
        using namespace dfp;
        using namespace dfp::shortname;

        auto back_product = make_placeholder<back_product_processing_node>();
        auto backproduct_serie = make_runnable(
            sample_releaser() <<= back_product <<= increment()
        );

        BOOST_CHECK_EQUAL(backproduct_serie.run(), 0);
        BOOST_CHECK_EQUAL(backproduct_serie.run(), 0);
        BOOST_CHECK_EQUAL(backproduct_serie.run(), 2);
        BOOST_CHECK_EQUAL(backproduct_serie.run(), 6);
        BOOST_CHECK_EQUAL(backproduct_serie.run(), 12);
    }
    { // subprocessing source and upstream context
        using namespace dfp;
        using namespace dfp::shortname;

        static const auto negate_function = [](int const& a) {return -a;};
        struct decrement_processing
        {
            auto operator()() const ->
            decltype(negate_function <<= increment())
            { return negate_function <<= increment(); }
        };

        auto square_serie = make_runnable(
            sample_releaser() <<= [](int in0, int in1){ return in0*in1; }    += subproc(decrement_processing{})
                                                                            |= -1
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), 1);
        BOOST_CHECK_EQUAL(square_serie.run(), 2);
        BOOST_CHECK_EQUAL(square_serie.run(), 3);
        BOOST_CHECK_EQUAL(square_serie.run(), 4);
    }
    { // static frame-based stream in input of the subprocessing node
        using namespace dfp;
        using namespace dfp::shortname;

        //TODO: constexpr
        auto serie = make_runnable(
            sample_releaser()
            <<= subproc([](auto in){ return apply([](auto const& a){ return a*a; }) <<= routing::to_single_sample() <<= in; })
            <<= routing::const_frame({0, 1, 2, 3, 4}, 5_c)
        );

        BOOST_CHECK_NE(serie.input_stream_specifier().get_channel_count(), DYNAMIC_EXTENT);
        BOOST_CHECK_NE(serie.input_stream_specifier().get_frame_length(), DYNAMIC_EXTENT);
        BOOST_CHECK_EQUAL(serie.run(), 0);
        BOOST_CHECK_EQUAL(serie.run(), 1);
        BOOST_CHECK_EQUAL(serie.run(), 4);
        BOOST_CHECK_EQUAL(serie.run(), 9);
        BOOST_CHECK_EQUAL(serie.run(), 16);
    }
    { // dynamic frame-based stream (here the channel extend is dynamic) in input of the subprocessing node
        using namespace dfp;
        using namespace dfp::shortname;

        auto serie = make_runnable(
            sample_releaser()
            <<= subproc([](auto in){ return apply([](auto const& a){ return a*a; }) <<= routing::to_single_sample() <<= in; })
            <<= routing::const_frame({0, 1, 2, 3, 4}, 5)
        );

        BOOST_CHECK_EQUAL(serie.input_stream_specifier().get_channel_count(), 1);
        BOOST_CHECK_EQUAL(serie.input_stream_specifier().get_frame_length(), 1);
        BOOST_CHECK_EQUAL(serie.run(), 0);
        BOOST_CHECK_EQUAL(serie.run(), 1);
        BOOST_CHECK_EQUAL(serie.run(), 4);
        BOOST_CHECK_EQUAL(serie.run(), 9);
        BOOST_CHECK_EQUAL(serie.run(), 16);
    }
#endif // DFP_CPP14_SUPPORT
    { // subprocessing of innerbranch producing lvalue const reference
        using namespace dfp;
        using namespace dfp::shortname;

        auto circle_serie = subproc(circle_processing{}) <<= increment();
        (void) circle_serie;
        //TODO: STATIC_CHECK((std::is_lvalue_reference<decltype(circle_serie.output_port().renew_sample(circle_serie))>{}));
    }
    { // 2-inputs subprocessing node
        using namespace dfp;
        using namespace dfp::shortname;

        static const auto negate_function = [](int const& a) {return -a;};
        struct decrement_processing
        {
            auto operator()() const ->
            decltype(negate_function <<= increment())
            { return negate_function <<= increment(); }
        };

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc(product_processing{})  += subproc(decrement_processing{})
                                                                |= increment()
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 0);
        BOOST_CHECK_EQUAL(square_serie.run(), -1);
        BOOST_CHECK_EQUAL(square_serie.run(), -4);
        BOOST_CHECK_EQUAL(square_serie.run(), -9);
        BOOST_CHECK_EQUAL(square_serie.run(), -16);
    }
    // subprocessing_node with non-copyable input
    {
        using namespace dfp;
        using namespace dfp::shortname;

        constexpr auto noncopyable = make_placeholder<std::tuple<>, noncopyable_source>();

        auto square_serie = make_runnable(
            sample_releaser() <<= subproc(square_processing{}) <<= noncopyable
        );

        BOOST_CHECK_EQUAL(square_serie.run(), 1);
    }
}


struct my_key{};
template<class Inputs>
struct my_recorder_sink
 :          sink<Inputs, run_for, my_recorder_sink<Inputs>, std::pair<my_key, std::pair<std::size_t, std::size_t*>>>
{
    typedef sink<Inputs, run_for, my_recorder_sink<Inputs>, std::pair<my_key, std::pair<std::size_t, std::size_t*>>> sink_type;

    my_recorder_sink(Inputs&& inputs, std::size_t length)
     :  sink_type(std::forward<Inputs>(inputs), length), channel_id_(0), length_(length), channel_count_(1)
    {}

    template<class InitializationStorageView>
    void initialize_upwards(InitializationStorageView init_storage)
    {
        auto& info = boost::fusion::at_key<my_key>(init_storage);
        info.first = length_;
        info.second = &channel_count_;
    }

    void release_sample(input_sample_t<sink_type> const& input)
    {
        ++channel_id_ %= channel_count_;

        std::cout << input;
        if (channel_id_ == 0)
            std::cout << std::endl;
        else
            std::cout << ", ";
    }

private:
    std::size_t channel_id_;
    std::size_t length_;
    std::size_t channel_count_;
};

template<class Context>
struct my_player_source
 :          source<initial_context, atomic_stream_specifier<char>, my_player_source<Context>>
{
    typedef source<initial_context, atomic_stream_specifier<char>, my_player_source> source_type;

    my_player_source(Context const&, std::size_t channel_count)
     :  source_type(), channel_count_(channel_count), channel_id_(0), value_('A'-channel_count_)
    {}

    template<class TreeConfig>
    void initialize_upwards(TreeConfig tree_config)
    {
        static_assert(functions::contains<TreeConfig, my_key>{}, "my_key not found in InitializationStorage");
        std::pair<std::size_t, std::size_t*>& info = boost::fusion::at_key<my_key>(tree_config);
        *info.second = channel_count_;
        length_ = info.first;
    }

    char acquire_sample()
    {
        channel_id_ %= channel_count_;

        char data = length_*(channel_id_+1)/channel_count_ + ((channel_id_ != channel_count_-1) ? value_ : value_++);

        channel_id_++;

        return data;
    }

private:
    std::size_t const channel_count_;
    std::size_t length_;
    std::size_t channel_id_;
    char value_;
};


BOOST_AUTO_TEST_CASE(initializable_cell_test)
{
    {
        auto my_recording = [](std::size_t length){ return make_placeholder<my_recorder_sink>(length); };
        auto plus2 = [](char const& input){ return (char) (input+2); };
        auto my_player = [](std::size_t channel_count){ return make_placeholder<my_player_source>(channel_count); };

        my_recording(11) <<= plus2 <<= my_player(3);
        std::cout << std::endl;
    }
    {
        struct my_player
         :          source<initial_context, atomic_stream_specifier<char>, my_player>
        {
            typedef source<initial_context, atomic_stream_specifier<char>, my_player> source_type;

            my_player(std::size_t channel_count)
             :  source_type(), channel_count_(channel_count), channel_id_(0), value_('A'-channel_count_)
            {}

            void initialize()
            { length_ = channel_count_; }

            char acquire_sample()
            {
                channel_id_ %= channel_count_;

                char data = length_*(channel_id_+1)/channel_count_ + ((channel_id_ != channel_count_-1) ? value_ : value_++);

                channel_id_++;

                return data;
            }

        private:
            std::size_t const channel_count_;
            std::size_t length_;
            std::size_t channel_id_;
            char value_;
        };

        auto my_recording = [](std::size_t length){ return make_placeholder<my_recorder_sink>(length).implicit_run(); };
        auto plus2 = [](char const& input){ return (char) (input+2); };

        my_recording(3) <<= plus2 <<= my_player(3);

        std::cout << std::endl;
    }
    {
        auto my_recorder = [](std::size_t length){ return make_placeholder<my_recorder_sink>(length); };
        auto plus2 = [](char const& input){ return (char) (input+2); };
        auto my_player = [](std::size_t channel_count){ return make_placeholder<my_player_source>(channel_count); };

        STATIC_CHECK((detail::is_cell_builder<decltype(my_recorder(11))>{}));

        auto processing_tree = make_runnable(
            my_recorder(11) <<= plus2 <<= my_player(3)
        );

        processing_tree.run();
    }
    {
        //TODO: check constexpr
    }
}


BOOST_AUTO_TEST_CASE(mutable_sample_test)
{
    using namespace dfp;

    // const reference type
    {
        double value = 2.2;
        const double& ref = value; // const here simply prevent from in-place processing
        auto tree = sample_releasing() <<= mutable_sample(ref);
        BOOST_CHECK_EQUAL(tree.get_result(), 2.2);

        value = 1.1;
        BOOST_CHECK_EQUAL(tree.run(), 1.1);
    }
    // reference type
    ///@[mutable_sample usage example]
    {
        double value = 2.2;
        auto tree = sample_releasing() <<= mutable_sample(value);
        BOOST_CHECK_EQUAL(tree.get_result(), 2.2);

        value = 1.1;
        BOOST_CHECK_EQUAL(tree.run(), 1.1);
    }
    ///@[mutable_sample usage example]
    // std::ref
    {
        double value = 2.2;
        auto tree = sample_releasing() <<= mutable_sample(std::ref(value));
        BOOST_CHECK_EQUAL(tree.get_result(), 2.2);

        value = 1.1;
        BOOST_CHECK_EQUAL(tree.run(), 1.1);
    }
}


BOOST_AUTO_TEST_CASE(is_integral_constant_test)
{
    using namespace dfp;

    BOOST_CHECK((!is_integral_constant<bool>{}));
    BOOST_CHECK((!is_integral_constant<std::reference_wrapper<bool>>{}));
    BOOST_CHECK((is_integral_constant<std::false_type>{}));
    BOOST_CHECK((is_integral_constant<decltype(false_c)>{}));
    BOOST_CHECK((is_integral_constant<std::integral_constant<int, 0>>{}));
    BOOST_CHECK((is_integral_constant<decltype(1_c)>{}));
}


BOOST_AUTO_TEST_CASE(pack_test)
{
    using namespace dfp;

    STATIC_CHECK((pack_and(1_c, 2_c, 3_c, 4_c) == true));
    STATIC_CHECK((pack_and(1_c, 2_c, 0_c, 4_c) == false));

    STATIC_CHECK((pack_or(0_c, 0_c, 0_c, 0_c) == false));
    STATIC_CHECK((pack_or(0_c, 0_c, 3_c, 0_c) == true));
}


BOOST_AUTO_TEST_CASE(are_same_test)
{
    STATIC_CHECK((are_same<int, int>{}));
    STATIC_CHECK((are_same<int, int, int>{}));
    STATIC_CHECK((are_same<int, int, int, int>{}));
    STATIC_CHECK((are_same<std::tuple<int, int, int, int>>{}));
    STATIC_CHECK((!are_same<void, int>{}));
    STATIC_CHECK((!are_same<int, std::nullptr_t, int>{}));
    STATIC_CHECK((!are_same<int, int, int, bool>{}));
}


BOOST_AUTO_TEST_CASE(but_last_test)
{
    STATIC_CHECK((std::tuple_size<but_last_t<>>{} == 0));
    STATIC_CHECK((std::tuple_size<but_last_t<int>>{} == 0));
    STATIC_CHECK((std::is_same<std::tuple<bool>, but_last_t<bool, int>>{}));
    STATIC_CHECK((std::is_same<std::tuple<bool, int>, but_last_t<bool, int, void>>{}));
}


BOOST_AUTO_TEST_CASE(sample_input_test)
{
    {
        ///@['sample_input' usage example]
        using namespace dfp;

        std::cout << "'sample_input' usage example" << std::endl;
        std::cout << "---" << std::endl;

        struct : custom_tag {} MY_INPUT;

        auto runnable_tree = make_runnable(
            sample_releaser() <<= [](double const& in ){ return in * 2; } <<= sample_input(DOUBLE, MY_INPUT)
        );
        auto pluggable_processing = make_runnable(
            pluggable() <<= [](double const& in ){ return in + 0.1; } <<= const_sample(0.0, 1.0, 2.0, 3.0)
        );

        runnable_tree.input(MY_INPUT).plug(pluggable_processing);

        std::cout << runnable_tree.run() << ", " << runnable_tree.run() << ", " << runnable_tree.run() << std::endl;
        // expected result: 0.2, 2.2, 4.2

        ///@['sample_input' usage example]
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(runnable_tree.run(), 6.2);
        BOOST_CHECK_EQUAL(runnable_tree.run(), 0.2);
    }
    {
        ///@['sample_input' usage example with integral const as identifier]
        using namespace dfp;

        std::cout << "'sample_input' usage example with integral const as identifier" << std::endl;
        std::cout << "---" << std::endl;

        auto runnable_tree = make_runnable(
            sample_releaser() <<= [](double const& in ){ return in * 2; } <<= sample_input(DOUBLE, 1_c)
        );
        auto pluggable_processing = make_runnable(
            pluggable() <<= [](double const& in ){ return in + 0.1; } <<= const_sample(0.0, 1.0, 2.0, 3.0)
        );

        runnable_tree.input(1_c).plug(pluggable_processing);

        std::cout << runnable_tree.run() << ", " << runnable_tree.run() << ", " << runnable_tree.run() << std::endl;
        // expected result: 0.2, 2.2, 4.2

        ///@['sample_input' usage example with integral const as identifier]
        std::cout << "---" << std::endl;

        BOOST_CHECK_EQUAL(runnable_tree.run(), 6.2);
        BOOST_CHECK_EQUAL(runnable_tree.run(), 0.2);
    }
}

BOOST_AUTO_TEST_SUITE_END()
