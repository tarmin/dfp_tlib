/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#include <dfptl/alsa/alsa.hpp>
#include <dfptl/core/core.hpp>
#include <dfptl/math/sin.hpp>
#include <dfptl/speex/aec.hpp>
#include <dfptl/sndfile/read.hpp>
#include <dfptl/sndfile/write.hpp>
#include <dfptl/routing/delay.hpp>
#include <dfptl/routing/looper.hpp>
#include <dfptl/routing/drainer.hpp>
#include <dfptl/routing/const_frame.hpp>
#include <dfptl/routing/synced_buffer.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/sample_static_cast.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>
#include <dfptl/alu/add.hpp>
#include <dfptl/alu/divide.hpp>
#include <dfptl/alu/modulo.hpp>
#include <dfptl/alu/counter.hpp>
#include <dfptl/alu/equal_to.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/alu/accumulate.hpp>
#include <dfptl/alu/logical_not.hpp>

#include <deque>
#include <mutex>
#include <future>
#include <thread>
#include <iostream>
#include <stl/experimental/array.hpp>

#include <boost/test/unit_test.hpp>


#define STATIC_CHECK(x) static_assert(x, "")


using namespace dfp;
using namespace dfp::math;
using namespace dfp::routing;
using namespace dfp::alu::shortname;
using namespace std;
using namespace std::experimental;


constexpr double PI = 3.1415926535898;
constexpr int SAMPLING_RATE = 48000; //hertz
constexpr auto FRAME_LENGTH = size_t_const<1024>{};
constexpr size_t PREBUF_COUNT = 8;
constexpr int DURATION = 3; //seconds


BOOST_AUTO_TEST_CASE(alsa_read_test)
{
    Alsa_pcmi alsa_device(nullptr, "sysdefault", nullptr, SAMPLING_RATE, FRAME_LENGTH, PREBUF_COUNT, Alsa_pcmi::DEBUG_ALL);

    alsa_device.printinfo();

    {
        // records default microphone for 3 seconds
        looping(SAMPLING_RATE*DURATION) <<= sndfile::write("output/alsa_capture.wav", SAMPLING_RATE) <<= alsa::read(alsa_device);
    }
    {
        // records default microphone for 3 seconds
        auto processing = make_runnable(
            looper(SAMPLING_RATE*DURATION) <<= sndfile::write("output/alsa_capture.wav", SAMPLING_RATE) <<= alsa::read(alsa_device)
        );
        processing.run();
    }
}

BOOST_AUTO_TEST_CASE(alsa_write_test)
{
    Alsa_pcmi alsa_device("sysdefault", nullptr, nullptr, SAMPLING_RATE, FRAME_LENGTH, PREBUF_COUNT, Alsa_pcmi::DEBUG_ALL);

    alsa_device.printinfo();

    #if 0
    {
        // won't compile because of more than one single ALSA cell in the branch
        alsa::write(alsa_device) <<= alsa::write(alsa_device) <<= const_frame(make_array(0, 1));
    }
    #endif
    
    {
        // plays a 3 seconds 220Hz tone + the previous recording
        looping(SAMPLING_RATE*DURATION) <<= alsa::write(alsa_device) <<= adjust_frame_length(FRAME_LENGTH)
        <<= add()   += to_single_sample() <<= sndfile::read("output/alsa_capture.wav", FLOAT, FRAME_LENGTH)
                    |= sample_static_cast<float>() <<= div(2) <<= sin() <<= mult(2*PI*220/SAMPLING_RATE) <<= mod(SAMPLING_RATE) <<= counter(1)
        ;
    }
}


BOOST_AUTO_TEST_CASE(alsa_read_write_test)
{
    {
        ///@[real-time echo cancellation]
        synced_buffer::share<int16_t, FRAME_LENGTH> echo_ref;
        bool continue_recording = true;

        // plays "speech that changed the world" to default device and forwards signal as echo ref.
        Alsa_pcmi playback_device("sysdefault", nullptr, nullptr, SAMPLING_RATE, FRAME_LENGTH, PREBUF_COUNT, Alsa_pcmi::DEBUG_ALL);

        auto player = make_runnable(
            looper(SAMPLING_RATE*DURATION*10) <<= alsa::write(playback_device) <<= adjust_frame_length(FRAME_LENGTH)
            <<= delay(SAMPLING_RATE*0.01, 0.0f) <<= div(INT16_MAX) <<= sample_static_cast<float>() <<= to_single_sample()
            <<= synced_buffer::write(echo_ref) <<= sndfile::read("input/NelsonMandelaSpeechThatChangedTheWorld1.ogg", INT16_T, FRAME_LENGTH)
        );

        // records from default device
        auto recording = std::async(launch::async, [&continue_recording, &echo_ref]{
            Alsa_pcmi recording_device(nullptr, "sysdefault", nullptr, SAMPLING_RATE, FRAME_LENGTH, PREBUF_COUNT, Alsa_pcmi::DEBUG_ALL);

            auto audio_capture =
                adjust_frame_length(FRAME_LENGTH) <<= sample_static_cast<int16_t>() <<= mult(INT16_MAX)
                <<= to_single_sample() <<= alsa::read(recording_device);

            draining(continue_recording) <<= to_single_sample() <<= sndfile::write("output/alsa_rw_capture.wav", SAMPLING_RATE)
            <<= speex::aec(SAMPLING_RATE*0.5)   += synced_buffer::read(echo_ref)
                                                |= std::move(audio_capture)
            ;
        });
        std::this_thread::yield();

        player.run();
        continue_recording = false;
        recording.wait();
        ///@[real-time echo cancellation]
    }
    #if 0
    //TODO: low latency alsa read/write
    {
        looper(x).driven_by(alsa_device) <<= alsa::play() <<= alu::divide(2) <<= alsa::capture();
        looper(x).driven_by(alsa_device) <<= alsa::play() <<= alu::divide(2) <<= alsa::in_state();
        * capture() source is non-blocking and can only be used with driven_xx(alsa_device) and always return data
        * in_state() source is non-blocking and can be used with every sink but return optional
        * read() is blocking, can be used with every sink and alway return data
        
        sink alias: looper(alsa_device, x)
        or
        drainer(x).driven_by(alsa_device) ...
        sink alias: drainer(alsa_device, x)
    }
    #endif
}
