#include <dfptl/core/core.hpp>
#include <dfptl/alu/alu.hpp>
#include <dfptl/math/math.hpp>
#include <dfptl/routing/looper.hpp>

#include <iostream>
#include <string>


using namespace std;
using namespace dfp;
using namespace dfp::alu;
using namespace dfp::routing;


int main() {
    cout << "DFPTLIB TEST" << endl;
    cout << "------------" << endl;

    looping(10)
    <<= [](int32_t in){ cout << in << ", "; }
    <<= [](int32_t in){ return in*in; }
    <<= [](int32_t in){ cout << "x:" << in << " -> x²:"; }
    <<= counter(INT32_T);

    cout << endl;

    return 0;
}

