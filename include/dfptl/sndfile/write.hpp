/**
* @file sndfile/write.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_WRITE_HPP
#define DFPTL_SNDFILE_WRITE_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/sndfile/detail/write_node.hpp"

#include <sndfile.h>


namespace dfp
{
namespace sndfile
{
inline namespace nodes
{
///@addtogroup sndfile_grp
///@{
/**
* @defgroup sndfile_write_grp "write soundfile" node
* @brief Writes into file the PCM-samples reaching its single input.
*
* A @ref sndfile_write_grp saves PCM-samples reaching its input in a sound file (WAV, MP3, etc.)
* Its input stream is forwarded to its output port without modification.
* Processing in this node are based on functions provided by the [libsndfile](http://www.mega-nerd.com/libsndfile/) library.
*
* Note that multi-channels charateristic of the input stream is preserved when stored in the file.  
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n) | save i(n) into file" <<sndfile::write(file)>> as write
* interface "input: ...i(n)" <<INPUT>> as in
* interface "output: ...i(n)" <<operation result>> as out
* write <-up- in
* out <-up- write
* @enduml
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | multi-channels @ref stream_grp "stream" to save in sound file
* "operation result" | **output**  | the same stream as the input one.
* @{
**/


/**
* @brief Creates a cell builder for a @ref sndfile_write_grp.
*
* **Usage example**
* @snippet sndfile_test.cpp sndfile::write usage example
*
* @param file_path is the path of sound file to be created.
* @param sample_rate is the sample frame_length of the audio signal to save.
* @param sf_format is the libsndfile format of the sound file to be created. Please refer to the libsnfile
* [open](http://www.mega-nerd.com/libsndfile/api.html#open) function for details.
*
* @warning For sake of performance during file access, although it is supported, @ref atomic_stream "atomic stream" should be avoided in input
* (consider using @ref adjust_frame_length_grp to convert @ref atomic_stream "atomic stream" into stream of larger frame length).
**/
//TODO : add flush_policy support
inline auto write(
    const char* file_path, int sample_rate = 44100, int sf_format = SF_FORMAT_WAV + SF_FORMAT_FLOAT
) /** @cond */ ->
decltype(make_placeholder<detail::write_node>(file_path, sample_rate, sf_format)) /** @endcond */
{ return make_placeholder<detail::write_node>(file_path, sample_rate, sf_format); }


///@} sndfile_write_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using sndfile::write;
} // namespace nodes
} // namespace shortname


///@} sndfile_grp
} // namespace sndfile
} // namespace dfp


#endif // DFPTL_SNDFILE_WRITE_HPP
