/**
* @file sndfile/exception.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_EXCEPTION_HPP
#define DFPTL_SNDFILE_EXCEPTION_HPP


#include <exception>
#include <sndfile.h>


namespace dfp
{
namespace sndfile
{
///@addtogroup sndfile_grp
///@{

/**
* @brief Defines a type of object to be thrown as exception. It reports errors that arise in the [libsndfile](http://www.mega-nerd.com/libsndfile/) library.
**/
class exception : public std::exception
{
public:
    exception(int sf_error_code) : sf_error_code_(sf_error_code)
    {}

    int code() const noexcept
    { return sf_error_code_; }

    virtual const char* what() const noexcept
    { return sf_error_number(sf_error_code_); }

private:
    int const sf_error_code_;
};


///@}
} //namespace sndfile
} //namespace dfp



#endif // DFPTL_SNDFILE_EXCEPTION_HPP
