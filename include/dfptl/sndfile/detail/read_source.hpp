/**
* @file sndfile/detail/read_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_DETAIL_READ_SOURCE_HPP
#define DFPTL_SNDFILE_DETAIL_READ_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/core/exception.hpp"
#include "dfptl/sndfile/exception.hpp"

#include "stl/type_traits.hpp"
#include <cassert>

#include <sndfile.h>


#define DFP_SNDFILE_READ sndfile::read


namespace dfp
{
namespace sndfile
{
namespace detail
{
template<class Context, class FileId, class ChannelChunkLength, class Sample> class read_source
{ 
    static_assert(
        always_false<ChannelChunkLength>{},
        "sndfile::read source does not support the given type for the ChannelChunkLength template argument"
    );
};


template<class FileId>
struct reading_handle
{
    static_assert
    (
        std::is_same<const char*, FileId>{} || std::is_same<SNDFILE*, FileId>{},
        "the file identifier given to " DFP_STRINGIFY(DFP_SNDFILE_READ) " shall be either a of type \"const char*\" or a \"SNDFILE*\""
    );
}; // struct reading_handle


template<>
struct reading_handle<SNDFILE*>
{
    constexpr reading_handle(SNDFILE* handle)
     :  handle_(handle)
    {}

    reading_handle(reading_handle const&) = delete;

    reading_handle(reading_handle&& it)
     :  handle_(it.handle_)
    { it.handle_ = nullptr; }

    SF_INFO build()
    {
        SF_INFO sf_info;
        int const result = sf_command(handle_, SFC_GET_CURRENT_SF_INFO, &sf_info, sizeof(SF_INFO));
        assert(result == 0); (void) result;
        return sf_info;
    }

    SNDFILE* get()
    { return handle_; }

private:
    SNDFILE* handle_;
}; // struct reading_handle<SNDFILE*>


template<>
struct reading_handle<const char*>
{
    reading_handle(char const* path)
     :  file_path_(path)
    {}

    reading_handle(reading_handle const&) = delete;

    reading_handle(reading_handle&& it)
     :  file_path_(std::move(it.file_path_)), handle_(it.handle_)
    { it.handle_ = nullptr; }

    ~reading_handle()
    {
        if (handle_ != nullptr)
        {
            int const sf_close_result = sf_close(handle_);
            assert(sf_close_result == 0); (void) sf_close_result;
        }
    }

    SF_INFO build()
    {
        SF_INFO sf_info;
    
        sf_info.format = 0;
        handle_ = sf_open(file_path_.c_str(), SFM_READ, &sf_info);

        if (handle_ == nullptr)
            throw cell_error(DFP_STRINGIFY(DFP_SNDFILE_READ), "input file descriptor is unexpectedly null");

        return sf_info;
    }

    SNDFILE* get() const
    { return handle_; }

private:
    std::string const file_path_;
    SNDFILE* handle_ = nullptr;
}; // struct reading_handle<const char*>


template<class Context, class FileId, class StreamSpecifier, class Derived>
class read_source_base
 :  public  source<Context, StreamSpecifier, Derived>,
    private reading_handle<FileId>
{
public:
    typedef source<Context, StreamSpecifier, Derived> source_type;
    typedef reading_handle<FileId> handle_type;

    static_assert(
        std::is_same<output_sample_t<source_type>, short>    ::value ||
        std::is_same<output_sample_t<source_type>, int>      ::value ||
        std::is_same<output_sample_t<source_type>, float>    ::value ||
        std::is_same<output_sample_t<source_type>, double>   ::value,
        "Output sample type of the " DFP_STRINGIFY(DFP_SNDFILE_READ) " source shall be one of following arithmetic types : short, int, float, double"
    );

    read_source_base(Context const& context, FileId fileId, std::nullptr_t eof_policy_tag)
     :  source_type(context, StreamSpecifier{}), reading_handle<FileId>(fileId)
    {}

    void initialize()
    {
        SF_INFO const& sf_info = handle_type::build();
        StreamSpecifier output_stream_specifier;

        sf_command(handle_type::get(), SFC_SET_SCALE_FLOAT_INT_READ, NULL, SF_TRUE);

        static_cast<Derived* const>(this)->set_output_stream_specifier(output_stream_specifier.set_channel_count(sf_info.channels));
    }

protected:
    // take care that sndfile read functions are the "frame" variants
    ::sf_count_t read_file(double*  buf, ::sf_count_t chunk_length)
    { return sf_readf_double(handle_type::get(), buf, chunk_length); }
    ::sf_count_t read_file(float*   buf, ::sf_count_t chunk_length)
    { return sf_readf_float(handle_type::get(), buf, chunk_length); }
    ::sf_count_t read_file(int*     buf, ::sf_count_t chunk_length)
    { return sf_readf_int(handle_type::get(), buf, chunk_length); }
    ::sf_count_t read_file(short*   buf, ::sf_count_t chunk_length)
    { return sf_readf_short(handle_type::get(), buf, chunk_length); }
}; // class read_source_base


template<class Context, class FileId, std::size_t ChannelChunkLength, class Sample>
class read_source<Context, FileId, size_t_const<ChannelChunkLength>, Sample>
 :  public  read_source_base<
        Context, FileId, static_stream_specifier<Sample, ChannelChunkLength, DYNAMIC_EXTENT>,
        read_source<Context, FileId, size_t_const<ChannelChunkLength>, Sample>
    > //TODO: bug, should  fixed_frame_specifier as frame_length depends on channel count
{
    // alias for the base type
    typedef read_source_base<
        Context, FileId, static_stream_specifier<Sample, ChannelChunkLength, DYNAMIC_EXTENT>,
        read_source
    > base;
    

public:
    using base::base;

    output_frame_t<base> const& acquire_frame()
    {
        const std::size_t channel_count = base::output_stream_specifier().get_channel_count();
        const std::size_t buffer_length = ChannelChunkLength * channel_count;
        output_sample_t<base>* const buffer = new output_sample_t<base>[buffer_length];

        // for now, only support reading of 1st channel
        
        // assuming below that frame supports data() method (i.e. is an std::array)
        ::sf_count_t read_count = base::read_file(buffer, ChannelChunkLength);
        (void) read_count;
        //TODO: define strategy for reading of partial frame
        //assert(read_count == ChannelChunkLength);

        auto frame_iterator = frame_.begin();

        for(std::size_t i = 0 ; i < ChannelChunkLength ; i++)
            *frame_iterator++ = buffer[channel_count * i];

        delete[] buffer;
        return frame_;
    }

    void set_output_stream_specifier(output_stream_specifier_t<base> const& stream_specifier)
    { base::set_output_stream_specifier(stream_specifier); }

private:
    output_frame_t<base> frame_;
}; // class read_source<... size_t_const<ChannelChunkLength> ...>


} // namespace detail
} // namespace sndfile
} // namespace dfp


#endif //DFPTL_SNDFILE_DETAIL_READ_SOURCE_HPP
