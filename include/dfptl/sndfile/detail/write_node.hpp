/**
* @file sndfile/detail/write_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_DETAIL_WRITE_NODE_HPP
#define DFPTL_SNDFILE_DETAIL_WRITE_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/sndfile/exception.hpp"

#include "stl/type_traits.hpp"
#include <cassert>

#include <sndfile.h>


#define DFP_SNDFILE_WRITE sndfile::write


namespace dfp
{
namespace sndfile
{
namespace detail
{


template<class Input>
class write_node
 :  public  node<Input, as_input0_tag, write_node<Input>>
{
    typedef node<Input, as_input0_tag, write_node> node_type;

    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_SNDFILE_WRITE, 1));

    static_assert(
        std::is_same<input_sample_t<node_type>, short>    ::value ||
        std::is_same<input_sample_t<node_type>, int>      ::value ||
        std::is_same<input_sample_t<node_type>, float>    ::value ||
        std::is_same<input_sample_t<node_type>, double>   ::value,
        DFP_INVALID_INPUT_SAMPLE(DFP_SNDFILE_WRITE, "short, int, float or double", 0)
    );

public:
    write_node(Input&& input, const char* file_path, int sample_rate, int sf_format)
     :  node_type(std::forward<Input>(input)),
        file_path_(file_path), sf_info_(build_sf_info(sample_rate, sf_format)), sndfile_(nullptr)
    {}

    write_node(write_node&& it)
     :  node_type(std::move(it)), file_path_(std::move(it.file_path_)), sf_info_(it.sf_info_), sndfile_(it.sndfile_)
    { it.sndfile_ = nullptr; }

    template<class T = node_type, DFP_F_REQUIRES(std::is_copy_constructible<T>{})>
    write_node(write_node const& it)
     :  node_type(it), file_path_(it.file_path_), sf_info_(it.sf_info_), sndfile_(nullptr)
    {
        if (it.sndfile_ != nullptr)
            throw cell_error(DFP_STRINGIFY(DFP_SNDFILE_READ), "input file descriptor has already been initialized so node is not copyable anymore");
    }

    ~write_node()
    {
        if (sndfile_ != nullptr)
        {
            assert(sf_close(sndfile_) == 0);
            sndfile_ = nullptr;
        }
    }

    void initialize()
    {
        node_type::initialize();

        sf_info_.channels = this->output_stream_specifier().get_channel_count();

        sndfile_ = sf_open(file_path_.c_str(), SFM_WRITE, &sf_info_);
  
        if (sndfile_ == nullptr)
            throw cell_error(DFP_STRINGIFY(DFP_SNDFILE_WRITE), "input file descriptor is unexpectedly null");

        sf_command(sndfile_, SFC_SET_SCALE_INT_FLOAT_WRITE, NULL, SF_TRUE);
    }

    /**
    * @brief Applies the bitwise xor operator with the given input sample as the left operand and the data
    * given at instance creation as the right operand.
    *
    * @param input is the sample on which to apply the operation.
    */
    //TODO: interleaved channel samples should be aggregated in a block and here should be rather exposed a process_block() method
    template<class T = node_type>
    typename std::enable_if<!input_stream_t<T>::IS_ATOMIC, size_t_const<0>>::type
    forward_frame(input_frame_t<T> const& input);

    template<class T = node_type>
    typename std::enable_if<input_stream_t<T>::IS_ATOMIC, size_t_const<0>>::type
    forward_sample(input_sample_t<T> const& input);

private:
    std::string const file_path_;
    SF_INFO sf_info_;
    SNDFILE* sndfile_;

    static SF_INFO build_sf_info(int sample_rate, int sf_format)
    {
        SF_INFO sf_info;

        sf_info.samplerate = sample_rate;
        sf_info.channels = -1;
        sf_info.format = sf_format;

        return sf_info;
    }

    // Writes "count" samples in the opened file
    ::sf_count_t write_file(const short* buf, ::sf_count_t count)
    { return sf_write_short(sndfile_, buf, count); }

    ::sf_count_t write_file(const int* buf, ::sf_count_t count)
    { return sf_write_int(sndfile_, buf, count); }

    ::sf_count_t write_file(const float* buf, ::sf_count_t count)
    { return sf_write_float(sndfile_, buf, count); }

    ::sf_count_t write_file(const double* buf, ::sf_count_t count)
    { return sf_write_double(sndfile_, buf, count); }
}; // class write_node


template<class Input>
template<class Node>
typename std::enable_if<!input_stream_t<Node>::IS_ATOMIC, size_t_const<0>>::type
write_node<Input>::forward_frame(input_frame_t<Node> const& input)
{
    std::size_t frame_size = input.size();
    ::sf_count_t const written_count = write_file(input.data(), frame_size);
    //TODO: should have another template specialization for case frame.data() is not available

    (void) written_count; // for release build
    assert((std::size_t) written_count == frame_size);
    return 0_c;
}


template<class Input>
template<class Node>
typename std::enable_if<input_stream_t<Node>::IS_ATOMIC, size_t_const<0>>::type
write_node<Input>::forward_sample(input_sample_t<Node> const& input)
{
    ::sf_count_t const written_count = write_file(&input, 1);

    (void) written_count; // for release build
    assert(written_count == 1);
    return 0_c;
}


} // namespace detail
} // namespace sndfile
} // namespace dfp


#endif // DFPTL_SNDFILE_DETAIL_WRITE_NODE_HPP
