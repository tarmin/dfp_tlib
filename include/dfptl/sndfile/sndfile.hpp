/**
* @file sndfile/sndfile.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_SNDFILE_HPP
#define DFPTL_SNDFILE_SNDFILE_HPP


#include "dfptl/sndfile/read.hpp"
#include "dfptl/sndfile/write.hpp"


namespace dfp
{
/**
* @defgroup sndfile_grp Sound file I/O cells
* @brief Defines Processing sources and nodes for handling sound file.
*
* @ref sndfile_grp are realized by wrapping function of the [libsndfile](http://www.mega-nerd.com/libsndfile/) library
*
* @note Processing cells defined in this namespace simply wrap functions provided by the [libsndfile](http://www.mega-nerd.com/libsndfile/) library
* which shall be available on your building host for your target. The @c bootstrap script can be used to install required dependency
* when building host and target is Ubuntu 16.04 (or higher revision)
*
* @{
*
* @defgroup sndfile_detail implementation details
* @brief Implementation details and boilerplate for @ref sndfile_grp
**/


/**
* @brief Contains items related to processing cells wrapping the libsndfile functions.
**/
namespace sndfile
{
/**
* @brief Set of helper functions for sound file handling nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for sound file handling sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief short name of functions in @ref sndfile_grp namespace
**/
namespace shortname {}


///@} sndfile_grp
} // namespace sndfile
} // namespace dfp


#endif // DFPTL_SNDFILE_SNDFILE_HPP
