/**
* @file sndfile/read.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SNDFILE_READ_HPP
#define DFPTL_SNDFILE_READ_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/sndfile/detail/read_source.hpp"
#include "dfptl/core/default_settings.hpp"

#include <sndfile.h>


namespace dfp
{
namespace sndfile
{
inline namespace sources
{
///@addtogroup sndfile_grp
///@{
/**
* @defgroup sndfile_read_grp "sndfile reader" source
* @brief Produces a flow of PCM-frame read from a sound file.
*
* A @ref sndfile_read_grp can produce a frame stream from reading a sound file (WAV, MP3, etc.)
* Operation in this source are based on functions provided by the [libsndfile library](http://www.mega-nerd.com/libsndfile/).
*
* **Source interface**
* @startuml
* rectangle "o(n) = PCM-sample from file" <<sndfile::read(file)>> as src
* interface "output: ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* "produced"         | **output**  | @ref stream_grp with static frame extent conveying the PCM-sample frame
*
* **Usage example**
* @snippet sndfile_test.cpp sndfile::read usage example
*
* @warning For now the @ref sndfile_read_grp only supports reading of the 1st channel of a multi-channels sound   file.
*
* @todo add support for eof_policy_tag
* @todo add support for dynamic frame length
* @todo add support for stream-local-storage to provide sf_info and sndfile objects to other node.
* @{
**/


/**
* @brief Creates a @ref sndfile_read_grp.
*
* @param[in] file_id is the path or the handle of the sound file to be read.
* @param[in] channel_chunk_length is the length of the sample chunk for each PCM channel
* @param[in] eof_policy_tag is for later use. For now, frames following end of file are filled with uninitialized data
*
* @tparam FileId can either be `const char*` or `SNDFILE*` 
**/
template<std::size_t ChannelChunkLength = 1, class Sample = default_sample_t<>, class FileId>
inline auto read(
    FileId file_id,
    type_meta<Sample> sample_type_meta = type_meta<Sample>{},
    size_t_const<ChannelChunkLength> channel_chunk_length = size_t_const<ChannelChunkLength>{},
    std::nullptr_t eof_policy_tag = nullptr
) /** @cond */ ->
decltype(make_placeholder<std::tuple<decltype(file_id), size_t_const<ChannelChunkLength>, Sample>, detail::read_source>(file_id, eof_policy_tag)) /** @endcond */ 
{ return make_placeholder<std::tuple<decltype(file_id), size_t_const<ChannelChunkLength>, Sample>, detail::read_source>(file_id, eof_policy_tag); }


//TODO: handling of channel_chunk_length as integral_constant


/**
* @brief Creates a @ref sndfile_read_grp.
*
* @param[in] file_id is the path or the handle of the sound file to be read.
* @param[in] channel_chunk_length is the length of the sample chunk for each PCM channel
* @param[in] eof_policy_tag is for later use. For now, frames following end of file are filled with uninitialized data
*
* @tparam FileId can either be `const char*` or `SNDFILE*` 
**/
template<class Sample = default_sample_t<>, std::size_t ChannelChunkLength = 1, class FileId>
inline auto read(
    FileId file_id, size_t_const<ChannelChunkLength> channel_chunk_length = size_t_const<ChannelChunkLength>{},
    type_meta<Sample> sample_type_meta = type_meta<Sample>{}, std::nullptr_t eof_policy_tag = nullptr
) /** @cond */ ->
decltype(make_placeholder<std::tuple<decltype(file_id), size_t_const<ChannelChunkLength>, Sample>, detail::read_source>(file_id, eof_policy_tag)) /** @endcond */ 
{ return make_placeholder<std::tuple<decltype(file_id), size_t_const<ChannelChunkLength>, Sample>, detail::read_source>(file_id, eof_policy_tag); }


/**
* @brief Creates a @ref sndfile_read_grp.
*
* @param[in] file_path is the path of sound file to be read.
* @param[in] eof_policy_tag is for later use. For now, frames following end of file are filled with uninitialized data
**/
inline auto read(const char* file_path, std::nullptr_t eof_policy_tag = nullptr) /** @cond */ ->
decltype(read<default_sample_t<>, 1>(file_path, size_t_const<1>{}, type_meta<default_sample_t<>>{}, eof_policy_tag)) /** @endcond */
{ return read<default_sample_t<>, 1>(file_path, size_t_const<1>{}, type_meta<default_sample_t<>>{}, eof_policy_tag); }


/**
* @brief Creates a @ref sndfile_read_grp.
*
* @param[in] file_handle is the sndfile handle of sound file to be read.
* @param[in] eof_policy_tag is for later use. For now, frames following end of file are filled with uninitialized data
**/
inline auto read(SNDFILE* file_handle, std::nullptr_t eof_policy_tag = nullptr) /** @cond */ ->
decltype(read<default_sample_t<>, 1>(file_handle, size_t_const<1>{}, type_meta<default_sample_t<>>{}, eof_policy_tag)) /** @endcond */
{ return read<default_sample_t<>, 1>(file_handle, size_t_const<1>{}, type_meta<default_sample_t<>>{}, eof_policy_tag); }


///@} sndfile_read_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
using sndfile::read;
} // namespace sources
} // namespace shortname


///@} sndfile_grp
} // namespace sndfile
} // namespace dfp


#endif // DFPTL_SNDFILE_READ_HPP
