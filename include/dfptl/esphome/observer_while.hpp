/**
* @file esphome/observer_while.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_OBSERVER_WHILE_HPP
#define DFPTL_ESPHOME_OBSERVER_WHILE_HPP


#include <dfptl/esphome/detail/observer_sink.hpp>
#include <dfptl/esphome/detail/run_on_updated_state_while.hpp>


namespace dfp
{


inline namespace core
{

template<> struct runner_args<dfp::esphome::detail::run_on_updated_state_while>
{ typedef dfp::esphome::detail::run_on_updated_state_while_args type; };

} // namespace core


namespace esphome
{
inline namespace sink
{
///@addtogroup esphome_grp
///@{
/**
* @defgroup esphome_observer_while_grp "entity observer" sink while predicate is valid
* @brief Fires while given predicate is valid a processing tree in sync with state update of an esphome entity.
*
* A @ref esphome_observer_while_grp is a sample-based processing sink which fires its processing tree
* in sync with the state update of an ESPHOME entity.
* It drains an single input stream (of any sample type).
* Processing in this sink are based on functions provided by the [esphome](https://esphome.io/) library.
*
* @warning usage of this sink requires support for CPP 17 or later revision
*
* **Sink interface**
* @startuml
* rectangle "i(n) = any sample" <<esphome::observer_while(sink, predicate)>> as sink
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* sink <-up- in
* @enduml
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "in"         | **input-0** | @ref atomic_stream            | drains in sync with the given entity while the `predicate` is valid
*
* @sa esphome_observer_for_grp
* @{
**/


/** @cond */ #if DFP_HAS_ESPHOME_NUMERIC_SENSOR /** @endcond */
/**
* @anchor observer_while_predicate
* @brief Creates a cell builder for a @ref esphome_observer_while_grp
*
* Resulting `sink` will drains its input stream in-sync with the firing of the state callback for the given esphome entity.
* The sink will continue draining while predicate result is true.
* Input captures by this @ref sink shall be a sample-based stream.
*
* `sink` is by default auto-initialized at construction.
* `sink` shall be explicitly run and is not moveable after initialization.
*
* @param esphome_entity is an esphome entity to observe
* @param predicate is the predicate to check validity to continue observing
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_while usage example
* some other example with explicit "run"
* @snippet esphome_test.cpp esphome::observer_while usage example with explicit "run"
**/
inline auto observer_while(::esphome::sensor::Sensor& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::sensor::Sensor*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @anchor observer_while_variable
* @brief Creates a cell builder for a @ref esphome_observer_while_grp
*
* Resulting `sink` will drains its input stream in-sync with the firing of the state callback for the given esphome entity.
* The sink will continue draining while given condition variable is true.
* Input captures by this @ref sink shall be a sample-based stream.
*
* `sink` is by default auto-initialized at construction.
* `sink` shall be explicitly run and is not moveable after initialization.
*
* @param esphome_entity is an esphome entity to observe
* @param condition_variable is the condition to check to continue observing
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_while with variable usage example
**/
//TODO: to be turned into 2 inputs-stream node
inline auto observer_while(::esphome::sensor::Sensor& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @anchor observer_forever
* @brief Creates a cell builder for a @ref esphome_observer_while_grp observing forever
*
* Alias for @code observer_while(esphome_entity, [](){ return true; }) @endcode
*
* @param esphome_entity is an esphome entity to observe
**/
inline auto observer_forever(::esphome::sensor::Sensor& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */

/** @cond */ #if DFP_HAS_ESPHOME_BINARY_SENSOR /** @endcond */
/**
* @overload
*
* @sa @ref observer_while_predicate "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, std::function<bool()>)"
**/
inline auto observer_while(::esphome::binary_sensor::BinarySensor& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::binary_sensor::BinarySensor*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @overload
*
* @sa @ref observer_while_variable "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, bool&)"
**/
inline auto observer_while(::esphome::binary_sensor::BinarySensor& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @overload
*
* @sa @ref observer_forever "dfp::esphome::sink::observer_forever(::esphome::sensor::Sensor&)"
**/
inline auto observer_forever(::esphome::binary_sensor::BinarySensor& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */

/** @cond */ #if DFP_HAS_ESPHOME_TEXT_SENSOR /** @endcond */
/**
* @overload
*
* @sa @ref observer_while_predicate "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, std::function<bool()>)"
**/
inline auto observer_while(::esphome::text_sensor::TextSensor& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::text_sensor::TextSensor*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @overload
*
* @sa @ref observer_while_variable "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, bool&)"
**/
inline auto observer_while(::esphome::text_sensor::TextSensor& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @overload
*
* @sa @ref observer_forever "dfp::esphome::sink::observer_forever(::esphome::sensor::Sensor&)"
**/
inline auto observer_forever(::esphome::text_sensor::TextSensor& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */

/** @cond */ #if DFP_HAS_ESPHOME_SELECT /** @endcond */
/**
* @overload
*
* @sa @ref observer_while_predicate "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, std::function<bool()>)"
**/
inline auto observer_while(::esphome::select::Select& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::select::Select*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @overload
*
* @sa @ref observer_while_variable "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, bool&)"
**/
inline auto observer_while(::esphome::select::Select& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @overload
*
* @sa @ref observer_forever "dfp::esphome::sink::observer_forever(::esphome::sensor::Sensor&)"
**/
inline auto observer_forever(::esphome::select::Select& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */

/** @cond */ #if DFP_HAS_ESPHOME_CLIMATE /** @endcond */
/**
* @overload
*
* @sa @ref observer_while_predicate "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, std::function<bool()>)"
**/
inline auto observer_while(::esphome::climate::Climate& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::climate::Climate*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @overload
*
* @sa @ref observer_while_variable "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, bool&)"
**/
inline auto observer_while(::esphome::climate::Climate& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @overload
*
* @sa @ref observer_forever "dfp::esphome::sink::observer_forever(::esphome::sensor::Sensor&)"
**/
inline auto observer_forever(::esphome::climate::Climate& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */

/** @cond */ #if DFP_HAS_ESPHOME_NUMBER /** @endcond */
/**
* @overload
*
* @sa @ref observer_while_predicate "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, std::function<bool()>)"
**/
inline auto observer_while(::esphome::number::Number& esphome_entity, std::function<bool()> predicate)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_while<>, ::esphome::number::Number*>, detail::observer_sink>(
    detail::run_on_updated_state_while_args{predicate, detail::entity_variant(&esphome_entity)}
); }

/**
* @overload
*
* @sa @ref observer_while_variable "dfp::esphome::sink::observer_while(::esphome::sensor::Sensor&, bool&)"
**/
inline auto observer_while(::esphome::number::Number& esphome_entity, bool& condition_variable)
{ return observer_while(esphome_entity, [&condition_variable](){ return condition_variable; }); }

/**
* @overload
*
* @sa @ref observer_forever "dfp::esphome::sink::observer_forever(::esphome::sensor::Sensor&)"
**/
inline auto observer_forever(::esphome::number::Number& esphome_entity)
{ return observer_while(esphome_entity, [](){ return true; }); }
/** @cond */ #endif /** @endcond */


} // namespace sink
} // namespace esphome
} // namespace dfp


#endif // DFPTL_ESPHOME_OBSERVER_WHILE_HPP
