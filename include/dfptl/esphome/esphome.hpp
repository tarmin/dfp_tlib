/**
* @file esphome/esphome.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_ESPHOME_HPP
#define DFPTL_ESPHOME_ESPHOME_HPP


#include "dfptl/esphome/state.hpp"
#include "dfptl/esphome/actual_state.hpp"
#include "dfptl/esphome/observer_for.hpp"
#include "dfptl/esphome/observer_while.hpp"


namespace dfp
{
/**
* @defgroup esphome_grp ESPHOME processing cells
* @brief Defines Processing sources and sinks of interest for esphome device.
*
* @ref esphome_grp are realized by wrapping features brough by the [esphome](https://esphome.io/).
*
* @note Processing cells defined in this namespace mainly wrap functions provided by the [esphome](https://esphome.io/) library
* which shall be available on your building host for your target. Some projet example for integration of DFP TLIB with ESPHOME is coming soon.
*
* @{
*
* @defgroup esphome_detail implementation details
* @brief Implementation details and boilerplate for @ref esphome_grp
**/


/**
* @brief Contains items related to processing cells wrapping the esphome functions.
**/
namespace esphome
{
/**
* @brief Set of helper functions for esphome sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief Set of helper functions for esphome sinks.
**/
inline namespace sinks {}
namespace trunks = sinks;

/**
* @brief short name of functions in @ref esphome_grp namespace
**/
namespace shortname {}


///@} esphome_grp
} // namespace esphome
} // namespace dfp


#endif // DFPTL_ESPHOME_ESPHOME_HPP
