/**
* @file esphome/state.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_STATE_HPP
#define DFPTL_ESPHOME_STATE_HPP


#undef F //arduino F macro conflicts with /boost/utility/detail/result_of_iterate.hpp
#include <dfptl/core/apply.hpp>

#include <optional>

// ****
// TODO: source to be renamed (or not) into in_state
// ****


#define DFP_ESPHOME_STATE esphome::state

#if !DFP_CPP17_SUPPORT
#error "c++17 or upper required for " DFP_STRINGIFY(DFP_ESPHOME_STATE) " source"
#endif


#define F(string_literal) (FPSTR(PSTR(string_literal))) // restore F macro definition
#include <dfptl/esphome/detail/on_purpose_include.hpp>


namespace dfp
{
namespace esphome
{
inline namespace source
{
///@addtogroup esphome_grp
///@{
/**
* @defgroup esphome_state_grp "entity state" source
* @brief Provides current state of an esphome entity.
*
* A @ref esphome_state_grp is a sample-based processing source which provides current state of an ESPHOME entity when fired.
* It then produces output stream of samples possibly conveying esphome entity state if known at firing time and `std::nullopt` otherwise.
* (`std::nullopt` will basically happens at beginning until entity provides its first state update)
* Processing in this source are based on functions provided by the [esphome](https://esphome.io/) library.
*
* @warning usage of this source requires support for CPP 17 or later revision
*
* **Source interface**
* @startuml
* rectangle "o(n) = sample conveying entity state" <<esphome::state(entity)>> as state
* interface "output: ...std::optional<o(n)>]" <<state>> as out
* out <-up- state
* @enduml
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "state"      | **output**  | @ref atomic_stream            | delivers current state of ESPHOME entity if available
*
* @sa esphome_actual_state_grp
* @{
**/


/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<float>`
*
* @param esphome_sensor is an esphome numerical sensor.
* 
* **Usage example**
* @snippet esphome_test.cpp esphome::state usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_NUMERIC_SENSOR /** @endcond */
inline auto state(::esphome::sensor::Sensor& esphome_sensor)
{ return apply(
    [&esphome_sensor]()
    {
        if (esphome_sensor.has_state())
            return std::make_optional(esphome_sensor.state);
        else
            return std::optional<float>{};
    }
); }
/** @cond */ #endif /** @endcond */


/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<bool>`
*
* @param esphome_sensor is an esphome binary sensor.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::state for binary sensor usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_BINARY_SENSOR /** @endcond */
inline auto state(::esphome::binary_sensor::BinarySensor& esphome_sensor)
{ return apply(
    [&esphome_sensor]()
    {
        if (esphome_sensor.has_state())
            return std::make_optional(esphome_sensor.state);
        else
            return std::optional<bool>{};
    }
); }
/** @cond */ #endif /** @endcond */


/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<std::string>`
*
* @param esphome_sensor is an esphome text sensor.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::state for text sensor usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_TEXT_SENSOR  /** @endcond */
inline auto state(::esphome::text_sensor::TextSensor& esphome_sensor)
{ return apply(
    [&esphome_sensor]()
    {
        if (esphome_sensor.has_state())
            return std::make_optional(esphome_sensor.state);
        else
            return std::optional<std::string>{};
    }
); }
/** @cond */ #endif /** @endcond */


/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<std::size_t>`
*
* @param esphome_entity is an esphome "select" control.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::state for "select" usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_SELECT /** @endcond */
inline auto state(::esphome::select::Select& esphome_entity)
{ return apply(
    [&esphome_entity]()
    {
        if (esphome_entity.has_state())
            return std::make_optional(esphome_entity.active_index().value());
        else
            return std::optional<std::size_t>{};
    }
); }
/** @cond */ #endif /** @endcond */


/** @cond */ #if DFP_HAS_ESPHOME_CLIMATE /** @endcond */
struct climate_state_reader
{
    ::esphome::climate::Climate& climate_;
    bool has_state_ { false };
    ::esphome::climate::ClimateDeviceRestoreState state_;

    std::optional<::esphome::climate::ClimateDeviceRestoreState> operator()()
    {
        state_.mode = climate_.mode;

        has_state_ |= // if all fields of climate have default value and never had state available before, likely no state available
            (climate_.mode != ::esphome::climate::CLIMATE_MODE_OFF)
            ||
            (climate_.target_temperature != float{})
            ||
            (climate_.fan_mode.has_value())
            ||
            (climate_.custom_fan_mode.has_value())
            ||
            (climate_.preset.has_value())
            ||
            (climate_.custom_preset.has_value())
            ||
            (climate_.swing_mode != ::esphome::climate::ClimateSwingMode{});

        if (has_state_)
        {
            ::esphome::climate::ClimateTraits const traits = climate_.get_traits();

            if (traits.get_supports_two_point_target_temperature())
            {
                state_.target_temperature_low = climate_.target_temperature_low;
                state_.target_temperature_high = climate_.target_temperature_high;
            }
            else
            {
                state_.target_temperature = climate_.target_temperature;
            }
            if (traits.get_supports_fan_modes() && climate_.fan_mode.has_value())
            {
                state_.uses_custom_fan_mode = false;
                state_.fan_mode = climate_.fan_mode.value();
            }
            if (!traits.get_supported_custom_fan_modes().empty() && climate_.custom_fan_mode.has_value())
            {
                state_.uses_custom_fan_mode = true;
                const auto &supported = traits.get_supported_custom_fan_modes();
                std::vector<std::string> vec{ supported.begin(), supported.end() };
                auto it = std::find(vec.begin(), vec.end(), climate_.custom_fan_mode);
                if (it != vec.end())
                    state_.custom_fan_mode = std::distance(vec.begin(), it);
            }
            if (traits.get_supports_presets() && climate_.preset.has_value())
            {
                state_.uses_custom_preset = false;
                state_.preset = climate_.preset.value();
            }
            if (!traits.get_supported_custom_presets().empty() && climate_.custom_preset.has_value())
            {
                state_.uses_custom_preset = true;
                const auto &supported = traits.get_supported_custom_presets();
                std::vector<std::string> vec { supported.begin(), supported.end() };
                auto it = std::find(vec.begin(), vec.end(), climate_.custom_preset);
                // only set custom preset if value exists, otherwise leave it as is
                if (it != vec.cend())
                  state_.custom_preset = std::distance(vec.begin(), it);
            }
            if (traits.get_supports_swing_modes())
            {
                state_.swing_mode = climate_.swing_mode;
            }

            return std::make_optional(state_);
        }
        else
            return std::optional<::esphome::climate::ClimateDeviceRestoreState>{};
    }
};

/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<::esphome::climate::ClimateDeviceRestoreState>`
*
* @param esphome_entity is an esphome "climate" device control.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::state for "climate" usage example
**/
inline auto state(::esphome::climate::Climate& esphome_entity)
{ return apply(climate_state_reader{ esphome_entity }); }
/** @cond */ #endif /** @endcond */


/**
* @brief Creates a cell builder for a @ref esphome_state_grp.
*
* Output produced by this @ref source is a stream of `std::optional<float>`
*
* @param esphome_entity is an esphome "number" control.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::state for "number" usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_NUMBER /** @endcond */
inline auto state(::esphome::number::Number& esphome_entity)
{ return apply(
    [&esphome_entity]()
    {
        if (esphome_entity.has_state())
            return std::make_optional(esphome_entity.state);
        else
            return std::optional<float>{};
    }
); }
/** @cond */ #endif /** @endcond */


} // namespace source
} // namespace esphome
} // namespace dfp


#endif // DFPTL_ESPHOME_STATE_HPP
