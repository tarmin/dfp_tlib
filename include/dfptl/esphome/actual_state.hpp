/**
* @file esphome/actual_state.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_ACTUAL_STATE_HPP
#define DFPTL_ESPHOME_ACTUAL_STATE_HPP


#include <dfptl/core/placeholder.hpp>
#include <dfptl/esphome/detail/actual_state_source.hpp>

#include <optional>


#define DFP_ESPHOME_ACTUAL_STATE esphome::actual_state

#if !DFP_CPP17_SUPPORT
#error "c++17 or upper required for " DFP_STRINGIFY(DFP_ESPHOME_ACTUAL_STATE) " source"
#endif


namespace dfp
{
namespace esphome
{
inline namespace source
{
///@addtogroup esphome_grp
///@{
/**
* @defgroup esphome_actual_state_grp "entity actual state" source
* @brief Provides current state of an esphome sensor.
*
* A @ref esphome_actual_state_grp is a sample-based processing source which provides current state of an ESPHOME sensor when fired.
* It shall take place in a processing tree terminated with one of the observer sinks. It then produces an output stream of samples
* conveying the esphome sensor state.
* Processing in this source are based on functions provided by the [esphome](https://esphome.io/) library.
*
* @warning usage of this source requires support for CPP 17 or later revision
*
* **Source interface**
* @startuml
* rectangle "o(n) = sample conveying sensor state" <<esphome::actual_state(TYPE)>> as state
* interface "output: ...o(n)]" <<current>> as out
* out <-up- state
* @enduml
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "current"    | **output**  | @ref atomic_stream            | delivers current state of ESPHOME sensor
*
* @sa observer_for() observer_while() state()
* @{
**/

// ***
//TODO: source to be renamed into captured or observee
// ***

/**
* @brief Creates a cell builder for a @ref esphome_actual_state_grp.
*
* Output produced by this @ref source is a stream of `OutputSample`
*
* @tparam OutputSample type of the sample delivered by the sensor observed in the processing tree.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_forever on text_sensor usage example
**/
template<class OutputSample>
inline auto actual_state()
{ return make_placeholder<std::tuple<OutputSample>, detail::actual_state_source>(); }


/**
* @overload
*
* @sa @ref actual_state() "actual_state<typename decltype(type_metadata)::type>()"
* 
* @param type_metadata is the metadata of the type delivered by the source.
*
* **Usage example**
* @snippet esphome_test.cpp esphome::actual_state usage example
**/
//TODO: turn OutputSample template parameter into sensor type, aka ClimatePreset, sensor::Sensor, number etc.
template<class OutputSample>
inline auto actual_state(type_meta<OutputSample> type_metadata)
{ return actual_state<typename decltype(type_metadata)::type>(); }


} // namespace source
} // namespace esphome
} // namespace dfp


#endif // DFPTL_ESPHOME_ACTUAL_STATE_HPP
