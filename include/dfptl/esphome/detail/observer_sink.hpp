/**
* @file esphome/detail/observer_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_DETAIL_OBSERVER_SINK_HPP
#define DFPTL_ESPHOME_DETAIL_OBSERVER_SINK_HPP


#undef F //arduino F macro conflicts with /boost/utility/detail/result_of_iterate.hpp

#include <unordered_map>
#include <functional>
#include <variant>
#include <list>


#define DFP_ESPHOME_OBSERVER esphome::observer


#if !DFP_CPP17_SUPPORT
#error "c++17 or upper required for " DFP_ESPHOME_OBSERVER " sink"
#endif


#define F(string_literal) (FPSTR(PSTR(string_literal))) // restore F macro definition
#include <dfptl/esphome/detail/on_purpose_include.hpp>

#ifndef ESPHOME_VERSION_CODE
    #include <esphome/core/version.h>
#else
    #include <esphome/core/macros.h>
#endif


namespace dfp
{
namespace esphome
{


namespace detail
{


namespace keys
{
struct observer_config {};
}; // namespace keys



typedef std::variant<
#if DFP_HAS_ESPHOME_NUMERIC_SENSOR
    ::esphome::sensor::Sensor*,
#endif
#if DFP_HAS_ESPHOME_BINARY_SENSOR
    ::esphome::binary_sensor::BinarySensor*,
#endif
#if DFP_HAS_ESPHOME_TEXT_SENSOR
    ::esphome::text_sensor::TextSensor*,
#endif
#if DFP_HAS_ESPHOME_SELECT
    ::esphome::select::Select*,
#endif
#if DFP_HAS_ESPHOME_CLIMATE
    ::esphome::climate::Climate*,
#endif
#if DFP_HAS_ESPHOME_NUMBER
    ::esphome::number::Number*,
#endif
    std::nullptr_t
> entity_variant;


template<class Sensor, class = void>
struct entity_data;

template<class Sensor>
using entity_data_t = typename entity_data<Sensor>::type;

template<class T>
struct entity_data<T, DFP_C_REQUIRES((std::is_pointer<T>{}))> : entity_data<std::remove_pointer_t<T>>
{};

#if DFP_HAS_ESPHOME_NUMERIC_SENSOR
template<>
struct entity_data<::esphome::sensor::Sensor>
{ typedef float type; };
#endif
#if DFP_HAS_ESPHOME_BINARY_SENSOR
template<>
struct entity_data<::esphome::binary_sensor::BinarySensor>
{ typedef bool type; };
#endif
#if DFP_HAS_ESPHOME_TEXT_SENSOR
template<>
struct entity_data<::esphome::text_sensor::TextSensor>
{ typedef std::string type; };
#endif
#if DFP_HAS_ESPHOME_SELECT
template<class Select>
struct entity_data<Select, DFP_C_REQUIRES((std::is_base_of<::esphome::select::Select, Select>{}))>
{ typedef std::size_t type; };
#endif
#if DFP_HAS_ESPHOME_CLIMATE
template<class Climate>
struct entity_data<Climate, DFP_C_REQUIRES((std::is_base_of<::esphome::climate::Climate, Climate>{}))>
{ typedef ::esphome::climate::ClimateDeviceRestoreState type; };
#endif
#if DFP_HAS_ESPHOME_NUMBER
template<class Number>
struct entity_data<Number, DFP_C_REQUIRES((std::is_base_of<::esphome::number::Number, Number>{}))>
{ typedef float type; };
#endif


// helper for the std::visit
template<class... Args>
struct overloaded : Args...
{ using Args::operator()...; };

template<class... Args>
overloaded(Args...) -> overloaded<Args...>;


template<class T = void> // dummy template for single instantiation of the static member
class run_on_updated_state
{
protected:
    static std::unordered_map<entity_variant, std::list<std::function<void()>> > entity_callbacks_map_;

    run_on_updated_state(entity_variant a_entity_variant)
     :  entity_variant_(a_entity_variant)
    {}

    run_on_updated_state(run_on_updated_state const& src)
    { entity_variant_ = src.entity_variant_; }

    run_on_updated_state(run_on_updated_state&& src)
    {
        entity_variant_ = src.entity_variant_;
        src.entity_variant_ = nullptr;
    }

    template<class Derived, class Sink>
    std::list<std::function<void()>>::const_iterator register_callback(Derived& derived, Sink& sink)
    {
        // runner only for sample-based sink
        static_assert(core::detail::supports_release_sample<Sink>{}, DFP_UNEXPECTED_ERROR);

        // As of now esphome does not allow to unregister state callback so
        // for memory usage consideration, it is preferable to use add_on_state_callback as few as possible.
        // Consequently for each sensor, needed callbacks list is actually handled in runner and only one single callback
        // is registered per sensor
        auto entity_callbacks_pair_it = entity_callbacks_map_.find(entity_variant_);

        if(entity_callbacks_pair_it == entity_callbacks_map_.cend())
        {
            entity_callbacks_pair_it = entity_callbacks_map_.emplace(entity_variant_, 0).first;

            auto const& callback_list = entity_callbacks_pair_it->second;

            // register the sensor callback if any
            std::visit( overloaded {
#if DFP_HAS_ESPHOME_NUMERIC_SENSOR
                [&callback_list](::esphome::sensor::Sensor* const entity)
                {
                    entity->add_on_state_callback(
                        [&callback_list](float state) { for(auto callback : callback_list) callback(); }
                    );
                },
#endif
#if DFP_HAS_ESPHOME_BINARY_SENSOR
                [&callback_list](::esphome::binary_sensor::BinarySensor* const entity)
                {
                    entity->add_on_state_callback(
                        [&callback_list](bool state) { for(auto callback : callback_list) callback(); }
                    );
                },
#endif
#if DFP_HAS_ESPHOME_TEXT_SENSOR
                [&callback_list](::esphome::text_sensor::TextSensor* const entity)
                {
                    entity->add_on_state_callback(
                        [&callback_list](std::string state) { for(auto callback : callback_list) callback(); }
                    );
                },
#endif
#if DFP_HAS_ESPHOME_SELECT
                [&callback_list](::esphome::select::Select* const entity)
                {
                    entity->add_on_state_callback(
                        [&callback_list](std::string state, std::size_t index) { for(auto callback : callback_list) callback(); }
                    );
                },
#endif
#if DFP_HAS_ESPHOME_CLIMATE
                [&callback_list](::esphome::climate::Climate* const entity)
                {
                    entity->add_on_state_callback(
#if (ESPHOME_VERSION_CODE < VERSION_CODE(2023, 8, 0))
                        [&callback_list]() { for(auto callback : callback_list) callback(); }
#else
                        [&callback_list](::esphome::climate::Climate&) { for(auto callback : callback_list) callback(); }
#endif
                    );
                },
#endif
#if DFP_HAS_ESPHOME_NUMBER
                [&callback_list](::esphome::number::Number* const entity)
                {
                    entity->add_on_state_callback(
                        [&callback_list](float state) { for(auto callback : callback_list) callback(); }
                    );
                },
#endif
                [](std::nullptr_t){}
            }, entity_variant_);
        }

        auto& callback_list = entity_callbacks_pair_it->second;

        // raise callback before registration if some state value is already available
        std::visit(overloaded {
#if DFP_HAS_ESPHOME_NUMERIC_SENSOR
            [&](::esphome::sensor::Sensor* sensor)
            { if(sensor->has_state()) derived.do_once(sink); },
#endif
#if DFP_HAS_ESPHOME_BINARY_SENSOR
            [&](::esphome::binary_sensor::BinarySensor* sensor)
            { if(sensor->has_state()) derived.do_once(sink); },
#endif
#if DFP_HAS_ESPHOME_TEXT_SENSOR
            [&](::esphome::text_sensor::TextSensor* sensor)
            { if(sensor->has_state()) derived.do_once(sink); },
#endif
#if DFP_HAS_ESPHOME_SELECT
            [&](::esphome::select::Select* sensor)
            { if(sensor->has_state()) derived.do_once(sink); },
#endif
#if DFP_HAS_ESPHOME_CLIMATE
            [&](::esphome::climate::Climate* sensor)
            {
                if( // if all fields of climate have default value, likely no state available
                    (sensor->mode != ::esphome::climate::CLIMATE_MODE_OFF)
                    ||
                    (sensor->target_temperature != float{})
                    ||
                    (sensor->fan_mode.has_value())
                    ||
                    (sensor->custom_fan_mode.has_value())
                    ||
                    (sensor->preset.has_value())
                    ||
                    (sensor->custom_preset.has_value())
                    ||
                    (sensor->swing_mode != ::esphome::climate::ClimateSwingMode{})
                ) derived.do_once(sink);
            },
#endif
#if DFP_HAS_ESPHOME_NUMBER
            [&](::esphome::number::Number* sensor)
            { if(sensor->has_state()) derived.do_once(sink); },
#endif
            [](std::nullptr_t)
            {}
        }, entity_variant_);

        callback_list.push_front([&derived, &sink](){ derived.do_once(sink); });

        return callback_list.cbegin();
    }

    void unregister_callback(std::optional<std::list<std::function<void()>>::const_iterator> callback_it)
    {
        if(callback_it)
        {
            auto& callbacks_list = entity_callbacks_map_.find(entity_variant_)->second;

            callbacks_list.erase(callback_it.value());
        }

    }

    ~run_on_updated_state()
    {
        if(!std::holds_alternative<std::nullptr_t>(entity_variant_))
            if(auto const search = entity_callbacks_map_.find(entity_variant_); search != entity_callbacks_map_.cend())
                if(search->second.empty())
                    entity_callbacks_map_.erase(entity_variant_);
    }

    entity_variant entity_variant_{nullptr};

public:
    entity_variant get_entity_variant() const
    { return entity_variant_; }
}; // run_on_updated_state

template<class T>
std::unordered_map<entity_variant, std::list<std::function<void()>> > run_on_updated_state<T>::entity_callbacks_map_{};


template<class ActualState>
struct iactual_state_getter
{
    typedef ActualState actual_state_type;

    virtual ActualState const& invoke() = 0;
};


template<class Entity, class = void>
struct actual_state_getter;


template<class T>
struct actual_state_getter<T, DFP_C_REQUIRES((std::is_pointer<T>{}))>
 :  actual_state_getter<std::remove_pointer_t<T>>
{ using actual_state_getter<std::remove_pointer_t<T>>::actual_state_getter; };

#if DFP_HAS_ESPHOME_NUMERIC_SENSOR
template<>
struct actual_state_getter<::esphome::sensor::Sensor>
 :  iactual_state_getter<float>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    float const& invoke() override
    { return std::get<::esphome::sensor::Sensor*>(entity_variant_)->state; }

private:
    entity_variant entity_variant_;
};
#endif


#if DFP_HAS_ESPHOME_BINARY_SENSOR
template<>
struct actual_state_getter<::esphome::binary_sensor::BinarySensor>
 :  iactual_state_getter<bool>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    bool const& invoke() override
    { return std::get<::esphome::binary_sensor::BinarySensor*>(entity_variant_)->state; }

private:
    entity_variant entity_variant_;
};
#endif


#if DFP_HAS_ESPHOME_TEXT_SENSOR
template<>
struct actual_state_getter<::esphome::text_sensor::TextSensor>
 :  iactual_state_getter<std::string>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    std::string const& invoke() override
    { return std::get<::esphome::text_sensor::TextSensor*>(entity_variant_)->state; }

private:
    entity_variant entity_variant_;
};
#endif

#if DFP_HAS_ESPHOME_SELECT
template<>
struct actual_state_getter<::esphome::select::Select>
 :  iactual_state_getter<std::size_t>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    std::size_t const& invoke() override
    {
        // Unfortunately processing behind active_index() is sadly called for each client (actual_state source)
        // while it should be enough to call it only once.
        // However the fix for this overprocessing should likely be performed inside ::esphome::select::Select class
        // not in dfptl wrapper
        actual_state_ = std::get<::esphome::select::Select*>(entity_variant_)->active_index().value();

        return actual_state_;
    }

private:
    entity_variant entity_variant_;
    std::size_t actual_state_;
};
#endif

#if DFP_HAS_ESPHOME_CLIMATE
template<>
struct actual_state_getter<::esphome::climate::Climate>
 :  iactual_state_getter<::esphome::climate::ClimateDeviceRestoreState>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    ::esphome::climate::ClimateDeviceRestoreState const& invoke() override
    {
//        std::get<::esphome::climate::Climate*>(entity_variant_)
        return actual_state_;
    }

private:
    entity_variant entity_variant_;
    ::esphome::climate::ClimateDeviceRestoreState actual_state_;
};
#endif

#if DFP_HAS_ESPHOME_NUMBER
template<>
struct actual_state_getter<::esphome::number::Number>
 :  iactual_state_getter<float>
{
    actual_state_getter(entity_variant sensor) : entity_variant_(sensor)
    {}

    float const& invoke() override
    { return std::get<::esphome::number::Number*>(entity_variant_)->state; }

private:
    entity_variant entity_variant_;
};
#endif


template<class Input, class Runner, class EntityPtr>
class observer_sink
{ static_assert(always_false<observer_sink>{}, DFP_UNEXPECTED_ERROR); };

template<class Input, template<class> class Runner, class EntityPtr>
class observer_sink<Input, Runner<void>, EntityPtr>
 :  public  core::sink<
        Input, Runner, observer_sink<Input, Runner<void>, EntityPtr>,
        std::pair<keys::observer_config, iactual_state_getter<entity_data_t<EntityPtr>>*>
    >,
    public  actual_state_getter<EntityPtr>
{
    typedef core::sink<
        Input, Runner, observer_sink,
        std::pair<keys::observer_config, iactual_state_getter<entity_data_t<EntityPtr>>*>
    > sink_type;

public:
    using sink_type::run;

    observer_sink(Input&& input, runner_args_t<Runner> const& runner_args_tuple) noexcept
     :  observer_sink(
            std::forward<Input>(input), runner_args_tuple,
            index_sequence_from_t<runner_args_t<Runner>>{}
        )
    {}

    virtual ~observer_sink()
    {}

    template<class InitStorage>
    void initialize_upwards(InitStorage init_storage)
    { boost::fusion::at_key<keys::observer_config>(init_storage) = this; }

    void release_sample(input_sample_t<sink_type> const&) const
    {/*nothing to do*/}

private:
    template<std::size_t... Is>
    observer_sink(Input&& input, runner_args_t<Runner> const& runner_args, std::index_sequence<Is...>) noexcept
     :  sink_type(std::forward<Input>(input), std::get<Is>(runner_args)...),
        actual_state_getter<EntityPtr>(this->get_runner().get_entity_variant())
    {}
}; // class observer_sink


} // namespace detail
} // namespace esphome
} // namespace dfp


#endif //DFPTL_ESPHOME_DETAIL_OBSERVER_SINK_HPP
