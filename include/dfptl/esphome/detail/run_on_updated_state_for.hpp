/**
* @file esphome/detail/run_on_updated_state_for.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_FOR_HPP
#define DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_FOR_HPP


#undef F //arduino F macro conflicts with /boost/utility/detail/result_of_iterate.hpp
#include <dfptl/core/sink.hpp>


#define F(string_literal) (FPSTR(PSTR(string_literal))) // restore F macro definition


namespace dfp
{
namespace esphome
{
namespace detail
{
/**
* @brief tuple of parameter types for run_on_updated_state_for constructor.
**/
typedef std::tuple<std::size_t, entity_variant> run_on_updated_state_for_args;


/**
* @brief @c Runner firing in loop the processing tree to release the given count of samples
*
* This runner basically implements a "for" loop in the current thread and release the count of samples given as parameter
**/
template<class Sink=void>
struct run_on_updated_state_for
 :  runner<Sink, run_on_updated_state_for<Sink>>,
    public run_on_updated_state<>
{
    static_assert(
        std::is_same<std::tuple_element_t<0, run_on_updated_state_for_args>, std::size_t>{},
        "Unconsistency in definition of run_on_updated_state_for_args, please contact developer"
    );
    static_assert(
        std::is_same<std::tuple_element_t<1, run_on_updated_state_for_args>, entity_variant>{},
        "Unconsistency in definition of run_on_updated_state_for_args, please contact developer"
    );
 
   run_on_updated_state_for(std::size_t sample_count, entity_variant esphome_sensor)
     :  run_on_updated_state<>(esphome_sensor), sample_count_(sample_count)
    {}

    run_on_updated_state_for(run_on_updated_state_for const& src)
     :  run_on_updated_state<>(src), sample_count_(src.sample_count_), remaining_count_(src.remaining_count_)
    {}

    run_on_updated_state_for(run_on_updated_state_for&& src)
     :  run_on_updated_state<>(std::move(src)), sample_count_(src.sample_count_), remaining_count_(src.remaining_count_)
    {
        unregister_callback(src.callback_it_);
        src.callback_it_.reset();
    }

    void do_once(Sink& sink)
    {
        if(remaining_count_ > 0)
        {
            this->release_sample(sink);
            remaining_count_--;
        }
    }

    template<class T = Sink>
    void operator()(Sink& sink)
    {
        remaining_count_ = sample_count_;
        if(!callback_it_)
            callback_it_ = register_callback(*this, sink);
    }

    bool is_running() const
    { return remaining_count_ > 0; }

    ~run_on_updated_state_for()
    {
        if (callback_it_)
            unregister_callback(callback_it_);
    }

private:
    std::optional<std::list<std::function<void()>>::const_iterator> callback_it_{};
    std::size_t const sample_count_;
    std::size_t remaining_count_;
}; // struct run_on_updated_state_for


template<>
struct run_on_updated_state_for<void>
{};


} // namespace detail
} // namespace esphome
} // namespace dfp


#endif //DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_FOR_HPP
