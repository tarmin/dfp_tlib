/**
* @file esphome/detail/on_purpose_include.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_DETAIL_ON_PURPOSE_INCLUDE_HPP
#define DFPTL_ESPHOME_DETAIL_ON_PURPOSE_INCLUDE_HPP


#if __has_include(<esphome/components/sensor/sensor.h>)
    #include <esphome/components/sensor/sensor.h>
    #define DFP_HAS_ESPHOME_NUMERIC_SENSOR 1
#else
    #define DFP_HAS_ESPHOME_NUMERIC_SENSOR 0
#endif
#if __has_include(<esphome/components/text_sensor/text_sensor.h>)
    #include <esphome/components/text_sensor/text_sensor.h>
    #define DFP_HAS_ESPHOME_TEXT_SENSOR 1
#else
    #define DFP_HAS_ESPHOME_TEXT_SENSOR 0
#endif
#if __has_include(<esphome/components/binary_sensor/binary_sensor.h>)
    #include <esphome/components/binary_sensor/binary_sensor.h>
    #define DFP_HAS_ESPHOME_BINARY_SENSOR 1
#else
    #define DFP_HAS_ESPHOME_BINARY_SENSOR 0
#endif
#if __has_include(<esphome/components/select/select.h>)
    #include <esphome/components/select/select.h>
    #define DFP_HAS_ESPHOME_SELECT 1
#else
    #define DFP_HAS_ESPHOME_SELECT 0
#endif
#if __has_include(<esphome/components/climate/climate.h>)
    #include <esphome/components/climate/climate.h>
    #define DFP_HAS_ESPHOME_CLIMATE 1
#else
    #define DFP_HAS_ESPHOME_CLIMATE 0
#endif
#if __has_include(<esphome/components/number/number.h>)
    #include <esphome/components/number/number.h>
    #define DFP_HAS_ESPHOME_NUMBER 1
#else
    #define DFP_HAS_ESPHOME_NUMBER 0
#endif


#endif // DFPTL_ESPHOME_DETAIL_ON_PURPOSE_INCLUDE_HPP
