/**
* @file esphome/detail/run_on_updated_state_while.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_WHILE_HPP
#define DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_WHILE_HPP


#undef F //arduino F macro conflicts with /boost/utility/detail/result_of_iterate.hpp
#include <dfptl/core/sink.hpp>


#define F(string_literal) (FPSTR(PSTR(string_literal))) // restore F macro definition


namespace dfp
{
namespace esphome
{
namespace detail
{
/**
* @brief tuple of parameter types for run_on_updated_state_while constructor.
**/
typedef std::tuple<std::function<bool()>, entity_variant> run_on_updated_state_while_args;


/**
* @brief @c Runner firing in loop the processing tree while a given predicate function is evaluated as true
**/
template<class Sink=void>
struct run_on_updated_state_while
 :  runner<Sink, run_on_updated_state_while<Sink>>,
    public run_on_updated_state<>
{
    static_assert(
        std::is_same<std::tuple_element_t<0, run_on_updated_state_while_args>, std::function<bool()>>{},
        "Unconsistency in definition of run_on_updated_state_while_args, please contact developer"
    );
    static_assert(
        std::is_same<std::tuple_element_t<1, run_on_updated_state_while_args>, entity_variant>{},
        "Unconsistency in definition of run_on_updated_state_while_args, please contact developer"
    );
 
    run_on_updated_state_while(std::function<bool()> predicate, entity_variant esphome_sensor)
     :  run_on_updated_state<>(esphome_sensor), predicate_(predicate)
    {}

    run_on_updated_state_while(run_on_updated_state_while const&) = default;
    run_on_updated_state_while(run_on_updated_state_while&&) = default;

    void do_once(Sink& sink)
    { if(predicate_()) this->release_sample(sink); }

    template<class T = Sink>
    void operator()(Sink& sink)
    {
        if(!callback_it_)
            callback_it_ = register_callback(*this, sink);
    }

    bool is_running() const
    { return predicate_(); }

    ~run_on_updated_state_while()
    {
        auto& callbacks_list = entity_callbacks_map_.find(entity_variant_)->second;

        if(callback_it_)
            callbacks_list.erase(callback_it_.value());
    }

private:
    std::optional<std::list<std::function<void()>>::const_iterator> callback_it_{};
    std::function<bool()> const predicate_;
}; // struct run_on_updated_state_while


template<>
struct run_on_updated_state_while<void>
{};


} // namespace detail
} // namespace esphome
} // namespace dfp


#endif //DFPTL_ESPHOME_DETAIL_RUN_ON_UPDATED_STATE_WHILE_HPP
