/**
* @file esphome/detail/actual_state_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_DETAIL_ACTUAL_STATE_SOURCE_HPP
#define DFPTL_ESPHOME_DETAIL_ACTUAL_STATE_SOURCE_HPP


#include <dfptl/esphome/detail/observer_sink.hpp>
#include <dfptl/core/source.hpp>


#define DFP_ESPHOME_ACTUAL_STATE esphome::actual_state


namespace dfp
{
namespace esphome
{
namespace detail
{


template<class Context, class ActualState>
class actual_state_source
 :  public  core::source<Context, atomic_stream_specifier<ActualState>, actual_state_source<Context, ActualState>>
{
    typedef core::source<Context, atomic_stream_specifier<ActualState>, actual_state_source> source_type;

public:
    using source_type::source_type;

    template<class InitStorage>
    void initialize_upwards(InitStorage init_storage)
    {
        using namespace keys;

        static_assert(
            boost::fusion::has_key<observer_config>(init_storage),
            DFP_STRINGIFY(DFP_ESPHOME_ACTUAL_STATE) " source can only take place in tree terminated with an esphome::observer_xxx sink"
        );

        auto config = boost::fusion::at_key<observer_config>(init_storage);

        static_assert(
            std::is_convertible<typename std::remove_pointer_t<decltype(config)>::actual_state_type, ActualState>{},
            DFP_STRINGIFY(DFP_ESPHOME_ACTUAL_STATE) " source shall be configured to delivered a sample type identical to type of the observed sensor state"
        );

        actual_state_getter_ = config;
    }

    output_sample_t<source_type> const& acquire_sample() const
    { return actual_state_getter_->invoke(); }

private:
    iactual_state_getter<ActualState>* actual_state_getter_;
}; // class actual_state_source


} // namespace detail
} // namespace esphome
} // namespace dfp


#endif //DFPTL_ESPHOME_DETAIL_ACTUAL_STATE_SOURCE_HPP
