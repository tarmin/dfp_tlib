/**
* @file esphome/observer_for.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ESPHOME_OBSERVER_FOR_HPP
#define DFPTL_ESPHOME_OBSERVER_FOR_HPP


#include <dfptl/esphome/detail/observer_sink.hpp>
#include <dfptl/esphome/detail/run_on_updated_state_for.hpp>


namespace dfp
{
inline namespace core
{

template<> struct runner_args<dfp::esphome::detail::run_on_updated_state_for>
{ typedef dfp::esphome::detail::run_on_updated_state_for_args type; };

} // namespace core
namespace esphome
{


inline namespace sink
{
///@addtogroup esphome_grp
///@{
/**
* @defgroup esphome_observer_for_grp "entity observer" sink for iteration count
* @brief Fires for a given iteration count a processing tree in sync with state update of an esphome entity.
*
* A @ref esphome_observer_for_grp is a sample-based processing sink which fires its processing tree
* in sync with the state update of an ESPHOME entity.
* It drains an single input stream (of any sample type).
* Processing in this sink are based on functions provided by the [esphome](https://esphome.io/) library.
*
* @warning usage of this sink requires support for CPP 17 or later revision
*
* **Sink interface**
* @startuml
* rectangle "i(n) = any sample" <<esphome::observer_for(sink, count)>> as sink
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* sink <-up- in
* @enduml
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "in"         | **input-0** | @ref atomic_stream            | drains in sync with the given entity the `count` of input samples
*
* @sa esphome_observer_while_grp
* @{
**/

// ******
//TODO: rework into looper(x).driven_by(esphome_entity) and create alias looper(esphome_entity, x)
// ******

/**
* @anchor observer_for 
* @brief Creates a cell builder for a @ref esphome_observer_for_grp
*
* Resulting `sink` will drains its input stream in-sync with the firing of the state callback for the given esphome entity.
* The sink will stop draining after number of specified iteration has been reached.
* Input captures by this @ref sink shall be a sample-based stream.
*
* `sink` is by default auto-initialized at construction.
* `sink` shall be explicitly run and is not moveable after initialization.
*
* @param esphome_entity is an esphome entity to observe
* @param loop_count is the number of time the processing will be fired in sync with the esphome entity
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_for usage example
* some other example with explicit "run"
* @snippet esphome_test.cpp esphome::observer_for usage example with explicit "run"
**/
/** @cond */ #if DFP_HAS_ESPHOME_NUMERIC_SENSOR /** @endcond */

//TODO: to be turned into 2 inputs-stream node
inline auto observer_for(::esphome::sensor::Sensor& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::sensor::Sensor*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


/**
* @overload
*
* @sa @ref observer_for "observer_for(::esphome::sensor::Sensor&, std::size_t)"
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_for on binary_sensor usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_BINARY_SENSOR /** @endcond */

inline auto observer_for(::esphome::binary_sensor::BinarySensor& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::binary_sensor::BinarySensor*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


/**
* @overload
*
* @sa @ref observer_for "observer_for(::esphome::sensor::Sensor&, std::size_t)"
**/
/** @cond */ #if DFP_HAS_ESPHOME_TEXT_SENSOR /** @endcond */

inline auto observer_for(::esphome::text_sensor::TextSensor& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::text_sensor::TextSensor*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


/**
* @overload
*
* @sa @ref observer_for "observer_for(::esphome::sensor::Sensor&, std::size_t)"
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_for on select usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_SELECT /** @endcond */

inline auto observer_for(::esphome::select::Select& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::select::Select*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


/**
* @overload
*
* @sa @ref observer_for "observer_for(::esphome::sensor::Sensor&, std::size_t)"
**/
/** @cond */ #if DFP_HAS_ESPHOME_CLIMATE /** @endcond */

inline auto observer_for(::esphome::climate::Climate& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::climate::Climate*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


/**
* @overload
*
* @sa @ref observer_for "observer_for(::esphome::sensor::Sensor&, std::size_t)"
*
* **Usage example**
* @snippet esphome_test.cpp esphome::observer_for on number usage example
**/
/** @cond */ #if DFP_HAS_ESPHOME_NUMBER /** @endcond */

inline auto observer_for(::esphome::number::Number& esphome_entity, std::size_t loop_count)
{ return make_placeholder<std::tuple<detail::run_on_updated_state_for<>, ::esphome::number::Number*>, detail::observer_sink>(
    detail::run_on_updated_state_for_args{loop_count, detail::entity_variant(&esphome_entity)}
); }

/** @cond */ #endif /** @endcond */


} // namespace sink
} // namespace esphome
} // namespace dfp


#endif // DFPTL_ESPHOME_OBSERVER_FOR_HPP
