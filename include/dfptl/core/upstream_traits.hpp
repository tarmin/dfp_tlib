/**
* @file core/upstream_traits.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_UPSTREAM_TRAITS_HPP
#define DFPTL_CORE_UPSTREAM_TRAITS_HPP


#include "dfptl/core/detail/cell_traits.hpp"
#include "dfptl/core/type_traits.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup upstream_traits upstream cell traits
* @brief Provides helper class to retrieve traits on processing @ref source or @ref node
* @{
**/


/**
* @brief Provides member typedef @c type which is defined as the output stream type
* of the given @ref upstream_cell "upstream cell" or @ref processing_branch "processing branch"
*
* @tparam T is the upstream cell type (aka @ref source of @ref node) on which to retrieve the output stream type.
*/
template<class T>
struct output_stream
{
    static_assert(detail::is_upstream<T>{}, DFP_UNEXPECTED_ERROR);

    typedef detail::port_stream_t<detail::cell_output_port_t<T>> type;
};


/**
* @brief Alias for output_stream
* @related output_stream
**/
template<class T>
using output_stream_t = typename output_stream<T>::type;


/**
* @brief Provides member typedef type which is defined as the output stream specifier type of the given upstream cell.
*
* @tparam T is the upstream cell type (aka @ref source of @ref node) on which to retrieve the type.
*/
template<class T, class = DFP_CONCEPT_ASSERT((detail::is_upstream<T>))>
struct output_stream_specifier
{ typedef detail::port_stream_specifier_t<detail::cell_output_port_t<T>> type; };

/**
* @brief Alias for output_stream_specifier
* @related output_stream_specifier
**/
template<class T>
using output_stream_specifier_t = typename output_stream_specifier<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the output sample type of the given upstream cell.
*
* @tparam T is the upstream cell type (aka @ref source of @ref node) on which to retrieve the output sample type.
**/
template<class T> struct output_sample
{ typedef sample_t<output_stream_t<T>> type; };

/**
* @brief Alias of output_sample
* @related output_sample
**/
template<class T> using output_sample_t = typename output_sample<T>::type;


/**
* @brief Provides member typedef type which is defined as the output frame type of the given upstream cell.
*
* @tparam T is the @ref upstream_cell "upstream cell" type
* (aka @ref source of @ref node) on which to retrieve the output frame type.
**/
template<class T> struct output_frame
{ typedef frame_t<output_stream_t<T>> type; };

/**
* @brief Alias of output_frame
* @related output_frame
**/
template<class T> using output_frame_t = typename output_frame<T>::type;


/**
* @brief Provides typedef @c type with the @ref stream_specifier "stream specifier" describing the given @c Stream
**/
template<class Stream, class = DFP_CONCEPT_ASSERT((is_stream<Stream>))>
struct specifier
{ typedef typename Stream::specifier type; };

/**
* @brief Alias for specifier
* @related specifier
**/
template<class Stream>
using specifier_t = typename specifier<Stream>::type;


// not the right place. Should be moved in some apply_upstream_traits.hpp
namespace detail
{
template<class T, class... Args>
using output_frame_length_type = decltype(std::declval<T const&>().output_frame_length(std::declval<Args>()...));

template<class T, class... Args>
struct supports_output_frame_length : std::experimental::is_detected<output_frame_length_type, T, Args...> {};

template<class T>
using initialize_type = decltype(std::declval<T const&>().initialize());

template<class T>
struct supports_initialize : std::experimental::is_detected<initialize_type, T> {};
} // namespace detail


///@}
///@} core_grp
} //namespace core
} //namespace dfp


#endif // DFPTL_CORE_UPSTREAM_TRAITS_HPP
