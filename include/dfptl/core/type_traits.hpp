/**
* @file core/type_traits.hpp
* Extends stl type traits
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_TYPE_TRAITS_HPP
#define DFPTL_CORE_TYPE_TRAITS_HPP


#include "dfptl/core/cplusplus.hpp"
#include "dfptl/core/integral_const.hpp"

#include <array>
#include <iterator>
#include "stl/span.hpp"
#include "stl/type_traits.hpp"
#include "stl/experimental/type_traits.hpp"

#include <boost/type_traits/function_traits.hpp>



namespace dfp
{
inline namespace core
{
//TODO cell_tag to be created
struct source_tag {};
struct sink_tag {};
struct node_tag {};
// to be renamed into graph_tag
struct processing_graph_tag {};
struct context_tag {};
// to be renamed into tree_tag
struct processing_tree_tag {};
struct cell_port_tag {};
struct output_port_tag  : cell_port_tag {};
struct input_port_tag   : cell_port_tag {};
struct stream_tag {};


///@addtogroup core_grp
///@{
///@addtogroup type_traits
///@{


/**
* @brief If @c T is a template specialization of @c Template
* provides the member constant @c value equal to true. Otherwise value is false.
*
* e.g.
* @code{.cpp}
* static_assert(is_specialization<std::tuple<int, long, char>, std::tuple>::value, "");
* @endcode
**/
template <class T, template <class...> class Template>
struct is_specialization : std::false_type {};

template <template <class...> class Template, class... Args>
struct is_specialization<Template<Args...>, Template> : std::true_type {};


template<class T, class = void>
struct is_integral_constant : std::false_type {};

template<class T>
struct is_integral_constant<T, DFP_C_REQUIRES((std::is_base_of<std::integral_constant<typename T::value_type, T::value>, T>{}))>
 :  std::true_type {};

/**
* @brief If @c T is boolean type or reference type possibly CV-qualified
* provides the member constant @c value equal to true. Otherwise value is false.
*
* @tparam T is the type to test.
**/
template<class T, class = void>
struct is_bool : std::false_type {};

template<class T>
struct is_bool<
    T, typename std::enable_if<std::is_same<std::remove_cv_t<std::remove_reference_t<T>>, bool>::value>::type
>
    :   std::true_type {};


/**
* @brief If @c T is a char type or reference type possibly CV-qualified
* provides the member constant @c value equal to true. Otherwise value is false.
*
* @tparam T is the type to test.
**/
template<class T, class = void>
struct is_char : std::false_type {};

template<class T>
struct is_char<
    T, typename std::enable_if<std::is_same<std::remove_cv_t<std::remove_reference_t<T>>, char>::value>::type
>   :   std::true_type {};


/**
* @brief If @c T is std::size_t type or reference type possibly CV-qualified
* provides the member constant @c value equal to true. Otherwise value is false.
*
* @tparam T is the type to test.
**/
template<class T, class = void>
struct is_size_t : std::false_type {};

template<class T>
struct is_size_t<
    T, typename std::enable_if<std::is_same<std::remove_cv_t<std::remove_reference_t<T>>, std::size_t>::value>::type
>   :   std::true_type {};


/**
* @brief If @c T is an @c int type or reference type possibly CV-qualified
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* @tparam T is the type to test.
* @tparam IsSignSensitive indicates if signess of type shall be considered in test.
**/
template<class T, bool IsSignSensitive = true, class = void>
struct is_int : std::false_type {};
/** @cond */
template<class T>
struct is_int<
    T, true, DFP_C_REQUIRES((std::is_same<std::remove_cv_t<std::remove_reference_t<T>>, int>{}))
>   :   std::true_type {};

template<class T>
struct is_int<
    T, false, DFP_C_REQUIRES((std::is_same<std::make_signed_t<std::remove_cv_t<std::remove_reference_t<T>>>, int>{}))
>   :   std::true_type {};
/** @endcond */


/**
* @brief Alias for @c std::true_type whatever is the given T type
**/
template<class T>
using always_true = std::is_pointer<std::remove_reference_t<T>*>;


/**
* @brief Alias for @c std::false_type whatever is the given T type
**/
template<class T>
using always_false = std::bool_constant<!always_true<T>::value>;


/**
* @brief If given template argument @c T provides iterator type, provides the constant member @c value equal to true.
* Otherwise @c value is false.
**/
//TODO: to be deprecated
template<class T, class = void>
struct supports_iterator : std::false_type {};

template<class T>
struct supports_iterator<T, std::void_t<typename T::iterator>> : std::true_type {};


/**
* @brief If given template argument @c T provides const iterator type,
* provides the constant member @c value equal to true. Otherwise @c value is false.
**/
//TODO: to be deprecated
template<class T, class = void>
struct supports_const_iterator : std::false_type {};

template<class T>
struct supports_const_iterator<T, std::void_t<typename T::const_iterator>> : std::true_type {};


template<class Container>
struct supports_const_iterator_check : std::true_type
{ static_assert(
        supports_const_iterator<std::remove_cvref_t<Container>>{},
        "Container type shall be const-iterable (shall expose 'cbegin' and 'cend' methods)"
); };


/**
* @brief Testify at compile-time that the given @c BoolConstant is resolved as std::true_type.
*
* Compilation will fails if @c BoolConstant is resolved as std::false_type.
**/
template<class BoolConstant>
struct concept_check
{
    static_assert(BoolConstant::value, "Concept check failure. Please raise an issue to developers at https://gitlab.com/tarmin/dfp_tlib/-/issues/new");
    typedef void type;
};


/**
* @brief Provides member typedef @c type as T::iterator.
**/
template<class T, class = DFP_CONCEPT_ASSERT((supports_iterator<T>))>
struct iterator
{ typedef typename T::iterator type; };


/**
* @brief Type alias for @ref iterator<T>
**/
template<class T>
using iterator_t = typename iterator<T>::type;


/**
* @brief Provides member typedef @c type as T::const_iterator.
**/
template<class T, class = DFP_CONCEPT_ASSERT((supports_const_iterator<T>))>
struct const_iterator
{ typedef typename T::const_iterator type; };


/**
* @brief Alias for @ref const_iterator<T>
**/
template<class T>
using const_iterator_t = typename const_iterator<T>::type;


/**
* @brief Provides constant member @c value equal to true if given @c Iterator is a random access iterator.
**/
template<class Iterator>
struct is_random_access_iterator
    :   std::is_same<typename std::iterator_traits<Iterator>::iterator_category, std::random_access_iterator_tag> {};


/**
* @brief Provides constant member @c value equal to true if given @c Iterator is a bidirectional iterator.
**/
template<class Iterator>
struct is_bidirectional_iterator : std::bool_constant<
    std::is_same<typename std::iterator_traits<Iterator>::iterator_category, std::bidirectional_iterator_tag>::value
    || is_random_access_iterator<Iterator>::value
> {};


/**
* @brief Provides constant member @c value equal to true if given @c Iterator is a forward iterator.
**/
template<class Iterator>
struct is_forward_iterator : std::bool_constant<
    std::is_same<typename std::iterator_traits<Iterator>::iterator_category, std::forward_iterator_tag>::value
    || is_bidirectional_iterator<Iterator>::value
> {};


/**
* @brief Provides constant member @c value equal to true if given @c Iterator is
* [an input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator).
**/
template<class Iterator>
struct is_input_iterator : std::bool_constant<
    std::is_same<typename std::iterator_traits<Iterator>::iterator_category, std::input_iterator_tag>::value
    || is_forward_iterator<Iterator>::value
> {};


/**
* @brief Provides constant member @c value equal to true if given @c Iterator is an output iterator.
**/
template<class Iterator>
struct is_output_iterator : std::bool_constant<
    std::is_same<typename std::iterator_traits<Iterator>::iterator_category, std::output_iterator_tag>::value
    || is_forward_iterator<Iterator>::value
> {};


/**
* @brief If T is std::array type or reference type, provides the member constant @c value equal true.
* For any other type, @c value is false.
**/
template<class T, class Enable = void>
struct is_std_array : std::false_type {};

template<class T>
struct is_std_array<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_std_array<std::remove_reference_t<T>> {};

template<class T, std::size_t N>
struct is_std_array<std::array<T, N>, void> : std::true_type {};


struct custom_tag_category {};

/**
* @brief Base class for custom tag
*
* Custom tag can also inherits from this base class.
*
* @sa as_input_tag
**/
struct custom_tag
{ typedef custom_tag_category tag_category; };


/**
* @brief If T inherits from @ref core::custom_tag, provides the member constant @c value equal true.
* For any other type, @c value is false.
**/
template<class T, class = void>
struct is_custom_tag : std::false_type {};

template<class T>
struct is_custom_tag<T, DFP_C_REQUIRES((std::is_same<typename T::tag_category, custom_tag_category>{}))> : std::true_type {};


template<class... Args>
struct are_same
{ static_assert(sizeof...(Args) >= 2, "at least 2 parameters required in input of are_same<>"); };

template<class... Args>
struct are_same<std::tuple<Args...>> : are_same<Args...>
{};

template<class Arg0, class Arg1, class... Args>
struct are_same<Arg0, Arg1, Args...> : bool_const<std::is_same<Arg0, Arg1>{} && are_same<Arg1, Args...>{}> 
{};

template<class Arg0, class Arg1>
struct are_same<Arg0, Arg1> : bool_const<std::is_same<Arg0, Arg1>{}> 
{};


namespace detail
{


/**
* @brief Special tag to indicate that a template argument has unspecified type.
**/
struct null_tag {};


template<class T>
struct is_null_tag : std::is_base_of<null_tag, T> {};


/**
* @brief Provides member typedef @c type which is defined as associated stream for handling constness.
*
* @tparam T is the type on which to retrieve the associated const type.
**/
//TODO: const_type does not work well better to mimic to_const pattern as implemented in stream
template<class T, class = void>
struct constify
{ typedef T const type; };

// partial specialization if const_type typedef is available
template<class T>
struct constify<T, DFP_C_REQUIRES((std::is_class<typename T::const_type>::value))>
{ typedef typename T::const_type type; };

/**
* @brief Alias for constify<...>::type
* @related constify
**/
template<class T>
using constify_t = typename constify<T>::type;


template<class T, class... Args>
using const_call_operator_type = decltype(std::declval<T const&>()(std::declval<Args>()...));


template<class T, class... Args>
using call_operator_type = decltype(std::declval<T&>()(std::declval<Args>()...));


/**
* @brief If given template argument @c T has a method @c operator()(...) @c const, provides the constant member @c value equal to true.
* Otherwise @c value is false.
**/
template<class T, class... Args>
struct supports_const_call_operator : std::experimental::is_detected<const_call_operator_type, T, Args...> {};


/**
* @brief If given template argument @c T has a method @c operator()(...) or @c operator()(...) @c const, provides the constant member @c value equal to true.
* Otherwise @c value is false.
**/
template<class T, class... Args>
struct supports_call_operator : std::experimental::is_detected<call_operator_type, T, Args...> {};


/**
* @brief If given template argument @c T has only a method @c operator()(...), provides the constant member @c value equal to true.
* Otherwise @c value is false.
**/
template<class T, class... Args>
struct supports_mutable_call_operator : std::conjunction<supports_call_operator<T, Args...>, std::negation<supports_const_call_operator<T, Args...>>>{};


template<class T, class Args, class = void>
struct returns_void_impl : std::false_type {};

template<class T, class... Args>
struct returns_void_impl<T, std::tuple<Args...>,
    DFP_C_REQUIRES((std::is_invocable_r<void, T, Args...>{} && std::is_same<decltype(std::declval<T>()(std::declval<Args>()...)), void>{}))
>   : std::true_type {};


/**
* @brief If given template argument @c T is an [invocable](https://en.cppreference.com/w/cpp/concepts/invocable)
* returning nothing (void), provides the constant member @c value equal to true.
* Otherwise @c value is false.
**/
template<class T, class... Args>
struct returns_void : returns_void_impl<T, std::tuple<Args...>> {};


template<template<class...> class... Templates>
struct template_pack{};


} // namespace detail


namespace fix
{

// add constexpr for forward_as_tuple
//TODO: to be moved in upg namespace
template<class... Types>
constexpr std::tuple<Types&&...> forward_as_tuple(Types&&... args) noexcept
{ return std::tuple<Types&&...>(std::forward<Types>(args)...); }


} // namespace fix


///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_TYPE_TRAITS_HPP
