/**
* @file core/processing_cell.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PROCESSING_CELL_HPP
#define DFPTL_CORE_PROCESSING_CELL_HPP


#include "dfptl/core/detail/processing_cell.hpp"


namespace dfp
{
inline namespace core
{


template<class ProcessingContext, class Derived>
class processing_cell
 :  private detail::processing_cell<ProcessingContext, Derived>
{
    typedef detail::processing_cell<ProcessingContext, Derived> impl;

public:    
    typedef typename impl::context_type context_type;
    typedef typename impl::tag_map tag_map;
    /**
    * @brief Provides member typedef @c type as the upstream cell type associated with the given @c Tag.
    *
    * The retrieved upstream cell can be somewhere in upstream part of the processing graph where this stream is emitted
    */
    template<class Tag>
    using at_tag    = typename impl::template at_tag<Tag>;
    /**
    * @brief alias for `typename at_tag<Tag>::type`
    **/
    template<class Tag>
    using at_tag_t  = typename at_tag<Tag>::type;
    /**
    * @brief Is same as [std::true_type](https://en.cppreference.com/w/cpp/types/integral_constant) if the given @c Tag
    * has been associated to some cell of the upstream part.
    *
    * Otherwise is same as [std::false_type](https://en.cppreference.com/w/cpp/types/integral_constant)
    **/
    template<class Tag>
    using has_tag   = typename impl::template has_tag<Tag>;
}; // class processing_cell


} // namespace core
} // namespace dfp


#endif //DFPTL_CORE_PROCESSING_CELL_HPP
