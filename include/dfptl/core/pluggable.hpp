/**
* @file core/pluggable.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PLUGGABLE_HPP
#define DFPTL_CORE_PLUGGABLE_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/detail/pluggable_sink.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup pluggable_grp "pluggable" sink
* @brief Allows to later connect the processing graph terminated by this sink to an downstream graph.
*
* the @ref pluggable_grp allows to define a processing graph that will be later plug as input of a downstream graph
* which help of @ref sample_input_grp. 
*
* **Sink interface**
* @startuml
* rectangle "saves reference on the input stream for later connection" <<core::pluggable()>> as snk
* interface "...i(n)" <<SOURCE>> as in
* snk <-up- in
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* SOURCE      | **input-0** | @ref atomic_stream "atomic stream" to be forward to a @ref sample_input_grp.
* @{
**/


/**
* @brief Creates a builder for a @ref pluggable_grp to create a processing tree that will be later plugged as input of a downstream graph.
*
* @snippet core_test.cpp 'sample_input' usage example with integral const as identifier
**/
constexpr inline auto pluggable() /** @cond */ ->
decltype(make_placeholder<detail::pluggable_sink>()) /** @endcond */
{ return make_placeholder<detail::pluggable_sink>(); }



///@}
///@}
}// namespace core
} // namespace dfp


#endif // DFPTL_CORE_CLONEABLE_HPP
