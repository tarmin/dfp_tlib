/**
* @file core/graph_designer.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_GRAPH_DESIGNER_HPP
#define DFPTL_CORE_GRAPH_DESIGNER_HPP


#include "dfptl/core/apply.hpp"
#include "dfptl/core/execute.hpp"
#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/sample_from.hpp"
#include "dfptl/core/default_settings.hpp"
#include "dfptl/core/detail/processing_graph.hpp"


namespace dfp
{
inline namespace core
{
struct graph_designer_tag  {};


/**
* @brief processing_graph builder
* (expected to be invoked by dfptl operators ("extend", "expand" and "collect")
*/
template<class Caps = typename default_settings_t<>::graph_designer_caps>
struct graph_designer
{
    static_assert(is_specialization<Caps, std::tuple>{}, "std::tuple specialization expected for Caps template parameter");
    static_assert(always_false<graph_designer>{}, DFP_UNEXPECTED_ERROR);
};


namespace detail
{


template<class T, class = void>
struct is_graph_designer : std::false_type{};

template<class T>
struct is_graph_designer<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_tag, graph_designer_tag>{}))>
 :  std::true_type{};


template<class Class, class... Caps>
struct graph_designer_impl
{ static_assert(always_false<graph_designer_impl>{}, DFP_UNEXPECTED_ERROR); };


template<class GraphDesigner, class GraphDesignerTag>
struct add_graph_designer_cap
{ static_assert(always_false<GraphDesignerTag>{}, "Unrecognized template parameter GraphDesignerTag"); };


template<class GraphDesigner, class GraphDesignerTag>
using add_graph_designer_cap_t = typename add_graph_designer_cap<GraphDesigner, GraphDesignerTag>::type;


template<class Class, class CapN, class... Caps>
struct graph_designer_impl<Class, CapN, Caps...> : graph_designer_impl<add_graph_designer_cap_t<Class, CapN>, Caps...>
{};


template<class Class>
struct graph_designer_impl<Class> : Class
{};


} // namespace detail


class graph_designer_base
{
public:
    typedef graph_designer_tag dfp_tag;

public:
    template<class Lhs, class Rhs, DFP_F_REQUIRES((detail::is_processing_graph<Rhs>{} &&
        detail::is_cell_builder<Lhs>{}
    ))>
    constexpr auto extend(Rhs&& graph, Lhs&& cell_builder) const
    // NOTE that std::forward<Rhs> is required to allow non-copyable input graph to be "extended"
    // by taking leverage of move-construction
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(std::forward<Rhs>(graph).extend(std::forward<Lhs>(cell_builder)))
    #endif
    { return std::forward<Rhs>(graph).extend(std::forward<Lhs>(cell_builder)); }




    template<class Lhs, class Rhs, DFP_F_REQUIRES((detail::is_processing_graph<Rhs>{} &&
        detail::invocable_convertible_with_apply<
            Lhs,
            std::tuple<typename std::remove_cvref_t<Rhs>::terminal_cell_type>
        >{}
    ))>
    constexpr auto extend(Rhs&& rhs, Lhs&& invocable) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(invocable))))
    #endif
    { return extend(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(invocable))); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((detail::is_processing_graph<Rhs>{} &&
        detail::invocable_convertible_with_execute<
            Lhs,
            std::tuple<typename std::remove_cvref_t<Rhs>::terminal_cell_type>
        >{}
    ))>
    constexpr auto extend(Rhs&& rhs, Lhs&& invocable) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(std::forward<Rhs>(rhs), execute(std::forward<Lhs>(invocable))))
    #endif
    { return extend(std::forward<Rhs>(rhs), execute(std::forward<Lhs>(invocable))); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((!detail::is_processing_graph<Rhs>{} &&
        detail::is_upstream<Rhs>{}
    ))>
    constexpr auto extend(Rhs&& upstream, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(std::declval<detail::processing_graph<Rhs>>(),                std::forward<Lhs>(lhs)))
    #endif
    { return extend(detail::processing_graph<Rhs>{ std::forward<Rhs>(upstream) }, std::forward<Lhs>(lhs)); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((!detail::is_processing_graph<Rhs>{} &&
        detail::is_source_builder<Rhs>{}
    ))>
    constexpr auto extend(Rhs&& source_builder, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(source_builder.build(), std::forward<Lhs>(lhs)))
    #endif
    { return extend(source_builder.build(), std::forward<Lhs>(lhs)); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((!detail::is_processing_graph<Rhs>{} &&
        (detail::invocable_convertible_with_apply<Rhs, std::tuple<>>{} && !is_integral_constant<Rhs>{})
    ))>
    constexpr auto extend(Rhs&& invocable, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(apply(std::forward<Rhs>(invocable)), std::forward<Lhs>(lhs)))
    #endif
    { return extend(apply(std::forward<Rhs>(invocable)), std::forward<Lhs>(lhs)); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((!detail::is_processing_graph<Rhs>{} &&
        detail::is_cell_builder<Lhs>{} &&
        !(
            detail::is_upstream<Rhs>{} || detail::is_source_builder<Rhs>{} || (std::is_invocable<Rhs>{} && !is_integral_constant<Rhs>{})
        )
    ))>
    constexpr auto extend(Rhs&& sample, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(extend(sample_from(std::forward<Rhs>(sample)), std::forward<Lhs>(lhs)))
    #endif
    { return extend(sample_from(std::forward<Rhs>(sample)), std::forward<Lhs>(lhs)); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_upstream<Rhs>{}
        ||
        detail::is_processing_graph<Rhs>{}
        ||
        detail::is_cell_builder<Lhs>{}
    )), class... Args>
    constexpr std::nullptr_t extend(Rhs&& rhs, Lhs&& lhs, Args...) const
    {
        static_assert(
            !detail::is_upstream<Rhs>{}
            || ( detail::is_upstream<Rhs>{}
                && (
                    detail::is_cell_builder<Lhs>{}
                    ||
                    detail::is_convertible_to_cell_builder<Lhs, std::tuple<typename std::remove_reference_t<Rhs>>>{}
                )
            ),
            "right-hand side of <<= operator is an upstream cell but left-hand side is not suitable (not a cell builder nor something convertible to a node or sink builder)"
        );

        static_assert(
            !detail::is_processing_graph<Rhs>{}
            || ( detail::is_processing_graph<Rhs>{}
                && (
                    detail::is_cell_builder<Lhs>{}
                    ||
                    detail::is_convertible_to_cell_builder<Lhs, std::tuple<typename std::remove_reference_t<Rhs>::terminal_cell_type>>{}
                )
            ),
            "right-hand side of <<= operator is a processing graph but left-hand side is not suitable (neither a cell builder nor something convertible to a node or sink builder)"
        );


        static_assert(
            !detail::is_cell_builder<Lhs>{}
            || ( detail::is_cell_builder<Lhs>{}
                && (
                    detail::is_upstream<Rhs>{} || detail::is_source_builder<Rhs>{}
                    ||
                    detail::invocable_convertible_with_apply<Rhs, std::tuple<>>{}
                    ||
                    !std::is_invocable<Rhs>{}
                )
            ),
            "left-hand side of <<= operator is a cell builder but right-hand side is not suitable (neither an upstream cell nor something convertible to a source builder)"
        );

        static_assert(always_false<Rhs>{}, DFP_UNEXPECTED_ERROR);
        return nullptr;
    }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_graph<Rhs>{} &&
        detail::is_cell_builder<Lhs>{}
    ))>
    constexpr auto expand(Rhs&& graph, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(std::forward<Rhs>(graph).expand(std::forward<Lhs>(lhs)))
    #endif
    { return std::forward<Rhs>(graph).expand(std::forward<Lhs>(lhs)); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_graph<Rhs>{} &&
        detail::is_processing_graph<Lhs>{}
    ))>
    constexpr auto expand(Rhs&& graph, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(std::forward<Rhs>(graph).expand(std::forward<Lhs>(lhs)))
    #endif
    { return std::forward<Rhs>(graph).expand(std::forward<Lhs>(lhs)); }

    // this overload is required in peculiar for subprocessing_node
    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_graph<Rhs>{} &&
        !detail::is_processing_graph<Lhs>{} && detail::is_upstream<Lhs>{}
    ))>
    constexpr auto expand(Rhs&& graph, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(std::forward<Rhs>(graph).expand(detail::processing_graph<Lhs>{ std::forward<Lhs>(lhs) }))
    #endif
    { return std::forward<Rhs>(graph).expand(detail::processing_graph<Lhs>{ std::forward<Lhs>(lhs) }); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((detail::is_processing_graph<Rhs>{} &&
        (detail::invocable_convertible_with_apply<Lhs, std::tuple<>>{} && !is_integral_constant<Lhs>{})
    ))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(lhs))))
    #endif
    { return expand(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(lhs))); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((
            detail::is_processing_graph<Rhs>{}
        &&  !detail::is_upstream<Lhs>{}
        //TODO: line below to be fixed once processing_graph and processing_context will have been merged
        &&  !detail::is_source_builder<Lhs, processing_context<void, std::tuple_element_t<0, typename Rhs::terminal_cells_tuple>>>{}
        &&  !(std::is_invocable<Lhs>{} && !is_integral_constant<Lhs>{})
        &&  !detail::is_downstream<Lhs>{}
    ))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(std::forward<Rhs>(rhs), sample_from(std::forward<Lhs>(lhs))))
    #endif
    { return expand(std::forward<Rhs>(rhs), sample_from(std::forward<Lhs>(lhs))); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        !detail::is_processing_graph<Rhs>{} && detail::is_upstream<Rhs>{}
    ))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(detail::processing_graph<Rhs>{ std::forward<Rhs>(rhs) }, std::forward<Lhs>(lhs)))
    #endif
    { return expand(detail::processing_graph<Rhs>{ std::forward<Rhs>(rhs) }, std::forward<Lhs>(lhs)); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((!detail::is_processing_graph<Rhs>{} && detail::is_cell_builder<Rhs>{}))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(std::forward<Rhs>(rhs).build(), std::forward<Lhs>(lhs)))
    #endif
    { return expand(std::forward<Rhs>(rhs).build(), std::forward<Lhs>(lhs)); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((std::is_invocable<Rhs>{} && !is_integral_constant<Rhs>{}))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(apply(std::forward<Rhs>(rhs)), std::forward<Lhs>(lhs)))
    #endif
    { static_assert(!detail::returns_void<Rhs>{}, "given callable shall return something to be considered as a processing source");
      return expand(apply(std::forward<Rhs>(rhs)), std::forward<Lhs>(lhs)); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((
            !detail::is_processing_graph<Rhs>{}
        &&  !detail::is_cell_builder<Rhs>{}
        &&  !detail::is_upstream<Rhs>{}
        &&  !(std::is_invocable<Rhs>{} && !is_integral_constant<Rhs>{})
    ))>
    constexpr auto expand(Rhs&& rhs, Lhs&& lhs) const
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(expand(sample_from(std::forward<Rhs>(rhs)), std::forward<Lhs>(lhs)))
    #endif
    { return expand(sample_from(std::forward<Rhs>(rhs)), std::forward<Lhs>(lhs)); }

    template<class Lhs, class Rhs, class... Args>
    constexpr std::nullptr_t expand(Rhs&& rhs, Lhs&& lhs, Args...) const
    {
        static_assert(
            !detail::is_processing_graph<Rhs>{}
            || ( detail::is_processing_graph<Rhs>{}
                && (
                    detail::is_upstream<Lhs>{}
                    ||
                    detail::is_source_builder<Lhs>{}
                    ||
                    detail::invocable_convertible_with_apply<Lhs, std::tuple<>>{}
                    ||
                    (!std::is_invocable<Lhs>{} && !detail::is_downstream<Lhs>{})
                )
            ),
            "right-hand side of |= operator is a processing graph but left-hand side is not suitable (neither a source, a source builder nor something convertible into a source builder)"
        );

        static_assert(
            !detail::is_upstream<Rhs>{}
            || ( detail::is_upstream<Rhs>{}
                && (
                    detail::is_processing_graph<Lhs>{}
                    ||
                    detail::is_source_builder<Lhs>{}
                    ||
                    detail::invocable_convertible_with_apply<Lhs, std::tuple<>>{}
                    ||
                    (!std::is_invocable<Lhs>{} && !detail::is_downstream<Lhs>{})
                )
            ),
            "right-hand side of |= operator is an upstream graph but left-hand side is not suitable (neither a source builder nor something convertible into a source builder)"
        );

        static_assert(always_false<Rhs>{}, DFP_UNEXPECTED_ERROR);
        return nullptr;
    }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_graph<Rhs>{} && detail::is_cell_builder<Lhs>{}
    ))>
    constexpr auto collect(Rhs&& graph, Lhs&& cell_builder) const
    // NOTE that std::forward<Rhs> is required to allow non-copyable input graph to be "collected"
    // by taking leverage of move-construction
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(std::forward<Rhs>(graph).collect(std::forward<Lhs>(cell_builder)))
    #endif
    { return std::forward<Rhs>(graph).collect(std::forward<Lhs>(cell_builder)); }


    template<class Lhs, class Rhs, DFP_F_REQUIRES((
            detail::invocable_convertible_with_apply<Lhs, typename std::remove_cvref_t<Rhs>::terminal_cells_tuple>{}
    ))>
    constexpr auto collect(Rhs&& rhs, Lhs&& lhs) const
    //TODO: create and make usage of core::apply_node rather core::apply 
    #if !DFP_CPP14_SUPPORT
    ->
    decltype(collect(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(lhs))))
    #endif
    { return collect(std::forward<Rhs>(rhs), apply(std::forward<Lhs>(lhs))); }

    template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_graph<Rhs>{}
        ||
        detail::is_downstream<Lhs>{}
    )), class... Args>
    constexpr std::nullptr_t collect(Rhs&& rhs, Lhs&& lhs, Args...) const
    {
        static_assert(
            !detail::is_processing_graph<Rhs>{}
            || ( detail::is_processing_graph<Rhs>{}
                && (
                    /*(*/detail::is_cell_builder<Lhs>{}/* && !detail::is_source_builder<Lhs>{})*/
                    ||
                    detail::invocable_convertible_with_apply<Lhs, typename std::remove_cvref_t<Rhs>::terminal_cells_tuple>{}
                    ||
                    detail::invocable_convertible_with_execute<Lhs, typename std::remove_cvref_t<Rhs>::terminal_cells_tuple>{}
                )
            ),
            "right-hand side of += operator is a processing graph but left-hand side is not suitable (neither a node or sink builder nor something convertible into a node or sink builder)"
        );

        static_assert(
            !detail::is_upstream<Lhs>{},
            "left-hand side of += operator is unexpectedly an upstream graph while a node or sink builder is expected"
        );

//        static_assert(
//            !detail::is_source_builder<Lhs>{},
//            "left-hand side of += operator is unexpectedly a source builder while a node or sink builder is expected"
//        );

        static_assert(always_false<Rhs>{}, DFP_UNEXPECTED_ERROR);
        return nullptr;
    }

}; // class graph_designer_base



template<class... Caps>
struct graph_designer<std::tuple<Caps...>> : detail::graph_designer_impl<graph_designer_base, Caps...>
{};


template<>
struct graph_designer<std::tuple<>> : graph_designer_base
{
    static constexpr graph_designer_base instance()
    { return graph_designer_base{}; }
};// struct graph_designer<>


} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_GRAPH_DESIGNER_HPP
