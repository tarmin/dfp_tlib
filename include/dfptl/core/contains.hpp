/**
* @file core/contains.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CONTAINS_HPP
#define DFPTL_CORE_CONTAINS_HPP


#include "stl/type_traits.hpp"
#include "dfptl/core/cplusplus.hpp"
#include "dfptl/core/integral_const.hpp"
#include <boost/fusion/include/has_key.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{
struct empty_map;
}

namespace functions
{


template<class Searchable, class Key>
struct contains : boost::fusion::result_of::has_key<Searchable, Key>
{}; // struct contains


template<class Key>
struct contains<detail::empty_map, Key>  : bool_const<false>
{}; // struct contains<empty_map, ...>


} // namespace functions 
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_CONTAINS_HPP
