/**
* @file core/runnable_tree.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_RUNNABLE_TREE_HPP
#define DFPTL_CORE_RUNNABLE_TREE_HPP


#include "stl/type_traits.hpp"
#include "dfptl/core/cplusplus.hpp"
#include "dfptl/core/sink.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{

template<bool, class TerminalCellTuple, class Derived>
struct processing_graph_base;


}

///@addtogroup core_grp
///@{
/** @defgroup runnable_tree_grp "runnable" processing tree
* @brief Before being able to drain samples, a declarative processing tree shall be turned into an "runnable" tree.
* @{
**/

/**
* @brief Turns a declarative processing tree into a new tree which can be run
*
* At instance creation, this class simply copies or takes ownership of a given processing tree and performs its initialization 
**/
template<class ProcessingTree>
class runnable_tree
 :  public  runnable_sink<typename ProcessingTree::terminal_cell_type>,
    public  detail::processing_graph_base<
        false,
        std::tuple<runnable_sink<typename ProcessingTree::terminal_cell_type>>,
        runnable_tree<ProcessingTree>
    >
{
private:
    typedef detail::processing_graph_base<
        false,
        std::tuple<runnable_sink<typename ProcessingTree::terminal_cell_type>>,
        runnable_tree
    > graph_base_type;

    typedef typename ProcessingTree::terminal_cell_type sink_type;

public:
    // type of the uninitialized tree
    typedef ProcessingTree green_tree_type;

    /**
    * @brief Take ownership on the given processing tree and move it to runnable state
    * @param tree the declarative processing tree to appropriate
    **/
    constexpr runnable_tree(ProcessingTree&& tree)
     :  runnable_sink<sink_type>(std::move(tree).get_sink())
    {}

    /**
    * @brief Copy on the given processing tree and move it to runnable state
    * @param tree the declarative processing tree to copy
    **/
    constexpr runnable_tree(ProcessingTree const& tree)
     :  runnable_sink<sink_type>(tree.get_sink())
    {}
}; // struct runnable_tree


template<class ProcessingTree>
constexpr runnable_tree<std::remove_cvref_t<ProcessingTree>>
inline make_runnable(ProcessingTree&& processing_tree)
{ return { std::forward<ProcessingTree>(processing_tree) }; }


///@}
///@} core_grp
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_RUNNABLE_TREE_HPP
