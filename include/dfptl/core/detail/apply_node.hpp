/**
* @file core/detail/apply_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_APPLY_NODE_HPP
#define DFPTL_CORE_DETAIL_APPLY_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/stream_specifier.hpp"
#include "dfptl/core/detail/ebco_member.hpp"

#include "stl/utility.hpp"


//Internal purpose only
#define DFP_FUNCTION_FAILED_INPUT_REQUIREMENT(NODE_NAME, REQUIREMENT) \
namespace functions { \
template<class T = void, class = void> struct NODE_NAME \
{ static_assert(always_false<T>::value, #REQUIREMENT " not met for input sample of node " #NODE_NAME); }; \
}


//Internal purpose only
#define DFP_CONST_CALLABLE_TYPE(NODE_NAME) const_ ## NODE_NAME ## _callable_type

//Internal purpose only
#define DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME) supports_ ## NODE_NAME ## _const_callable


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief Provides member typedef type which is defined as the output type resulting from
* applying function-object @c Function on samples produced by the given @c InputCells parameter pack.
*
* @tparam Function is the function-object class (in sense of C++ STL) on which to retrieve the output result type.
* @tparam InputCells is a parameter pack of  processing cell types that will provides input samples for the function-object.
**/
template<class Function, class... InputCells>
struct result_sample_of
{
    //TODO: Note that std::remove_reference_t was introduced here as a simple workaround but
    // looks to be useless now
    typedef decltype(
        std::declval<Function>()(std::declval</*std::remove_reference_t<*/output_sample_t<InputCells>>/*>*/()...)
    ) type;
};

/**
* @brief Alias for @c result_sample_of<Function, InputCells>::type
* @sa result_sample_of
**/
template<class Function, class... InputCells>
using result_sample_of_t = typename result_sample_of<Function, InputCells...>::type;


/**
* @brief Provides member typedef type which is defined as the output type resulting from
* applying function-object @c Function on frames produced by the given @c InputCells parameter pack.
*
* @tparam Function is the function-object class (in sense of C++ STL) on which to retrieve the output result type.
* @tparam InputCells is a parameter pack of  processing cell types that will provides input samples for the function-object.
**/
template<class Function, class... InputCells>
struct result_frame_of
{
    //Note that std::remove_reference_t has been introduced here as a simple workaround and need
    // further analysis for rational.
    typedef decltype(
        std::declval<Function>()(std::declval<std::remove_reference_t<output_frame_t<InputCells>>>()...)
    ) type;
};

/**
* @brief Alias for @c result_frame_of<Function, InputCells>::type
* @sa result_sample_of
**/
template<class Function, class... InputCells>
using result_frame_of_t = typename result_frame_of<Function, InputCells...>::type;


template<class Descriptor, class... InputCells>
struct processing_result;

template<class Callable, class... InputCells>
struct processing_result<sample_based_processing<Callable>, InputCells...> : result_sample_of<Callable, InputCells...> {};

template<class Callable, class... InputCells>
struct processing_result<frame_based_processing<Callable>, InputCells...> : result_frame_of<Callable, InputCells...> {};

/**
* @brief Alias for @c processing_result<Descriptor, InputCells>::type
* @sa result_sample_of
**/
template<class Descriptor, class... InputCells>
using processing_result_t = typename processing_result<Descriptor, InputCells...>::type;


/**
* @brief Helper class to infer a stream specifier given the processing @c Descriptor
**/
template<class Descriptor, class Inputs, class InputIndexSequence>
struct apply_node_stream
{ static_assert(always_false<Descriptor>{}, DFP_UNEXPECTED_ERROR); };


template<class Callable, class Inputs, std::size_t... InputIds>
struct apply_node_stream<sample_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>
{ typedef atomic_stream_specifier<std::remove_cvref_t<result_sample_of_t<Callable, std::tuple_element_t<InputIds, Inputs>...>>> type; };


template<class Callable, class Inputs, std::size_t... InputIds>
struct apply_node_stream<frame_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>
{ typedef stream_specifier_from_frame<result_frame_of_t<Callable, std::tuple_element_t<InputIds, Inputs>...>> type; };


template<class Inputs, class InputIndexSequence, class Descriptor, class = void>
struct apply_node_impl
{ static_assert(always_false<apply_node_impl>{}, DFP_UNEXPECTED_ERROR); };


/**
* @brief Sample-based node which applies a function-object (as defined by STL) for each input stream sample.
*
* @tparam Inputs is the type list of the processing cells (source or node) bound to the input ports to this node.
* @tparam Descriptor is kernel to process the stream.
**/
template<class Inputs, class Descriptor>
struct apply_node
 :  apply_node_impl<Inputs, index_sequence_from_t<Inputs>, Descriptor>
{ using apply_node_impl<Inputs, index_sequence_from_t<Inputs>, Descriptor>::apply_node_impl; };


template<class Inputs, class InputIndexSequence, class Descriptor> class apply_regular_node
{ static_assert(always_false<Descriptor>{}, "Inputs template argument shall model tuple concept"); };


template<class Inputs, class InputIndexSequence, class Descriptor> class apply_unregular_node
{ static_assert(always_false<Descriptor>{}, "Inputs template argument shall model tuple concept"); };


template<class Inputs, std::size_t... InputIds, class Callable>
struct apply_node_impl<
    Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>,
    DFP_C_REQUIRES((supports_const_call_operator<Callable, output_sample_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>:      apply_regular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
{ using apply_regular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>::apply_regular_node; };


template<class Inputs, std::size_t... InputIds, class Callable>
struct apply_node_impl<
    Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>,
    DFP_C_REQUIRES((supports_const_call_operator<Callable, output_frame_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>:      apply_regular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
{ using apply_regular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>::apply_regular_node; };


template<class Inputs, std::size_t... InputIds, class Callable>
struct apply_node_impl<
    Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>,
    DFP_C_REQUIRES((supports_mutable_call_operator<Callable, output_sample_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>:      apply_unregular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
{ using apply_unregular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>::apply_unregular_node; };


template<class Inputs, std::size_t... InputIds, class Callable>
struct apply_node_impl<
    Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>,
    DFP_C_REQUIRES((supports_mutable_call_operator<Callable, output_frame_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>:      apply_unregular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
{ using apply_unregular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>::apply_unregular_node; };


template<class Inputs, std::size_t... InputIds, class Callable>
class apply_regular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
 :  public node<
        Inputs, typename apply_node_stream<sample_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_regular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
    >,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable> callable_member;
    typedef  node<
        Inputs, typename apply_node_stream<sample_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_regular_node
    >   node_type;

public:
    /**
    * @brief Creates a new instance of apply_regular_node.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr apply_regular_node(Inputs&& inputs, CallableArgs&&... callable_args)
     :  node_type{std::forward<Inputs>(inputs)}, callable_member(std::forward<CallableArgs>(callable_args)...) {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    constexpr output_sample_t<node_type> process_sample(input_sample_t<node_type, InputIds> const&... inputs) const
    { return callable_member::get()(inputs...); }
}; // class apply_regular_node<... sample_based_processing ...>


template<class Inputs, std::size_t... InputIds, class Callable>
class apply_regular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
 :  private ebco_member<Callable>,
    public node<
        Inputs, typename apply_node_stream<frame_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_regular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
    >
{
    typedef ebco_member<Callable>   callable_member;
    typedef  node<
        Inputs, typename apply_node_stream<frame_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_regular_node
    >   node_type;

public:
    typedef typename node_type::output_specifier_type output_specifier_type;

    /**
    * @brief Creates a new instance of apply_regular_node.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class T = output_specifier_type, class... CallableArgs, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr apply_regular_node(Inputs&& inputs, CallableArgs&&... callable_args)
     :  callable_member(std::forward<CallableArgs>(callable_args)...),
        node_type{std::forward<Inputs>(inputs)}
    {}

    template<class T = output_specifier_type, class... CallableArgs,
        DFP_F_REQUIRES((T::FRAME_EXTENT == DYNAMIC_EXTENT && !supports_output_frame_length<T, input_stream_specifier_t<node_type, InputIds>...>{}))
    >
    constexpr apply_regular_node(Inputs&& inputs, CallableArgs&&... callable_args)
     :  callable_member(std::forward<CallableArgs>(callable_args)...),
        node_type(
            std::forward<Inputs>(inputs),
            output_specifier_type{}.set_frame_length(std::get<0>(inputs).output_stream_specifier().get_frame_length())
        )
    {}

    template<class T = Callable, class... CallableArgs, DFP_F_REQUIRES((supports_output_frame_length<T, input_stream_specifier_t<node_type, InputIds>...>{}))>
    constexpr apply_regular_node(Inputs&& inputs, CallableArgs&&... callable_args)
     :  callable_member(std::forward<CallableArgs>(callable_args)...),
        node_type{std::forward<Inputs>(inputs), output_frame_length_(inputs)}
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    constexpr output_frame_t<node_type> process_frame(input_frame_t<node_type, InputIds> const&... inputs) const
    { return callable_member::get()(inputs...); }

private:
    constexpr std::size_t output_frame_length_(Inputs const& inputs) const
    { return callable_member::output_frame_length(std::get<InputIds>(inputs).output.stream_specifier()...); }
}; // class apply_regular_node<... , frame_based_processing ...>


template<class Inputs, std::size_t... InputIds, class Callable>
class apply_unregular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
   : public node<
        Inputs, typename apply_node_stream<sample_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_unregular_node<Inputs, std::index_sequence<InputIds...>, sample_based_processing<Callable>>
    >,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable>   callable_member;
    typedef node<
        Inputs, typename apply_node_stream<sample_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_unregular_node
    > node_type;

public:
    /**
    * @brief Creates a new instance of apply_unregular_node.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr apply_unregular_node(Inputs&& inputs, CallableArgs&&... callable_args)
        :   node_type{std::forward<Inputs>(inputs)}, callable_member(std::forward<CallableArgs>(callable_args)...) {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    //TODO: add specific implementation when returned type of function is output_sample_t const&
    output_sample_t<node_type> process_sample(input_sample_t<node_type, InputIds> const&... inputs)
    { return callable_member::get()(inputs...); }
}; // class apply_unregular_node<... sample_based_processing ...>


template<class Inputs, std::size_t... InputIds, class Callable>
class apply_unregular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
   : public node<
        Inputs, typename apply_node_stream<frame_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_unregular_node<Inputs, std::index_sequence<InputIds...>, frame_based_processing<Callable>>
    >,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable>   callable_member;
    typedef node<
        Inputs, typename apply_node_stream<frame_based_processing<Callable>, Inputs, std::index_sequence<InputIds...>>::type,
        apply_unregular_node
    > node_type;

public:
    typedef typename node_type::output_specifier_type output_specifier_type;

    /**
    * @brief Creates a new instance of apply_unregular_node.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class T = output_specifier_type, class... CallableArgs, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr apply_unregular_node(Inputs&& inputs, CallableArgs&&... callable_args)
     :  node_type{std::forward<Inputs>(inputs)}, callable_member(std::forward<CallableArgs>(callable_args)...)
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    //TODO: add specific implementation when returned type of function is output_sample_t const&
    output_frame_t<node_type> process_frame(input_frame_t<node_type, InputIds> const&... inputs)
    { return callable_member::get()(inputs...); }
}; // class apply_unregular_node<... frame_based_processing ...>


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_APPLY_NODE_HPP
