/**
* @file core/detail/placeholder.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PLACEHOLDER_HPP
#define DFPTL_CORE_DETAIL_PLACEHOLDER_HPP


#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/processing_context.hpp"
#include "dfptl/core/detail/cell_traits.hpp"
#include "dfptl/core/detail/placeholder_traits.hpp"

#include "stl/utility.hpp"
#include "stl/tuple.hpp"


namespace dfp
{
inline namespace core
{


namespace detail
{
struct cell_builder_tag {};

template<class CellArgs, template<class, class ...> class Cell, class CtorArgs, class PresetInputs, bool ImplicitRun>
class placeholder
{
    static_assert(
        is_specialization<CellArgs, std::tuple>{},
        "The provided CellArgs template argument for placeholder is not a std::tuple"
    );

    static_assert(
        is_specialization<CtorArgs, std::tuple>{},
        "The provided CtorArgs template argument for placeholder is not a std::tuple"
    );

    static_assert(
        is_specialization<PresetInputs, std::tuple>{},
        "The provided PresetInputs template argument for placeholder is not a std::tuple"
    );

    static_assert(always_false<CellArgs>{}, DFP_UNEXPECTED_ERROR);
};


template<class... CellArgs, template<class, class ...> class Cell, class ...CtorArgs, class... PresetInputs, bool ImplicitRun>
class placeholder /** @cond **/ <
    std::tuple<CellArgs...>, Cell, std::tuple<CtorArgs...>, std::tuple<PresetInputs...>, ImplicitRun
> /** @endcond **/
{
    typedef std::tuple<CellArgs...>     cell_args;
    typedef std::tuple<CtorArgs...>     ctor_args_type;
    typedef std::tuple<PresetInputs...>  preset_inputs_type;

    friend class placeholder<cell_args, Cell, ctor_args_type, preset_inputs_type, !ImplicitRun>;

protected:
    ctor_args_type ctor_args_;
    preset_inputs_type preset_inputs_;


public:
    typedef cell_builder_tag dfptl_tag;

    /** @ref dfp::core::placeholder::cell */
    template<class ProcessingContext> struct cell;

    template<class Tag, class... UpstreamCells>
    struct cell<core::processing_context<Tag, UpstreamCells...>>
    { typedef Cell<core::processing_context<Tag, UpstreamCells..., PresetInputs...>, CellArgs...> type; };

    template<class ProcessingContext> using cell_t = typename cell<ProcessingContext>::type;

    //Note that the split in the both template specializations is a workaround for old versin of GCC, with clang only one ctor is enough
    template<class Args, class Presets, DFP_F_REQUIRES((std::tuple_size<std::remove_cvref_t<Args>>{} > 0))>
    constexpr placeholder(Args&& args, Presets&& inputs)
        :   ctor_args_{std::forward<Args>(args)},  preset_inputs_(std::forward<Presets>(inputs)) {}

    template<class Args, class Presets, DFP_F_REQUIRES((std::tuple_size<std::remove_cvref_t<Args>>{} == 0))>
    constexpr placeholder(Args&&, Presets&& inputs)
        :   ctor_args_{},  preset_inputs_(std::forward<Presets>(inputs)) {}

    // TODO: usage of cellify removed as it is now handled at grap_designer layer (shall be only used for preset -- until preset is reworked to be based on processing_graph)
    template<class Tag, class... Inputs>
    constexpr cell_t<core::processing_context<Tag, Inputs...>>
    _build(Inputs&&... inputs) const
    { return  __build<Tag, Inputs...>(
        build_all_inputs<Tag>(index_sequence_from_t<preset_inputs_type>{}, std::forward<Inputs>(inputs)...),
        index_sequence_from_t<ctor_args_type>{}
    ); }

    // TODO: should take a processing_context in input rather than an tuple, shouldn't  it ? 
    template<class Tag, class... Inputs, std::size_t... InputIndexes>
    auto constexpr build(std::tuple<Inputs...> const& inputs_tuple, std::index_sequence<InputIndexes...>) const ->
    decltype(_build<Tag>(std::get<InputIndexes>(inputs_tuple)...))
    { return _build<Tag>(std::get<InputIndexes>(inputs_tuple)...); }

    template<class Tag, class... Inputs, std::size_t... InputIndexes>
    constexpr cell_t<core::processing_context<Tag, Inputs...>>
    build(std::tuple<Inputs...>&& inputs_tuple, std::index_sequence<InputIndexes...>) const
    { return _build<Tag>(std::get<InputIndexes>(std::move(inputs_tuple))... ); }

protected:
    template<class Tag, class... InputCells, std::size_t... ExtraIs>
    constexpr core::processing_context<Tag, InputCells..., PresetInputs...>
    build_all_inputs(std::index_sequence<ExtraIs...>, InputCells&&... inputs) const
    { return { std::forward<InputCells>(inputs)..., std::get<ExtraIs>(preset_inputs_)... }; }

    template<class Tag, class... InputCells, std::size_t... ArgIs>
    constexpr cell_t<core::processing_context<Tag, InputCells...>>
    __build(core::processing_context<Tag, InputCells..., PresetInputs...>&& context, std::index_sequence<ArgIs...>) const
    { return { std::move(context), std::get<ArgIs>(ctor_args_)... }; }
}; // class placeholder


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PLACEHOLDER_HPP
