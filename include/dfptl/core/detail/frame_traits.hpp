/**
* @file core/detail/frame_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_FRAME_TRAITS_HPP
#define DFPTL_CORE_DETAIL_FRAME_TRAITS_HPP


#include "dfptl/core/utility.hpp"
#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/detail/single_sample_frame.hpp"

#include <array>
#include <deque>
#include <vector>
#include <stddef.h>
#include "stl/span.hpp"


namespace dfp
{
inline namespace core
{


namespace detail
{
///@addtogroup core
///@{
///@addtogroup stream
///@{
///@addtogroup core_detail
///@{
/** @defgroup frame_traits Frame traits
* @brief Provides helper class to retrieve traits on @ref sample frame
* @{
**/


template<class Sample>
struct frame_traits_concept_check
{
    static_assert
    (
        !std::is_reference<Sample>{} || std::is_const<std::remove_reference_t<Sample>>{},
        "Sample template argument cannot be a mutable reference"
    );
};


template<class Frame>
struct frame_meta{}; // move to frame_meta.hpp in dfp::core namespace


template<class T, std::size_t N>
struct frame_meta<std::array<T, N>> : template_meta<std::array<T, N>>
{
    typedef std::array<T, N> frame_type;
    static constexpr std::size_t EXTENT = N;

    template<class Sample>
    constexpr frame_meta<std::array<Sample, N>> set_sample_type(type_meta<Sample> = type_meta<Sample>{}) const
    { return frame_meta<std::array<Sample, N>>{}; }

    template<std::size_t Length>
    constexpr frame_meta<std::array<T, Length>> set_length(size_t_const<Length> = size_t_const<Length>{}) const
    { return frame_meta<std::array<T, Length>>{}; }

    //TODO: build();
};


template<class T, std::size_t N>
struct frame_meta<upg::span<T, N>> : template_meta<upg::span<T, N>>
{
    typedef upg::span<T, N> frame_type;
    static constexpr std::size_t EXTENT = N;

    template<class Sample>
    constexpr frame_meta<upg::span<Sample, N>> set_sample_type(type_meta<Sample> = type_meta<Sample>{}) const
    { return frame_meta<upg::span<Sample, N>>{}; }

    template<std::size_t Length>
    constexpr frame_meta<upg::span<T, Length>> set_length(size_t_const<Length> = size_t_const<Length>{}) const
    { return frame_meta<upg::span<T, Length>>{}; }

    //TODO: build();
};


template<class T>
struct frame_meta<std::vector<T>> : template_meta<std::vector<T>>
{
    typedef std::vector<T> frame_type;
    static constexpr std::size_t EXTENT = DYNAMIC_EXTENT;
    template<class U> friend struct frame_meta;

    constexpr frame_meta() : initial_length_(/*default_frame_length_const<>{}*/0) {}

    template<class Sample>
    constexpr frame_meta<std::vector<Sample>> set_sample_type(type_meta<Sample> = type_meta<Sample>{}) const
    { return frame_meta<std::vector<Sample>>(initial_length_); }

    template<std::size_t Length>
    constexpr frame_meta<std::vector<T>> set_length(size_t_const<Length> = size_t_const<Length>{}) const
    { return frame_meta(Length); }

    //TODO: build();

private:
    explicit constexpr frame_meta(std::size_t length) : initial_length_(length)
    {}

    std::size_t const initial_length_;
};


template<class T>
struct frame_meta<single_sample_frame<T>> : template_meta<single_sample_frame<T>>
{
    typedef single_sample_frame<T> frame_type;
    static constexpr std::size_t EXTENT = 1;
    template<class U> friend struct frame_meta;

    template<class Sample>
    constexpr frame_meta<single_sample_frame<Sample>> set_sample_type(type_meta<Sample> = type_meta<Sample>{}) const
    { return frame_meta<single_sample_frame<Sample>>{}; }

    template<std::size_t Length>
    constexpr frame_meta<single_sample_frame<T>> set_length(size_t_const<Length> = size_t_const<Length>{}) const
    { static_assert(Length == 1, "a single_sample frame has a fixed length of 1"); return frame_meta<single_sample_frame<T>>{}; }
};


/**
* shall provide:
* * typedef sample_type
* * template<class NewSample> struct set_sample
**/
template<class Frame>
struct frame_traits
{ static_assert(always_false<Frame>{}, "frame_traits class template shall be specialized for Frame"); };


template<class T, std::size_t N>
struct frame_traits<std::array<T, N>> : frame_traits_concept_check<T>
{
    typedef T sample_type;
    static constexpr std::size_t EXTENT = N;

    template<class NewSample>
    struct set_sample{ typedef std::array<NewSample, N> type; };

    template<std::size_t NewSize>
    //TODO: rename into set_extent
    struct set_size{ typedef std::array<T, NewSize> type; };
};


template<class T>
struct not_applicable
{ static_assert(always_false<T>{}, "Helper class cannot be resolved for given T"); };


template<class T>
struct frame_traits<std::vector<T>> : frame_traits_concept_check<T>
{
    typedef T sample_type;
    static constexpr std::size_t EXTENT = DYNAMIC_EXTENT;

    template<class NewSample>
    struct set_sample{ typedef std::vector<NewSample> type; };

    template<std::size_t NewSize>
    struct set_size{ typedef not_applicable<frame_traits> type; };
};


template<class T>
struct frame_traits<std::deque<T>> : frame_traits_concept_check<T>
{
    typedef T sample_type;
    static constexpr std::size_t EXTENT = DYNAMIC_EXTENT;

    template<class NewSample>
    struct set_sample{ typedef std::deque<NewSample> type; };
};


template<class T, std::size_t Size>
struct frame_traits<upg::span<T, Size>> : frame_traits_concept_check<T>
{
    typedef T sample_type;
    static constexpr std::size_t EXTENT = Size;

    template<class NewSample>
    struct set_sample{ typedef upg::span<NewSample, Size> type; };

    template<std::size_t NewSize>
    struct set_size{ typedef upg::span<T, NewSize> type; };
};


template<class Sample>
struct frame_traits<single_sample_frame<Sample>> : frame_traits_concept_check<Sample>
{
    typedef Sample sample_type;
    static constexpr std::size_t EXTENT = 1;

    template<class NewSample>
    struct set_sample{ typedef single_sample_frame<NewSample> type; };
};


/**
* @brief Provides the member constant @c value equal to true if T is a type or reference type modeling the frame concept.
* For any other type, value is false.
**/
template<class T, class = void>
struct models_frame_concept : std::false_type {};

template<class T>
struct models_frame_concept<T, DFP_C_REQUIRES((std::is_reference<T>{}))>
    :   models_frame_concept<std::remove_reference_t<T>> {};


template<class T>
struct models_frame_concept<T, std::void_t<
    typename T::size_type,
    typename T::iterator,
    typename T::const_iterator,
    decltype(std::declval<T>().size()),
    decltype(std::declval<T>().empty()),
    decltype(std::declval<T>().cbegin()),
    decltype(std::declval<T>().cend()),
    decltype(std::declval<T>().begin()),
    decltype(std::declval<T>().end())
> > : std::conjunction<std::is_copy_constructible<T>, std::is_move_constructible<T>> {};


template<class T, class = void>
struct is_frame_traits : std::false_type {};

template<class T>
struct is_frame_traits<
    T,
    DFP_C_REQUIRES((std::is_constructible<typename T::sample_type>{} && std::is_class<typename T::template set_sample<detail::null_tag>::type>{}))
> : std::true_type {};


/**
* @brief Alias to @c iterator type of the given [container](https://en.cppreference.com/w/cpp/named_req/Container) @c T
**/
template<class T>
using iterator_t = typename T::iterator;


/**
* @brief Alias to @c const_iterator type of the given [container](https://en.cppreference.com/w/cpp/named_req/Container) @c T
**/
template<class T>
using const_iterator_t = typename T::const_iterator;


/**
* @brief Alias to @c iterator type returned by end() operation for the given container @c T
**/
//TODO: to be removed
//template<class T>
//using end_iterator_t = decltype(std::declval<T>().end());



///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_FRAME_TRAITS_HPP
