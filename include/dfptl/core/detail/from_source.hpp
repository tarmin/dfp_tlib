/**
* @file core/detail/from_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_FROM_SOURCE_HPP
#define DFPTL_CORE_DETAIL_FROM_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/core/probe.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


#define DFP_CORE_FROM core::from


template<class Context, class FromTag, class = void>
struct from_source_output_specifier
{ typedef atomic_stream_specifier<int> type; };

template<class Context, class FromTag>
struct from_source_output_specifier<Context, FromTag, DFP_C_REQUIRES((typename Context::template has_tag<FromTag>{}))>
{ typedef output_stream_specifier_t<typename Context::template at_tag_t<FromTag>> type; };


template<class Context, class FromTag>
class from_source
 :  public  source<
        Context, typename from_source_output_specifier<Context, FromTag>::type, from_source<Context, FromTag>
    >
{
    // alias for the base type
    typedef source<
        Context, typename from_source_output_specifier<Context, FromTag>::type, from_source
    > source_type;

    static_assert(
        typename Context::template has_tag<FromTag>{},
        "given FromTag shall have been associated to a " DFP_STRINGIFY(DFP_CORE_PROBE) " in upstream part before the "  DFP_STRINGIFY(DFP_CORE_FROM) " source"
    );

public:
    using source_type::source_type;

    template<class InitStorage>
    void initialize_upwards(InitStorage init_storage)
    {
        static_assert(
            boost::fusion::result_of::has_key<InitStorage, keys::probe_config<FromTag>>::type::value,
            "given FromTag of " DFP_STRINGIFY(DFP_CORE_FROM) " have been associated to a cell in upstream part but not to a " DFP_STRINGIFY(DFP_CORE_PROBE)
        );
        frame_ = boost::fusion::at_key<keys::probe_config<FromTag>>(init_storage);
    }

    constexpr output_frame_t<source_type> const& acquire_frame() const
    { return *frame_; }

private:
    output_frame_t<source_type> const* frame_;
}; // class from_source


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_FROM_SOURCE_HPP
