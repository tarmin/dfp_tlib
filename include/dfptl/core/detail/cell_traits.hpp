/**
* @file core/detail/cell_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_CELL_TRAITS_HPP
#define DFPTL_CORE_DETAIL_CELL_TRAITS_HPP


#include "dfptl/core/detail/port_traits.hpp"
#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/cell_traits.hpp"
#include "dfptl/core/utility.hpp"

#include "stl/experimental/type_traits.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
///@addtogroup processing_cell
///@{
///@addtogroup core_detail
///@{
/** @defgroup cell_traits Processing cell traits
* @brief Provides helper class to retrieve traits on @ref node, @ref source and @ref sink
* @{
**/


/**
* @brief If @c T is an upstream processing cell, i.e. exposes an @ref output_port type,
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* In DFPTL @ref node "nodes" and @ref source "sources" are basically upstream processing cells.
* Upstream cells can deliver data for the down part of a processing branch.
* On the contrary @ref sink "sinks" are not upstream elements as they cannot deliver any data.
*
* @tparam T the type or reference type to test
**/
//TODO: replace with some models_upstream_cell helper
template<class T, class = void>
struct is_upstream : std::false_type {};

template<class T>
struct is_upstream<T, DFP_C_REQUIRES((is_output_port<typename std::remove_cvref_t<T>::output_port_type>{}))> : std::true_type {};


/**
* @brief If @c T is a downstream processing cell, i.e. exposes an @ref input_port type,
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* In DFPTL @ref node "nodes" and @ref sink "sinks" are basically downstream processing cells.
* Downstream cells can consume data from the up part of a processing branch.
* On the contrary @ref source "sources" are not downstream elements as they cannot consume any data
* (in the processing branch).
*
* @tparam T the type or reference type to test
**/
//TODO: replace with some models_upstream_cell helper
template <class T, class = void>
struct is_downstream : std::false_type {};

template <class T>
struct is_downstream<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_downstream<std::remove_reference_t<T>> {};

template <class T>
struct is_downstream<T, DFP_C_REQUIRES((is_input_port<typename T::template input_port_type<0>>{}))> : std::true_type {};
//struct is_downstream<T, DFP_C_REQUIRES((are_input_ports<typename T::input_ports_type>::value))> : std::true_type {};


/**
* @brief If T is a downstream or upstream processing cell,
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* In DFPTL @ref node "nodes", @ref source "sources" and @ref sink "sinks" are the processing cells.
*
* @tparam T the type or reference type to test
*
* @sa is_downstream is_upstream
**/
template<class T, class = void>
struct is_cell : std::false_type {};

template<class T>
struct is_cell<T, DFP_C_REQUIRES((is_upstream<T>::value || is_downstream<T>::value))> : std::true_type {};


/**
* @brief If @c T is a processing source i.e. inherits from @ref source,
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template <class T, class Enable = void>
struct is_source : std::false_type {};

template <class T>
struct is_source<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_source<std::remove_reference_t<T>> {};

template <class T>
struct is_source<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_cell_tag, source_tag>{}))> : std::true_type {};


template <class T, class Runner, class Enable = void>
struct is_sink_impl : std::false_type {};

template <class T, template<class> class Runner>
struct is_sink_impl<T, Runner<typename T::derived>, DFP_C_REQUIRES((std::is_same<typename T::dfp_cell_tag, sink_tag>{}))> : std::true_type {};


template<class T>
using inner_runner_type_t = typename T::runner_type;


/**
* @brief If @c T is a processing sink i.e. inherits from @ref sink,
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template <class T, class = void>
struct is_sink : std::false_type {};

template <class T>
struct is_sink<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_sink<std::remove_reference_t<T>> {};

template <class T>
struct is_sink<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_cell_tag, sink_tag>{}))> : std::true_type {};


/**
* @brief If @c T is a processing node i.e. inherits from @ref node,
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template <class T, class Enable = void>
struct is_node : std::false_type {};

template <class T>
struct is_node<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_node<std::remove_reference_t<T>> {};

template <class T>
struct is_node<T,DFP_C_REQUIRES((std::is_same<typename T::dfp_cell_tag, node_tag>{}))> : std::true_type {};


/**
* @brief If @c T is a processing graph
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template<class T, class = void>
struct is_processing_graph : std::false_type {};

template<class T>
struct is_processing_graph<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_processing_graph<std::remove_reference_t<T>> {};

template<class T>
struct is_processing_graph<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_tag, processing_graph_tag>{}))> : std::true_type {};


template<class T, class = void>
struct is_processing_tree : std::false_type {};

template<class T>
struct is_processing_tree<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_processing_tree<std::remove_reference_t<T>> {};

template<class T>
struct is_processing_tree<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_tag, processing_tree_tag>{}))> : std::true_type {};


template<class T, class = void>
struct is_processing_context : std::false_type {};

template<class T>
struct is_processing_context<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_processing_context<std::remove_reference_t<T>> {};

template<class T>
struct is_processing_context<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_tag, context_tag>{}))> : std::true_type {};


/**
* @brief Provides member typedef @c type which is defined as the @ref output_port "output port" type
* of the given upstream cell @c T.
*
* @remark @c T shall be an upstream cell or compile-time assertion failure will happen.
*
* @tparam T is the upstream cell type (aka @ref node or @ref source) on which to retrieve the output port type.
**/
template<class T, class = void>
struct cell_output_port
{
    static_assert(is_upstream<T>{}, "input T template parameter shall be an upstream cell");
    typedef typename std::remove_reference_t<T>::output_port_type type;
};

template<class T>
struct cell_output_port<T, DFP_C_REQUIRES((std::is_const<T>{}))>
{ typedef constify_t<typename cell_output_port<std::remove_const_t<T> >::type> type; };

/**
* @brief type alias for cell_output_port<...>::type
* @related cell_output_port
**/
template<class T>
using cell_output_port_t = typename cell_output_port<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the @ref input_port "input port" type
* of the given downstream cell @c T.
*
* @remark @c T shall be a downstream cell or compile-time assertion failure will happen.
*
* @tparam T is the downstream cell type (aka @ref node or @ref sink) on which to retrieve the input port type.
**/
template<class T, std::size_t Index, class = DFP_CONCEPT_ASSERT((is_downstream<T>))>
struct cell_input_port
{ typedef typename std::remove_reference<T>::type::template input_port_type<Index> type; };

template<class T, class = DFP_CONCEPT_ASSERT((is_downstream<T>))>
struct cell_input_ports
{ typedef typename std::remove_reference<T>::type::downstream_cell_type::input_ports_type type; };

/**
* @brief type alias for cell_input_port<...>::type
* @related cell_input_port
**/
template<class T, std::size_t Index = 0>
using cell_input_port_t = typename cell_input_port<T, Index>::type;
template<class T>
using cell_input_ports_t = typename cell_input_ports<T>::type;


/**
* @brief If @c T is a sample-based @ref source (exporting some @c acquire_sample())
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref source is sample-based if it exports publicly one of the following (possibly template) methods
* @code
* port_sample_t< cell_output_port_t<T> > acquire_sample()
* port_sample_t< cell_output_port_t<T> > acquire_sample() const
* port_sample_t< cell_output_port_t<T> > const& acquire_sample()
* port_sample_t< cell_output_port_t<T> > const& acquire_sample() const
* @endcode
* 2 first overloadings should be preferred when returned type is an integral type.
*
* @note The @c port_sample_t<cell_output_port_t<T>> type is equivalent to @c output_sample_t<T>
*
* @remark T shall be a @ref source or compile-time assertion failure will happen.
*
* @sa output_sample_t
**/
template<class T, class = void>
struct source_produces_sample : std::false_type
{ static_assert(is_source<T>{}, "Given template argument is not a source (does not publicly inherit from class source)"); };

template<class T>
struct source_produces_sample<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_source<T>{}))>
    :   source_produces_sample<std::remove_reference_t<T>> {};

template<class T, class = void>
struct supports_acquire_sample : std::false_type {};

template<class T, class = void>
struct supports_const_acquire_sample : std::false_type {};

template<class T>
struct source_produces_sample<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_source<T>{}))>
 :  std::disjunction<supports_acquire_sample<T>, supports_const_acquire_sample<T>> {};

///@cond
template<class T>
struct supports_acquire_sample<T, DFP_C_REQUIRES((
    always_true<decltype(static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)()>(&T::acquire_sample))>{}
))> : std::true_type {};

template<class T>
struct supports_acquire_sample<T, DFP_C_REQUIRES((
    always_true<decltype(static_cast<port_sample_t<cell_output_port_t<T>> (T::*)()>(&T::acquire_sample))>{}
))> : std::true_type {};

template<class T>
struct supports_const_acquire_sample<T, DFP_C_REQUIRES((
    always_true<decltype(static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)() const>(&T::acquire_sample))>{}
))> : std::true_type {};

template<class T>
struct supports_const_acquire_sample<T, DFP_C_REQUIRES((
    always_true<decltype(static_cast<port_sample_t<cell_output_port_t<T>> (T::*)() const>(&T::acquire_sample))>{}
))> : std::true_type {};
///@endcond


/**
* @brief If @c T is a frame-based @ref source (exporting some @c acquire_frame())
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref source is frame-based if it exports publicly one of the following (possibly template) methods
* @code
* port_frame_t< cell_output_port_t<T> > acquire_frame()
* port_frame_t< cell_output_port_t<T> > acquire_frame() const
* port_frame_t< cell_output_port_t<T> > const& acquire_frame()
* port_frame_t< cell_output_port_t<T> > const& acquire_frame() const
* @endcode
* 2 first overloadings should be preferred when returned type is an integral type.
*
* @note The @c port_frame_t<cell_output_port_t<T>> type is equivalent to @c output_frame_t<T>
*
* @remark T shall be a @ref source or compile-time assertion failure will happen.
*
* @sa output_frame_t
**/
template<class T, class = void>
struct source_produces_frame : std::false_type
{ static_assert(is_source<T>{}, "Given template argument is not a source (does not publicly inherit from class source)"); };

template<class T>
struct source_produces_frame<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_source<T>{}))>
 :  source_produces_frame<std::remove_reference_t<T>> {};

template<class T, class = void>
struct supports_acquire_frame : std::false_type {};

template<class T, class = void>
struct supports_const_acquire_frame : std::false_type {};

template<class T>
struct source_produces_frame<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_source<T>{}))>
 :  std::disjunction<supports_acquire_frame<T>, supports_const_acquire_frame<T>> {};

///@cond
template<class T>
struct supports_acquire_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)()>(&T::acquire_frame)
)>{}))> : std::true_type {};

template<class T>
struct supports_acquire_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)()>(&T::acquire_frame)
)>{}))> : std::true_type {};

template<class T>
struct supports_const_acquire_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)() const>(&T::acquire_frame)
)>{}))> : std::true_type {};

template<class T>
struct supports_const_acquire_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)() const>(&T::acquire_frame)
)>{}))> : std::true_type {};
///@endcond


/**
* @brief If @c T is a stream-based @ref source (exporting some @c acquire_stream())
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref source is stream-based if it exports publicly one of the following (possibly template) methods
* @code
* port_stream_t< cell_output_port_t<T> > acquire_stream()
* port_stream_t< cell_output_port_t<T> > acquire_stream() const
* @endcode
*
* @note The @c port_stream_t<cell_output_port_t<T>> type is equivalent to @c output_stream_t<T>
*
* @remark T shall be a @ref source or compile-time assertion failure will happen.
*
* @sa output_frame_t
**/
template<class T, class = void>
struct source_produces_stream : std::false_type
{ static_assert(is_source<T>{}, "Given template argument is not a source (does not publicly inherit from class source)"); };

template<class T>
struct source_produces_stream<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_source<T>{}))>
 :  source_produces_stream<std::remove_reference_t<T>> {};

template<class T, class = void>
struct supports_acquire_stream : std::false_type {};

template<class T, class = void>
struct supports_const_acquire_stream : std::false_type {};

template<class T>
struct source_produces_stream<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_source<T>{}))>
 :  std::disjunction<supports_acquire_stream<T>, supports_const_acquire_stream<T>> {};

///@cond
template<class T>
struct supports_acquire_stream<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<cell_output_port_t<T>> (T::*)()>(&T::acquire_stream)
)>{}))> : std::true_type {};


template<class T>
struct supports_const_acquire_stream<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<cell_output_port_t<T>> (T::*)() const>(&T::acquire_stream)
)>{}))> : std::true_type {};
///@endcond


template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_release_sample : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_release_frame : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_release_stream : std::false_type {};


///@cond
template<class T, class... InputPorts>
struct supports_release_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_sample_t<InputPorts> const&...)>(&T::release_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_release_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_sample_t<InputPorts> const&...) const>(&T::release_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_release_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_frame_t<InputPorts> const&...)>(&T::release_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_release_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_frame_t<InputPorts> const&...) const>(&T::release_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_release_stream<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_stream_t<InputPorts>&&...)>(&T::release_stream)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_release_stream<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(port_stream_t<InputPorts>&&...) const>(&T::release_stream)
)>{}))> : std::true_type {};
///@endcond


/**
* @brief If @c T is a sample-based @ref node
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref node is sample-based if it exposes publicly one of the following (possibly template) methods
* @code
* port_sample_t< cell_output_port_t<T> > process_sample(port_sample_t< cell_input_port_t<T> > const&)
* port_sample_t< cell_output_port_t<T> > process_sample(port_sample_t< cell_input_port_t<T> > const&) const
* port_sample_t< cell_output_port_t<T> > const& process_sample(port_sample_t< cell_input_port_t<T> > const&)
* port_sample_t< cell_output_port_t<T> > const& process_sample(port_sample_t< cell_input_port_t<T> > const&) const
* port_sample_t< cell_output_port_t<T> > const& process_stream(port_stream_t< cell_input_port_t<T> >&&)
* port_sample_t< cell_output_port_t<T> > const& process_sample(port_stream_t< cell_input_port_t<T> >&&) const
* @endcode
* 2 first overloadings should be preferred when return type is an integral type.
*
* @note The @c port_sample_t<cell_input_port_t<T>> type is equivalent to @c input_sample_t<T>,
* and @c port_sample_t<cell_output_port_t<T>> @type is equivalent to @c output_sample_t<T>
* and @c port_stream_t<cell_output_port_t<T>> @type is equivalent to @c output_stream_t<T>
*
* @remark @c T shall be a @ref node or compile-time assertion failure will happen.
*
* @sa input_sample_t, output_sample_t
**/
template<class T, class = void>
struct node_produces_sample : std::false_type
{ static_assert(is_node<T>{}, "Given template argument is not a node (does not publicly inherit from class node)"); };

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_process_sample : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_const_process_sample : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_process_stream_returning_sample : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_const_process_stream_returning_sample : std::false_type {};

template<class T>
struct node_produces_sample<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_node<T>{}))>
 :  node_produces_sample<std::remove_reference_t<T>> {};

template<
    class T, std::size_t ForwardedInputIndex = typename T::output_port_type::template get_forwarded_input<>{},
    class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void
>
struct supports_forward_sample : std::false_type {};
template<class T, class InputPorts>
struct supports_forward_sample<T, SIZE_MAX, InputPorts> : std::false_type {};


template<
    class T, std::size_t ForwardedInputIndex = typename T::output_port_type::template get_forwarded_input<>{},
    class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void
>
struct supports_const_forward_sample : std::false_type {};
template<class T, class InputPorts>
struct supports_const_forward_sample<T, SIZE_MAX, InputPorts> : std::false_type {};

template<class T>
struct node_produces_sample<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_node<T>{}))> : std::bool_constant<
    supports_process_sample<T>{} || supports_const_process_sample<T>{} ||
    supports_process_stream_returning_sample<T>{} || supports_const_process_stream_returning_sample<T>{} ||
    supports_forward_sample<T>{} || supports_const_forward_sample<T>{}
> {};

///@cond
template<class T, class... InputPorts>
struct supports_process_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> (T::*)(port_sample_t<InputPorts> const&...)>(&T::process_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> (T::*)(port_sample_t<InputPorts> const&...) const>(&T::process_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_process_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)(port_sample_t<InputPorts> const&...)>(&T::process_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)(port_sample_t<InputPorts> const&...) const>(&T::process_sample)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_process_stream_returning_sample< T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> (T::*)(port_stream_t<InputPorts>&&...)>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, class... InputPorts>
struct supports_process_stream_returning_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)(port_stream_t<InputPorts>&&...)>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_stream_returning_sample< T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> (T::*)(typename port_stream_t<InputPorts>::to_const::type&&...) const>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_stream_returning_sample<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<cell_output_port_t<T>> const& (T::*)(typename port_stream_t<InputPorts>::to_const::type&&...) const>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, std::size_t I, class... InputPorts>
struct supports_forward_sample<T, I, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<size_t_const<I> (T::*)(port_sample_t<InputPorts> const&...)>(&T::forward_sample)
)>{}))> : std::true_type {};

template<class T, std::size_t I, class... InputPorts>
struct supports_const_forward_sample<T, I, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<size_t_const<I> (T::*)(port_sample_t<InputPorts> const&...) const>(&T::forward_sample)
)>{}))> : std::true_type {};
///@endcond


/**
* @brief If @c T is a frame-based @ref node
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref node is frame-based if it exposes publicly one of the following (possibly template) methods
* @code
* port_frame_t< cell_output_port_t<T> > process_frame(port_frame_t< cell_input_port_t<T> > const&)
* port_frame_t< cell_output_port_t<T> > process_frame(port_frame_t< cell_input_port_t<T> > const&) const
* port_frame_t< cell_output_port_t<T> > const& process_frame(port_frame_t< cell_input_port_t<T> > const&)
* port_frame_t< cell_output_port_t<T> > const& process_frame(port_frame_t< cell_input_port_t<T> > const&) const
* port_frame_t< cell_output_port_t<T> > const& process_stream(port_stream_t< cell_input_port_t<T> >&&)
* port_frame_t< cell_output_port_t<T> > const& process_stream(port_stream_t< cell_input_port_t<T> >&&) const
* @endcode
* 2 first overloadings should be preferred when return type is an integral type.
*
* @note The @c port_frame_t<cell_input_port_t<T>> type is equivalent to @c input_frame_t<T>,
* @c port_frame_t<cell_output_port_t<T>> @type is equivalent to @c output_frame_t<T>
* and @c port_stream_t<cell_output_port_t<T>> @type is equivalent to @c output_stream_t<T>
*
* @remark @c T shall be a @ref node or compile-time assertion failure will happen.
*
* @sa input_frame_t, output_frame_t
**/
template<class T, class = void>
struct node_produces_frame : std::false_type
{ static_assert(is_node<T>{}, "Given template argument is not a node (does not publicly inherit from class node)"); };

template<class T, class Inputs = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_process_frame : std::false_type {};

template<class T, class Inputs = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_const_process_frame : std::false_type {};

template<class T, class Inputs = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_process_stream_returning_frame : std::false_type {};

template<class T, class Inputs = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_const_process_stream_returning_frame : std::false_type {};

template<class T>
struct node_produces_frame<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_node<T>{}))>
 :  node_produces_frame<std::remove_reference_t<T>> {};

template<
    class T, std::size_t ForwardedInputIndex = typename T::output_port_type::template get_forwarded_input<>{},
    class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void
>
struct supports_forward_frame : std::false_type {};
template<class T, class InputPorts>
struct supports_forward_frame<T, SIZE_MAX, InputPorts> : std::false_type {};


template<
    class T, std::size_t ForwardedInputIndex = typename T::output_port_type::template get_forwarded_input<>{},
    class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void
>
struct supports_const_forward_frame : std::false_type {};
template<class T, class InputPorts>
struct supports_const_forward_frame<T, SIZE_MAX, InputPorts> : std::false_type {};

template<class T>
struct node_produces_frame<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_node<T>{}))> : std::bool_constant<
    supports_process_frame<T>{} || supports_const_process_frame<T>{} ||
    supports_process_stream_returning_frame<T>{} || supports_const_process_stream_returning_frame<T>{} ||
    supports_forward_frame<T>{} || supports_const_forward_frame<T>{}
> {};

///@cond
template<class T, class... InputPorts>
struct supports_process_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)(port_frame_t<InputPorts> const&...)>(&T::process_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)(port_frame_t<InputPorts> const&...) const>(&T::process_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_process_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)(port_frame_t<InputPorts> const&...)>(&T::process_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)(port_frame_t<InputPorts> const&...) const>(&T::process_frame)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_process_stream_returning_frame< T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)(port_stream_t<InputPorts>&&...)>(&T::process_stream)
)>{})) > : std::true_type {};
template<class T, class... InputPorts>
struct supports_process_stream_returning_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)(port_stream_t<InputPorts>&&...)>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_stream_returning_frame< T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> (T::*)(typename port_stream_t<InputPorts>::to_const::type&&...) const>(&T::process_stream)
)>{})) > : std::true_type {};
template<class T, class... InputPorts>
struct supports_const_process_stream_returning_frame<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<cell_output_port_t<T>> const& (T::*)(typename port_stream_t<InputPorts>::to_const::type&&...) const>(&T::process_stream)
)>{})) > : std::true_type {};

template<class T, std::size_t I, class... InputPorts>
struct supports_forward_frame<T, I, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<size_t_const<I> (T::*)(port_frame_t<InputPorts> const&...)>(&T::forward_frame)
)>{}))> : std::true_type {};

template<class T, std::size_t I, class... InputPorts>
struct supports_const_forward_frame<T, I, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<size_t_const<I> (T::*)(port_frame_t<InputPorts> const&...) const>(&T::forward_frame)
)>{}))> : std::true_type {};

///@endcond


/**
* @brief If @c T is a stream-based @ref node
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* A @ref node is stream-based if it exposes publicly one of the following (possibly template) methods
* @code
* port_stream_t< cell_output_port_t<T> > process_stream(port_stream_t< cell_input_port_t<T> >&&)
* port_stream_t< cell_output_port_t<T> > process_stream(port_stream_t< cell_input_port_t<T> >&&) const
* @endcode
*
* @note The @c port_stream_t<cell_input_port_t<T>> type is equivalent to @c input_stream_t<T>
*
* @remark @c T shall be a @ref node or compile-time assertion failure will happen.
*
* @sa input_stream_t, output_stream_t
**/
template<class T, class = void>
struct node_produces_stream : std::false_type
{ static_assert(is_node<T>{}, "Given template argument is not a node (does not publicly inherit from class node)"); };

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_process_stream : std::false_type {};

template<class T, class InputPorts = typename T::downstream_cell_type::input_ports_type, class = void>
struct supports_const_process_stream : std::false_type {};

template<class T>
struct node_produces_stream<T, DFP_C_REQUIRES((std::is_reference<T>{} && is_node<T>{}))>
 :  node_produces_stream<std::remove_reference_t<T>> {};

template<class T>
struct node_produces_stream<T, DFP_C_REQUIRES((!std::is_reference<T>{} && is_node<T>{}))>
 :  std::disjunction<supports_process_stream<T>, supports_const_process_stream<T>>
{};

///@cond
template<class T, class... InputPorts>
struct supports_process_stream<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<cell_output_port_t<T>> (T::*)(port_stream_t<InputPorts>&&...)>(&T::process_stream)
)>{}))> : std::true_type {};

template<class T, class... InputPorts>
struct supports_const_process_stream<T, std::tuple<InputPorts...>, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<cell_output_port_t<T>> (T::*)(port_stream_t<InputPorts>&&...) const>(&T::process_stream)
)>{}))> : std::true_type {};


template<std::size_t Id, class T>
struct processing_input
{ //TODO: assert T is_processing_context
    typedef std::tuple_element_t<Id, T> type;
};
template<std::size_t Id, class T>
using processing_input_t = typename processing_input<Id, T>::type;


/**
* @brief If @c T is a node which provides a processing function returning a reference on a frame or a sample
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* @remark T shall be a sample-based or frame-based node or compile-time assertion failure will happen.
*
* @sa node_produces_frame, node_produces_sample
**/
template<
    class T, class Context = typename T::downstream_cell_type::input_cells_type,
    class InputsIndexSequence = typename T::downstream_cell_type::input_cells_type::upstream_index_sequence_type, class = void
>
struct node_applies_in_place_processing : std::false_type
{
    //TODO: static_assert(is_processing_context<Context>{}, DFP_UNEXPECTED_ERROR);
    static_assert(node_produces_frame<T>{} || node_produces_sample<T>{}, DFP_UNEXPECTED_ERROR);
};

template<class T, class Context, class InputsIndexSequence>
struct node_applies_in_place_processing<T, Context, InputsIndexSequence, DFP_C_REQUIRES((
    std::bool_constant<std::is_lvalue_reference<std::result_of_t<decltype(&T::process_frame)()>>{}>{}
))> {};

template<class T, class Context, std::size_t... InputIds>
struct node_applies_in_place_processing<T, Context, std::index_sequence<InputIds...>, DFP_C_REQUIRES((node_produces_sample<T>{}))>
 :  std::conjunction<
        std::is_lvalue_reference<std::invoke_result_t<decltype(&T::process_sample), T, port_sample_t<cell_output_port_t<processing_input_t<InputIds, Context>>> const&...>>,
        std::negation<std::is_const<std::remove_reference_t<std::invoke_result_t<decltype(&T::process_sample), T, port_sample_t<cell_output_port_t<processing_input_t<InputIds, Context>>> const&...>>>>
    > {};


template<class T, class Context>
struct node_applies_in_place_processing<T, Context, DFP_C_REQUIRES((
    std::bool_constant<std::is_lvalue_reference<std::result_of_t<decltype(&T::process_stream)()>>{}>{}
))> {};


/**
* @brief If @c T is a source which provides a processing function returning a reference on a frame or a sample
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* @remark T shall be a sample-based or frame-based source or compile-time assertion failure will happen.
*
* @sa source_produces_frame, source_produces_sample
**/
template<class T, class = void>
struct source_applies_in_place_processing : std::false_type
{ static_assert(source_produces_frame<T>{} || source_produces_sample<T>{}, DFP_UNEXPECTED_ERROR); };

template<class T>
struct source_applies_in_place_processing<T, DFP_C_REQUIRES((!std::is_const<T>{} && (
        std::is_lvalue_reference<std::invoke_result_t<decltype(&T::acquire_frame), T>>{}
        &&
        !std::is_const<std::remove_reference_t<std::invoke_result_t<decltype(&T::acquire_frame), T>>>{}
)))> : std::true_type{};

template<class T>
struct source_applies_in_place_processing<T, DFP_C_REQUIRES((!std::is_const<T>{} && (
        std::is_lvalue_reference<std::invoke_result_t<decltype(&T::acquire_sample), T>>{}
        &&
        !std::is_const<std::remove_reference_t<std::invoke_result_t<decltype(&T::acquire_sample), T>>>{}
)))> : std::true_type{};


/**
* @brief If @c T is a sink which provides a processing function returning a reference on a frame or a sample
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* @remark T shall be a sample-based or frame-based sink or compile-time assertion failure will happen.
*
* @sa supports_release_frame, supports_release_sample
**/
template<class T, class = void, class = DFP_CONCEPT_ASSERT((std::disjunction</*supports_release_frame<T>, */supports_release_sample<T>>))>
struct sink_applies_in_place_processing;


/**
* @brief If @c T is a processing cell which provides a processing function returning a reference on a frame or a sample
* provides the member constant @c value equal to true. For any other type, @c value is false.
*
* @remark T shall be a sample-based or frame-based processing cell or compile-time assertion failure will happen.
*
* @sa is_cell
**/
template<class T, class = void, class = DFP_CONCEPT_ASSERT((is_cell<T>))> struct applies_in_place_processing;

template<class T>
struct applies_in_place_processing<T, DFP_C_REQUIRES((is_source<T>{}))>    : source_applies_in_place_processing<T> {};

template<class T>
struct applies_in_place_processing<T, DFP_C_REQUIRES((is_node<T>{}))>      : node_applies_in_place_processing<T>   {};

template<class T>
struct applies_in_place_processing<T, DFP_C_REQUIRES((is_sink<T>{}))>      : sink_applies_in_place_processing<T>   {};

//TODO: move to upg namespace
template<class T>
struct unwrap_refwrapper
{ using type = T; };

template<class T>
struct unwrap_refwrapper<std::reference_wrapper<T>>
{ using type = T&; };


template<class T>
using special_decay_t = typename unwrap_refwrapper<typename std::decay<T>::type>::type;

template<class T>
struct is_reference_wrapper : std::false_type {};

template<class T>
struct is_reference_wrapper<std::reference_wrapper<T>> : std::true_type {};


template<class T, class = void>
struct upstream_produces_sample : std::false_type
{ static_assert(is_upstream<T>{}, DFP_UNEXPECTED_ERROR); };


template<class T>
struct upstream_produces_sample<T, DFP_C_REQUIRES((is_node<T>{}))> : node_produces_sample<T> {};


template<class T>
struct upstream_produces_sample<T, DFP_C_REQUIRES((is_source<T>{}))> : source_produces_sample<T> {};


template<class T, class = void>
struct upstream_produces_frame : std::false_type
{ static_assert(is_upstream<T>{}, DFP_UNEXPECTED_ERROR); };


template<class T>
struct upstream_produces_frame<T, DFP_C_REQUIRES((is_node<T>{}))> : node_produces_frame<T> {};


template<class T>
struct upstream_produces_frame<T, DFP_C_REQUIRES((is_source<T>{}))> : source_produces_frame<T> {};

template<class T, class InitStorageView, class = void>
struct supports_initialize_upwards : std::false_type {};

template<class T, class InitStorageView>
struct supports_initialize_upwards<T, InitStorageView, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(InitStorageView)>(&T::initialize_upwards)
)>{}))> : std::true_type {};


template<class T, class InitStorageView, class = void>
struct supports_initialize_downwards : std::false_type {};

template<class T, class InitStorageView>
struct supports_initialize_downwards<T, InitStorageView, DFP_C_REQUIRES((always_true<decltype(
    static_cast<void (T::*)(InitStorageView)>(&T::initialize_downwards)
)>{}))> : std::true_type {};


template<class T, class = void>
struct has_initialization_storage_keyvalue : std::false_type {};

template<class T>
struct has_initialization_storage_keyvalue<T, DFP_C_REQUIRES((always_true<typename T::initialization_storage_keyvalue>{}))>
 :  std::true_type {};


///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_CELL_TRAITS_HPP
