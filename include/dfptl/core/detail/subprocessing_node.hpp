/**
* @file core/detail/subprocessing_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SUBPROCESSING_NODE_HPP
#define DFPTL_CORE_DETAIL_SUBPROCESSING_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/source.hpp"
#include "dfptl/core/runnable_tree.hpp"
#include "dfptl/core/sample_releaser.hpp"
#include "dfptl/core/frame_releaser.hpp"
#include "dfptl/core/detail/subprocessing_traits.hpp"
#include "dfptl/core/graph_designer.hpp"

#include "stl/optional.hpp"

namespace dfp
{
inline namespace core
{
namespace detail
{


template<class Context, class UpstreamCell>
class upstream_ref_source
 :  public  core::source<
        Context, output_stream_specifier_t<UpstreamCell>, upstream_ref_source<Context, UpstreamCell>
    >
{
    typedef core::source<
        Context, output_stream_specifier_t<UpstreamCell>, upstream_ref_source
    > source_type;

    UpstreamCell& upstream_;

public:
    constexpr upstream_ref_source(Context&& context, UpstreamCell& upstream) noexcept
     :  source_type(std::forward<Context>(context), upstream.output_stream_specifier()), upstream_(upstream)
    {}

    template<class T = UpstreamCell, DFP_F_REQUIRES((
        supports_const_renew_sample<cell_output_port_t<T>>{} && !(supports_const_renew_frame<cell_output_port_t<T>>{} || supports_renew_frame<cell_output_port_t<T>>{})
    ))>
    constexpr
    decltype(std::declval<T const&>()   .output_port().renew_sample(std::declval<T const&>()))
    acquire_sample() const
    { return upstream_                  .output_port().renew_sample(upstream_); }

    template<class T = UpstreamCell, DFP_F_REQUIRES((
        supports_renew_sample<cell_output_port_t<T>>{} && !(supports_renew_frame<cell_output_port_t<T>>{} || supports_const_renew_frame<cell_output_port_t<T>>{})
    ))>
    DFP_MUTABLE_CONSTEXPR
    decltype(std::declval<T&>() .output_port().renew_sample(std::declval<T&>()))
    acquire_sample()
    { return upstream_          .output_port().renew_sample(upstream_); }

    template<class T = UpstreamCell, DFP_F_REQUIRES((supports_const_renew_frame<cell_output_port_t<T>>{}))>
    constexpr
    decltype(std::declval<T const&>()   .output_port().renew_frame(std::declval<T const&>()))
    acquire_frame() const
    { return upstream_                  .output_port().renew_frame(upstream_); }

    template<class T = UpstreamCell, DFP_F_REQUIRES((supports_renew_frame<cell_output_port_t<T>>{}))>
    DFP_MUTABLE_CONSTEXPR
    decltype(std::declval<T&>() .output_port().renew_frame(std::declval<T&>()))
    acquire_frame()
    { return upstream_          .output_port().renew_frame(upstream_); }
}; // upstream_ref_source


template<class Inputs, class Callable, class InputIndexSequence, class InitStorageKeyValue>
class subprocessing_impl_node_base
{ static_assert(always_false<std::tuple<Inputs, Callable, InputIndexSequence>>{}, DFP_UNEXPECTED_ERROR); };


template<class Inputs, class Callable, class InputIndexSequence, class = void>
struct subprocessing_impl_node
 :  subprocessing_impl_node_base<Inputs, Callable, InputIndexSequence, void>
{ using subprocessing_impl_node_base<Inputs, Callable, InputIndexSequence, void>::subprocessing_impl_node_base; };

template<class Inputs, class Callable, class InputIndexSequence>
struct subprocessing_impl_node<Inputs, Callable, InputIndexSequence, DFP_C_REQUIRES((has_initialization_storage_keyvalue<Callable>{}))>
 :  subprocessing_impl_node_base<Inputs, Callable, InputIndexSequence, typename Callable::initialization_storage_keyvalue>
{ using subprocessing_impl_node_base<Inputs, Callable, InputIndexSequence, typename Callable::initialization_storage_keyvalue>::subprocessing_impl_node_base; };


template<class Inputs, class Callable>
struct subprocessing_node
 :      subprocessing_impl_node<Inputs, Callable, index_sequence_from_t<Inputs>>
{ using subprocessing_impl_node<Inputs, Callable, index_sequence_from_t<Inputs>>::subprocessing_impl_node; };


template<class Callable, class Inputs, std::size_t... InputIds>
struct subprocessing_node_innerbranch
{
private:
    template<std::size_t InputId>
    using input_t = std::remove_cvref_t<context_upstream_t<InputId, Inputs>>; // experience shown that "const" removal is required here 

public:
    template<std::size_t InputId>
    using input_source_t = upstream_ref_source<initial_context, input_t<InputId>>;

    typedef decltype(std::declval<Callable>()(std::declval<input_source_t<InputIds>>()...)) type;
}; //struct subprocessing_node_innerbranch


template<class Callable, class Inputs, std::size_t... InputIds>
using subprocessing_node_innerbranch_t = typename subprocessing_node_innerbranch<Callable, Inputs, InputIds...>::type;


/**
* @brief Out-of-class definition of @c callable member for @ref subprocessing_node class.
*
* @note Implements EBCO idiom with @c Callable class for purpose of memory consumption optimization
*
* @tparam Callable is the type of the callable wrapped by the subprocessing node
**/
template<class Callable>
struct subprocessing_node_callable : Callable
{
    template<class... CallableArgs>
    constexpr subprocessing_node_callable(CallableArgs&&...   callable_args)
        :   Callable(std::forward<CallableArgs>(callable_args)...) {}

    constexpr Callable const&   get() const { return *this; }
    Callable&                   get()       { return *this; }
}; // class subprocessing_node_callable


template<class Inputs, class Callable, std::size_t... InputIds, class InitStorageKeyValue>
class subprocessing_impl_node_base<Inputs, Callable, std::index_sequence<InputIds...>, InitStorageKeyValue>
 :  public  node<
                Inputs,
                typename output_stream_specifier_t<subprocessing_node_innerbranch_t<Callable, Inputs, InputIds...>>
                    ::template set_tag_clue_t<get_tag_type_t<Callable>>,
                subprocessing_impl_node_base<Inputs, Callable, std::index_sequence<InputIds...>, InitStorageKeyValue>
    >::template set_runnable_moveable_t<!subprocessing_node_innerbranch_t<Callable, Inputs, InputIds...>::UNMOVEABLE_RUNNABLE>,
    private subprocessing_node_callable<Callable>
{
    typedef typename node<
                Inputs,
                typename output_stream_specifier_t<subprocessing_node_innerbranch_t<Callable, Inputs, InputIds...>>
                    ::template set_tag_clue_t<get_tag_type_t<Callable>>,
                subprocessing_impl_node_base<Inputs, Callable, std::index_sequence<InputIds...>, InitStorageKeyValue>
    >::template set_runnable_moveable_t<!subprocessing_node_innerbranch_t<Callable, Inputs, InputIds...>::UNMOVEABLE_RUNNABLE> node_type;
    typedef subprocessing_node_innerbranch<Callable, Inputs, InputIds...>               innerbranch;
    typedef typename innerbranch::type                                                  innerbranch_type;
    typedef std::conditional_t<
        upstream_produces_sample<innerbranch_type>{},
        decltype(sample_releaser() <<= std::declval<innerbranch_type>()),
        decltype(frame_releaser()  <<= std::declval<innerbranch_type>())
    > uninitialized_innertree_type;
    typedef runnable_tree<uninitialized_innertree_type>                                 runnable_innertree_type;
    typedef subprocessing_node_callable<Callable>                                       callable_member;
    typedef std::tuple<typename innerbranch::template input_source_t<InputIds>...>      input_sources_type;

public:
    template<class... CallableArgs>
    subprocessing_impl_node_base(Inputs&& inputs, CallableArgs&&... callable_args)
     :  node_type(std::forward<Inputs>(inputs), output_stream_specifier_t<node_type>{}), callable_member(std::forward<CallableArgs>(callable_args)...)
    {
        // inner branch type is resolved once to get stream dynamic traits
        // (likely not optimal, one may want to consider how to delayed this at initialization stage)
        if (!input_stream_specifier_t<runnable_innertree_type>::IS_STATIC)
        {
            auto input_specifier = callable_member::get()(std::get<InputIds>(input_sources_)...).output_stream_specifier();

            node_type::set_output_stream_specifier(this->output_stream_specifier()
                .set_frame_length( input_specifier.get_frame_length())
                .set_channel_count(input_specifier.get_channel_count())
            );
        }
    }

    template<class T = node_type, DFP_F_REQUIRES((std::is_copy_constructible<T>{}))>
    subprocessing_impl_node_base(subprocessing_impl_node_base const& it)
     :  node_type(it), callable_member(it.callable_member::get())
    {}

    subprocessing_impl_node_base(subprocessing_impl_node_base&& it)
     :  node_type(std::move(it)), callable_member(std::move(it.callable_member::get()))
    {}

    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_sample<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_sample_t<node_type> process_stream(input_stream_t<node_type, InputIds>&&...)
    { return opt_innertree_->run(); }

    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_frame<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_frame_t<node_type> process_stream(input_stream_t<node_type, InputIds>&&...)
    { return opt_innertree_->run(); }

    template<class InitStorageView, DFP_F_REQUIRES((detail::supports_initialize_upwards<callable_member, InitStorageView>{}))>
    void initialize_upwards(InitStorageView init_storage)
    { callable_member::initialize_upwards(init_storage); }

    template<class InitStorageView, DFP_F_REQUIRES((!detail::supports_initialize_upwards<callable_member, InitStorageView>{}))>
    void initialize_upwards(InitStorageView init_storage)
    { node_type::initialize_upwards(init_storage); }

    template<class InitStorageView, DFP_F_REQUIRES((detail::supports_initialize_downwards<callable_member, InitStorageView>{}))>
    void initialize_downwards(InitStorageView init_storage)
    { callable_member::initialize_downwards(init_storage); init(); }

    template<class InitStorageView, DFP_F_REQUIRES((!detail::supports_initialize_downwards<callable_member, InitStorageView>{}))>
    void initialize_downwards(InitStorageView init_storage)
    { node_type::initialize_downwards(init_storage); init(); }

private:
    // note that as input_cell as stored by reference so the init of input_sources shall be re-applied at anytime of constructor
    // (even for move or copy-constructors) 
    static constexpr input_sources_type init_input_sources(subprocessing_impl_node_base& base)
    { return { { initial_context{}, base.input_port(size_t_const<InputIds>{}).input_cell() }... }; }

    // TODO: investigate if callback could be copied to avoid subprocessing node to be unmoveable
    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_frame<T>{}))>
    static constexpr uninitialized_innertree_type init_innertree(callable_member const& callable, input_sources_type&& input_sources)
    { return frame_releaser() <<= callable.get()(std::get<InputIds>(std::move(input_sources))...); }

    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_sample<T>{}))>
    static constexpr uninitialized_innertree_type init_innertree(callable_member const& callable, input_sources_type&& input_sources)
    { return sample_releaser() <<= callable.get()(std::get<InputIds>(std::move(input_sources))...); }

    // note that input_sources_ ownership can be moved in this init() as it won't be used anymore
    // this allows non-copyable input to be attached to subprocessing node
    void init()
    { opt_innertree_.emplace(init_innertree(this->callable_member::get(), std::move(input_sources_))); }

    input_sources_type input_sources_{ init_input_sources(*this) };
    upg::optional<runnable_innertree_type> opt_innertree_;
}; // class subprocessing_impl_node_base


} // namespace detail
} // inline namespace core
} // namespace dfp



#endif //DFPTL_CORE_DETAIL_SUBPROCESSING_NODE_HPP
