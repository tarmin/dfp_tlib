/**
* @file core/detail/subprocessing_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SUBPROCESSING_TRAINTS_HPP
#define DFPTL_CORE_DETAIL_SUBPROCESSING_TRAINTS_HPP


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class T, class = void>
struct get_tag_type
{
    typedef null_tag type;
};

template<class T>
struct get_tag_type<T, std::void_t<typename T::tag_type>>
{
    typedef typename T::tag_type type;
};

template<class T>
using get_tag_type_t = typename get_tag_type<T>::type;


} // namespace detail
} // inline namespace core
} // namespace dfp



#endif //DFPTL_CORE_DETAIL_SUBPROCESSING_TRAINTS_HPP
