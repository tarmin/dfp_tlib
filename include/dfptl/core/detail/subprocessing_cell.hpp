/**
* @file core/detail/subprocessing_cell.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SUBPROCESSING_CELL_HPP
#define DFPTL_CORE_DETAIL_SUBPROCESSING_CELL_HPP


#include "dfptl/core/detail/subprocessing_node.hpp"
#include "dfptl/core/detail/subprocessing_source.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief Statically polymorphic cell which is resolved as subprocessing_source or subprocessing_node
**/
template<class Context, class UpstreamIndexesSequence, class Callable, class = void> struct subprocessing_cell_impl
{ static_assert(always_false<Context>{}, DFP_UNEXPECTED_ERROR); };


template<class Inputs, class Callable>
struct subprocessing_cell : subprocessing_cell_impl<Inputs, index_sequence_from_t<Inputs>, Callable>
{ using subprocessing_cell_impl<Inputs, index_sequence_from_t<Inputs>, Callable>::subprocessing_cell_impl; };


template<class Context, std::size_t... UpstreamIds, class Callable>
struct subprocessing_cell_impl<
    Context, std::index_sequence<UpstreamIds...>, Callable, DFP_C_REQUIRES((
        (
            not (
                (sizeof...(UpstreamIds) > 0)
                && std::is_invocable<
                    Callable,
                    upstream_ref_source<initial_context, context_upstream_t<UpstreamIds, Context>>...
                >{}
        ) &&
            not std::is_invocable<Callable>{}
        )
    ))
>
{ static_assert(always_false<subprocessing_cell_impl>{}, "Callable provided to subprocesssing cell cannot be invoked to build an upstream"); };


/**
* @brief partial template specialization which aliases as the subprocessing_node template class
*
* Aliases as a subprocessing_node if the given @c Callable accepts processing branches as input parameters and returns a core::node
**/
template<class Context, std::size_t... UpstreamIds, class Callable>
struct subprocessing_cell_impl<
    Context, std::index_sequence<UpstreamIds...>, Callable, DFP_C_REQUIRES((
        (sizeof...(UpstreamIds) > 0)
        &&
        std::is_invocable<
            Callable,
            upstream_ref_source<initial_context, context_upstream_t<UpstreamIds, Context>>...
        >{}
    ))
> : subprocessing_node<Context, Callable>
{ using subprocessing_node<Context, Callable>::subprocessing_node; };


/**
* @brief partial template specialization which aliases as the subprocessing_source template class
*
* Aliases as the subprocessing_source if the given @c Callable has no input parameter and returns a @ref core::source
**/
template<class Context, class UpstreamIndexesSequence, class Callable>
struct subprocessing_cell_impl<
    Context, UpstreamIndexesSequence, Callable, DFP_C_REQUIRES((std::is_invocable<Callable>{}))
> : subprocessing_source<Context, Callable>
{ using subprocessing_source<Context, Callable>::subprocessing_source; };


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif //DFPTL_CORE_DETAIL_SUBPROCESSING_CELL_HPP
