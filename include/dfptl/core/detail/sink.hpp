/**
* @file core/detail/sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SINK_HPP
#define DFPTL_CORE_DETAIL_SINK_HPP


#include "dfptl/core/downstream_cell.hpp"
#include "dfptl/core/initializable_cell.hpp"
#include "dfptl/core/detail/sample_input_source.hpp"
#include "dfptl/core/detail/sink_port.hpp"

#include <boost/fusion/include/filter_view.hpp>
#include <boost/fusion/include/as_map.hpp>
#include <boost/fusion/include/at_key.hpp>
#include <boost/fusion/include/size.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class T>
class sample_inputs_of
{
    template<class E, class = void>
    struct contains_impl : std::false_type
    {};

    template<class E>
    struct contains_impl<E, DFP_C_REQUIRES((
        detail::is_sample_input_key<typename E::first_type>{}
    ))> : std::true_type
    {};

    template<class E>
    struct contains : contains_impl<E>
    {};

public:
    typedef boost::fusion::filter_view<
        typename T::initialization_storage_type::view_type const,
        typename boost::mpl::quote1<contains>::template apply<boost::mpl::_>
    > filter_view_type;

    typedef typename boost::fusion::result_of::as_map<filter_view_type>::type map_type;
}; // class sample_inputs_of


template<class Inputs, class InputIndexSequence, template<class> class Runner, class Derived, class InitStorageKeyValue, bool UnmoveableRunnable>
class sink
{ static_assert(always_false<Inputs>{}, DFP_UNEXPECTED_ERROR); };


template<class Inputs, std::size_t... InputIndexes, template<class> class Runner, class Derived, class InitStorageKeyValue, bool UnmoveableRunnable>
class sink<Inputs, std::index_sequence<InputIndexes...>, Runner, Derived, InitStorageKeyValue, UnmoveableRunnable>
    :   public core::downstream_cell<detail::sink_input_port, Inputs, Derived>,
        protected core::initializable_cell<Inputs, InitStorageKeyValue, Derived, UnmoveableRunnable>,
        public processing_cell<Inputs, Derived>,
        private Runner<Derived>
{
    static_assert(
        std::conjunction<is_upstream<std::unwrap_reference_t<std::tuple_element_t<InputIndexes, Inputs>>>...>{},
        "one of the given Inputs is surprisling not a downstream cell (aka source or node)"
    );

    friend struct runner<Derived, Runner<Derived>>;

    typedef core::initializable_cell<Inputs, InitStorageKeyValue, Derived, UnmoveableRunnable> initializable_cell_type;

public:
    typedef core::downstream_cell<detail::sink_input_port, Inputs, Derived> downstream_cell_type;
    typedef Derived derived;
    typedef InitStorageKeyValue initialization_keyvalue_type;
    typedef typename downstream_cell_type::input_cells_type     input_cells_type; // required for is_sink helper
    typedef typename initializable_cell_type::initialization_storage_type initialization_storage_type;
    typedef Runner<Derived> runner_type;
    typedef processing_cell<Inputs, Derived>                    processing_cell_type;

    static constexpr auto UNMOVEABLE_RUNNABLE = initializable_cell_type::UNMOVEABLE_RUNNABLE;

    using downstream_cell_type::input_port;

    template<class... RunnerArgs, class T = Derived>
    constexpr sink(Inputs&& inputs, RunnerArgs&&... runner_args)
     :  downstream_cell_type(std::forward<Inputs>(inputs)), runner_type(std::forward<RunnerArgs>(runner_args)...)
    {}

    template<class T = typename initialization_storage_type::view_type, DFP_F_REQUIRES((!std::is_same<boost::fusion::map<>, T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) typename initialization_storage_type::view_type initialize_tree()
    {
        auto init_storage_view = initializable_cell_type::build_initialization_storage(*this).view();

        invoke_initialize(init_storage_view);

        return init_storage_view;
    }

    // workaround for minimal constexpr support
    // TODO: work with boost::hana for c++14 and above
    template<class T = typename initialization_storage_type::view_type, DFP_F_REQUIRES((std::is_same<boost::fusion::map<>, T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) void initialize_tree()
    { invoke_initialize(empty_map{}); }

    template<class T = InitStorageKeyValue, DFP_F_REQUIRES((!std::is_void<T>{}))>
    typename initialization_storage_type::view_type initialize_tree(typename boost::fusion::result_of::second<T>::type init_value)
    {
        auto init_storage_view = initializable_cell_type::build_initialization_storage(*this).view();

        boost::fusion::at_key<typename boost::fusion::result_of::first<InitStorageKeyValue>::type>(init_storage_view) = init_value;
        invoke_initialize(init_storage_view);

        return init_storage_view;
    }

    /**
    * @brief Creates runner and launch processing of the tree
    **/
    //TODO: runner shall also be a reducer and run() may return a value 
    DFP_CONSTEXPR_VOID run()
    { runner_type::operator()(*static_cast<Derived* const>(this)); }

protected:
    runner_type const& get_runner() const
    { return *this; }

    DFP_CONSTEXPR_VOID invoke_initialize(typename initialization_storage_type::view_type initialization_storage)
    {
        static_cast<Derived* const>(this)->initialize_upwards(initialization_storage);

        using call_sequence = char[]; //trick helping to invoke method on objects of pack
        (void) call_sequence{(
            input_port(size_t_const<InputIndexes>{}).initialize(initialization_storage)
        , '\0')...};

        static_cast<Derived* const>(this)->initialize_downwards(initialization_storage);
    }

    DFP_CONSTEXPR_VOID invoke_initialize(empty_map initialization_storage)
    {
        static_cast<Derived* const>(this)->initialize_upwards(initialization_storage);

        using call_sequence = char[]; //trick helping to invoke method on objects of pack
        (void) call_sequence{(
            input_port(size_t_const<InputIndexes>{}).initialize(initialization_storage)
        , '\0')...};

        static_cast<Derived* const>(this)->initialize_downwards(initialization_storage);
    }
}; // class sink


} // namespace detail
} // namespace core
} // namespace dfp


#endif //DFPTL_CORE_DETAIL_SINK_HPP
