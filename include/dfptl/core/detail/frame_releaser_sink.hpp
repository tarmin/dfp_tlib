/**
* @file core/frame_releaser_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_FRAME_RELEASER_SINK_HPP
#define DFPTL_CORE_DETAIL_FRAME_RELEASER_SINK_HPP


#include "dfptl/core/sink.hpp"
#include "dfptl/core/exception.hpp"
#include "dfptl/core/default_settings.hpp"


#define DFP_CORE_FRAME_RELEASER core::frame_releaser


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class Input, class Derived, class = void>
class frame_releaser_sink_base
{ static_assert(always_false<Input>{}, DFP_INVALID_INPUT_COUNT(DFP_CORE_FRAME_RELEASER, 1)); };


template<class Input>
class frame_releaser_sink
 :  public frame_releaser_sink_base<Input, frame_releaser_sink<Input>>
{
    typedef frame_releaser_sink_base<Input, frame_releaser_sink<Input>> base;

public:
    using base::base;
}; // class frame_releaser_sink


template<class Input, class Derived>
class frame_releaser_sink_base<Input, Derived, DFP_C_REQUIRES((std::tuple_size<Input>{} == 1))>
 :  public  core::sink<Input, run_once, Derived>
{
    typedef core::sink<Input, run_once, Derived> sink_type;

public:
    typedef typename frame_builder<frame_t<typename input_stream_specifier_t<sink_type>::template
        set_sample_clue_t<std::remove_cv_t<input_sample_t<sink_type>>>
    >>::result_type frame_type;

    //TODO: investigate constexpr in C++11 support
    // (will likely need to change run_once into run_never and directly access the input port)

//    template<class T = input_stream_specifier_t<sink_type>, DFP_F_REQUIRES((T::IS_STATIC))>
//    constexpr frame_releaser_sink_base(Input&& input)
//     :  sink_type(std::forward<Input>(input)), current_(frame_builder<frame_type>{}.build()) {}

//    template<class T = input_stream_specifier_t<sink_type>, DFP_F_REQUIRES((!T::IS_STATIC))>
    frame_releaser_sink_base(Input&& input)
     :  sink_type(std::forward<Input>(input)),
        current_(frame_builder<frame_type>{}
            .set_frame_length(this->input_stream_specifier().get_frame_length())
            .build()
        )
    {}

    template<bool Condition = input_stream_specifier_t<sink_type>::IS_FRAME_LENGTH_STATIC, DFP_F_REQUIRES((Condition))>
    void initialize() const
    {}

    template<bool Condition = !input_stream_specifier_t<sink_type>::IS_FRAME_LENGTH_STATIC, DFP_F_REQUIRES((Condition))>
    void initialize()
    { // handle possibly late definition of the input stream specifier
        auto const input_frame_length = this->input_stream_specifier().get_frame_length();

        if (input_frame_length == DYNAMIC_EXTENT)
            throw frame_length_error(DFP_STRINGIFY(DFP_CORE_FRAME_RELEASER), 0, "Its actual length is unexpectly DYNAMIC_EXTENT");

        if (get_length(current_) == input_frame_length)
            return;

        update_length(current_, input_frame_length);
    }

    void release_frame(input_frame_t<sink_type> const& in)
    { std::copy(in.begin(), in.end(), current_.begin()); }

    // for optimization purpose output is given by const reference
    // however, take attention that life time of the provided output is so
    // same as the sink instance
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) frame_type const& run() &
    { sink_type::run(); return current_; }

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) frame_type run() &&
    { sink_type::run(); return current_; }

#if DFP_CPP11_SUPPORT
    // specific overloadings to give a chance to constexpr in c++11
    
    constexpr frame_type const& run() const &
    { return (sink_type::run(),  current_); }

    constexpr frame_type run() const &&
    { return (sink_type::run(), current_); }
#endif

protected:
    //TODO: 1. optimize when input stream holds a reference
    //TODO: 2. make frame_type a class template parameter
    frame_type current_;
}; // class frame_releaser_sink_base


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_FRAME_RELEASER_SINK_HPP
