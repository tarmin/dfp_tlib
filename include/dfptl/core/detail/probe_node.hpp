/**
* @file core/detail/probe_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PROBE_NODE_HPP
#define DFPTL_CORE_DETAIL_PROBE_NODE_HPP


#include "dfptl/core/node.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


#define DFP_CORE_PROBE core::probe


namespace keys
{
    template<class Tag>
    struct probe_config {};
} // namespace keys


template<class Context, class ForcedFrameCopy = bool_const<false>>
class probe_node
 :  public  node<
        Context, as_input0_tag, probe_node<Context, ForcedFrameCopy>,
        std::pair<keys::probe_config<typename Context::cell_tag>, output_frame_t<context_upstream_t<0, Context>> const*>
    >
{
    static_assert(!std::is_void<typename Context::cell_tag>{}, "a Tag shall be associated to "  DFP_STRINGIFY(DFP_CORE_PROBE) " node");
    static_assert(std::tuple_size<Context>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_CORE_PROBE, 1));

    constexpr static bool FORCED_FRAME_COPY = ForcedFrameCopy{};

    // alias for the base type
    typedef node<
        Context, as_input0_tag, probe_node<Context, ForcedFrameCopy>,
        std::pair<keys::probe_config<typename Context::cell_tag>, output_frame_t<context_upstream_t<0, Context>> const*>
    > node_type;

    typedef decltype(std::declval<output_stream_t<typename context_upstream<0, Context>::type>&&>().cdrain()) stream_cdrain_result;

    template<class T>
    using is_const_lvalue_ref = bool_const<std::is_lvalue_reference<T>{} && std::is_const<std::remove_reference_t<T>>{}>;

    typedef std::remove_cvref_t<stream_cdrain_result> frame_type;

    //optimization:
    // * if cdrain() returns some const lvalue reference only keeping track of such ref
    // * otherwise a copy of the input frame is kept
    // * this optimization is inhibited if FORCED_FRAME_COPY is worth true
    typedef std::conditional_t<
        is_const_lvalue_ref<stream_cdrain_result>{} && !FORCED_FRAME_COPY, frame_type const*, frame_type
    > frame_handle_type;

    typedef std::conditional_t<
        is_const_lvalue_ref<stream_cdrain_result>{} && !FORCED_FRAME_COPY, frame_type const&, frame_type
    > process_stream_result;


    template<class T = frame_handle_type, DFP_F_REQUIRES((std::is_pointer<T>{}))>
    frame_type const* get_frame_ptr()
    { return frame_handle_; }

    template<class T = frame_handle_type, DFP_F_REQUIRES((!std::is_pointer<T>{}))>
    frame_type const* get_frame_ptr()
    { return &frame_handle_; }

    template<class T, DFP_F_REQUIRES((is_const_lvalue_ref<T>{} && !FORCED_FRAME_COPY))>
    void set_frame_handle(T&& cdrain_result)
    { frame_handle_ = &cdrain_result; }

    template<class T, DFP_F_REQUIRES((!is_const_lvalue_ref<T>{} || FORCED_FRAME_COPY))>
    void set_frame_handle(T&& cdrain_result)
    { frame_handle_ = cdrain_result; }

public:
    using node_type::node_type;

    template<class InitStorage>
    void initialize_upwards(InitStorage init_storage)
    { boost::fusion::at_key<keys::probe_config<typename Context::cell_tag>>(init_storage) = get_frame_ptr(); }

    DFP_MUTABLE_CONSTEXPR process_stream_result process_stream(input_stream_t<node_type>&& input)
    {
        set_frame_handle(input.cdrain());
        return *get_frame_ptr();
    }

private:
    frame_handle_type frame_handle_;
}; // class probe_node


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PROBE_NODE_HPP
