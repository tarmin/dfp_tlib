/**
* @file core/detail/mutable_sample_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_MUTABLE_SAMPLE_SOURCE_HPP
#define DFPTL_CORE_DETAIL_MUTABLE_SAMPLE_SOURCE_HPP


#include "dfptl/core/source.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief Sample-based source to inject in a processing branch a sample value or reference-value.
*
* @sa mutable_sample
**/
template<class Context, class T>
class mutable_sample_source
 :  public  core::source<Context, atomic_stream_specifier<T>, mutable_sample_source<Context, T>>
{
    typedef core::source<Context, atomic_stream_specifier<T>, mutable_sample_source> source_type;

public:
    constexpr mutable_sample_source(Context const& context, T const& sample) noexcept
     :  source_type(context), sample_(sample)
    {}

    constexpr output_sample_t<source_type> acquire_sample() const noexcept
    { return sample_; }

private:
    T const& sample_;
}; // class mutable_sample_source< !reference && !reference_wrapper >


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_MUTABLE_SAMPLE_SOURCE_HPP
