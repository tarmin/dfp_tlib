/**
* @file core/detail/port_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PORT_TRAITS_HPP
#define DFPTL_CORE_DETAIL_PORT_TRAITS_HPP


#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/stream_traits.hpp"
#include "dfptl/core/stream_specifier_traits.hpp"

#include <algorithm>


namespace dfp
{
inline namespace core
{


template<class, class> class stream;


namespace detail
{
///@addtogroup core
///@{
///@addtogroup processing_cell Processing cells
///@{
///@addtogroup core_detail
///@{
/** @defgroup port_traits Port traits
* @brief Provides helper class to retrieve traits on @ref input_port and output_port
* @{
**/


template<class, class, class, class, class> class output_port;
template<class> class input_port;


/**
* @brief If T is an @ref output_port type or reference type (i.e. inheriting from @ref output_port),
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template <class T, class = void>
struct is_output_port : std::false_type {};

template <class T>
struct is_output_port<T, DFP_C_REQUIRES((std::is_reference<T>{}))>
 :  is_output_port<std::remove_reference_t<T>> {};

template <class T>
struct is_output_port<T,DFP_C_REQUIRES((std::is_same<output_port_tag, typename T::dfp_cell_tag>{}))>
 :  std::true_type {};


/**
* @brief If T is an @ref input_port type or reference type (i.e. inheriting from @ref input_port),
* provides the member constant @c value equal to true. Otherwise @c value is false.
**/
template<class T, class = void>
struct is_input_port : std::false_type {};

template<class T>
struct is_input_port<T, DFP_C_REQUIRES((std::is_reference<T>{}))> 
 :  is_input_port<std::remove_reference_t<T>> {};

template<class T>
struct is_input_port<T, DFP_C_REQUIRES((std::is_same<input_port_tag, typename T::dfp_cell_tag>{}))>
 :  std::true_type {};


template<class T>
struct are_input_ports : std::false_type {};


template<class... InputPorts>
struct are_input_ports<std::tuple<InputPorts...>> : std::conjunction<is_input_port<InputPorts>...> {};


/**
* @brief If T is an @ref input_port @ref output_port type or reference type 
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* @sa is_input_port is_output_port
**/
template <class T>
using is_port = std::is_base_of<cell_port_tag, typename T::dfp_cell_tag>;


/**
* @brief Provides member typedef type which is defined as the input cell type connected
* to the given @ref input_port "input port" @c T.
*
* @remark T shall be an @ref input_port or compile-time assertion failure will happen.
*
* @tparam T is the input_port on which to retrieve the connected input cell.
**/
template<class T, class = DFP_CONCEPT_ASSERT((is_input_port<T>))> struct input_cell
{ typedef typename T::input_cell_value type; };

/**
* @brief Alias for input_cell<...>::type
* @related input_cell
**/
template<class T> using input_cell_t = typename input_cell<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the @ref stream_specifier "stream specifier" type
* handled by the given port @c T.
*
* @remark T shall be a port ( @ref input_port or @ref output_port) or compile-time assertion failure will happen.
*
* @tparam T is the port type (aka @ref input_port or @ref output_port) on which to retrieve the stream specifier.
**/
template<class T, class = void, class = DFP_CONCEPT_ASSERT((is_port<T>))> struct port_stream_specifier {};

template<class T>
struct port_stream_specifier<T, DFP_C_REQUIRES((is_output_port<T>::value))>
{ typedef typename T::stream_specifier_type type; };

template<class T>
struct port_stream_specifier<T, DFP_C_REQUIRES((is_input_port<T>::value))>
{ typedef typename port_stream_specifier<typename std::remove_cvref_t<input_cell_t<T>>::output_port_type>::type type; };

/**
* @brief Alias for port_stream_specifier<...>::type
* @related port_stream_specifier
**/
template<class T>
using port_stream_specifier_t = typename port_stream_specifier<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the stream type handled by the given port @c T.
*
* @remark T shall be a port ( @ref input_port or @ref output_port) or compile-time assertion failure will happen.
*
* @tparam T is the port type (aka @ref input_port or @ref output_port) on which to retrieve the handled stream type.
**/
template<class T, class = void> struct port_stream
{ static_assert(is_port<T>{}, "Given T template parameter shall be a port"); };

template<class T>
struct port_stream<T, DFP_C_REQUIRES((is_output_port<T>{}))>
{
    // conditional below is a forward for apply_cell inheritance graph which does not propagate the top-most child type
    // TODO: likely to be fixed
    typedef std::conditional_t<std::is_base_of<typename T::cell_type, typename T::emitter_type>{},
        core::stream<typename T::stream_specifier_type, typename T::cell_type>,
        core::stream<typename T::stream_specifier_type, typename T::emitter_type>
    >type;
};

template<class T>
struct port_stream<T, DFP_C_REQUIRES((is_input_port<T>{}))>
{ typedef typename port_stream<typename std::remove_cvref_t<std::unwrap_reference_t<input_cell_t<T>>>::output_port_type>::type type; };

/**
* @brief Alias for port_stream<...>::type
* @related port_stream
**/
template<class T>
using port_stream_t = typename port_stream<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the sample type handled by the given port @c T.
*
* @remark T shall be a port ( @ref input_port or @ref output_port) or compile-time assertion failure will happen.
*
* @tparam T is the port type (aka @ref input_port or @ref output_port) on which to retrieve the handled sample type.
**/
template<class T, class = DFP_CONCEPT_ASSERT((is_port<T>))> struct port_sample
{ typedef sample_t<port_stream_t<T>> type; };

/**
* @brief Aias for port_sample<...>::type
* @related port_sample
**/
template<class T>
using port_sample_t = typename port_sample<T>::type;


/**
* @brief Provides member typedef type which is defined as the frame @c type handled by the given port.
*
* @remark T shall be a port ( @ref input_port or @ref output_port) or compile-time assertion failure will happen.
*
* @tparam T is the port type (aka @ref input_port or @ref output_port) on which to retrieve the handled frame type.
**/
template<class T> struct port_frame
{
    static_assert(is_port<T>{}, DFP_UNEXPECTED_ERROR);
    typedef frame_t<port_stream_t<T> > type;
};

/**
* @brief Alias for port_frame<...>::type
* @related port_frame
**/
template<class T>
using port_frame_t = typename port_frame<T>::type;


template<class T, class = void>
struct supports_renew_sample : std::false_type {};

template<class T>
struct supports_renew_sample<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<T> (T::*)(typename T::cell_type&)>(&T::renew_sample)
)>{}))> : std::true_type {};

template<class T>
struct supports_renew_sample<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<T> const& (T::*)(typename T::cell_type&)>(&T::renew_sample)
)>{}))> : std::true_type {};


template<class T, class = void>
struct supports_const_renew_sample : std::false_type {};

template<class T>
struct supports_const_renew_sample<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<T> (T::*)(typename T::cell_type const&) const>(&T::renew_sample)
)>{}))> : std::true_type {};

template<class T>
struct supports_const_renew_sample<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_sample_t<T> const& (T::*)(typename T::cell_type const&) const>(&T::renew_sample)
)>{}))> : std::true_type {};


template<class T, class = void>
struct supports_renew_stream : std::false_type {};

template<class T>
struct supports_renew_stream<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<T> (T::*)(typename T::cell_type&)>(&T::renew_stream)
)>{}))> : std::true_type {};


template<class T, class = void>
struct supports_const_renew_stream : std::false_type {};

template<class T>
struct supports_const_renew_stream<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_stream_t<T> (T::*)(typename T::cell_type const&) const>(&T::renew_stream)
)>{}))> : std::true_type {};


template<class T, class = void>
struct supports_renew_frame : std::false_type {};

template<class T>
struct supports_renew_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<T> (T::*)(typename T::cell_type&)>(&T::renew_frame)
)>{}))> : std::true_type {};

template<class T>
struct supports_renew_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<T> const& (T::*)(typename T::cell_type&)>(&T::renew_frame)
)>{}))> : std::true_type {};


template<class T, class = void>
struct supports_const_renew_frame : std::false_type {};

template<class T>
struct supports_const_renew_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<T> (T::*)(typename T::cell_type const&) const>(&T::renew_frame)
)>{}))> : std::true_type {};

template<class T>
struct supports_const_renew_frame<T, DFP_C_REQUIRES((always_true<decltype(
    static_cast<port_frame_t<T> const& (T::*)(typename T::cell_type const&) const>(&T::renew_frame)
)>{}))> : std::true_type {};


///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PORT_TRAITS_HPP
