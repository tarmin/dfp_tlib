/**
* @file core/detail/placeholder_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PLACEHOLDER_TRAITS_HPP
#define DFPTL_CORE_DETAIL_PLACEHOLDER_TRAITS_HPP


#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/upstream_traits.hpp"
#include "dfptl/core/detail/cell_traits.hpp"


namespace dfp
{
inline namespace core
{


struct no_preset;


namespace detail
{
///@addtogroup core
///@{
///@addtogroup placeholder
///@{
///@addtogroup core_detail
///@{
/** @defgroup placeholder_traits placeholder traits
* @brief Provides helper class to retrieve traits on @ref placeholder
* @{
**/


//Helps to identify placehoder (in is_cell_builder helper template class)
struct cell_builder_tag;


/**
* @brief if T is same as dfp::placeholder provides the member constant value equal to true. Otherwise value is false.
*
* @related dfp::placeholder
*
* @sa dfp::placeholder
**/
template <class T, class = void>
struct is_cell_builder :  std::false_type
{}; // struct is_cell_builder


template <class T>
struct is_cell_builder<T, DFP_C_REQUIRES((std::is_reference<T>{}))> : is_cell_builder<std::remove_reference_t<T>> 
{}; // class is_cell_builder<... is_same<..., is_reference ...>


template <class T>
struct is_cell_builder<T, DFP_C_REQUIRES((std::is_same<typename T::dfptl_tag, cell_builder_tag>{}))> : std::true_type
{}; // class is_cell_builder<... is_same<..., cell_builder_tag> ...>


/**
* @brief Provides member template type which is defined as the processing cell
* depicted by the given @ref placeholder.
*
* @tparam CellBuilder shall be a @ref placeholder
**/
template <class CellBuilder>
struct cell
{
    static_assert(is_cell_builder<CellBuilder>{}, DFP_UNEXPECTED_ERROR);

    /**
    * @brief alias template type of the depicted processing cell
    *
    * @tparam ProcessingContext cell tag and upstream graph traits
    **/
    template<class ProcessingContext> using type = typename CellBuilder::template cell<ProcessingContext>::type;
};


/**
* @related cell
* @brief Alias of @ref cell
**/
template <class CellBuilder, class ProcessingContext = initial_context>
using cell_t = typename cell<CellBuilder>::template type<ProcessingContext>;


//TODO: reconsider need for is_source_builder. Couldn't be dropped ?
template <class T, class UpstreamContext = initial_context, class = void>
struct is_source_builder : std::false_type
{}; // struct is_source_builder


template <class T, class UpstreamContext>
struct is_source_builder<T, UpstreamContext, DFP_C_REQUIRES((std::is_reference<T>{}))>
 :  is_source_builder<std::remove_reference_t<T>, UpstreamContext>
{}; // class is_source_builder<... is_same<..., is_reference ...>


template <class T, class UpstreamContext>
struct is_source_builder<T, UpstreamContext, DFP_C_REQUIRES((is_cell_builder<T>{} && !std::is_reference<T>{}))>
 :  is_source<cell_t<T, UpstreamContext>>
{}; // class is_source_builder<... is_cell_builder<T>{} ...>


// because of how lookup resolution is performed on template function by compilers,
// next operation shall be implemented as function class 
namespace functions
{
template<class T, class = void>
struct cellify;

template<class T>
struct cellify<T, DFP_C_REQUIRES((is_upstream<T>{}))>
{
constexpr auto operator()(T&& t) const ->
decltype(std::forward<T>(t))
{ return std::forward<T>(t); }
}; // struct cellify<is_upstream>


template<class T>
struct cellify<T, DFP_C_REQUIRES((!is_upstream<T>{} && is_source_builder<T>{}))>
{
constexpr auto operator()(T&& t) const ->
decltype(t.build())
{ return t.build(); }
}; // strut cellify<is_source_builder>


template<class T>
struct cellify<T, DFP_C_REQUIRES((is_reference_wrapper<T>{}))>
{
constexpr T operator()(T t) const
{ return t; }
}; // struct cellify<is_reference_wrapper>
} //namespace functions


/**
* @brief if @c t parameter is not an upstream cell instance wraps the @c t
* within a @ref const_sample_source
*
* @param t data to wraps within a @ref const_sample_source
*
* @tparam T type is implicitly deduced from the @c t argument
*/
template<class T>
static constexpr auto cellify(T&& t) ->
decltype(functions::cellify<T>{}(std::forward<T>(t)))
{ return functions::cellify<T>{}(std::forward<T>(t)); }


namespace result_of
{
#if (defined(__clang__) && (__clang_major__ > 3)) || !defined(__clang__)
template<class T>
struct cellify
{ typedef std::invoke_result_t<decltype(detail::cellify<T>), T> type; };
#else
// workaround for clang 3
template<class T, class = void>
struct cellify;

template<class T>
static constexpr auto forward(T&& t) ->
decltype(std::forward<T>(t))
{ return std::forward<T>(t); }


template<class T>
struct cellify<T, DFP_C_REQUIRES((is_upstream<T>{}))>
{ typedef std::invoke_result_t<decltype(forward<T>), T> type; };

template<class T>
struct cellify<T, DFP_C_REQUIRES((!is_upstream<T>{} && is_source_builder<T>{}))>
{ typedef decltype(std::declval<T>().build()) type; };

template<class T>
struct cellify<T, DFP_C_REQUIRES((is_reference_wrapper<T>{}))>
{ typedef T type; };
#endif


template<class T>
using cellify_t = typename cellify<T>::type;
}


template<class T, class Input, class = void>
struct is_convertible_to_cell_builder;


template<class T, class Input>
struct is_convertible_to_cell_builder<T, Input, DFP_C_REQUIRES((is_cell_builder<T>{}))> : std::true_type {};


template<class T, class Input, class = void>
struct invocable_convertible_with_apply : std::false_type {};

template<class T>
struct invocable_convertible_with_apply<T, std::tuple<>, DFP_C_REQUIRES((
    //Note that for now an invocable returning nothing (void) cannot be implicitly converted to sink because there is no way to specify its runner
    //but it can be implicitly converted into node using core::execute()
    (std::is_invocable<T>{} && !returns_void<T>{})
))> : std::true_type {};

template<class T, class... Inputs>
struct invocable_convertible_with_apply<T, std::tuple<Inputs...>, DFP_C_REQUIRES((sizeof...(Inputs) > 0 && (
    //Note that for now an invocable returning nothing (void) cannot be implicitly converted to sink because there is no way to specify its runner
    //but it can be implicitly converted into node using core::execute()
    (
        std::is_invocable<T, output_sample_t<result_of::cellify_t<Inputs>> const&...>{}
        &&
        !returns_void<T, output_sample_t<result_of::cellify_t<Inputs>> const&...>{}
    ) ||
    (
        std::is_invocable<T, output_frame_t<result_of::cellify_t<Inputs>> const&...>{}
        && 
        !returns_void<T, output_frame_t<result_of::cellify_t<Inputs>> const&...>{}
    ) ||
    (
        std::is_invocable<T, output_stream_t<result_of::cellify_t<Inputs>>&&...>{}
        &&
        !returns_void<T, output_stream_t<result_of::cellify_t<Inputs>>&&...>{}
    )
)))> : std::true_type {};


template<class T, class Input, class = void>
struct invocable_convertible_with_execute : std::false_type {};

template<class T>
struct invocable_convertible_with_execute<T, std::tuple<>, DFP_C_REQUIRES((
    (std::is_invocable<T>{} && returns_void<T>{})
))> : std::true_type {};

template<class T, class... Inputs>
struct invocable_convertible_with_execute<T, std::tuple<Inputs...>, DFP_C_REQUIRES((sizeof...(Inputs) > 0 && (
    (
        std::is_invocable<T, output_sample_t<result_of::cellify_t<Inputs>> const&...>{}
        &&
        returns_void<T, output_sample_t<result_of::cellify_t<Inputs>> const&...>{}
    ) ||
    (
        std::is_invocable<T, output_frame_t<result_of::cellify_t<Inputs>> const&...>{}
        &&
        returns_void<T, output_frame_t<result_of::cellify_t<Inputs>> const&...>{}
    ) ||
    (
        std::is_invocable<T, output_stream_t<result_of::cellify_t<Inputs>>&&...>{}
        &&
        returns_void<T, output_stream_t<result_of::cellify_t<Inputs>>&&...>{}
    )
)))> : std::true_type {};


template<class T, class Input>
struct invocable_convertible_to_cell_builder
 : std::disjunction<invocable_convertible_with_apply<T, Input>, invocable_convertible_with_execute<T, Input>>
{};


template<class T, class Input>
struct is_convertible_to_cell_builder<T, Input, DFP_C_REQUIRES((!is_cell_builder<T>{}))>
 : invocable_convertible_to_cell_builder<T, Input>
{};


///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PLACEHOLDER_TRAITS_HPP
