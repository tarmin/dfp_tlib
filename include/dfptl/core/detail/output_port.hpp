/**
* @file core/detail/output_port.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_OUTPUT_PORT_HPP
#define DFPTL_CORE_DETAIL_OUTPUT_PORT_HPP


#include "dfptl/core/stream.hpp"
#include "dfptl/core/upstream_traits.hpp"
#include "dfptl/core/detail/port_traits.hpp"
#include "dfptl/core/detail/cell_traits.hpp"

#include "stl/tuple.hpp"
#include "stl/utility.hpp"
#include <exception>


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
///@addtogroup upstream
///@{
///@addtogroup core_detail
///@{
/** @defgroup output_port Generic output port for processing cell
* @brief Output port provides mechanism to generate samples or frames
* into stream produced by the downstream processing cells.
* @{
**/


/**
* @brief An output port is the part of an upstream cell which can stream out the sample (frame).
*
* This output_port should serve as a base class for specialized class dedicated to a type of processing cell.
* output_port childs are required to provides the following methods:
* @code{.cpp}
*  * sample_t<stream_type> renew_sample(Cell&) // to produce atomic stream
*  * constexpr sample_t<stream_type> renew_sample(Cell const&) const      // to produce atomic stream
*  * frame_t<stream_type> renew_frame(Cell&)   // to produce frame-based stream
*  * constexpr frame_t<stream_type> const& renew_frame(Cell const&) const // to produce frame-based stream
* @endcode
*
* @remarks @c StreamSpecifier template argument shall be a @ref stream_specifier or compilation failure will happen.
*
* @tparam StreamSpecifier is the type of the @ref stream_specifier "stream specifier" which specifies the stream
* outputted by the port.
* @tparam Cell defines the type of the processing cell whose the output_port class is part of.
* @tparam Emitter defines the type of the processing cell which produces the stream (by defautl is same a `Cell`).
* @tparam Derived defines the type final specialization of the output port.
**/
template<class StreamSpecifier, class Cell, class Emitter, class Derived, class = void>
class output_port
{ static_assert(is_stream_specifier<StreamSpecifier>{}, DFP_UNEXPECTED_ERROR); };


template<class StreamSpecifier, class Cell, class Emitter, class Derived, class = void>
class output_port_base
{
    friend Cell;

protected:
    static constexpr bool IS_OWNED_BY_EMITTER = std::is_same<Cell, Emitter>{};

public:
    typedef output_port_tag dfp_cell_tag;
    /**
    * @brief type of the @ref stream_specifier "stream specifier" defining output @ref stream produced by the port
    **/
    typedef StreamSpecifier                                                     stream_specifier_type;
    typedef Emitter                                                             emitter_type;
    typedef Cell                                                                cell_type;
    typedef Derived                                                             derived;
    typedef output_port<StreamSpecifier, constify_t<cell_type>, constify_t<emitter_type>, Derived> const  const_type;

    template<class T = stream_specifier_type>
    constexpr stream_specifier_type stream_specifier() const
    {
        static_assert(always_false<T>{}, DFP_UNEXPECTED_ERROR); /* should have been defined in child classes */
        return std::declval<stream_specifier_type>();
    }

    core::stream<stream_specifier_type, cell_type>
    DFP_MUTABLE_CONSTEXPR build_stream(cell_type& owner)
    { return core::stream<stream_specifier_type, cell_type>{ owner }; }

    typename core::stream<stream_specifier_type, cell_type>::to_const::type
    constexpr build_stream(cell_type const& owner) const
    { return typename core::stream<stream_specifier_type, cell_type>::to_const::type{ owner }; }


    /**
    * @brief Pour out a sample from the output port of the processing cell
    *
    * Given here for prototype specification, this method shall be defined in any child output_port
    * when a @ref atomic_stream "atomic stream" is produced by the processing cell.
    **/
    template<class S = stream_specifier_type, DFP_F_REQUIRES((S::IS_ATOMIC && always_false<S>{}))>
    sample_t<stream_specifier_type>          renew_sample(cell_type&)
    { return std::declval<sample_t<stream_specifier_type>>(); }

    /**
    * @brief const flavour of renew_sample()
    **/
    template<class S = stream_specifier_type, DFP_F_REQUIRES((S::IS_ATOMIC && always_false<S>{}))>
    constexpr sample_t<stream_specifier_type>     renew_sample(cell_type const&) const
    { return std::declval<sample_t<stream_specifier_type>>(); }

    /**
    * @brief Pour out a frame from the output port of the processing cell
    *
    * Given here for prototype specification, this method shall be defined in any child output_port
    * when a non-atomic stream is produced by the processing cell.
    **/
    template<class S = stream_specifier_type, DFP_F_REQUIRES((!S::IS_ATOMIC && always_false<S>{}))>
    frame_t<stream_specifier_type>                renew_frame(cell_type&)
    { return std::declval<frame_t<stream_specifier_type>>(); }

    template<class S = stream_specifier_type, DFP_F_REQUIRES((!S::IS_ATOMIC && always_false<S>{}))>
    frame_t<stream_specifier_type>                renew_frame(cell_type const&) const
    { return std::declval<frame_t<stream_specifier_type>>(); }

    template<class T = Derived, DFP_F_REQUIRES((!T::IS_OWNED_BY_EMITTER && supports_renew_stream<T>{}))>
    DFP_MUTABLE_CONSTEXPR frame_t<stream_specifier_type>  renew_frame(cell_type& owner)
    { return static_cast<derived* const>(this)->renew_stream(owner).drain(); }

    template<class T = Derived, DFP_F_REQUIRES((!T::IS_OWNED_BY_EMITTER && supports_const_renew_stream<T>{}))>
    constexpr frame_t<stream_specifier_type>              renew_frame(cell_type const& owner) const
    { return static_cast<derived const* const>(this)->renew_stream(owner).drain(); }

    template<class T = Derived, DFP_F_REQUIRES((
        stream_specifier_type::IS_ATOMIC && supports_renew_sample<T>{} && (T::IS_OWNED_BY_EMITTER || !supports_renew_stream<T>{})
    ))>
    DFP_MUTABLE_CONSTEXPR frame_t<stream_specifier_type>  renew_frame(cell_type& owner)
    { return frame_t<stream_specifier_type>{{ static_cast<derived* const>(this)->renew_sample(owner) }}; }

    template<class T = Derived, DFP_F_REQUIRES((
        stream_specifier_type::IS_ATOMIC && supports_const_renew_sample<T>{} && (T::IS_OWNED_BY_EMITTER || !supports_const_renew_stream<T>{})
    ))>
    constexpr frame_t<stream_specifier_type>              renew_frame(cell_type const& owner) const
    { return frame_t<stream_specifier_type>{{ static_cast<derived const* const>(this)->renew_sample(owner) }}; }

    template<class T = Derived, class... O>
    constexpr frame_t<stream_specifier_type>              renew_frame(cell_type const&, O...) const
    {
        static_assert(always_false<T>{}, DFP_UNEXPECTED_ERROR);
        return std::declval<frame_t<stream_specifier_type>>();
    }
}; // class output_port_base


template<class StreamSpecifier, class Cell, class Emitter, class Derived>
class output_port<StreamSpecifier, Cell, Emitter, Derived, DFP_C_REQUIRES((StreamSpecifier::IS_STATIC))>
 :  public  output_port_base<StreamSpecifier, Cell, Emitter, Derived>
{
    typedef output_port_base<StreamSpecifier, Cell, Emitter, Derived> base;

public:
    constexpr output_port() noexcept {}

    constexpr output_port(StreamSpecifier const&) noexcept {}

    void set_stream_specifier(StreamSpecifier const&) noexcept
    {}

    constexpr StreamSpecifier stream_specifier() const noexcept
    { return StreamSpecifier{}; }
}; // class output_port<... StreamSpecifier::IS_STATIC ...>


template<class StreamSpecifier, class Cell, class Emitter, class Derived>
class output_port<StreamSpecifier, Cell, Emitter, Derived, DFP_C_REQUIRES((!StreamSpecifier::IS_STATIC))>
 :  public  output_port_base<StreamSpecifier, Cell, Emitter, Derived>
{
    typedef output_port_base<StreamSpecifier, Cell, Emitter, Derived> base;

public:
    explicit constexpr output_port(StreamSpecifier const& stream_specifier)
     :  stream_specifier_(stream_specifier)
    {}

    void set_stream_specifier(StreamSpecifier const& src) noexcept
    { stream_specifier_ = src; }

    constexpr StreamSpecifier stream_specifier() const noexcept
    { return stream_specifier_; }

private:
    StreamSpecifier stream_specifier_;
}; // class output_port<... !StreamSpecifier::IS_STATIC ...>


///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_OUTPUT_PORT_HPP
