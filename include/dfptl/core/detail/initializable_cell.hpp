/**
* @file core/detail/initializable_cell.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_INITIALIZABLE_CELL_HPP
#define DFPTL_CORE_DETAIL_INITIALIZABLE_CELL_HPP


#include "dfptl/core/type_traits.hpp"

#include <boost/fusion/include/size.hpp>
#include <boost/fusion/container/map.hpp>
#include <boost/fusion/view/joint_view.hpp>
#include <boost/fusion/adapted/std_pair.hpp>

#include <stl/tuple.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{


// workaround for constexpr support
// as boot::fusion::map<> does not support constexpr
struct empty_map
{};


template<class T, class = void>
struct cell_initialization_storage
{ typedef typename std::remove_reference_t<std::unwrap_reference_t<T>>::initialization_storage_type type; };

template<class T>
using cell_initialization_storage_t = typename cell_initialization_storage<T>::type;


template<class KeyValue, class Inputs, class InputIndexesSequence, class = void>
struct initialization_storage;


template<class KeyValue, class NoInput>
struct initialization_storage<KeyValue, NoInput, std::index_sequence<>>
// need inheritance here or GCC (9/10/11) will sometimes weirdly assign 0 to size of map<KeyValue> !?
 :  boost::fusion::joint_view<boost::fusion::map<KeyValue>, boost::fusion::map<>>
{
    using view_type = boost::fusion::joint_view<boost::fusion::map<KeyValue>, boost::fusion::map<>>;
    constexpr static std::size_t SIZE = 1;

    initialization_storage() :  view_type(keyvalue_, empty_map_)
    {}

    initialization_storage(const initialization_storage& a)
     :  view_type(keyvalue_, empty_map_), keyvalue_(a.keyvalue_), empty_map_()
    {}

    view_type& view()
    { return *this; }

private:
    boost::fusion::map<KeyValue> keyvalue_;
    boost::fusion::map<> empty_map_;
};


template<class NoInput>
struct initialization_storage<void, NoInput, std::index_sequence<>>
{
    using view_type = boost::fusion::map<>;
    constexpr static std::size_t SIZE = 0;

    view_type& view()
    { return empty_map_; }

private:
    view_type empty_map_;
};


template<class Inputs>
struct initialization_storage<void, Inputs, std::index_sequence<0>>
{
private:
    using input_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;

public:
    using view_type = typename input_init_storage_type::view_type;
    constexpr static std::size_t SIZE = input_init_storage_type::SIZE;

    initialization_storage(input_init_storage_type&& input_init_storage)
     :  input_init_storage_(std::move(input_init_storage))
    {}

    view_type& view()
    { return input_init_storage_.view(); }

private:
    input_init_storage_type input_init_storage_;
};


template<class KeyValue, class Inputs>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} &&
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE > 0)
))>
{
private:
    using input_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<boost::fusion::map<KeyValue>, typename input_init_storage_type::view_type>;
    constexpr static std::size_t SIZE = input_init_storage_type::SIZE+1;

    initialization_storage(input_init_storage_type&& input_init_storage)
     :  input_init_storage_(std::move(input_init_storage)), view_(keyvalue_, input_init_storage_.view())
    {}

    initialization_storage(initialization_storage const& a)
     :  input_init_storage_(a.input_init_storage_), view_(view_type(keyvalue_, input_init_storage_.view()))
    {}

    view_type& view()
    { return view_; }

private:
    input_init_storage_type input_init_storage_;
    boost::fusion::map<KeyValue> keyvalue_;
    view_type view_;
};


template<class KeyValue, class Inputs>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} &&
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE == 0)
))>
{
private:
    using input_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<boost::fusion::map<KeyValue>, boost::fusion::map<>>;
    constexpr static std::size_t SIZE = 1;

    initialization_storage(input_init_storage_type&&)
     :  view_(keyvalue_, empty_map_)
    {}

    initialization_storage(initialization_storage const& a)
     :  keyvalue_(a.keyvalue_), empty_map_(), view_(keyvalue_, empty_map_)
    {}

    view_type& view()
    { return view_; }

private:
    boost::fusion::map<KeyValue> keyvalue_;
    boost::fusion::map<> empty_map_;
    view_type view_;
};


template<class Inputs>
struct initialization_storage<void, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE > 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE > 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<typename input0_init_storage_type::view_type, typename input1_init_storage_type::view_type>;
    constexpr static std::size_t SIZE = input0_init_storage_type::SIZE + input1_init_storage_type::SIZE;

    initialization_storage(
        input0_init_storage_type&& input0_init_storage,
        input1_init_storage_type&& input1_init_storage
    )
     :  input0_init_storage_(std::move(input0_init_storage)), input1_init_storage_(std::move(input1_init_storage)),
        view_(input0_init_storage_.view(), input1_init_storage_.view())
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_), input1_init_storage_(a.input1_init_storage_),
        view_(input0_init_storage_.view(), input1_init_storage_.view())
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    input1_init_storage_type input1_init_storage_;
    view_type view_;
};


template<class Inputs>
struct initialization_storage<void, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE > 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE == 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<typename input0_init_storage_type::view_type, boost::fusion::map<>>;
    constexpr static std::size_t SIZE = input0_init_storage_type::SIZE;

    initialization_storage(
        input0_init_storage_type&& input0_init_storage,
        input1_init_storage_type&&
    )
     :  input0_init_storage_(std::move(input0_init_storage)),
        view_(input0_init_storage_.view(), empty_map_)
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_),
        view_(input0_init_storage_.view(), empty_map_)
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    boost::fusion::map<> empty_map_;
    view_type view_;
};


template<class Inputs>
struct initialization_storage<void, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE == 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE > 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<typename input1_init_storage_type::view_type, boost::fusion::map<>>;
    constexpr static std::size_t SIZE = input1_init_storage_type::SIZE;

    initialization_storage(
        input0_init_storage_type&&,
        input1_init_storage_type&& input1_init_storage
    )
     :  input1_init_storage_(std::move(input1_init_storage)),
        view_(input1_init_storage_.view(), empty_map_)
    {}

    initialization_storage(initialization_storage const& a)
     :  input1_init_storage_(a.input1_init_storage_),
        view_(input1_init_storage_.view(), empty_map_)
    {}

    view_type& view()
    { return view_; }

private:
    input1_init_storage_type input1_init_storage_;
    boost::fusion::map<> empty_map_;
    view_type view_;
};


template<class Inputs>
struct initialization_storage<void, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE == 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE == 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::map<>;
    constexpr static std::size_t SIZE = 0;

    initialization_storage(input0_init_storage_type&&, input1_init_storage_type&&)
    {}

    view_type& view()
    { return empty_map_; }

private:
    boost::fusion::map<> empty_map_;
};


template<class KeyValue, class Inputs>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} &&
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE > 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE > 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;
    using inputs_view_type = boost::fusion::joint_view<typename input0_init_storage_type::view_type, typename input1_init_storage_type::view_type>;

public:
    using view_type = boost::fusion::joint_view<KeyValue, inputs_view_type>;
    constexpr static std::size_t SIZE = input0_init_storage_type::SIZE + input1_init_storage_type::SIZE + 1;

    initialization_storage(
        input0_init_storage_type&& input0_init_storage,
        input1_init_storage_type&& input1_init_storage
    )
     :  input0_init_storage_(std::move(input0_init_storage)), input1_init_storage_(std::move(input1_init_storage)),
        inputs_view_(input0_init_storage_.view(), input1_init_storage_.view()), view_(keyvalue_, inputs_view_)
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_), input1_init_storage_(a.input1_init_storage_),
        inputs_view_(input0_init_storage_.view(), input1_init_storage_.view()), view_(keyvalue_, inputs_view_)
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    input1_init_storage_type input1_init_storage_;
    inputs_view_type inputs_view_;
    KeyValue keyvalue_;
    view_type view_;
};


template<class KeyValue, class Inputs>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} &&
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE > 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE == 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<KeyValue, typename input0_init_storage_type::view_type>;
    constexpr static std::size_t SIZE = input0_init_storage_type::SIZE + 1;

    initialization_storage(input0_init_storage_type&& input0_init_storage, input1_init_storage_type&&)
     :  input0_init_storage_(std::move(input0_init_storage)),
        view_(keyvalue_, input0_init_storage_.view())
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_),
        view_(keyvalue_, input0_init_storage_.view())
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    KeyValue keyvalue_;
    view_type view_;
};


template<class KeyValue, class Inputs>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0, 1>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} &&
    (cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>::SIZE == 0) &&
    (cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>::SIZE > 0)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using input1_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<1, Inputs>>;

public:
    using view_type = boost::fusion::joint_view<KeyValue, typename input1_init_storage_type::view_type>;
    constexpr static std::size_t SIZE = input1_init_storage_type::SIZE + 1;

    initialization_storage(
        input0_init_storage_type&&,
        input1_init_storage_type&& input1_init_storage
    )
     :  input1_init_storage_(std::move(input1_init_storage)),
        view_(keyvalue_, input1_init_storage_.view())
    {}

    initialization_storage(initialization_storage const& a)
     :  input1_init_storage_(a.input1_init_storage_),
        view_(keyvalue_, input1_init_storage_.view())
    {}

    view_type& view()
    { return view_; }

private:
    boost::fusion::map<> empty_map_;
    input1_init_storage_type input1_init_storage_;
    KeyValue keyvalue_;
    view_type view_;
};


template<class Inputs, std::size_t... InputIndexes>
struct initialization_storage<void, Inputs, std::index_sequence<0, InputIndexes...>, DFP_C_REQUIRES((
    sizeof...(InputIndexes) > 1
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using others_init_storage_type = initialization_storage<void, std::tuple<std::tuple_element_t<InputIndexes, Inputs>...>, std::index_sequence<(InputIndexes-1)...>>;

public:
    using view_type = boost::fusion::joint_view<typename input0_init_storage_type::view_type, typename others_init_storage_type::view_type>;
    constexpr static std::size_t SIZE = input0_init_storage_type::SIZE + others_init_storage_type::SIZE;

    initialization_storage(input0_init_storage_type&& input0_init_storage,
        cell_initialization_storage_t<std::tuple_element_t<InputIndexes, Inputs>>&&... other_init_storages
    )
     :  input0_init_storage_(std::move(input0_init_storage)), others_init_storage_(std::move(other_init_storages)...),
        view_(input0_init_storage_.view(), others_init_storage_.view())
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_), others_init_storage_(a.others_init_storage_),
        view_(input0_init_storage_.view(), others_init_storage_.view())
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    others_init_storage_type others_init_storage_;
    view_type view_;
};


template<class KeyValue, class Inputs, std::size_t... InputIndexes>
struct initialization_storage<KeyValue, Inputs, std::index_sequence<0, InputIndexes...>, DFP_C_REQUIRES((
    !std::is_void<KeyValue>{} && (sizeof...(InputIndexes) > 1)
))>
{
private:
    using input0_init_storage_type = cell_initialization_storage_t<std::tuple_element_t<0, Inputs>>;
    using others_init_storage_type = initialization_storage<void, std::tuple<std::tuple_element_t<InputIndexes, Inputs>...>, std::index_sequence<(InputIndexes-1)...>>;
    using inputs_view_type = boost::fusion::joint_view<typename input0_init_storage_type::view_type, typename others_init_storage_type::view_type>;

public:
    using view_type = boost::fusion::joint_view<boost::fusion::map<KeyValue>, inputs_view_type>;
    constexpr static std::size_t SIZE = 1 + input0_init_storage_type::SIZE + others_init_storage_type::SIZE;

    initialization_storage(input0_init_storage_type&& input0_init_storage,
        cell_initialization_storage_t<std::tuple_element_t<InputIndexes, Inputs>>&&... other_init_storages
    )
     :  input0_init_storage_(std::move(input0_init_storage)), others_init_storage_(std::move(other_init_storages)...),
        inputs_view_(input0_init_storage_.view(), others_init_storage_.view()), view_(keyvalue_, inputs_view_)
    {}

    initialization_storage(initialization_storage const& a)
     :  input0_init_storage_(a.input0_init_storage_), others_init_storage_(a.others_init_storage_),
        inputs_view_(input0_init_storage_.view(), others_init_storage_.view()), view_(keyvalue_, inputs_view_)
    {}

    view_type& view()
    { return view_; }

private:
    input0_init_storage_type input0_init_storage_;
    others_init_storage_type others_init_storage_;
    inputs_view_type inputs_view_;
    boost::fusion::map<KeyValue> keyvalue_;
    view_type view_;
};


template<class Inputs, bool UnmoveableRunnable, std::size_t... InputIndexes>
class initializable_cell_base
{
public:
    static constexpr auto UNMOVEABLE_RUNNABLE = pack_or(
        bool_const<UnmoveableRunnable>{},
        std::remove_cvref_t<std::tuple_element_t<InputIndexes, Inputs>>::UNMOVEABLE_RUNNABLE...
    );
};


template<class Inputs, class InputIndexesSequence, class KeyValue, class Derived, bool UnmoveableRunnable>
class initializable_cell;


template<class Inputs, std::size_t... InputIndexes, class Derived, bool UnmoveableRunnable>
class initializable_cell<Inputs, std::index_sequence<InputIndexes...>, void, Derived, UnmoveableRunnable>
 :  public initializable_cell_base<Inputs, UnmoveableRunnable, InputIndexes...>
{
public:
    using initialization_storage_type = initialization_storage<void, Inputs, std::index_sequence<InputIndexes...>>;

protected:
    template<class T = Derived, DFP_F_REQUIRES((is_downstream<T>{}))>
    constexpr initialization_storage_type build_initialization_storage(T const& cell) const
    { return build_initialization_storage_(std::get<InputIndexes>(cell.detail().input_ports()).build_initialization_storage()...); }

    template<class T = Derived, DFP_F_REQUIRES((!is_downstream<T>{}))>
    constexpr initialization_storage_type build_initialization_storage(T const&) const
    { return build_initialization_storage_(); }

    void invoke_initialize_upwards(typename initialization_storage_type::view_type)
    {
        // invoke_initialize_upwards shall be implemented and accessible in every processing cell base (sink, source, node) 
        static_assert(always_false<Derived>{}, DFP_UNEXPECTED_ERROR);
    }

private:
    constexpr initialization_storage_type build_initialization_storage_(
        cell_initialization_storage_t<std::tuple_element_t<InputIndexes, Inputs>>&&... input_init_storages
    ) const
    { return {std::move(input_init_storages)...}; }
}; // class initializable_cell<... is_void<KeyValue> ...>


template<class Inputs, std::size_t... InputIndexes, class KeyValue, class Derived, bool UnmoveableRunnable>
class initializable_cell<Inputs, std::index_sequence<InputIndexes...>, KeyValue, Derived, UnmoveableRunnable>
 :  public initializable_cell_base<Inputs, UnmoveableRunnable, InputIndexes...>
{
public:
    using initialization_storage_type = initialization_storage<KeyValue, Inputs, std::index_sequence<InputIndexes...>>;

    template<class T = Derived, DFP_F_REQUIRES((is_downstream<T>{}))>
    initialization_storage_type build_initialization_storage(T const& cell) const
    { return build_initialization_storage_(std::move(std::get<InputIndexes>(cell.detail().input_ports()).build_initialization_storage())...); }

    template<class T = Derived, DFP_F_REQUIRES((!is_downstream<T>{}))>
    initialization_storage_type build_initialization_storage(T const&) const
    { return build_initialization_storage_(); }

    template<class InitializationStorageView>
    void invoke_initialize_upwards(InitializationStorageView initialization_storage)
    {
        // invoke_initialize_upwards shall be implemented and accessible in every processing cell base (sink, source, node) 
        static_assert(always_false<InitializationStorageView>{}, DFP_UNEXPECTED_ERROR);
    }

private:
    initialization_storage_type build_initialization_storage_(
        cell_initialization_storage_t<std::tuple_element_t<InputIndexes, Inputs>>&&... input_init_storages
    ) const
    { return {std::move(input_init_storages)...}; }
}; // class initializable_cell<... !is_void<KeyValue> ...>

} // namespace detail
} // inline namespace core
} // namespace dfp


#endif //DFPTL_CORE_DETAIL_INITIALIZABLE_CELL_HPP
