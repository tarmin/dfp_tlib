/**
* @file core/detail/downstream_cell.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_DOWNSTREAM_CELL_HPP
#define DFPTL_CORE_DETAIL_DOWNSTREAM_CELL_HPP


#include "dfptl/core/downstream_traits.hpp"
#include "dfptl/core/detail/input_port.hpp"
#include "dfptl/core/processing_context.hpp"

#include "stl/tuple.hpp"
#include "stl/utility.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template<template<class, class> class InputPort, class OwningCell, class Context, class UpstreamIndexSequence>
struct input_ports_downstream_cell
{};


template<template<class, class> class InputPort, class OwningCell, class Context, std::size_t... UpstreamIds>
struct input_ports_downstream_cell<InputPort, OwningCell, Context, std::index_sequence<UpstreamIds...>>
 :  std::tuple<InputPort<context_upstream_t<UpstreamIds, Context>, OwningCell>...>
{
    template<std::size_t UpstreamId>
    using input_port = InputPort<context_upstream_t<UpstreamId, Context>, OwningCell>;

    typedef std::tuple<input_port<UpstreamIds>...> tuple_type;

    constexpr input_ports_downstream_cell(Context&& context)
     :  tuple_type(input_port<UpstreamIds>{
            std::get<UpstreamIds>(std::move(context))
        }...)
    {}
};


template<class InputsTuple, class InputIndexSequence>
struct are_inputs_copy_constructible_impl : std::false_type
{};

template<class Context>
struct are_inputs_copy_constructible
 :  are_inputs_copy_constructible_impl<typename Context::upstreams_tuple, typename Context::upstream_index_sequence_type>
{};

template<class InputsTuple, std::size_t... InputIndexes>
struct are_inputs_copy_constructible_impl<InputsTuple, std::index_sequence<InputIndexes...>>
 :  std::conjunction<std::is_copy_constructible<std::tuple_element_t<InputIndexes, InputsTuple>>...>
{};


template<template<class, class> class InputPort, class Context, class Derived, class = void>
class downstream_cell
{
    static_assert(Context::UPSTREAM_COUNT > 0, "downstream cell shall at least have one input");
    static_assert(always_false<Context>{}, DFP_UNEXPECTED_ERROR);
};


template<template<class, class> class InputPort, class Context, class Derived>
class downstream_cell<InputPort, Context, Derived, DFP_C_REQUIRES((Context::UPSTREAM_COUNT > 0))>
    :       input_ports_downstream_cell<InputPort, Derived, Context, typename Context::upstream_index_sequence_type>
{
    typedef input_ports_downstream_cell<InputPort, Derived, Context, typename Context::upstream_index_sequence_type> input_ports_member;
    template<class, class> friend struct runner;

public:
    typedef typename input_ports_member::tuple_type input_ports_type;
    typedef Context input_cells_type; // public visibility required for helper is_downstream<> (friend does not help with gcc)
    typedef typename Context::upstream_index_sequence_type input_index_sequence;
    template<
        std::size_t Index, std::size_t inputs_tuple_size = std::tuple_size<input_ports_type>{},
        class = DFP_CONCEPT_ASSERT((bool_const<Index < inputs_tuple_size>))
    >
    using input_port_type = std::tuple_element_t<Index, input_ports_type>;

    // remove copy-constructor if one of the input is not copy-constructible
    template<class T = input_cells_type, DFP_F_REQUIRES((are_inputs_copy_constructible<T>{}))>
    downstream_cell(downstream_cell const&) = delete;

    constexpr downstream_cell(Context&& context) : input_ports_member(std::move(context))
    {}


    constexpr input_ports_type const& input_ports() const noexcept
    { return *static_cast<input_ports_member const* const>(this); }

    template<class T = input_cells_type, DFP_F_REQUIRES((std::tuple_size<T>() == 1))>
    constexpr output_stream_specifier_t<typename input_port_type<0>::input_cell_value> input_stream_specifier() const noexcept
    { return input_stream_specifier(0_c); }

    template<class Integral, Integral Index>
    constexpr output_stream_specifier_t<typename input_port_type<(std::size_t) Index>::input_cell_value>
    input_stream_specifier(integral_const<Integral, Index> index) const noexcept
    { return input_port<index>().stream_specifier(); }


    template<std::size_t Index>
    static constexpr output_stream_specifier_t<typename input_port_type<Index>::input_cell_value>
    output_stream_specifier(input_cells_type const& inputs, size_t_const<Index> = size_t_const<Index>{}) noexcept
    { return std::get<Index>(inputs).output_stream_specifier(); }

    /**
    * @brief Returns the reference on the cell @ref input_port "input port" for the given @c Index value.
    */
    template<class T = input_cells_type, DFP_F_REQUIRES((std::tuple_size<T>() == 1))>
    DFP_MUTABLE_CONSTEXPR input_port_type<0>& input_port() noexcept
    { return this->input_port<0>(); }

    template<std::size_t Index>
    DFP_MUTABLE_CONSTEXPR input_port_type<Index>&    input_port(size_t_const<Index> = size_t_const<Index>{}) noexcept
    // note that static_cast is required here to clarify possible ambiguous resolution of this as tuple
    // because tuple can be found as base class multiple times in inheritance tree
    { return std::get<Index>(*static_cast<input_ports_type* const>(this)); }

    template<class Integral, Integral Index, DFP_F_REQUIRES((!std::is_same<Integral, std::size_t>{}))>
    DFP_MUTABLE_CONSTEXPR input_port_type<(std::size_t) Index>&
    input_port(integral_const<Integral, Index> index) noexcept
    { static_assert(index >= 0, "index shall be a positive number");
      return std::get<(std::size_t) index>(*static_cast<input_ports_type* const>(this)); }

    /**
    * @brief const flavour of input_port()
    */
    template<std::size_t Index>
    constexpr input_port_type<Index> const& input_port(size_t_const<Index> = size_t_const<Index>{}) const noexcept
    { return std::get<Index>(*static_cast<input_ports_type const* const>(this)); }

    input_ports_type& input_ports() noexcept
    { return *static_cast<input_ports_type* const>(this); }
}; // class downstream_cell


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_DOWNSTREAM_CELL_HPP
