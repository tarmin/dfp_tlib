/**
* @file core/detail/stream.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_STREAM_HPP
#define DFPTL_CORE_DETAIL_STREAM_HPP


#include "dfptl/core/stream_specifier.hpp"
#include "dfptl/core/detail/port_traits.hpp"
#include "dfptl/core/detail/cell_traits.hpp"

#include <functional>

#include <boost/fusion/include/map.hpp>
#include <boost/fusion/include/pair.hpp>
#include <boost/fusion/include/value_at_key.hpp>
#include <boost/fusion/include/has_key.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
/**@addtogroup core_detail
* @{
**/
/**
* @brief Handles the data signal exchanged between processing cells.
*
* Stream instance transports data signal between ports of processing cells.

* From design perspective, stream life time shall be the duration of the data exchange from the output port
* to the associated input port.
*
* @tparam Emitter type of the cell emitting this stream type
**/
template<class Specifier, class Emitter, class = DFP_CONCEPT_ASSERT((models_frame_concept<frame_t<Specifier>>))>
class stream
{
    typedef tag_t<Specifier>    tag_clue;

public:
    typedef stream_tag  dfp_cell_tag;
    typedef Specifier   specifier_type;
    typedef Emitter     emitter_type;

    static constexpr auto FRAME_EXTENT  = specifier_type::FRAME_EXTENT;
    static constexpr bool IS_STATIC     = specifier_type::IS_STATIC;
    static constexpr bool IS_ATOMIC     = stream::FRAME_EXTENT == 1;

    /**
    * @brief Provides typedef @c type with tag map available from this stream
    */
    struct tag_map
    {
    private:
        template<class InputPorts>
        struct inputs_view;

        template<class InputPort>
        struct inputs_view<std::tuple<InputPort>> : port_stream_t<InputPort>::tag_map::type {};

        template<class InputPort, class... InputPorts>
        struct inputs_view<std::tuple<InputPort, InputPorts...>>
            :   boost::fusion::joint_view<typename port_stream_t<InputPort>::tag_map::type, inputs_view<std::tuple<InputPorts...>>> {};

        template<class T, class = void>
        struct current_view;

        template<class T>
        struct current_view<T, DFP_C_REQUIRES((is_downstream<T>{} && !std::is_same<tag_clue, null_tag>{}))>
            :   boost::fusion::joint_view<inputs_view<typename T::input_ports_type>, boost::fusion::map<boost::fusion::pair<tag_clue, stream>>> {};

        template<class T>
        struct current_view<T, DFP_C_REQUIRES((is_downstream<T>{} && std::is_same<tag_clue, null_tag>{}))>
            :   inputs_view<typename T::input_ports_type> {};

        template<class T>
        struct current_view<T, DFP_C_REQUIRES((!is_downstream<T>{} && !std::is_same<tag_clue, null_tag>{}))>
            :   boost::fusion::map<boost::fusion::pair<tag_clue, stream>> {};

        template<class T>
        struct current_view<T, DFP_C_REQUIRES((!is_downstream<T>{} && std::is_same<tag_clue, null_tag>{}))>
            :   boost::fusion::map<> {};

    public:
        typedef current_view<Emitter> type;
    }; // struct tag_map


    template<class StreamTag>
    struct at_tag { typedef typename boost::fusion::result_of::value_at_key<typename tag_map::type, StreamTag>::type type; };

    template<class StreamTag>
    using at_tag_t = typename at_tag<StreamTag>::type;


    template<class StreamTag>
    using has_tag = std::bool_constant<boost::fusion::result_of::has_key<typename tag_map::type, StreamTag>::value>;


    /**
    * @brief Creates a new instance of stream given the parameters for the inner frame initialization
    */
    explicit constexpr stream(emitter_type& emitter) : emitter_(emitter) {}

    /**
    * @brief Fires all upstream cells of the processing branch and returns a frame (or const reference on frame) of processed samples.
    **/
    //TODO: should use upstream_cell::drain instead of output_port
    //output_port should not be accessible publicly
    template<class T = emitter_type>
    decltype(std::declval<T&>().output_port().renew_frame(std::declval<T&>()))
    DFP_MUTABLE_CONSTEXPR drain()
    { return emitter_.          output_port().renew_frame(emitter_); }

    template<class T = emitter_type>
    decltype(std::declval<T&>().output_port().renew_frame(std::declval<T&>()))
    constexpr drain() const
    { return emitter_.          output_port().renew_frame(emitter_); }

    // this one is a C++11 workaround as constexpr is not allowed on non-const method
    template<class T = emitter_type>
    decltype(std::declval<T&>().output_port().renew_frame(std::declval<T&>()))
    constexpr cdrain() const
    { return emitter_.          output_port().renew_frame(emitter_); }

protected:
    emitter_type& emitter_;
}; // class stream


///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_STREAM_HPP
