/**
* @file core/detail/execute_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_EXECUTE_NODE_HPP
#define DFPTL_CORE_DETAIL_EXECUTE_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/stream_specifier.hpp"
#include "dfptl/core/detail/ebco_member.hpp"

#include "stl/utility.hpp"


#define DFP_CORE_EXECUTE core::execute


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief Sample-based node which applies a function-object (as defined by STL) for each input stream sample.
*
* @tparam Input is the type list of the processing cells (source or node) bound to the input ports to this node.
* @tparam Descriptor is kernel to process the stream.
**/
template<class Input, class Descriptor, class = void>
struct execute_node
{ static_assert(always_false<execute_node>{}, DFP_UNEXPECTED_ERROR); };


template<class Input, class Descriptor> class execute_regular_node
{ static_assert(always_false<execute_regular_node>{}, DFP_UNEXPECTED_ERROR); };


template<class Input, class Descriptor> class execute_unregular_node
{ static_assert(always_false<execute_unregular_node>{}, DFP_UNEXPECTED_ERROR); };


template<class Input, class Callable>
struct execute_node<
    Input, sample_based_processing<Callable>,
    DFP_C_REQUIRES((supports_const_call_operator<Callable, output_sample_t<std::tuple_element_t<0, Input>>>{}))
>:      execute_regular_node<Input, sample_based_processing<Callable>>
{ using execute_regular_node<Input, sample_based_processing<Callable>>::execute_regular_node; };


template<class Input, class Callable>
struct execute_node<
    Input, sample_based_processing<Callable>,
    DFP_C_REQUIRES((supports_mutable_call_operator<Callable, output_sample_t<std::tuple_element_t<0, Input>>>{}))
>:      execute_unregular_node<Input, sample_based_processing<Callable>>
{ using execute_unregular_node<Input, sample_based_processing<Callable>>::execute_unregular_node; };


template<class Input, class Callable>
struct execute_node<
    Input, frame_based_processing<Callable>,
    DFP_C_REQUIRES((supports_const_call_operator<Callable, output_frame_t<std::tuple_element_t<0, Input>>>{}))
>:      execute_regular_node<Input, frame_based_processing<Callable>>
{ using execute_regular_node<Input, frame_based_processing<Callable>>::execute_regular_node; };


template<class Input, class Callable>
struct execute_node<
    Input, frame_based_processing<Callable>,
    DFP_C_REQUIRES((supports_mutable_call_operator<Callable, output_frame_t<std::tuple_element_t<0, Input>>>{}))
>:      execute_unregular_node<Input, frame_based_processing<Callable>>
{ using execute_unregular_node<Input, frame_based_processing<Callable>>::execute_unregular_node; };


template<class Input, class Callable>
class execute_regular_node<Input, sample_based_processing<Callable>>
 :  public  node<Input, as_input0_tag, execute_regular_node<Input, sample_based_processing<Callable>>>,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable> callable_member;
    typedef  node<Input, as_input0_tag, execute_regular_node>   node_type;

public:
    /**
    * @brief Creates a new instance of execute_regular_node.
    *
    * @param input is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr execute_regular_node(Input&& input, CallableArgs&&... callable_args)
     :  node_type{std::forward<Input>(input)}, callable_member(std::forward<CallableArgs>(callable_args)...)
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param input is the sample on which to apply the operation.
    */
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) decltype(0_uc)
    forward_sample(input_sample_t<node_type> const& input) const
    { callable_member::get()(input); return 0_uc; }
}; // class execute_regular_node<... sample_based_processing ...>


template<class Input, class Callable>
class execute_regular_node<Input, frame_based_processing<Callable>>
 :  private ebco_member<Callable>,
    public  node<Input, as_input0_tag, execute_regular_node<Input, frame_based_processing<Callable>>>
{
    typedef ebco_member<Callable>   callable_member;
    typedef node<Input, as_input0_tag, execute_regular_node>   node_type;

public:
    /**
    * @brief Creates a new instance of execute_regular_node.
    *
    * @param input is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr execute_regular_node(Input&& input, CallableArgs&&... callable_args)
     :  callable_member{std::forward<CallableArgs>(callable_args)...}, node_type{std::forward<Input>(input)}
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param input are the samples on which to apply the operation.
    */
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) decltype(0_uc)
    forward_frame(input_frame_t<node_type> const& input) const
    { callable_member::get()(input); return 0_uc; }
}; // class execute_regular_node<... frame_based_processing ...>


template<class Input, class Callable>
class execute_unregular_node<Input, sample_based_processing<Callable>>
 :  public  node<Input, as_input0_tag, execute_unregular_node<Input, sample_based_processing<Callable>>>,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable>   callable_member;
    typedef node<Input, as_input0_tag, execute_unregular_node> node_type;

public:
    /**
    * @brief Creates a new instance of execute_unregular_node.
    *
    * @param input is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr execute_unregular_node(Input&& input, CallableArgs&&... callable_args)
     :  node_type{std::forward<Input>(input)}, callable_member{std::forward<CallableArgs>(callable_args)...}
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param input are the samples on which to apply the operation.
    */
    decltype(0_uc) forward_sample(input_sample_t<node_type> const& input)
    { callable_member::get()(input); return 0_uc; }
}; // class execute_unregular_node<... sample_based_processing ...>


template<class Input, class Callable>
class execute_unregular_node<Input, frame_based_processing<Callable>>
 :  public  node<Input, as_input0_tag, execute_unregular_node<Input, frame_based_processing<Callable>>>,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable>   callable_member;
    typedef node<Input, as_input0_tag, execute_unregular_node> node_type;

public:
    /**
    * @brief Creates a new instance of execute_unregular_node.
    *
    * @param input is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr execute_unregular_node(Input&& input, CallableArgs&&... callable_args)
     :  node_type{std::forward<Input>(input)}, callable_member{std::forward<CallableArgs>(callable_args)...}
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param input is the sample frame on which to apply the operation.
    */
    decltype(0_uc) forward_frame(input_frame_t<node_type> const& input)
    { callable_member::get()(input); return 0_uc; }
}; // class execute_unregular_node<... frame_based_processing ...>


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_EXECUTE_NODE_HPP
