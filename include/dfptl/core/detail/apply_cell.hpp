/**
* @file core/detail/apply_cell.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_APPLY_CELL_HPP
#define DFPTL_CORE_DETAIL_APPLY_CELL_HPP


#include "dfptl/core/processing_descriptor.hpp"
#include "dfptl/core/detail/apply_node.hpp"
#include "dfptl/core/detail/apply_sink.hpp"
#include "dfptl/core/detail/apply_source.hpp"

#include "stl/experimental/type_traits.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class T>
struct is_processing_descriptor : std::false_type {};


template<class Invocable, processing_driving ProcessingDriving>
struct is_processing_descriptor<processing_descriptor<Invocable, ProcessingDriving>> : std::true_type {};


/**
* @brief Statically polymorphic cell which is resolved as apply_source or apply_node
**/
template<class Inputs, class InputIndexesSequence, class Descriptor, class Runner, class Derived, class = void>
struct apply_cell_impl
{
    static_assert(is_processing_descriptor<Descriptor>{}, "Descriptor template parameter shall be a processing_descriptor");
    static_assert(always_false<Descriptor>{}, "arguments type and count supported by the callable passed to apply cell shall be compatible with sample types of its input streams and their count");
};


template<class Inputs, class Descriptor, class Runner>
struct apply_cell : apply_cell_impl<Inputs, index_sequence_from_t<Inputs>, Descriptor, Runner, apply_cell<Inputs, Descriptor, Runner>>
{ using apply_cell_impl<Inputs, index_sequence_from_t<Inputs>, Descriptor, Runner, apply_cell<Inputs, Descriptor, Runner>>::apply_cell_impl; };

/**
* @brief partial template specialization which aliases as the apply_node template class
*
* Aliases as the apply_node if the given @c Callable has some input parameter, is sample-based and has non-void return type
**/
template<class Context, std::size_t... InputIndexes, class Callable, class Derived>
struct apply_cell_impl<
    Context, std::index_sequence<InputIndexes...>, sample_based_processing<Callable>, template_pack<run_never>, Derived,
    DFP_C_REQUIRES((
        (sizeof...(InputIndexes) > 0)
        &&
        !returns_void       <Callable, output_sample_t<context_upstream_t<InputIndexes, Context>> const&...>{}
        &&
        std::is_invocable   <Callable, output_sample_t<context_upstream_t<InputIndexes, Context>> const&...>{}
    ))
> :     apply_node<Context, sample_based_processing<Callable>>
{ using apply_node<Context, sample_based_processing<Callable>>::apply_node; };


/**
* @brief partial template specialization which aliases as the apply_node template class
*
* Aliases as the apply_node if the given @c Descriptor has some input parameter, is frame-based and has non-void return type
**/
template<class Inputs, std::size_t... InputIndexes, class Callable, class Derived>
struct apply_cell_impl<
    Inputs, std::index_sequence<InputIndexes...>, frame_based_processing<Callable>, template_pack<run_never>, Derived,
    DFP_C_REQUIRES((
        (sizeof...(InputIndexes) > 0)
        &&
        !(returns_void<Callable, output_frame_t<std::tuple_element_t<InputIndexes, Inputs>> const&...>{})
        &&
        std::is_invocable<Callable, output_frame_t<std::tuple_element_t<InputIndexes, Inputs>> const&...>{}

    ))
> : apply_node<Inputs, frame_based_processing<Callable>>
{ using apply_node<Inputs, frame_based_processing<Callable>>::apply_node; };


/**
* @brief partial template specialization which aliases as the apply_source template class
*
* Aliases as the apply_source if the given @c Descriptor has no input parameter, is sample-based and has non-void return type
**/
template<class Context, std::size_t... InputIndexes, class Callable, processing_driving Driving, class Derived>
struct apply_cell_impl<
    Context, std::index_sequence<InputIndexes...>, processing_descriptor<Callable, Driving>, template_pack<run_never>, Derived,
    DFP_C_REQUIRES((
        !returns_void<Callable>{}
        &&
        std::is_invocable<Callable>{}
    ))
> : public  apply_source<Context, processing_descriptor<Callable, Driving>>
{
private:
    typedef apply_source<Context, processing_descriptor<Callable, Driving>> base;

public:
    template<class... FunctionArgs>
    constexpr apply_cell_impl(Context&& context, FunctionArgs&&... function_args)
     :  base(std::forward<Context>(context), std::forward<FunctionArgs>(function_args)...)
    {}
};


/**
* @brief partial template specialization which aliases as the apply_function_sink template class
*
* Aliases as the apply_function_sink if the given @c Descriptor has some input parameter and void return type
**/
template<class Inputs, std::size_t... InputIndexes, class Descriptor, template<class> class Runner, class Derived>
struct apply_cell_impl<
    Inputs, std::index_sequence<InputIndexes...>, Descriptor, template_pack<Runner>, Derived,
    //TODO: add static check to ensure that RunnerArgs is suitable with Runner
    DFP_C_REQUIRES((
        (sizeof...(InputIndexes) > 0)  &&
        returns_void<typename Descriptor::invocable, output_sample_t<std::tuple_element_t<InputIndexes, Inputs>> const&...>{}
    ))
> : apply_sink<Inputs, typename Descriptor::invocable, Runner, Derived>
{ using apply_sink<Inputs, typename Descriptor::invocable, Runner, Derived>::apply_sink; };


template<class Descriptor, template<class> class Runner, class... RunnerArgs, class... Args, std::size_t... RunnerArgsIs>
constexpr auto apply(std::tuple<RunnerArgs...>&& runner_args_tuple, std::index_sequence<RunnerArgsIs...>, Args&&... args) /** @cond */ ->
decltype(make_placeholder<
    std::tuple<Descriptor, template_pack<Runner>>, apply_cell
>(std::forward<RunnerArgs>(std::get<RunnerArgsIs>(std::move(runner_args_tuple)))..., std::forward<Args>(args)...)) /** @endcond */
{ return make_placeholder<
    std::tuple<Descriptor, template_pack<Runner>>, apply_cell
>(std::forward<RunnerArgs>(std::get<RunnerArgsIs>(std::move(runner_args_tuple)))..., std::forward<Args>(args)...); }


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif //DFPTL_CORE_DETAIL_APPLY_CELL_HPP
