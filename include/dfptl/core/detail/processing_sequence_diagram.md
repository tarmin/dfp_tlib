```plantuml
-> node_output_port ++ : renew_sample
node_output_port -> node_output_port ++ : _renew_sample
alt support_process_sample<node>
 node_output_port -> owner.node : process_sample
else support_forward_sample<node>
 node_output_port -> owner.node : forward_sample
node_output_port <-- owner.node --
else support_process_stream_returning_sample<node>
 node_output_port -> owner.node : process_stream
node_output_port <-- owner.node --
end
<-- node_output_port --
```

```plantuml
-> node_output_port ++ : renew_stream
alt (port) !IS_OWNED_BY_EMITTER
node_output_port -> node_output_port ++ : _renew_stream
    alt support_process_stream<node>
    node_output_port -> owner.node : process_stream
    else support_forward_stream<node> ??
    node_output_port -> owner.node : forward_sample
    node_output_port <-- owner.node --
    end
else (port) IS_OWNED_BY_EMITTER
    node_output_port -> output_port ++ : renew_stream
    node_output_port <-- output_port -- : stream<specifier, cell>{}
end
<-- node_output_port --
```
