/**
* @file core/detail/type_traits.hpp
* TODO: rename into tag_traits.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_TYPE_TRAITS_HPP
#define DFPTL_CORE_DETAIL_TYPE_TRAITS_HPP


#include "dfptl/core/cplusplus.hpp"
#include "dfptl/core/integral_const.hpp"

#include <cstddef>
#include <type_traits>


namespace dfp
{
inline namespace core
{


template<std::size_t> struct as_input_tag;


namespace detail
{


template<class T>
struct is_as_input_tag : std::false_type {};

template<std::size_t N>
struct is_as_input_tag<as_input_tag<N>> : std::true_type {};


template<class AsInputTag, class = DFP_CONCEPT_ASSERT((is_as_input_tag<AsInputTag>))>
struct input_index_of;

template<std::size_t N>
struct input_index_of<as_input_tag<N>> : size_t_const<N> {};


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_TYPE_TRAITS_HPP
