/**
* @file core/detail/processing_graph.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PROCESSING_GRAPH_HPP
#define DFPTL_CORE_PROCESSING_GRAPH_HPP


#include "dfptl/core/running_tree.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class TerminalCells, class = void>
class processing_graph
{ static_assert(always_false<processing_graph>{}, DFP_UNEXPECTED_ERROR); };

template<class Sink, bool RequiresImplicitRun>
class processing_tree;


template<bool RequiresImplicitRun, class DownmostCells, class Derived>
struct processing_graph_base
{
    static_assert(is_specialization<DownmostCells, std::tuple>{}, "DownmostCells shall be a tuple");
    static_assert(always_false<processing_graph_base>{}, DFP_UNEXPECTED_ERROR);
};


template<bool RequiresImplicitRun, class... DownmostCells, class Derived>
struct processing_graph_base<RequiresImplicitRun, std::tuple<DownmostCells...>, Derived>
{
    static_assert(pack_and(!std::is_rvalue_reference<DownmostCells>{}...), DFP_UNEXPECTED_ERROR);

private:
    typedef std::tuple_element_t<0, std::tuple<DownmostCells...>> in_progress_branch;

public:
    static constexpr bool_const<RequiresImplicitRun> REQUIRES_IMPLICIT_RUN = bool_const<RequiresImplicitRun>{};
    typedef processing_graph_tag dfp_tag;
    typedef std::tuple<std::remove_cvref_t<DownmostCells>...> downmost_cells_tuple;

    struct result_of
    {
        template<class CellBuilder>
        struct base
        {
        protected:
            typedef std::remove_cvref_t<CellBuilder> cell_builder_type;
        }; // struct base

        template<class CellBuilder>
        class extend :  public base<CellBuilder>
        {
            typedef base<CellBuilder> base_type;

        public:
            typedef core::processing_context<
                typename std::remove_cvref_t<CellBuilder>::tag_type,
                std::conditional_t<std::is_lvalue_reference<in_progress_branch>{}, std::remove_cvref_t<in_progress_branch> const&, std::remove_cvref_t<in_progress_branch>>
            > context_type;

        private:
            static constexpr auto REQUIRES_IMPLICIT_RUN = base_type::cell_builder_type::REQUIRES_IMPLICIT_RUN;
            typedef typename base_type::cell_builder_type::template cell_t<context_type> cell_type;

        public:
            typedef std::conditional_t<
                is_sink<cell_type>{},
                processing_tree<cell_type, REQUIRES_IMPLICIT_RUN>,
                processing_graph<core::result_of::tuple_cat_t<std::tuple<cell_type>, but_first_t<DownmostCells...>>>
            > type;
        }; // class extend<>

        template<class ProcessingGraph, class = void>
        class expand
        {
            static_assert(is_processing_graph<ProcessingGraph>{}, DFP_UNEXPECTED_ERROR);
    
        public:
            typedef processing_graph<std::tuple<std::tuple_element_t<0, std::remove_cvref_t<ProcessingGraph>>, DownmostCells...>> type;
//            typedef processing_graph<std::tuple<std::tuple_element_t<0, ProcessingGraph>, DownmostCells...>> type;
        }; // class expand<...>

        template<class CellBuilder>
        class expand<CellBuilder, DFP_C_REQUIRES((is_cell_builder<CellBuilder>{}))> :  public base<CellBuilder>
        {
            typedef base<CellBuilder> base_type;

        public:
            typedef core::processing_context<typename std::remove_cvref_t<CellBuilder>::tag_type, std::remove_cvref_t<DownmostCells> const&...> context_type;

        private:
            typedef typename base_type::cell_builder_type::template cell_t<context_type> cell_type;

        public:
            typedef processing_graph<std::tuple<cell_type, DownmostCells...>> type;
        }; // class_expand<>

        template<class CellBuilder>
        class collect :  public base<CellBuilder>
        {
    static constexpr std::size_t LAST_BRANCH_ID = sizeof...(DownmostCells)-1;
            typedef base<CellBuilder> base_type;

        public:
            typedef typename core::processing_context<
                typename std::remove_cvref_t<CellBuilder>::tag_type,
                std::conditional_t<std::is_lvalue_reference<DownmostCells>{}, std::remove_cvref_t<DownmostCells> const&, std::remove_cvref_t<DownmostCells>>...
            >::reverse_type context_type;

        private:
            static constexpr auto REQUIRES_IMPLICIT_RUN = base_type::cell_builder_type::REQUIRES_IMPLICIT_RUN;
            typedef typename base_type::cell_builder_type::template cell_t<context_type> cell_type;

        public:
            typedef std::conditional_t<
                is_sink<cell_type>{},
                processing_tree<cell_type, REQUIRES_IMPLICIT_RUN>,
                processing_graph<std::tuple<cell_type>>
            > type;
        }; // class collect<>
    }; // struct result_of


    DFP_MUTABLE_CONSTEXPR downmost_cells_tuple get_downmost_cells() &&
    {
        static_assert(always_false<Derived>{}, "this method shall be defined in Derived class");
        return static_cast<Derived* const>(this)->get_downmost_cells();
    }

    constexpr downmost_cells_tuple get_downmost_cells() const &
    {
        static_assert(always_false<Derived>{}, "this method shall be defined in Derived class");
        return static_cast<Derived const* const>(this)->get_downmost_cells();
    }
};


// This specialization is actually a simplifyed implementation of the generic implementation based on tuple.
// It is not at all mandantory (generic implementation covers this one) but it is kept for sake of comprehensiveness. 
template<class TerminalCell>
// TODO: !is_sink<T> is a workaround here, need to investigate why some sinks are not filtered out by is_upstream
class processing_graph<TerminalCell, DFP_C_REQUIRES((is_upstream<TerminalCell>{} && !is_sink<TerminalCell>{}))>
 :  public  std::remove_cvref_t<TerminalCell>,
    public  processing_graph_base<false, std::tuple<TerminalCell>, processing_graph<TerminalCell>>
{
    //TODO: add static assert !is_sink
    typedef processing_graph_base<false, std::tuple<TerminalCell>, processing_graph> base;

public:
    typedef std::remove_cvref_t<TerminalCell> terminal_cell_type;
    typedef std::tuple<terminal_cell_type> terminal_cells_tuple;
    typedef typename base::downmost_cells_tuple downmost_cells_tuple;
    typedef typename base::result_of result_of;

private:
    template<class, class> friend class processing_graph;

    DFP_IF_NOT(DFP_CONSTEXPR_MAY_CRASH_COMPILER)(constexpr,) terminal_cell_type const& get_terminal_cell() const
    { return static_cast<terminal_cell_type const&>(*this); }

    DFP_MUTABLE_CONSTEXPR terminal_cell_type& to_terminal_cell() &
    { return static_cast<terminal_cell_type&>(*this); }

    constexpr terminal_cell_type const& to_terminal_cell() const &
    { return static_cast<terminal_cell_type const&>(*this); }

    DFP_MUTABLE_CONSTEXPR terminal_cell_type&& to_terminal_cell() &&
    { return std::move(to_terminal_cell()); }

    constexpr terminal_cell_type const&& to_terminal_cell() const &&
    { return std::move(to_terminal_cell()); }

    DFP_MUTABLE_CONSTEXPR terminal_cell_type& get_terminal_cell()
    { return static_cast<terminal_cell_type&>(*this); }

public:
    constexpr terminal_cell_type const&  get_downmost_cell() const
    { return to_terminal_cell(); }

    //TODO: replace by get_processing_context()
    constexpr std::tuple<terminal_cell_type const&> get_downmost_cells() const
    { return { to_terminal_cell() }; }

    template<class T = terminal_cell_type, DFP_F_REQUIRES((std::is_copy_constructible<T>{}))>
    constexpr processing_graph(TerminalCell&& terminal_cell)
     :  terminal_cell_type(std::forward<TerminalCell>(terminal_cell))
    {}

    template<class T = terminal_cell_type, DFP_F_REQUIRES((!std::is_copy_constructible<T>{}))>
    constexpr processing_graph(TerminalCell&& terminal_cell)
     :  terminal_cell_type(std::move(terminal_cell))
    {}

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template extend<CellBuilder>::type extend(CellBuilder&& cell_builder) &&
    { return { cell_builder.build(typename result_of::template extend<CellBuilder>::context_type::upstreams_tuple{ std::move(to_terminal_cell()) }) }; }

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{})
    ))>
    constexpr typename result_of::template extend<CellBuilder>::type extend(CellBuilder&& cell_builder) const&
    {
        static_assert(
            std::is_copy_constructible<std::remove_reference_t<TerminalCell>>{},
            "Graph is not copy-constructible so its lvalue-reference cannot be used to 'extend' it."\
            "Either consider rather to work with an rvalue-reference or take leverage of the probe()/sample_from_tag() couple."
        );
        return { cell_builder.build(typename result_of::template extend<CellBuilder>::context_type::upstreams_tuple{ to_terminal_cell() }) };
    }

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        !DFP_CPP14_SUPPORT && std::is_copy_constructible<terminal_cell_type>{}
    ))>
    constexpr typename result_of::template extend<CellBuilder>::type extend(CellBuilder&& cell_builder) const
    { return { cell_builder.build(typename result_of::template extend<CellBuilder>::context_type::upstreams_tuple{ to_terminal_cell() }) }; }

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{})
    ))>
    constexpr typename result_of::template expand<CellBuilder>::type expand(CellBuilder&& cell_builder) const &
    { return { cell_builder.build(typename result_of::template expand<CellBuilder>::context_type::upstreams_tuple{ to_terminal_cell() }), to_terminal_cell() }; }

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template expand<CellBuilder>::type expand(CellBuilder&& cell_builder) &&
    { return { cell_builder.build(typename result_of::template expand<CellBuilder>::context_type::upstreams_tuple{ to_terminal_cell() }), std::move(to_terminal_cell()) }; }

    template<class CellBuilder, DFP_F_REQUIRES((is_cell_builder<CellBuilder>{} &&
        !DFP_CPP14_SUPPORT && std::is_copy_constructible<terminal_cell_type>{}
    ))>

    constexpr typename result_of::template expand<CellBuilder>::type expand(CellBuilder&& cell_builder) const
    { return { cell_builder.build(typename result_of::template expand<CellBuilder>::context_type::upstreams_tuple{ to_terminal_cell() }), to_terminal_cell() }; }

    template<class ProcessingGraph, DFP_F_REQUIRES((is_processing_graph<ProcessingGraph>{} &&
        (DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{})
        ))>
    constexpr typename result_of::template expand<ProcessingGraph>::type expand(ProcessingGraph&& graph) const &
    { return { std::get<0>(std::forward<ProcessingGraph>(graph).get_downmost_cells()), to_terminal_cell() }; }

    template<class ProcessingGraph, DFP_F_REQUIRES((is_processing_graph<ProcessingGraph>{} && (
        DFP_CPP14_SUPPORT || !std::is_copy_constructible<terminal_cell_type>{}
    )))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template expand<ProcessingGraph>::type expand(ProcessingGraph&& graph) &&
    { return { std::get<0>(std::forward<ProcessingGraph>(graph).get_downmost_cells()), std::move(to_terminal_cell()) }; }

    template<class ProcessingGraph, DFP_F_REQUIRES((is_processing_graph<ProcessingGraph>{} && (
        !DFP_CPP14_SUPPORT && std::is_copy_constructible<terminal_cell_type>{}
    )))>
    constexpr typename result_of::template expand<ProcessingGraph>::type expand(ProcessingGraph&& graph) const
    { return { std::get<0>(std::forward<ProcessingGraph>(graph).get_downmost_cells()), to_terminal_cell() }; }

    template<class T>
    constexpr auto collect(T&& t) const ->
    decltype(this->extend(std::forward<T>(t)))
    { return this->extend(std::forward<T>(t)); }
}; // class processing_graph<... is_upstream ...>


// Processing graphs terminated by sink cannot be extended.
// However they can be initialized and run.
template<class Sink, bool ImplicitRun>
class processing_tree
 :  public  std::remove_cvref_t<Sink>,
    public  processing_graph_base<ImplicitRun, std::tuple<Sink>, processing_tree<Sink, ImplicitRun>>
{
    typedef std::remove_cvref_t<Sink> sink_type;
    typedef processing_graph_base<ImplicitRun, std::tuple<Sink>, processing_tree> base;

    constexpr sink_type const& get_terminal_cell() const
    { return *static_cast<sink_type const* const>(this); }

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) sink_type& get_terminal_cell()
    { return *static_cast<sink_type* const>(this); }

public:
    typedef processing_tree_tag dfp_tag;

    typedef sink_type terminal_cell_type;
    typedef std::tuple<sink_type> terminal_cells_tuple;
    typedef typename base::downmost_cells_tuple downmost_cells_tuple;

    constexpr processing_tree(sink_type const& sink)
     :  sink_type(sink)
    {}

    constexpr processing_tree(sink_type&& sink)
     :  sink_type(std::move(sink))
    {}

    constexpr downmost_cells_tuple get_downmost_cells() const &
    { return { get_terminal_cell() }; }

    DFP_MUTABLE_CONSTEXPR downmost_cells_tuple get_downmost_cells() &&
    { return { std::move(get_terminal_cell()) }; }

    DFP_MUTABLE_CONSTEXPR terminal_cell_type&& get_sink() &&
    { return std::move(*this); }

    constexpr terminal_cell_type const& get_sink() const &
    { return *this; }
}; // class processing_tree


//TODO: likely to be removed
template<class TerminalCell>
class processing_graph<std::tuple<TerminalCell>>
 :  public processing_graph<TerminalCell>
{
public:
    using processing_graph<TerminalCell>::processing_graph;
};


template<class... TerminalCells>
class processing_graph<std::tuple<TerminalCells...>, DFP_C_REQUIRES((sizeof...(TerminalCells) > 1))>
 :  private std::tuple<std::remove_cvref_t<TerminalCells>...>,
// :  private std::tuple<TerminalCells...>,
    public  processing_graph_base<false, std::tuple<TerminalCells...>, processing_graph<std::tuple<TerminalCells...>>>
{
    typedef processing_graph_base<false, std::tuple<TerminalCells...>, processing_graph<std::tuple<TerminalCells...>>> base;
    typedef typename base::downmost_cells_tuple downmost_cells_tuple;
    typedef typename base::result_of result_of;

public:
    typedef std::tuple<std::remove_cvref_t<TerminalCells>...> terminal_cells_tuple;
//    typedef std::tuple<TerminalCells...> terminal_cells_tuple;

    constexpr terminal_cells_tuple const& get_downmost_cells() const &
    { return *this; }

    DFP_MUTABLE_CONSTEXPR terminal_cells_tuple&& get_downmost_cells() &&
    { return std::move(*this); }

    //TODO: this one looks weird. Is it really needed ?
//    constexpr terminal_cells_tuple&& get_downmost_cells() const &&
//    { return std::move(*this); }

private:
    static constexpr std::size_t LAST_BRANCH_ID = sizeof...(TerminalCells)-1;
    typedef std::tuple_element_t<0, terminal_cells_tuple> current_branch;

    static_assert(
        std::conjunction<std::is_move_constructible<std::remove_cvref_t<TerminalCells>>...>{},
        "The upstream processing branchs shall all be at least move-constructible"
    );

    DFP_STATIC_WARNING(
        std::conjunction<std::bool_constant<std::is_lvalue_reference<TerminalCells>{} && !std::is_copy_constructible<std::remove_cvref_t<TerminalCells>>{}>...>{},
        "One of the upstream branch is not copyable.\n Consequently this branch will be moved-construct during graph construction.\
 This choice is a bit hazardous because we don't know precisely what is your intent.\n\
 To remove this warning either make explicit the move-construct with use of std::move or enforce reference use with the cloneable sink"
    );

    template<class CellBuilder, std::size_t... Is>
    constexpr typename result_of::template extend<CellBuilder>::type _extend(CellBuilder&& cell_builder, std::index_sequence<0, Is...>) const &
    // TODO: make cell_builder dealing wih context => need for create_extend_context(this->get_downmost_cells())
    { return {
        cell_builder.build(typename result_of::template extend<CellBuilder>::context_type::upstreams_tuple{ std::get<0>(get_downmost_cells()) }),
        std::get<Is>(get_downmost_cells())...
    }; }

    template<class CellBuilder, std::size_t... Is>
    DFP_MUTABLE_CONSTEXPR typename result_of::template extend<CellBuilder>::type _extend(CellBuilder&& cell_builder, std::index_sequence<0, Is...>) &&
    // TODO: create_expand_context
    { return {
        cell_builder.build(typename result_of::template extend<CellBuilder>::context_type::upstreams_tuple{ std::get<0>(std::move(*this).get_downmost_cells()) }),
        std::get<Is>(std::move(*this).get_downmost_cells())...
    }; }

    template<class CellBuilder, std::size_t... Is, DFP_F_REQUIRES((
        is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template expand<CellBuilder>::type _expand(CellBuilder&& cell_builder, std::index_sequence<Is...>) const &
    { return { cell_builder.build(get_downmost_cells()), std::get<Is>(get_downmost_cells())... }; }

    template<class CellBuilder, std::size_t... Is, DFP_F_REQUIRES((
        is_cell_builder<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template expand<CellBuilder>::type _expand(CellBuilder&& graph, std::index_sequence<Is...>) &&
    //TODO: BUG : won't compile with non-copyable cell
    { return { graph.build(get_downmost_cells()), std::get<Is>(std::move(*this).get_downmost_cells())... }; }


    template<class CellBuilder, std::size_t... Is, DFP_F_REQUIRES((
        is_cell_builder<CellBuilder>{} &&
        (!DFP_CPP14_SUPPORT && std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template expand<CellBuilder>::type _expand(CellBuilder&& cell_builder, std::index_sequence<Is...>) const
    { return { cell_builder.build(get_downmost_cells()), std::get<Is>(get_downmost_cells())... }; }


    //TODO: do we want to genereralize support processing graph as input for all operator ?
    template<class ProcessingGraph, std::size_t... Is, DFP_F_REQUIRES((
        is_processing_graph<ProcessingGraph>{}
    ))>
    constexpr typename result_of::template expand<ProcessingGraph>::type _expand(ProcessingGraph&& graph, std::index_sequence<Is...>) const &
    // get<0> is just because I'm lazy, all terminal cells should be provided to create the new processing_graph
    { return { std::get<0>(graph.get_downmost_cells()), std::get<Is>(get_downmost_cells())... }; }

    template<class ProcessingGraph, std::size_t... Is, DFP_F_REQUIRES((
        is_processing_graph<ProcessingGraph>{}
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template expand<ProcessingGraph>::type _expand(ProcessingGraph&& graph, std::index_sequence<Is...>) &&
    { return { std::get<0>(graph.get_downmost_cells()), std::get<Is>(std::move(*this).get_downmost_cells())... }; }

    template<class CellBuilder, std::size_t... Is>
    constexpr typename result_of::template collect<CellBuilder>::type _collect(CellBuilder&& cell_builder, std::index_sequence<Is...>) const &
    { return { cell_builder.build(typename result_of::template collect<CellBuilder>::context_type::upstreams_tuple{ std::get<LAST_BRANCH_ID-Is>(get_downmost_cells())... }) }; }

    template<class CellBuilder, std::size_t... Is>
    DFP_MUTABLE_CONSTEXPR typename result_of::template collect<CellBuilder>::type _collect(CellBuilder&& cell_builder, std::index_sequence<Is...>) &&
    { return { cell_builder.build(typename result_of::template collect<CellBuilder>::context_type::upstreams_tuple{ std::get<LAST_BRANCH_ID-Is>(std::move(*this).get_downmost_cells())... }) }; }

public:
    typedef current_branch terminal_cell_type;

    template<class... TerminalCellArgs>
    constexpr processing_graph(TerminalCellArgs&&... terminal_cells)
     :  terminal_cells_tuple(std::forward<TerminalCellArgs>(terminal_cells)...) {}

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template extend<T>::type extend(T&& t) const &
    { return this->_extend(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template extend<T>::type extend(T&& t) &&
    { return std::move(*this)._extend(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (!DFP_CPP14_SUPPORT && std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template extend<T>::type extend(T&& t) const
    { return this->_extend(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template expand<T>::type expand(T&& t) const &
    { return this->_expand(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template expand<T>::type expand(T&& t) &&
    { return std::move(*this)._expand(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    template<class T, DFP_F_REQUIRES((
        always_true<T>{} &&
        (!DFP_CPP14_SUPPORT && std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template expand<T>::type expand(T&& t) const
    { return this->_expand(std::forward<T>(t), std::index_sequence_for<TerminalCells...>{}); }

    // NOTE C++11 workaround below for constexpr support.
    // Move-construction of the resulting graph is enabled only if some of the TerminalCells are not copy-constructible
    // or compiler has at least C++14 support
    // Otherwise copy-construction is preferred (to enable constexpr support) 
    //
    // ---
    template<class CellBuilder, DFP_F_REQUIRES((
        always_true<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template collect<CellBuilder>::type collect(CellBuilder&& cell_builder) const &
    { return this->_collect(std::forward<CellBuilder>(cell_builder), std::index_sequence_for<TerminalCells...>{}); }

    template<class CellBuilder, DFP_F_REQUIRES((
        always_true<CellBuilder>{} &&
        (DFP_CPP14_SUPPORT || !std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    DFP_MUTABLE_CONSTEXPR typename result_of::template collect<CellBuilder>::type collect(CellBuilder&& cell_builder) &&
    { return std::move(*this)._collect(std::forward<CellBuilder>(cell_builder), std::index_sequence_for<TerminalCells...>{}); }

    template<class CellBuilder, DFP_F_REQUIRES((
        always_true<CellBuilder>{} &&
        (!DFP_CPP14_SUPPORT && std::conjunction<std::is_copy_constructible<std::remove_reference_t<TerminalCells>>...>{})
    ))>
    constexpr typename result_of::template collect<CellBuilder>::type collect(CellBuilder&& cell_builder) const
    { return this->_collect(std::forward<CellBuilder>(cell_builder), std::index_sequence_for<TerminalCells...>{}); }
    // ---
}; // class processing_graph


} // namespace detail
} // namespace core
} // namespace dfp


namespace std
{


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmismatched-tags"
#endif


template<std::size_t I, class TerminalCells>
struct tuple_element<I, dfp::core::detail::processing_graph<TerminalCells>>
{ typedef tuple_element_t<I, typename dfp::core::detail::processing_graph<TerminalCells>::downmost_cells_tuple> type; };


#ifdef __clang__
#pragma clang diagnostic pop
#endif


} // namespace std


#endif // DFPTL_CORE_PROCESSING_GRAPH_HPP
