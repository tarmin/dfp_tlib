/**
* @file core/detail/release_sample_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SAMPLE_RELEASER_SINK_HPP
#define DFPTL_CORE_DETAIL_SAMPLE_RELEASER_SINK_HPP


#include "dfptl/core/sink.hpp"


#define DFP_CORE_SAMPLE_RELEASER core::sample_releaser


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class Input, class Derived, class = void>
class sample_releaser_sink_base
{ static_assert(always_false<Input>{}, DFP_INVALID_INPUT_COUNT(DFP_CORE_SAMPLE_RELEASER, 1)); };


template<class Input>
struct sample_releaser_sink : sample_releaser_sink_base<Input, sample_releaser_sink<Input>>
{ using sample_releaser_sink_base<Input, sample_releaser_sink<Input>>::sample_releaser_sink_base; };

template<class Input, class Derived>
class sample_releaser_sink_base<Input, Derived, DFP_C_REQUIRES((std::tuple_size<Input>{} == 1))>
 :  public  core::sink<Input, run_once, Derived>
{
    typedef core::sink<Input, run_once, Derived> sink_type;

public:
    typedef std::remove_const_t<input_sample_t<sink_type>> sample_type;

    //TODO: investigate constexpr in C++11 support
    // (will likely need to change run_once into run_never and directly access the input port)
    template<class T>
    constexpr sample_releaser_sink_base(T&& context) noexcept
     :  sink_type(std::forward<T>(context)) {}

    DFP_CONSTEXPR_VOID release_sample(input_sample_t<sink_type> const& in)
    { current_ = in; }

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) input_sample_t<sink_type> const& run()
    { sink_type::run(); return current_; }

protected:
    sample_type current_ {};
}; // class sample_releaser_sink_base



} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_SAMPLE_RELEASER_SINK_HPP
