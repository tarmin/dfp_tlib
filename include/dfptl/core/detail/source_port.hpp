/**
* @file core/detail/source_port.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SOURCE_PORT_HPP
#define DFPTL_CORE_DETAIL_SOURCE_PORT_HPP


#include "dfptl/core/frame_ref_wrapper.hpp"
#include "dfptl/core/detail/output_port.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
///@addtogroup source
///@{
///@addtogroup core_detail
///@{
/** @defgroup source_port Definition of @ref output_port "output port" specialized for the @ref source processing cells.
* @{
**/


/**
* @brief Provides @ref output_port implementation for @ref source
*
* @tparam Source type of the inheriting @ref source class
* @tparam StreamSpecifier type of the @ref stream_specifier for creation of the output stream
*
* @note this class is not nested in source class to allow source from inheriting from it
* (inheritance is required for EBO idiom)
**/
template<class Source, class StreamSpecifier, class Emitter>
class source_output_port
 :  public  output_port<StreamSpecifier, Source, Emitter, source_output_port<Source, StreamSpecifier, Emitter>>
{
    typedef output_port<StreamSpecifier, Source, Emitter, source_output_port> output_port_type;
    typedef Source source;

    friend source;

public:
    typedef typename output_port_type::stream_specifier_type stream_specifier_type;

    using output_port_type::output_port_type;


    /**
    * @brief Provides a default implementation for renew_sample() functions required by the output_port base.
    *
    * Invokes the acquire_sample() of the Derived class to flow downstream a single sample.
    **/
    //TODO: make renew_sample private
    template<class T = source>
    decltype(std::declval<T&>().acquire_sample())
    DFP_MUTABLE_CONSTEXPR renew_sample(source& owner)
    { return owner.acquire_sample(); }

    template<class T = source>
    decltype(std::declval<T const&>().acquire_sample())
    constexpr renew_sample(source const& owner) const
    { return owner.acquire_sample(); }

    /**
    * @brief Provides a default implementation for renew_frame() function required by the output_port base.
    *
    * If available, it invokes the acquire_frame() of the Derived class to flow downstream a sample frame
    * or if the source is sample-based it will provide some default implementation based on acquire_sample().
    **/
    //TODO: make renew_frame private
    template<class T = source>
    decltype(std::declval<T&>().acquire_frame())
    DFP_MUTABLE_CONSTEXPR renew_frame(T& owner)
    { return owner.acquire_frame(); }

    template<class T = source>
    decltype(std::declval<T const&>().acquire_frame())
    constexpr renew_frame(T const& owner) const
    { return owner.acquire_frame(); }

    template<class T = source, class... O, DFP_F_REQUIRES((
        !supports_const_acquire_frame<T>{} && !supports_const_acquire_sample<T>{} && !supports_const_acquire_stream<T>{}
    ))>
    port_frame_t<output_port_type>
    constexpr renew_frame(source const&, O...) const
    {
        static_assert(
            !supports_acquire_frame<Source>{} || supports_const_acquire_frame<T>{},
            "Source is involved in a const expression but it does not implement const acquire_frame() method"
        );

        static_assert(
            !supports_acquire_sample<Source>{} || supports_const_acquire_sample<T>{},
            "Source is involved in a const expression but it does not implement const acquire_sample() method"
        );

        static_assert(
            !supports_acquire_stream<Source>{} || supports_const_acquire_stream<T>{},
            "Source is involved in a const expression but it does not implement const acquire_stream() method"
        );

        return std::declval<port_frame_t<source_output_port>>();
    }

    template<class T = source>
    decltype(std::declval<T&>().acquire_stream())
    DFP_MUTABLE_CONSTEXPR renew_stream(T& owner)
    { return owner.acquire_stream(); }

    template<class T = source, DFP_F_REQUIRES((!supports_const_acquire_stream<T>{} && !supports_acquire_stream<T>{}))>
    core::stream<stream_specifier_type, source>
    DFP_MUTABLE_CONSTEXPR renew_stream(T& owner)
    { return output_port_type::build_stream(owner); }

    template<class T = source>
    decltype(std::declval<T const&>().acquire_stream())
    constexpr renew_stream(T const& owner) const
    { return owner.acquire_stream(); }

    template<class T = source, DFP_F_REQUIRES((!supports_const_acquire_stream<T>{} && !supports_acquire_stream<T>{}))>
    typename core::stream<stream_specifier_type, source>::to_const::type
    constexpr renew_stream(T const& owner) const
    { return output_port_type::build_stream(owner); }

    template<class T = source, class... O, DFP_F_REQUIRES((supports_acquire_stream<T>{}))>
    port_stream_t<output_port_type>
    constexpr renew_stream(source const&, O...) const
    {
        static_assert(supports_const_acquire_stream<Source>{}, "Source is involved in a const expression but it does not implement const acquire_stream() method");
        return std::declval<port_stream_t<source_output_port>>();
    }

//TODO: not sure we really need "frame_ref" for in-place-processing
//    template<class T = source, DFP_F_REQUIRES((detail::source_produces_frame<T>{} && detail::source_applies_in_place_processing<T>{}))>
//    auto renew_frame(T& owner) ->
//    decltype(frame_ref(owner.acquire_frame()))
//    { return frame_ref(owner.acquire_frame()); }

//    template<class T = source, DFP_F_REQUIRES((detail::source_produces_frame<T>{} && detail::source_applies_in_place_processing<T>{}))>
//    frame_ref_wrapper<output_frame_t<T> const> constexpr    renew_frame(source const& owner) const
//    { return cframe_ref(owner.acquire_frame()); }

    template<class T = source, DFP_F_REQUIRES((supports_acquire_sample<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_frame_t<T>           renew_frame(source& owner)
    { return output_port_type::renew_frame(owner); }

    /**
     * @brief create a new frame
     * 
     * @tparam T source type
     * @param owner instance of source owning the output port
     * @return DFP_MUTABLE_CONSTEXPR output_frame_t<T>
     * 
     * @startuml
     * alt REQUIRES(supports_acquire_stream<source>{})
     *   [-> source_port : renew_frame(owner) 
     *   activate source_port
     *     source_port -> source_port : renew_stream(owner)
     *     activate source_port
     *     source <- source_port : acquire_stream()
     *     return stream
     *     source_port -> stream : drain()
     *   return frame
     * end
     * @enduml
     */
    template<class T = source, DFP_F_REQUIRES((supports_acquire_stream<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_frame_t<T>           renew_frame(source& owner)
    { return renew_stream(owner).drain(); }

    template<class T = source, DFP_F_REQUIRES((detail::supports_const_acquire_sample<T>{}))>
    constexpr output_frame_t<T> renew_frame(source const& owner) const
    { return output_port_type::renew_frame(owner); }

    template<class T = source, DFP_F_REQUIRES((detail::supports_const_acquire_stream<T>{}))>
    constexpr output_frame_t<T> renew_frame(source const& owner) const
    { return renew_stream(owner).cdrain(); }

    // helps to retrieve child instance in parent
    DFP_MUTABLE_CONSTEXPR source_output_port&   get()   { return *this; }
    constexpr source_output_port const&     get() const { return *this; }

    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID invoke_initialize(source& owner, InitializationStorageView initialization_storage)
    {
        owner.initialize_upwards(initialization_storage);
        // not really useful to have both initialize_upwards and initialize_downwards in source
        // but more consistent with node and sink
        owner.initialize_downwards(initialization_storage);
    }

    template<class T = source>
    cell_initialization_storage_t<T> invoke_build_initialization_storage(source const& owner) const
    { return owner.build_initialization_storage(owner); }
}; // class source_output_port


///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_SOURCE_PORT_HPP
