/**
* @file core/detail/single_sample_frame.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SINGLE_SAMPLE_FRAME_HPP
#define DFPTL_CORE_DETAIL_SINGLE_SAMPLE_FRAME_HPP


#include "dfptl/core/frame_concept.hpp"

#include "stl/type_traits.hpp"
#include <tuple>
#include <limits>


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief A sample_frame refers to a sequence of samples produced by a processing @c Cell.
*
* With help of the sample_frame a processing cell generating samples is lazy-evaluted in a processing branch.
* sample_frame can provides at least a
* [incremental](https://www.boost.org/doc/libs/1_70_0/libs/iterator/doc/new-iter-concepts.html#iterator-traversal-concepts-lib-iterator-traversal) iterator.
*
* @tparam Sample is the type or the reference type of the sample.
**/
template<class Sample>
class single_sample_frame : public frame_concept<single_sample_frame<Sample>>
{
    typedef std::remove_cvref_t<Sample>                                 value_type;
    typedef std::add_lvalue_reference_t<value_type>                     reference;
    typedef std::add_lvalue_reference_t<std::add_const_t<value_type>>   const_reference;
    typedef std::ptrdiff_t                                              difference_type;

public:
    typedef std::size_t                                                 size_type;

    class const_iterator
    {
    public:
        typedef typename single_sample_frame::value_type        value_type;
        typedef value_type*                                     pointer;
        typedef typename single_sample_frame::reference         reference;
        typedef typename single_sample_frame::const_reference   const_reference;
        typedef std::forward_iterator_tag                       iterator_category;
        typedef typename single_sample_frame::difference_type   difference_type;
        typedef typename single_sample_frame::size_type         size_type;

        constexpr explicit const_iterator(value_type const& sample) : sample_(&sample) {}

        constexpr const_iterator(const_iterator const& a) : sample_(a.sample_) {}

        constexpr value_type const& operator*() const
        { return *sample_; }

        constexpr pointer operator->() const
        { return sample_; }

        DFP_MUTABLE_CONSTEXPR const_iterator operator++() noexcept
        { sample_++ ; return *this; }

        DFP_MUTABLE_CONSTEXPR const_iterator operator++(int) noexcept
        { sample_++ ; return const_iterator(*(sample_-1)); }

        template<class T, DFP_F_REQUIRES((std::is_integral<T>{} || std::is_convertible<T, std::size_t>{}))>
        constexpr const_iterator operator+(T distance) const noexcept
        { return const_iterator(*(sample_ + distance)); }

        template<class T, DFP_F_REQUIRES((std::is_integral<T>{} || std::is_convertible<T, std::size_t>{}))>
        DFP_MUTABLE_CONSTEXPR const_iterator operator+=(T distance) noexcept
        { sample_ += distance; return *this; }

        constexpr bool operator==(const_iterator const& a) const noexcept
        { return sample_ == a.sample_; }

        constexpr bool operator!=(const_iterator const& a) const noexcept
        { return sample_ != a.sample_; }

        constexpr bool operator<(const_iterator const& a) const noexcept
        { return sample_ < a.sample_; }

        constexpr bool operator>(const_iterator const& a) const noexcept
        { return sample_ > a.sample_; }

        constexpr bool operator<=(const_iterator const& a) const noexcept
        { return sample_ <= a.sample_; }

        constexpr bool operator>=(const_iterator const& a) const noexcept
        { return sample_ >= a.sample_; }

        //TODO: add data() method
    private:
        value_type const* sample_;
    }; // class const_iterator

    class iterator
    {
    public:
        typedef typename single_sample_frame::value_type        value_type;
        typedef value_type*                                     pointer;
        typedef typename single_sample_frame::reference         reference;
        typedef typename single_sample_frame::const_reference   const_reference;
        typedef std::forward_iterator_tag                       iterator_category;
        typedef typename single_sample_frame::difference_type   difference_type;
        typedef typename single_sample_frame::size_type         size_type;

        constexpr explicit iterator(value_type& sample) noexcept : sample_(&sample) {}

        constexpr iterator(iterator const& a) noexcept : sample_(a.sample_) {}

        DFP_MUTABLE_CONSTEXPR reference operator*()
        { return *sample_; }

        DFP_MUTABLE_CONSTEXPR pointer operator->()
        { return sample_; }

        DFP_MUTABLE_CONSTEXPR iterator operator++() noexcept
        { sample_++ ; return *this; }

        DFP_MUTABLE_CONSTEXPR iterator operator++(int) noexcept
        { sample_++ ; return iterator(*(sample_-1)); }

        template<class T, DFP_F_REQUIRES((std::is_integral<T>{} || std::is_convertible<T, std::size_t>{}))>
        constexpr iterator operator+(T distance) const noexcept
        { return iterator(*(sample_ + distance)); }

        template<class T, DFP_F_REQUIRES((std::is_integral<T>{} || std::is_convertible<T, std::size_t>{}))>
        DFP_MUTABLE_CONSTEXPR iterator operator+=(T distance) noexcept
        { sample_ += distance; return *this; }

        constexpr bool operator==(iterator const& a) const noexcept
        { return sample_ == a.sample_; }

        constexpr bool operator!=(iterator const& a) const noexcept
        { return sample_ != a.sample_; }

        constexpr bool operator<(iterator const& a) const noexcept
        { return sample_ < a.sample_; }

        constexpr bool operator>(iterator const& a) const noexcept
        { return sample_ > a.sample_; }

        constexpr bool operator<=(iterator const& a) const noexcept
        { return sample_ <= a.sample_; }

        constexpr bool operator>=(iterator const& a) const noexcept
        { return sample_ >= a.sample_; }

        constexpr operator const_iterator() const noexcept
        { return const_iterator(*sample_); }

    private:
        value_type* sample_;
    }; // class iterator

    constexpr single_sample_frame() noexcept : sample_() {}

    constexpr single_sample_frame(value_type sample) : sample_(sample) {}

    constexpr single_sample_frame(std::array<value_type, 1> const& array) : sample_(array[0]) {}

    constexpr single_sample_frame(std::array<value_type const, 1> const& array) : sample_(array[0]) {}

    constexpr single_sample_frame(std::initializer_list<value_type> list) : sample_(*list.begin()) {}

    DFP_MUTABLE_CONSTEXPR iterator begin()
    { return iterator(sample_); }

    constexpr const_iterator begin() const
    { return const_iterator(sample_); }

    DFP_MUTABLE_CONSTEXPR iterator end()
    { return iterator(*(&sample_+1)); }

    constexpr const_iterator end() const
    { return const_iterator(*(&sample_+1)); }

    constexpr const_iterator cbegin() const
    { return const_iterator(sample_); }

    constexpr const_iterator cend() const
    { return const_iterator(*(&sample_+1)); }

    constexpr size_type size() const
    { return 1; }

    constexpr bool empty() const
    { return false; }

    constexpr operator Sample const&() const
    { return sample_; }

    constexpr operator std::array<Sample, 1>() const
    { return std::array<Sample, 1>{{ sample_ }}; }

private:
    value_type sample_;
};


} // namespace detail
} // namespace core
} // namespace dfp


namespace std
{


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmismatched-tags"
#endif
/**
* @brief partial specialization for dfptl::framework::detail::sample_frame with constant size
**/
template<class Sample>
struct tuple_size<dfp::detail::single_sample_frame<Sample>>
{
    static constexpr size_t value = 1;
}; // struct tuple_size<... sample_frame ...>
#ifdef __clang__
#pragma clang diagnostic pop
#endif

} // namespace std


#endif // DFPTL_CORE_DETAIL_SINGLE_SAMPLE_FRAME_HPP
