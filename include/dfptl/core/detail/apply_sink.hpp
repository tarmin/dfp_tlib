/**
* @file core/detail/apply_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_APPLY_SINK_HPP
#define DFPTL_CORE_DETAIL_APPLY_SINK_HPP


#include "dfptl/core/sink.hpp"
#include "dfptl/core/stream_specifier.hpp"
#include "dfptl/core/detail/ebco_member.hpp"

#include "stl/utility.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class Inputs, class InputIndexSequence, class Callable, template<class> class Runner, class Derived, class = void> struct apply_sink_impl
{ static_assert(always_false<apply_sink_impl>{}, DFP_UNEXPECTED_ERROR); };

/**
* @brief Sample-based sink which applies a function-object (as defined by STL) for each input stream sample.
*
* @tparam Inputs is the type list of the processing cells (source or node) bound to the input ports to this sink.
* @tparam Callable is a std::function to apply on each sample.
* @tparam Runner is the type runner scheduling execution of the sink
**/
template<class Inputs, class Callable, template<class> class Runner, class Derived>
struct apply_sink : apply_sink_impl<Inputs, index_sequence_from_t<Inputs>, Callable, Runner, Derived>
{ using apply_sink_impl<Inputs, index_sequence_from_t<Inputs>, Callable, Runner, Derived>::apply_sink_impl; };


//TODO: check that RunnerArgs is tuple
template<class Inputs, class InputIndexSequence, class Callable, template<class> class Runner, class Derived, class RunnerArgs = runner_args_t<Runner>> class apply_regular_sink
{ static_assert(always_false<apply_regular_sink>{}, "Inputs template argument shall model tuple concept"); };


//TODO: check that RunnerArgs is tuple
template<class Inputs, class InputIndexSequence, class Callable, template<class> class Runner, class Derived, class RunnerArgs = runner_args_t<Runner>> class apply_unregular_sink
{ static_assert(always_false<apply_unregular_sink>{}, "Inputs template argument shall model tuple concept"); };


template<class Inputs, std::size_t... InputIds, class Callable, template<class> class Runner, class Derived>
struct apply_sink_impl<
    Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived,
    DFP_C_REQUIRES((supports_const_call_operator<Callable, output_sample_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>
    :   apply_regular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived>
{ using apply_regular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived>::apply_regular_sink; };


template<class Inputs, std::size_t... InputIds, class Callable, template<class> class Runner, class Derived>
struct apply_sink_impl<
    Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived,
    DFP_C_REQUIRES((supports_mutable_call_operator<Callable, output_sample_t<std::tuple_element_t<InputIds, Inputs>>...>{}))
>   :   apply_unregular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived>
{ using apply_unregular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived>::apply_unregular_sink; };


template<class Inputs, std::size_t... InputIds, class Callable, template<class> class Runner, class Derived, class... RunnerArgs>
class apply_regular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived, std::tuple<RunnerArgs...>>
 :  public  core::sink<Inputs, Runner, Derived>,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable>   callable_member;
    typedef core::sink<Inputs, Runner, Derived> sink_type;

public:
    using sink_type::run;

    /**
    * @brief Creates a new instance of apply_regular_sink.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr apply_regular_sink(Inputs&& inputs, RunnerArgs... runner_args, CallableArgs&&... callable_args)
        :   sink_type{std::forward<Inputs>(inputs), runner_args...}, callable_member(std::forward<CallableArgs>(callable_args)...) {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    void release_sample(input_sample_t<sink_type, InputIds> const&... inputs) const
    { callable_member::get()(inputs...); }
}; // class apply_regular_sink


template<class Inputs, std::size_t... InputIds, class Callable, template<class> class Runner, class Derived, class... RunnerArgs>
class apply_unregular_sink<Inputs, std::index_sequence<InputIds...>, Callable, Runner, Derived, std::tuple<RunnerArgs...>>
 :  public  core::sink<Inputs, Runner, Derived>,
    private ebco_member<Callable>
{
    typedef ebco_member<Callable> callable_member;
    typedef core::sink<Inputs, Runner, Derived>    sink_type;

public:
    using sink_type::run;

    /**
    * @brief Creates a new instance of apply_impure_function_node.
    *
    * @param inputs is the list of processing cells bound to the input ports of this node.
    * @param callable_args is parameters pack passed to function-object construtor.
    **/
    template<class... CallableArgs>
    constexpr apply_unregular_sink(Inputs&& inputs, RunnerArgs... runner_args, CallableArgs&&... callable_args)
     :  sink_type{std::forward<Inputs>(inputs), runner_args...}, callable_member(std::forward<CallableArgs>(callable_args)...)
    {}

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    *
    * @param inputs are the samples on which to apply the operation.
    */
    void release_sample(input_sample_t<sink_type, InputIds> const&... inputs)
    { callable_member::get()(inputs...); }
}; // class apply_unregular_sink


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_APPLY_SINK_HPP
