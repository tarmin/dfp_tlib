/**
* @file core/detail/pluggable_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PLUGGABLE_SINK_HPP
#define DFPTL_CORE_DETAIL_PLUGGABLE_SINK_HPP


#include "dfptl/core/sink.hpp"
#include "dfptl/core/upstream_cell.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template <class> class sample_stream_source;


#define DFP_PLUGGABLE_SINK core::pluggable


template<class Input>
class pluggable_sink
 :  public core::sink<Input, run_once, pluggable_sink<Input>>,
    public core::iupstream_cell<typename output_stream_t<std::tuple_element_t<0, Input>>::specifier_type>
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_CLONEABLE_SINK, 1));

    typedef core::sink<Input, run_once, pluggable_sink<Input>> sink_type;
    friend class sample_stream_source<input_sample_t<sink_type>>;

public:
    using sink_type::sink_type;

    void release_stream(input_stream_t<sink_type>&& in)
    { current_frame_ = in.drain(); }

private:
    input_frame_t<sink_type> const& drain() noexcept
    { run(); return current_frame_; }

    void run()
    { sink_type::run(); }

    // for performance consideration 
    // current_frame_ type is adapted to the input drain() result which can input_frame_t value ref or input_frame_t value
    input_frame_t<sink_type> current_frame_;
}; // class pluggable_sink


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PLUGGABLE_SINK_HPP
