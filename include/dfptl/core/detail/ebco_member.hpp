/**
* @file core/detail/ebco_member.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_EBCO_MEMBER_HPP
#define DFPTL_CORE_DETAIL_EBCO_MEMBER_HPP


#include "dfptl/core/cplusplus.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{

/**
* @brief Out-of-class definition of @c object member.
*
* @note Implements EBCO idiom with Object class for purpose of memory consumption optimization
*
* @tparam Object is the type of the object wrapped by the ebco_member
**/
template<class Object, bool EnableOptimization = true>
struct ebco_member : Object
{
    template<class... Args>
    constexpr ebco_member(Args&&...   args) noexcept
        :   Object(std::forward<Args>(args)...) {}

    constexpr Object const&         get() const { return *this; }
    DFP_MUTABLE_CONSTEXPR Object&   get()       { return *this; }
}; // class ebco_member<EnableOptimization == true>


template<class Object>
struct ebco_member<Object, false>
{
    template<class... Args>
    constexpr ebco_member(Args&&...   args) noexcept
        :   object_(std::forward<Args>(args)...) {}

    constexpr Object const&         get() const { return object_; }
    DFP_MUTABLE_CONSTEXPR Object&   get()       { return object_; }

private:
    Object object_;
}; // class ebco_member<EnableOptimization == false>

} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_EBCO_MEMBER_HPP
