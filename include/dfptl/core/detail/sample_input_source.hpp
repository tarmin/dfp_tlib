/**
* @file core/detail/sample_input_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SAMPLE_INPUT_SOURCE_HPP
#define DFPTL_CORE_DETAIL_SAMPLE_INPUT_SOURCE_HPP


#include "dfptl/core/source.hpp"

#include <boost/fusion/include/pair.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{


#define DFP_CORE_SAMPLE_INPUT core::sample_input


struct sample_input_key_tag {};

template<class Identifier>
struct sample_input_key
{
    typedef sample_input_key_tag dfp_tag;
    typedef Identifier identifier_type;
};


template<class T, class = void>
struct is_sample_input_key_impl : std::false_type
{};

template<class T>
struct is_sample_input_key_impl<T, DFP_C_REQUIRES((std::is_same<sample_input_key_tag, typename T::dfp_tag>{}))>
 :  std::true_type
{};

template<class T>
struct is_sample_input_key : is_sample_input_key_impl<T>
{};

template<class T>
using is_sample_input_key_t = typename is_sample_input_key<T>::type;



template<class Context, class Identifier, class Sample>
class sample_input_source
 :  public  core::source<
        Context,
        atomic_stream_specifier<Sample>,
        sample_input_source<Context, Identifier, Sample>,
        boost::fusion::pair<sample_input_key<Identifier>, sample_input_source<Context, Identifier, Sample>*>
    >
{
    typedef core::source<
        Context,
        atomic_stream_specifier<Sample>,
        sample_input_source,
        boost::fusion::pair<sample_input_key<Identifier>, sample_input_source*>
    > source_type;

    typedef Identifier identifier_type;

public:
    using source_type::source_type;

    template<class InitStorageView>
    void initialize_upwards(InitStorageView init_storage)
    { boost::fusion::at_key<sample_input_key<Identifier>>(init_storage) = this; }

    output_sample_t<source_type> const& acquire_sample() const
    { return *upstream_cell_->drain().cbegin(); }

    void plug(iupstream_cell<atomic_stream_specifier<Sample>>& upstream)
    { this->upstream_cell_ = &upstream; }

    iupstream_cell<atomic_stream_specifier<Sample>>* upstream_cell_;
}; // class sample_input_source


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_SAMPLE_INPUT_SOURCE_HPP
