/**
* @file core/detail/apply_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_APPLY_SOURCE_HPP
#define DFPTL_CORE_DETAIL_APPLY_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/core/stream_specifier.hpp"
#include "dfptl/core/processing_descriptor.hpp"
#include "dfptl/core/detail/ebco_member.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
/**
* @brief Helper class to infer a stream specifier given the processing @c Descriptor
**/
template<class Descriptor, class = void>
struct apply_source_stream
{ static_assert(always_false<apply_source_stream>{}, DFP_UNEXPECTED_ERROR); };


template<class Callable>
struct apply_source_stream<sample_based_processing<Callable>>
{ typedef atomic_stream_specifier<std::result_of_t<Callable()>> type; };


template<class Callable>
struct apply_source_stream<frame_based_processing<Callable>, DFP_C_REQUIRES((
    (frame_traits<std::result_of_t<Callable()>>::EXTENT != DYNAMIC_EXTENT) &&
    always_true<typename frame_traits<std::result_of_t<Callable()>>::sample_type>{}
))>
{ typedef static_stream_specifier<typename frame_traits<std::result_of_t<Callable()>>::sample_type, frame_traits<std::result_of_t<Callable()>>::EXTENT> type; };


// will likely need some rework when supporting variable-flow stream
template<class Callable>
struct apply_source_stream<frame_based_processing<Callable>, DFP_C_REQUIRES((
    (frame_traits<std::result_of_t<Callable()>>::EXTENT == DYNAMIC_EXTENT) &&
    always_true<typename frame_traits<std::result_of_t<Callable()>>::sample_type>{}
))>
{ typedef dynamic_stream_specifier<typename frame_traits<std::result_of_t<Callable()>>::sample_type> type; };


template<class Context, class Descriptor, class = void> struct apply_source
{ static_assert(always_false<apply_source>{}, DFP_UNEXPECTED_ERROR); };


/**
* @brief Sample-based source which applies a binary function-object (as defined by STL) for each input stream sample.
*
* @tparam Descriptor is a std::function to apply on each sample.
**/
//TODO: result_of_t<> to be turned into invoke_result<>
template<class Context, class Descriptor>
class apply_regular_source
{ static_assert(always_false<apply_regular_source>{}, DFP_UNEXPECTED_ERROR); };


template<class Context, class Descriptor>
class apply_unregular_source
{ static_assert(always_false<apply_unregular_source>{}, DFP_UNEXPECTED_ERROR); };


template<class Context, class Descriptor, class Optimization = default_apply_source_optimization>
struct apply_source_base
 :  private ebco_member<typename Descriptor::invocable, !apply_source_constraint::discard_ebo && Optimization::enable_ebo>
{

    friend class apply_regular_source<Context, Descriptor>;
    friend class apply_unregular_source<Context, Descriptor>;
    typedef typename Descriptor::invocable                                                                                  invocable;
    typedef typename apply_source_stream<Descriptor>::type                                                                  stream_specifier_type;
    typedef ebco_member<typename Descriptor::invocable, !apply_source_constraint::discard_ebo && Optimization::enable_ebo>  callable_member;

    static_assert(
        stream_specifier_type::IS_FRAME_LENGTH_STATIC || supports_output_frame_length<invocable>{},
        "provided Callable produces frame with dynamic extent but does not expose an output_frame_length() method"
    );

public:
    template<class... CallableArgs>
    constexpr apply_source_base(CallableArgs&&... callable_args) : callable_member(std::forward<CallableArgs>(callable_args)...)
    {}

protected:
    DFP_MUTABLE_CONSTEXPR invocable&  callable()    { return callable_member::get(); }
    constexpr invocable const&    callable() const  { return callable_member::get(); }
}; // class apply_source_base


template<class Context, class Descriptor>
struct apply_source<
    Context, Descriptor, DFP_C_REQUIRES((supports_const_call_operator<typename Descriptor::invocable>{}))
>   :   apply_regular_source<Context, Descriptor>
{ using apply_regular_source<Context, Descriptor>::apply_regular_source; };


template<class Context, class Descriptor>
struct apply_source<
    Context, Descriptor, DFP_C_REQUIRES((supports_mutable_call_operator<typename Descriptor::invocable>{}))
>   :   apply_unregular_source<Context, Descriptor>
{ using apply_unregular_source<Context, Descriptor>::apply_unregular_source; };


template<class Context, class Callable>
class apply_regular_source<Context, sample_based_processing<Callable>>
 :  public  apply_source_base<Context, sample_based_processing<Callable>>,
    public  core::source<Context, typename apply_source_stream<sample_based_processing<Callable>>::type, apply_regular_source<Context, sample_based_processing<Callable>>>
{
    typedef apply_source_base<Context, sample_based_processing<Callable>> base;
    typedef core::source<Context, typename apply_source_stream<sample_based_processing<Callable>>::type, apply_regular_source> source_type;

public:
    /**
    * @brief Creates a new instance of apply_function_source_impl.
    *
    * @param callable_args is parameters pack of arguments following the sample value for @c Callable invocation.
    **/
    template<class... CallableArgs>
    constexpr apply_regular_source(Context&& context, CallableArgs&&... callable_args)
     :  base(std::forward<CallableArgs>(callable_args)...),
        source_type(std::forward<Context>(context))
    {}

    template<class T = Callable, DFP_F_REQUIRES((supports_initialize<T>{}))>
    void initialize()
    { base::callable().initialize(); }

    template<class T = Callable, DFP_F_REQUIRES((!supports_initialize<T>{}))>
    void initialize()
    { source_type::initialize(); }

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    */
    // may take leverage of RVO
    constexpr output_sample_t<source_type> acquire_sample() const
    { return base::callable()(); }
}; // class apply_regular_source<... sample_based_processing ...>


template<class Context, class Callable>
class apply_regular_source<Context, frame_based_processing<Callable>>
 :  public  apply_source_base<Context, frame_based_processing<Callable>>,
    public  core::source<Context, typename apply_source_stream<frame_based_processing<Callable>>::type, apply_regular_source<Context, frame_based_processing<Callable>>>
{
    typedef apply_source_base<Context, frame_based_processing<Callable>> base;
    typedef core::source<Context, typename apply_source_stream<frame_based_processing<Callable>>::type, apply_regular_source> source_type;

public:
    typedef typename source_type::output_specifier_type output_specifier_type;

    template<class T = output_specifier_type, class... CallableArgs,
        DFP_F_REQUIRES((T::IS_STATIC))
    >
    constexpr apply_regular_source(Context&& context, CallableArgs&&... callable_args)
     :  base(std::forward<CallableArgs>(callable_args)...),
        source_type(std::forward<Context>(context), output_specifier_type{})
    {}

    template<class T = Callable, class... CallableArgs,
        DFP_F_REQUIRES((supports_output_frame_length<T>{}))
    >
    constexpr apply_regular_source(Context&& context, CallableArgs&&... callable_args)
     :  base(std::forward<CallableArgs>(callable_args)...),
        source_type(std::forward<Context>(context), output_specifier_type{}.set_frame_length(base::callable().output_frame_length()))
    {}

    template<class T = Callable, DFP_F_REQUIRES((supports_initialize<T>{}))>
    void initialize()
    { base::callable().initialize(); }

    template<class T = Callable, DFP_F_REQUIRES((!supports_initialize<T>{}))>
    void initialize()
    { source_type::initialize(); }

    /**
    * @brief Applies the frame-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    */
    constexpr output_frame_t<source_type> acquire_frame() const
    { return base::callable()(); }
}; // class apply_regular_source<... frame_based_processing ...>


template<class Context, class Callable>
class apply_unregular_source<Context, sample_based_processing<Callable>>
 :  public  apply_source_base<Context, sample_based_processing<Callable>>,
    public  core::source<Context, typename apply_source_stream<sample_based_processing<Callable>>::type, apply_unregular_source<Context, sample_based_processing<Callable>>>
{
    typedef apply_source_base<Context, sample_based_processing<Callable>> base;
    typedef core::source<Context, typename apply_source_stream<sample_based_processing<Callable>>::type, apply_unregular_source> source_type;

public:
    typedef typename source_type::output_specifier_type output_specifier_type;

    template<class... FunctionArgs>
    constexpr apply_unregular_source(Context&& context, FunctionArgs&&... function_args)
     :  base(std::forward<FunctionArgs>(function_args)...),
        source_type(std::forward<Context>(context))
    {}

    template<class T = Callable, DFP_F_REQUIRES((supports_initialize<T>{}))>
    void initialize()
    { base::callable().initialize(); }

    template<class T = Callable, DFP_F_REQUIRES((!supports_initialize<T>{}))>
    void initialize()
    { source_type::initialize(); }

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    */
    DFP_MUTABLE_CONSTEXPR output_sample_t<source_type> acquire_sample()
    { return base::callable()(); }
}; // class apply_unregular_source< ... sample_based_processing ...>


template<class Context, class Callable>
class apply_unregular_source<Context, frame_based_processing<Callable>>
 :  public  apply_source_base<Context, frame_based_processing<Callable>>,
    public  core::source<Context, typename apply_source_stream<frame_based_processing<Callable>>::type, apply_unregular_source<Context, frame_based_processing<Callable>>>
{
    typedef apply_source_base<Context, frame_based_processing<Callable>> base;
    typedef core::source<Context, typename apply_source_stream<frame_based_processing<Callable>>::type, apply_unregular_source> source_type;

public:
    typedef typename source_type::output_specifier_type output_specifier_type;

    template<class... FunctionArgs, class T = output_specifier_type, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr apply_unregular_source(Context&& context, FunctionArgs&&... function_args)
     :  base(std::forward<FunctionArgs>(function_args)...),
        source_type(std::forward<Context>(context))
    {}

    template<class... FunctionArgs, class T = output_specifier_type, DFP_F_REQUIRES((!T::IS_STATIC && supports_output_frame_length<Callable>{}))>
    constexpr apply_unregular_source(Context&& context, FunctionArgs&&... function_args)
     :  base(std::forward<FunctionArgs>(function_args)...),
        source_type(std::forward<Context>(context), output_specifier_type{}.set_frame_length(base::callable().output_frame_length()))
    { static_assert(
        T::frame_length_property_type::IS_FRAME_LENGTH_STATIC || supports_output_frame_length<Callable>{},
        "provided Callable produces frame with dynamic extent but does not expose an output_frame_length() method"
    ); }

    template<class T = Callable, DFP_F_REQUIRES((supports_initialize<T>{}))>
    void initialize()
    { base::callable().initialize(); }

    template<class T = Callable, DFP_F_REQUIRES((!supports_initialize<T>{}))>
    void initialize()
    { source_type::initialize(); }

    /**
    * @brief Applies the element-wise function-object with the given sample of the input stream as the left operand
    * and the data given at instance creation as the right operand.
    */
    DFP_MUTABLE_CONSTEXPR output_frame_t<source_type> acquire_frame()
    { return base::callable()(); }
}; // class apply_unregular_source< ... frame_based_processing ...>


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_APPLY_SOURCE_HPP
