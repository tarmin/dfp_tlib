/**
* @file core/detail/node_port.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_NODE_PORT_HPP
#define DFPTL_CORE_DETAIL_NODE_PORT_HPP


#include "dfptl/core/detail/output_port.hpp"
#include "dfptl/core/detail/input_port.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core_grp
///@{
///@addtogroup node
///@{
///@addtogroup core_detail
///@{
/** @defgroup node_port Definition of @ref input_port "input port" and @ref output_port "output port"
* specialized for the @ref node processing cells.
* @{
**/


/**
* @brief Provides @ref output_port implementation for @ref node
*
* @tparam Node type of the inheriting @ref node class
* @tparam StreamSpecifier type of the @ref stream_specifier for creation of the output stream
*
* @note this class is not nested in node class to allow node from inheriting from it
* (inheritance is required for EBO idiom)
**/
template<class Node, class StreamSpecifier, class Emitter = Node>
class node_output_port
 :  public  output_port<StreamSpecifier, Node, Emitter, node_output_port<Node, StreamSpecifier, Emitter>>
{
    typedef output_port<StreamSpecifier, Node, Emitter, node_output_port> output_port_type;
    typedef Node node;

    friend node;

    template<class T, class InputIndexSequence, class = void>
    struct get_forwarded_input_impl;

    template<class T, std::size_t CheckedIndex, std::size_t... InputIndexes>
    struct get_forwarded_input_impl<T, std::index_sequence<CheckedIndex, InputIndexes...>, DFP_C_REQUIRES((
        supports_forward_sample<T, CheckedIndex>{} || supports_const_forward_sample<T, CheckedIndex>{}
        ||
        supports_forward_frame<T, CheckedIndex>{} || supports_const_forward_frame<T, CheckedIndex>{}
    ))
    > : size_t_const<CheckedIndex> {};

    template<class T, std::size_t CheckedIndex, std::size_t... InputIndexes>
    struct get_forwarded_input_impl<T, std::index_sequence<CheckedIndex, InputIndexes...>, DFP_C_REQUIRES((!(
        supports_forward_sample<T, CheckedIndex>{} || supports_const_forward_sample<T, CheckedIndex>{}
        ||
        supports_forward_frame<T, CheckedIndex>{} || supports_const_forward_frame<T, CheckedIndex>{}
    )))
    > : get_forwarded_input_impl<T, std::index_sequence<InputIndexes...>> {};

    template<class T>
    struct get_forwarded_input_impl<T, std::index_sequence<>> : size_t_const<SIZE_MAX> {};

public:
    template<class T = Node>
    struct get_forwarded_input : get_forwarded_input_impl<T, typename T::input_index_sequence> {};

    using output_port_type::output_port_type;

    // note that this result_sample::type can be slightly different from the usual output_sample_t<node> (aka sample_t<output_stream_t<node>>)
    // because it directly exposes the type that node::process_sample method (or node::process_stream) returns, so it can be either const reference or value.
    template<class T = node, class InputIndexSequence = typename T::input_index_sequence, class = void>
    struct result_sample;

    template<class T, std::size_t... InputIndexes>
    struct result_sample<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && (supports_process_sample<T>{} || supports_const_process_sample<T>{})
    ))>
    { typedef decltype(std::declval<T&>().process_sample(std::declval<T&>().input_port(size_t_const<InputIndexes>{}).absorb_sample()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_sample<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_process_sample<T>{}))>
    { typedef decltype(std::declval<T const&>().process_sample(std::declval<T const&>().input_port(size_t_const<InputIndexes>{}).absorb_sample()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_sample<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && supports_process_stream_returning_sample<T>{}
    ))>
    {
    private:
        typedef decltype(
            std::declval<T&>().process_stream(std::declval<T&>().input_port(size_t_const<InputIndexes>{}).capture_stream()...)
        ) result_of_process_stream;
        typedef output_sample_t<T> output_sample;

        //TODO: STATIC WARNING to be enabled (but here is maybe not the best place...)
#if 0
        DFP_STATIC_WARNING(
            std::is_fundamental<output_sample>{} && (std::is_const<output_sample>{} || std::is_volatile<output_sample>{}),
            "Some node of the processing graph returns samples of fundamental type but CV-qualified. \
This should be avoided as this implies some processing overhead for in-place processing"
        );
#endif

        template<class U = result_of_process_stream>
        using add_const_if_missing = std::conditional_t<
                std::is_fundamental<result_of_process_stream>{} && !std::is_const<result_of_process_stream>{} && std::is_const<output_sample>{},
                std::add_const_t<U>, U
        >;

        template<class U = result_of_process_stream>
        using add_volatile_if_missing = std::conditional_t<
                std::is_fundamental<result_of_process_stream>{} && !std::is_volatile<result_of_process_stream>{} && std::is_volatile<output_sample>{},
                std::add_volatile_t<U>, U
        >;

    public:
        // Because of specification C++11 §3.10[basic.lval]/4, result_of_process_stream will lost const or volatile qualification
        // if output sample is a fundamental type. But because of the type propagation mechanism of DFP_TLIB over the processing graph
        // this leads to some compilation isse. So CV-qualification is restored if needed
        // c.f. https://stackoverflow.com/questions/18502188/why-does-decltype-remove-const-from-return-types-for-built-in-types#18502590
        typedef add_volatile_if_missing<add_const_if_missing<result_of_process_stream>> type;
    }; // result_sample<..., DFP_C_REQUIRES((supports_process_stream_returning_sample<T>{}))>

    //TODO: implementation below will likely need to be a bit improved as for the supports_process_stream_returning_sample variant but no test-case found for now
    template<class T, std::size_t... InputIndexes>
    struct result_sample<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_process_stream_returning_sample<T>{}))>
    { typedef decltype(std::declval<T const&>().process_stream(std::declval<T const&>().input_port(size_t_const<InputIndexes>{}).capture_stream()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_sample<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && (supports_forward_sample<T>{} || supports_const_forward_sample<T>{})
    ))>
    {
        //TODO: could be improved to enforce const reference result type when cpu/memory tradeoff has to be considered
        // (this would imply allocation of buffer in node input for forwarded sample input)
        typedef decltype(std::declval<T&>().input_port(get_forwarded_input<>{}).absorb_sample()) type;
        static_assert(std::is_same<std::remove_cv_t<output_sample_t<T>>, std::remove_cvref_t<type>>{}, "forwarded input sample type and node returned type are different");
    };

    template<class T, std::size_t... InputIndexes>
    struct result_sample<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_forward_sample<T>{}))>
    {
        typedef decltype(std::declval<T const&>().input_port(get_forwarded_input<>{}).absorb_sample()) type;
        static_assert(std::is_same<std::remove_cv_t<output_sample_t<T>>, std::remove_cvref_t<type>>{}, "forwarded input sample type and node returned type are different");
    };

    template<class T = node>
    using result_sample_t = typename result_sample<T>::type;

    template<class T = node, class InputIndexSequence = typename T::input_index_sequence, class = void>
    struct result_frame;

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && (supports_process_frame<T>{} || supports_const_process_frame<T>{})
    ))>
    { typedef decltype(std::declval<T&>().process_frame(std::declval<T&>().input_port(size_t_const<InputIndexes>{}).absorb_frame()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_process_frame<T>{}))>
    { typedef decltype(std::declval<T const&>().process_frame(std::declval<T const&>().input_port(size_t_const<InputIndexes>{}).absorb_frame()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && supports_process_stream_returning_frame<T>{}
    ))>
    { typedef decltype(std::declval<T&>().process_stream(std::declval<T&>().input_port(size_t_const<InputIndexes>{}).capture_stream()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_process_stream_returning_frame<T>{}))>
    { typedef decltype(std::declval<T const&>().process_stream(std::declval<T const&>().input_port(size_t_const<InputIndexes>{}).capture_stream()...)) type; };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        !std::is_const<T>{} && (supports_forward_frame<T>{} || supports_const_forward_frame<T>{})
    ))>
    {
        //TODO: could be improved to enforce const reference result type when cpu/memory tradeoff has to be considered
        // (this would imply allocation of buffer in node input for forwarded frame input)
        typedef decltype(std::declval<T&>().input_port(get_forwarded_input<>{}).absorb_frame()) type;
        static_assert(std::is_same<std::remove_cv_t<output_frame_t<T>>, std::remove_cvref_t<type>>{}, "forwarded input frame type and node returned type are different");
    };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((supports_const_forward_frame<T>{}))>
    {
        typedef decltype(std::declval<T const&>().input_port(get_forwarded_input<>{}).absorb_frame()) type;
        static_assert(std::is_same<std::remove_cv_t<output_frame_t<T>>, std::remove_cvref_t<type>>{}, "forwarded input fr tmeype and node returned type are different");
    };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((!std::is_const<T>{} && (
        supports_process_sample<T>{} || supports_process_stream_returning_sample<T>{} || supports_process_stream<T>{} ||
        supports_const_process_sample<T>{} || supports_forward_sample<T>{} || supports_const_forward_sample<T>{}
    )))>
    { typedef port_frame_t<output_port_type> type; };

    template<class T, std::size_t... InputIndexes>
    struct result_frame<T const, std::index_sequence<InputIndexes...>, DFP_C_REQUIRES((
        supports_const_process_sample<T>{} || supports_const_process_stream_returning_sample<T>{} ||
        supports_const_process_stream<T>{} || supports_const_forward_sample<T>{}
    ))>
    { typedef port_frame_t<output_port_type> type; };

    template<class T = node>
    using result_frame_t = typename result_frame<T>::type;

    /**
    * @brief Provides node-specific implementation of @ref output_port::renew_stream().
    *
    * invokes the process_stream() method on the given node instance to produce the output stream
    *
    * @param owner the node instance owning this output port
    **/
    DFP_MUTABLE_CONSTEXPR port_stream_t<output_port_type>  renew_stream(node& owner)
    { return _renew_stream(owner, typename node::input_index_sequence{}); }

    constexpr typename port_stream_t<output_port_type>::to_const::type
    renew_stream(node const& owner) const
    { return _renew_stream(owner, typename node::input_index_sequence{}); }


    /**
    * @brief Provides node-specific implementation of @ref output_port::renew_sample().
    *
    * invokes the process_sample() method on the given node instance to produce the output sample
    *
    * @param owner the node instance owning this output port
    **/
    template<class T = node>
    DFP_MUTABLE_CONSTEXPR result_sample_t<T> renew_sample(node& owner)
    { return _renew_sample(owner, typename T::input_index_sequence{}); }

    /**
    * @brief const flavour of renew_sample()
    */
    template<class T = node>
    constexpr result_sample_t<T const> renew_sample(node const& owner) const
    { return _renew_sample(owner, typename T::input_index_sequence{}); }

    /**
    * @brief Provides node-specificy implementation of @ref output_port::renew_frame().
    *
    * Invokes the process_frame(output_stream_t<_>&) on the given node instance class
    * to produce the output sample stream.
    **/
    template<class T = node>
    DFP_MUTABLE_CONSTEXPR result_frame_t<T> renew_frame(node& owner)
    { return _renew_frame(owner, typename T::input_index_sequence{}); }

    /**
    * @brief Const flavour of renew_frame()
    **/
    template<class T = node>
    constexpr result_frame_t<T const>    renew_frame(node const& owner) const
    { return _renew_frame(owner, typename T::input_index_sequence{}); }

    template<class T = node, class... O, DFP_F_REQUIRES((
        !supports_const_process_frame<T>{} && !supports_const_process_sample<T>{} && !supports_const_process_stream<T>{} &&
        !supports_const_process_stream_returning_sample<T>{} && !supports_const_process_stream_returning_frame<T>{} &&
        !supports_const_forward_sample<T>{}
    ))>
    port_frame_t<output_port_type>
    constexpr renew_frame(node const&, O...) const
    {
        static_assert(
            !supports_process_frame<Node>{} || supports_const_process_frame<T>{},
            "Node is involved in a const expression but it does not implement const process_frame() method"
        );

        static_assert(
            !supports_process_sample<Node>{} || supports_const_process_sample<T>{},
            "Node is involved in a const expression but it does not implement const process_sample() method"
        );

        static_assert(
            !supports_process_stream_returning_sample<Node>{} || supports_const_process_stream_returning_sample<T>{},
            "Node is involved in a const expression but it does not implement const process_stream() method"
        );

        static_assert(
            !supports_process_stream_returning_frame<Node>{} || supports_const_process_stream_returning_frame<T>{},
            "Node is involved in a const expression but it does not implement const process_stream() method"
        );

        static_assert(
            !supports_process_stream<Node>{} || supports_const_process_stream<T>{},
            "Node is involved in a const expression but it does not implement const process_stream() method"
        );

        static_assert(
            !supports_forward_sample<Node>{} || supports_const_forward_sample<T>{},
            "Node is involved in a const expression but it does not implement const forward_sample() method"
        );

        static_assert(
            !supports_forward_frame<Node>{} || supports_const_forward_frame<T>{},
            "Node is involved in a const expression but it does not implement const forward_frame() method"
        );

        return std::declval<port_frame_t<node_output_port>>();
    }

    DFP_MUTABLE_CONSTEXPR node_output_port& get()       { return *this; }
    constexpr node_output_port const&       get() const { return *this; }

    template<class T = node, class InitializationStorageView>
    DFP_CONSTEXPR_VOID invoke_initialize(node& owner, InitializationStorageView initialization_storage)
    {
        invoke_initialize_upwards(owner, typename T::input_index_sequence{}, initialization_storage);
        owner.initialize_downwards(initialization_storage);
    }

    template<class T = node>
    cell_initialization_storage_t<T> invoke_build_initialization_storage(node const& owner) const
    { return owner.build_initialization_storage(owner); }

private:
    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_process_stream<T>{} || supports_const_process_stream<T>{}))>
    DFP_MUTABLE_CONSTEXPR port_stream_t<output_port_type> _renew_stream(node& owner, std::index_sequence<InputIndexes...>)
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_process_stream<T>{}))>
    constexpr typename port_stream_t<output_port_type>::to_const::type
    _renew_stream(node const& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((!supports_process_stream<T>{} && !supports_const_process_stream<T>{}))>
    DFP_MUTABLE_CONSTEXPR port_stream_t<output_port_type> _renew_stream(node& owner, std::index_sequence<InputIndexes...>)
    { return output_port_type::build_stream(owner); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((!supports_const_process_stream<T>{}))>
    constexpr typename port_stream_t<output_port_type>::to_const::type _renew_stream(node const& owner, std::index_sequence<InputIndexes...>) const
    { return output_port_type::build_stream(owner); }

    // Note that this variant of _renew_sample is enabled for "supports_const_process_sample" in order to avoid propagating constness to input port
    // when node is not involved in a const expression
    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_process_sample<T>{} || supports_const_process_sample<T>{}))>
#ifdef __clang__
    constexpr result_sample_t<T> _renew_sample(node& owner, std::index_sequence<InputIndexes...>) const
    //TODO: rename absorb_sample into capture_sample
    { return owner.process_sample(owner.input_port(size_t_const<InputIndexes>{}).absorb_sample()...); }
#else
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) result_sample_t<T> _renew_sample(node& owner, std::index_sequence<InputIndexes...>) const
    //TODO: verify if cloneable still depend on evaluation order
    //GCC evaluates function arguments in reverse order compared to clang
    //Even if it is a bad thing, for now, cloneable source relies on evaluation order (which is not guaranteed as per C++ specification)
    //so the hack below is required at least for GCC even if it should be applied for all compilers (1st implementation is kept as reminder).
    // (hopefully the hack will be removed soon after cloneable source rework)
    //c.f. https://stackoverflow.com/questions/14058592/how-to-guarantee-order-of-argument-evaluation-when-calling-a-function-object
    {
        auto t = std::tuple<decltype(   owner.input_port(size_t_const<InputIndexes>{}).absorb_sample())...>
        {                               owner.input_port(size_t_const<InputIndexes>{}).absorb_sample()...};

        return owner.process_sample(std::get<InputIndexes>(t)...);
    }
#endif

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_forward_sample<T>{} || supports_const_forward_sample<T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) result_sample_t<T> _renew_sample(node& owner, std::index_sequence<InputIndexes...>) const
    {
        auto t = std::tuple<decltype(   owner.input_port(size_t_const<InputIndexes>{}).absorb_sample())...>
        {                               owner.input_port(size_t_const<InputIndexes>{}).absorb_sample()...};

        owner.forward_sample(std::get<InputIndexes>(t)...);

        return std::get<get_forwarded_input<T>{}>(t);
    }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_forward_sample<T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) result_sample_t<T const> _renew_sample(node const& owner, std::index_sequence<InputIndexes...>) const
    {
        auto t = std::tuple<decltype(   owner.input_port(size_t_const<InputIndexes>{}).absorb_sample())...>
        {                               owner.input_port(size_t_const<InputIndexes>{}).absorb_sample()...};

        owner.forward_sample(std::get<InputIndexes>(t)...);

        return std::get<get_forwarded_input<T>{}>(t);
    }

    //TODO: add variant for return type as const reference -> possibly create some new supports_process_stream_returning_const_reference_sample
    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_process_stream_returning_sample<T>{}))>
    result_sample_t<T> _renew_sample(node& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_process_stream_returning_sample<T>{}))>
    result_sample_t<T const> _renew_sample(node const& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_process_sample<T>{}))>
    constexpr result_sample_t<T const> _renew_sample(node const& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_sample(owner.input_port(size_t_const<InputIndexes>{}).absorb_sample()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_process_stream_returning_frame<T>{}))>
    constexpr result_frame_t<T> _renew_frame(node& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_process_frame<T>{} || supports_const_process_frame<T>{}))>
    constexpr result_frame_t<T> _renew_frame(node& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_frame(owner.input_port(size_t_const<InputIndexes>{}).absorb_frame()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_process_frame<T>{}))>
    constexpr result_frame_t<T const> _renew_frame(node const&  owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_frame(owner.input_port(size_t_const<InputIndexes>{}).absorb_frame()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_process_stream_returning_frame<T>{}))>
    constexpr result_frame_t<T const> _renew_frame(node const& owner, std::index_sequence<InputIndexes...>) const
    { return owner.process_stream(owner.input_port(size_t_const<InputIndexes>{}).capture_stream()...); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_forward_frame<T>{} || supports_const_forward_frame<T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) result_frame_t<T> _renew_frame(node& owner, std::index_sequence<InputIndexes...>) const
    {
        auto t = std::tuple<decltype(   owner.input_port(size_t_const<InputIndexes>{}).absorb_frame())...>
        {                               owner.input_port(size_t_const<InputIndexes>{}).absorb_frame()...};

        owner.forward_frame(std::get<InputIndexes>(t)...);

        return std::get<get_forwarded_input<T>{}>(t);
    }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((supports_const_forward_frame<T>{}))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) result_frame_t<T const> _renew_frame(node const& owner, std::index_sequence<InputIndexes...>) const
    {
        auto t = std::tuple<decltype(   owner.input_port(size_t_const<InputIndexes>{}).absorb_frame())...>
        {                               owner.input_port(size_t_const<InputIndexes>{}).absorb_frame()...};

        owner.forward_frame(std::get<InputIndexes>(t)...);

        return std::get<get_forwarded_input<T>{}>(t);
    }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((
        supports_process_sample<T>{} || supports_process_stream_returning_sample<T>{} || supports_process_stream<T>{} ||
        supports_const_process_sample<T>{} || supports_forward_sample<T>{} || supports_const_forward_sample<T>{}
    ))>
    DFP_MUTABLE_CONSTEXPR result_frame_t<T> _renew_frame(node& owner, std::index_sequence<InputIndexes...>)
    { return output_port_type::renew_frame(owner); }

    template<class T = node, std::size_t... InputIndexes, DFP_F_REQUIRES((
        supports_const_process_sample<T>{} || supports_const_process_stream_returning_sample<T>{} ||
        supports_const_process_stream<T>{} || supports_const_forward_sample<T>{}
    ))>
    constexpr result_frame_t<T const> _renew_frame(node const& owner, std::index_sequence<InputIndexes...>) const
    { return output_port_type::renew_frame(owner); }

    template<class InitializationStorageView, std::size_t... InputIndexes>
    DFP_CONSTEXPR_VOID invoke_initialize_upwards(node& owner, std::index_sequence<InputIndexes...>, InitializationStorageView initialization_storage)
    {
        owner.initialize_upwards(initialization_storage);

        using call_sequence = char[]; //trick helping to invoke methods on objects of pack
        (void) call_sequence{(
            owner.input_port(size_t_const<InputIndexes>{}).initialize(initialization_storage)
        , '\0')...};
    }
}; // class node_output_port


template<class InputCell, class Node>
class node_input_port : public detail::input_port<InputCell>
{
    friend Node;

public:
    using detail::input_port<InputCell>::input_port;

    node_input_port&                   get()       { return *this; }
    constexpr node_input_port const&   get() const { return *this; }
}; // class node_input_port


///@}
///@}
///@}
///@} core_grp
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_FRAMEWORK_DETAIL_NODE_PORT_HPP
