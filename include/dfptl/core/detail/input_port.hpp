/**
* @file core/detail/input_port.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_INPUT_PORT_HPP
#define DFPTL_CORE_DETAIL_INPUT_PORT_HPP


#include "dfptl/core/upstream_traits.hpp"
#include "dfptl/core/downstream_traits.hpp"
#include "dfptl/core/detail/port_traits.hpp"
#include "dfptl/core/detail/cell_traits.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
///@addtogroup downstream
///@{
///@addtogroup core_detail
///@{
/** @defgroup input_port Generic input port for processing cell
* @brief Input port provides mechanism to retrieve samples or frames
* from stream produced by the upstream processing cells.
* @{
**/


/**
* @brief Out-of-class definition of @c input_cell member for @ref input_port class.
*
* @note Implements EBCO idiom with InputCell class for purpose of memory consumption optimization
*
* @tparam InputCell is the type or the reference type of the upstream input cell
**/
template<class InputCell, class = void> struct input_port_input_cell;

template<class InputCell>
struct input_port_input_cell<InputCell, DFP_C_REQUIRES((
    !is_reference_wrapper<std::remove_reference_t<InputCell>>{} && (
        !std::is_lvalue_reference<InputCell>{} || std::is_copy_constructible<std::remove_cvref_t<InputCell>>{}
    )
))>
 :  std::remove_cvref_t<InputCell>
{
public:
    typedef std::remove_cvref_t<InputCell> input_cell_value;

    constexpr input_port_input_cell(InputCell&& input_cell) noexcept
     :  input_cell_value(std::forward<InputCell>(input_cell))
    {}

    DFP_MUTABLE_CONSTEXPR input_cell_value&    get()       { return *this; }
    constexpr input_cell_value const&          get() const { return *this; }
}; // class input_port_input_cell<... !std::is_lvalue_reference<InputCell>{} || std::is_copy_constructible<std::remove_cvref_t<InputCell>>{} ...>


template<class InputRefWrapper>
struct input_port_input_cell<InputRefWrapper, DFP_C_REQUIRES((
    is_reference_wrapper<std::remove_reference_t<InputRefWrapper>>{}
))>
 :  public  InputRefWrapper
{
    typedef InputRefWrapper base;
public:
    typedef std::unwrap_reference_t<InputRefWrapper> input_cell_value;

    constexpr input_port_input_cell(InputRefWrapper const& ref_wrapper) noexcept
     :  base(ref_wrapper) {}

    input_cell_value&                  get()       { return *this; }
    constexpr input_cell_value const&  get() const { return *this; }
}; // class input_port_input_cell<... is_reference_wrapper ...>


template<class InputCell>
struct input_port_input_cell<InputCell, DFP_C_REQUIRES((std::is_lvalue_reference<InputCell>{} && !std::is_copy_constructible<std::remove_cvref_t<InputCell>>{}))>
 :  std::reference_wrapper<std::remove_reference_t<InputCell>>
{
public:
    typedef std::remove_reference_t<InputCell> input_cell_value;

 DFP_STATIC_WARNING(true, "Given input cell is not copy-constructible so the processing branch has been built with a ref on the input cell.\n\
 This choice is a bit hazardous as working with branch ref was maybe not your intent.\n To get rid of this warning and clarifying your expectation,\
 you should consider to either\n\
 enforce move-construction by using std::move on the input or\n\
 explicitly request a reference by enclosing the input within std::ref or\n\
 close the input graph with a cloneable sink and then work with its \"ref\" source");

    constexpr input_port_input_cell(InputCell& input_cell) noexcept : std::reference_wrapper<input_cell_value>(input_cell)
    {}

    input_cell_value&                  get()       { return *this; }
    constexpr input_cell_value const&  get() const { return *this; }
}; // class input_port_input_cell<... is_lvalue_reference<InputCell> && && !is_copy_constructible<remove_cvref_t<InputCell>>{} ...>


/**
* @brief Defines generic input port for upstream processing cell.
*
* This class provides some default implementation but shall be specialized for node and source classes.
*
* @tparam InputCell is the type of the input cell to associate to this input port
**/
template<class InputCell>
class input_port
 :  public    input_port_input_cell<InputCell>
 // some issue with clang3, clang6 and gcc5 if input_port_input_cell is not public
// :  protected input_port_input_cell<InputCell>
{
    typedef   input_port_input_cell<InputCell>              input_cell_member;

public:
    typedef input_port_tag dfp_cell_tag;
    typedef InputCell                                       input_cell_type;
    typedef typename input_cell_member::input_cell_value    input_cell_value;

    using input_cell_member::input_cell_member;

    constexpr typename output_stream_t<input_cell_value>::to_const::type    capture_stream() const
    { return input_cell().output_port().renew_stream(input_cell()); }

    DFP_MUTABLE_CONSTEXPR output_stream_t<input_cell_value>    capture_stream()
    { return input_cell().output_port().renew_stream(input_cell()); }

    /**
    * @brief Absorbs a single sample from the linked input cell.
    * @return A single sample.
    **/
    template<class Stream = output_stream_t<input_cell_value>, DFP_F_REQUIRES((Stream::IS_ATOMIC))>
    DFP_MUTABLE_CONSTEXPR
    sample_t<Stream>
    //TODO: should forward result of *begin() rather than always enforce sample_t as returned type.
    // However, this can't be done as of today because of single_sample_frame which always embeds a value
    // while it would be more optimized to sometimes embeds a cref value when input lifefime is known to be long enough
    // sizeof(sample_t) > sizeof(reference type)
//    decltype(*std::declval<Stream>().drain().begin())
    absorb_sample()
    { return *capture_stream()      .drain().begin(); }

    /**
    * @brief const flavour of absorb_sample()
    **/
    template<class STREAM = output_stream_t<input_cell_value>, DFP_F_REQUIRES((STREAM::IS_ATOMIC))>
    constexpr sample_t<STREAM>                      absorb_sample() const
    { return *capture_stream().cdrain().cbegin(); }

    template<class STREAM = output_stream_t<input_cell_value>, DFP_F_REQUIRES((!STREAM::IS_ATOMIC))>
    constexpr sample_t<STREAM>                      absorb_sample() const
    {
        static_assert(always_false<input_cell_value>{},
            "Feeding input of a sample-based processing cell with stream whose frame length is not 1 is not trivial. Consider inserting a routing::to_single_sample node"
        );

        return output_sample_t<input_cell_value>{};
    }

    /**
    * @brief Absorbs a frame from the linked input cell.
    * @return A frame.
    **/
    DFP_MUTABLE_CONSTEXPR output_frame_t<input_cell_value> absorb_frame() //TODO: returned type inference
    { return capture_stream().drain(); }

    /**
    * @brief const flavour of absorb_frame()
    *
    * @note From design perspective, remark that if input port is const the owning cell is also assumed to be const.
    * This enforcement might be overdone if some hybrid cell with mixture of const and non-const input ports
    * could be imagined.
    **/
    output_frame_t<constify_t<input_cell_value>> constexpr absorb_frame() const
    { return capture_stream().cdrain(); }

    constexpr output_stream_specifier_t<input_cell_value> stream_specifier() const noexcept
    { return input_cell().output_port().stream_specifier(); }

    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize(InitializationStorageView initialization_storage)
    //TODO: at this stage all input_cell shall have been fully specified (no unset dynamic field in stream_specifier)
    { return input_cell().output_port().invoke_initialize(input_cell(), initialization_storage); }

    cell_initialization_storage_t<input_cell_value> build_initialization_storage() const
    { return input_cell().output_port().invoke_build_initialization_storage(input_cell()); }

protected:
    DFP_MUTABLE_CONSTEXPR input_cell_value& input_cell()         { return input_cell_member::get(); }
    constexpr input_cell_value const&       input_cell() const   { return input_cell_member::get(); }
}; // class input_port<.. output_stream_t<input_cell_value>::IS_ATOMIC ...>


///@} input_port
///@} core_detail
///@} downstream
///@} core
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_INPUT_PORT_HPP
