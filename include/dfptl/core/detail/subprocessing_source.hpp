/**
* @file core/detail/subprocessing_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SUBPROCESSING_SOURCE_HPP
#define DFPTL_CORE_DETAIL_SUBPROCESSING_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/core/detail/subprocessing_traits.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{


template<class Callable>
struct subprocessing_source_innerbranch
{
public:
    typedef decltype(std::declval<Callable>()()) type;
}; //struct subprocessing_source_innerbranch


template<class Callable>
using subprocessing_source_innerbranch_t = typename subprocessing_source_innerbranch<Callable>::type;

/**
* @brief Out-of-class definition of @c callable member for @ref subprocessing_node class.
*
* @note Implements EBCO idiom with @c Callable class for purpose of memory consumption optimization
*
* @tparam Callable is the type of the callable wrapped by the subprocessing node
**/
template<class Callable>
struct subprocessing_source_callable : Callable
{
    template<class... CallableArgs>
    constexpr subprocessing_source_callable(CallableArgs&&...   callable_args)
        :   Callable(std::forward<CallableArgs>(callable_args)...) {}

    constexpr Callable const&       get() const { return *this; }
    DFP_MUTABLE_CONSTEXPR Callable& get()       { return *this; }
}; // class subprocessing_source_callable


template<class Context, class Callable, class InitStorageKeyValue, class Derived>
class subprocessing_source_base
 :  public  core::source<
        Context,
        typename output_stream_specifier_t<subprocessing_source_innerbranch_t<Callable>>::template set_tag_clue_t<get_tag_type_t<Callable>>,
        Derived, InitStorageKeyValue
    >, private subprocessing_source_callable<Callable>
{
    typedef core::source<
        Context,
        typename output_stream_specifier_t<subprocessing_source_innerbranch_t<Callable>>::template set_tag_clue_t<get_tag_type_t<Callable>>,
        Derived, InitStorageKeyValue
    > source_type;
    typedef subprocessing_source_innerbranch_t<Callable>    innerbranch_type;
    typedef subprocessing_source_callable<Callable>         callable_member;

public:
    template<class... CallableArgs>
    constexpr subprocessing_source_base(Context&& context, CallableArgs&&... callable_args)
    // note that passing a defaulted constructed output stream specifier is actually a workaround
    // to allow subprocessing_source wrapping non-STATIC terminal cell.
    // However this workaround does not allow to give actual value for dynamic part of the stream specifier
    // which most of the time will be an issue.
    // A clean design would be to adapt initialize_xxx method to return the output stream specified and allow its late initialization.
     :  source_type(std::forward<Context>(context), typename source_type::output_specifier_type{}),
        callable_member(std::forward<CallableArgs>(callable_args)...)
    {}

    constexpr subprocessing_source_base(subprocessing_source_base const& it)
     :  source_type(it), callable_member(it.callable_member::get())
    {}

    constexpr subprocessing_source_base(subprocessing_source_base&& it)
     :  source_type(std::move(it)), callable_member(std::move(it.callable_member::get()))
    {}

    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_sample<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_sample_t<source_type> acquire_sample()
    { return *innerbranch_.drain().begin(); }

    template<class T = innerbranch_type, DFP_F_REQUIRES((upstream_produces_frame<T>{}))>
    DFP_MUTABLE_CONSTEXPR output_frame_t<source_type> acquire_frame()
    { return innerbranch_.drain(); }

    template<class InitStorageView, DFP_F_REQUIRES((detail::supports_initialize_upwards<callable_member, InitStorageView>{}))>
    void initialize_upwards(InitStorageView init_storage)
    { initialize_lately_stream_specifier(); callable_member::initialize_upwards(init_storage); }

    template<class InitStorageView, DFP_F_REQUIRES((!detail::supports_initialize_upwards<callable_member, InitStorageView>{}))>
    void initialize_upwards(InitStorageView init_storage)
    { initialize_lately_stream_specifier(); source_type::initialize_upwards(init_storage); }

    template<class InitStorageView, DFP_F_REQUIRES((detail::supports_initialize_downwards<callable_member, InitStorageView>{}))>
    void initialize_downwards(InitStorageView init_storage)
    { callable_member::initialize_downwards(init_storage); }

    template<class InitStorageView, DFP_F_REQUIRES((!detail::supports_initialize_downwards<callable_member, InitStorageView>{}))>
    void initialize_downwards(InitStorageView init_storage)
    { source_type::initialize_downwards(init_storage); }

private:
    void initialize_lately_stream_specifier()
    { this->set_output_stream_specifier(this->output_stream_specifier()
                .set_frame_length(innerbranch_.output_stream_specifier().get_frame_length())
                .set_channel_count(innerbranch_.output_stream_specifier().get_channel_count())
    ); }

    static constexpr innerbranch_type init_innerbranch(callable_member& callable)
    { return callable.get()(); }

    //Note that wathever is the ctor invoked (move, cpy, etc.) the innerbranch shall always be rebuilt,
    //as it may depend on objects no more valid (e.g. dependency on members of the subsource factory)
    innerbranch_type innerbranch_{init_innerbranch(*this)};
}; // class subprocessing_source_base


template<class Context, class Callable, class = void>
struct subprocessing_source
 :      subprocessing_source_base<Context, Callable, void, subprocessing_source<Context, Callable>>
{ using subprocessing_source_base<Context, Callable, void, subprocessing_source<Context, Callable>>::subprocessing_source_base; };

template<class Context, class Callable>
struct subprocessing_source<Context, Callable, DFP_C_REQUIRES((has_initialization_storage_keyvalue<Callable>{}))>
 :      subprocessing_source_base<Context, Callable, typename Callable::initialization_storage_keyvalue, subprocessing_source<Context, Callable>>
{ using subprocessing_source_base<Context, Callable, typename Callable::initialization_storage_keyvalue, subprocessing_source<Context, Callable>>::subprocessing_source_base; };


} // namespace detail
} // inline namespace core
} // namespace dfp



#endif //DFPTL_CORE_DETAIL_SUBPROCESSING_SOURCE_HPP
