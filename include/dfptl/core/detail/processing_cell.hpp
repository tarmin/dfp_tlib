/**
* @file core/detail/processing_cell.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PROCESSING_CELL_HPP
#define DFPTL_CORE_DETAIL_PROCESSING_CELL_HPP


#include "dfptl/core/type_traits.hpp"

#include <boost/fusion/include/map.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{

template<class Context, class Derived>
struct processing_cell
{
    typedef Context context_type;

    /**
    * @brief Provides typedef @c type map of tag vs upstream cell available in current processing cell
    */
    struct tag_map
    {
    private:
        typedef boost::fusion::map<> empty_map_type;

        template<class C, class = void>
        struct view;

        template<class C>
        struct view<C, DFP_C_REQUIRES((
            //TODO: null_tag to be removed
            !std::is_same<null_tag, typename C::cell_tag>{}
            &&
            !std::is_void<typename C::cell_tag>{}
            &&
            !std::is_same<empty_map_type, typename C::upstream_tag_map::type>{}
        ))>
        { typedef boost::fusion::joint_view<
            typename context_type::upstream_tag_map::type,
            boost::fusion::map<boost::fusion::pair<typename context_type::cell_tag, Derived>>
        > type; };

        template<class C>
        struct view<C, DFP_C_REQUIRES((
            !std::is_same<null_tag, typename C::cell_tag>{}
            &&
            !std::is_void<typename C::cell_tag>{}
            &&
            std::is_same<empty_map_type, typename C::upstream_tag_map::type>{}
        ))>
        { typedef boost::fusion::map<boost::fusion::pair<typename context_type::cell_tag, Derived>> type; };

        template<class C>
        struct view<C, DFP_C_REQUIRES((
            std::is_same<null_tag, typename C::cell_tag>{}
            ||
            std::is_void<typename C::cell_tag>{}
        ))>
        { typedef typename context_type::upstream_tag_map::type type; };

    public:
        typedef typename view<context_type>::type type;
    }; // struct upstream_tag_map

    template<class Tag, class = void>
    struct at_tag
    { typedef typename boost::fusion::result_of::value_at_key<typename tag_map::type, Tag>::type type; };

    template<class Tag>
    using at_tag_t = typename at_tag<Tag>::type;


    template<class Tag>
    using has_tag = std::bool_constant<boost::fusion::result_of::has_key<typename tag_map::type, Tag>::value>;
}; // class processing_cell


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PROCESSING_CELL_HPP
