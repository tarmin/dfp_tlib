/**
* @file core/detail/processing_context.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_PROCESSING_CONTEXT_HPP
#define DFPTL_CORE_DETAIL_PROCESSING_CONTEXT_HPP


#include <boost/fusion/include/map.hpp>
#include <boost/fusion/include/value_at_key.hpp>
#include <boost/fusion/include/has_key.hpp>


namespace dfp
{
inline namespace core
{
namespace detail
{

template<class... UpstreamCells>
struct processing_context
{
    /**
    * @brief Provides typedef @c type map of tag vs upstream cell available in current processing context
    */
    struct upstream_tag_map
    {
    private:
        typedef boost::fusion::map<> empty_map_type;

        template<class CellsTuple, class = void>
        struct view
        { typedef empty_map_type type; };

        template<class Cell>
        struct view<std::tuple<Cell>>
        { typedef typename std::remove_cvref_t<Cell>::tag_map::type type; };

        template<class Cell, class NextCell, class... Cells>
        struct view<std::tuple<Cell, NextCell, Cells...>, DFP_C_REQUIRES((
            !std::is_same<empty_map_type, typename std::remove_cvref_t<Cell>::tag_map::type>{}
        ))>
        { typedef boost::fusion::joint_view<
            typename std::remove_cvref_t<Cell>::tag_map::type,
            typename view<std::tuple<NextCell, Cells...>>::type
        > type; };

        template<class Cell, class NextCell, class... Cells>
        struct view<std::tuple<Cell, NextCell, Cells...>, DFP_C_REQUIRES((
            std::is_same<empty_map_type, typename std::remove_cvref_t<Cell>::tag_map::type>{}
        ))>
        { typedef typename view<std::tuple<NextCell, Cells...>>::type type; };

    public:
        typedef typename view<std::tuple<UpstreamCells...>>::type type;
    }; // struct upstream_tag_map

    template<class Tag>
    struct at_tag
    { typedef typename boost::fusion::result_of::value_at_key<typename upstream_tag_map::type, Tag>::type type; };

    template<class Tag>
    using at_tag_t = typename at_tag<Tag>::type;


    template<class Tag>
    using has_tag = std::bool_constant<boost::fusion::result_of::has_key<typename upstream_tag_map::type, Tag>::value>;
}; // class processing_context


} // namespace detail
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_PROCESSING_CONTEXT_HPP
