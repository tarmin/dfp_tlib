/**
* @file core/detail/sink_port.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SINK_PORT_HPP
#define DFPTL_CORE_DETAIL_SINK_PORT_HPP


#include "dfptl/core/detail/input_port.hpp"


namespace dfp
{
inline namespace core
{
namespace detail
{
///@addtogroup core
///@{
///@addtogroup sink
///@{
///@addtogroup core_detail
///@{
/** @defgroup sink_port Definition of @ref input_port "input port" specialized for the @ref node processing cells.
* @{
**/


template<class InputCell, class Sink>
class sink_input_port : public detail::input_port<InputCell>
{
    friend Sink;

public:
    using detail::input_port<InputCell>::input_port;

    sink_input_port&                   get()       { return *this; }
    constexpr sink_input_port const&   get() const { return *this; }
}; // class sink_input_port



///@}
///@}
///@}
///@}
} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_FRAMEWORK_DETAIL_NODE_PORT_HPP
