/**
* @file core/detail/sample_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DETAIL_SAMPLE_SOURCE_HPP
#define DFPTL_CORE_DETAIL_SAMPLE_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/core/stream_specifier.hpp"

#include <initializer_list>
#include <vector>


namespace dfp
{
inline namespace core
{
namespace detail
{


/**
* @brief Sample-based source to inject in a processing branch a sample value or reference-value.
*
* @sa const_sample, mutable_sample
**/
template<class Context, class Sample>
class sample_source
    :   public  core::source<Context, atomic_stream_specifier<std::remove_const_t<Sample>>, sample_source<Context, Sample>>
{
    typedef     core::source<Context, atomic_stream_specifier<std::remove_const_t<Sample>>, sample_source> source_type;

public:
    constexpr sample_source(Context&& context, Sample sample) noexcept
     :  source_type(std::forward<Context>(context)), sample_(sample)
    {}

    constexpr output_sample_t<source_type> const& acquire_sample() const noexcept
    { return sample_; }

private:
    Sample sample_;
}; // class sample_source


template<class Context, class T, T Value>
class sample_source<Context, integral_const<T, Value>>
    :   public core::source<Context, atomic_stream_specifier<integral_const<T, Value>>, sample_source<Context, integral_const<T, Value>>>
{
    typedef    core::source<Context, atomic_stream_specifier<integral_const<T, Value>>, sample_source> source_type;

public:
    constexpr sample_source(Context&& context, integral_const<T, Value>) noexcept
     :  source_type(std::forward<Context>(context))
    {}

    constexpr output_sample_t<source_type> acquire_sample() const noexcept
    { return integral_const<T, Value>{}; }
}; // class sample_source<integral_const<...>>


//TODO: investigate: this template specialization and default implementation seems to conflict as decltype(const_sample(0.0).build()) is weirdly resolved as sample_sample(std::initializer<int>)
template<class Context, class Sample>
class sample_source<Context, std::initializer_list<Sample>>
    :   public  core::source<Context, atomic_stream_specifier<std::remove_const_t<Sample>>, sample_source<Context, std::initializer_list<Sample>>>
{
    typedef     core::source<Context, atomic_stream_specifier<std::remove_const_t<Sample>>, sample_source> source_type;

public:
    template<class... S>
    constexpr sample_source(Context&& context, S... samples) noexcept
     :  source_type(std::forward<Context>(context)), samples_({samples...}), index_(0)
    {}

    DFP_MUTABLE_CONSTEXPR output_sample_t<source_type> const& acquire_sample() noexcept
    {
        /*note that returning element reference is safe here because
         * vector reallocation is not expected as vector is never extended */
        return samples_[index_++%samples_.size()];
    }

private:
    std::vector<Sample> samples_;
    std::size_t index_;
}; // class sample_source<std::initializer_list<...>>


} // namespace detail
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DETAIL_SAMPLE_SOURCE_HPP
