/**
* @file core/sample_input.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_CORE_SAMPLE_INPUT_HPP
#define DFPTL_CORE_SAMPLE_INPUT_HPP


#include "dfptl/core/detail/sample_input_source.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup sample_input_grp "sample input" source
* @brief A @ref sample_input_grp is a placeholder in a processing graph where to later plug another upstream graph.
*
* @sa pluggable_grp
*
* **Source interface**
* @startuml
* rectangle "o(n) = output(n) from later given upstream graph" <<sample_input(sample)>> as src
* interface "output: *...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | an @ref atomic_stream "atomic stream" conveying the samples delivered by a graph plug later
**/
///@{


/**
* @brief Creates a builder for @ref sample_input_grp in a processing grap.
*
* A @ref sample_input_grp helps to simply declare an input of in processing graph which will be
* later defined by invoking its plug() method
*
* @param sample_meta indicates the data type that is injected in the processing branch.
* @param id is a tag class used as input identifier
*
* @tparam Identifier is implictly deduced from `id` parameter
* @tparam Sample is implicitly deduded from `sample_meta` parameter
*
* @return the source instance.
*
* **Usage example**
* @snippet core_test.cpp 'sample_input' usage example
**/
template<class Identifier, class Sample>
inline auto sample_input(type_meta<Sample> sample_meta = type_meta<Sample>{}, Identifier id = Identifier{})
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<std::tuple<decltype(id), typename decltype(sample_meta)::type>, detail::sample_input_source>())
#endif
{ return make_placeholder<std::tuple<decltype(id), typename decltype(sample_meta)::type>, detail::sample_input_source>(); }


///@} // sample input_grp
///@} // core_grp


}// namespace core
} // namespace dfp


#endif // DFPTL_CORE_SAMPLE_INPUT_HPP
