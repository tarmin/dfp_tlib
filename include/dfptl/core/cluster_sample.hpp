/**
* @file core/cluster_sample.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CLUSTER_SAMPLE_HPP
#define DFPTL_CORE_CLUSTER_SAMPLE_HPP


#include "dfptl/core/type_traits.hpp"


namespace dfp
{
inline namespace core
{
/** 
* @addtogroup core_grp
* @addgroup cluster_stream_grp
* @{
**/
/**
* @brief Holds heterogeneous data as a sample type for streaming in processing branch
*
* Each piece of data in the cluster can be retrieved with help of indexing operator and key
*
* @tparam Data is heterogneous data type. It can be struct, tuple
* or any [boost fusion container](https://www.boost.org/doc/libs/1_71_0/libs/fusion/doc/html/fusion/container.html)
*
* @tparam View is the proxy to retrieve each individual piece of data in the cluster
*
* @note For peculiar case where @c Data is a specialization of std::reference_wrapper,
* the @c View proxy is applied on the reference type wrapped.
**/
template<class Data, class View, class = void> struct cluster_sample : Data
{
    typedef View view_type;
    typedef Data data_type;
    template<class Key>
    struct at_r
    { typedef typename boost::fusion::result_of::at_key<View, Key>::type type; };
    template<class Key>
    using at_rt = typename at_r<Key>::type;

    using Data::Data;

    constexpr cluster_sample(Data&& data) : Data(std::move(data)) {}
    constexpr cluster_sample(Data const& data) : Data(data) {}

    template<class Key>
    auto at() /** @cond */ ->
    decltype(boost::fusion::at_key<Key>(view_type{*this})) /** @endcond */
    { return boost::fusion::at_key<Key>(view_type{*this}); }

    template<class Key>
    auto at() const /** @cond */ ->
    decltype(boost::fusion::at_key<Key>(view_type{*const_cast<Data* const>(static_cast<Data const* const>(this))})) /** @endcond */
    { return boost::fusion::at_key<Key>(view_type{*const_cast<Data* const>(static_cast<Data const* const>(this))}); }

    template<class Key>
    auto operator[](Key) /** @cond */ ->
    decltype(this->at<Key>()) /** @endcond */
    { return this->at<Key>(); }
}; // struct cluster_sample


template<class Ref, class View>
struct cluster_sample<Ref, View, DFP_C_REQUIRES((is_specialization<Ref, std::reference_wrapper>::value))> : Ref
{
    typedef View    view_type;
    typedef Ref     data_type;

    using data_type::data_type;

    template<class Key>
    auto at() /** @cond */ ->
    decltype(boost::fusion::at_key<Key>(view_type{this->get()})) /** @endcond */
    { return boost::fusion::at_key<Key>(view_type{this->get()}); }

    template<class Key>
    auto operator[](Key) /** @cond */ ->
    decltype(this->at<Key>()) /** @endcond */
    { return this->at<Key>(); }
}; // cluster_sample<... is_specialization<std::reference_wrapper> ...>


template<class T, class = void>
struct is_cluster_sample : std::false_type {};

template<class T>
struct is_cluster_sample<T, DFP_C_REQUIRES((std::is_base_of<cluster_sample<typename T::data_type, typename T::view_type>, T>::value))>
    :   std::true_type {};



///@} cluster_stream_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif //DFPTL_CORE_CLUSTER_SAMPLE_HPP
