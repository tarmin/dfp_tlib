/**
* @file core/core.hpp
*
* @brief Includes all headers files part of the DFPL framework core
*
* This header file simplifies inclusion of DFPTL required header files for defining processing cell or processing branch
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CORE_HPP
#define DFPTL_CORE_CORE_HPP


#include "dfptl/core/sink.hpp"
#include "dfptl/core/node.hpp"
#include "dfptl/core/from.hpp"
#include "dfptl/core/probe.hpp"
#include "dfptl/core/apply.hpp"
#include "dfptl/core/source.hpp"
#include "dfptl/core/stream.hpp"
#include "dfptl/core/execute.hpp"
#include "dfptl/core/contains.hpp"
#include "dfptl/core/exception.hpp"
#include "dfptl/core/pluggable.hpp"
#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/sample_from.hpp"
#include "dfptl/core/const_sample.hpp"
#include "dfptl/core/sample_input.hpp"
#include "dfptl/core/running_tree.hpp"
#include "dfptl/core/runnable_tree.hpp"
#include "dfptl/core/compile_error.hpp"
#include "dfptl/core/frame_concept.hpp"
#include "dfptl/core/subprocessing.hpp"
#include "dfptl/core/mutable_sample.hpp"
#include "dfptl/core/frame_releaser.hpp"
#include "dfptl/core/sample_releaser.hpp"
#include "dfptl/core/graph_operators.hpp"
#include "dfptl/core/default_settings.hpp"
#include "dfptl/core/stream_specifier.hpp"


/**
* @brief root namespace of the DFP template library.
**/
namespace dfp
{
/**
* @brief namespace of the DFP framework core
*
* It contains all types and functions required to manage a processing tree and implement
* some new processing cells.
**/
inline namespace core
{
/**
* @defgroup core_grp Framework core
* @brief Core definitions of the DFP_TLIB framework for processing network and custom processing cell creation.
*
* **Recommended order for reading**:
* * @ref function_grp
* * @ref placeholder_grp
* @{
*
* @defgroup core_detail implementation details
* @brief Implementation details and boilerplate for framework core
**/


/** @brief short name of functions in @ref core namespace **/
namespace shortname {};


/**
* @brief Details of implementation
* @note Client source code shall not depend on elements in this namespace as they can be changed/removed
* from the library without prior notice.
**/
namespace detail {};


///@} core_grp
} // namespace core
} // namespace dfp


/** @example dfptl_overview_example.cpp
* This is an example presenting the DFPTL DSL.
**/
#endif // DFPTL_CORE_CORE_HPP
