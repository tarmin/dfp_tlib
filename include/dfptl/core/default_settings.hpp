/**
* @file core/default_settings.hpp
*
* Gathers default settings definitions whichs allow to configure globally the DFP TLIB.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DEFAULT_SETTINGS_HPP
#define DFPTL_CORE_DEFAULT_SETTINGS_HPP


#include "dfptl/core/utility.hpp"
#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/detail/single_sample_frame.hpp"

#include <vector>
#include <array>


///@addtogroup core_grp
///@{
/** @defgroup default_settings_grp default settings
* @brief Defines macros and types which help to retrieve or apply some default settings on a compilation unit (per cpp file)
*
* @todo add some usage example (e.g. with std::complex)
**/
///@{


/**
* @def DFPTL_SET_DEFAULT_SETTINGS
* @brief Helps to specify the default settings for the compilation unit given a user-defined default settings structure.
**/
#define DFPTL_SET_DEFAULT_SETTINGS(USER_SETTINGS)\
namespace dfp { \
inline namespace core { \
template<> struct default_settings<true> { \
    static constexpr bool user_defined = true; \
    typedef USER_SETTINGS type; \
}; \
} \
}


///@}
///@}


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
///@addtogroup default_settings_grp
///@{


/**
* @brief Defines predefined default values and types in DFP_TLIB
**/
struct predefined_default_settings
{
    /** default length value of frame delivered by @ref constant_flow_constant "constant flow stream" **/
    static constexpr const std::size_t FRAME_LENGTH = 512;
    /** default type for sample **/
    typedef double sample_type;
    /** default signed integral type for sample **/
    typedef int integral_sample_type;
    /** default unsigned integral type for sample **/
    typedef unsigned unsigned_integral_sample_type;
    /** default frame type with static extent **/
    template<class T, size_t Extent = FRAME_LENGTH>
    using static_extent_frame = std::array<T, Extent>; //TODO: std::array does not allow constexpr support with C++11 in atomic_stream
    /** default frame type conveyed by stream with dynamic frame extent **/
    template<class T>
    using dynamic_extent_frame = std::vector<T>; // std::vector is preferred to std::deque as it is assumed that in most common case, length will be fixed after cell initialization
    /** default frame type containing only one single sample **/
    template<class T>
    using atomic_frame = detail::single_sample_frame<T>;
    /** set of tags describing the default graph designer capabilities **/
    typedef std::tuple<> graph_designer_caps;
}; //struct predefined_default_settings


/**
* @brief Provides typedef @c type specifying the default settings to apply in the compilation unit.
*
* By default, default settings are given by the @ref predefined_default_settings "predefined default settings" struct.
* However some user-defined settings can be applied with help of the macro DFPTL_SET_DEFAULT_SETTINGS.
* To specify its own user-defined settings, one must specialize dfptl::core::default_settings for UserDefined template argument equal to true.
*
* @todo provide some example
**/
template<bool UserDefined = true> struct
default_settings
{
    static constexpr bool user_defined = false;
    typedef predefined_default_settings type; /**< indicates the type of the default settings in use **/
}; // struct default_settings


/**
* @brief Alias of default_settings<...>::type
**/
// Attention: shall not be used in template library source code to avoid early instantiation of default_settings<true>
// which would prevent user-defined default settings to be implemented.
template<bool UserDefined = true>
using default_settings_t = typename default_settings<UserDefined>::type;


/**
* @brief Alias of default_settings<...>::sample_type
**/
template<bool UserDefined = true>
using default_sample_t = typename default_settings_t<UserDefined>::sample_type;


/**
* @brief Alias of size_t_const<default_settings<...>::FRAME_LENGTH>
**/
template<bool UserDefined = true>
using default_frame_length_const = size_t_const<default_settings_t<UserDefined>::FRAME_LENGTH>;


template<class Sample = void, bool UserDefined = true>
using  default_dynamic_extent_frame_t =
    typename default_settings_t<UserDefined>::template dynamic_extent_frame<Sample>;


template<class Sample, std::size_t Length, bool UserDefined = true>
using default_static_extent_frame_t =
    typename default_settings_t<UserDefined>::template static_extent_frame<Sample, Length>;


template<class Sample, bool UserDefined = true>
using default_atomic_frame_t =
    typename default_settings_t<UserDefined>::template atomic_frame<Sample>;


template<class Specifier, bool UserDefined = true, class = void>
struct default_frame_template;

template<class Specifier, bool UserDefined>
struct default_frame_template<Specifier, UserDefined, DFP_C_REQUIRES(((Specifier::FRAME_EXTENT != DYNAMIC_EXTENT) && (Specifier::FRAME_EXTENT > 1)))>
{ typedef default_static_extent_frame_t<std::nullptr_t, 0, UserDefined> type; };

template<class Specifier, bool UserDefined>
struct default_frame_template<Specifier, UserDefined, DFP_C_REQUIRES((Specifier::FRAME_EXTENT == DYNAMIC_EXTENT))>
{ typedef default_dynamic_extent_frame_t<std::nullptr_t, UserDefined> type; };

template<class Specifier, bool UserDefined>
struct default_frame_template<Specifier, UserDefined, DFP_C_REQUIRES((Specifier::FRAME_EXTENT == 1))>
{ typedef default_atomic_frame_t<std::nullptr_t, UserDefined> type; };

/**
* @brief Alias for type `typename default_frame_template<...>::type`
*/
template<class Specifier, bool UserDefined = true>
using default_frame_template_t = typename default_frame_template<Specifier, UserDefined>::type;


///@} default_settings_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DEFAULT_SETTINGS_HPP
