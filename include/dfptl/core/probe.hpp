/**
* @file core/probe.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PROBE_HPP
#define DFPTL_CORE_PROBE_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/detail/probe_node.hpp"


namespace dfp
{
inline namespace core
{
inline namespace nodes
{
///@addtogroup core_grp
///@{
/**
* @defgroup core_probe_grp "probe" node
* @brief Bufferizes input frame to later use in the processing graph
*
* A @ref core_probe_grp bufferizes at its each firing the incoming frame from later use in the processing graph  (with help of @ref core_from_grp).
*
* **Node interface**
* @startuml
* interface "input: ...i(n)" <<captured>> as in
* rectangle "o(n) = i(n)" <<core::probe(TAG)>> as node
* interface "output: ...o(n)" <<produced>> as out
* node <-up- in
* out <-up- node
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* "captured"         | **input**   | @ref stream_grp
* "produced"         | **output**  | same @ref stream_grp as in input
*
* **Usage example**
* @snippet core_cell_test.cpp core::from usage example
*
* @sa @ref core_from_grp
*
* @warning @ref core_probe_grp instance is not "moveable" (not move-constructible) when runnable
* @{
**/


/**
* @brief Creates an unsafe @ref core_probe_grp builder
*
* probe point is identified by the mandatory @c tag associated to this cell builder in the graph.
*
* @note
* Some memory optimization in implementation of this unsafe @ref core_probe_grp allows to save allocation and copy of the received frame
* when upstream part delivers it as lvalue const reference.
* However lifetime of such a lvalue-const-reference frame is only guaranteed in the call stack of the processing call.
* So @ref core_from_grp shall be invoked in same processing call that the associated @ref core_probe_grp to have this optimization applied correctly,
* otherwise the frame reference kept on the @ref core_from_grp will be a dangling reference. 
* Some cells can perform branching in the processing flow and prevent some parts of the graph from being called. With such a cell,
* usage of this variant of @ref core_probe_grp is unsafe (but the safe variant is ok). 
* In peculiar, unsafe @ref core_probe_grp shall not be in use within same graph as following others cells of DFPTLIB:
* * @ref routing::switch_if
* * @ref routing::hold
**/
constexpr inline auto unsafe_probe() /** @cond */ ->
decltype(make_placeholder<detail::probe_node>()) /** @endcond */ 
{ return make_placeholder<detail::probe_node>(); }


/**
* @brief Creates a safe @ref core_probe_grp builder
*
* probe point is identified by the mandatory @c tag associated to this cell builder in the graph.
**/
constexpr inline auto probe() /** @cond */ ->
decltype(make_placeholder<std::tuple<bool_const<true>>, detail::probe_node>()) /** @endcond */ 
{ return make_placeholder<std::tuple<bool_const<true>>, detail::probe_node>(); }


/**
* @brief Creates an unsafe @ref core_probe_grp builder
*
* @param[in] tag identifying the frame stream at this location of the graph processing.
*
* @tparam Tag is implicitly deduced from @c tag parameter and shall inherit from `custom_tag`
**/
template<class Tag>
constexpr inline auto unsafe_probe(Tag tag = Tag{}) /** @cond */ ->
decltype(unsafe_probe().tag(tag)) /** @endcond */ 
{ return unsafe_probe().tag(tag); }


/**
* @brief Creates a safe @ref core_probe_grp builder
*
* @param[in] tag identifying the frame stream at this location of the graph processing.
*
* @tparam Tag is implicitly deduced from @c tag parameter and shall inherit from `custom_tag`
**/
template<class Tag>
constexpr inline auto probe(Tag tag = Tag{}) /** @cond */ ->
decltype(probe().tag(tag)) /** @endcond */ 
{ return probe().tag(tag); }


///@} core_probe_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using core::unsafe_probe;
using core::probe;
} // namespace nodes
} // namespace shortname


///@} core_grp
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_PROBE_HPP
