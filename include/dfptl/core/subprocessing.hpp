/**
* @file core/subprocessing.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_CORE_SUBPROCESSING_HPP
#define DFPTL_CORE_SUBPROCESSING_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/detail/subprocessing_cell.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/**
* @defgroup subprocessing_grp subprocessing cell
* @brief Embeds some reusable processing in an upper level graph.
*
* Depending on the processing which is embedded a @ref subprocessing_grp can take place in a source, node or sink placement.
*
* @warning For now, subprocessing at a sink placement is not supported.
* @warning As a node he @ref subprocessing_grp instance is not "moveable" (not move-constructible) when runnable
* @{
**/


/**
* @brief Creates a placeholder for a @ref subprocessing_grp to embed some reusable processing in an upper level graph. 
*
* **Usage examples**
* * example of subprocessing node
* @snippet core_test.cpp subprocessing_node subpart example
* ...
* @snippet core_test.cpp subprocessing_node upperpart example
*
* * example of subprocessing node and source
* @snippet core_test.cpp subprocessing_source upperpart example
*
* @param[in] callable is a factory of the sub-processing branch which may take as input(s) some <b>reference</b> on the input(s)
* reaching this subprocessing cell.
*
* @note Take attention that input(s) passed to the callable factory are passed by reference and not by value
* Thus if some input appears at multiple place in the sub-processing branch it will be drained as many times as it is present.
**/
template<class Callable>
inline constexpr auto subprocessing(Callable&& callable) /** @cond */ ->
decltype(make_placeholder<std::tuple<std::remove_reference_t<Callable>>, detail::subprocessing_cell>(std::forward<Callable>(callable))) /** @endcond */
{ return make_placeholder<std::tuple<std::remove_reference_t<Callable>>, detail::subprocessing_cell>(std::forward<Callable>(callable)); }


namespace shortname
{
///@addtogroup subprocessing_grp
///@{
/**
* @brief shortname for @ref core::subprocessing(Callable)
**/
template<class Callable>
inline constexpr auto subproc(Callable&& callable) /** @cond */ ->
decltype(subprocessing(std::forward<Callable>(callable))) /** @endcond */
{ return subprocessing(std::forward<Callable>(callable)); }
///@} //subprocessing_grp
} // namespace shortname


///@} subprocessing_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_SUBPROCESSING_HPP
