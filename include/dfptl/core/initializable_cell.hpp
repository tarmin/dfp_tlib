/**
* @file core/downstream_cell.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_INITIALIZABLE_CELL_HPP
#define DFPTL_CORE_INITIALIZABLE_CELL_HPP


#include "dfptl/core/detail/initializable_cell.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup initializable_grp initializable cells
* @brief Any custom @ref source_grp, @ref sink_grp and @ref node_grp can optionally exposed some initilialization operation.
*
* Such an initialization operation if present is invoked by the DFP framework once for all when the entire processing tree has been constructed
* but before first execution of any processing operations (`process_`, `forward_`, `release_` or `acquire_` methods).
* If defined, the initialization operations `initialize_upwards` of processing cells are executed in cascade from the downmost, the @ref core::sink instance,
* up to the topmost  ones, the @ref core::source instances. The cascading invocation allows cells to exchange some data with help of an "initialization storage"
* only available during the initialization stage
* Then if defined, the initialization operations `initialize_downwards` of processing cells are executed in cascade from the upmost, the @ref core::source instances,
* up to the downmost  one, the @ref core::sink instance. The cascading invocation allows again cells to exchange some data with help of an "initialization storage"
* only available during the initialization stage
*
* @warning Note that a common mistake when working with the initialization operations is to wrongly expect them to be invoked while the processing graph
* is not terminated by a @ref core::sink. 
* @{
**/


/**
* @brief base class for @ref source_grp, @ref sink_grp and @ref node_grp bringing the initialization mechanism.
*
* @tparam Inputs is the tuple of input cells provided by the DFP framework.
* @tparam KeyValue defines a pair<Key, Value> added by this cell in the "initialisation storage".
* Note that if an identical pair<Key, Value> already exists, the "initialisation storage" is silently left unchanged.
* The "initialisation storage" is a temporary map view only available within the optional `upwards_initialize` method
* possibly implemented by the `Derived` class.
* @tparam Derived topmost derived class from initializable_cell (CRTP idiom)
* @tparam UnmoveableRunnable indicates that the cell cannot be moved once it has been initialized
**/
//TODO: introduce runnable_cell concept for all cells.
// runnable_cell is different from runnable(_tree)
// runnable_cell is produced by initializing initializable_cell
template<class Inputs, class KeyValue, class Derived, bool UnmoveableRunnable = false>
class initializable_cell
 :  public  detail::initializable_cell<Inputs, index_sequence_from_t<Inputs>, KeyValue, Derived, UnmoveableRunnable>
{
    typedef detail::initializable_cell<Inputs, index_sequence_from_t<Inputs>, KeyValue, Derived, UnmoveableRunnable> initializable_detail;
    template<class> friend class detail::input_port;

public:
    static constexpr auto UNMOVEABLE_RUNNABLE = initializable_detail::UNMOVEABLE_RUNNABLE;

protected:
    /**
    * @brief customizable operation of a cell invoked at upwards initialization of the processing graph
    *
    * This method by default does not perform anything but optionally can be overloaded in child processing cell.
    * It is invoked by the DFP_TLIB framework once for all right after completing creation of the processing tree
    * i.e. when ther terminal `core::sink` is bound to the graph. All the `initialize_upwards` of various cells
    * in the processing tree are called from the downmost one, the terminal sink, to the upwards ones, the sources.
    *
    * @param initialization_storage is a map view which can be manipulated by each processing cells to share information with others.
    * @warning the `initialization_storage` passed may only be accessed in the scope of the `initialize_upwards`
    * as it is a temporary storage destroyed right after the initialization stage. No reference or pointer shall be kept. 
    *
    * @tparam InitializationStorageView is implicitly deduced from `initialization_storage`. 
    * This type actually complies with [boost::fusion concept of associative sequence](https://www.boost.org/doc/libs/1_77_0/libs/fusion/doc/html/fusion/sequence/concepts/associative_sequence.html)
    **/
    template<class InitializationStorageView>
    void initialize_upwards(InitializationStorageView initialization_storage)
    { static_cast<Derived const*>(this)->initialize_upwards(initialization_storage); }

    /**
    * @brief customizable operation of a cell invoked at downwards initialization of the processing graph
    *
    * This method by default does not perform anything but optionally can be overloaded in child processing cell.
    * It is invoked by the DFP_TLIB framework once for all right after completing creation of the processing tree
    * i.e. when ther terminal `core::sink` is bound to the graph and after upwards initialization.
    * All the `initialize_downwards` of various cells in the processing tree are called from the upmost one, the sources,
    * to the downward one, the terminal sink.
    *
    * @param initialization_storage is a map view which can be manipulated by each processing cells to share information with others.
    * @warning the `initialization_storage` passed may only be accessed in the scope of the `initialize_downwards`
    * as it is a temporary storage destroyed right after the initialization stage. No reference or pointer shall be kept. 
    *
    * @tparam InitializationStorageView is implicitly deduced from `initialization_storage`. 
    * This type actually complies with [boost::fusion concept of associative sequence](https://www.boost.org/doc/libs/1_77_0/libs/fusion/doc/html/fusion/sequence/concepts/associative_sequence.html)
    *
    * @sa initialize_upwards
    **/
    template<class InitializationStorageView>
    void initialize_downwards(InitializationStorageView initialization_storage)
    { static_cast<Derived const*>(this)->initialize_downwards(initialization_storage); }
};


///@} initializable_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_INITIALIZABLE_CELL_HPP
