/**
* @file core/cell_traits.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CELL_TRAITS_HPP
#define DFPTL_CORE_CELL_TRAITS_HPP


#include "stl/type_traits.hpp"


namespace dfp
{
inline namespace core
{


template<class T, class = void>
struct cell_initialization_storage
{ typedef typename std::remove_reference_t<T>::initialization_storage_type type; };

template<class T>
using cell_initialization_storage_t = typename cell_initialization_storage<T>::type;


template<class T, class = void>
struct cell_initialization_keyvalue
{ typedef typename std::remove_reference_t<T>::initialization_keyvalue_type type; };

template<class T>
using cell_initialization_keyvalue_t = typename cell_initialization_keyvalue<T>::type;


} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_CELL_TRAITS_HPP
