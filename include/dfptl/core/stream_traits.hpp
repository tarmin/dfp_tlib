/**
* @file core/stream_traits.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_STREAM_TRAITS_HPP
#define DFPTL_CORE_STREAM_TRAITS_HPP


#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/utility.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
///@addtogroup stream_grp
///@{
/** @defgroup stream_traits_grp Stream traits
* @brief Provides helper class to retrieve traits on @ref stream
* @{
**/


/**
* @brief If T is a @ref stream
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* A stream encapsulates the data sample or frame and flow from upstream processing cell to downstream one.
* It is not expected to be directly manipulated by end-user but provides relevant information for processing tree
* management.
*
* @tparam T the type or reference type to test
**/
template<class T, class = void>
struct is_stream
 :  std::false_type
{}; // struct is_stream

//TODO: dfp_cell_tag to be renamed into dfp_processing_tag
template<class T>
struct is_stream<T, DFP_C_REQUIRES((std::is_base_of<stream_tag, typename T::dfp_cell_tag>{}))>
 :  std::true_type
{}; // struct is_stream<... stream ...>


///@} stream_traits_grp
///@} stream_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_STREAM_TRAITS_HPP
