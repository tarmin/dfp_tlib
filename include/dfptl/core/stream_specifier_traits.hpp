/**
* @file core/stream_specifier_traits.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_STREAM_SPECIFIER_TRAITS_HPP
#define DFPTL_CORE_STREAM_SPECIFIER_TRAITS_HPP


#include "dfptl/core/stream_traits.hpp"
#include "dfptl/core/default_settings.hpp"
#include "dfptl/core/detail/frame_traits.hpp"
#include "dfptl/core/detail/type_traits.hpp"


namespace dfp
{
inline namespace core
{


template<std::size_t, std::size_t, class, class, class> struct stream_specifier;
//template<class, std::size_t, class> struct stream_specifier_from_frame;
template<class, bool UserDefined, class> struct default_frame_template;

struct stream_specifier_tag {};


///@addtogroup core_grp
///@{
///@addtogroup stream_specifiers
///@{
/** @defgroup stream_spec_traits stream specifier traits
* @brief Provides helper class to retrieve traits on @ref stream_specifier
* @{
**/


/**
* @brief If T is a template specialization of @ref stream_specifier
* provides the member constant @c value equal to true. Otherwise @c value is false.
*
* @tparam T the type or reference type to test
**/
template<class T, class Enable = void>
struct is_stream_specifier : std::false_type
{}; // struct is_stream_specifier

template<class T>
struct is_stream_specifier<T, DFP_C_REQUIRES((std::is_same<typename T::dfp_tag, stream_specifier_tag>{}))> :   std::true_type
{}; // struct is_stream_specifier<... stream_specifier ...>


/**
* @brief Provides member typedef @c type which is defined as the sample type conveyed by the given @ref stream T.
*
* @remark T shall be a @ref stream or @ref stream_specifier otherwise compile-time assertion failure will happen.
*
* @tparam T is the @ref stream type on which to retrieve the sample type.
**/
template<class T, class = void>
struct sample
{ static_assert(std::disjunction<is_stream<T>, detail::is_as_input_tag<T>, is_stream_specifier<T>>{}, "given template argument T is neither a stream_specifier neither a stream nor a valid tag"); };

template<class T>
struct sample<T, DFP_C_REQUIRES((is_stream_specifier<T>{}))> 
{ typedef typename T::sample_clue type; };

template<class T>
struct sample<T, DFP_C_REQUIRES((is_stream<T>{}))>
{ typedef typename sample<typename T::specifier_type>::type type; };

template<class T>
struct sample<T, DFP_C_REQUIRES((is_custom_tag<typename T::frame_sketch_clue>{} && detail::is_as_input_tag<typename T::frame_sketch_clue>{}))>
{ typedef as_input_tag<detail::input_index_of<typename T::frame_sketch_clue, void>{}> type; };

template<class T>
struct sample<T, DFP_C_REQUIRES((detail::is_as_input_tag<T>{}))> 
{ typedef T type; };

template<class T>
struct sample<T, DFP_C_REQUIRES((is_custom_tag<typename T::frame_sketch_clue>{} && !detail::is_as_input_tag<typename T::frame_sketch_clue>{}))>
{ static_assert(always_false<T>{}, "sample_t class helper cannot handle the tag of frame_sketch_clue in the given stream specifier"); };


/**
* @brief Alias for @ref sample<T>::type
**/
template<class T>
using sample_t = typename sample<T>::type;


/**
* @brief Provides member typedef @c type which is defined as the frame type conveyed by the given @ref stream T.
*
* @remark T shall be a @ref stream or stream_specifier otherwise compile-time assertion failure will happen.
*
* @tparam T is the @ref stream type on which to retrieve the frame type.
**/
template<class T, class = void>
struct frame
{
    static_assert(std::disjunction<is_stream<T>, detail::is_as_input_tag<T>, is_stream_specifier<T>>{}, "given template argument T is neither a stream_specifier neither a stream nor a valid tag");

    typedef typename decltype(std::declval<T>().get_frame_builder())::frame_type type;
};

template<class T>
struct frame<T, DFP_C_REQUIRES((is_stream<T>{}))>
{ typedef typename frame<typename T::specifier_type>::type type; };


//TODO: remove as this cannot work as it is
template<class T>
struct frame<T, DFP_C_REQUIRES((detail::is_as_input_tag<T>{}))>
{ typedef T type; };


template<class StreamSpecifier>
struct frame<StreamSpecifier, DFP_C_REQUIRES((
    !std::is_void<typename StreamSpecifier::frame_sketch_clue>{} &&
    !std::is_same<typename StreamSpecifier::frame_sketch_clue, dynamic_template_meta>{}
))>
{
    typedef typename decltype(StreamSpecifier::FRAME_SKETCH_CLUE
        .set_sample_type(StreamSpecifier::SAMPLE_CLUE)
        .set_length(StreamSpecifier::FRAME_EXTENT)
    )::frame_type type;

//    static constexpr detail::frame_meta<type> META = detail::frame_meta<type>{}/*.to_type_meta()*/; // TODO: rename into TYPE
};

template<class T>
struct frame<T, DFP_C_REQUIRES((is_stream_specifier<T>{} && std::is_same<typename T::frame_sketch_clue, dynamic_template_meta>{}))>
{ static_assert(always_false<T>{}, "TODO"); };


template<class StreamSpecifier>
struct frame<StreamSpecifier, DFP_C_REQUIRES((std::is_void<typename StreamSpecifier::frame_sketch_clue>{}))>
 :  frame<typename StreamSpecifier::template set_frame_sketch_clue_t<default_frame_template_t<StreamSpecifier>>>
{};


/**
* @brief Alias for @ref frame<T>::type
**/
template<class T>
using frame_t = typename frame<T>::type;


/**
* @brief Provides member typedef @c type with the tag of the given @ref stream T.
*
* @remark T shall be a @ref stream or stream_specifier otherwise compile-time assertion failure will happen.
*
* @tparam T is the @ref stream type on which to retrieve the tag.
**/
template<class T, class = void>
struct tag
{
    static_assert(is_stream_specifier<T>{} || is_stream<T>{}, DFP_UNEXPECTED_ERROR);
    typedef typename T::tag_clue type;
};


/**
* @brief Alias for @ref tag<T>::type
**/
template<class T>
using tag_t = typename tag<T>::type;


///@}
///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_STREAM_SPECIFIER_TRAITS_HPP
