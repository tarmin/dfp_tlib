/**
* @file core/sample_from.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_SAMPLE_FROM_HPP
#define DFPTL_CORE_SAMPLE_FROM_HPP


#include "dfptl/core/from.hpp"
#include "dfptl/core/const_sample.hpp"
#include "dfptl/core/mutable_sample.hpp"


namespace dfp
{
inline namespace core
{
inline namespace sources
{
///@addtogroup core_grp
///@{
/**
* @defgroup core_sample_from_grp "sample_from" source
* @brief Source delivers samples given a producer which may taken various form.
*
* A @ref core_sample_from_grp delivers frame sample to the processing graph given a producer which can be:
* * a constant value (if so, aliases @ref const_sample_grp),
* * a reference on value (if so, aliases @ref mutable_sample_grp),
* * a callable producing samples (if so, aliases @ref from_callable_grp),
* * an iterator producing samples (if so, aliases @ref from_iterator_grp),
* * a @ref custom_tag in graph where sample is probed,
* * or anything else as long as a custom specialization of the @ref core_sample_from_grp builder exists.
*
* **Node interface**
* @startuml
* rectangle "o(n) = output(n) from given sample producer" <<core::from(producer)>> as src
* interface "output: ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* "produced"         | **output**  | some @ref atomic_stream "atomic stream"
*
* @sa the @ref const_sample_grp, @ref mutable_sample_grp, from_callable_grp, from_iterator_grp
* @{
**/


/**
* @brief Creates a @ref core_sample_from_grp builder given value(s).
*
* This default specialization simply delivers given value(s) as input samples of the graph.
*
* @param[in] values to deliver as input samples of the graph. 
*
* @tparam Values are implicitly deduced from @c values parameter
*
* **Usage example**
* @snippet core_cell_test.cpp 'core::sample_from' example usage given values
*
* @note A value located at the upstream edge of of a graph is implicitly converted to a source with @c sample_from
* (as long as this value is not itself a graph or cell builder)
*
* **Implicit conversion example**
* @snippet core_cell_test.cpp example of implicit conversion from value to 'core::sample_from'
**/
template<class... Values>
constexpr inline auto sample_from(Values&&... values)
#if not DFP_CPP14_SUPPORT
->
decltype(const_sample(std::forward<Values>(values)...))
#endif
{ static_assert(sizeof...(Values) > 0, "at least one value shall be passed to 'sample_from' source builder");
  return const_sample(std::forward<Values>(values)...); }


/**
* @brief Creates a @ref core_sample_from_grp builder given a value reference.
*
* This specialization delivers referred value as input samples of the graph.
* If referred value change, the input sample is changed at next firing of the processing graph. 
*
* @param[in] ref is the reference on value to deliver as input samples of the graph. 
*
* @tparam Value is implicitly deduced from @c ref parameter
*
* **Usage example**
* @snippet core_cell_test.cpp 'core::sample_from' example usage given a value reference
*
**/
template<class Value>
constexpr inline auto sample_from(std::reference_wrapper<Value> ref)
#if not DFP_CPP14_SUPPORT
->
decltype(mutable_sample(ref.get()))
#endif
{ return mutable_sample(ref.get()); }


/**
* @brief Creates a @ref core_sample_from_grp builder given a @ref custom_tag.
*
* This specialization delivers samples probed at @ref custom_tag position as input samples of the graph.
*
* @param[in] custom_tag is the tag of the location where samples are probed in upstream part of the graph. 
*
* @tparam CustomTag is implicitly deduced from @c custom_tag parameter
*
* **Usage example**
* @snippet core_cell_test.cpp 'core::sample_from' example usage given a custom_tag
*
**/
template<class CustomTag, DFP_F_REQUIRES((is_custom_tag<CustomTag>{}))>
constexpr inline auto sample_from(CustomTag custom_tag)
#if not DFP_CPP14_SUPPORT
->
decltype(from(custom_tag))
#endif
{ return from(custom_tag); }


///@} core_sample_from_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
/** @brief shortname for @ref sample_from() **/
template<class T>
inline constexpr auto sfrom(T&& t)
#if not DFP_CPP14_SUPPORT
->
decltype(sample_from(std::forward<T>(t)))
#endif
{ return sample_from(std::forward<T>(t)); }
} // namespace sources
} // namespace shortname


///@} core_grp
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_SAMPLE_FROM_HPP
