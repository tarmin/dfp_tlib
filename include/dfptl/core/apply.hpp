/**
* @file core/apply.hpp
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_APPLY_HPP
#define DFPTL_CORE_APPLY_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/processing_descriptor.hpp"
#include "dfptl/core/detail/apply_cell.hpp"

#include <functional>


// Work-in-progress, not ready for use in client code
#define DFP_DEFINE_1IN_NODE(NODE_NAME, CALLABLE, REQUIREMENT) \
DFP_FUNCTION_FAILED_INPUT_REQUIREMENT(NODE_NAME, REQUIREMENT) \
namespace detail { \
template<class INPUT0> \
using DFP_CONST_CALLABLE_TYPE(NODE_NAME) = decltype(CALLABLE(std::declval<INPUT0 const&>())); \
template<class INPUT0> \
struct DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME) : std::experimental::is_detected<DFP_CONST_CALLABLE_TYPE(NODE_NAME), INPUT0> {}; \
} /*namespace detail*/ \
namespace functions { \
template<class INPUT0> \
struct NODE_NAME<INPUT0, DFP_C_REQUIRES((REQUIREMENT && detail:: DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME)<INPUT0>{}))> { \
constexpr auto operator()(INPUT0 const& input_sample) const -> \
decltype(CALLABLE(input_sample)) \
{ return CALLABLE(input_sample); } \
}; \
template<class INPUT0> \
struct NODE_NAME<INPUT0, DFP_C_REQUIRES((REQUIREMENT && !detail:: DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME)<INPUT0>{}))> { \
DFP_MUTABLE_CONSTEXPR auto operator()(INPUT0 const& input_sample) -> \
decltype(CALLABLE(input_sample)) \
{ return CALLABLE(input_sample); } \
}; \
template<> \
struct NODE_NAME<void> { \
template<class INPUT0, DFP_F_REQUIRES((detail:: DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME)<INPUT0>{}))> \
constexpr auto operator()(INPUT0 const& input_sample) const -> \
decltype(NODE_NAME<INPUT0>()(input_sample)) \
{ return NODE_NAME<INPUT0>()(input_sample); } \
template<class INPUT0, DFP_F_REQUIRES((!detail:: DFP_SUPPORTS_CONST_CALLABLE(NODE_NAME)<INPUT0>{}))> \
DFP_MUTABLE_CONSTEXPR auto operator()(INPUT0 const& input_sample) -> \
decltype(NODE_NAME<INPUT0>()(input_sample)) \
{ return NODE_NAME<INPUT0>()(input_sample); } \
}; \
} /*namespace functions*/ \
inline namespace nodes { \
auto constexpr NODE_NAME() -> \
decltype(apply<functions::NODE_NAME<>>()) \
{ return apply<functions::NODE_NAME<>>(); } \
} /*namespace nodes*/


namespace dfp
{
inline namespace core
{
/**
* @addtogroup core_grp
* @{
* @defgroup apply_grp "apply" cells
* @brief the @ref apply_grp create processing cells from [callables](https://en.cppreference.com/w/cpp/named_req/Callable).
*
* The type of the processing cells which will be created depends on the prototype of the operation of the callable.
* Thus, if the operation:
* * has no input, a processing source will be created
* * has no output, a processing sink will be created
* * has some input **and** output, a processing node will be created
*
* The output sample type is automatically inferred from the output type of the callable. When to `callable` consumes a single input stream
* and simply forwards this input stream to its output, please consider rather working with @ref execute_grp.
*
* Operations of the callable shall meet definition requirements of @ref core::source, @ref core::node or @ref core::sink.
*
* @note for detailed example, please see @ref dfptl_overview_example.cpp
*
* @sa execute_grp
* @{
**/


template<class Descriptor, class... Args, DFP_F_REQUIRES((detail::is_processing_descriptor<Descriptor>{}))>
inline constexpr auto apply(Args&&... args) /** @cond */ ->
decltype(make_placeholder<std::tuple<Descriptor, detail::template_pack<run_never>>, detail::apply_cell>(std::forward<Args>(args)...)) /** @endcond */
{ return make_placeholder<std::tuple<Descriptor, detail::template_pack<run_never>>, detail::apply_cell>(std::forward<Args>(args)...); }


template<template<class> class Runner, class Descriptor, class... RunnerArgs, class... Args, DFP_F_REQUIRES((detail::is_processing_descriptor<Descriptor>{}))>
inline constexpr auto apply(std::tuple<RunnerArgs...>&& runner_args_tuple, Args&&... args) /** @cond */ ->
decltype(detail::apply<Descriptor, Runner>(std::move(runner_args_tuple), std::index_sequence_for<RunnerArgs...>{}, std::forward<Args>(args)...)) /** @endcond */
{ return detail::apply<Descriptor, Runner>(std::move(runner_args_tuple), std::index_sequence_for<RunnerArgs...>{}, std::forward<Args>(args)...); }


/**
* @brief Creates a cell builder for a sample-based source or node whose processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments.
*
* @param callable_args is the function parameter pack to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the source or node.
* @tparam Args is a template parameter pack implicity deduced from the @c callable_args function parameter pack
*
* **Usage example**
* @snippet core_test.cpp sample-based "apply node" given a function-object type
* @snippet core_test.cpp sample-based "apply sink" given a function-object type
**/
template<class Callable, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply(Args&&... callable_args)
#if not DFP_CPP14_SUPPORT
->
decltype(apply<sample_based_processing<Callable>>(std::forward<Args>(callable_args)...))
#endif
{ return apply<sample_based_processing<Callable>>(std::forward<Args>(callable_args)...); }


/**
* @brief Creates a cell builder for a frame-based source or node whose processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments.
*
* @param callable_args is the function parameter pack to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the source or node.
* @tparam Args is a template parameter pack implicity deduced from the @c callable_args function parameter pack
*
* **Usage example**
* @snippet core_test.cpp frame-based "apply node & source" given a function-object type and lambda
**/
template<class Callable, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply_f(Args&&... callable_args) /** @cond */ ->
decltype(apply<frame_based_processing<Callable>>(std::forward<Args>(callable_args)...)) /** @endcond */
{ return apply<frame_based_processing<Callable>>(std::forward<Args>(callable_args)...); }


/**
* @brief Creates a cell for a sample-based source or node whose processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable applying the processing operation
*
* @tparam Callable type is implicitly deduced from the @c callable argument
**/
template<class Callable, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{} && !detail::is_cell_builder<Callable>{}))>
inline constexpr auto apply(Callable&& callable) /** @cond */ ->
decltype(make_placeholder<
    std::tuple<sample_based_processing<std::remove_reference_t<Callable>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::forward<Callable>(callable))) /** @endcond */
{ return make_placeholder<
    std::tuple<sample_based_processing<std::remove_reference_t<Callable>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::forward<Callable>(callable)); }


/**
* @brief Creates a cell for a sample-based source or node whose processing is defined by the given
* a C-function reference
*
* @param cfunc is the c-function applying the processing operation
*
* @tparam R type is implicitly deduced from the @c cfunc argument. It cannot be void.
* @tparam Args parameters pack are implicitly deduced from the @c cfunc argument.
* It shall be suitable to accept sample types delivered by upstream processings.
**/
template<class R, class... Args, DFP_F_REQUIRES((!std::is_void<R>{}))>
inline constexpr auto apply(R(&cfunc)(Args...))
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<
    std::tuple<sample_based_processing<std::function<R(Args...)>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::function<R(Args...)>{ cfunc }))
#endif
{ return make_placeholder<
    std::tuple<sample_based_processing<std::function<R(Args...)>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::function<R(Args...)>{ cfunc }); }


/**
* @brief Creates a cell builder for a frame-based source or node whose processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable applying the processing operation
*
* @tparam Callable type is implicitly deduced from the @c callable argument
*
* **Usage example**
* @snippet core_test.cpp frame-based "apply node & source" given a function-object type and lambda
**/
template<class Callable, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply_f(Callable&& callable) /** @cond */ ->
decltype(make_placeholder<
    std::tuple<frame_based_processing<std::remove_reference_t<Callable>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::forward<Callable>(callable))) /** @endcond */
{ return make_placeholder<
    std::tuple<frame_based_processing<std::remove_reference_t<Callable>>, detail::template_pack<run_never>>, detail::apply_cell
>(std::forward<Callable>(callable)); }


/**
* @brief Creates a sink builder whose the sample-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments
*
* @param args is the parameter pack to to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the sink
* @tparam Runner is a template class which models a sink @ref runner
* @tparam Args is a template parameter pack implicity deduced from the @c args function parameter pack
**/
//TODO: miss variant taking const copy ref of runner_args_tuple as input parameter 
//TODO: add proxy to allow parameter pack for runner_args_tuple, i.e. : apply(function_args...).run<Runner>(runner_args_tuple...)
//(runner defaulted to run_never)
template<template<class> class Runner, class Callable, class... RunnerArgs, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply(std::tuple<RunnerArgs...>&& runner_args_tuple, Args&&... args) /** @cond */ ->
decltype(apply<Runner, sample_based_processing<Callable>>(std::move(runner_args_tuple), std::forward<Args>(args)...)) /** @endcond */
{ return apply<Runner, sample_based_processing<Callable>>(std::move(runner_args_tuple), std::forward<Args>(args)...); }


/**
* @brief Creates a sink builder whose the frame-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments
*
* @param args is the parameter pack to to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the sink
* @tparam Runner is a template class which models a sink @ref runner
* @tparam Args is a template parameter pack implicity deduced from the @c args function parameter pack
**/
template<template<class> class Runner, class Callable, class... RunnerArgs, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply_f(std::tuple<RunnerArgs...>&& runner_args, Args&&... args) /** @cond */ ->
decltype(apply<Runner, frame_based_processing<Callable>>(std::move(runner_args), std::forward<Args>(args)...)) /** @endcond */
{ return apply<Runner, frame_based_processing<Callable>>(std::move(runner_args), std::forward<Args>(args)...); }


/**
* @brief Creates a sink builder whose the sample-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable to call to apply the processing operation
*
* @tparam Runner is a template class which models a sink @ref runner
* @tparam Callable type is implicitly deduced from the @c callable argument
**/
template<template<class> class Runner, class Callable, class... RunnerArgs, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply(Callable&& callable, RunnerArgs&&... runner_args) /** @cond */ ->
decltype(detail::apply<sample_based_processing<std::remove_reference_t<Callable>>, Runner>
    (runner_args_t<Runner>(runner_args...), std::index_sequence_for<RunnerArgs...>{}, std::forward<Callable>(callable))) /** @endcond */
{ return detail::apply<sample_based_processing<std::remove_reference_t<Callable>>, Runner>
    (runner_args_t<Runner>(runner_args...), std::index_sequence_for<RunnerArgs...>{}, std::forward<Callable>(callable)); }


/**
* @brief Creates a sink builder whose the frame-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable to call to apply the processing operation
*
* @tparam Runner is a template class which models a sink @ref runner
* @tparam Callable type is implicitly deduced from the @c callable argument
**/
template<template<class> class Runner, class Callable, class... RunnerArgs, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto apply_f(Callable&& callable, RunnerArgs&&... runner_args) /** @cond */ ->
decltype(detail::apply<frame_based_processing<std::remove_reference_t<Callable>>, Runner>
    (runner_args_t<Runner>(runner_args...), std::index_sequence_for<RunnerArgs...>{}, std::forward<Callable>(callable))) /** @endcond */
{ return detail::apply<frame_based_processing<std::remove_reference_t<Callable>>, Runner>
    (runner_args_t<Runner>(runner_args...), std::index_sequence_for<RunnerArgs...>{}, std::forward<Callable>(callable)); }


template<class Placeholder, DFP_F_REQUIRES((detail::is_cell_builder<Placeholder>{}))>
inline constexpr auto apply(Placeholder&& placeholder) noexcept /** @cond */ ->
decltype(std::forward<Placeholder>(placeholder)) /** @endcond */
{ return std::forward<Placeholder>(placeholder); }


///@} apply_grp
///@} core_grp


namespace shortname
{
    using core::apply;
    using core::apply_f;
}


} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_APPLY_HPP
