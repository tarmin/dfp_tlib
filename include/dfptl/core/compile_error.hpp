/**
* @file core/compile_error.hpp
*
* @brief Defines preprocessor macro for creation of error message raised at compile-time.
*
* This header file simplifies inclusion of DFPTL required header files for defining processing cell or processing branch
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_COMPILE_ERROR_HPP
#define DFPTL_CORE_COMPILE_ERROR_HPP


///@addtogroup core_grp
///@{
/** @defgroup cerror_grp compile error
* @brief Defines preprocessor macro for creation of usual error messages raised at compile-time.
**/
///@{


#define DFP_STRINGIFY(x) DFP_STRINGIFY_(x)
#define DFP_STRINGIFY_(y) #y

/**
* Reports some unexpected error. Support from DFPTL developers is likely required.
**/
#define DFP_UNEXPECTED_ERROR \
"Unexpected error. Please raise an issue to developers at https://gitlab.com/tarmin/dfp_tlib/-/issues/new"

/**
* Reports that cell @c C is fed with an invalid count of input streams.
**/
#define DFP_INVALID_INPUT_COUNT(C, E) \
DFP_STRINGIFY(C) " cell requires " DFP_STRINGIFY(E) " stream(s) in input."


/**
* Reports that cell @c C is fed with an invalid sample type on its input @c I.
**/
#define DFP_INVALID_INPUT_SAMPLE(C, E, I) \
DFP_STRINGIFY(C) " cell requires " DFP_STRINGIFY(E) " sample type on its input(s) " DFP_STRINGIFY(I) "."


/**
* Reports that cell @c C is fed with an invalid length of frame on its input @c I.
**/
#define DFP_INVALID_INPUT_FRAME_EXTENT(C, E, I) \
DFP_STRINGIFY(C) " cell requires frame extent " DFP_STRINGIFY(E) " on its input(s) " DFP_STRINGIFY(I) "."


/**
* Reports that cell @c C shall be fed with a @ref atomic_stream "atomic stream" on its input @c I.
**/
#define DFP_INPUT_NOT_ATOMIC_STREAM(C, I) \
DFP_STRINGIFY(C) " cell requires a atomic stream on its input(s) " DFP_STRINGIFY(I) ". Consider to insert a to_single_sample node on th(is/ese) input(s)."


///@} // cerror_grp
///@} // core_grp


#endif // DFPTL_CORE_COMPILE_ERROR_HPP
