/**
* @file core/execute.hpp
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_EXECUTE_HPP
#define DFPTL_CORE_EXECUTE_HPP


#include "dfptl/core/detail/execute_node.hpp"


namespace dfp
{
inline namespace core
{
/**
* @addtogroup core_grp
* @{
* @defgroup execute_grp "execute" node
* @brief the @ref execute_grp create processing node applying some action defined by [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
* but leaving stream unchanged.
*
* the @ref execute_grp is a sample-based (or frame-based for the `execute_f` variant) single input node applying some action each time
* the node is fired and forwarding without any modification the input stream on its output.
*
* @ref execute_grp is kind of specialization of @ref apply_grp "apply node" supporting only one single input and
* whose callable does not apply any modification on the stream.
*
* @note a `callable` which returns nothing (aka result type declared as `void`) at a node position in a processing graph will be implicitly
* converted as an @ref execute_grp. For example,
* @snippet core_test.cpp callable implicit conversion to 'execute' node
*
* @sa apply_grp
* @{
**/


/**
* @brief Creates a node builder for a sample-based or frame-based processing defined by the given
* @c Descriptor type and its initialization arguments.
*
* @param descriptor_args is the function parameters pack to later initialize the callable
*
* @tparam Descriptor is the descritpro class hosting the processing operation wrapped by the node.
* @tparam DescriptorArgs is a template parameter pack implicity deduced from the @c descriptor_args function parameters pack
*
* @note This overload of the @c execute() function is a bit advanced and is probably not intended for direct use.
**/
template<class Descriptor, class... DescriptorArgs, DFP_F_REQUIRES((detail::is_processing_descriptor<Descriptor>{}))>
inline constexpr auto execute(DescriptorArgs&&... descriptor_args)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<std::tuple<Descriptor>, detail::execute_node>(std::forward<DescriptorArgs>(descriptor_args)...))
#endif
{ return make_placeholder<std::tuple<Descriptor>, detail::execute_node>(std::forward<DescriptorArgs>(descriptor_args)...); }


/**
* @brief Creates a node builder for a sample-based processing defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments.
*
* @param callable_args is the function parameters pack to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the node.
* @tparam Args is a template parameter pack implicity deduced from the @c callable_args function parameters pack
*
* **Usage example**
* @snippet core_test.cpp sample-based "execute node" given a function-object type
**/
template<class Callable, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto execute(Args&&... callable_args)
#if not DFP_CPP14_SUPPORT
->
decltype(execute<sample_based_processing<Callable>>(std::forward<Args>(callable_args)...))
#endif
{ return execute<sample_based_processing<Callable>>(std::forward<Args>(callable_args)...); }


/**
* @brief Creates a node builder for a frame-based processing defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and its initialization arguments.
*
* @param callable_args is the function parameter pack to later initialize the callable
*
* @tparam Callable is the callable class hosting the processing operation wrapped by the node.
* @tparam Args is a template parameter pack implicity deduced from the @c callable_args function parameters pack
*
* **Usage example**
* @snippet core_test.cpp frame-based "execute node" given a function-object type
**/
template<class Callable, class... Args, DFP_F_REQUIRES((!detail::is_processing_descriptor<Callable>{}))>
inline constexpr auto execute_f(Args&&... callable_args)
#if not DFP_CPP14_SUPPORT
->
decltype(execute<frame_based_processing<Callable>>(std::forward<Args>(callable_args)...))
#endif
{ return execute<frame_based_processing<Callable>>(std::forward<Args>(callable_args)...); }


/**
* @brief Creates a node builder whose sample-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable applying the processing operation
*
* @tparam Callable type is implicitly deduced from the @c callable argument
*
* @snippet core_test.cpp sample-based 'execute node' given a lambda
**/
template<class Callable>
inline constexpr auto execute(Callable&& callable)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<
    std::tuple<sample_based_processing<std::remove_reference_t<Callable>>>, detail::execute_node
>(std::forward<Callable>(callable)))
#endif
{ return make_placeholder<
    std::tuple<sample_based_processing<std::remove_reference_t<Callable>>>, detail::execute_node
>(std::forward<Callable>(callable)); }


/**
* @brief Creates a node builder whose frame-based processing is defined by the given
* [callable](https://en.cppreference.com/w/cpp/named_req/Callable)
*
* @param callable is the callable applying the processing operation
*
* @tparam Callable type is implicitly deduced from the @c callable argument
*
* @snippet core_test.cpp frame-based 'execute node' given a lambada
**/
template<class Callable>
inline constexpr auto execute_f(Callable&& callable)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<
    std::tuple<frame_based_processing<std::remove_reference_t<Callable>>>, detail::execute_node
>(std::forward<Callable>(callable)))
#endif
{ return make_placeholder<
    std::tuple<frame_based_processing<std::remove_reference_t<Callable>>>, detail::execute_node
>(std::forward<Callable>(callable)); }


///@} execute_grp
///@} core_grp


namespace shortname
{
inline namespace nodes
{
///@addtogroup execute_grp
///@{
/**
* @brief short alias for @ref execute(Callable&&)
**/
template<class Callable>
inline constexpr auto exec(Callable&& callable)
#if not DFP_CPP14_SUPPORT
->
decltype(core::execute(std::forward<Callable>(callable)))
#endif
{ return core::execute(std::forward<Callable>(callable)); }


/**
* @brief short alias for @ref execute<Callable>(Args&&... callable_args)
**/
template<class Callable, class... Args>
inline constexpr auto exec(Args&&... callable_args)
#if not DFP_CPP14_SUPPORT
->
decltype(core::execute<Callable>(std::forward<Args>(callable_args)...))
#endif
{ return core::execute<Callable>(std::forward<Args>(callable_args)...); }


/**
* @brief short alias for @ref execute_f(Callable&&)
**/
template<class Callable>
inline constexpr auto exec_f(Callable&& callable)
#if not DFP_CPP14_SUPPORT
->
decltype(core::execute_f(std::forward<Callable>(callable)))
#endif
{ return core::execute_f(std::forward<Callable>(callable)); }


/**
* @brief short alias for @ref execute_f<Callable>(Args&&... callable_args)
**/
template<class Callable, class... Args>
inline constexpr auto exec_f(Args&&... callable_args)
#if not DFP_CPP14_SUPPORT
->
decltype(core::execute_f<Callable>(std::forward<Args>(callable_args)...))
#endif
{ return core::execute_f<Callable>(std::forward<Args>(callable_args)...); }


///@} execute_grp
} // namespace nodes
}


} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_EXECUTE_HPP
