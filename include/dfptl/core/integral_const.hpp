/**
* @file core/integral_const.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_INTEGRAL_CONST_HPP
#define DFPTL_CORE_INTEGRAL_CONST_HPP


#include "stl/type_traits.hpp"
#include "stl/experimental/type_traits.hpp"

#include <cstdint>
#include <limits>


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
///@defgroup integral_const_grp integral-constant helpers
///@{


template<class T, T Value>
struct integral_const_base;


/**
* @brief integral constant with arithmetic operators support.
**/
template<class T, T Value, class = void>
struct integral_const : integral_const_base<T, Value>{};


template<class T, T Value>
struct integral_const<T, Value, DFP_C_REQUIRES((!std::is_enum<T>{} && !std::is_same<T, bool>{}))>  : integral_const_base<T, Value>
{
    constexpr integral_const<T, -Value> operator-() const noexcept
    { return integral_const<T, -Value>(); }

    constexpr auto operator!() const noexcept ->
    integral_const<T, !Value>
    { return integral_const<T, !Value>(); }
}; // struct integral_const<... !is_bool<T> && !std::enum<T> ...>


template<class T, T Value>
struct integral_const<T, Value, DFP_C_REQUIRES((std::is_same<T, bool>{}))>  : integral_const_base<T, Value>
{
    constexpr auto operator!() const noexcept ->
    integral_const<T, !Value>
    { return integral_const<T, !Value>(); }
}; // struct integral_const<... is_bool<T> ...>


template<class T, T Value>
struct integral_const_base : std::integral_constant<T, Value>
{
    template <T Rhs>
    constexpr auto operator+(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value + decltype(rhs)::value>
    { return integral_const<T, Value + Rhs>(); }

    template <T Rhs>
    constexpr auto operator*(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value *  decltype(rhs)::value>
    { return integral_const<T, Value * Rhs>(); }

    template <T Rhs>
    constexpr auto operator-(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value -  decltype(rhs)::value>
    { return integral_const<T, Value - Rhs>(); }

    template <T Rhs>
    constexpr auto operator/(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value /  decltype(rhs)::value>
    { return integral_const<T, Value / Rhs>(); }

    template <T Rhs>
    constexpr auto operator==(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value == decltype(rhs)::value>
    { return integral_const<T, Value == Rhs>(); }

    template <T Rhs>
    constexpr auto operator!=(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value != decltype(rhs)::value>
    { return integral_const<T, Value != Rhs>(); }

    template <T Rhs>
    constexpr auto operator&&(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value && decltype(rhs)::value>
    { return integral_const<T, Value && Rhs>(); }

    template <T Rhs>
    constexpr auto operator||(std::integral_constant<T, Rhs> rhs) const noexcept ->
    integral_const<T, Value || decltype(rhs)::value>
    { return integral_const<T, Value || Rhs>(); }

    template<class To, DFP_F_REQUIRES((std::is_convertible<T, To>{}))>
    constexpr operator integral_const<To, Value>() const
    {
        static_assert(Value <= std::numeric_limits<To>::max(), "Value is above max value of the destination type");
        static_assert(Value >= std::numeric_limits<To>::min(), "Value is below min value of the destination type");
        return integral_const<To, Value>{};
    }
};


/**
* @brief Alias of @c std::integral_constant<int, Value>
**/
template<int Value>
using int_const = integral_const<int, Value>;


/**
* @brief Alias of @c std::integral_constant<long long, Value>
**/
template<long long Value>
using llong_const = integral_const<long long, Value>;


/**
* @brief Alias of @c std::integral_constant<std::size_t, Value>
**/
template<std::size_t Value>
using size_t_const = integral_const<std::size_t, Value>;


/**
* @brief Alias of @c std::integral_constant<std::uint16_t, Value>
**/
template<std::uint16_t Value>
using uint16_t_const = integral_const<std::uint16_t, Value>;


/**
* @brief Alias of @c std::integral_constant<std::uint8_t, Value>
**/
template<std::uint8_t Value>
using uint8_t_const = integral_const<std::uint8_t, Value>;


/**
* @brief Alias of @c std::integral_constant<bool, Value>
**/
template<bool Value>
using bool_const = integral_const<bool, Value>;

DFP_INLINE_CONSTEXPR bool_const<true> true_c = bool_const<true>{};
DFP_INLINE_CONSTEXPR bool_const<false> false_c = bool_const<false>{};

/**
* @brief If @c T is std::size_t type
* provides the member constant @c value equal to true. Otherwise value is false.
*
* @tparam T is the type to test.
**/
template<class T, class Enable = void>
struct is_size_t_const : std::false_type {};

template<class T>
struct is_size_t_const<T, typename std::enable_if<std::is_same<size_t_const<T::value>, std::remove_const_t<T>>{}>::type> : std::true_type {};


/**
* @brief makes enum integral_const<T, Value> type for a given integral @c Value of type @c T
**/
template<class T, T Value>
inline constexpr integral_const<T, Value> mec(T = Value);


template<char F, class Acc>
constexpr auto parse_ic_digits(Acc acc) ->
decltype(acc*int_const<10>{} + int_const<F-'0'>{})
{ return acc*int_const<10>{} + int_const<F-'0'>{}; }

template<char F, char... O, class Acc, DFP_F_REQUIRES((sizeof...(O) > 0))>
constexpr auto parse_ic_digits(Acc acc) ->
decltype(parse_ic_digits<O...>(acc*int_const<10>{} + int_const<F-'0'>{}))
{ return parse_ic_digits<O...>(acc*int_const<10>{} + int_const<F-'0'>{}); }

template<char... Digits>
constexpr auto operator"" _c() ->
decltype(parse_ic_digits<Digits...>(int_const<0>{}))
{ return parse_ic_digits<Digits...>(int_const<0>{}); }


template<char F, class Acc>
constexpr auto parse_uc_digits(Acc acc) ->
decltype(acc*size_t_const<10>{} + size_t_const<F-'0'>{})
{ return acc*size_t_const<10>{} + size_t_const<F-'0'>{}; }

template<char F, char... O, class Acc, DFP_F_REQUIRES((sizeof...(O) > 0))>
constexpr auto parse_uc_digits(Acc acc) ->
decltype(parse_uc_digits<O...>(acc*size_t_const<10>{} + size_t_const<F-'0'>{}))
{ return parse_uc_digits<O...>(acc*size_t_const<10>{} + size_t_const<F-'0'>{}); }

template<char... Digits>
constexpr auto operator"" _uc() ->
decltype(parse_uc_digits<Digits...>(size_t_const<0>{}))
{ return parse_uc_digits<Digits...>(size_t_const<0>{}); }


template<class V0, class V1>
constexpr auto pack_add(V0 v0, V1 v1) ->
decltype(v0 + v1)
{ return v0 + v1; }


template<class V0>
constexpr auto pack_add(V0 v0) ->
decltype(v0)
{ return v0; }


template<class V0, class... Values>
constexpr auto pack_add(V0 v0, Values... values) ->
decltype(v0 + pack_add(values...))
{ return v0 + pack_add(values...); }


template<class V0>
constexpr V0 pack_and(V0 v0)
{ return v0; }


#if !DFP_CPP14_SUPPORT
template<class V0, class V1>
constexpr auto pack_and(V0 v0, V1 v1) ->
decltype(v0 && v1)
{ return v0 && v1; }

template<class V0, class V1, class V2>
constexpr auto pack_and(V0 v0, V1 v1, V2 v2) ->
decltype(pack_and(v0, pack_and(v1, v2)))
{ return pack_and(v0, pack_and(v1, v2)); }
#endif


template<class V0, class... Values>
constexpr auto pack_and(V0 v0, Values... values)
#if !DFP_CPP14_SUPPORT
->
decltype(v0 && pack_and(values...))
#endif
{ return v0 && pack_and(values...); }


template<class V0, class V1>
constexpr auto pack_or(V0 v0, V1 v1) ->
decltype(v0 || v1)
{ return v0 || v1; }


template<class V0>
constexpr auto pack_or(V0 v0) ->
decltype(v0 || v0)
{ return v0 || v0; }


template<class V0, class... Values>
constexpr auto pack_or(V0 v0, Values... values) ->
decltype(v0 || pack_or(values...))
{ return v0 || pack_or(values...); }


template<class Ref, class V0>
constexpr auto pack_equal(Ref ref, V0 v0) ->
decltype(ref == v0)
{ return ref == v0; }


template<class Ref, class V0, class... Values>
constexpr auto pack_equal(Ref ref, V0 v0, Values... values) ->
decltype((ref == v0) && pack_equal(ref, values...))
{ return (ref == v0) && pack_equal(ref, values...); }


template<class V0>
constexpr auto pack_equal(V0 v0) ->
decltype(v0 == v0)
{ return v0 == v0; }


template<class Then, class Else>
std::conditional_t<std::is_convertible<Else, Then>{}, Then, Else>
static_if(bool condition, Then then, Else otherwise)
{ return condition ? then : otherwise; }


template<bool Condition, class Then, class Else, DFP_F_REQUIRES((Condition))>
Then static_if(std::bool_constant<Condition>, Then then, Else)
{ return then; }


template<bool Condition, class Then, class Else, DFP_F_REQUIRES((!Condition))>
Else static_if(std::bool_constant<Condition>, Then, Else otherwise)
{ return otherwise; }


///@} integral_const_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_INTEGRAL_CONST_HPP
