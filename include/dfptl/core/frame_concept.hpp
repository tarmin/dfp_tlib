/**
* @file core/frame_concept.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_FRAME_CONCEPT_HPP
#define DFPTL_CORE_FRAME_CONCEPT_HPP


#include "stl/type_traits.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/**
* @defgroup frame_concept_grp frame concept
* @brief Specifies static interface that any frame type shall expose.
*
* Frame in DFP_TLIB is nothing else than a [STD container](https://en.cppreference.com/w/cpp/named_req/Container) storing samples.
* However frame types are not required to model entirely the [STD container](https://en.cppreference.com/w/cpp/named_req/Container) concept.
*
* The @c frame_concept class is mainly introduced for documentation purpose.
* It aims at presenting the minimal set of required operations and type or typedef exposed by any frame types.
*
* @remarks Here are some key differences between @ref frame_concept_grp and [STD container](https://en.cppreference.com/w/cpp/named_req/Container)
* * default-constructible is not a requirement for frames
* * default constructor does not neccessary imply that frame is empty right after construction
* * providing some input iterator is enough for frames
* * equality-comparable is not a requirement for frames
* * swappable is not a requirement for frames
* * providing a @c max_size() method is not a requirement for frames
* * providing @c value_type, @c reference_type, @c const_reference_type and @c difference_type types are not requirements for frames
* * if frame type is intended to be conveyed by a @ref stream_grp with static frame extent then the @c tuple_size<> helper must be specialized for it
* * if frame type is intended to be conveyed by a @ref stream_grp with dynamic frame extent then it shall expose an explicit specific constructor
* taking the frame size as unique parameter.
* @{
**/


/**
* @brief Defines the minimal set of operations and typedef exposed by any types modeling the frame concept.
*
* @tparam ActualFrame when implementing a frame class from scratch the frame class can be made inheriting from
* the frame_concept to highlight implementation requirements. If so the frame class shall be passed as the @c ActualFrame
* template parameter of frame_concept.
**/
template<class ActualFrame>
class frame_concept
{
    struct value_type_ {};

    struct size_type_
    { static_assert(!std::is_same<typename ActualFrame::size_type, size_type_>{},
        "size_type typedef shall be defined in ActualFrame type"
    ); };

    struct iterator_
    { static_assert(!std::is_same<typename ActualFrame::iterator, iterator_>{},
        "iterator typedef shall be defined in ActualFrame type"
    ); value_type_& operator*(); };

    struct const_iterator_
    { static_assert(!std::is_same<typename ActualFrame::const_iterator, const_iterator_>{},
        "const_iterator typedef shall be defined in ActualFrame type"
    ); value_type_& operator*(); };

public:
    /** @brief @c ActualFrame type shall provide a @c size_type inner type or typedef **/ 
    typedef size_type_      size_type;
    /** @brief @c ActualFrame type shall provide a @c iterator inner type or typedef **/ 
    typedef iterator_       iterator;
    /** @brief @c ActualFrame type shall provide a @c const_iterator inner type or typedef **/ 
    typedef const_iterator_ const_iterator;

    /**
    * @brief @c ActualFrame type shall provide a default constructor to initialize frame with correct size if conveyed by
    * @ref stream_grp with static frame extent or with null size otherwise.
    **/
    constexpr frame_concept() noexcept {}

    /** @brief @c ActualFrame type shall be [copy-constructible](https://en.cppreference.com/w/cpp/named_req/CopyConstructible) **/ 
    constexpr frame_concept(frame_concept const&) noexcept
#if defined(__clang_major__) && (__clang_major__ <= 3)
    //workaround for invalid use of incomplete type issue
    {}
#else
    { static_assert(std::is_copy_constructible<ActualFrame>{},
        "ActualFrame shall be copy-constructible"
    ); }
#endif

    /** @brief @c ActualFrame type shall be [move-constructible](https://en.cppreference.com/w/cpp/named_req/MoveConstructible) **/ 
    constexpr frame_concept(frame_concept&&) noexcept
#if defined(__clang_major__) && (__clang_major__ <= 3)
    //workaround for invalid use of incomplete type issue
    {}
#else
    { static_assert(std::is_move_constructible<ActualFrame>{},
        "ActualFrame shall be move-constructible"
    ); }
#endif

    /** @brief @c ActualFrame type shall be [copy-assignable](https://en.cppreference.com/w/cpp/named_req/CopyAssignable) **/ 
    frame_concept& operator=(frame_concept const&) noexcept
#if defined(__clang_major__) && (__clang_major__ <= 3)
    //workaround for invalid use of incomplete type issue
    { return *this; }
#else
    { static_assert(std::is_copy_assignable<ActualFrame>{},
        "ActualFrame shall be copy assignable"
    ); return *this; }
#endif

    /**
    * @brief @c ActualFrame type shall provide a dedicated constructor to initialize frame with correct size if conveyed by
    *
    * @remarks only required for frame conveyed by @ref stream_grp with dynamic frame extent
    **/
    explicit frame_concept(std::size_t size) noexcept;

    /** @brief ActualFrame type shall provide a @c size() method returning item count stored in frame **/
    constexpr size_type             size() const noexcept
    { static_assert(&ActualFrame::size != &frame_concept::size,
        "size() shall be implemented in ActualFrame type"
    ); return std::declval<size_type>(); }

    /**
    * @brief ActualFrame type shall provide a @c cbegin() method returning an iterator to beginning.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
    constexpr const_iterator        cbegin() const noexcept
    { static_assert(&ActualFrame::cbegin != &frame_concept::cbegin,
        "cbegin() shall be implemented in ActualFrame type"
    ); return std::declval<const_iterator>(); }

    /**
    * @brief ActualFrame type shall provide a @c cend() method returning an iterator to end.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
    constexpr const_iterator        cend() const noexcept
    { static_assert(&ActualFrame::cend != &frame_concept::cend,
        "cend() shall be implemented in ActualFrame type"
    ); return std::declval<const_iterator>(); }

    /**
    * @brief ActualFrame type shall provide a @c begin() method returning an iterator to beginning.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
    DFP_MUTABLE_CONSTEXPR iterator  begin() noexcept
    { return std::declval<iterator>(); }

    /**
    * @brief ActualFrame type shall provide a @c begin() method returning an iterator to beginning.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
    constexpr const_iterator        begin() const noexcept
    { return std::declval<const_iterator>(); }

    /**
    * @brief ActualFrame type shall provide a @c end() method returning an iterator to end.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
   DFP_MUTABLE_CONSTEXPR iterator   end() noexcept
    { return std::declval<iterator>(); }

    /**
    * @brief ActualFrame type shall provide a @c end() method returning an iterator to end.
    *
    * @return iterator shall at least model the [input iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator) concept.
    **/
    constexpr const_iterator        end() const noexcept
    { return std::declval<const_iterator>(); }

    /** @brief ActualFrame type shall provide an @c empty() method indicating if at least one sample has been stored **/
    constexpr bool                  empty() const noexcept
    { static_assert(&ActualFrame::empty != &frame_concept::empty,
        "empty() shall be implemented in ActualFrame type"
    ); return true; }
}; //class frame_concept


/**
* @brief Gets length of a sample frame contained in a `std::array`
**/
template<class T, std::size_t N>
constexpr size_t_const<N> get_length(std::array<T, N>) noexcept
{ return size_t_const<N>{}; }

/**
* @brief update length of a sample frame contained in a `std::array`
*
* @note This function actually does nothing as std::array is not resizeable but as long as
* the given length is same as the current array length, it helps to write generic source code 
**/
template<class T, std::size_t N>
constexpr std::array<T, N>& update_length(std::array<T, N>& it, size_t_const<N>) noexcept
{ return it; }

/**
* @brief Gets length of a sample frame contained in a `std::span` (without dynamic extent)
**/
template<class T, std::size_t N, DFP_F_REQUIRES((N != DYNAMIC_EXTENT))>
constexpr size_t_const<N> get_length(upg::span<T, N>) noexcept
{ return size_t_const<N>{}; }

/**
* @brief update length of a sample frame contained in a `std::span` (without dynamic extent)
*
* @note This function actually does nothing as std::span without dynamic extent is not resizeable but as long as
* the given length is same as the current array length, it helps to write generic source code 
**/
template<class T, std::size_t N>
constexpr upg::span<T, N>& update_length(upg::span<T, N>& it, size_t_const<N>) noexcept
{ return it; }

/**
* @brief Fallabck method to get length of a sample frame
*
* @note will fail at compile time if underlying container does not support size() method
**/
template<class T>
constexpr std::size_t get_length(T const& container) noexcept
{ return container.size(); }

/**
* @brief Fallabck method to update length of a sample frame
*
* @note will fail at compile time if underlying container does not support resize() method
**/
template<class T>
T& update_length(T& it, std::size_t new_length)
{ it.resize(new_length); return it; }



///@} frame_concept_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_FRAME_CONCEPT_HPP
