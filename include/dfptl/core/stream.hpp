/**
* @file core/stream.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_STREAM_HPP
#define DFPTL_CORE_STREAM_HPP


#include "dfptl/core/detail/stream.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup stream_grp processing stream
* @brief Processing streams are produced on each output port of processing cells in a graph and consumed on their input ports.
*
* Samples frame resulting from a processing branch can be retrieved through processing stream.
* Processing stream has several traits described through a @ref stream_specifier_grp and is bound to its producing cell.
* * @anchor static_stream **static stream** is a peculiar stream whose frame length is static (known at compile-time)
* * @anchor atomic_stream **atomic stream** is a peculiar static stream with frame length set to 1.
* @{
**/


/**
* @brief The processing stream class
*
* @tparam Specifier holds traits of the stream
* @tparam Emitter The cell producing this stream on its output
**/
template<class Specifier, class Emitter>
class stream :  detail::stream<Specifier, Emitter>
{
    typedef detail::stream<Specifier, Emitter> impl;

public:
    typedef typename impl::specifier_type   specifier_type;
    typedef typename impl::emitter_type     emitter_type;
    typedef typename impl::tag_map          tag_map;
    typedef typename impl::dfp_cell_tag     dfp_cell_tag;     
    /**
    * @brief Provides member typedef @c type as the stream type associated with the given @c StreamTag.
    *
    * The retrieved stream can be somewhere in upstream part of the processing graph where this stream is emitted
    */
    template<class StreamTag>
    using at_tag    = typename impl::template at_tag<StreamTag>;
    /**
    * @brief alias for `typename at_tag<StreamTag>::type`
    **/
    template<class StreamTag>
    using at_tag_t  = typename at_tag<StreamTag>::type;
    /**
    * @brief Is same as [std::true_type](https://en.cppreference.com/w/cpp/types/integral_constant) if the given @c StreamTag
    * has been associated to this stream or somewhere in upstream.
    *
    * Otherwise is same as [std::false_type](https://en.cppreference.com/w/cpp/types/integral_constant)
    **/
    template<class StreamTag>
    using has_tag   = typename impl::template has_tag<StreamTag>;

    /** @brief Is worth length of the frame conveyed by the stream if static (known at compile-time) otherwise @ref DYNAMIC_EXTENT */
    static constexpr auto FRAME_EXTENT    = impl::FRAME_EXTENT;

    /** @brief Indicates if length of the frame conveyed by the stream is static and set to 1 */
    static constexpr bool IS_ATOMIC         = impl::IS_ATOMIC;

    /** @brief Provides const flavour of stream type */
    struct to_const
    { typedef stream<Specifier, detail::constify_t<Emitter>> const type; };

    /** @brief Creates a new instance of stream given the producing cell */
    explicit constexpr stream(Emitter& emitter) : impl(emitter) {}

    /**
    * @brief Provides a new sample frame produced by the @c Emitter
    **/
    template<class T = emitter_type>
    auto DFP_MUTABLE_CONSTEXPR drain() /** @cond */ ->
    decltype(this->impl::template drain<T>()) /** @endcond */
    { return this->impl::template drain<T>(); }

    /**
    * @copybrief drain()
    *
    * const flavour
    **/
    template<class T = emitter_type>
    auto constexpr drain() const /** @cond */ ->
    decltype(this->impl::template drain<T>()) /** @endcond */
    { return this->impl::template drain<T>(); }

    /**
    * @copybrief drain()
    *
    * const flavour of drain() to enforce usage of stream instance as immutable object.
    * (workaround for C++11 implementation which does not support non-const constexpr method)
    **/
    template<class T = emitter_type>
    auto constexpr cdrain() const /** @cond */ ->
    decltype(this->impl::template cdrain<T>()) /** @endcond */
    { return this->impl::template cdrain<T>(); }
}; // class stream


///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_STREAM_HPP
