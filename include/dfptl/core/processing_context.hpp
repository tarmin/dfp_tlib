/**
* @file core/processing_context.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PROCESSING_CONTEXT_HPP
#define DFPTL_CORE_PROCESSING_CONTEXT_HPP


#include "dfptl/core/detail/processing_context.hpp"
#include "dfptl/core/utility.hpp"

#include "stl/utility.hpp"


namespace dfp
{
inline namespace core
{


//TODO: processing_context and processing_graph concept should be merged as they both can provide current state of the processing graph
// in input of a processing cell.
// However for backward compatibility consideration, processing_context shall (still) be implicitly convertible into tuple of (input) cells
// Therefore, merging these both concept is postponed.
/**
* @brief Container for processing cells that can then be linked to another cell.
**/
template<class Tag = void, class... UpstreamCells>
class processing_context
 :  public std::tuple<UpstreamCells...>, private detail::processing_context<UpstreamCells...>
{
    // UpstreamCells are either value type of lvalue-reference type but never rvalue-reference type
    static_assert(!std::disjunction<std::is_rvalue_reference<UpstreamCells>...>{}, DFP_UNEXPECTED_ERROR);

    //TODO: static_assert(is_processing_cell<UpstreamCells..., DFP_UNEXPECTED_ERROR>{});
    typedef detail::processing_context<UpstreamCells...> impl;

    template<class CellIndexSequence>
    struct reverse_;

    template<std::size_t... CellIndexes>
    struct reverse_<std::index_sequence<CellIndexes...>>
    { typedef processing_context<Tag, std::tuple_element_t<sizeof...(UpstreamCells) - 1 - CellIndexes, std::tuple<UpstreamCells...>>...> type; };

public:
    typedef context_tag dfp_tag;
    typedef Tag cell_tag;
    typedef std::tuple<UpstreamCells...> upstreams_tuple;
    typedef typename impl::upstream_tag_map upstream_tag_map;
    typedef typename index_sequence_from<upstreams_tuple>::type upstream_index_sequence_type;
    typedef typename reverse_<index_sequence_from_t<std::tuple<UpstreamCells...>>>::type reverse_type;

    /**
    * @brief Provides member typedef @c type as the upstream cell type associated with the given @c RequestedTag.
    *
    * The retrieved upstream cell can be somewhere in upstream part of the processing graph where this stream is emitted
    */
    template<class RequestedTag>
    using at_tag    = typename impl::template at_tag<RequestedTag>;
    /**
    * @brief alias for `typename at_tag<RequestedTag>::type`
    **/
    template<class RequestedTag>
    using at_tag_t  = typename at_tag<RequestedTag>::type;
    /**
    * @brief Is same as [std::true_type](https://en.cppreference.com/w/cpp/types/integral_constant) if the given @c RequestedTag
    * has been associated to some cell of the upstream part.
    *
    * Otherwise is same as [std::false_type](https://en.cppreference.com/w/cpp/types/integral_constant)
    **/
    template<class RequestedTag>
    using has_tag   = typename impl::template has_tag<RequestedTag>;

   /** @brief Helper class to specify the tag at compile-time */
    template<class NewTag>
    struct set_tag
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewTag>
    using set_tag_t = typename set_tag<NewTag>::type;


    static constexpr std::size_t UPSTREAM_COUNT = sizeof...(UpstreamCells);

    using std::tuple<UpstreamCells...>::tuple;

    constexpr processing_context(std::tuple<UpstreamCells...> const& a)
     :  std::tuple<UpstreamCells...>::tuple(a)
    {}

    constexpr processing_context(std::tuple<UpstreamCells...>&& a)
     :  std::tuple<UpstreamCells...>::tuple(std::move(a))
    {}

    constexpr processing_context() = default;

    //TODO: to be specialized for CPP14 and beyond
    template<std::size_t I>
    DFP_MUTABLE_CONSTEXPR std::tuple_element_t<I, upstreams_tuple>& get_upstream()
    { return std::get<I>(*this); }

    template<std::size_t I>
    constexpr std::tuple_element_t<I, upstreams_tuple> const& get_upstream() const
    { return std::get<I>(*this); }
}; // class processing_context


typedef processing_context<> initial_context;


template<std::size_t I, class ProcessingContext>
struct context_upstream
{ typedef std::tuple_element_t<I, typename ProcessingContext::upstreams_tuple> type; };

template<std::size_t I, class ProcessingContext>
using context_upstream_t = typename context_upstream<I, ProcessingContext>::type;

template<class Tag, class... UpstreamCells>
template<class NewTag>
struct processing_context<Tag, UpstreamCells...>
    ::set_tag<NewTag>::update
{ typedef processing_context<NewTag, UpstreamCells...> type; };


} // namespace core
} // namespace dfp


namespace std
{


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmismatched-tags"
#endif


template<size_t I, class Tag, class... Types>
class tuple_element<I, dfp::core::processing_context<Tag, Types...>>
 :  public tuple_element<I, typename dfp::core::processing_context<Tag, Types...>::upstreams_tuple>
{};

template<class Tag, class... Types>
class tuple_size<dfp::core::processing_context<Tag, Types...>>
 :  public tuple_size<typename dfp::core::processing_context<Tag, Types...>::upstreams_tuple>
{};


#ifdef __clang__
#pragma clang diagnostic pop
#endif


} // namespace std


#endif //DFPTL_CORE_PROCESSING_CONTEXT_HPP
