/**
* @file core/cpluscplus.hpp
* re-define some C++ keyword for backward language compatibility and compiler issue workarounds 
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CPLUSPLUS_HPP
#define DFPTL_CORE_CPLUSPLUS_HPP


// GCC looks to have plenty of (unresolved) issues in constexpr handling with static_cast
// especially https://gcc.gnu.org/bugzilla/show_bug.cgi?id=97752 prevents efficient constexpr support when implementing CRTP
#define DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST 0
#if !defined(__clang__) && defined(__GNUC__)
#if (__GNUC__ <= 11) // most annoying issues have been solved since gcc12
    #undef  DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST
    #define DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST 1
#endif
#endif


// older GCC may crash when encountering constexpr
#define DFP_CONSTEXPR_MAY_CRASH_COMPILER 0
#if !defined(__clang__) && defined(__GNUC__)
#if (__GNUC__ <= 4)
    #undef  DFP_CONSTEXPR_MAY_CRASH_COMPILER
    #define DFP_CONSTEXPR_MAY_CRASH_COMPILER 1
#endif
#endif


#if (DFP_CONSTEXPR_VOID_SUPPORT && !DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST && !DFP_CONSTEXPR_MAY_CRASH_COMPILER)
    #define DFP_TREE_CONSTEXPR_SUPPORT 1
#else
    #define DFP_TREE_CONSTEXPR_SUPPORT 0
#endif

// GCC issue : https://gcc.gnu.org/bugzilla/show_bug.cgi?id=61806
#ifndef DFP_SFINAE_FAILURE_IN_PARTIAL_SPECIALIZATION_WO_ACCESS
#define DFP_SFINAE_FAILURE_IN_PARTIAL_SPECIALIZATION_WO_ACCESS 0
#if !defined(__clang__) && defined(__GNUC__)
#if (__GNUC__ < 8) || ((__GNUC__ == 8) && (__GNUC_MINOR__ < 2))
    #undef  DFP_SFINAE_FAILURE_IN_PARTIAL_SPECIALIZATION_WO_ACCESS
    #define DFP_SFINAE_FAILURE_IN_PARTIAL_SPECIALIZATION_WO_ACCESS 1
#endif
#endif
#endif


#ifndef DFP_EBCO_BADLY_SUPPORTED_WITH_MULTIPLE_BASES
#define DFP_EBCO_BADLY_SUPPORTED_WITH_MULTIPLE_BASES 0
#if !defined(__clang__) && defined(__GNUC__)
#if (__GNUC__ <= 10)
    #undef  DFP_EBCO_BADLY_SUPPORTED_WITH_MULTIPLE_BASES
    #define DFP_EBCO_BADLY_SUPPORTED_WITH_MULTIPLE_BASES 1
#endif
#endif
#endif


#ifndef DFP_SFINAE_FAILURE_WITH_AUTO
#define DFP_SFINAE_FAILURE_WITH_AUTO 0
#if defined(__clang__) || defined(__GNUC__)
#if (!defined(__clang__) && (__GNUC__ <= 4)) || (defined(__clang__) && (__clang_major__ <= 3))
    #undef  DFP_SFINAE_FAILURE_WITH_AUTO
    #define DFP_SFINAE_FAILURE_WITH_AUTO 1
#endif
#endif
#endif

#ifndef DFP_BASE_MEMBER_VISIBILITY_ISSUE_IN_METHOD_PROTOTYPE
#define DFP_BASE_MEMBER_VISIBILITY_ISSUE_IN_METHOD_PROTOTYPE 0
#if defined(__GNUC__)
#if (!defined(__clang__) && (__GNUC__ <= 9))
    #undef  DFP_BASE_MEMBER_VISIBILITY_ISSUE_IN_METHOD_PROTOTYPE
    #define DFP_BASE_MEMBER_VISIBILITY_ISSUE_IN_METHOD_PROTOTYPE 1
#endif
#endif
#endif


#ifndef DFP_INFERENCE_FAILURE_WITH_THIS
#define DFP_INFERENCE_FAILURE_WITH_THIS 0
#if defined(__clang__)
#if (__clang_major__ <= 3)
    #undef  DFP_INFERENCE_FAILURE_WITH_THIS
    #define DFP_INFERENCE_FAILURE_WITH_THIS 1
#endif
#endif
#endif


#if (__cplusplus >= 201703L)
    #define DFP_CPP17_SUPPORT 1
#else
    #define DFP_CPP17_SUPPORT 0
#endif


#if (__cplusplus >= 201402L)
    #define DFP_CPP14_SUPPORT 1
#else
    #define DFP_CPP14_SUPPORT 0
#endif


#if (__cplusplus >= 201103L)
    #define DFP_CPP11_SUPPORT 1
#else
    #define DFP_CPP11_SUPPORT 0
#endif


#if DFP_CPP17_SUPPORT
    #define DFP_INLINE_CONSTEXPR_SUPPORT    1
#endif


#if DFP_CPP14_SUPPORT
    #define DFP_MUTABLE_CONSTEXPR_SUPPORT   1
#endif

#if DFP_CPP14_SUPPORT
    #define DFP_CONSTEXPR_VOID_SUPPORT      1
#endif

#ifndef DFP_INLINE_CONSTEXPR_SUPPORT
    #define DFP_INLINE_CONSTEXPR_SUPPORT    0
#endif
#ifndef DFP_MUTABLE_CONSTEXPR_SUPPORT
    #define DFP_MUTABLE_CONSTEXPR_SUPPORT   0
#endif
#ifndef DFP_CONSTEXPR_VOID_SUPPORT
    #define DFP_CONSTEXPR_VOID_SUPPORT   0
#endif


#define _DFP_IF_0(DISCARDED,KEPT) KEPT
#define _DFP_IF_NOT_0(KEPT,DISCARDED) KEPT
#define _DFP_IF_1(KEPT,DISCARDED) KEPT
#define _DFP_IF_NOT_1(DISCARDED,KEPT)
#define _DFP_IF_X(V) _DFP_IF_ ## V
#define _DFP_IF_NOT_X(V) _DFP_IF_NOT_ ## V
#define _DFP_IF_EVAL(V) _DFP_IF_X(V)
#define DFP_IF(V) _DFP_IF_EVAL(V)
#define _DFP_IF_NOT_EVAL(V) _DFP_IF_NOT_X(V)
#define DFP_IF_NOT(V) _DFP_IF_NOT_EVAL(V)


/**
* @def DFP_SINCE_CPP17
* @deprecated
**/
//TODO: removed
#define DFP_SINCE_CPP17(x) DFP_IF(DFP_CPP17_SUPPORT)(x,)


///@addtogroup core_grp
///@{
/** @defgroup type_traits type traits
* @brief Extends STL type traits
* @{
**/
/**
* @def DFP_F_REQUIRES(x)
* @brief Helper macro for "enable_if" idiom applied to template function
*
* @param x is the boolean value to test in order to select the template function
*
* @code{.cpp}
* // Following example shows how to specialize foo template function in case given T template argument
* // is an arithmetic type.
* template<class T, DFP_F_REQUIRES(std::is_arithmetic<T>::value)>
* void foo(T t){...}
* @endcode
**/
#define DFP_F_REQUIRES(x) typename std::enable_if<x, int>::type = 0
/**
* @brief Helper macro for "enable_if" idiom applied to template class
*
* @param x is the boolean value to test in order to select the partial specialization of the template class
*
* @code{.cpp}
* // Following example shows how to specialize foo template struct in case given T template argument
* // is an arithmetic type.
* template<class T, class = void> struct foo;
* template<class T>
* struct foo<T, DFP_C_REQUIRES(std::is_arithmetic<T>::value)>{...}
* @endcode
**/
#define DFP_C_REQUIRES(x) typename std::enable_if<x>::type


/**
* @brief Macro for concept check assertion
*
* @param x is the boolean integral type to test in order to check that concept expectation is met.
*
* @code{.cpp}
* // Following example shows how to check that given type T for foo template is a processing_branch type.
* template<class T, class = DFP_CONCEPT_ASSERT((dfp::is_processing_branch<T>))> struct foo
* {};
* @endcode
*
* @sa dfp::concept_check
**/
#define DFP_CONCEPT_ASSERT(x) typename dfp::concept_check<typename boost::function_traits<void x>::arg1_type>::type

/**
* @brief Macro for constexpr variable inlining in header file
*
* if compiler version is C++17 or above macro is 'inline' keyword, otherwise macro is 'static' keyword
*
* @code{.cpp}
* // to be used in header file
* DFP_INLINE_CONSTEXPR int foo = 5; 
* // for C++17 compiler is same as 
* inline constexpr int foo = 5; 
* // for C++14 and below compiler is same as 
* static constexpr int foo = 5;
* @endcode
**/
#define DFP_INLINE_CONSTEXPR DFP_IF(DFP_INLINE_CONSTEXPR_SUPPORT)(inline constexpr, static constexpr)

/**
* @brief Macro for constexpr non-const class method
*
* if compiler version is C++14 or above macro is resolved as 'constexpr' keyword otherwise nothing
*
* @code{.cpp}
* DFP_MUTABLE_CONSTEXPR int my_class::foo(){...};
* // for C++14 and above compiler is same as 
* constexpr int my_class::foo(){...}; 
* // for C++11 compiler is same as 
* int my_class::foo(){...}; 
* @endcode
**/
#define DFP_MUTABLE_CONSTEXPR DFP_IF(DFP_MUTABLE_CONSTEXPR_SUPPORT)(constexpr,)

/**
* @brief Macro for constexpr 'void' return type
*
* if compiler version is C++14 or above macro is resolved as 'constexpr void' keyword otherwise 'void'
*
* @code{.cpp}
* DFP_CONSTEXPR_VOID my_class::foo(){...};
* // for C++14 and above compiler is same as 
* constexpr void my_class::foo(){...}; 
* // for C++11 compiler is same as 
* void my_class::foo(){...}; 
* @endcode
**/
#define DFP_CONSTEXPR_VOID DFP_IF(DFP_CONSTEXPR_VOID_SUPPORT)(constexpr void, void)
///@}
///@}



/**
* @brief Macro to bring C++14 "deprecated" directive for C++11 compiler (allowing it)
**/
#if DFP_CPP14_SUPPORT
    #define DFP_DEPRECATED(msg) [[deprecated(msg)]]
#else
    #if defined(__GNUC__)
    #define DFP_DEPRECATED(msg) __attribute__((deprecated(msg)))
    #elif defined(_MSC_VER)
    #define DFP_DEPRECATED(msg) __declspec(deprecated(msg))
    #else
    #warning This compiler supports no "deprecated" directive
    #endif
#endif


#define DFP_CONCAT(x,y) _DFP_CONCAT(x,y)
#define _DFP_CONCAT(x,y) x##y


#define DFP_STATIC_WARNING(cond, msg) \
struct DFP_CONCAT(static_warning, __LINE__) { \
    constexpr DFP_CONCAT(static_warning,__LINE__)(std::false_type const&) {} \
    DFP_DEPRECATED(msg) constexpr DFP_CONCAT(static_warning,__LINE__)(std::true_type const&) {} \
    constexpr operator int() const { return 0; } \
}; static constexpr int DFP_CONCAT(_DFP_AUTO_VAR, __LINE__) = DFP_CONCAT(static_warning, __LINE__)(std::bool_constant<cond>{})


#ifdef __cpp_guaranteed_copy_elision
    #define DFP_GUARANTEED_COPY_ELISION (__cplusplus >= __cpp_guaranteed_copy_elision)
#else
    #define DFP_GUARANTEED_COPY_ELISION 0
#endif


#if defined(__cpp_concepts) && __cpp_concepts >= 201907L
    #define DFP_CONSTRAINTS_SUPPORT 1
#else
    #define DFP_CONSTRAINTS_SUPPORT 0
#endif

namespace dfp
{
inline namespace core
{
namespace detail
{
// with some versions of GCC EBO idiom cannot be used for upstream_cell
struct upstream_cell_constraint
{ static constexpr bool discard_ebo = DFP_SFINAE_FAILURE_IN_PARTIAL_SPECIALIZATION_WO_ACCESS != 0; };
}


// with some versions of GCC EBO idiom cannot be used for apply_source
// (as this would prevent build of chaining of multiple apply cells in same branch)
struct apply_source_constraint
{ static constexpr bool discard_ebo = DFP_EBCO_BADLY_SUPPORTED_WITH_MULTIPLE_BASES != 0; };


struct default_upstream_cell_optimization
{ static constexpr bool enable_ebo = true; };


struct default_apply_source_optimization
{ static constexpr bool enable_ebo = true; };


}
}


#endif // DFPTL_CORE_CPLUSPLUS_HPP
