/**
* @file core/exception.hpp
*/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_EXCEPTION_HPP
#define DFPTL_CORE_EXCEPTION_HPP


#include <stdexcept>
#include <array>
#include <sstream>
#include <string>

#include "stl/experimental/array.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup exception_grp exception class types
* @brief Defines classes to be thrown as exception by DFP processing cells.
**/
///@{


class frame_length_error : public std::logic_error
{
    template<std::size_t InputCount>
    static std::string create_msg(std::string const& cell_name, std::array<int, InputCount> const& input_indexes, std::string const& msg)
    {
        std::stringstream msg_stream;

        if (input_indexes.size() > 1)
            msg_stream << "Input streams of " << cell_name;
        else
            msg_stream << "Input stream of " << cell_name;

        for(int index : input_indexes)
            msg_stream << " #" << index;

        if (input_indexes.size() > 1)
            msg_stream << " have invalid frame lengths.";
        else
            msg_stream << " has invalid frame length.";

        if (!msg.empty())
            msg_stream << " " << msg;

        return msg_stream.str();
    }

public:
    template<std::size_t InputCount>
    frame_length_error(std::string const cell_name, std::array<int, InputCount> const& input_indexes, std::string const& msg = "")
     :  std::logic_error(create_msg(cell_name, input_indexes, msg)) {}

    frame_length_error(std::string const cell_name, int input_index, std::string const& msg = "")
     :  std::logic_error(create_msg(cell_name, std::experimental::make_array(input_index), msg)) {}
}; // class frame_length_error


class channel_count_error : public std::logic_error
{
    template<std::size_t InputCount>
    static std::string create_msg(std::string const& cell_name, std::array<int, InputCount> const& input_indexes, std::string const& msg)
    {
        std::stringstream msg_stream;

        if (input_indexes.size() > 1)
            msg_stream << "Input streams of " << cell_name;
        else
            msg_stream << "Input stream of " << cell_name;

        for(int index : input_indexes)
            msg_stream << " #" << index;

        if (input_indexes.size() > 1)
            msg_stream << " have invalid channel counts.";
        else
            msg_stream << " has invalid channel count.";

        if (!msg.empty())
            msg_stream << " " << msg;

        return msg_stream.str();
    }

public:
    template<std::size_t InputCount>
    channel_count_error(std::string const cell_name, std::array<int, InputCount> const& input_indexes, std::string const& msg = "")
     :  std::logic_error(create_msg(cell_name, input_indexes, msg)) {}

    channel_count_error(std::string const cell_name, int input_index, std::string const& msg = "")
     :  std::logic_error(create_msg(cell_name, std::experimental::make_array(input_index), msg)) {}
}; // class channel_count_error


class cell_error : public std::runtime_error
{
    static std::string create_msg(std::string const& cell_name, std::string const& msg = "")
    {
        std::stringstream msg_stream;

        msg_stream << "Cell " << cell_name << " has unexpected error.";

        if (!msg.empty())
            msg_stream << " " << msg;

        return msg_stream.str();
    }

public:
    cell_error(std::string const cell_name, std::string const& msg = "")
     :  std::runtime_error(create_msg(cell_name, msg)) {}
}; // class cell_error


///@} // exception_grp
///@} // core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_EXCEPTION_HPP
