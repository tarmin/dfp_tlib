/**
* @file core/processing_descriptor.hpp
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PROCESSING_DESCRIPTOR_HPP
#define DFPTL_CORE_PROCESSING_DESCRIPTOR_HPP


namespace dfp
{
inline namespace core
{
/**
* @addtogroup core_grp
* @{
* @addtogroup apply_grp
* @{
**/


/**
* @brief List of possible driving mode of a @ref processing.
**/
enum class processing_driving
{
    sample_based,   /**< @ref processing captures one single sample on all its inputs and produce one single output sample at each process invocation */
    frame_based,    /**< @ref processing captures one single frame on all its inputs and produce one single output frame at each process invocation */
};



/**
* @brief Wraps [callable](https://en.cppreference.com/w/cpp/named_req/Callable) type and provides info on its processing_driving mode.
*
* processing_descriptor helps to indicate if the wrapped callable processing is
* @ref processing_driving::sample_based "sample-based", @ref processing_driving::frame_based "frame-based" or @ref processing_driving "stream-based".
**/
template<class Callable, processing_driving ProcessingDriving>
struct processing_descriptor
{
    typedef Callable invocable;
    static constexpr processing_driving driving = ProcessingDriving;
};

template<class Invocable>
using sample_based_processing = processing_descriptor<Invocable, processing_driving::sample_based>;

template<class Invocable>
using frame_based_processing = processing_descriptor<Invocable, processing_driving::frame_based>;


///@} apply_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif //DFPTL_CORE_PROCESSING_DESCRIPTOR_HPP
