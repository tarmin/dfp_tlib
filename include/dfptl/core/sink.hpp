/**
* @file core/sink.hpp
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_SINK_HPP
#define DFPTL_CORE_SINK_HPP


#include "dfptl/core/processing_cell.hpp"
#include "dfptl/core/detail/sink.hpp"

#include <boost/fusion/include/pair.hpp>
#include <boost/fusion/include/empty.hpp>
#include <boost/fusion/include/has_key.hpp>
#include <boost/fusion/include/value_at_key.hpp>

#include "stl/functional.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup sink_grp sink processing cells
* @brief Sinks are the downmost downstream cells of a processing graph in DFPTL
*
* Any processing graph shall be actually terminated with a processing @ref core::sink. 
* They help to release to external world the samples streamed out of a processing graph.
* As it is the downmost cell, a processing tree cannot be extended after its `sink`.
* However some peculiar `sink` (e.g. the core::cloneable_sink) can expose some processing `source` and help to re-iniate a new processing graph.
* @{
**/


template<template<class> class Runner> struct runner_args;

template<template<class> class Runner> using runner_args_t = typename runner_args<Runner>::type;

template<class Derived, class = void>
class runnable_sink;


/**
* @brief Runner drives execution scheduling of a @ref sink in a processing tree.
*
* Each sink have a unique runner instance.
* This class shall be the base class of any runner implementation.
* By using the CRTP idiom, this class is involved as a "static interface".
**/
//TODO: make runner a reducer so it may return a value when invoked
template<class Sink, class Derived>
struct runner
{
    /**
    * @brief Fires execution of a processing tree through the given @ref sink
    *
    * @param sink is the sink of the processing to drain regularly.
    */
    //TODO: rename into complete() for emphasing sync execution
    //TODO: create new async_result start() for async execution
    void operator()(Sink& sink)
    {
        static_assert(&Derived::operator() != &runner::operator(), "void operator()(Sink& sink) shall be implemented in Derived class");
        static_cast<Derived const*>(this)(sink); /* failure here likely indicates that operator()(Sink&) is missing in Derived class */
    }

protected:
    template<class T = Sink>
    typename T::template input_port<0>& sink_port(Sink& sink) const noexcept
    { return sink.input(size_t_const<0>{}); }

    DFP_CONSTEXPR_VOID release_sample(Sink& sink)
    { sink.release_sample(sink.input_port(size_t_const<0>{}).absorb_sample()); }

    void release_frame(Sink& sink)
    { sink.release_frame(sink.input_port(size_t_const<0>{}).absorb_frame()); }

    void release_stream(Sink& sink)
    { sink.release_stream(sink.input_port(size_t_const<0>{}).capture_stream()); }
}; // struct runner


/**
* @brief run_never runner actually performs nothing at all.
*
* This runner is useful when samples are delivered by another dedicated operation of the sink.
**/
template<class Sink>
struct run_never : runner<Sink, run_never<Sink>>
{ void operator()(Sink&) {} }; // struct run_never
template<> struct runner_args<run_never>
{ typedef std::tuple<> type; };


/**
* @brief run_once runner fires the processing tree one single time in the current thread.
**/
template<class Sink>
struct run_once : runner<Sink, run_once<Sink>>
{
    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_sample<T>{}))>
    DFP_CONSTEXPR_VOID operator()(Sink& sink)
    { this->release_sample(sink); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_frame<T>{}))>
    void operator()(Sink& sink)
    { this->release_frame(sink); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_stream<T>{}))>
    void operator()(Sink& sink)
    { this->release_stream(sink); }
};
template<> struct runner_args<run_once>
{ typedef std::tuple<> type; };


/**
* @brief run_while runner fires the processing tree in the current thread @b while the given @c continue condition given is met.
*
* This runner basically implements a "while" loop
**/
template<class Sink>
struct run_while : runner<Sink, run_while<Sink>>
{
    run_while(std::function<bool()> condition) : continue_(condition) {}

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_sample<T>{}))>
    void operator()(Sink& sink)
    { while(continue_()) this->release_sample(sink); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_frame<T>{}))>
    void operator()(Sink& sink)
    { while(continue_()) this->release_frame(sink); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_stream<T>{}))>
    void operator()(Sink& sink)
    { while(continue_()) this->release_stream(sink); }

private:
    std::function<bool()> const continue_;
};


/**
* @brief run_until runner fires the processing tree in the current thread @b until the given @c stop condition given is met.
*
* @note the processing tree is always fired at least once before testing the condition.
*
* This runner basically implements a "do-while" loop.
**/
template<class Sink>
struct run_until : runner<Sink, run_until<Sink>>
{
    run_until(std::function<bool()> stop) : stop_(stop) {}

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_sample<T>{}))>
    void operator()(Sink& sink)
    { do this->release_sample(sink); while(!stop_()); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_frame<T>{}))>
    void operator()(Sink& sink)
    { do this->release_frame(sink); while(!stop_()); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_stream<T>{}))>
    void operator()(Sink& sink)
    { do this->release_stream(sink); while(!stop_()); }

private:
    std::function<bool()> const stop_;
};


/**
* @brief tuple of parameter types for run_for constructor.
**/
typedef std::tuple<std::size_t> run_for_args;


/**
* @brief @c Runner firing in loop the processing tree to release the given count of samples
*
* This runner basically implements a "for" loop in the current thread and release the count of samples given as parameter
**/
template<class Sink>
struct run_for : runner<Sink, run_for<Sink>>
{
    static_assert(std::is_same<std::tuple_element_t<0, run_for_args>, std::size_t>::value, "Unconsistency in definition of run_for_args, please contact developer");

    run_for(std::size_t sample_count) : sample_count_(sample_count) {}

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_sample<T>{}))>
    void operator()(Sink& sink)
    { for(std::size_t i = 0 ; i < sample_count_ ; i++) this->release_sample(sink); }

    template<class T = Sink, DFP_F_REQUIRES((detail::supports_release_frame<T>{}))>
    void operator()(Sink& sink)
    {
        auto frame_length = sink.input_stream_specifier().get_frame_length();

        //todo: add warning/assert/exception/static_assert/nothing if sample_count_ is not divisble by frame_length.
        //To be configured with some "sample loss policy"
        for(std::size_t i = 0 ; i < (sample_count_/frame_length) + !!(sample_count_%frame_length) ; i++)
            this->release_frame(sink);
    }

private:
    const std::size_t sample_count_;
};
template<> struct runner_args<run_for>
{ typedef run_for_args type; };


/**
* @brief Defines the terminal cell of a processing graph that helps to release to external world some samples or frames.
*
* A processing graph shall be completed by a sink. This association create a processing tree.
* The processing flow of a processing tree cannot be extended.
*
* * ***For user*** of @ref sink_grp, it is worth to mention that a `run` method is sometimes `publicly` exposed
* and need to be invoked to run execution of the processing (i.e. @ref routing::looper `sink`) but most of the time,
* execution of processing tree is automatically run as soon as its terminal `sink` is built (i.e. routing::looping `sink`). 
* * ***For implementor*** of new @ref sink_grp, any custom `sink` <u>shall</u>
*   * inherit from this base @ref core::sink class 
*   * have a first `Inputs` template parameter in its own declaration
*   * implement <u>one and only one</u> of the `release_sample`, `release_frame` or `release_stream` methods
*     which receive samples, frames or stream from the processing graph. 
*
* @todo custom sink implementation example
* @todo note on "apply" sink
*
* @tparam Inputs is a type required internally by the DFP framework. It is mandatory in any child `sink` as the 1st template parameter in its own class declaration,
* meaning that all `sink` classes are actually template classes. This template parameter shall be forwarded by any child to this base `sink` class <u>without</u> modification.
* At last, `tuple_size<Inputs>()` returns the count of input branches connected to a sink.
*
* @extends core::downstream_cell
* @extends core::initializable_cell
**/
template<class Inputs, template<class> class Runner, class Derived, class InitStorageKeyValue = void, bool UnmoveableRunnable=false>
struct sink
 :  detail::sink<Inputs, index_sequence_from_t<Inputs>, Runner, Derived, InitStorageKeyValue, UnmoveableRunnable>
{
    friend class runnable_sink<Derived, void>;

private:
    typedef detail::sink<Inputs, index_sequence_from_t<Inputs>, Runner, Derived, InitStorageKeyValue, UnmoveableRunnable> sink_detail;

    template<class T = Derived>  //templatized to avoid early evaluation of later static_assert
    struct concept_check
    { static_assert(
            detail::supports_release_sample<T>{} || detail::supports_release_frame<T>{} || detail::supports_release_stream<T>{},
            "Derived class shall implement one (and only one) of the release_sample(), release_frame() or release_stream() methods to comply with sink concept"
    ); };

public:
    typedef sink_tag dfp_cell_tag;
    typedef typename sink_detail::initialization_keyvalue_type  initialization_keyvalue_type;
    typedef typename sink_detail::processing_cell_type  processing_cell_type;
    template<class CellTag>
    using at_tag            = typename processing_cell_type::template at_tag<CellTag>;
    template<class CellTag>
    using at_tag_t = typename at_tag<CellTag>::type;

    /**
    * @brief Creates a new instance of @ref sink given the arguments for the @ref runner initialization
    *
    * @param inputs is an internal DFP_TLIB parameter required and passed by the framework to any `Derived` class of `sink` cell
    * which <u>shall</u> be simply forwarded to this base constructor without modification.
    * @param runner_args pack of possible arguments required for initialization of a `Runner` instance.
    *
    * @tparam RunnerArgs is implicitly deduced from `runner_args` arguments.
    **/
    template<class... RunnerArgs>
    constexpr sink(Inputs&& inputs, RunnerArgs... runner_args)
     :  sink_detail(std::forward<Inputs>(inputs), runner_args...)
    { static_assert(std::is_class<decltype(concept_check<>{})>{}, ""); }

    /**
     * @brief Defines prototype of one of the `release` methods which shall be implemented in `Derived` of core::sink.
     *
     * @param output_samples for purpose of defining here an "abstract" prototype, this parameter is provided as a paramter pack.
     * The actual count of input parameters passed to `release` method is actually equal to the `std::tuple_size<Inputs>{}` value and correspond to
     * the input count connected to the `sink` instance.
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_sample(input_sample_t<T, Ii> const&... output_samples)
    {}

    /**
     * @copydoc release_sample
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_sample(input_sample_t<T, Ii> const&... output_samples) const
    {}

    /**
     * @copydoc release_sample
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_frame(input_frame_t<T, Ii> const&... output_frames)
    {}

    /**
     * @copydoc release_sample
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_frame(input_frame_t<T, Ii> const&... output_frames) const
    {}

    /**
     * @copydoc release_sample
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_stream(input_stream_t<T, Ii>&&... output_streams)
    {}

    /**
     * @copydoc release_sample
     */
    template<class T = sink, std::size_t... Ii, DFP_F_REQUIRES((always_false<T>{}))>
    void release_stream(input_stream_t<T, Ii>&&... output_streams) const
    {}

    /**
    * @brief default empty (doing nothing) implementation of initialize_upwards()
    *
    * In `Derived` class of core::sink requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_upwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_upwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; }

    /**
    * @brief default empty (doing nothing) implementation of initialize_downwards()
    *
    * In `Derived` class of core::sink requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_downwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_downwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; static_cast<Derived* const>(this)->initialize(); }

protected:
    DFP_CONSTEXPR_VOID initialize() const
    {}

    /**
    * @brief Runs execution of the tree processing
    *
    * @note This method is `protected` because the `public` exposure is left to the `runnable` child
    * when it is sure that sink has been initialized.
    **/
    //TODO: add new start() for async exec
#if !DFP_BASE_MEMBER_VISIBILITY_ISSUE_IN_METHOD_PROTOTYPE
    DFP_CONSTEXPR_VOID run()
    { sink_detail::run(); }
#else
public: // otherwise run returned type won't be seen in runnable
    DFP_CONSTEXPR_VOID run()
    { sink_detail::run(); }
protected:
#endif

    typename sink_detail::runner_type const& get_runner() const
    { return sink_detail::get_runner(); }
};


template<class UninitializedSink>
class sink_initializer_base
{
    static_assert(detail::is_sink<UninitializedSink>{}, "Input template parameter \"UninitializedSink\" shall be a sink");

    sink_initializer_base(sink_initializer_base const&) = delete;

public:
    sink_initializer_base(sink_initializer_base&&) = default;
    sink_initializer_base() = default;
}; // sink_initializer_base


template<class UninitializedSink, class = void>
class sink_initializer
 :  public sink_initializer_base<UninitializedSink>
{
    sink_initializer() = delete;

public:
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) sink_initializer(UninitializedSink& a_sink)
    { a_sink.initialize_tree(); }
}; // sink_initializer<...>


template<class UninitializedSink>
class sink_initializer<UninitializedSink, DFP_C_REQUIRES((
    !boost::fusion::result_of::empty<typename detail::sample_inputs_of<UninitializedSink>::map_type>{}
))>
 :  public sink_initializer_base<UninitializedSink>
{
    sink_initializer() = delete;

    typedef detail::sample_inputs_of<UninitializedSink> sample_inputs;

public:
    constexpr sink_initializer(UninitializedSink& a_sink)
// tricky inconsistency issue in compilation between GCC & Clang for map construction from the filter view.
#if defined(__clang__)
     :  sample_inputs_map_(boost::fusion::as_map(typename sample_inputs::filter_view_type{
#else
     :  sample_inputs_map_((typename sample_inputs::filter_view_type{
#endif
            a_sink.initialize_tree()
        }))
    {}

    template<class InputIdentifier, DFP_F_REQUIRES((boost::fusion::result_of::has_key<typename sample_inputs::map_type, detail::sample_input_key<InputIdentifier>>{}))>
    std::remove_pointer_t<typename boost::fusion::result_of::value_at_key<typename sample_inputs::map_type, detail::sample_input_key<InputIdentifier>>::type>&
    input(InputIdentifier id = InputIdentifier{})
    { return *boost::fusion::at_key<detail::sample_input_key<decltype(id)>>(sample_inputs_map_); }

private:
    typename sample_inputs::map_type sample_inputs_map_;

}; // sink_initializer<... sample_inputs map NOT empty ...>


/**
* @brief extension on checking run-ability of a sink
*
* * ensure that run() method is exposed only on a sink which has been initialized
* * prevent copy-construction of runnable given a runnable sink as it has already been initialized
**/
template<class UninitializedSink, class>
class runnable_sink
 :  public UninitializedSink,
    public sink_initializer<UninitializedSink>
{
    // Not sure anymoe that it is still required
    // But let keep this copy constructor delete for performance consideration (initialized tree may have done instensive task like memory allocation and init.
    runnable_sink(runnable_sink const&) = delete; // A runnable sink (or consequently a runnable tree) is never copy-constructible
    // and may even not be move-constructible; If compiler points out
    // this line when failing because of call to implicitly-deleted constructor, you may try either:
    // * replace use of "auto" with "auto&&" in your runnable tree declaration or
    // * replace use of "auto" with "runnable_tree<YOUR_TREE_TYPE>" in your runnable tree declaration

    typedef detail::sample_inputs_of<UninitializedSink> sample_inputs;

public:
    typedef UninitializedSink uninitialized_sink_type;
    typedef sink_initializer<UninitializedSink> sink_initializer_type;

    template<class T = UninitializedSink, DFP_F_REQUIRES((!T::UNMOVEABLE_RUNNABLE))>
    constexpr runnable_sink(runnable_sink&& src)
     :  uninitialized_sink_type(std::move(src)),
        // re-initialize sink in case of move ctor as initilizaion may depend on "this"
        // this is required for c++14 and below as copy elision is not guaranteed
        sink_initializer_type(std::move(static_cast<sink_initializer_type&&>(*this)))
    {}

    /**
     * @brief Creates a runnable_sink (initialized) sink from an uninitialized sink
     **/
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) runnable_sink(uninitialized_sink_type const& a_sink)
     :  uninitialized_sink_type(a_sink),
        sink_initializer_type(static_cast<uninitialized_sink_type&>(*this))
    {}

    /**
     * @brief Creates a runnable_sink (initialized) sink from an uninitialized sink
     **/
    constexpr runnable_sink(UninitializedSink&& a_sink)
     :  UninitializedSink(std::move(a_sink)),
        sink_initializer_type(static_cast<uninitialized_sink_type&>(*this))
    {}

#if DFP_CPP14_SUPPORT
    constexpr decltype(auto) run() &
    { return UninitializedSink::run(); }

    constexpr decltype(auto) run() const &&
    {
        static_assert(
            always_false<UninitializedSink>{},
            "run() cannot be applyed on a rvalue reference because the object may have to survive beyond method invocation to complete the run"
        );
        return UninitializedSink::run();
    }

#else
    template<class T = UninitializedSink>
    decltype(std::declval<T&>().run()) run() &
    { return UninitializedSink::run(); }

    template<class T = UninitializedSink>
    constexpr decltype(std::declval<T&>().run()) run() const &
    { return UninitializedSink::run(); }

    template<class T = UninitializedSink>
    constexpr decltype(std::declval<T&&>().run()) run() const &&
    {
        static_assert(
            always_false<UninitializedSink>{},
            "run() cannot be applyed on a rvalue reference because the object may have to survive beyond method invocation to complete the run"
        );
        return UninitializedSink::run();
    }
 #endif

    template<class InputIdentifier>
    std::remove_pointer_t<typename boost::fusion::result_of::value_at_key<typename sample_inputs::map_type, detail::sample_input_key<InputIdentifier>>::type>&
    input(InputIdentifier id = InputIdentifier{})
    { return static_cast<sink_initializer_type&>(*this).input(id); }
}; //class runnable_sink


template<class Derived>
class runnable_sink<Derived, DFP_C_REQUIRES((!detail::is_processing_tree<Derived>{} && always_true<typename Derived::uninitialized_sink_type>{}))>
 :  public runnable_sink<typename Derived::uninitialized_sink_type>
{
    // falling here would indicate a performance overkill !! sink is already runnable !!
    static_assert(always_false<Derived>{}, DFP_UNEXPECTED_ERROR);

public:
    using runnable_sink<typename Derived::uninitialized_sink_type>::runnable_sink;
}; //class runnable_sink<...runnable_sink...>;


template<class Tree>
class runnable_sink<Tree, DFP_C_REQUIRES((detail::is_processing_tree<Tree>{}))>
 :  public runnable_sink<typename Tree::terminal_cell_type>
{
public:
    using runnable_sink<typename Tree::terminal_cell_type>::runnable_sink;
}; //class runnable_sink<...processing_tree...>;


///@} sink_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_SINK_HPP
