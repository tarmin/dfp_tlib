/**
* @file core/stream_specifier.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_STREAM_SPECIFIER_HPP
#define DFPTL_CORE_STREAM_SPECIFIER_HPP


#include "dfptl/core/stream_specifier_traits.hpp"
#include "dfptl/core/detail/single_sample_frame.hpp"

#include <array>
#include <vector>
#include "stl/span.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup stream_specifier_grp stream specifier
* @brief Specifies the traits of the @ref stream_grp that will be instantiated by the DFP_TLIB framework.
*
* Data transiting between processing cells are hold within a stream object. Thus any @ref source_grp or @ref node_grp produce some @ref stream
* on its output. But Instanciation of the appropriate class template @ref stream "streams" is the responsability of the DFP_TLIB framework.
* However user-defined @ref source_grp or @ref node_grp can specify with help of @ref class stream_specifier and @ref stream_specifier_from_frame
* some traits of their stream outputted.
*
* A @c stream_specifier supports default values for all its configurable properties but working with such default values may be irrelevant or suboptimal.
*   
* @ref Following properties can be configured for a stream:
* * its frame length (number of samples in a frame), default is one. 
* * its channel count (number of data channels per frame), default is one
* * its sample type, default is same as type @ref default_sample_t<> 
* * its frame template, default is same as type @ref default_frame_template_t<>
* * and its tag type, default is @ref null_tag
*
* Note that @ref stream_specifier_from_frame allows to specify once for all frame length, sample type and frame template properties
* by giving a single frame type.
*
* * Streams can be easily specified by using one of the specialized classes:
* * @ref static_stream_specifier refers to stream whose frame length is defined at compile-time.
* * @ref dynamic_stream_specifier refers to stream whose frame length is defined at runtime and all other propertie are defined at compile-time.
* * @ref atomic_stream_specifier refers to a peculiar @ref static_stream_specifier whose frame length is worth 1.
**/
///@{


template<class Frame>
class frame_builder;



template<class Sample>
class frame_builder<detail::single_sample_frame<Sample>>
{
public:
    typedef detail::single_sample_frame<Sample> frame_type;
    /**
    * return type of the @c build() operation
    **/
    typedef frame_type result_type;

    constexpr frame_builder() {}

    template<class NewSampleClue>
    constexpr frame_builder<detail::single_sample_frame<NewSampleClue>> set_sample_clue(type_meta<NewSampleClue>) const
    { return frame_builder<detail::single_sample_frame<NewSampleClue>>{}; }

    constexpr frame_builder set_frame_extent(size_t_const<1>) const
    { return *this; }

    constexpr frame_builder set_frame_length(size_t_const<1>) const
    { return *this; }

    constexpr frame_type build() const
    { return frame_type{}; }
}; //struct frame_builder<single_sample_frame<...>>


template<class Sample>
class frame_builder<std::vector<Sample>>
{
public:
    typedef std::vector<Sample> frame_type;
    /**
    * return type of the @c build() operation
    **/
    typedef frame_type result_type;

    constexpr frame_builder() {}

    template<class NewSampleClue>
    constexpr   frame_builder<std::vector<NewSampleClue>> set_sample_clue(type_meta<NewSampleClue>) const
    { return    frame_builder<std::vector<NewSampleClue>>{}
        .set_frame_length(frame_length_)
    ; }

    constexpr frame_builder set_frame_extent(decltype(DYNAMIC_EXTENT)) const
    { return set_frame_length(frame_length_); }

    constexpr frame_builder set_frame_length(std::size_t frame_length) const
    { return frame_builder(*this, frame_length); }

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) frame_type build() const
    {
        if (frame_length_ == DYNAMIC_EXTENT)
            return frame_type{}; // frame will have to be resized later with helper update_length
        else
            return frame_type(frame_length_);
    }

private:
    constexpr frame_builder(frame_builder const& original, std::size_t frame_length) : frame_length_(frame_length)
    {}

    std::size_t frame_length_{ DYNAMIC_EXTENT };
}; //struct frame_builder<std::vector<...>>


template<class Sample, std::size_t FrameLength>
class frame_builder<std::array<Sample, FrameLength>>
{
public:
    typedef std::array<Sample, FrameLength> frame_type;
    /**
    * return type of the @c build() operation
    **/
    typedef frame_type result_type;

    constexpr frame_builder() {}

    template<class NewSampleClue>
    constexpr   frame_builder<std::array<NewSampleClue, FrameLength>> set_sample_clue(type_meta<NewSampleClue>) const
    { return    frame_builder<std::array<NewSampleClue, FrameLength>>{}; }

    template<std::size_t NewExtent>
    constexpr   frame_builder<std::array<Sample, NewExtent>> set_frame_extent(size_t_const<NewExtent>) const
    { return    frame_builder<std::array<Sample, NewExtent>>{}; }

    constexpr frame_builder set_frame_length(size_t_const<FrameLength>) const
    { return *this; }

    constexpr frame_type build() const
    { return frame_type{}; }
}; //struct frame_builder<std::array<...>>


template<class Sample, std::size_t FrameLength>
class frame_builder<upg::span<Sample, FrameLength>>
{
public:
    typedef upg::span<Sample, FrameLength> frame_type;
    /**
    * return type of the @c build() operation
    **/
    typedef std::conditional_t<frame_type::extent != DYNAMIC_EXTENT, std::array<Sample, FrameLength>, std::vector<Sample>> result_type;

private:
    std::conditional_t<frame_type::extent != DYNAMIC_EXTENT, size_t_const<FrameLength>, std::size_t> frame_length_{ size_t_const<FrameLength>() };

public:
    constexpr frame_builder() {}

    template<class NewSampleClue>
    constexpr   frame_builder<upg::span<NewSampleClue, FrameLength>> set_sample_clue(type_meta<NewSampleClue>) const
    { return    frame_builder<upg::span<NewSampleClue, FrameLength>>{}
        .set_frame_length(frame_length_)
    ; }

    template<std::size_t NewExtent>
    constexpr   frame_builder<upg::span<Sample, NewExtent>> set_frame_extent(size_t_const<NewExtent>) const
    { return    frame_builder<upg::span<Sample, NewExtent>>{}.set_frame_length(frame_length_); }

    constexpr frame_builder set_frame_length(decltype(frame_length_) frame_length) const
    { return frame_builder(*this, frame_length); }

    template<std::size_t FrameExtent = frame_type::extent, DFP_F_REQUIRES((FrameExtent != DYNAMIC_EXTENT))>
    constexpr std::array<Sample, FrameLength> build() const
    { return std::array<Sample, FrameLength>{}; }

    template<std::size_t FrameExtent = frame_type::extent, DFP_F_REQUIRES((FrameExtent == DYNAMIC_EXTENT))>
    constexpr std::vector<Sample> build() const
    { return std::vector<Sample>(frame_length_); }

private:
    constexpr frame_builder(frame_builder const& original, decltype(frame_length_) frame_length) : frame_length_(frame_length) {}
}; //struct frame_builder<std::vector<...>>


/**
* @brief Defines the frame length in a data stream
*
* Such a class is a piece of definition for a stream specifying it frame length.
*
* @tparam FrameExtent the frame length in the data stream.
* If it is not worth `DYNAMIC_EXTENT`, it specifies the frame length otherwise the value shall be specified
* at runtime by passing the actual value to the object constructor.
*
* @see stream_specifier
**/
template<std::size_t FrameExtent>
struct stream_frame_length_property
{
    /** @brief Is worth length of the frame conveyed by the stream if static (known at compile-time) otherwise @ref DYNAMIC_EXTENT */
    static constexpr size_t_const<FrameExtent> FRAME_EXTENT {};
    static constexpr bool_const<true> IS_FRAME_LENGTH_STATIC {};

    static constexpr size_t_const<FrameExtent> get_frame_length()
    { return size_t_const<FrameExtent>{}; }

    static constexpr bool_const<true> is_frame_length_specified()
    { return bool_const<true>{}; }
};


template<>
struct stream_frame_length_property<DYNAMIC_EXTENT>
{
    static constexpr size_t_const<DYNAMIC_EXTENT> FRAME_EXTENT {};
    static constexpr bool_const<false> IS_FRAME_LENGTH_STATIC {};

    constexpr stream_frame_length_property() : frame_length_(DYNAMIC_EXTENT)
    {}

    constexpr stream_frame_length_property(std::size_t frame_length) : frame_length_(frame_length)
    {}

    constexpr std::size_t get_frame_length() const
    { return frame_length_; }

    constexpr bool is_frame_length_specified() const
    { return frame_length_ != DYNAMIC_EXTENT; }

private:
    std::size_t frame_length_;
};


/**
* @brief Defines the channel count in a data stream
*
* Such a class is a piece of definition for a stream specifying it count of channels.
*
* @tparam ChannelExtent the channel count in the data stream.
* If it is not worth `DYNAMIC_EXTENT`, it specifies the channel count otherwise the value shall be specified
* at runtime by passing the actual value to the object constructor.
*
* @see stream_specifier
**/
template<std::size_t ChannelExtent>
struct stream_channel_count_property
{
    static constexpr size_t_const<ChannelExtent> CHANNEL_EXTENT {};
    static constexpr bool_const<true> IS_CHANNEL_COUNT_STATIC {};

    static constexpr size_t_const<ChannelExtent> get_channel_count()
    { return size_t_const<ChannelExtent>{}; }


    static constexpr bool_const<true> is_channel_count_specified()
    { return bool_const<true>{}; }
};


template<>
struct stream_channel_count_property<DYNAMIC_EXTENT>
{
    static constexpr size_t_const<DYNAMIC_EXTENT> CHANNEL_EXTENT {};
    static constexpr bool_const<false> IS_CHANNEL_COUNT_STATIC {};

    constexpr stream_channel_count_property() : channel_count_(1)
    {}

    constexpr stream_channel_count_property(std::size_t channel_count) : channel_count_(channel_count)
    {}

    constexpr std::size_t get_channel_count() const
    { return channel_count_; }

    constexpr bool is_channel_count_specified() const
    { return channel_count_ != DYNAMIC_EXTENT; }

private:
    std::size_t channel_count_;
};


/**
* @brief Defines the sample type in a data stream
*
* Such a class is a piece of definition for a stream specifying its sample trait.
*
* @tparam SampleClue the sample type conveyed by the stream
* If it is not worth `dynamic_type_tag`, it specifies the sample type otherwise it shall be specified
* at runtime by passing the actual @ref dynamic_type_meta value to the object constructor.
*
* @see stream_specifier
**/
template<class SampleClue>
struct stream_sample_type_property
{
    typedef SampleClue sample_clue;
    static constexpr type_meta<SampleClue> SAMPLE_CLUE {};
    static constexpr bool_const<true> IS_SAMPLE_TYPE_STATIC {};

    static constexpr type_meta<SampleClue> get_sample_type()
    { return type_meta<SampleClue>{}; }
};


template<>
struct stream_sample_type_property<dynamic_type_tag>
{
    typedef dynamic_type_tag sample_clue;
    static constexpr type_meta<dynamic_type_tag> SAMPLE_CLUE {};
    static constexpr bool_const<false> IS_SAMPLE_TYPE_STATIC {};

    constexpr stream_sample_type_property() : type_meta_(DEFAULT_SAMPLE_TYPE)
    {}

    constexpr stream_sample_type_property(dynamic_type_meta const& meta) : type_meta_(meta)
    {}

    constexpr dynamic_type_meta const& get_sample_type() const
    { return type_meta_; }

private:
    static constexpr auto DEFAULT_SAMPLE_TYPE = to_dynamic_type_meta(type_meta<default_sample_t<>>{});
    dynamic_type_meta const& type_meta_;
};


/**
* @brief Defines the frame type in a data stream
*
* Such a class is a piece of definition for a stream specifying its frame trait.
*
* @tparam FrameSketchClue the frame type conveyed by the stream
* If it is not worth `dynamic_template_tag`, it specifies the frame type otherwise the it shall be specified
* at runtime by passing the actual @ref dynamic_type_meta value to the object constructor.
*
* @see stream_specifier
**/
template<class FrameSketchClue>
struct stream_frame_template_property
{
    typedef FrameSketchClue frame_sketch_clue;
    // should be :static_constexpr template_meta<FrameSketchClue> FRAME_SKETCH_CLUE = template_meta<FrameSketchClue>{};
    static constexpr detail::frame_meta<FrameSketchClue> FRAME_SKETCH_CLUE {};
    static constexpr bool_const<true> IS_FRAME_TEMPLATE_STATIC {};

    static constexpr detail::frame_meta<FrameSketchClue> get_frame_template()
    { return detail::frame_meta<FrameSketchClue>{}; }
};


template<>
struct stream_frame_template_property<dynamic_template_tag>
{
    typedef dynamic_template_tag frame_sketch_clue;
    static constexpr detail::frame_meta<dynamic_template_tag> FRAME_SKETCH_CLUE {};
    static constexpr bool_const<false> IS_FRAME_TEMPLATE_STATIC {};

    constexpr stream_frame_template_property() : template_meta_(DEFAULT_FRAME_META)
    {}

    constexpr stream_frame_template_property(dynamic_template_meta const& meta) : template_meta_(meta)
    {}

    constexpr dynamic_template_meta const& get_frame_template() const
    { return template_meta_; }

private:
    static constexpr auto DEFAULT_FRAME_META = to_dynamic_template_meta(detail::frame_meta<default_dynamic_extent_frame_t<detail::null_tag>>{});
    dynamic_template_meta const& template_meta_;
};


/**
* @brief Defines the tag type in a data stream
*
* Such a class is a piece of definition for a stream specifying its tag trait.
*
* @tparam TagClue the tag type conveyed by the stream
* If it is not worth `dynamic_type_tag`, it specifies the tag type otherwise the it shall be specified
* at runtime by passing the actual @ref dynamic_type_meta value to the object constructor.
*
* @see stream_specifier
**/
template<class TagClue>
struct stream_tag_type_property
{
    static_assert(detail::is_null_tag<TagClue>{} || is_custom_tag<TagClue>{}, "Given TagClue template parameter shall be a custom tag");

    typedef TagClue tag_clue;
    static constexpr type_meta<TagClue> TAG_CLUE {};
    static constexpr bool_const<true> IS_TAG_TYPE_STATIC {};

    static constexpr type_meta<TagClue> get_tag_type()
    { return type_meta<TagClue>{}; }
};


template<>
struct stream_tag_type_property<dynamic_type_tag>
{
    typedef dynamic_type_tag tag_clue;
    static constexpr type_meta<dynamic_type_tag> TAG_CLUE {};
    static constexpr bool_const<false> IS_TAG_TYPE_STATIC {};

    constexpr stream_tag_type_property() : type_meta_(DEFAULT_TAG_META)
    {}

    constexpr stream_tag_type_property(dynamic_type_meta const& meta) : type_meta_(meta)
    {}

    constexpr dynamic_type_meta const& get_tag_type() const
    { return type_meta_; }

private:
    static constexpr auto DEFAULT_TAG_META = to_dynamic_type_meta(type_meta<detail::null_tag>{});
    dynamic_type_meta const& type_meta_;
};


template<
    class FrameLengthProperty, class ChannelCountProperty, class SampleTypeProperty, class FrameTemplateProperty, class TagTypeProperty
>
struct stream_specifier_base
 :  FrameLengthProperty, ChannelCountProperty, SampleTypeProperty, FrameTemplateProperty, TagTypeProperty
{
    typedef stream_specifier_tag dfp_tag;
    typedef FrameLengthProperty frame_length_property_type;
    typedef ChannelCountProperty channel_count_property_type;
    typedef SampleTypeProperty sample_type_property_type;
    typedef FrameTemplateProperty frame_template_property_type;
    typedef TagTypeProperty tag_type_property_type;

    /**
    * @brief Indicates if all properties part of the @ref stream_specifier definition are statically defined (known at compile-time)
    **/
    constexpr static bool IS_STATIC =
        FrameLengthProperty::IS_FRAME_LENGTH_STATIC && ChannelCountProperty::IS_CHANNEL_COUNT_STATIC
        && SampleTypeProperty::IS_SAMPLE_TYPE_STATIC && FrameTemplateProperty::IS_FRAME_TEMPLATE_STATIC
        && TagTypeProperty::IS_TAG_TYPE_STATIC;

    constexpr static bool IS_ATOMIC = IS_STATIC && (FrameLengthProperty::FRAME_EXTENT == 1);

    constexpr stream_specifier_base()
    {}

    constexpr stream_specifier_base(
        FrameLengthProperty const& frame_length_property, ChannelCountProperty const& channel_count_property,
        SampleTypeProperty const& sample_type_property, FrameTemplateProperty const& frame_template_property,
        TagTypeProperty const& tag_type_property
    )
     :  FrameLengthProperty(frame_length_property), ChannelCountProperty(channel_count_property),
        SampleTypeProperty(sample_type_property), FrameTemplateProperty(frame_template_property),
        TagTypeProperty(tag_type_property)
    {}

    template<class T = typename FrameTemplateProperty::frame_sketch_clue, DFP_F_REQUIRES((!std::is_void<T>{}))>
    constexpr auto get_frame_builder() const ->
    decltype(core::frame_builder<T>{}
        .set_sample_clue(this->SAMPLE_CLUE)
        .set_frame_extent(this->FRAME_EXTENT)
        .set_frame_length(this->get_frame_length())
    )
    { return core::frame_builder<T>{}
        .set_sample_clue(this->SAMPLE_CLUE)
        .set_frame_extent(this->FRAME_EXTENT)
        .set_frame_length(this->get_frame_length())
    ; };

    template<class T = stream_specifier_base, DFP_F_REQUIRES((std::is_void<typename T::frame_sketch_clue>{}))>
    constexpr auto get_frame_builder() const ->
    decltype(this->get_frame_builder<default_frame_template_t<T>>())
    { return this->get_frame_builder<default_frame_template_t<T>>(); };
}; //struct stream_specifier_base


/**
* @brief Holds user-defined requirements for a stream whose type will be later resolved.
*
* stream_specifier is intented to reduce boilerplate in user code and hide stream implementation details.
* However some alias @ref atomic_stream_specifier, @ref static_stream_specifier or @ref dynamic_stream_specifier
* can ever be preferred as they should suite end-user needs in most cases.
*
* @tparam FrameExtent specifies the frame length for the stream or is set to DYNAMIC_EXTENT if not known at compile-time
* @todo @c FrameExtent to be turned into template class parameter to support "as_input_tag"
* @tparam ChannelExtent specifies the channel count for the stream or is set to DYNAMIC_EXTENT if not known at compile-time
* @todo @c ChannelExtent to be turned into template class parameter to support "as_input_tag"
* @tparam SampleClue is either a type defining the sample type conveyed by the stream or a tag which describes such a type. 
* Possible tag for @c SampleClue is @ref dynamic_type_tag if type is defined at compile-time.
* @tparam FrameSketchClue is the type template of the sample frame conveyed by the required stream.
* If this actual template parameter is worth `void`, the frame template will be automatically deduced by DFP_TLIB
* @tparam TagClue optional type associated to the resulting stream. It shall be unique in the processing graph where the stream will flow
*
* @sa atomic_stream_specifier, static_stream_specifier, dynamic_stream_specifier
**/
template<
    std::size_t FrameExtent = DYNAMIC_EXTENT, std::size_t ChannelExtent = 1/*TODO: should be size_t_const<Ch> to allow support as_input_tag ?*/, class SampleClue = default_sample_t<>, class FrameSketchClue = void, class TagClue = detail::null_tag
>
struct stream_specifier
 :  stream_specifier_base<
        stream_frame_length_property<FrameExtent>, stream_channel_count_property<ChannelExtent>, stream_sample_type_property<SampleClue>,
        stream_frame_template_property<FrameSketchClue>, stream_tag_type_property<TagClue>
    >
{
    using base =
    stream_specifier_base<
        stream_frame_length_property<FrameExtent>, stream_channel_count_property<ChannelExtent>, stream_sample_type_property<SampleClue>,
        stream_frame_template_property<FrameSketchClue>, stream_tag_type_property<TagClue>
    >;

    /** @brief Helper class to specify the frame extent value at compile-time*/
    template<std::size_t NewFrameExtent>
    struct set_frame_extent
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<std::size_t NewFrameExtent>
    using set_frame_extent_t = typename set_frame_extent<NewFrameExtent>::type;

   /** @brief Helper class to specify the channel extent value at compile-time */
    template<std::size_t NewChannelExtent>
    struct set_channel_extent
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<std::size_t NewChannelExtent>
    using set_channel_extent_t = typename set_channel_extent<NewChannelExtent>::type;

   /** @brief Helper class to specify the sample clue at compile-time */
    template<class NewSampleClue>
    struct set_sample_clue
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewSampleClue>
    using set_sample_clue_t = typename set_sample_clue<NewSampleClue>::type;

   /** @brief Helper class to specify the frame clue at compile-time */
    template<class NewFrameClue>
    struct set_frame_sketch_clue
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewFrameClue>
    using set_frame_sketch_clue_t = typename set_frame_sketch_clue<NewFrameClue>::type;

   /** @brief Helper class to specify the tag clue at compile-time */
    template<class NewTagClue>
    struct set_tag_clue
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewTagClue>
    using set_tag_clue_t = typename set_tag_clue<NewTagClue>::type;

    /**
    * @brief Specifies at compile-time the channel count property for a stream 
    * @return a new instance of stream_specifier with the new value of channel count
    **/
    template<std::size_t ChannelCount>
    constexpr   stream_specifier<FrameExtent, ChannelCount, SampleClue, FrameSketchClue, TagClue>
    set_channel_count(size_t_const<ChannelCount>) const
    {
        static_assert(
            (FrameExtent == DYNAMIC_EXTENT) || ((FrameExtent % ChannelCount) == 0),
            "Required channel count cannot be divided by the extent value of frame length"
        );
        return  stream_specifier<FrameExtent, ChannelCount, SampleClue, FrameSketchClue, TagClue>(*this);
    }

    /**
    * @brief Specifies at runtime the channel count property for a stream 
    * @return a new instance of stream_specifier with the new value of channel count
    **/
    constexpr   stream_specifier<FrameExtent, DYNAMIC_EXTENT, SampleClue, FrameSketchClue, TagClue>
    set_channel_count(std::size_t channel_count) const
    { return    stream_specifier<FrameExtent, DYNAMIC_EXTENT, SampleClue, FrameSketchClue, TagClue>(
        *this, stream_channel_count_property<DYNAMIC_EXTENT>(channel_count)
    ); }

    /**
    * @brief Specifies at compile-time the frame length property for a stream 
    * @return a new instance of stream_specifier with the new value of frame length
    **/
    template<std::size_t FrameLength, bool Condition = base::IS_FRAME_LENGTH_STATIC, DFP_F_REQUIRES((Condition))>
    constexpr   stream_specifier<FrameLength, ChannelExtent, SampleClue, FrameSketchClue, TagClue>
    set_frame_length(size_t_const<FrameLength>) const
    { return    stream_specifier<FrameLength, ChannelExtent, SampleClue, FrameSketchClue, TagClue>(*this); }

    /**
    * @brief Specifies at runtime the frame length property for a stream 
    * @return a new instance of stream_specifier with the new value of frame length
    **/
    constexpr   stream_specifier<DYNAMIC_EXTENT, ChannelExtent, SampleClue, FrameSketchClue, TagClue>
    set_frame_length(std::size_t frame_length) const
    { return    stream_specifier<DYNAMIC_EXTENT, ChannelExtent, SampleClue, FrameSketchClue, TagClue>(
        *this, stream_frame_length_property<DYNAMIC_EXTENT>(frame_length)
    ); }

    /**
    * @brief Specifies at compile-time the sample type property for a stream 
    * @return a new instance of stream_specifier with the new type of sample
    **/
    template<class NewSample>
    constexpr stream_specifier<FrameExtent, ChannelExtent, NewSample, FrameSketchClue, TagClue> set_sample_type(type_meta<NewSample> = type_meta<NewSample>{}) const
    { return stream_specifier<FrameExtent, ChannelExtent, NewSample, FrameSketchClue, TagClue>(*this, stream_sample_type_property<NewSample>()); }

    /**
    * @brief Specifies at runtime the sample type property for a stream 
    * @return a new instance of stream_specifier with the new type of sample
    **/
    constexpr stream_specifier<FrameExtent, ChannelExtent, dynamic_type_tag, FrameSketchClue, TagClue> set_sample_type(dynamic_type_meta const& meta) const
    { return stream_specifier<FrameExtent, ChannelExtent, dynamic_type_tag, FrameSketchClue, TagClue>(*this, stream_sample_type_property<dynamic_type_tag>(meta)); }

    /**
    * @brief Specifies at compile-time the frame template property for a stream 
    * @return a new instance of stream_specifier with the new template of frame
    **/
    template<class NewFrame>
    constexpr stream_specifier<FrameExtent, ChannelExtent, SampleClue, NewFrame, TagClue> set_frame_template(template_meta<NewFrame> = template_meta<NewFrame>{}) const
    { return stream_specifier<FrameExtent, ChannelExtent, SampleClue, NewFrame, TagClue>(*this, stream_frame_template_property<NewFrame>()); }

    /**
    * @brief Specifies at runtime the frame template property for a stream 
    * @return a new instance of stream_specifier with the new template of frame
    **/
    constexpr stream_specifier<FrameExtent, ChannelExtent, SampleClue, dynamic_template_tag, TagClue> set_frame_template(dynamic_template_meta const& meta) const
    { return stream_specifier<FrameExtent, ChannelExtent, SampleClue, dynamic_template_tag, TagClue>(*this, stream_frame_template_property<dynamic_template_tag>(meta)); }

    /**
    * @brief Specifies at compile-time the tag type property for a stream 
    * @return a new instance of stream_specifier with the new tag type
    **/
    template<class NewTag>
    constexpr stream_specifier<FrameExtent, ChannelExtent, SampleClue, FrameSketchClue, NewTag> set_tag_type(type_meta<NewTag> = type_meta<NewTag>{}) const
    { return stream_specifier<FrameExtent, ChannelExtent, SampleClue, FrameSketchClue, NewTag>(*this, stream_tag_type_property<NewTag>()); }


    /**
    * @brief Specifies at runtime the tag type property for a stream 
    * @return a new instance of stream_specifier with the new tag type
    **/
    constexpr stream_specifier<FrameExtent, ChannelExtent, SampleClue, FrameSketchClue, dynamic_type_tag> set_tag_type(dynamic_type_meta const& meta) const
    { return stream_specifier<FrameExtent, ChannelExtent, SampleClue, FrameSketchClue, dynamic_type_tag>(*this, stream_tag_type_property<dynamic_type_tag>(meta)); }

    /**
    * @brief Creates a new instance of stream_specifier() with default value for each of the stream property 
    **/
    constexpr stream_specifier()
    {}

    /**
    * @brief Copy-constructs a new instance of stream_specifier() updated with the given frame length property 
    **/
    template<std::size_t OriginalFrameExtent>
    constexpr stream_specifier(
        stream_specifier<OriginalFrameExtent, ChannelExtent, SampleClue, FrameSketchClue, TagClue> const& original,
        typename base::frame_length_property_type frame_length_property
    ) : base(frame_length_property, original, original, original, original)
    {}

    /**
    * @brief Copy-constructs a new instance of stream_specifier() updated with the given channel count property 
    **/
    template<std::size_t OriginalChannelExtent>
    constexpr stream_specifier(
        stream_specifier<FrameExtent, OriginalChannelExtent, SampleClue, FrameSketchClue, TagClue> const& original,
        typename base::channel_count_property_type channel_count_property
    ) : base(original, channel_count_property, original, original, original)
    {}

    /**
    * @brief Copy-constructs a new instance of stream_specifier() updated with the given sample type property 
    **/
    template<class OriginalSample>
    constexpr stream_specifier(
        stream_specifier<FrameExtent, ChannelExtent, OriginalSample, FrameSketchClue, TagClue> const& original,
        typename base::sample_type_property_type sample_property
    ) : base(original, original, sample_property, original, original)
    {}

    /**
    * @brief Copy-constructs a new instance of stream_specifier() updated with the given frame template property 
    **/
    template<class OriginalFrame>
    constexpr stream_specifier(
        stream_specifier<FrameExtent, ChannelExtent, SampleClue, OriginalFrame, TagClue> const& original,
        typename base::frame_template_property_type frame_property
    ) : base(original, original, original, frame_property, original)
    {}

    /**
    * @brief Copy-constructs a new instance of stream_specifier() updated with the given tag type property 
    **/
    template<class OriginalTag>
    constexpr stream_specifier(
        stream_specifier<FrameExtent, ChannelExtent, SampleClue, FrameSketchClue, OriginalTag> const& original,
        typename base::tag_type_property_type tag_property
    ) : base(original, original, original, original, tag_property)
    {}
}; // struct stream_specifier


template<class Frame, std::size_t ChannelExtent = 1, class TagClue = detail::null_tag>
class stream_specifier_from_frame
 :  public  stream_specifier<
        detail::frame_traits<Frame>::EXTENT, ChannelExtent, typename detail::frame_traits<Frame>::sample_type, Frame, TagClue
    >
{
    typedef stream_specifier<
        detail::frame_traits<Frame>::EXTENT, ChannelExtent, typename detail::frame_traits<Frame>::sample_type, Frame, TagClue
    > base;

public:
    using  base::base;

    constexpr stream_specifier_from_frame(base const& it) : base(it)
    {}
};


/**
* @brief Alias for stream specifier whose frame length is defined at compile-time
*
* @tparam Sample is the type of the sample in the stream
* @tparam FrameLength is the frame length value of the stream. It shall be greater or equal to 1.
* @tparam TagClue optional type associated to the resulting stream. It shall be unique in the processing graph where the stream will flow
*
* @sa stream_specifier
* @todo exchange position of Sample and FrameLength template parameter
**/
template<class Sample, std::size_t FrameLength, std::size_t ChannelExtent = 1, class TagClue = detail::null_tag>
using static_stream_specifier = stream_specifier<FrameLength, ChannelExtent, Sample, void, TagClue>;


/**
* @brief Specifies properties for a stream whose frame length has dynamic extent
*
* Frame length of a @ref dynamic_stream_specifier is defined dynamically at runtime with help of some @ref frame_length_property::set_frame_length "set_frame_length()" method
* of the @ref stream_specifier class
*
* @tparam Sample is the type of the sample in the stream
* @tparam TagClue optional type associated to the resulting stream. It shall be unique in the processing graph where the stream will flow
*
* @sa stream_specifier
**/
template<class Sample, std::size_t ChannelExtent = 1, class TagClue = detail::null_tag>
using dynamic_stream_specifier = stream_specifier<DYNAMIC_EXTENT, ChannelExtent, Sample, void, TagClue>;


/**
* @brief The atomic_stream_specifier is a specialized static_stream_specifier whose frame length is worth 1.
*
* @tparam Sample is the type of the sample in the stream
* @tparam TagClue optional type associated to the resulting stream. It shall be unique in the processing graph where the stream will flow
*
* @sa stream_specifier
**/
template<class Sample, class TagClue = detail::null_tag>
using atomic_stream_specifier = static_stream_specifier<Sample, 1, 1, TagClue>;


// 1. 'update' struct cannot be named 'type' or GCC will warn that the second later 'type' definition in set_channel_extent will hide the 1st one.
// 2. following definition cannot be inlined in class stream_specifier definition otherwise gcc will warn about "invalid use of incomplete type"
template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
template<std::size_t ChannelCount>
struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_channel_extent<ChannelCount>::update
{ typedef stream_specifier<FrameExtent, ChannelCount, Sample, Frame, TagClue> type; };


namespace detail
{


template<std::size_t NewFrameExtent, std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
struct update_frame_extent
{ typedef stream_specifier<NewFrameExtent, ChannelExtent, Sample,
    std::conditional_t<
        (NewFrameExtent == detail::frame_traits<Frame>::EXTENT) || (detail::frame_traits<Frame>::EXTENT == DYNAMIC_EXTENT)
    , Frame, void>,
TagClue> type; };

template<std::size_t NewFrameExtent, std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class TagClue>
struct update_frame_extent<NewFrameExtent, FrameExtent, ChannelExtent, Sample, void, TagClue>
{ typedef stream_specifier<NewFrameExtent, ChannelExtent, Sample, void, TagClue> type; };


} // namespace detail


template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
template<std::size_t NewFrameExtent>
struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_frame_extent<NewFrameExtent>::update
{ typedef typename detail::update_frame_extent<NewFrameExtent, FrameExtent, ChannelExtent, Sample, Frame, TagClue>::type type; };


//template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
//template<std::size_t NewFrameExtent>
//struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_frame_extent<NewFrameExtent>::update
//{ typedef stream_specifier<NewFrameExtent, ChannelExtent, Sample, Frame, TagClue> type; };


template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
template<class NewSample>
struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_sample_clue<NewSample>::update
{ typedef stream_specifier<FrameExtent, ChannelExtent, NewSample, Frame, TagClue> type; };


template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
template<class NewFrame>
struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_frame_sketch_clue<NewFrame>::update
{ typedef stream_specifier<FrameExtent, ChannelExtent, Sample, NewFrame, TagClue> type; };


template<std::size_t FrameExtent, std::size_t ChannelExtent, class Sample, class Frame, class TagClue>
template<class NewTag>
struct stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, TagClue>::set_tag_clue<NewTag>::update
{ typedef stream_specifier<FrameExtent, ChannelExtent, Sample, Frame, NewTag> type; };


///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_STREAM_SPECIFIER_HPP
