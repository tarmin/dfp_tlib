/**
* @file core/frame_ref_wrapper.hpp
**/
/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_FRAME_REF_WRAPPER_HPP
#define DFPTL_CORE_FRAME_REF_WRAPPER_HPP


#include <stl/functional.hpp>


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/**
* @defgroup frame_ref frame reference wrapper
* @brief Function templates @c frame_ref and @c cframe_ref are helper functions that generate an object of type frame_ref_wrapper,
* using template argument deduction to determine the template argument of the result
*
* Frame reference wrappers are usually resulting from in-place processing cells.
* @{
**/


template <class Frame>
class frame_ref_wrapper : public std::reference_wrapper<Frame>
{
    typedef std::reference_wrapper<Frame> reference_wrapper_type;

public:
    using reference_wrapper_type::reference_wrapper_type;

    //TODO: replace with assert on frame concept compliance
    static_assert(supports_iterator<Frame>::value, "Frame template argument shall provides a typedef iterator");
    static_assert(supports_const_iterator<Frame>::value, "Frame template argument shall provides a typedef const_iterator");

//TODO: make frame_ref_wrapper complies with frame concept or remove it ?
}; // class frame_ref_wrapper

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#endif
/**
* @brief returns frame_ref_wrapper<const Frame>(frame)
**/
#ifdef __clang__
#pragma clang diagnostic pop
#endif
template<class Frame>
frame_ref_wrapper<Frame const> cframe_ref(Frame const& frame) noexcept
{ return frame_ref_wrapper<Frame const>(frame); }

/**
* @brief returns cframe_ref(t.get())
**/
template<class Frame>
frame_ref_wrapper<Frame const> cframe_ref(frame_ref_wrapper<Frame> t) noexcept
{ return cframe_ref(t.get()); }

/**
* @brief rvalue reference wrapper is deleted.
**/
template <class T>
void cframe_ref(const T&&) = delete;


/**
* @brief returns frame_ref_wrapper<Frame>(frame)
**/
template<class Frame>
frame_ref_wrapper<Frame> frame_ref(Frame& frame) noexcept
{ return frame_ref_wrapper<Frame>(frame); }

/**
* @brief returns frame_ref(t.get())
**/
template<class Frame>
frame_ref_wrapper<Frame> frame_ref(frame_ref_wrapper<Frame> t) noexcept
{ return cframe_ref(t.get()); }

/**
* @brief rvalue reference wrapper is deleted.
**/
template <class T>
void frame_ref(T&&) = delete;

///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_FRAME_REF_WRAPPER_HPP
