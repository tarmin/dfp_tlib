/**
* @file core/from.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_FROM_HPP
#define DFPTL_CORE_FROM_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/detail/from_source.hpp"


namespace dfp
{
inline namespace core
{
inline namespace sources
{
///@addtogroup core_grp
///@{
/**
* @defgroup core_from_grp "from" source
* @brief Delivers frame stream sooner bufferized in the processing graph
*
* A @ref core_from_grp delivers frame stream sooner bufferized (with help of @ref core_probe_grp) in the processing graph.
*
* **Node interface**
* @startuml
* rectangle "o(n) = output(n) from cell at TAG" <<core::from(TAG)>> as src
* interface "output: ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* "produced"         | **output**  | same @ref stream_grp as output stream from cell at TAG
*
* **Usage example**
* @snippet core_cell_test.cpp core::from usage example
*
* @sa @ref core_probe_grp
* @{
**/


/**
* @brief Creates a @ref core_from_grp builder
*
* @param[in] fromTag a tag identifying the frame stream to retrieve in the graph processing
*
* @tparam FromTag is implicitly deduced from @c fromTag parameter and shall inherit from `custom_tag`
**/
template<class FromTag>
constexpr inline auto from(FromTag fromTag = FromTag{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<FromTag>, detail::from_source>()) /** @endcond */ 
{ return make_placeholder<std::tuple<FromTag>, detail::from_source>(); }


///@} core_from_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
using core::from;
} // namespace sources
} // namespace shortname


///@} core_grp
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_FROM_HPP
