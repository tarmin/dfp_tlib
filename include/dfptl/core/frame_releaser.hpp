/**
* @file core/frame_releaser.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_FRAME_RELEASER_HPP
#define DFPTL_CORE_FRAME_RELEASER_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/detail/frame_releaser_sink.hpp"


namespace dfp
{
inline namespace core
{
inline namespace sinks
{
///@addtogroup core_grp
///@{
/**
* @defgroup frame_releaser_grp "frame-releaser" sink
* @brief Releases one single frame from its single input.
*
* A @ref frame_releaser_grp releases one single frame acquired from its single input port when its \c run() method is invoked.
* It does not apply any transformation on captured frames.
*
* **Sink interface**
* @startuml
* rectangle "[i(k-1) i(k) ... i(k+L-1)] <- run() | with k the count of run() calls, L the frame length" <<frame_releaser()>> as sink
* interface "input-0: [i(0) i(1) ...i(L-1)] ..." <<in>> as in
* sink <-up- in
* @enduml

 * port name    | location    | stream specification | description
* --------------|-------------|----------------------|------------
* "in"          | **input-0** | any stream supported | consumes the frames to release
* @{
**/


/**
* @brief Creates a buildder for a @ref frame_releaser_grp.
*
* **Usage example**
* @snippet core_cell_test.cpp frame_releaser usage example
**/
constexpr inline auto frame_releaser() noexcept /** @cond */ ->
decltype(make_placeholder<detail::frame_releaser_sink>()) /** @endcond */
{ return make_placeholder<detail::frame_releaser_sink>(); }



/**
* @brief Creates a builder for a @ref frame_releaser_grp and auto run execution.
*
* The sink ultimately built with frame_releasing() is implicitly convertible into the frame captured on its input.
*
* **Usage example**
* @snippet core_cell_test.cpp frame_releasing usage example
**/
constexpr inline auto frame_releasing() noexcept /** @cond */ ->
decltype(make_placeholder<detail::frame_releaser_sink>().implicit_run()) /** @endcond */
{ return make_placeholder<detail::frame_releaser_sink>().implicit_run(); }


///@} frame_releaser_grp
///@} core_grp
} // inline namespace sinks
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_FRAME_RELEASER_HPP
