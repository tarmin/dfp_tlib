/**
* @file core/source.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_SOURCE_HPP
#define DFPTL_CORE_SOURCE_HPP


#include "dfptl/core/upstream_cell.hpp"
#include "dfptl/core/processing_cell.hpp"
#include "dfptl/core/processing_context.hpp"
#include "dfptl/core/initializable_cell.hpp"
#include "dfptl/core/detail/source_port.hpp"

namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup source_grp source processing cells
* @brief Sources are the upmost upstream cells of a processing branch.
*
* Any processing graph shall be actually started with one or more processing @ref core::source. 
* They help to acquire from external world the samples to stream into a processing graph.
* @{
**/


/**
* @brief Defines the start-up cell of a processing graph that helps to acquire from external world some samples or frames.
*
* ***For implementor*** of new @ref source_grp, any custom `source` <u>shall</u>
*  * inherit from this base @ref core::source class 
*  * implement <u>one and only one</u> of the `acquire_sample`, `acquire_frame` or `acquire_stream` methods
*     which transmit samples, frames or stream to the processing graph.
*
* @todo custom source implementation example
* @todo note on "apply" source
*
* @tparam OutputStreamSpecifier is the type of the @ref stream_specifier "stream specifier" describing the output stream of the source
* @tparam Derived topmost derived class from source (CRTP idiom)
*
* @extends core::upstream_cell
* @extends core::initializable_cell
**/
//TODO: turn UnmoveableRunnable into IsRunnableMoveable
template<class Context, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue = void, class Emitter = Derived, class Optimization = default_upstream_cell_optimization, bool UnmoveableRunnable = false>
class source
 :  public  upstream_cell<detail::source_output_port, OutputStreamSpecifier, Derived, Emitter, Optimization>,
    public  initializable_cell<std::tuple<>, InitStorageKeyValue, Derived, UnmoveableRunnable>,
    //TODO: move hosting of tag from stream_specifier to dedicated template parameter of source, sink and node
    public  processing_cell<Context, Derived>
{
    typedef upstream_cell<detail::source_output_port, OutputStreamSpecifier, Derived, Emitter, Optimization>    upstream_cell_type;
    typedef initializable_cell<std::tuple<>, InitStorageKeyValue, Derived, UnmoveableRunnable>                  initializable_cell_type;
    typedef processing_cell<Context, Derived>                                                                   processing_cell_type;

    friend class detail::source_output_port<Derived, OutputStreamSpecifier, Emitter>; // to allow calls to initialize_upwards() & build_initialization_storage()

    template<class T = Derived>  //templatized to avoid early evaluation of later static_assert
    struct concept_check
    { static_assert(
            detail::source_produces_sample<T>{} xor detail::source_produces_frame<T>{} xor detail::source_produces_stream<T>{},
            "Derived class of core::source shall implement one and only one of the acquire_sample(), acquire_frame() or acquire_stream() methods to comply with source concept"
    ); };


protected:
    template<class T = OutputStreamSpecifier, DFP_F_REQUIRES((T::IS_FRAME_LENGTH_STATIC))>
    DFP_CONSTEXPR_VOID initialize() const
    {}

    template<class T = OutputStreamSpecifier, DFP_F_REQUIRES((!T::IS_FRAME_LENGTH_STATIC))>
    void initialize() const
    {
        if (this->output_stream_specifier().get_frame_length() == DYNAMIC_EXTENT)
            throw std::logic_error("output frame length is unspecified");
    }


public:
    typedef source_tag              dfp_cell_tag;
    typedef OutputStreamSpecifier   output_specifier_type;
    typedef Derived                 derived;
    typedef InitStorageKeyValue     initialization_keyvalue_type;
    typedef Optimization            optimization;

    typedef typename initializable_cell_type::initialization_storage_type initialization_storage_type;

    using context_type      = typename processing_cell_type::context_type;
    using tag_map           = typename processing_cell_type::tag_map;
    template<class CellTag>
    using at_tag            = typename processing_cell_type::template at_tag<CellTag>;
    template<class CellTag>
    using at_tag_t = typename at_tag<CellTag>::type;

    static constexpr auto UNMOVEABLE_RUNNABLE = initializable_cell_type::UNMOVEABLE_RUNNABLE;

    using upstream_cell_type::output_stream_specifier;

   /** @brief Helper class to specify the emittter type at compile-time */
    template<class NewEmitter>
    struct set_emitter_class
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;

   /** @brief Helper class to specify the emittter type at compile-time */
    template<bool IsRunnableMoveable>
    struct set_runnable_moveable
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<bool IsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<IsRunnableMoveable>::type;

    /**
    * @brief Creates a new instance of @ref source whose given output stream_specifier is static.
    *
    * this constructor is only available when the given `OutputStreamSpecifier` type provides only well defined static properties.
    * (no DYNAMIC extends or DYNAMIC types are specified in the stream_specifier)
    *
    * @sa stream_specifier::IS_STATIC
    **/
    template<class T = OutputStreamSpecifier, DFP_F_REQUIRES((T::IS_STATIC))>
    // context is not used but I enforsee that context may also someday exposes some feature that we may want to spread to all cell base classes
    // (runtime tag - might be interesting for source tagging at runtime-, etc.) )
    constexpr source(Context const&) noexcept
     :  upstream_cell_type()
    { static_assert(std::is_class<decltype(concept_check<>{})>{}, ""); }

    //TODO: source specialization for initial_context as per definition this situation depends on the context while definition of a source constructor
    // shall be context agnostic
    template<class T = OutputStreamSpecifier, DFP_F_REQUIRES((T::IS_STATIC && std::is_same<initial_context, Context>{}))>
    constexpr source() noexcept
     :  source(initial_context{})
    {}

    /**
    * @brief Creates a new instance of @ref source
    *
    * @param output_specifier is a stream_specifier instance describing the output stream.
    **/
    constexpr source(Context const&, OutputStreamSpecifier const& output_specifier) : upstream_cell_type(output_specifier)
    { static_assert(std::is_class<decltype(concept_check<>{})>{}, ""); }

    /**
     * @brief Defines prototype of one of the `acquire` methods which shall be implemented in `Derived` of core::source.
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_sample_t<T> acquire_sample()
    { return std::declval<output_sample_t<T>>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_sample_t<T> acquire_sample() const
    { return std::declval<output_sample_t<T>>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_sample_t<T> const& acquire_sample()
    { return std::declval<output_sample_t<T> const&>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_sample_t<T> const& acquire_sample() const
    { return std::declval<output_sample_t<T> const&>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_frame_t<T> acquire_frame()
    { return std::declval<output_frame_t<T>>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_frame_t<T> acquire_frame() const
    { return std::declval<output_frame_t<T>>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_frame_t<T> const& acquire_frame()
    { return std::declval<output_frame_t<T> const&>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_frame_t<T> const& acquire_frame() const
    { return std::declval<output_frame_t<T> const&>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_stream_t<T> acquire_stream()
    { return std::declval<output_stream_t<T>>(); }

    /**
     * @copybrief acquire_sample
     */
    template<class T = source, DFP_F_REQUIRES((always_false<T>{}))>
    output_stream_t<T> acquire_stream() const
    { return std::declval<output_stream_t<T>>(); }

    /**
    * @brief default empty (doing nothing) implementation of initialize_upwards()
    *
    * In `Derived` class of core::source requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_upwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_upwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; }

    /**
    * @brief default empty (doing nothing) implementation of initialize_downwards()
    *
    * In `Derived` class of core::source requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_downwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_downwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; static_cast<Derived* const>(this)->initialize(); }
}; // class source


template<class Context, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, class Optimization, bool UnmoveableRunnable>
template<class NewEmitter>
struct source<Context, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, Optimization, UnmoveableRunnable>::set_emitter_class<NewEmitter>::update
{ typedef source<Context, OutputStreamSpecifier, Derived, InitStorageKeyValue, NewEmitter, Optimization, UnmoveableRunnable> type; };


template<class Context, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, class Optimization, bool UnmoveableRunnable>
template<bool UpdateIsRunnableMoveable>
struct source<Context, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, Optimization, UnmoveableRunnable>::set_runnable_moveable<UpdateIsRunnableMoveable>::update
{ typedef source<Context, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, Optimization, !UpdateIsRunnableMoveable> type; };


///@} source_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_SOURCE_HPP
