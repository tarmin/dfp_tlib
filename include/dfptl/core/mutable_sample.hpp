/**
* @file core/mutable_sample.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_CORE_MUTABLE_SAMPLE_HPP
#define DFPTL_CORE_MUTABLE_SAMPLE_HPP


#include "dfptl/core/detail/mutable_sample_source.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup mutable_sample_grp "mutable-sample" source
* @brief A @ref mutable_sample_grp feeds a processing branch with an mutable input sample.
*
* @todo A std::reference_wrapper on non-callable placed at the start of a processing branch
* will be implicitly converted into a mutable-sample source.
* @sa graph_operators_grp
*
* **Source interface**
* @startuml
* rectangle "o(n) = sample" <<mutable_sample(sample)>> as src
* interface "output: *&sample *&sample ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | an @ref atomic_stream "atomic stream" conveying the given mutable sample
**/
///@{


/**
* @brief Positions and creates a @ref detail::mutable_sample_source "mutable-sample source" in a processing branch.
*
* A mutable-sample source feeds a processing branch with an input sample which can be modified by or out of the branch.
*
* @todo A std::reference_wrapper placed at start of a processing branch will be implicitly converted into a mutable-sample source.
*
* @sa const_sample_grp
*
* @param sample is the data reference to inject in the processing branch.
*
* @tparam Sample is implicity deduded from `sample` parameter
*
* @return the source instance.
*
* **Usage example**
* @snippet core_test.cpp mutable_sample usage example
**/
template<class Sample>
inline auto mutable_sample(Sample const& sample) noexcept ->
decltype(make_placeholder<std::tuple<Sample>, detail::mutable_sample_source>(std::cref(sample)))
{ return make_placeholder<std::tuple<Sample>, detail::mutable_sample_source>(std::cref(sample)); }


template<class Sample>
inline auto mutable_sample(std::reference_wrapper<Sample> sample) noexcept ->
decltype(make_placeholder<std::tuple<Sample>, detail::mutable_sample_source>(sample))
{ return make_placeholder<std::tuple<Sample>, detail::mutable_sample_source>(sample); }


///@}
///@}
}// namespace core
} // namespace dfp


#endif // DFPTL_CORE_MUTABLE_SAMPLE_HPP
