/**
* @file core/upstream_cell.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_UPSTREAM_CELL_HPP
#define DFPTL_CORE_UPSTREAM_CELL_HPP


#include <dfptl/core/stream.hpp>
#include <dfptl/core/upstream_traits.hpp>


namespace dfp
{
inline namespace core
{
//forward declaration
namespace detail
{
template<class> class input_port;
template<class, class> class upstream_ref_source;
}


///@addtogroup core_grp
///@{
/** @defgroup upstream_grp upstream cells
* Common base for @ref source_grp and @ref node_grp processing cells
* @{
**/
/**
* @brief Upstream processing cell is element taking place in an upstream location of a processing branch.
*
* An upstream cell owns a single @ref output_port "output port".
* Basically, @ref node and @ref source are the upstream cell of a processing branch.
*
* @tparam OutputPort is the template type of the @ref output_port "output port" owned by the upstream cell.
* @tparam StreamSpecifier is the @ref stream_specifier "specifier" of the output stream.
* @tparam Derived topmost derived class from upstream_cell (CRTP idiom).
**/
template<template<class, class, class> class OutputPort, class StreamSpecifier, class Derived, class Emitter = Derived, class Optimization = default_upstream_cell_optimization, class = void>
class upstream_cell;


template<template<class, class, class> class OutputPort, class StreamSpecifier, class Derived, class Emitter>
class upstream_cell_base
{
    DFP_MUTABLE_CONSTEXPR  Derived* const child()
    { return static_cast<Derived* const>(this); }

    constexpr  Derived const* const child() const
    { return static_cast<Derived const* const>(this); }

public:
    typedef OutputPort<Derived, StreamSpecifier, Emitter>   output_port_type;
    typedef stream<StreamSpecifier, Emitter>                output_stream_type;
    typedef StreamSpecifier                                 output_specifier_type;
     // Indicator to help framework to optimize graph as long as the output stream has been fully specified at construction time
     // It should be overriden in specialized class if any
    static constexpr bool IS_STREAM_EARLY_SPECIFIED = output_specifier_type::IS_STATIC;

    /**
    * @brief Returns the processing stream produced by the processing branch terminated by this cell.
    **/
    //TODO: to be removed
    template<class T = output_stream_type>
    DFP_MUTABLE_CONSTEXPR T stream_out()
    { return child()->output_port().renew_stream(*child()); }

    /**
    * @brief const variant of stream_out()
     **/
    template<class T = typename output_stream_type::to_const::type>
    constexpr T stream_out() const
    { return child()->output_port().renew_stream(*child()); }

    /**
    * @brief Returns a frame produced by just firing the processing branch terminated by this cell.
    **/
    //TODO: to be replaced with output_port::renew_frame
    template<class T = output_stream_type>
    decltype(std::declval<T>().drain())
    DFP_MUTABLE_CONSTEXPR drain()
    { return stream_out<T>()  .drain(); }

    /**
    * @brief const variant of drain()
    **/
    template<class T = typename output_stream_type::to_const::type>
    decltype(std::declval<T>().drain())
    constexpr drain() const
    { return stream_out<T>()  .drain(); }

    /**
    * @brief Returns a const frame produced by just firing the processing branch terminated by this cell.
    **/
    template<class T = typename output_stream_type::to_const::type>
    decltype(std::declval<T>().cdrain())
    constexpr cdrain() const
    { return stream_out<T>()  .cdrain(); }

    // this lazy overloading helps to detect compile error on stream_out<T> construction
    // Due to it, compiler will provide more accurate error message
    template<class T = typename output_stream_type::to_const::type, class... O>
    frame_t<T>
    constexpr cdrain(O...) const
    { return stream_out<T>()  .cdrain(); }

    /**
    * @brief Gets @ref stream_specifier "specifier" of the output stream
    **/
    constexpr output_specifier_type output_stream_specifier() const noexcept
    { return child()->output_port().stream_specifier(); }

protected:
    /**
    * @brief Sets @ref stream_specifier "specifier" of the output stream
    **/
    //TODO: add compile-time and runtime check for frame length divisible by channel count
    void set_output_stream_specifier(output_specifier_type const& src)
    { child()->output_port().set_stream_specifier(src); }
}; // class upstream_cell_base


template<template<class, class, class> class OutputPort, class StreamSpecifier, class Derived, class Emitter, class Optimization>
class upstream_cell<OutputPort, StreamSpecifier, Derived, Emitter, Optimization, DFP_C_REQUIRES((
    detail::upstream_cell_constraint::discard_ebo || !Optimization::enable_ebo
))> : public    upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter>
{
    template<class, class, class> friend class detail::stream;
    friend class upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter>;
    template<class> friend class detail::input_port;
    template<class, class> friend class detail::upstream_ref_source;

    typedef     upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter> base;

public:
    typedef typename base::output_port_type         output_port_type;
    typedef typename base::output_specifier_type    output_specifier_type;
    typedef typename base::output_stream_type      output_stream_type;

    using base::drain;
    using base::cdrain;

    template<class T = StreamSpecifier, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr upstream_cell() noexcept {}

    constexpr upstream_cell(StreamSpecifier const& stream_specifier) : output_port_(stream_specifier)
    {}

protected:
    /**
    * @brief Returns the reference on the cell output_port.
    *
    * @note As this function is protected, only an @ref input_port or a @ref detail::processing_branch
    * can access to output_port of a cell.
    */
    DFP_MUTABLE_CONSTEXPR output_port_type&  output_port()    { return output_port_; }
    constexpr output_port_type const&    output_port() const  { return output_port_; }

private:
    output_port_type output_port_;
}; // class upstream< ... !EBO ... >


template<template<class, class, class> class OutputPort, class StreamSpecifier, class Derived, class Emitter, class Optimization>
class upstream_cell<OutputPort, StreamSpecifier, Derived, Emitter, Optimization, DFP_C_REQUIRES((
    !detail::upstream_cell_constraint::discard_ebo && Optimization::enable_ebo
))>
 :  private OutputPort<Derived, StreamSpecifier, Emitter>,
    public  upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter>
{
    template<class, class, class> friend class detail::stream;
    friend class upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter>;
    template<class> friend class detail::input_port;
    template<class, class> friend class detail::upstream_ref_source;

    typedef upstream_cell_base<OutputPort, StreamSpecifier, Derived, Emitter> base;

public:
    typedef typename base::output_port_type         output_port_type;
    typedef typename base::output_specifier_type    output_specifier_type;
    typedef typename base::output_stream_type       output_stream_type;
    typedef Derived const                           const_type; // define default type if not overriden by child
#if defined (__clang__) && (__clang_major__ <= 3)
    // workaround for visibility issue (with clang3) of member of public type member when same type is also a private base class
    typedef typename output_port_type::cell_type cell_type;
    typedef typename output_port_type::stream_specifier_type stream_specifier_type;
#endif

    using base::drain;
    using base::cdrain;

    template<class T = StreamSpecifier, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr upstream_cell() noexcept {}

    constexpr upstream_cell(StreamSpecifier const& stream_specifier)
     :  output_port_type(stream_specifier)
    {}

protected:
    /**
    * @brief Returns the reference on the cell @ref output_port.
    *
    * @note As this function is protected, only an @ref input_port or a @ref detail::processing_branch
    * can access to @ref output_port of a cell.
    */
    DFP_MUTABLE_CONSTEXPR output_port_type&  output_port()   { return output_port_type::get(); }
    constexpr output_port_type const& output_port() const    { return output_port_type::get(); }
}; // class upstream_cell<... EBO ...>


template<class StreamSpecifier>
struct iupstream_cell
{
    virtual frame_t<StreamSpecifier> const& drain() = 0;
};


///@} upstream_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_UPSTREAM_CELL_HPP
