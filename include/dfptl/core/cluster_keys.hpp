/**
* @file core/cluster_keys.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CLUSTER_KEYS_HPP
#define DFPTL_CORE_CLUSTER_KEYS_HPP


#include "dfptl/core/type_traits.hpp"


namespace dfp
{
inline namespace core
{
/**
* @brief Set of keys type for access fields in clustered data with help of
* [boost::fusion::at_keys](https://www.boost.org/doc/libs/1_68_0/libs/fusion/doc/html/fusion/sequence/intrinsic/functions/at_key.html)
**/
namespace keys
{
/** 
* @addtogroup core_grp
* @{
* @defgroup cluster_stream_grp clustered data stream
* @brief Clustered data stream conveys data with structured type (struct or class) which can be manipulated
* with help of dedicated nodes of DFPTL.
*
* A stream is a clustered data stream as long as its conveyed data is a [boost-fusion associative sequence]
* (https://www.boost.org/doc/libs/1_69_0/libs/fusion/doc/html/fusion/sequence/concepts/associative_sequence.html)
*
* @note that any struct or class type can be adapted into a boost-fusion associative sequence using
* [one of the boost helper macros](https://www.boost.org/doc/libs/1_69_0/libs/fusion/doc/html/fusion/adapted.html).
* See sming/detail/dht_esp_source.hpp for adaption example.
*
* Couple of DFP dedicated nodes help to handle clustered stream:
* * @ref extract() "extract node"
*
* As an example, following piece of code extract the temperature signal from the clustered stream
* (of temperature and humidity data) outputted from a DHT source.
*
* @code{.cpp}
* #include <dftpl/sming/dht_esp.hpp>
*
* void init()
* {
*   using namespace dfp;
*   using namespace dfp::basic;
* ...
*   auto processing_branch =
*       logic::schmitt_trigger(18.5, 19) <<= extract(keys::TEMPERATURE) <<= sming::dht_esp(DHT21_PIN, DHTesp::DHT22);
* ...
* }
* @endcode
* @{
*/
///@defgroup keys keys type of clustered data
///@{



struct humidity     {}; /*!< @brief keys type to retrieve humidity value in associative sequence */
struct light        {}; /*!< @brief keys type to retrieve light value in associative sequence */
struct temperature  {}; /*!< @brief keys type to retrieve temperature value in associative sequence */
struct timestamp    {}; /*!< @brief keys type to retrieve timestamp value in associative sequence */


/** @brief unique constant of type @ref humidity key */
DFP_INLINE_CONSTEXPR auto const HUMIDITY    = humidity{};
/** @brief unique constant of type @ref light key */
DFP_INLINE_CONSTEXPR auto const LIGHT       = light{};
/** @brief unique constant of type @ref temperature key */
DFP_INLINE_CONSTEXPR auto const TEMPERATURE = temperature{};
/** @brief unique constant of type @ref timestamp key */
DFP_INLINE_CONSTEXPR auto const TIMESTAMP   = timestamp{};


///@}
///@} cluster_stream_grp
///@} core_grp
} // namespace keys
} // namespace core
} // namespace dfp


#endif //DFPTL_CORE_CLUSTER_KEYS_HPP
