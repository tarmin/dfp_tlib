/**
* @file core/graph_operators.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_GRAPH_OPERATORS_HPP
#define DFPTL_CORE_GRAPH_OPERATORS_HPP


#include "dfptl/core/detail/processing_graph.hpp"
#include "dfptl/core/graph_designer.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup graph_operators_grp processing graph operators
* @brief The @ref graph_operators_grp help to to associate processing cells all together to create a tree of linked list of processing cells.
*
* A linked list terminated by a @ref node_grp "node" forms a **processing branch**.
* When terminated by a @ref sink_grp "sink", the linked list is a **processing tree** ready for running.
*
* A processing branch shall be initiated by a @ref source_grp "source". Thus if the first element of the linked list is not a source,
* the given input is implicitly converted into a @ref source with help of @ref apply() if it is an invocable or @ref const_sample() otherwise.
*
* The 3 following C++ operators are overloaded in DFP_TLIB to create a DSL for textual dataflow programming.
* * The \c operator<<= is called the **extend** operator. It extends the processing branch on its right side with the given cell builder on its left side.
* @note The "extend" operator links its right hand side to the left hand side, so processing branch or tree shall be read from right to left.
* * The \c operator|=, the **expand** operator initiates a new branch in the processing graph in construction.
* The left side of the operator shall be a @ref source cell or convertible into a source.
* * The \c operator+=, the **collect** operator aggregates all branches part of the processing graph on its right hand side and connect them as inputs
* for the left hand cell (node or sink)
*
* **Usage example**
* Just below are represented the both processing trees created in following source code.
* @startuml
* rectangle "routing::sample_releasing()" <<sink>> as sample_releasing
* rectangle "[](int const& in){ return in*2; }" <<node>> as callable
* rectangle "alu::counter()" <<source>> as counter
*
* sample_releasing <- callable
* callable <- counter
* @enduml
* @startuml
* rectangle "routing::looping(container_size*2)" <<sink>> as looping
* rectangle "io::cout_csv()" <<node>> as cout_csv
* rectangle "alu::equal_to(3)" <<node>> as equal
* rectangle "alu::modulo(4)" <<node>> as modulo
* rectangle "alu::counter(1)" <<source>> as counter
* rectangle "alu::multiply(2)" <<node>> as multiply
* rectangle "node::iterate()" <<node>> as iterate
* rectangle "container" <<source>> as container
*
* looping <- cout_csv
* cout_csv <- equal
* equal <- modulo
* modulo <- counter
* cout_csv <-down- multiply
* multiply <- iterate
* iterate <- container
* @enduml

* @include operators_overview_example.cpp
**/
///@{


/**
* @brief Turn the right hand side into a processing branch and extends it with some newly created @ref core::node or @ref core::sink depicted by the left hand side.
*
* **Example**
* @snippet operators_overview_example.cpp Extend operator
*
* @param[in] lhs is a cell builder or objet convertible into cell builder of new node or sink to add to the processing graph.
* @param[in] rhs is  processing graph or an upstream cell or object convertible into source to serve as input for the node or sink to create.
*
* @note Even when the given @c rhs is actually a lvalue-reference, the input branch is copied. For actual binding of a input reference,
* wrap the reference with `std::ref`.
*
* @tparam Lhs type is implicitly deduced from @c lhs argument.
* @tparam Rhs type is implicitly deduced from @c rhs argument.
**/
template<class Lhs, class Rhs, DFP_F_REQUIRES((
    !detail::is_processing_tree<decltype(graph_designer<>::instance().extend(std::declval<Rhs>(), std::declval<Lhs>()))>{}
))>
constexpr auto operator<<=(Lhs&& lhs, Rhs&& rhs)
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs))) /** @endcond */
#endif
{ return graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)); }


template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_tree<decltype(graph_designer<>::instance().extend(std::declval<Rhs>(), std::declval<Lhs>()))>{}
    &&  decltype(graph_designer<>::instance().extend(std::declval<Rhs>(), std::declval<Lhs>()))::REQUIRES_IMPLICIT_RUN
))>
constexpr auto operator<<=(Lhs&& lhs, Rhs&& rhs) /** @cond */ ->
running_tree<decltype(graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)))> /** @endcond */
{ return { graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)) }; }


template<class Lhs, class Rhs, DFP_F_REQUIRES((
        detail::is_processing_tree<decltype(graph_designer<>::instance().extend(std::declval<Rhs>(), std::declval<Lhs>()))>{}
    &&  !decltype(graph_designer<>::instance().extend(std::declval<Rhs>(), std::declval<Lhs>()))::REQUIRES_IMPLICIT_RUN
))>
constexpr auto operator<<=(Lhs&& lhs, Rhs&& rhs)
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs))) /** @endcond */
#endif
{ return graph_designer<>::instance().extend(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)); }


/**
* @brief Expands the processing graph on the right side with some new branch created from a @ref core::source or object convertible to branch on the left hand side.
*
* **Example**
* @snippet operators_overview_example.cpp Linking to multiple input cells
*
* @param[in] lhs is a source or objet convertible downstream cell builder.
* @param[in] rhs is some processing graph or object convertible into a processing graph).
*
* @tparam Lhs type is implicitly deduced from @c lhs argument.
* @tparam Rhs type is implicitly deduced from @c rhs argument.
**/
template<class Lhs, class Rhs>
constexpr auto operator|=(Lhs&& lhs, Rhs&& rhs) /** @cond */ ->
decltype(graph_designer<>::instance().expand(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs))) /** @endcond */
{ return graph_designer<>::instance().expand(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)); }


/**
* @brief Collects all unterminated branches of the processing graph on the right hand side and create a downstream cell fed with all those input branches to extend te processing graph.
*
* **Example**
* @snippet operators_overview_example.cpp Linking to multiple input cells
*
* @param[in] lhs is a cell builder or objet convertible into cell builder of new node or sink to add to the processing graph.
* @param[in] rhs is some processing graph or object convertible into a processing graph whose all unterminated branches are bound as the inputs for the node or sink to create.
*
* @tparam Lhs type is implicitly deduced from @c lhs argument.
* @tparam Rhs type is implicitly deduced from @c rhs argument.
**/
//TODO: to be renamed into "join" or "assemble" ?
template<class Lhs, class Rhs, DFP_F_REQUIRES((
    !detail::is_processing_tree<decltype(graph_designer<>::instance().collect(std::declval<Rhs>(), std::declval<Lhs>()))>{}
))>
constexpr auto operator+=(Lhs&& lhs, Rhs&& rhs) /** @cond */ ->
decltype(graph_designer<>::instance().collect(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs))) /** @endcond */
{ return graph_designer<>::instance().collect(std::forward<Rhs>(rhs), std::forward<Lhs>(lhs)); }


///@} graph_operators_grp
///@} core_grp
} // namespace core
} // namespace dfp
/**
* @example operators_overview_example.cpp
* This example presents various use-cases of usage for the @ref graph_operators_grp.
**/


#endif // DFPTL_CORE_GRAPH_OPERATORS_HPP
