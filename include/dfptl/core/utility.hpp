/**
* @file core/utility.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_UTILITY_HPP
#define DFPTL_CORE_UTILITY_HPP


#include "dfptl/core/type_traits.hpp"
#include "dfptl/core/compile_error.hpp"
#include "dfptl/core/integral_const.hpp"

#include <vector>
#include <string>
#include "stl/utility.hpp"


namespace dfp
{
inline namespace core
{
template<class T, class> struct is_stream;
template<class T, class> struct is_stream_specifier;

namespace detail
{
template<class T, class> struct is_frame_traits;
template<class T, class> struct models_frame_concept;
template<class Frame> struct frame_traits;
}


///@addtogroup core_grp
///@{
/** @defgroup utility utility types
* @{
**/


/**
* @brief Defines type alias of C-array T[Size]
*
* This can be helpful for creating rvalue of C-array
**/
template<class T, std::size_t Size>
using c_array = T[Size];


/**
* @brief Provides typedef @c type with the @c T stream specifier updated with the given @c New sample
**/
//TODO: to be removed
template<
    class New, class T, class = void,
    class = DFP_CONCEPT_ASSERT((std::disjunction<detail::models_frame_concept<T, void>, is_stream_specifier<T, void>, detail::is_frame_traits<T, void>>))
>
struct set_sample
{ typedef typename detail::frame_traits<T>::template set_sample<New>::type type; };

template<class New, class T>
struct set_sample<New, T, DFP_C_REQUIRES((!is_stream_specifier<T, void>{} && !detail::models_frame_concept<T, void>{}))>
{ typedef typename T::template set_sample<New>::type type; };

template<class New, class T>
struct set_sample<New, T, DFP_C_REQUIRES((is_stream_specifier<T, void>{}))>
{ typedef typename T::template set_sample_clue<New>::type type; };


/**
* @brief Alias for set_sample<New, T>
**/
template<class New, class T>
using set_sample_t = typename set_sample<New, T>::type;


/**
* @brief Provides typedef @c type with the @c T stream specifier updated with the given @c New frame
**/
template<class New, class T, class = DFP_CONCEPT_ASSERT((is_stream_specifier<T, void>))>
struct set_frame
{ typedef typename T::template set_frame<New>::type type; };


/**
* @brief Alias for set_frame<New, T>
**/
template<class New, class T>
using set_frame_t = typename set_frame<New, T>::type;



/**
* @brief Provides typedef @c type with the @c T stream associated with the given @c Tag
**/
template<class Tag, class Stream, class = DFP_CONCEPT_ASSERT((is_stream<Stream, void>))>
struct at_tag
{ typedef typename Stream::template at_tag_t<Tag> type; };


/**
* @brief Alias for at_tag<Stream, Tag>
**/
template<class Tag, class Stream>
using at_tag_t = typename at_tag<Tag, Stream>::type;


template<class C, class T, class = void>
struct add_const_if_const
{ typedef T type; };


template<class C, class T>
struct add_const_if_const<C, T, DFP_C_REQUIRES((std::is_const<C>{}))>
{ typedef std::add_const_t<T> type; };


template<class C, class T>
using add_const_if_const_t = typename add_const_if_const<C, T>::type;


template<class Derived>
struct element_meta {
    typedef Derived derived;

    static constexpr auto PARENT    = nullptr;

    static constexpr const char* const name() { return Derived::NAME; }
    static constexpr bool has_parent() { return !std::is_same<decltype(Derived::PARENT), std::nullptr_t const>{}; }
    template<class T = Derived>
    static constexpr decltype(T::PARENT) parent() { return T::PARENT; }
    template<class T = Derived, DFP_F_REQUIRES(( T::has_parent() ))>
    static std::string fully_qualified_name() { return parent().fully_qualified_name() + "::" + name(); }
    template<class T = Derived, DFP_F_REQUIRES(( !T::has_parent() ))>
    static std::string fully_qualified_name() { return name(); }
    template<class T = Derived, DFP_F_REQUIRES(( T::has_parent() ))>
    static constexpr std::size_t parent_count() { return parent().parent_count() + 1; }
    template<class T = Derived, DFP_F_REQUIRES(( !T::has_parent() ))>
    static constexpr std::size_t parent_count() { return 0; }
};


struct dynamic_type_tag {};


template<class Type> struct resolve_type_meta;

template<class Template> struct resolve_template_meta;


template<class Type, class Derived = typename resolve_type_meta<Type>::type, class =  void>
struct type_meta;


/**
* @brief Holds type metadata
**/
template<class Type, class Derived>
struct type_meta_base : element_meta<Derived> {
    typedef Type type;
    typedef Derived derived;

    static constexpr bool has_super() { return false; }

    template<class OtherType, DFP_F_REQUIRES((std::is_same<OtherType, Type>{} && !std::is_same<dynamic_type_tag, Type>{}))>
    constexpr bool operator==(type_meta<OtherType> const& rhs) const
    { return true; }

    template<class OtherType, DFP_F_REQUIRES((!std::is_same<OtherType, Type>{} && !std::is_same<dynamic_type_tag, Type>{}))>
    constexpr bool operator==(type_meta<OtherType> const& rhs) const
    { return false; }

    template<class OtherType, DFP_F_REQUIRES((std::is_same<dynamic_type_tag, OtherType>{}  || std::is_same<dynamic_type_tag, Type>{}))>
    constexpr bool operator==(type_meta<OtherType> const& rhs) const
    { return (this->name() == rhs.name()) && (this->parent() == rhs.parent()); }

    operator char const*() const
    { return this->name(); }
};


template<class Type, class Derived>
struct type_meta<Type, Derived> : type_meta_base<Type, Derived>
{};


//TODO: to be improved to bring out value type of the integral_const
struct integral_const_meta : type_meta_base<void, integral_const_meta> {
    constexpr integral_const_meta() {}
    static constexpr auto NAME      = "integral_const<?>"; // TODO: implement helper for joining of static constexpr string
};


template<class IntegralConst>
struct type_meta<IntegralConst, integral_const_meta, DFP_C_REQUIRES((is_integral_constant<IntegralConst>{}))>
{
    type_meta() {}

    // any integral constant is implicitly convertible into a type_meta
    type_meta(IntegralConst) {}
}; // type_meta<... is_integral_const ...>


/**
* @brief Holds type template metadata
**/
template<class Template, class Derived = typename resolve_template_meta<Template>::type>
struct template_meta : element_meta<Derived> {
    typedef Template template_type;
    typedef Derived derived;
};


/**
* @brief Holds type metadata
**/
template<class Derived>
struct package_meta : element_meta<Derived> {
    typedef Derived derived;
};

template<class = void>
struct std_meta : package_meta<std_meta<>> {
    static constexpr auto NAME      = "std";
};
DFP_INLINE_CONSTEXPR std_meta<> STD;

template<class = void>
struct dfp_meta : package_meta<dfp_meta<>> {
    static constexpr auto NAME      = "dfp";
};
DFP_INLINE_CONSTEXPR dfp_meta<> DFP;


template<class T, class = void>
struct unknown_type_meta : type_meta<T, unknown_type_meta<T>> {
    static constexpr auto NAME      = "?";
};
template<class Type> struct resolve_type_meta { typedef unknown_type_meta<Type, void> type; };

template<class T, class = void>
struct unknown_template_meta : template_meta<T, unknown_template_meta<T>> {
    static constexpr auto NAME      = "?<?>";
};
template<class Type> struct resolve_template_meta { typedef unknown_template_meta<Type, void> type; };

template<class Referent>
struct pointer_meta : type_meta<Referent*, pointer_meta<Referent>> {
    static constexpr auto NAME      = "*";

    typedef type_meta<Referent> referent_meta;
};
template<class Referent> struct resolve_type_meta<Referent*> { typedef pointer_meta<Referent> type; };

template<class Unqualified>
struct const_meta : type_meta<Unqualified const, const_meta<Unqualified>> {
    static constexpr auto NAME      = "const";

    typedef type_meta<Unqualified> unqualified_meta;
};
template<class Unqualified> struct resolve_type_meta<Unqualified const> { typedef const_meta<Unqualified> type; };

template<class = void>
struct void_meta : type_meta<void, void_meta<>> {
    static constexpr auto NAME      = "void";
};
template<> struct resolve_type_meta<void> { typedef void_meta<> type; };
DFP_INLINE_CONSTEXPR void_meta<> VOID;

template<class = void>
struct bool_meta : type_meta<bool, bool_meta<>> {
    static constexpr auto NAME      = "bool";
};
template<> struct resolve_type_meta<bool> { typedef bool_meta<> type; };
DFP_INLINE_CONSTEXPR bool_meta<> BOOL;

template<class = void>
struct int32_t_meta : type_meta<std::int32_t, int32_t_meta<>> {
    static constexpr auto NAME      = "int32_t";
    static constexpr auto PARENT    = STD;
};
template<> struct resolve_type_meta<std::int32_t> { typedef int32_t_meta<> type; };
DFP_INLINE_CONSTEXPR int32_t_meta<> INT32_T;

template<class = void>
struct uint32_t_meta : type_meta<std::uint32_t, uint32_t_meta<>> {
    static constexpr auto NAME      = "uint32_t";
    static constexpr auto PARENT    = STD;
};
template<> struct resolve_type_meta<std::uint32_t> { typedef uint32_t_meta<> type; };
DFP_INLINE_CONSTEXPR uint32_t_meta<> UINT32_T;

template<class = void>
struct int16_t_meta : type_meta<std::int16_t, int16_t_meta<>> {
    static constexpr auto NAME      = "int16_t";
    static constexpr auto PARENT    = STD;
};
template<> struct resolve_type_meta<std::int16_t> { typedef int16_t_meta<> type; };
DFP_INLINE_CONSTEXPR int16_t_meta<> INT16_T;


DFP_INLINE_CONSTEXPR typename resolve_type_meta<std::size_t>::type SIZE_T;
DFP_INLINE_CONSTEXPR typename resolve_type_meta<unsigned int>::type UINT;


template<class = void>
struct float_meta : type_meta<float, float_meta<>> {
    static constexpr auto NAME      = "float";
};
template<> struct resolve_type_meta<float> { typedef float_meta<> type; };
DFP_INLINE_CONSTEXPR float_meta<> FLOAT;

template<class = void>
struct double_meta : type_meta<double, double_meta<>> {
    static constexpr auto NAME      = "double";
};
template<> struct resolve_type_meta<double> { typedef double_meta<> type; };
DFP_INLINE_CONSTEXPR double_meta<> DOUBLE;

template<class = void>
struct string_meta : type_meta<std::string, string_meta<>> {
    static constexpr auto NAME      = "string";
    static constexpr auto PARENT    = STD;
};
template<> struct resolve_type_meta<std::string> { typedef string_meta<> type; };
DFP_INLINE_CONSTEXPR string_meta<> STRING;

template<class = void>
struct vector_meta : template_meta<std::vector<void>, vector_meta<>> {
    static constexpr auto NAME      = "vector";
    static constexpr auto PARENT    = STD;
};
template<class T, class A> struct resolve_template_meta<std::vector<T, A>> { typedef vector_meta<> type; };
DFP_INLINE_CONSTEXPR vector_meta<> VECTOR;

template<class = void>
struct array_meta : template_meta<std::array<void, 0>, array_meta<>> {
    static constexpr auto NAME      = "array";
    static constexpr auto PARENT    = STD;
};
template<class T, std::size_t N> struct resolve_template_meta<std::array<T, N>> { typedef array_meta<> type; };
DFP_INLINE_CONSTEXPR array_meta<> ARRAY;

template<class = void>
struct null_tag_meta : type_meta<detail::null_tag, null_tag_meta<>> {
    static constexpr auto NAME      = "null_tag";
    static constexpr auto PARENT    = DFP;
};
template<> struct resolve_type_meta<detail::null_tag> { typedef null_tag_meta<> type; };
DFP_INLINE_CONSTEXPR null_tag_meta<> NULL_TAG;


struct dynamic_element_meta : element_meta<dynamic_element_meta> {
    virtual const char* actual_name() const = 0;
    virtual dynamic_element_meta const& actual_parent() const = 0;
};


struct dynamic_type_meta : type_meta<dynamic_type_tag, dynamic_type_meta>, dynamic_element_meta {
    static constexpr auto NAME      = nullptr; // nullptr indicates here dynamic type
};
template<> struct resolve_type_meta<dynamic_type_tag> { typedef dynamic_type_meta type; };

struct dynamic_template_tag {};

struct dynamic_template_meta : template_meta<dynamic_template_tag, dynamic_template_meta>, dynamic_element_meta {
    static constexpr auto NAME      = nullptr; // nullptr indicates here dynamic type

};
template<> struct resolve_template_meta<dynamic_template_tag> { typedef dynamic_template_meta type; };

template<class T, class = void>
struct is_type_meta : std::false_type {}; // required for non-class types

template<class T>
struct is_type_meta<T, DFP_C_REQUIRES((std::is_base_of<type_meta<typename T::type, typename T::derived>, T>{}))> : std::true_type {};


template<class T, class = void>
struct is_template_meta : std::false_type {}; // required for non-class types

template<class T>
struct is_template_meta<T, DFP_C_REQUIRES((std::is_base_of<template_meta<typename T::template_type, typename T::derived>, T>{}))> : std::true_type {};


template<class MetaType, class = void>
struct dynamic_element_meta_adapter;


template<class MetaType>
struct dynamic_element_meta_adapter<MetaType, DFP_C_REQUIRES((MetaType::PARENT != nullptr))> : dynamic_element_meta
{
    virtual const char* actual_name() const override
    { return MetaType::name(); }

    virtual dynamic_element_meta const& actual_parent() const override
    { return dynamic_element_meta_adapter<MetaType>{}.actual_parent(); }

    static const dynamic_element_meta_adapter instance;
};


struct root_package : dynamic_element_meta
{
    virtual const char* actual_name() const override
    { return ""; }

    virtual dynamic_element_meta const& actual_parent() const override
    { return instance; }

    static const root_package instance;
};


template<class MetaType>
struct dynamic_element_meta_adapter<MetaType, DFP_C_REQUIRES((MetaType::PARENT == nullptr))> : dynamic_element_meta
{
    virtual const char* actual_name() const override
    { return MetaType::name(); }

    virtual dynamic_element_meta const& actual_parent() const override
    { return root_package::instance; }

    static const dynamic_element_meta_adapter instance;
};


template<class MetaType, class = DFP_CONCEPT_ASSERT((
    std::conjunction<is_type_meta<MetaType>, std::negation<std::is_same<MetaType, type_meta<dynamic_type_tag>>>>
))>
struct dynamic_type_meta_adapter : dynamic_type_meta
{
    virtual const char* actual_name() const override
    { return MetaType::name(); }

    virtual dynamic_element_meta const& actual_parent() const override
    { return dynamic_element_meta_adapter<MetaType>{}.actual_parent(); }

    static const dynamic_type_meta_adapter instance;
};


template<class MetaTemplate, class = DFP_CONCEPT_ASSERT((
    std::conjunction<is_template_meta<MetaTemplate>, std::negation<std::is_same<MetaTemplate, template_meta<dynamic_template_tag>>>>
))>
struct dynamic_template_meta_adapter : dynamic_template_meta
{
    virtual const char* actual_name() const override
    { return MetaTemplate::name(); }

    virtual dynamic_element_meta const& actual_parent() const override
    { return dynamic_element_meta_adapter<MetaTemplate>{}.actual_parent(); }
};


/**
* @brief Converts a @ref element_meta into a @ref dynamic_element_meta
**/
template<class MetaType>
inline constexpr dynamic_element_meta_adapter<MetaType> to_dynamic_element_meta(MetaType meta)
{ return dynamic_element_meta_adapter<MetaType>{}; }


/**
* @brief Converts a @ref type_meta into a @ref dynamic_type_meta
**/
template<class MetaType>
inline constexpr dynamic_type_meta_adapter<MetaType> to_dynamic_type_meta(MetaType meta)
{ return dynamic_type_meta_adapter<MetaType>{}; }


template<class MetaTemplate>
inline constexpr dynamic_template_meta_adapter<MetaTemplate> to_dynamic_template_meta(MetaTemplate meta)
{ return dynamic_template_meta_adapter<MetaTemplate>{}; }


/** @brief Indicates that extent value is unknown at compile-time */
DFP_INLINE_CONSTEXPR size_t_const<upg::dynamic_extent> DYNAMIC_EXTENT = size_t_const<upg::dynamic_extent>{};
/* TODO: consider opportunity to add some other new extent types, e.g.:
* FIXED_EXTENT: for extent fixed at construction time
* BOUNDED_EXTENT: for variable but limited frame length
*/ 


namespace detail
{


// yes, it is so weird ... but it is required !!
// for some unexplained reason gcc does not resolve std::make_index_sequence<> to index_sequence without this
// (likely because of no inheritance relationship between make_index_sequence and index_sequence
template<std::size_t... Indexes>
std::index_sequence<Indexes...> as_index_sequence(std::index_sequence<Indexes...>)
{ return std::index_sequence<Indexes...>{}; }


}

template<class Tuple>
struct index_sequence_from
{ typedef decltype(detail::as_index_sequence(std::make_index_sequence<std::tuple_size<Tuple>{}>{})) type; };


template<class Tuple>
using index_sequence_from_t = typename index_sequence_from<Tuple>::type;


namespace result_of
{
template<class... Args>
struct tuple_cat
{
    typedef decltype(std::tuple_cat(std::declval<Args>()...)) type;
}; // tuple_cat

template<class... Args>
using tuple_cat_t = typename tuple_cat<Args...>::type;

} // namespace result_of


template<class... Args>
struct but_last;

template<>
struct but_last<>
{ typedef std::tuple<> type; };

template<class Arg>
struct but_last<Arg>
{ typedef std::tuple<> type; };

template<class Arg0, class... Args>
struct but_last<Arg0, Args...>
{ typedef result_of::tuple_cat_t<std::tuple<Arg0>, typename but_last<Args...>::type> type; };

template<class... Args>
using but_last_t = typename but_last<Args...>::type;


template<class... Args>
struct but_first;

template<>
struct but_first<>
{ typedef std::tuple<> type; };

template<class Arg>
struct but_first<Arg>
{ typedef std::tuple<> type; };

template<class Arg0, class... Args>
struct but_first<Arg0, Args...>
{ typedef std::tuple<Args...> type; };

template<class... Args>
using but_first_t = typename but_first<Args...>::type;


template<class... ClassArgs>
struct constructor_parameters_type_check
{
    template<class Verifier>
    constructor_parameters_type_check(Verifier, typename Verifier::template check_type<ClassArgs...> = {})
    {}
}; //struct constructor_parameters_type_check


///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_UTILITY_HPP
