/**
* @file core/running_tree.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_RUNNING_TREE_HPP
#define DFPTL_CORE_RUNNING_TREE_HPP


#include "dfptl/core/runnable_tree.hpp"

#include <cstddef>


namespace dfp
{
inline namespace core
{


///@addtogroup core_grp
///@{
/** @defgroup running_tree_grp "running" processing tree
* @brief A @ref runnable_tree_grp which is automatically run once during its construction.
* @{
**/

/**
* @brief Turns a declarative processing tree into a new tree which has been run
*
* This specialization of the running_tree class does not produce result.
*
* At instance creation, this class simply copies or takes ownership of a given processing tree and performs its initialization and run it.
**/
template<class ProcessingTree, class = void>
struct running_tree final
 : runnable_tree<ProcessingTree>
{
    typedef void value_type;

    /**
    * @brief Take ownership on the given processing tree and run it
    * @param tree the declarative processing tree to appropriate
    **/
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) running_tree(ProcessingTree&& tree)
     :  runnable_tree<ProcessingTree>::runnable_tree(std::move(tree))
    { this->run(); }

    /**
    * @brief Copy on the given processing tree and run it
    * @param tree the declarative processing tree to copy
    **/
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) running_tree(ProcessingTree const& tree)
     :  runnable_tree<ProcessingTree>::runnable_tree(tree)
    { this->run(); }

    /**
     * @brief as the running_tree does not produce result, invoking this method will fail at compile time.
     */
    void get_result()
    { static_assert(always_false<running_tree>{}, "run of the processing tree does not produce any result"); }

    /**
     * @brief under construction. running_tree might somehow model iterator concept  
     */
    DFP_MUTABLE_CONSTEXPR running_tree& operator++()
    { this->run(); return *this; }
};  // struct running_tree


/**
* @brief Turns a declarative processing tree into a new tree which has been run
*
* This specialization of the running_tree class does produce result.
*
* At instance creation, this class simply copies or takes ownership of a given processing tree and performs its initialization and run it.
*
* @note that this specialization of running_tree models c++20 [input_iterator](https://en.cppreference.com/w/cpp/iterator/input_iterator) 
**/
// TODO: maybe make it to model also LegacyInputForwardIterator (+cpy-constructible +cpy assignable + equaly comparable)
// note that EquallyComparable requirement could be implemented by recursive check over all cells of the processing tree
template<class ProcessingTree>
struct running_tree<ProcessingTree, DFP_C_REQUIRES((not std::is_void<decltype(std::declval<runnable_tree<ProcessingTree>&>().run())>{}))> final
 :  runnable_tree<ProcessingTree>
{
    typedef typename ProcessingTree::terminal_cell_type terminal_cell_type;
    typedef std::remove_cvref_t<decltype(std::declval<runnable_tree<ProcessingTree>&>().run())> value_type;
    typedef std::ptrdiff_t difference_type; //useless except to make running_tree compliant with input_iterator concept
    // next type definitions only serve for std::iterator_traits<> support
    typedef value_type*                                     pointer;
    typedef std::add_lvalue_reference_t<value_type>         reference;
    typedef std::input_iterator_tag                         iterator_category;
    typedef std::size_t                                     size_type;

    struct value_proxy
    {
        constexpr value_proxy(value_type const& sample)
         :  sample_(sample)
        {}

        constexpr value_proxy(value_type && sample)
         :  sample_(std::move(sample))
        {}

        constexpr value_type operator*() const
        { return sample_; }

    private:
        value_type sample_;
    }; // struct value_proxy

    /**
    * @brief Take ownership on the given processing tree and run it
    * @param tree the declarative processing tree to appropriate
    **/
    constexpr running_tree(ProcessingTree&& tree)
     :  runnable_tree<ProcessingTree>::runnable_tree(std::move(tree)), result_(this->runnable_tree<ProcessingTree>::run())
    {}

    /**
    * @brief Copy on the given processing tree and run it
    * @param tree the declarative processing tree to copy
    **/
    constexpr running_tree(ProcessingTree const& tree)
     :  runnable_tree<ProcessingTree>::runnable_tree(tree), result_(this->runnable_tree<ProcessingTree>::run())
    {}

    /**
     * @brief Gets result of the last run
     */
    constexpr value_type const& get_result() const
    { return result_; }

    /**
    * @brief same as get_result()
    **/
    constexpr value_type const& operator*() const
    { return get_result(); }

    /**
    * @brief Triggers anoother run and returns the tree in its new state
    **/
    DFP_MUTABLE_CONSTEXPR running_tree& operator++()
    { result_ = run(); return *this; }

    /**
    * @brief Triggers anoother run of the tree and returns a proxy on the value before the run.
    **/
    // take care that for optimization purpose the post-increment operator does not copy the entire processing tree
    // and simply provide a proxy on the current sample
    DFP_MUTABLE_CONSTEXPR value_proxy operator++(int)
    {
        value_type const current = result_;

        ++*this;

        return { current };
    }

    /**
    * @brief trigger anoother run of the tree and returns the value result.
    **/
    DFP_MUTABLE_CONSTEXPR value_type run()
    { return result_ = terminal_cell_type::run(); }

    /**
    * @brief implicitly convertible to the result type of the sink
    **/
    constexpr operator value_type() const
    { return result_; }

private:
    value_type result_;
}; // struct running_tree<not is_void<result_of RunnableTree::run>>


/**
* @brief Creates a @ref running_tree given a declarative processing tree or a @ref runnable_tree
* @param processing_tree declarative processing tree or @ref runnable_tree to make running
**/
template<class ProcessingTree>
running_tree<std::remove_cvref_t<ProcessingTree>> make_running(ProcessingTree&& processing_tree)
{ return { std::forward<ProcessingTree>(processing_tree) }; }


} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_RUNNING_TREE_HPP
