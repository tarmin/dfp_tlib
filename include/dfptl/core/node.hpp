/**
* @file core/node.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_NODE_HPP
#define DFPTL_CORE_NODE_HPP


#include "dfptl/core/utility.hpp"
#include "dfptl/core/upstream_cell.hpp"
#include "dfptl/core/downstream_cell.hpp"
#include "dfptl/core/processing_cell.hpp"
#include "dfptl/core/initializable_cell.hpp"
#include "dfptl/core/detail/type_traits.hpp"
#include "dfptl/core/detail/node_port.hpp"
#include "dfptl/core/detail/stream.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup node_grp node processing cells
* @brief Nodes are at the same time upstream and downstream cells of a processing branch.
* They capture sample frames from their input stream(s) and produce some unique output stream.
* @{
**/


/**
* @brief Tag to specify in an instantiation of a @ref node,
* that output sample, frame or stream type is same as type on the input port of given @c Index.
**/
template<std::size_t Index>
struct as_input_tag { constexpr static std::size_t index = Index; };
/**
* @brief Alia for as_input_tag<0>
*
* @sa as_input_tag
**/
typedef as_input_tag<0> as_input0_tag;
/**
* @brief Alia for as_input_tag<1>
*
* @sa as_input_tag
**/
typedef as_input_tag<1> as_input1_tag;
/**
* @brief Alia for as_input_tag<2>
*
* @sa as_input_tag
**/
typedef as_input_tag<2> as_input2_tag;


/**
* @brief Node instance processes some input stream(s) and produces a unique output stream for the downstream part of a processing branch.
* 
* It is at the same time a @ref downstream_cell "downstream cell" and a @ref upstream_cell "upstream cell".
* This is a base class that any custom processing node shall inherit from.
* Custom processing node shall also have to implement one <b><u>and only one</u></b> (apart for `const` or "move" variants) of the @c process_sample(), @c process_frame() or @c process_stream() methods.
*
* Nodes defining one of the possible @c process_sample() methods consume a sample from each of their inputs and produce a single sample each time they are fired.
* Such a nodes are qualified as sample-based processing node.
*
* Following @c process_sample() definition are possible:
* @code
* output_sample_t<node>         process_sample(input_sample_t<node, I> const&...)
* output_sample_t<node>         process_sample(input_sample_t<node, I> const&...) const
* output_sample_t<node> const&  process_sample(input_sample_t<node, I> const&...)
* output_sample_t<node> const&  process_sample(input_sample_t<node, I> const&...) const
* @endcode
* (for rational about @c process_sample variants taking as input a rvalue reference,
* please take a look at the chapter "copy-process vs move-process" a bit below)
*
* Nodes defining one of the possilbe @c process_frame() methods consume a frame from each of their inputs and produce a single frame each time they are fired.
* Such a nodes are qualified as frame-based processing node.
*
* Following @c process_frame() definition are possible:
* @code
* output_frame_t<node>          process_frame(input_frame_t<node, I> const&...)
* output_frame_t<node>          process_frame(input_frame_t<node, I> const&...)   const
* output_frame_t<node> const&   process_frame(input_frame_t<node, I> const&...)
* output_frame_t<node> const&   process_frame(input_frame_t<node, I> const&...)   const
* @endcode
*
* At last if none of the @c process_sample() or @c process_frame() are suitable for defining processing of the node, one of the advanced @c process_stream() can be defined:
* Such a nodes are qualified as stream-based processing node.
* @code
* output_sample_t<node>         process_stream(input_stream_t<node, I>&&...)
* output_sample_t<node>         process_stream(input_stream_t<node, I>&&...) const
* output_sample_t<node> const&  process_stream(input_stream_t<node, I>&&...)
* output_sample_t<node> const&  process_stream(input_stream_t<node, I>&&...) const
* output_frame_t<node>          process_stream(input_stream_t<node, I>&&...)
* output_frame_t<node>          process_stream(input_stream_t<node, I>&&...)  const
* output_frame_t<node> const&   process_stream(input_stream_t<node, I>&&...)
* output_frame_t<node> const&   process_stream(input_stream_t<node, I>&&...)  const
* output_stream_t<node>         process_stream(input_stream_t<node, I>&&...)
* output_stream_t<node>         process_stream(input_stream_t<node, I>&&...)  const
* @endcode
* 
* For all @c process_sample, @c process_frame and @c process_stream variants given above, @c I is in range [0, count of input ports - 1].
*
* @warning When a node simply forwards its input torwards its output, one might be tempted to implement the
* `output_sample_t<node> const& process_sample(input_sample_t<node, I> const&...)` or the
* `output_frame_t<node> const& process_frame(input_frame_t<node, I> const&...)` and simply pass one of the input towards the output.
* However, this practice is insane because the input const reference is **only** valid in the scope of the process function
* therefore cannot be passed as output. For such need, please take a look at "input forwarding" chapter below. As example,
* such a code below is invalid:
* @code
* output_sample_t<node> const& process_sample(input_sample_t<node, 0> const& in)
* {
*   std::cout << "the process function will fail at runtime because the in reference is only valid in function scope";
*   return in; // <<= INSANE ! "in" reference validity is only guaranteed inside function scope.
* }
* @endcode
*
* ### "const" variant of processing in node
* Introduction mentioned that one and only one of the `process_sample`, `process_frame` or `process_stream` shall be implemented
* in a node. However a node can have **both** `const` and non-`const` processing functions defined.
* For example, both following methods are authorized in same node implementation:
* @code
* output_sample_t<node>         process_sample(input_sample_t<node, I> const&...)
* output_sample_t<node>         process_sample(input_sample_t<node, I> const&...) const
* @endcode
* `const` variant of the processing function will be automatically selected when the node is involved in a const expression (also called "regular" processing branch).
* At last, for node stateless processing (i.e. when node is a "regular" processing), only the `const` variant shall be implemented.
*
* ### input forwarding
* When a node simply forwards one of its input towards its output, `process_sample` or `process_frame` variants are not recommended
* because they imply systematic copy for creation of the output while it might not be optimal from a performance point of view.
* Thus one of the following `forward_sample` or `forward_frame` method can be implemented instead of the `process` method
* in this peculiar situation:
*
* @code
* size_t_const<K>               forward_sample(input_sample_t<node, I> const&...)
* size_t_const<K>               forward_sample(input_sample_t<node, I> const&...) const
* size_t_const<K>               forward_frame(input_frame_t<node, I> const&...)
* size_t_const<K>               forward_frame(input_frame_t<node, I> const&...)   const
* @endcode
*
* @note @c process_sample(), @c process_frame() and @c process_stream() custom implementation should test assertion on traits compatibility of input and output streams.
* For example, if a process_sample() implementation required input and output sample type to be identical, this equality shall be asserted, i.e.:
* ~~~
* static_assert(std::is_same<output_frame_t<node>, input_frame_t<node, 0>>::value, "sample type of input 0 shall be identical to output sample type);
* ~~~
* If the custom node does not consume exactly one frame per input (0 or >1) to produce one single output sample or frame,
* one of the @ref process_stream() variant should be implemented.
*
* @note If you plan to develop your own processing node, take a look at at the @ref apply, @ref apply_f, @ref execute
* and @ref execute_f templates as they can greatly help in reducing boilerplate.
* Indeed they easily turn any [function object](https://en.cppreference.com/w/cpp/utility/functional) into a processing node.
* For implementation examples:
* * of sample-based node taking leverage of the @ref apply helper, refer to @ref dfptl/math/cos.hpp
* * of sample-based node from scratch, refer to @ref dfptl/routing/detail/delay_node.hpp and @ref dfptl/routing/delay.hpp
*
* @warning Every custom node implementation shall somehow have @ref node as parent class
* what is actually the case when working with @ref apply, @ref apply_f, @ref execute or @ref execute_f
* 
* @tparam Input is the input list of cell(s)
* @tparam OutputStreamSpecifier is the type of the @ref stream_specifier "stream specifier" specifying the output stream of the node.
* @tparam Derived is the type of the inheriting class.
*
* @sa apply, apply_f, execute, execute_f
*
* @todo in-place processing node
**/
template<class Input, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue = void, class Emitter = Derived, bool IsUnmoveableRunnable = false, class = void>
class node
{
    static_assert(
        is_stream_specifier<OutputStreamSpecifier>{} || detail::is_as_input_tag<OutputStreamSpecifier>{},
        "OutputStreamSpecifier template parameter shall be a stream specifier or as_input_tag<>"
    );
    static_assert(always_false<Input>{}, DFP_UNEXPECTED_ERROR);

public:
    /**
    * @brief Creates a new instance of processing node.
    *
    * @param input is the inputs list of cells feeding the node instance.
    **/
    explicit constexpr node(Input&& input);
};


template<class Input, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node<Input, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable, DFP_C_REQUIRES((
    detail::is_as_input_tag<sample_t<OutputStreamSpecifier>>{}
    &&
    !detail::is_as_input_tag<frame_t<OutputStreamSpecifier>>{} && !detail::is_as_input_tag<OutputStreamSpecifier>{}
))>
 :  public  node<
                Input,
                typename OutputStreamSpecifier::template set_sample_clue_t<
                    output_sample_t<std::tuple_element_t<sample_t<OutputStreamSpecifier>::index, Input>>
                >,
                Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable
    >
{
    typedef std::tuple_element_t<sample_t<OutputStreamSpecifier>::index, Input> reference_input;
    typedef node<
                Input,
                typename OutputStreamSpecifier::template set_sample_clue_t<output_sample_t<reference_input>>,
                Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable
    > base;

public:
    using base::base;

    template<class NewEmitter>
    using set_emitter_class = typename base::template set_emitter_class<NewEmitter>;

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable = typename base::template set_runnable_moveable<UpdateIsRunnableMoveable>;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<UpdateIsRunnableMoveable>::type;
}; // class node<... sample_t<OutputStreamSpecifier> == as_input_tag<...> ...>


template<class Input, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node<Input, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable, DFP_C_REQUIRES((
    detail::is_as_input_tag<frame_t<OutputStreamSpecifier>>{}
    &&
    !detail::is_as_input_tag<OutputStreamSpecifier>{}
))> :   public  node<
            Input,
            set_frame_t<
                output_frame_t<std::tuple_element_t<frame_t<OutputStreamSpecifier>::index, Input>>,
                OutputStreamSpecifier
            >,
            Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable
        >
{
    typedef std::tuple_element_t<frame_t<OutputStreamSpecifier>::index, Input> reference_input;
    typedef node<Input, set_frame_t<output_frame_t<reference_input>, OutputStreamSpecifier>, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable> base;

public:
    using base::base;

    template<class NewEmitter>
    using set_emitter_class = typename base::template set_emitter_class<NewEmitter>;

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable = typename base::template set_runnable_moveable<UpdateIsRunnableMoveable>;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<UpdateIsRunnableMoveable>::type;
}; // class node<... frame_t<OutputStreamSpecifier> == as_input_tag<...> ...>


template<class Input, class AsInputTag, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node<
    Input, AsInputTag, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable, DFP_C_REQUIRES((detail::is_as_input_tag<AsInputTag>{}))
> : public  node<Input, output_stream_specifier_t<std::tuple_element_t<AsInputTag::index, Input>>, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable>
{
    typedef node<Input, output_stream_specifier_t<std::tuple_element_t<AsInputTag::index, Input>>, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable> base;

public:
    using base::base;

    template<class NewEmitter>
    using set_emitter_class = typename base::template set_emitter_class<NewEmitter>;

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable = typename base::template set_runnable_moveable<UpdateIsRunnableMoveable>;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<UpdateIsRunnableMoveable>::type;

    /**
    * Creates an instance of processing node whose flow size of its same as the one of the referenced input (with help of the @ref as_input_tag) 
    **/
    constexpr node(Input&& input)
     :  base(std::forward<Input>(input), output_stream_specifier_t<base>{}
            .set_frame_length(  std::get<AsInputTag::index>(input).output_stream_specifier().get_frame_length())
            .set_channel_count( std::get<AsInputTag::index>(input).output_stream_specifier().get_channel_count())
//            .set_sample_type(input_stream_specifier<AsInputTag::index>().get_sample_type())
//            .set_frame_type(input_stream_specifier<AsInputTag::index>().get_frame_type())
//            .set_tag_type(input_stream_specifier<AsInputTag::index>().get_tag_type())
        ) {}

    //if one of the stream specifier property is dynamic the assignment shall be completed at initialization time
    //because property of input might have been set lately.
    template<class T = output_stream_specifier_t<base>, DFP_F_REQUIRES((
        (T::FRAME_EXTENT == DYNAMIC_EXTENT)
        || (T::CHANNEL_EXTENT == DYNAMIC_EXTENT)
    ))>
    void initialize()
    {
        this->set_output_stream_specifier(this->output_stream_specifier()
            .set_frame_length(  this->input_stream_specifier(size_t_const<AsInputTag::index>{}).get_frame_length())
            .set_channel_count( this->input_stream_specifier(size_t_const<AsInputTag::index>{}).get_channel_count())
        );
    }

    template<class T = output_stream_specifier_t<base>, DFP_F_REQUIRES((
        (T::FRAME_EXTENT != DYNAMIC_EXTENT)
        && (T::CHANNEL_EXTENT != DYNAMIC_EXTENT)
    ))>
    void initialize() const
    { base::initialize(); }
}; // class node<... OutputStreamSpecifier == as_input_tag<...> ...>


template<class Inputs, class InputsIndexSequence, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node_base
{ static_assert(always_false<node_base>{}, DFP_UNEXPECTED_ERROR); };


template<class Inputs, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node<Inputs, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable,
    DFP_C_REQUIRES((
        !detail::is_as_input_tag<sample_t<OutputStreamSpecifier>>{}
        &&
        !detail::is_as_input_tag<frame_t<OutputStreamSpecifier>>{} && !detail::is_as_input_tag<OutputStreamSpecifier>{}
    ))
> : public  node_base<Inputs, index_sequence_from_t<Inputs>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable>
{
    typedef node_base<Inputs, index_sequence_from_t<Inputs>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable> base;

public:
    using base::base;

    template<class NewEmitter>
    using set_emitter_class = typename base::template set_emitter_class<NewEmitter>;

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable = typename base::template set_runnable_moveable<UpdateIsRunnableMoveable>;

    template<bool UpdateIsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<UpdateIsRunnableMoveable>::type;
};


// Note that below node inherits from output_port and input_port. These strong relationships may look excessive
//and might have been replaced with more usual aggregation associations.
// However inheritance here, by taking leverage of the compiler Empty-Base-Optimization, will optimize memory usage
// when sizeof output_port or input_port is actually null.
/** @cond **/
template<class Context, std::size_t... InputIds, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
class node_base<Context, std::index_sequence<InputIds...>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable>
 :  public  upstream_cell<detail::node_output_port, OutputStreamSpecifier, Derived, Emitter>,
    public  downstream_cell<detail::node_input_port, Context, Derived>,
    public  initializable_cell<Context, InitStorageKeyValue, Derived, IsUnmoveableRunnable>,
    public processing_cell<Context, Derived>
{
    static_assert(
        std::conjunction<detail::is_upstream<context_upstream_t<InputIds, Context>>...>{},
        "some of the given type in Context template parameter is not upstream cell");

    template<class T = Derived> //templatized to avoid early evaluation of later static_assert
    struct concept_check
    {
        #ifdef __GNUC__
        #pragma GCC diagnostic ignored "-Wparentheses"
        #endif
        static_assert(
            detail::node_produces_sample<T>{} || detail::node_produces_frame<T>{} || detail::node_produces_stream<T>{},
            "Derived node class shall implement one of the process_sample() or process_frame() or process_stream() or forward_sample() methods to comply with node concept"
        );
        static_assert(
            !detail::node_produces_frame<T>{}
            ||
            (detail::supports_process_frame<T>{} != detail::supports_process_stream_returning_frame<T>{} != detail::supports_forward_frame<T>{})
            ||
            (detail::supports_const_process_frame<T>{} != detail::supports_const_process_stream_returning_frame<T>{}  != detail::supports_const_forward_frame<T>{}),
            "Derived node class shall implement one and only one variant of the process_frame() or process_stream() returning a frame (apart from method overloading for const class object)"
        );
        static_assert(
            !detail::node_produces_sample<T>{}
            ||
            (detail::supports_process_sample<T>{} != detail::supports_process_stream_returning_sample<T>{} != detail::supports_forward_sample<T>{})
            ||
            (detail::supports_const_process_sample<T>{} != detail::supports_const_process_stream_returning_sample<T>{} != detail::supports_const_forward_sample<T>{}),
            "Derived node class shall implement one and only one variant of the process_sample() or forward_sample() or process_stream() returning a sample (apart from method overloading for const class object)"
        );
        #ifdef __GNUC__
        #pragma GCC diagnostic ignored "-Wparentheses"
        #endif
    };

    typedef initializable_cell<Context, InitStorageKeyValue, Derived, IsUnmoveableRunnable>  initializable_cell_type;
    typedef processing_cell<Context, Derived>                                                processing_cell_type;
    template<class> friend class detail::input_port; // to allow call to invoke_initialize_upwards() (to be reworked, dependency should be handled at output_port level)

public:
    typedef upstream_cell<detail::node_output_port, OutputStreamSpecifier, Derived, Emitter> upstream_cell_type;
    typedef downstream_cell<detail::node_input_port, Context, Derived> downstream_cell_type;
    typedef node_tag dfp_cell_tag;

//TODO: protected
//protected:
    // following methods are made non-public to prevent them from not being invoked within an initialized processing tree 
    using upstream_cell_type::drain;
    using upstream_cell_type::cdrain;

public:
    using upstream_cell_type::set_output_stream_specifier;
    using upstream_cell_type::output_stream_specifier;
    using upstream_cell_type::output_port; // should be removed (or turned protected) but hard to carry out because of EBO in upstream_cell

    typedef Derived derived;
    typedef InitStorageKeyValue initialization_keyvalue_type;
    typedef typename initializable_cell_type::initialization_storage_type initialization_storage_type;
    typedef typename upstream_cell_type::output_port_type       output_port_type;
    typedef typename upstream_cell_type::output_specifier_type  output_specifier_type;
    typedef typename upstream_cell_type::output_stream_type     output_stream_type;
    typedef typename downstream_cell_type::input_cells_type     input_cells_type; // required for is_node helper
    typedef node<
        processing_context<typename processing_cell_type::context_type::cell_tag, detail::constify_t<std::tuple_element_t<InputIds, Context>>...>,
        OutputStreamSpecifier, Derived, Emitter
    > const const_type;
    template<std::size_t Index> using input_port_type = typename downstream_cell_type::template input_port_type<Index>;

    using context_type      = typename processing_cell_type::context_type;
    using tag_map           = typename processing_cell_type::tag_map;
    template<class CellTag>
    using at_tag            = typename processing_cell_type::template at_tag<CellTag>;
    template<class CellTag>
    using at_tag_t = typename at_tag<CellTag>::type;

    static constexpr auto UNMOVEABLE_RUNNABLE = initializable_cell_type::UNMOVEABLE_RUNNABLE;

   /** @brief Helper class to specify the emittter type at compile-time */
    template<class NewEmitter>
    struct set_emitter_class
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewEmitter>
    using set_emitter_class_t = typename set_emitter_class<NewEmitter>::type;


   /** @brief Helper class to specify the emittter type at compile-time */
    template<bool IsRunnableMoveable>
    struct set_runnable_moveable
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<bool IsRunnableMoveable>
    using set_runnable_moveable_t = typename set_runnable_moveable<IsRunnableMoveable>::type;


    template<class T = OutputStreamSpecifier, DFP_F_REQUIRES((T::IS_STATIC))>
    constexpr node_base(Context&& context)
     :  upstream_cell_type(), downstream_cell_type(std::move(context))
    { static_assert(std::is_class<decltype(concept_check<>{})>{}, ""); }

    constexpr node_base(Context&& context, OutputStreamSpecifier const& output_specifier)
     :  upstream_cell_type(output_specifier), downstream_cell_type(std::move(context))
    { static_assert(std::is_class<decltype(concept_check<>{})>{}, ""); }

    /**
    * @brief default empty (doing nothing) implementation of initialize_upwards()
    *
    * In `Derived` class of core::node requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_upwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_upwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; }

    /**
    * @brief default empty (doing nothing) implementation of initialize_downwards()
    *
    * In `Derived` class of core::node requiring some specific initialization step, this method can be overloaded.
    *
    * @param initialization_storage is a map available during initialization stage and shared between all cells of a processing tree.
    *
    * @sa core::initializable_cell::initialize_downwards
    **/
    template<class InitializationStorageView>
    DFP_CONSTEXPR_VOID initialize_downwards(InitializationStorageView initialization_storage)
    { (void) initialization_storage; static_cast<Derived* const>(this)->initialize(); }

protected:
    friend class detail::node_output_port<Derived, OutputStreamSpecifier, Emitter>;

    using downstream_cell_type::input_port;

    DFP_CONSTEXPR_VOID initialize() const {}

private:
    initialization_storage_type build_initialization_storage(node_base const& cell) const
    { return this->initializable_cell_type::build_initialization_storage(cell); }
}; // class node_base
/** @endcond **/


template<class Context, std::size_t... InputIds, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
template<class NewEmitter>
struct node_base<Context, std::index_sequence<InputIds...>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable>::set_emitter_class<NewEmitter>::update
{
    typedef node_base<
        Context, std::index_sequence<InputIds...>, OutputStreamSpecifier, Derived, InitStorageKeyValue,
        // Actually reference of Emitter here would not bring relevant information
        // so it is better (for sake of the framework simplicity) to not try to support.
        // However, removing automatically reference helps to reduce boilerplate when using set_emitter_class
        // in peculiar when emitter is one of the node input (aka ::set_emitter_class_t<std::tuple_element_t<0, Input>>
        std::remove_reference_t<NewEmitter>, IsUnmoveableRunnable
    > type;
};


template<class Context, std::size_t... InputIds, class OutputStreamSpecifier, class Derived, class InitStorageKeyValue, class Emitter, bool IsUnmoveableRunnable>
template<bool UpdateIsRunnableMoveable>
struct node_base<Context, std::index_sequence<InputIds...>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, IsUnmoveableRunnable>::set_runnable_moveable<UpdateIsRunnableMoveable>::update
{ typedef node_base<Context, std::index_sequence<InputIds...>, OutputStreamSpecifier, Derived, InitStorageKeyValue, Emitter, !UpdateIsRunnableMoveable> type; };


/**
* @example dfptl/math/cos.hpp
* This is an example of node implementation using the function_node helper class.
**/

///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_NODE_HPP
