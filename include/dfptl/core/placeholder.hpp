/**
* @file core/placeholder.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_PLACEHOLDER_HPP
#define DFPTL_CORE_PLACEHOLDER_HPP


#include "dfptl/core/detail/placeholder.hpp"


namespace dfp
{
inline namespace core
{
/**
* @addtogroup core_grp
* @{
* @defgroup placeholder_grp processing cell placeholder
* @brief Placeholder surrogates a processing cell in a processing branch.
* @note for detailed example, please see @ref placeholder_creation_example.cpp and @ref placeholder_preset_inputs_example.cpp
* @{
**/


/**
* @brief Placeholder helps to capture initialization parameters of a processing cell, locate and ultimately construct it.
*
* Placeholder surrogates a processing cell until this latest can be created.
* This is actually placeholders which firstly define cells location in processing branch.
*
* Cell development
* ================
* For development of downstream processing cell (sink or node) a companion placeholder shall also be provided.
* However some helper functions  make_placeholder() - or mph() - can create a placeholder for any processing cell.
*
* For detailed example, please see @ref placeholder_creation_example.cpp
*
* @note Custom processing nodes defined with apply() will automatically get their companion placeholder.
* In this case, their is no need to direclty use make_placeholder().
*
* Cell usage
* ==========
* Client code simply using cells won't need to deal with placeholder creation but should be familiar
* with placeholder::preset_inputs() method - or placeholder::pi() -.
*
* For detailed example, please see @ref placeholder_preset_inputs_example.cpp
*
* @tparam CellArgs template argument of the processing cell. It shall be a @c std::tuple.
* @tparam Cell template class of the processing cell surrogated by the placeholder.
* @tparam CtorArgs the argument types of constructor for the processing cell. It shall be a @c std::tuple.
* @tparam Tag optional type associated to the resulting processing cell.
* It shall be unique in the processing graph where the cell will take place or `void` (default type)
* @todo example of preset_inputs() usage
* @todo create make_branch_reference
* @todo update test-cases
**/
//TODO: ImplicitRun template parameter should actually be only part of a "sink_builder" specialization
template<class CellArgs, template<class, class ...> class Cell, class CtorArgs, class PresetInputs = std::tuple<>, bool ImplicitRun=false, class Tag = void>
class placeholder final
 :  public detail::placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun>
{
    static_assert(std::is_void<Tag>{} || is_custom_tag<Tag>{}, "Given Tag template parameter shall be a custom tag");

    friend class placeholder<CellArgs, Cell, CtorArgs, PresetInputs, !ImplicitRun, Tag>;

    typedef detail::placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun> impl;

public:
    static constexpr auto REQUIRES_IMPLICIT_RUN = bool_const<ImplicitRun>{};
    //TODO: rename all dfp_cell_tag into dfptl_tag
    typedef typename impl::dfptl_tag dfptl_tag;
    typedef Tag tag_type;

    template<bool R = ImplicitRun, DFP_F_REQUIRES((R))>
    constexpr placeholder(placeholder<CellArgs, Cell, CtorArgs, PresetInputs, false, Tag> const& implicitly_run)
     :  placeholder(implicitly_run.ctor_args_, implicitly_run.preset_inputs_)
    {}

    /**
    * @brief Creates a new instance of placeholder given the processing cell arguments of the cell's constructor.
    *
    * @param[in] args Constructor arguments of the depicted processing cell to retain.
    *
    * @remarks make_placeholder() should be used instead of direct invocation of this constructor to take leverage of types deduction.
    *
    * @sa make_placeholder()
    **/
    template<class ArgsTuple, class PresetsTuple>
    constexpr placeholder(ArgsTuple&& args, PresetsTuple&& inputs)
     :  impl::placeholder(std::forward<ArgsTuple>(args), std::forward<PresetsTuple>(inputs))
    {}

    /**
    * @brief Copy-constructs a new instance of cell_builder updated with the given tag type property 
    **/
    template<class OriginalTag, class T, DFP_F_REQUIRES((std::is_same<Tag, T>{} && std::is_base_of<custom_tag, T>{}))>
    constexpr placeholder(
        placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun, OriginalTag> const& original,
        T
    ) : impl::placeholder(original)
    {}

    /**
    * @brief Creates a placeholder with some of its first inputs preset to the given pack of input branches and/or data.
    *
    * preset_inputs() usage is related to multi-input processing cells (more than one input) and provides another way
    * to bind inputs to the cell.
    *
    * The pack size of input branches and/or data can be less than the count of input ports (for example, only 1 input branch given while cell expect 2 inputs).
    * The rank of the port linked to an input branch or data is given by the argument position.
    *
    * @param inputs is a parameter pack of input branches and/or data. If given argument is not a processing branch but a data
    * it is implicitly converted into a processing source with help of @ref const_sample source.
    *
    * @note If an input branch (or data) is provided as non-const lvalue reference, it is copied to leave the referenced input unchanged.
    * If passing input by non-const reference is required, consider working with @ref make_branch_reference().
    *
    * @return The new placeholder with same traits as this instance except that some of its first inputs are preset.
    *
    * @tparam NewPresetInputs is implicitly deduced from @c inputs parameter pack.
    *
    * @sa placeholder::pi(), const_sample(), make_branch_reference()
    *
    * **Usage example**
    * @include placeholder_preset_inputs_example.cpp
    */
    template<class... NewPresetInputs>
    constexpr auto preset_inputs(NewPresetInputs&&... inputs) const ->
    placeholder<CellArgs, Cell, CtorArgs, decltype(std::make_tuple(std::declval<detail::result_of::cellify_t<NewPresetInputs>>()...)), ImplicitRun, Tag>
    { return { impl::ctor_args_, std::make_tuple(detail::cellify(std::forward<NewPresetInputs>(inputs))...) }; }

    /**
    * @brief Alias for @ref preset_inputs()
    * @sa preset_inputs()
    **/
    template<class... NewPresetInputs>
    constexpr auto pi(NewPresetInputs&&... inputs) const /** @cond */ ->
    decltype(preset_inputs(std::forward<NewPresetInputs>(inputs)...)) /** @endcond */
    { return preset_inputs(std::forward<NewPresetInputs>(inputs)...); }

    /**
    * @cond
    * @brief Provides typedef @c type of the cell to be built.
    *
    * Given @c ProcessingContext as processing_context specialization,
    * provides typedef @c type of the cell to be built.
    *
    * @note This type is mainly for internal purpose, it should not be directly manipulated by end-user code.
    **/
    template<class ProcessingContext> using cell = typename impl::template cell<typename ProcessingContext::template set_tag_t<Tag>>;
    template<class ProcessingContext> using cell_t = typename cell<ProcessingContext>::type;

   /** @brief Helper class to specify the tag at compile-time */
    template<class NewTag>
    struct set_tag
    {
    private:
        struct update;

    public:
        typedef typename update::type type;
    };

    template<class NewTag>
    using set_tag_t = typename set_tag<NewTag>::type;

    /**
    * @brief Specifies at compile-time the tag type of a placeholder 
    * @return a new instance of placeholder with the new tag type
    **/
    template<class NewTag, DFP_F_REQUIRES((std::is_base_of<custom_tag, NewTag>{}))>
    constexpr placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun, NewTag> tag(NewTag = NewTag{}) const
    { return  { *this, NewTag{} }; }


    auto constexpr build() const /** @cond */ ->
    decltype(this->impl::template build<Tag>(std::tuple<>{}, std::make_index_sequence<0>{})) /** @endcond */
    { return this->impl::template build<Tag>(std::tuple<>{}, std::make_index_sequence<0>{}); }

    /**
    * @brief Creates a new instance of the processing cell depicted by the placeholder instance.
    *
    * **Example**
    * @snippet operators_overview_example.cpp Linking to multiple input cells
    *
    * @param inputs_tuple Processing cell instance (or something convertible to cell)
    * linked to the input of the cell to be created.
    *
    * @tparam Inputs Implicitly deduced from @c input_cell argument
    */
    template<class... Inputs>
    auto constexpr build(std::tuple<Inputs...>&& inputs_tuple) const /** @cond */ ->
    decltype(this->impl::template build<Tag>(std::move(inputs_tuple), std::index_sequence_for<Inputs...>{})) /** @endcond */
    { return this->impl::template build<Tag>(std::move(inputs_tuple), std::index_sequence_for<Inputs...>{}); }

    template<class... Inputs>
    auto constexpr build(std::tuple<Inputs...> const& inputs_tuple) const /** @cond */ ->
    decltype(this->impl::template build<Tag>(inputs_tuple, std::index_sequence_for<Inputs...>{})) /** @endcond */
    { return this->impl::template build<Tag>(inputs_tuple, std::index_sequence_for<Inputs...>{}); }

    template<bool R = ImplicitRun, DFP_F_REQUIRES((!R))>
    constexpr placeholder<CellArgs, Cell, CtorArgs, PresetInputs, true>
    implicit_run() const
    { return  {*this}; }
};


/**
* @brief Creates a new instance of @ref placeholder given constructor arguments of the surrogate processing cell.
* @related placeholder
*
* @param ctor_args arguments to later forward to the constructor of the processing cell.
*
* @tparam CellArgs tuple of the template types parameter for the depicted processing cell.
* @tparam Cell template class of the processing cell depicted by the placeholder.
* @tparam CtorArgs tuple of the parameter types for cell's constructor.
* This template parameter should not be set as it is implicitly deduced by compiler
* based on the given function arguments.
*
* @sa placeholder
*
* **Usage example**
* @include placeholder_creation_example.cpp
**/
template<class CellArgs, template<class, class ...> class Cell, class... CtorArgs>
placeholder<CellArgs, Cell, std::tuple<std::decay_t<CtorArgs>...>>
constexpr make_placeholder(CtorArgs&&... ctor_args)
{ return placeholder<CellArgs, Cell, std::tuple<std::decay_t<CtorArgs>...>>(fix::forward_as_tuple(std::forward<CtorArgs>(ctor_args)...), std::tuple<>{}); }


/**
* @related dfp::core::placeholder
* @brief Alias for @c make_placeholder<std::tuple<>, Cell>(CtorArgs&&)
*
* @sa make_placeholder()
**/
template<template<class, class ...> class Cell, class... CtorArgs>
constexpr auto make_placeholder(CtorArgs&&... ctor_args) /** @cond */ ->
decltype(make_placeholder<std::tuple<>, Cell>(std::forward<CtorArgs>(ctor_args)...)) /** @endcond */
{ return make_placeholder<std::tuple<>, Cell>(std::forward<CtorArgs>(ctor_args)...); }


namespace shortname
{


/**
* @related dfp::core::placeholder
* @brief Alias for @c make_placeholder<CellArgs, Cell>(CtorArgs&&)
*
* @sa make_placeholder()
**/
template<class CellArgs, template<class, class ...> class Cell, class... CtorArgs>
constexpr auto mph(CtorArgs&&... ctor_args) /** @cond */ ->
decltype(make_placeholder<CellArgs, Cell>(std::forward<CtorArgs>(ctor_args)...)) /** @endcond */
{ return make_placeholder<CellArgs, Cell>(std::forward<CtorArgs>(ctor_args)...); }


/**
* @related dfp::core::placeholder
* @brief Alias for @c make_placeholder<std::tuple<>, Cell>(CtorArgs&&)
*
* @sa make_placeholder()
**/
template<template<class, class ...> class Cell, class... CtorArgs>
constexpr auto mph(CtorArgs&&... ctor_args) /** @cond */ ->
decltype(make_placeholder<std::tuple<>, Cell>(std::forward<CtorArgs>(ctor_args)...)) /** @endcond */
{ return make_placeholder<std::tuple<>, Cell>(std::forward<CtorArgs>(ctor_args)...); }


};


// 1. 'update' struct cannot be named 'type' or GCC will warn that the second later 'type' definition in set_tag will hide the 1st one.
// 2. following definition cannot be inlined in class stream_specifier definition otherwise gcc will warn about "invalid use of incomplete type"
template<
    class CellArgs, template<class, class ...> class Cell, class CtorArgs, class PresetInputs,
    bool ImplicitRun, class Tag
>
template<class NewTag>
struct placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun, Tag>
    ::set_tag<NewTag>::update
{ typedef placeholder<CellArgs, Cell, CtorArgs, PresetInputs, ImplicitRun, NewTag> type; };


///@}
///@} core_grp
} // namespace core
} // namespace dfp
/**
* @example placeholder_creation_example.cpp
* This example shows how to define companion @ref dfp::core::placeholder "placehoder" for processing cells.
*
* @example placeholder_preset_inputs_example.cpp
* This example shows some possible usages of dfp::core::placeholder::preset_inputs()
**/


#endif // DFPTL_CORE_PLACEHOLDER_HPP
