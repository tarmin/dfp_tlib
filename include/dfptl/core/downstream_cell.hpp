/**
* @file core/downstream_cell.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DOWNSTREAM_CELL_HPP
#define DFPTL_CORE_DOWNSTREAM_CELL_HPP


#include "dfptl/core/detail/downstream_cell.hpp"


namespace dfp
{
inline namespace core
{
template<class, class> struct runner;
namespace detail
{
template<class, class, class, class, bool> class initializable_cell;
}


///@addtogroup core_grp
///@{
/** @defgroup downstream_grp downstream cells
* @brief Common base for @ref sink and @ref node processing cells
* @{
**/


/**
* @brief A Downstream processing cell is an element taking place in a downstream location of a processing branch.
*
* A downstream cell owns a set of @ref input_port "input ports".
* Basically, @ref node and @ref sink are the possible downstream cells of a processing branch.
*
* @tparam InputPort is the template type of @ref input_port "input ports" owned by the downstream cell.
* @tparam Context is the types list of input cells reaching this downstream cell.
* @tparam Derived topmost derived class from downstream_cell (CRTP idiom)
**/
template<template<class, class> class InputPort, class Context, class Derived>
class downstream_cell
 :  protected   detail::downstream_cell<InputPort, Context, Derived>
{
    typedef     detail::downstream_cell<InputPort, Context, Derived> downstream_cell_detail_type;
    template<class, class, class, class, bool> friend class detail::initializable_cell;


public:
    using input_ports_type = typename downstream_cell_detail_type::input_ports_type;  // public visibility required for for cell_input_ports helper (friend does not help with gcc)
    using input_index_sequence = typename downstream_cell_detail_type::input_index_sequence;
    template<std::size_t Index>
    using input_port_type = typename downstream_cell_detail_type::template input_port_type<Index>;  // public visibility required for for cell_input_port and is_downstream helpers (friend does not help with gcc)

    /**
     * @brief Gets the specifier of the stream feeding the unique input port
     *
     * @note this member method is only available for single-input cell.
     */
    template<class T = Context, DFP_F_REQUIRES((T::UPSTREAM_COUNT == 1))>
    constexpr input_stream_specifier_t<downstream_cell_detail_type, 0> input_stream_specifier() const noexcept
    { return downstream_cell_detail_type::input_stream_specifier(); }

    /**
    * @brief Gets the specifier of the stream feeding the given input port
    *
    * @param index is the index expressed as a integral constant type instance (e.g. `std::integral_constant<int, 2>()`)
    *
    * @tparam Integral is implicitly deduced from @c index parameter
    * @tparam Index is is implicitly deduced from @c index parameter
    **/
    template<class Integral, Integral Index>
    constexpr input_stream_specifier_t<downstream_cell_detail_type, (std::size_t) Index>
    input_stream_specifier(integral_const<Integral, Index> index) const noexcept
    { static_assert(index >= 0, "index shall be a positive number");
      return downstream_cell_detail_type::input_stream_specifier(index); }

protected:
    using downstream_cell_detail_type::downstream_cell_detail_type;

    template<class T = Context, DFP_F_REQUIRES((T::UPSTREAM_COUNT == 1))>
    static constexpr input_stream_specifier_t<downstream_cell_detail_type, 0>
    output_stream_specifier_of(Context const& context) noexcept
    { return downstream_cell_detail_type::output_stream_specifier(context, size_t_const<0>{}); }

    /**
    * @brief Gets the output stream specifier of the selected cell in the tuple of input cells
    *
    * @param context is the current processing context for this downstream cell 
    *
    * @param index is the index expressed as an instance of `size_t_const<>`.
    * Note that parameter value is defaulted to value of `Index` template parameter.
    *
    * @tparam Index can be implicitly deduced from @c index parameter or sets the value if `index` parameter value is not passed to the method
    *
    * @note this method is only of interest in the ctor of the downstream_cell where `inputs` parameter can be retrieved.
    * In any other case, retrieval of the output stream specifier on an input port shall be performed with help of the method @ref input_stream_specifier
    **/
    template<std::size_t Index>
    static constexpr input_stream_specifier_t<downstream_cell_detail_type, Index>
    output_stream_specifier_of(Context const& context, size_t_const<Index> index = size_t_const<Index>{}) noexcept
    { return downstream_cell_detail_type::output_stream_specifier(context, index); }

private:
    downstream_cell_detail_type& detail()
    { return *static_cast<downstream_cell_detail_type* const>(this); }

    downstream_cell_detail_type const& detail() const
    { return *static_cast<downstream_cell_detail_type const* const>(this); }
};


///@} downstream_grp
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DOWNSTREAM_CELL_HPP
