/**
* @file core/downstream_traits.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_DOWNSTREAM_TRAITS_HPP
#define DFPTL_CORE_DOWNSTREAM_TRAITS_HPP


#include "dfptl/core/detail/cell_traits.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup downstream_traits downstream cell traits
* @brief Provides helper classes to retrieve traits on processing @ref sink or @ref node
* @{
**/


/**
* @brief Provides member typedef @c type which is defined as the input stream type of the given @ref downstream cell.
*
* @tparam T is the downstream cell type (aka @ref sink of @ref node) on which to retrieve the input stream type.
*/
template<class T, std::size_t Index, class = DFP_CONCEPT_ASSERT((detail::is_downstream<T>))>
struct input_stream
{ typedef detail::port_stream_t<detail::cell_input_port_t<T, Index>> type; };

/**
* @brief Alias for @ref input_stream<T, Index>::type
**/
template<class T, std::size_t Index = 0>
using input_stream_t = typename input_stream<T, Index>::type;


/**
* @brief Provides member typedef type which is defined as the input stream specifier type of the given downstream cell.
*
* @tparam T is the downstream cell type (aka @ref sink of @ref node) on which to retrieve the type.
*/
template<class T, std::size_t Index>
struct input_stream_specifier
{
    static_assert(detail::is_downstream<T>{}, DFP_UNEXPECTED_ERROR);
    typedef detail::port_stream_specifier_t<detail::cell_input_port_t<T, Index>> type;
};

/**
* @brief Alias for input_stream_specifier
* @related input_stream_specifier
**/
template<class T, std::size_t Index = 0>
using input_stream_specifier_t = typename input_stream_specifier<T, Index>::type;


/**
* @brief Provides member typedef @c type which is defined as the input sample type of the given @ref downstream cell.
*
* @tparam T is the downstream cell type (aka @ref sink of @ref node) on which to retrieve the input sample type.
**/
template<class T, std::size_t Index> struct input_sample
{
    static_assert(detail::is_downstream<T>{}, "T shall be a node or sink type");
    static_assert(Index < std::tuple_size<typename T::input_ports_type>{}, "Given index is out of range of available inputs for the processing cell");

    typedef typename detail::frame_traits<frame_t<input_stream_t<T, Index>>>::sample_type type;
};

/**
* @brief Alias for @ref input_sample<T, Index>::type
**/
template<class T, std::size_t Index = 0> using input_sample_t = typename input_sample<T, Index>::type;


/**
* @brief Provides member typedef @c type which is defined as the input frame type of the given @ref downstream cell.
*
* @tparam T is the downstream cell type (aka @ref sink of @ref node) on which to retrieve the input frame type.
**/
template<class T, std::size_t Index> struct input_frame
{ typedef frame_t<input_stream_t<T, Index>> type; };

/**
* @brief Alias for @ref input_frame<T, Index>::type
**/
template<class T, std::size_t Index = 0> using input_frame_t = typename input_frame<T, Index>::type;


/**
* @brief Provides member typedef @c type which is defined as the @ref processing_context depicting the upstream processing branch for the given @ref downstream cell.
*
* @tparam T is the downstream cell type (aka @ref sink of @ref node) on which to retrieve the processing_context.
**/
template<class T, class = DFP_CONCEPT_ASSERT((detail::is_downstream<T>))>
struct input_cells { typedef typename T::downstream_cell_type::input_cells_type type; };

/**
* @brief Alias for @ref input_cells<T>::type
**/
template<class T>
using input_cells_t = typename input_cells<T>::type;


///@}
///@} core_grp
} // namespace core
} // namespace dfp


#endif // DFPTL_CORE_DOWNSTREAM_TRAITS_HPP
