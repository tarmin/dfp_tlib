/**
* @file core/sample_releaser.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_CORE_SAMPLE_RELEASER_HPP
#define DFPTL_CORE_SAMPLE_RELEASER_HPP


#include "dfptl/core/detail/sample_releaser_sink.hpp"
#include "dfptl/core/placeholder.hpp"


namespace dfp
{
inline namespace core
{
inline namespace sinks
{
///@addtogroup core_grp
///@{
/**
* @defgroup sample_releaser_grp "sample releaser" sink
* @brief Releases one single sample from its single input.
*
* A @ref sample_releaser_grp pulls one single sample acquired from its single input port when its \c run() method is invoked.
* It does not apply any transformation on captured samples.
*
* **Sink interface**
* @startuml
* rectangle "i(k-1) <- run() | with k the count of run() calls" <<sample_releaser()>> as sink
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* sink <-up- in
* @enduml

 * port name    | location    | stream specification          | description
* --------------|-------------|-------------------------------|------------
* "in"          | **input-0** | @ref atomic_stream            | consumes the samples to release
* @{
**/


/**
* @brief Creates a builder for a @ref sample_releaser_grp.
*
* **Usage example**
* @snippet core_cell_test.cpp sample_releaser usage example
**/
constexpr inline auto sample_releaser() noexcept /** @cond */ ->
decltype(make_placeholder<detail::sample_releaser_sink>()) /** @endcond */
{ return make_placeholder<detail::sample_releaser_sink>(); }



/**
* @brief Creates a builder for a @ref sample_releaser_grp and auto run execution.
*
* The sink ultimately built with sample_releasing() is implicitly convertible into the sample captured on its input.
*
* **Usage example**
* @snippet core_cell_test.cpp sample_releasing usage example
**/
constexpr inline auto sample_releasing() noexcept /** @cond */ ->
decltype(make_placeholder<detail::sample_releaser_sink>().implicit_run()) /** @endcond */
{ return make_placeholder<detail::sample_releaser_sink>().implicit_run(); }


///@} sample_releaser_grp
///@} core_grp
} // inline namespace sinks
} // inline namespace core
} // namespace dfp


#endif // DFPTL_CORE_SAMPLE_RELEASER_HPP
