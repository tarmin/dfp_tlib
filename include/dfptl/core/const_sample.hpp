/**
* @file core/const_sample.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CORE_CONST_SAMPLE_HPP
#define DFPTL_CORE_CONST_SAMPLE_HPP


#include "dfptl/core/detail/sample_source.hpp"
#include "dfptl/core/placeholder.hpp"


namespace dfp
{
inline namespace core
{
///@addtogroup core_grp
///@{
/** @defgroup const_sample_grp "const-sample" source
* @todo create `from` source with is equivalent to:
* * `const_frame` when input is std::initializer_list
* * `mutable_sample` when input is std::reference_wrapper
* * `apply_source` when input is a callable
* * `const_sample` otherwise
* @brief A @ref const_sample_grp feeds a processing branch with an unmutable input sample.
*
* @note A value or value-reference placed at the start of a processing branch will be implicitly converted into a const-sample source.
* @sa graph_operators_grp
*
* **Source interface**
* @startuml
* rectangle "o(n) = sample" <<const_sample(sample)>> as src
* interface "output: sample sample ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | an @ref atomic_stream "atomic stream" conveying the given immutable sample
**/
///@{


/**
* @brief Creates a cell_builder for a @ref const_sample_grp to feed a processing branch with some immutable sample.
*
* **Usage example**
* @include const_sample_overview_example.cpp
*
* @param sample is the data to inject in the processing branch.
*
* @tparam Sample type is implicity deduced from @c sample argument.
**/
template<class Sample>
inline constexpr auto const_sample(Sample&& sample) noexcept ->
decltype(make_placeholder<std::tuple<std::remove_reference_t<Sample>>, detail::sample_source>(std::forward<Sample>(sample)))
{ return make_placeholder<std::tuple<std::remove_reference_t<Sample>>, detail::sample_source>(std::forward<Sample>(sample)); }


/**
* @brief Creates a cell_builder for a @ref const_sample_grp to feed a processing branch with some sample immutable pointer
*
* This overloading is actually given to allow implicit conversion in processing graph of c-array (in peculiar c-string) into pointer
*
* **Usage example**
* @include const_sample_overview_example.cpp
*
* @param sample_ptr is the immutable pointer on sample to inject in the processing branch.
*
* @tparam Sample type is implicity deduced from @c sample_array argument.
**/
template<class Sample>
inline constexpr auto const_sample(Sample* sample_ptr) noexcept ->
decltype(make_placeholder<std::tuple<Sample*>, detail::sample_source>(sample_ptr))
{ return make_placeholder<std::tuple<Sample*>, detail::sample_source>(sample_ptr); }


// std::initializer is temporary handled by const_sample node but at some time this should be the responsability of const_frame
// when implicit conversion from any stream to atomic stream will be available
template<class First, class... Samples>
constexpr auto
inline const_sample(First first, Samples... samples) noexcept ->
decltype(make_placeholder<std::tuple<std::initializer_list<typename std::common_type<First, Samples...>::type>>, detail::sample_source>(first, samples...))
{ return make_placeholder<std::tuple<std::initializer_list<typename std::common_type<First, Samples...>::type>>, detail::sample_source>(first, samples...); }


///@} const_sample_grp
///@} core_grp


namespace shortname
{
///@addtogroup core_grp
///@{
///@addtogroup const_sample_grp
///@{

/** @brief shortname for @ref const_sample() **/
template<class T>
inline constexpr auto const_s(T&& t) /** @cond */ ->
decltype(const_sample(std::forward<T>(t))) /** @endcond */
{ return const_sample(std::forward<T>(t)); }

/** @brief shortname for @ref const_sample() **/
template<class... Sample>
inline constexpr auto const_s(Sample... s) /** @cond */ ->
decltype(const_sample(s...)) /** @endcond */
{ return const_sample(s...); }


///@} const_sample_grp
///@} core_grp
}// namespace shortname


namespace detail
{


namespace functions
{
// TODO: is_source_builder could simply be replaced with is_cell_builder
// or even better, cellify might be removed when processing_context and processing_graph will have been merged
// and cell_builder reworked to take processing_graph as input
template<class T>
struct cellify<T, DFP_C_REQUIRES((!is_upstream<T>{} && !is_source_builder<T>{} && !is_reference_wrapper<T>{}))> 
{
constexpr auto operator()(T&& t) const ->
decltype(const_sample(std::forward<T>(t)).build())
{ return const_sample(std::forward<T>(t)).build(); }
}; // struct cellify<fallback>
} // namespace functions


namespace result_of
{
#if (defined(__clang__) && (__clang_major__ > 3)) || !defined(__clang__)
#else
// workaround for clang 3
template<class T>
// TODO: is_source_builder could simply be replaced with is_cell_builder
struct cellify<T, DFP_C_REQUIRES((!is_upstream<T>{} && !is_source_builder<T>{} && !is_reference_wrapper<T>{}))>
{ typedef decltype(const_sample(std::declval<T>()).build()) type; };
#endif
} // namespace result_of


} // namespace detail


}// namespace core
}// namespace dfp


#endif // DFPTL_CORE_CONST_SAMPLE_HPP
