/**
* @file container/iterate.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CONTAINER_ITERATE_HPP
#define DFPTL_CONTAINER_ITERATE_HPP


#include "dfptl/container/detail/iterate_node.hpp"


namespace dfp
{
namespace container
{
inline namespace nodes
{


///@addtogroup container_grp
///@{
/**
* @defgroup iterate_grp "iterate" node
* @brief Iterates over samples contained in a [standard container](https://en.cppreference.com/w/cpp/named_req/Container)
* brought by its input @ref atomic_stream "atomic stream".
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n/L)[j] | j in [0..L[" <<iterate()>> as iterate
* interface "input: [1 2 3][4 5 6] ...i(n/3)" <<in>> as in
* interface "output: 0 1 2 4 5 6 ... i(n/3)[0] i(n/3)[1] i(n/3)[2]" <<operation result>> as out
* iterate <- in
* out <- iterate
* @enduml
* * <tt>"in" **input**               </tt> : @ref atomic_stream "atomic stream" conveying container of samples
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying individual samples from input container
* @{
**/


/**
* @brief Creates a placeholder for a @ref iterate_grp node.
*
* **Usage example**
* @snippet container_test.cpp Iterate over container
**/
inline auto iterate() /** @cond */ ->
decltype(make_placeholder<detail::iterate_node>()) /** @endcond */
{ return make_placeholder<detail::iterate_node>(); }


///@} iterate_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using container::nodes::iterate;
} // namespace nodes
} // namespace shortname


///@} container_grp
} // namespace container
} // namespace dfp


#endif // DFPTL_CONTAINER_ITERATE_HPP
