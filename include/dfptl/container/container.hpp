/**
* @file container/container.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CONTAINER_CONTAINER_HPP
#define DFPTL_CONTAINER_CONTAINER_HPP


#include "dfptl/container/iterate.hpp"


namespace dfp
{
/**
* @defgroup container_grp Processing cells for containers handling
* @brief Defines processing cells helping in manipulating samples gathered in container.
* @{
*
* @defgroup container_detail implementation details
* @brief Implementation details and boilerplate for @ref container_grp
**/

/**
* @brief Contains items related to processing cells for container manipulations.
**/
namespace container
{
/**
* @brief Set of helper functions for container-related nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for container-related sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief Set of helper functions for container-related sinks.
**/
inline namespace sinks {}
namespace trunks = sinks;


/**
* @brief short name of functions in @ref container_grp namespace
**/
namespace shortname {}


///@}
} // namespace container
} // namespace dfp


#endif // DFPTL_CONTAINER_CONTAINER_HPP
