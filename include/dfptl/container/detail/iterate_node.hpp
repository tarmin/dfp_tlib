/**
* @file container/detail/iterate_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_CONTAINER_DETAIL_ITERATE_NODE_HPP
#define DFPTL_CONTAINER_DETAIL_ITERATE_NODE_HPP


#include "dfptl/core/node.hpp"


namespace dfp
{
namespace container
{
namespace detail
{


#define DFP_CONTAINER_ITERATE container::iterate



/**
* @brief Sample-based node which iterates over content of the input container
*
* @tparam Input is the type of the processing cell (source or node) bound to the input port to this node.
**/
//TODO: verify that input sample is a container
template<class Input>
class iterate_node
 :  public  node<Input, atomic_stream_specifier<typename output_sample_t<std::tuple_element_t<0, Input>>::value_type>, iterate_node<Input>>
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_CONTAINER_ITERATE, 1));
    typedef node<Input, atomic_stream_specifier<typename output_sample_t<std::tuple_element_t<0, Input>>::value_type>, iterate_node> node_type;
    static_assert(input_stream_specifier_t<node_type>::IS_ATOMIC, DFP_INPUT_NOT_ATOMIC_STREAM(DFP_CONTAINER_ITERATE, 0));

public:
    iterate_node(Input&& input) : node_type(std::move(input)), current_(nullptr), end_(current_) {}

    output_sample_t<node_type> process_stream(input_stream_t<node_type>&& input_stream) noexcept
    {
        if (current_ == end_) 
        {
            container_ = *input_stream.cdrain().begin();
            current_ = container_.cbegin();
            end_ = container_.cend();
        }

        return *current_++;
    }

private:
    input_sample_t<node_type> container_;
    const_iterator_t<input_sample_t<node_type>> current_;
    const_iterator_t<input_sample_t<node_type>> end_;
}; // class iterate_node


} // namespace detail
} // namespace container
} // namespace dfp


#endif // DFPTL_CONTAINER_DETAIL_ITERATE_NODE_HPP
