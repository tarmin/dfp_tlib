/**
* @file io/write_text_chart.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_WRITE_TEXT_CHART_HPP
#define DFPTL_IO_WRITE_TEXT_CHART_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/io/detail/write_text_chart_node.hpp"

#include <iostream>


namespace dfp
{
namespace io
{
inline namespace nodes
{
///@addtogroup io_grp
///@{
/**
* @defgroup write_text_chart_grp "write text-chart" node
* @brief Writes into some [std::ostream](https://en.cppreference.com/w/cpp/io/basic_ostream)
* a character-based chart representing samples from its single input @ref atomic_stream "atomic stream".
*
* Uncommonly, the chart x-axis is oriented vertically from top to bottom and the y-axis is oriented horizontally from left to right.
* Node input stream is forwarded to its output port without modification.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n) | write text-chart from i(n)" <<write_text_chart(ostream)>> as write
* interface "input: ...i(n)" <<in>> as in
* interface "output: ...i(n)" <<operation result>> as out
* write <- in
* out <- write
* @enduml
* * <tt>"in" **input**              </tt> : @ref core::sample_based_stream "atomic stream" to represents a chart in text file
* * <tt>"operation result" **output**</tt> : the same stream as the input one.
* @{
**/


/**
* @brief Creates a placeholder for a @ref write_text_chart_grp writing chart into the given [std::ostream](https://en.cppreference.com/w/cpp/io/basic_ostream).
*
* **Usage example**
* @snippet io_test.cpp write_text_chart usage example
*
* @param ostream is the [std::ostream](https://en.cppreference.com/w/cpp/io/basic_ostream) into which to write chart.
* @param chart_conf is the chart appearance configuration.
**/
// add move initializer
inline auto write_text_chart(std::ostream& ostream, text_chart_config const& chart_conf = {-1, 1, 80}) /** @cond */ ->
decltype(make_placeholder<detail::write_text_chart_node>(std::ref(ostream), chart_conf)) /** @endcond */
{ return make_placeholder<detail::write_text_chart_node>(std::ref(ostream), chart_conf); }


/**
* @brief Creates a placeholder for a @ref write_text_chart_grp writing chart in
* [std::cout](https://en.cppreference.com/w/cpp/io/cout) stream.
*
* **Usage example**
* @snippet io_test.cpp write_text_chart usage example
*
* @param chart_conf is the chart appearance configuration.
**/
inline auto cout_text_chart(text_chart_config const& chart_conf = {-1, 1, 80}) /** @cond */ ->
decltype(write_text_chart(std::cout, chart_conf)) /** @endcond */
{ return write_text_chart(std::cout, chart_conf); }


///@} write_text_chart_grp
///@} io_grp
} // namespace nodes
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_WRITE_TEXT_CHART_HPP
