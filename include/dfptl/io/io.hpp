/**
* @file io/io.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_IO_HPP
#define DFPTL_IO_IO_HPP


#include "dfptl/io/write_csv.hpp"
#include "dfptl/io/write_text_chart.hpp"


namespace dfp
{
/**
* @defgroup io_grp Standard I/O stream-related cells
* @brief Defines processing nodes to bridge between STD I/O streams and DFP streams.
*
* @{
*
* @defgroup io_detail implementation details
* @brief Implementation details and boilerplate for @ref io_grp
**/


/**
* @brief Contains items related to processing cells for STD I/O manipulations.
**/
namespace io
{
/**
* @brief Set of helper functions for STD I/O stream-related nodes.
**/
inline namespace nodes{}
namespace knots = nodes;


} // namespace io
} // namespace dfp


#endif // DFPTL_IO_IO_HPP
