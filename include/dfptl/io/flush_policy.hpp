/**
* @file io/flush_policy.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_FLUSH_POLICY_HPP
#define DFPTL_IO_FLUSH_POLICY_HPP


#include <cstdlib>


namespace dfp
{
namespace io
{
///@addtogroup io_grp
///@{


/**
* @brief Defines enumeration value to control flushing of file cache.
**/
enum class flush_policy
{
    system_default = 0x0,   ///< file cache management is left defaulty
    on_eol         = 0x2,   ///< file cache is flushed at each end-of-line
    on_write       = 0x4    ///< file cache is flushed after each "write" operation invocation
};

constexpr int operator&(flush_policy a, flush_policy b)
{ return static_cast<int>(a) & static_cast<int>(b); }

constexpr flush_policy operator|(flush_policy a, flush_policy b)
{ return flush_policy(static_cast<int>(a) & static_cast<int>(b)); }


/**
* @brief Makes integral constant from io::flush_policy value
**/
template<flush_policy Value>
inline constexpr integral_const<flush_policy, Value> mec(flush_policy = Value)
{ return integral_const<flush_policy, Value>{}; }


DFP_INLINE_CONSTEXPR auto FLUSH_POLICY_SYSTEM_DEFAULT = mec<flush_policy::system_default>();
DFP_INLINE_CONSTEXPR auto FLUSH_POLICY_ON_EOL = mec<flush_policy::on_eol>();
DFP_INLINE_CONSTEXPR auto FLUSH_POLICY_ON_WRITE = mec<flush_policy::on_write>();



///@} io_grp
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_FLUSH_POLICY_HPP
