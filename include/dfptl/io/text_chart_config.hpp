/**
* @file io/text_chart_config.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_TEXT_CHART_CONFIG_HPP
#define DFPTL_IO_TEXT_CHART_CONFIG_HPP


#include <cstdlib>


namespace dfp
{
namespace io
{
///@addtogroup io_grp
///@{


/**
* @brief configuration for text-chart appearance
* @sa write_text_chart_grp
**/
struct text_chart_config
{
    float y_min; /**< max possible value on y-axis **/
    float y_max; /**< min possible value on y-axis **/
    std::size_t y_point_count; /**< count of point on y-axis **/
}; // text_chart_config


///@} io_grp
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_TEXT_CHART_CONFIG_HPP
