/**
* @file io/write_csv.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_WRITE_CSV_HPP
#define DFPTL_IO_WRITE_CSV_HPP


#include "dfptl/io/flush_policy.hpp"
#include "dfptl/io/detail/write_csv_node.hpp"

#include <iostream>


namespace dfp
{
namespace io
{
inline namespace nodes
{
///@addtogroup io_grp
///@{
/**
* @defgroup write_csv_grp "write to CSV" node
* @brief Writes on [basic STL output stream](https://en.cppreference.com/w/cpp/io/basic_ostream) using Comma-Separated-Values format
* the samples from the 1st-input DFP @ref stream_grp reaching this node.
*
* 1st-input node stream is forwarded to its output port without modification.
*
* Usually the @ref write_csv_grp expects 2 streams on its input ports, a first one conveying the samples to write
* and a second one conveying the indicator of new line for the STL output stream.
*
* However a simplify version of the @ref write_csv_grp also accepts one single stream as input
* while new line is automatically inserted at end of each frame.
* 
* **Node interfaces**
* * for 2 input @ref atomic_stream "atomic streams"
* @startuml
* rectangle "o(n) = i(n) | write i(n) in CSV" <<write_csv(ostream)>> as write
* interface "...i(n)" <<#0-DATA>> as in
* interface "...e(n)" <<#1-LINE_FEED>> as lf
* interface "...i(n)" <<"forwarded DATA">> as out
* write <-up- in
* write <-up- lf
* out <-up- write
* @enduml
*
* stream name      | location    | description
* -----------------|-------------|------------
* DATA             | **input-0** | @ref atomic_stream "atomic sream" of samples to write on the STL output stream.
* LINE_FEED        | **input-1** | @ref atomic_stream "atomic sream" of boolean indicator to control insertion of new line in the STL output stream.
* "forwarded DATA" | **output**  | the exact same stream as the DATA input one.
*
* * for 2 input @ref static_stream "static streams" with equal frame length.
* @startuml
* rectangle "...[o(n) ...o(n+L)] = ...[i(n) ...i(n+L)] | write i(n) in CSV" <<write_csv(ostream)>> as write
* interface "...[i(n) ...i(n+L)]" <<#0-DATA>> as in
* interface "...[e(n) ...e(n+L)]" <<#1-LINE_FEED>> as lf
* interface "...[i(n) ...i(n+L)]" <<"forwarded DATA">> as out
* write <-up- in
* write <-up- lf
* out <-up- write
* @enduml
*
* stream name      | location    | description
* -----------------|-------------|------------
* DATA             | **input-0** | @ref static_stream "static sream" of sample frames to write on the STL output stream.
* LINE_FEED        | **input-1** | @ref static_stream "static sream" of boolean indicator to control insertion of new line in the STL output stream.
* "forwarded DATA" | **output**  | the exact same stream as the DATA input one.

* * for 1 single input @ref static_stream "static streams".
* @startuml
* rectangle "...[o(n) ...o(n+L)] = ...[i(n) ...i(n+L)] | write i(n) in CSV" <<write_csv(ostream)>> as write
* interface "...[i(n) ...i(n+L)]" <<#0-DATA>> as in
* interface "...[i(n) ...i(n+L)]" <<"forwarded DATA">> as out
* write <-up- in
* out <-up- write
* @enduml
*
* stream name      | location    | description
* -----------------|-------------|------------
* DATA             | **input-0** | @ref static_stream "static sream" of sample frames to write on the STL output stream with automatic new line at each end of frame.
* "forwarded DATA" | **output**  | the exact same stream as the DATA input one.

* @{
**/


/**
* @brief Creates a cell builder for a @ref write_csv_grp writing in CSV format into the given [std::ostream](https://en.cppreference.com/w/cpp/io/basic_ostream).
*
* **Usage example**
*
* refer to @ref cout_csv()
*
* @param ostream is the [std::ostream](https://en.cppreference.com/w/cpp/io/basic_ostream) into which to write chart.
* @param sample_separator is character separator between samples
* @param flush_policy defines how often cache buffers shall be flushed to storing file. It type shall be an integral_constant of io::flush_policy
*
* @tparam FlushPolicy template parameter type implicitly deduced from @c flush_policy parameter.
**/
// add move initializer
template<class FlushPolicy = decltype(mec<flush_policy::on_eol>())>
inline auto write_csv(std::ostream& ostream, char sample_separator, FlushPolicy flush_policy) /** @cond */ ->
decltype(make_placeholder<std::tuple<FlushPolicy>, detail::write_csv_node>(std::ref(ostream), sample_separator)) /** @endcond */
{ return make_placeholder<std::tuple<FlushPolicy>, detail::write_csv_node>(std::ref(ostream), sample_separator); }


template<flush_policy FlushPolicy = flush_policy::on_eol>
inline auto write_csv(std::ostream& ostream, char sample_separator = ',') /** @cond */ ->
decltype(write_csv(ostream, sample_separator, mec<FlushPolicy>())) /** @endcond */
{ return write_csv(ostream, sample_separator, mec<FlushPolicy>()); }


/**
* @brief Creates a cell builder for a @ref write_csv_grp writing in CSV format into the [std::cout](https://en.cppreference.com/w/cpp/io/cout) stream.
*
* **Usage example**
* @snippet io_test.cpp write_csv usage example
*
*
* @param sample_separator is character separator between samples
* @param flush_policy defines how often cache buffers shall be flushed to storing file. It type shall an integral_constant of io::flush_policy
*
* @tparam FlushPolicy template parameter type implicitly deduced from @c flush_policy parameter.
**/
template<class FlushPolicy = decltype(mec<flush_policy::on_eol>())>
inline auto cout_csv(char sample_separator, FlushPolicy flush_policy) /** @cond */ ->
decltype(write_csv(std::cout, sample_separator, flush_policy)) /** @endcond */
{ return write_csv(std::cout, sample_separator, flush_policy); }


template<flush_policy FlushPolicy = flush_policy::on_eol>
inline auto cout_csv(char sample_separator = ',') /** @cond */ ->
decltype(cout_csv(sample_separator, mec<FlushPolicy>())) /** @endcond */
{ return cout_csv(sample_separator, mec<FlushPolicy>()); }


///@} write_csv_grp
///@} io_grp
} // namespace nodes
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_WRITE_CSV_HPP
