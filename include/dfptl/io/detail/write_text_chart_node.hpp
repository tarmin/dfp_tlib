/**
* @file io/detail/write_text_chart_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_WRITE_TEXT_CHART_NODE_HPP
#define DFPTL_IO_WRITE_TEXT_CHART_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/io/text_chart_config.hpp"

#include <cmath>
#include <ostream>


namespace dfp
{
namespace io
{
namespace detail
{
///@addtogroup io_grp
///@{
///@addtogroup io_detail
///@{


#define DFP_FILE_WRITE_TEXT_CHART file::write_text_chart


template<class Input>
class write_text_chart_node
 :  public  node<Input, as_input0_tag, write_text_chart_node<Input>>
{
    typedef node<Input, as_input0_tag, write_text_chart_node> node_type;

    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_FILE_WRITE_TEXT_CHART, 1));

    static_assert(
        std::is_arithmetic<input_sample_t<node_type>>{},
        DFP_INVALID_INPUT_SAMPLE(DFP_FILE_WRITE_TEXT_CHART, ARITHMETIC, 0)
    );

public:
    write_text_chart_node(Input&& input, std::ostream& ostream, text_chart_config const& chart_conf)
     :  node_type(std::forward<Input>(input)), ostream_(ostream), chart_conf_(chart_conf)
    {
        //TODO: raise error if y_max < y_min
    }

    output_sample_t<node_type> const& process_sample(input_sample_t<node_type> const& input)
    {
        float const x_axis_offset = 0; // x-axis offset on y-axis
        float const y_span = chart_conf_.y_max - chart_conf_.y_min;
        int x_axis_position;
        std::string line(chart_conf_.y_point_count, ' ');
        float const pitch_per_char = y_span / (chart_conf_.y_point_count-1);
        char const x_axis_symbol = '|';

        //the following computation should be done in contructor
        x_axis_position = (int) std::round((y_span - (chart_conf_.y_max+chart_conf_.y_min))/2 / pitch_per_char);

        bool const is_above_x_axis = input >= x_axis_offset;
        int const bar_start_position = std::min(std::max(x_axis_position, 0), (int)(chart_conf_.y_point_count-1));
        int scaled;

        if(is_above_x_axis && (x_axis_offset <= chart_conf_.y_max))
        {
            float const bar_start = std::max(chart_conf_.y_min, x_axis_offset);
            scaled = (int) std::round(std::min((float)input - bar_start, chart_conf_.y_max) / pitch_per_char);

            if (scaled >= 0)
                line.replace(bar_start_position, scaled+1, scaled+1, 'x');
            else
                line[0] = '<';

            if (input > chart_conf_.y_max)
                line[chart_conf_.y_point_count-1] = '>';
        }
        else
        {
            float const bar_start = std::min(chart_conf_.y_max, x_axis_offset);
            scaled = (int) -std::round(std::max((float)input - bar_start, chart_conf_.y_min) / pitch_per_char);

            if (scaled >= 0)
                line.replace(bar_start_position-scaled, scaled+1, scaled+1, 'x');
            else
                line[chart_conf_.y_point_count-1] = '>';

            if (input < chart_conf_.y_min)
                line[0] = '<';
        }

        if (((bar_start_position+scaled) != x_axis_position) && (x_axis_position >= 0) && (x_axis_position < (int)chart_conf_.y_point_count))
            line[x_axis_position] = x_axis_symbol;

        ostream_ << line << std::endl;

        return input;
    }

private:
    std::ostream& ostream_;
    text_chart_config const chart_conf_;
}; //class write_text_chart_node


///@} io_detail
///@} io_grp
} // namespace detail
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_WRITE_TEXT_CHART_NODE_HPP
