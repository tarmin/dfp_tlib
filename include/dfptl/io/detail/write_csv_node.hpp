/**
* @file file/detail/write_csv_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_IO_DETAIL_WRITE_CSV_NODE_HPP
#define DFPTL_IO_DETAIL_WRITE_CSV_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/io/flush_policy.hpp"

#include <ostream>

#include <boost/fusion/include/at_c.hpp>
#include <boost/fusion/include/is_sequence.hpp>
#include <boost/fusion/include/std_tuple.hpp>


namespace dfp
{
namespace io
{
namespace detail
{
///@addtogroup io_grp
///@{
///@addtogroup io_detail
///@{


#define DFP_IO_WRITE_CSV io::write_csv


//TODO: move to cluster.hpp and improve
template<class Sequence, std::size_t Size>
struct insert_sequence_to_stream
{
    std::ostream& operator()(std::ostream& stream, Sequence const& s)
    { return insert_sequence_to_stream<Sequence, Size-1>()(stream, s) << ' ' << boost::fusion::at_c<Size-1>(s); }
};

template<class Sequence>
struct insert_sequence_to_stream<Sequence, 1>
{
    std::ostream& operator()(std::ostream& stream, Sequence const& s)
    { return stream << boost::fusion::at_c<0>(s); }
};


template<class Sequence, DFP_F_REQUIRES((boost::fusion::traits::is_sequence<Sequence>::value))>
std::ostream& operator<<(std::ostream& stream, Sequence const& s)
{ return insert_sequence_to_stream<Sequence, boost::fusion::result_of::size<Sequence>::type::value>()(stream, s); }


template<
    class Input, class FlushPolicy, class Enable = void>
class write_csv_node
{
    static_assert(std::is_convertible<FlushPolicy, flush_policy>{}, "FlushPolicy template parameter of write_csv() shall be convertible to io::flush_policy");
    static_assert(
        (std::tuple_size<Input>{} == 1) || (std::tuple_size<Input>{} == 2),
        DFP_INVALID_INPUT_COUNT(DFP_IO_WRITE_CSV, 1 or 2)
    );
};


template<class Inputs, class FlushPolicy, class Enable = void>
class write_csv_node_double_inputs
{
    static_assert(
        (output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::FRAME_EXTENT != DYNAMIC_EXTENT) &&
        (output_stream_specifier_t<std::tuple_element_t<1, Inputs>>::FRAME_EXTENT != DYNAMIC_EXTENT),
        DFP_INVALID_INPUT_FRAME_EXTENT(DFP_IO_WRITE_CSV, to be static, #0 & #1)
    );
    static_assert(
        output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::FRAME_EXTENT == output_stream_t<std::tuple_element_t<1, Inputs>>::FRAME_EXTENT,
        DFP_INVALID_INPUT_FRAME_EXTENT(DFP_IO_WRITE_CSV, to be equal, #0 & #1)
    );
};



template<class Input, class FlushPolicy>
class write_csv_node_single_input
 :  public  node<Input, as_input0_tag, write_csv_node<Input, FlushPolicy>>
{
    typedef node<Input, as_input0_tag, write_csv_node<Input, FlushPolicy>> node_type;
    typedef typename node_type::output_specifier_type output_specifier_type;

public:
    write_csv_node_single_input(Input&& input, std::ostream& file_stream, char sample_separator)
     :  node_type(
            std::forward<Input>(input),
            output_specifier_type{}.set_frame_length(node_type::output_stream_specifier_of(input).get_frame_length())
        ),
        file_stream_(file_stream), sample_separator_(sample_separator)
    {}

    void initialize()
    {
        if(this->input_stream_specifier().get_channel_count() == DYNAMIC_EXTENT)
            throw channel_count_error(DFP_STRINGIFY(DFP_IO_WRITE_CSV), 0, "It shall not be undefined at initialization stage.");        
    }

    size_t_const<0> forward_frame(input_frame_t<node_type, 0> const& data_frame)
    {
        auto data = data_frame.cbegin();
        auto const end = data_frame.cend();

        if (data < end)
        {
            file_stream_ << *data++;

            for ( ; data < end ; data++)
            {
                file_stream_ << sample_separator_;

                file_stream_ << *data;
                if(FlushPolicy{} & flush_policy::on_write)
                    file_stream_.flush();
            }

        }

        file_stream_ << std::endl;
        if(FlushPolicy{} & flush_policy::on_eol)
            file_stream_.flush();

        return 0_c;
    }

private:
    std::ostream&   file_stream_;
    char const      sample_separator_;
}; // class write_csv_node_single_input


template<class Inputs, class FlushPolicy>
class write_csv_node<Inputs, FlushPolicy, DFP_C_REQUIRES((std::tuple_size<Inputs>{} == 2))>
 :  public write_csv_node_double_inputs<Inputs, FlushPolicy>
{ using write_csv_node_double_inputs<Inputs, FlushPolicy>::write_csv_node_double_inputs; };


template<class Input, class FlushPolicy>
class write_csv_node<Input, FlushPolicy, DFP_C_REQUIRES((std::tuple_size<Input>{} == 1))>
 :  public write_csv_node_single_input<Input, FlushPolicy>
{ using write_csv_node_single_input<Input, FlushPolicy>::write_csv_node_single_input; };


template<class Inputs, class FlushPolicy>
class write_csv_node_double_inputs<Inputs, FlushPolicy, DFP_C_REQUIRES((
    std::is_convertible<FlushPolicy, flush_policy>{} &&
    output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::IS_ATOMIC &&
    output_stream_specifier_t<std::tuple_element_t<1, Inputs>>::IS_ATOMIC
))>
 :  public  node<Inputs, output_stream_specifier_t<std::tuple_element_t<0, Inputs>>, write_csv_node<Inputs, FlushPolicy>>
{
    typedef node<Inputs, output_stream_specifier_t<std::tuple_element_t<0, Inputs>>, write_csv_node<Inputs, FlushPolicy>> node_type;

public:
    write_csv_node_double_inputs(Inputs&& inputs, std::ostream& file_stream, char sample_separator)
        :   node_type(std::forward<Inputs>(inputs)), file_stream_(file_stream), sample_separator_(sample_separator),
            line_start_(true)
    {}

    size_t_const<0> forward_sample(input_sample_t<node_type, 0> const& input, input_sample_t<node_type, 1> const& new_line)
    {
        if(!line_start_)
            file_stream_ << sample_separator_;

        file_stream_ << input;
        if(FlushPolicy{} & flush_policy::on_write)
            file_stream_.flush();

        if(new_line)
        {
            file_stream_ << std::endl;
            line_start_ = true;
            if(FlushPolicy{} & flush_policy::on_eol)
                file_stream_.flush();
        }
        else
        {   line_start_ = false; }

        return 0_c;
    }

private:
    std::ostream&   file_stream_;
    char const      sample_separator_;
    bool            line_start_;
}; // class write_csv_node_double_inputs<... ATOMIC input streams ...>


template<class Inputs, class FlushPolicy>
class write_csv_node_double_inputs<Inputs, FlushPolicy, DFP_C_REQUIRES((
    std::is_convertible<FlushPolicy, flush_policy>{} &&
    output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::FRAME_EXTENT == output_stream_specifier_t<std::tuple_element_t<1, Inputs>>::FRAME_EXTENT &&
    output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::IS_STATIC &&
    output_stream_specifier_t<std::tuple_element_t<0, Inputs>>::FRAME_EXTENT != 1
))>
 :  public  node<Inputs, output_stream_specifier_t<std::tuple_element_t<0, Inputs>>, write_csv_node<Inputs, FlushPolicy>>
{
    typedef node<Inputs, output_stream_specifier_t<std::tuple_element_t<0, Inputs>>, write_csv_node<Inputs, FlushPolicy>> node_type;

public:
    write_csv_node_double_inputs(Inputs&& inputs, std::ostream& file_stream, char sample_separator)
        :   node_type(std::forward<Inputs>(inputs)), file_stream_(file_stream), sample_separator_(sample_separator),
            line_start_(true)
    {}

    size_t_const<0> forward_frame(input_frame_t<node_type, 0> const& data_frame, input_frame_t<node_type, 1> const& new_line_frame)
    {
        auto new_line = new_line_frame.cbegin();
 
        for (auto data = data_frame.cbegin() ; data < data_frame.cend() ; data++, new_line++)
        {
            if(!line_start_)
                file_stream_ << sample_separator_;

            file_stream_ << *data;
            if(FlushPolicy{} & flush_policy::on_write)
                file_stream_.flush();

            if(*new_line)
            {
                file_stream_ << std::endl;
                line_start_ = true;
                if(FlushPolicy{} & flush_policy::on_eol)
                    file_stream_.flush();
            }
            else
            {   line_start_ = false; }
        }

        return 0_c;
    }

private:
    std::ostream&   file_stream_;
    char const      sample_separator_;
    bool            line_start_;
}; // class write_csv_node_double_inputs<... non-ATOMIC input streams ...>


///@} io_detail
///@} io_grp
} // namespace detail
} // namespace io
} // namespace dfp


#endif // DFPTL_IO_DETAIL_WRITE_CSV_NODE_HPP
