/**
* @file routing/synced_buffer/share.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_SINCED_BUFFER_SHARE_HPP
#define DFPTL_ROUTING_SINCED_BUFFER_SHARE_HPP


#include <deque>
#include <mutex>
#include <condition_variable>


namespace dfp
{
namespace routing
{
namespace synced_buffer
{
namespace detail
{
template<class, class, class> class write_node;
template<class, class, class, class, class> class read_source;
}
///@addtogroup routing_grp
///@{
///@addtogroup synced_buffer_grp
///@{


/**
* @brief Shared ressource for @ref synced_buffer_grp
*
* @tparam MaxLength is the buffer length
**/
template<class Sample, std::size_t MaxLength = (std::size_t) -1>
struct share
{
    typedef Sample sample_type;
    typedef std::deque<Sample> container_type; //for performance consideration, investigate relevance of working with boost::circular_buffer

    template<class, class, class> friend class detail::write_node;
    template<class, class, class, class, class> friend class detail::read_source;

    template<std::size_t L = MaxLength, DFP_F_REQUIRES((L != (std::size_t)-1))>
    share() : container(MaxLength), length(MaxLength) { container.clear(); }

    template<std::size_t L = MaxLength, DFP_F_REQUIRES((L == (std::size_t)-1))>
    share(std::size_t buffer_length) : container(buffer_length), length(buffer_length) { container.clear(); }

    share(share&& src)
     :  container(std::move(src.container)), cv(std::move(src.cv)), mutex(std::move(src.mutex))
    {}

    share(share const&) = delete;

    size_t_const<MaxLength> max_length() const { return size_t_const<MaxLength>{}; }

private:
    container_type container;
    std::size_t length;
    std::condition_variable cv;
    std::mutex mutex;
}; // class share


///@} synced_buffer_grp
///@} routing_grp
} // namespace synced_buffer
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_SINCED_BUFFER_SHARE_HPP
