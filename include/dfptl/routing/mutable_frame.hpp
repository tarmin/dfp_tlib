//TODO: rename into frame_ref
/**
* @file routing/mutable_frame.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_MUTABLE_FRAME_HPP
#define DFPTL_ROUTING_MUTABLE_FRAME_HPP


#include "dfptl/routing/detail/mutable_frame_source.hpp"
#include "dfptl/routing/detail/const_frame_source.hpp"

#include "stl/tuple.hpp"
#include "stl/span.hpp"
#include "stl/experimental/array.hpp"
#include <array>
#include <utility>
#include <initializer_list>


namespace dfp
{
namespace routing
{
inline namespace sources
{
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation-html"
#endif
///@addtogroup routing_grp
///@{
/**
* @defgroup mutable_frame_grp "mutable-frame" source
* @todo to be rename into frame_from_ref
* @brief Produces a stream of mutable frame.
*
* A @ref mutable_frame_grp feeds a processing branch with an mutable samples frame over the time.
* Given input frame can be a STL-container or a C-array.
* Frame samples can be arranged in different channels, If so they shall be stored interleaved in the given frame,
* e.g. for a 3-channels frame the samples mapping shall be :
* [s00, s10, s20, s01, s11, s21, s02, s12, s22, s03, ..., sXY] where @c X is the channel index and @c Y the sample rank in the channel.
*
* **Source interface**
* @startuml
* rectangle "o(n) = *&frame[n % frame.size()]" <<const_frame(frame)>> as src
* interface "output: [*&frame[0] *&frame[1] ... *&frame[n-1]] [*&frame[0] ... o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | <ul><li>a @ref stream_grp with static frame extent conveying the given mutable frame, if the given container is a std::array or C-array</li><li>Otherwise the frame extent of the produced @ref stream_grp is dynamic</li></ul>
*
* @sa const_frame_grp, mutable_sample_grp
* @todo: [std::span](https://en.cppreference.com/w/cpp/container/span) as input container should make the source producing a static frame stream
* if @c Extent is not @c std::dynamic_extent
* @{
**/
#ifdef __clang__
#pragma clang diagnostic pop
#endif


/**
* @brief Creates a @ref mutable_frame_grp from sequence container with a static extent of channel count whose value is given as a function parameter.
*
* @param container is a lvalue reference on a [sequence container](https://en.cppreference.com/w/cpp/named_req/SequenceContainer)
* that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed as compile-time integral constant convertible to @ref dfp::core::size_t_const "size_t_const".
* @note Frame length shall be divisible by the @c channel_count value or compile-time error will be raised.
*
* @tparam Container type is implicitly deduced from @c container parameter.
* @tparam ChannelCount type is implicitly deduced from @c channel_count parameter.
*
* **Usage example**
* @snippet routing_test.cpp mutable_frame usage example with std::array as input
* Producing a mutable 2-channels stream from a @c std::array
* @snippet routing_test.cpp 2-channels mutable_frame from array
**/
template<
    class Container, class Integral = std::size_t, Integral ChannelCount = 1 /** @cond */, DFP_F_REQUIRES((
        !std::is_array<Container>{}
    )) /** @endcond */>
inline auto mutable_frame(Container const& container, integral_const<Integral, ChannelCount> channel_count = integral_const<Integral, ChannelCount>{}) ->
decltype(make_placeholder<std::tuple<Container, size_t_const<ChannelCount>>, detail::mutable_frame_source>(std::cref(container)))
{ static_assert(channel_count > 0, "channel_count shall be a strictly positive number");
  static_assert(supports_const_iterator_check<Container>{}, "A const iterator shall be available for Container type");
  return make_placeholder<std::tuple<Container, size_t_const<ChannelCount>>, detail::mutable_frame_source>(std::cref(container)); }


/**
* @brief Creates a @ref mutable_frame_grp from sequence container with a dynamic extent of channel count.
*
* @param container is a lvalue reference on a [sequence container](https://en.cppreference.com/w/cpp/named_req/SequenceContainer)
* that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed at runtime.
*
* @tparam Container type is implicitly deduced from @c container parameter.
*
* **Usage example**
* Producing a mutable 2-channels stream whose channel size has been specified at runtime
* @snippet routing_test.cpp 2-channels mutable_frame specified at runtime
**/
template<
    class Container /** @cond */, DFP_F_REQUIRES((
        !std::is_array<Container>{}
    )) /** @endcond */>
inline auto mutable_frame(Container const& container, std::size_t channel_count) ->
decltype(make_placeholder<std::tuple<Container, decltype(DYNAMIC_EXTENT)>, detail::mutable_frame_source>(std::cref(container), channel_count))
{ static_assert(supports_const_iterator_check<Container>{}, "A const iterator shall be available for Container type");
  return make_placeholder<std::tuple<Container, decltype(DYNAMIC_EXTENT)>, detail::mutable_frame_source>(std::cref(container), channel_count); }


/**
* @brief Creates a @ref mutable_frame_grp from C-array with a static extent of channel count whose value is given as a function parameter.
*
* @param c_array is a C-array passed as lvalue reference that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed as compile-time integral constant convertible to @ref dfp::core::size_t_const "size_t_const".
* @note Frame length shall be divisible by the @c channel_count value or compile-time error will be raised.
*
* **Usage example**
* @snippet routing_test.cpp mutable_frame usage example with C-array as input
* @snippet routing_test.cpp mutable_frame usage example with 2-channels C-array as input
*
* @tparam T is implicitly deduced from @c c_array parameter
* @tparam Size is implicitly deduced from @c c_array parameter
* @tparam ChannelCount type is implicitly deduced from @c channel_count parameter.
**/
template<class T, std::size_t Size, class Integral = std::size_t, Integral ChannelCount = 1>
inline auto mutable_frame(T const(&c_array)[Size], integral_const<Integral, ChannelCount> channel_count = integral_const<Integral, ChannelCount>{}) ->
decltype(make_placeholder<std::tuple<upg::span<T const, Size>, size_t_const<ChannelCount>>, detail::const_frame_source>(upg::span<T const, Size>(c_array)))
{ static_assert(channel_count > 0, "channel_count shall be a strictly positive number");
  return make_placeholder<std::tuple<upg::span<T const, Size>, size_t_const<ChannelCount>>, detail::const_frame_source>(upg::span<T const, Size>(c_array)); }


/**
* @brief Creates a @ref mutable_frame_grp from C-array with a dynamic extent of channel count.
*
* @param c_array is a C-array lvalue reference that is the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed at runtime.
*
* **Usage example**
* @snippet routing_test.cpp 2-channels mutable_frame from c-array specified at runtime
*
* @tparam T is implicitly deduced from @c c_array parameter
* @tparam Size is implicitly deduced from @c c_array parameter
**/
template<class T, std::size_t Size>
inline auto mutable_frame(T const(&c_array)[Size], std::size_t channel_count) ->
decltype(make_placeholder<std::tuple<upg::span<T const, Size>, decltype(DYNAMIC_EXTENT)>, detail::const_frame_source>(upg::span<T const, Size>(c_array), channel_count))
{ return make_placeholder<std::tuple<upg::span<T const, Size>, decltype(DYNAMIC_EXTENT)>, detail::const_frame_source>(upg::span<T const, Size>(c_array), channel_count); }


///@} mutable_frame_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
//TODO: create alias ffrmref
using routing::sources::mutable_frame;
} // namespace sources
} // namespace shortname

///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_MUTABLE_FRAME_HPP
