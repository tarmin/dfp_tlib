/**
* @file routing/delay.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DELAY_HPP
#define DFPTL_ROUTING_DELAY_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/delay_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup delay_grp "delay" node
* @brief Introduces some delay expressed in sample count on the input data stream.
*
* A @ref delay_grp forwards to its output the received sample on its SINGLE input with some delay expressed in sample count.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n-d)" <<delay(d)>> as delay
* interface "input: 1 2 ...i(n)" <<INPUT>> as in
* interface "output: 0 ... 1 2 ...i(n-d)" <<operation result>> as out
* delay <-up- in
* out <-up- delay
* @enduml
*
* stream name         | location    | description
* --------------------|-------------|------------
* INPUT               | **input-0** | @ref atomic_stream "atomic stream" conveying sample to delay
* "operation result"  | **output**  | @ref atomic_stream "atomic stream" resulting from input delaying
*
* @todo add support for input stream with frame length > 1
* @todo add support for initial_value as function-object
* @{
**/


/**
* @brief Creates a cell builder for a @ref delay_grp.
*
* **Usage example**
* @snippet routing_test.cpp delay node usage with delay length as std::size_t
*
* @param delay_length delay to insert in the input stream expressed in sample count.
* Its value shall be strictly above 0.
* @param initial_value is a C-array, stl-sequence container, single value or as_input0_tag{}.
* If C-array or container, its content represents the first values to produce on the output
* at beginning of the processing. Container length shall be equal to the given @c delay_length.
* It can also take value @c as_input0_tag{}, if so, first sample provided by the input stream is duplicated
* on the output until end of the first delay period.
* @warning initial_value support is for now limited to single value
*
* @tparam InitialValue template parameter type implicitly deduced from @c initial_value parameter.
**/
template<class InitialValue = int_const<0>>
auto delay(std::size_t delay_length, InitialValue const& initial_value = InitialValue{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<std::size_t, InitialValue>, detail::delay_node>(delay_length, initial_value)) /** @endcond */
{ return make_placeholder<std::tuple<std::size_t, InitialValue>, detail::delay_node>(delay_length, initial_value); }


/**
* @brief Creates a cell builder for a @ref delay_grp.
*
* **Usage example**
* @snippet routing_test.cpp delay node usage with delay length as size_t integral constant
*
* @return an optimized delay node (with memory usage reduced) because @c delay_length is an integral constant
*
* @param delay_length delay to insert in the input stream expressed in sample count.
* Its value shall be strictly above 0.
* @param initial_value is a C-array, stl-sequence container, single value or as_input0_tag{}.
* If C-array or container, its content represents the first values to produce on the output
* at beginning of the processing. Container length shall be equal to the given @c delay_length.
* It can also take value @c as_input0_tag{}, if so, first sample provided by the input stream is duplicated
* on the output until end of the first delay period.
* @warning initial_value support is for now limited to single value
*
* @tparam DelayLength delay to insert in the input stream expressed in sample count.
* Its value shall be strictly above 0. it can be deduced from @c delay_length parameter.
* @tparam InitialValue template parameter type implicitly deduced from @c initial_value parameter.
**/
template<class Integral = std::size_t, Integral DelayLength = 1, class InitialValue = int_const<0>>
constexpr auto delay(integral_const<Integral, DelayLength> delay_length = integral_const<Integral, DelayLength>{}, InitialValue const& initial_value = InitialValue{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<size_t_const<DelayLength>, InitialValue>, detail::delay_node>(initial_value)) /** @endcond */
{ static_assert(delay_length > 0, "delay_length shall be a strictly positive number");
  return make_placeholder<std::tuple<size_t_const<delay_length>, InitialValue>, detail::delay_node>(initial_value); }


///@} delay_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using routing::nodes::delay;
} // namespace nodes
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DELAY_HPP
