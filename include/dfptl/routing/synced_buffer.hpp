/**
* @file routing/synced_buffer.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_SINCED_BUFFER_HPP
#define DFPTL_ROUTING_SINCED_BUFFER_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/synced_buffer/share.hpp"
#include "dfptl/routing/detail/synced_buffer/write_node.hpp"
#include "dfptl/routing/detail/synced_buffer/read_source.hpp"


namespace dfp
{
namespace routing
{
namespace synced_buffer
{
///@addtogroup routing_grp
///@{
/**
* @defgroup synced_buffer_grp "synchonized-buffer" cells
* @brief Synchronized transfer of a samples stream between 2 processing trees run in different execution threads.
*
* @ref synced_buffer_grp are dedicated for synchronized samples transfer between one unique producer and another unique consumer in different threads.
*
* **Usage example**
* @snippet routing_test.cpp Synced buffer usage
**/


inline namespace nodes
{
///@addtogroup synced_buffer_grp
///@{
/**
* @defgroup synced_buffer_write_grp "synchonized-buffer-write" node
* @brief Writes samples from its single input in an external buffer safe for multi-thread access.
*
* A @ref synced_buffer_write_grp performed frame-based saving of samples arriving on its single input in a buffer.
* Writing in the buffer is synchronized to reading from a different thread.
*
* Its input stream is forwarded to its output port without modification.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n) | save i(n) into buffer" <<synced_buffer::write(buffer)>> as write
* interface "input: ...i(n)" <<in>> as in
* interface "output: ...i(n)" <<operation result>> as out
* write <- in
* out <- write
* @enduml
* * <tt>"in" **input**              </tt> : any @ref stream_grp
* to write to buffer
* * <tt>"operation result" **output**</tt> : the same stream as the input one.
* @{
**/


/**
* @brief Creates a placeholder for a @ref synced_buffer_write_grp.
*
* **Usage example**
* @snippet alsa_test.cpp real-time echo cancellation
*
* @param buffer is the shared ressource for the synchronized buffering.
*
* @tparam Sample is implicitly deduced from @c buffer parameter
* @tparam BufferLength is implicitly deduced from @c buffer parameter
*
* @todo add overflow policy (wait, block, throw_error, silently_override, override) -> for now override
**/
template<class Sample, std::size_t BufferLength>
inline auto write(share<Sample, BufferLength>& buffer) /** @cond */ ->
decltype(make_placeholder<std::tuple<Sample, size_t_const<BufferLength>>, detail::write_node>(std::ref(buffer))) /** @endcond */
{ return make_placeholder<std::tuple<Sample, size_t_const<BufferLength>>, detail::write_node>(std::ref(buffer)); }


///@} synced_buffer_write_grp
} // namespace nodes


inline namespace sources
{
///@addtogroup synced_buffer_grp
///@{
/**
* @defgroup synced_buffer_read_grp "synchonized-buffer-read" source
* @brief Synchronized extract of samples frames from buffer.
*
* A @ref synced_buffer_read_grp performs frame-based extract of samples stored in a synchonized safe data exchange between different thread.
*
* **Source interface**
* @startuml
* rectangle "o(n) = | extract sample from buffer" <<synced_buffer::read(buffer)>> as read
* interface "output: ...o(n)" <<operation result>> as out
* out <- read
* @enduml
* * <tt>"operation result" **output**</tt> : A @ref stream_grp with static frame extent of samples extracted from buffer.
* @{
**/


/**
* @brief Creates a instance of @ref synced_buffer_read_grp.
*
* **Usage example**
* @snippet alsa_test.cpp real-time echo cancellation
*
* @param buffer is the shared ressource for the synchronized buffering.
* @param frame_length length of the output frame
*
* @tparam FrameLength is implicitly deduced from @c frame_length
* @tparam Sample is implicitly deduced from @c buffer parameter
* @tparam BufferMaxLength is implicitly deduced from @c buffer parameter
* @todo add timeout support
* @todo add underflow policy (wait, block, throw_error, silently_zeroing, zeroing) -> for now block
**/
template<class Sample, std::size_t BufferMaxLength, class Integral = std::size_t, Integral FrameLength = BufferMaxLength , DFP_F_REQUIRES((FrameLength != DYNAMIC_EXTENT))>
inline auto
read(share<Sample, BufferMaxLength>& buffer, integral_const<Integral, FrameLength> frame_length = integral_const<Integral, FrameLength>{}) ->
decltype(make_placeholder<std::tuple<size_t_const<FrameLength>, Sample, size_t_const<BufferMaxLength>>, detail::read_source>(std::ref(buffer)))
{ static_assert(frame_length > 0, "frame_length shall be a strictly positive number");
 return  make_placeholder<std::tuple<size_t_const<FrameLength>, Sample, size_t_const<BufferMaxLength>>, detail::read_source>(std::ref(buffer)); }


///@} synced_buffer_read_grp
} // namespace sources


namespace shortname
{
namespace sbuff
{
inline namespace nodes
{
using routing::synced_buffer::write;
} // namespace nodes
inline namespace sources
{
using routing::synced_buffer::read;
} // namespace sources
} // namespace sbuff
} // namespace shortname


///@} synced_buffer_grp
///@} routing_grp
} // namespace synced_buffer
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_SINCED_BUFFER_HPP
