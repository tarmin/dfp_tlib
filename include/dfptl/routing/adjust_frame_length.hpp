/**
* @file routing/adjust_frame_length.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_ADJUST_FRAME_LENGTH_HPP
#define DFPTL_ROUTING_ADJUST_FRAME_LENGTH_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/adjust_frame_length_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup adjust_frame_length_grp "adjust frame length" node
* @brief Adjust frame length in stream
*
* A @ref adjust_frame_length_grp adapts frame length of frame reaching its SINGLE input to a given new length value
* and produces the resulting frame on its output.
*
* @note Depending on type of the new frame length value passed to the @ref adjust_frame_length_grp
* (constant integral aka @c size_t_const vs @c size_t) and the input frame extent,
* the resulting frame extent can be static or dynamic according to some internal rules.
* @todo provide synthetic table to explain possible combinations.
*
* @warning Current implementation only supports case where the requested output frame length is divisible by the input frame length.
*
* **Node interface**
* @startuml
* rectangle "adjust_frame_length(size_t_const<4>{})" <<adjust_frame_length()>> as conv
* interface "input: [1 2][3 4][5 6][7 8] ...[i(n-1) i(n)]" <<INPUT>> as in
* interface "output: [1 2 3 4][5 6 7 8] ...[i(n-3) i(n-2) i(n-1) i(n)]" <<conversion result>> as out
* conv <-down- in
* out <-down- conv
* @enduml
*
* stream name         | location    | description
* --------------------|-------------|------------
* INPUT               | **input-0** | @ref core::stream "stream" to transform
* "conversion result" | **output**  | a new @ref stream_grp with adjusted frame length resulting from frame length conversion
*
* @{
**/


/**
* @brief Creates a placeholder for a @ref adjust_frame_length_grp.
*
* With `frame_length` parameter type not an integral constant, this variant of @c adjust_frame_length() method
* will always produce a stream of @ref DYNAMIC_EXTENT frame.
*
* **Usage example**
* @snippet routing_test.cpp Convert atomic_stream into dynamic 10-framelength stream
*
* @param frame_length indicates the length of the frame produced.
**/
auto constexpr adjust_frame_length(std::size_t frame_length) /** @cond */ ->
decltype(make_placeholder<std::tuple<size_t_const<DYNAMIC_EXTENT>>, detail::adjust_frame_length_node>(frame_length)) /** @endcond */
{ return make_placeholder<std::tuple<size_t_const<DYNAMIC_EXTENT>>, detail::adjust_frame_length_node>(frame_length); }


/**
* @brief Creates a placeholder for a @ref adjust_frame_length_grp.
*
* With `frame_length` parameter type as an integral constant, this variant of @c adjust_frame_length() method
* may produce a stream of static exent frame or @ref DYNAMIC_EXTENT frame.
*
* @param frame_length indicates the length of the frame produced.
*
* **Usage example**
* @snippet routing_test.cpp Convert atomic_stream into static 10-framelength stream
*
* @tparam Integral is implicitly deduced from @c frame_length parameter
* @tparam FrameLength is implicitly deduced from @c frame_length parameter
**/
template<class Integral = std::size_t, Integral FrameLength>
auto constexpr adjust_frame_length(std::integral_constant<Integral, FrameLength> frame_length) /** @cond */ ->
decltype(make_placeholder<std::tuple<size_t_const< FrameLength>>, detail::adjust_frame_length_node>()) /** @endcond */
{ static_assert(frame_length > 0, "frame_length shall be a strictly positive number");
  return make_placeholder<std::tuple<size_t_const<frame_length>>, detail::adjust_frame_length_node>(); }


///@} adjust_frame_length_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup adjust_frame_length_grp
///@{
/**
* @brief shortname for @ref routing::nodes::adjust_frame_length() "adjust_frame_length()"
**/
auto constexpr adjustfl(std::size_t frame_length) /** @cond */ ->
decltype(adjust_frame_length(frame_length)) /** @endcond */
{ return adjust_frame_length(frame_length); }


/**
* @brief shortname for @ref routing::nodes::adjust_frame_length() "adjust_frame_length()"
**/
template<class Integral, Integral FrameLength>
inline auto constexpr adjustfl(integral_const<Integral, FrameLength> frame_length) /** @cond */ ->
decltype(adjust_frame_length(frame_length)) /** @endcond */
{ return adjust_frame_length(frame_length); }


///@} adjust_frame_length_grp
} // namespace nodes
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_ADJUST_FRAME_LENGTH_HPP
