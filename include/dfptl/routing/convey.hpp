/**
* @file routing/convey.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_CONVEY_HPP
#define DFPTL_ROUTING_CONVEY_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/const_sample.hpp" // required because of preset_inputs usage
#include "dfptl/routing/detail/convey_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup convey_grp "convey" node
* @brief Guides inputs samples to output according to a selector value.
*
* A @ref convey_grp is a N-inputs processing node which conveys samples from one of its inputs
* according to a selector value stream.
* The selector stream is the latest input of the node.
*
* So each sample reaching the last input of the node indicates the index of the input stream to forward to the output.
* Thus this node requires at least 3 input streams.
*
* Except the selector stream which shall always deliver `std::size_t` value, others inputs shall deliver same sample type. 
* 
* @note Compared to @ref switch_if_grp, all upstream branches are fired on this node input ports whatever is the selected
* output sample.
*
* @note Take attention that on the contrary to @ref switch_if_grp, the selector stream is the last one.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i2(n) ? i1(n) : i0(n)" <<convey()>> as node
* interface "input-2:  T  F  T  F ...i2(n)" <<SELECTOR>> as cond
* interface "input-1: -1 -2 -3 -4 ...i1(n)" <<ALTERNATIVE#1>> as in1
* interface "input-0:  1  2  3  4 ...i0(n)" <<ALTERNATIVE#0>> as in0
* interface "output: -1 2 -3 4 ...o(n)" <<operation result>> as out
*
* in0 -down-> node
* in1 -down-> node
* cond -down-> node
* out <-up- node
* @enduml
* stream name        | location    | description
* -------------------|-------------|------------
* ALTERNATIVE#0      | **input-0** | @ref atomic_stream "atomic stream" #0
* ALTERNATIVE#1      | **input-1** | @ref atomic_stream "atomic stream" #1
* ...                | ...         | ...
* SELECTOR           | **input-N** | @ref atomic_stream "atomic stream" indicating the index of the stream from which to forward the data
* "operation result" | **output**  | the @ref atomic_stream "atomic stream" conveying the operation result
*
* @{
**/


/**
* @brief Creates a cell builder for a @ref convey_grp whose last input is selector
*
* **Usage example**
* @snippet routing_test.cpp convey usage example
*
* @sa switch_if_grp
**/
//TODO: add input parameter /*outof_range_policy[exception[default], last_index, custom_index<X>]*/
constexpr auto convey() noexcept
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(make_placeholder<std::tuple<>, detail::convey_node>()) /** @endcond */
#endif
{ return make_placeholder<std::tuple<>, detail::convey_node>(); }


/**
* @brief Forward input stream selected with help of the given parameter.
* Alias for `convey().preset_inputs(std::forward<Selector>(selector))`
*
* @param selector replaces the last input stream.
* This parameter shall provide a processing graph (or something convertible into it)
* delivering value to select the input stream to forward on the output.
*
* @tparam Selector is implicitly deduced from `selector` parameter. This type shall be
* suitable  for creation of an `std::size_t` sample stream.
*
* **Usage example**
* @snippet routing_test.cpp convey usage example with preset
*
* @sa convey(), switch_if_grp
**/
template<class Selector>
inline auto constexpr convey(Selector&& selector)
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(convey().preset_inputs(std::forward<Selector>(selector))) /** @endcond */
#endif
{ return convey().preset_inputs(std::forward<Selector>(selector)); }


/**
* @brief Always forward 1st input stream (alias for `convey(0_uc)`)
*
* **Usage example**
* @snippet routing_test.cpp convey1st usage example
*
* @sa convey(Selector&&), switch_if_grp
**/
inline auto constexpr convey1st()
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(convey(0_uc)) /** @endcond */
#endif
{ return convey(0_uc); }


/**
* @brief Always forward 2nd input stream (alias for `convey(1_uc)`)
*
* **Usage example**
* @snippet routing_test.cpp convey2nd usage example
*
* @sa convey(Selector&&), switch_if_grp
**/
inline auto constexpr convey2nd()
#if !DFP_CPP14_SUPPORT
/** @cond */ ->
decltype(convey(1_uc)) /** @endcond */
#endif
{ return convey(1_uc); }


///@}
///@}
} // namespace nodes
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_CONVEY_HPP
