/**
* @file routing/switch_if.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_SWITCH_IF_HPP
#define DFPTL_ROUTING_SWITCH_IF_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/switch_if_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup switch_if_grp "switch_if" node
* @brief Drains conditionally one of its 2nd and 3rd input branch based on values delivered by its 1st input stream.
*
* A @ref switch_if_grp is a 3-inputs sample-based node which for each captured sample on its 1st input
* will drain its 2nd input (@c THEN stream) if the sample value is worth @c true or its 3rd input (@c ELSE stream) otherwise.
* The resulting stream is a @ref atomic_stream "atomic stream" interleaving drained samples from the @c THEN or @c ELSE streams.
*
* @warning For now, the @ref switch_if_grp only accepts @ref atomic_stream "atomic stream" on its inputs
*
* **Node interface**
* @startuml
* rectangle "o(n) = (i0(n)) ? *THEN++ : *ELSE++" <<routing::switch_if()>> as routing
* interface "i2(0) ...i2(q)" <<#2-ELSE>> as in2
* interface "i1(0) ...i1(p)" <<#1-THEN>> as in1
* interface "false true true ...i0(p+q)" <<#0-CONDITION>> as in0
* interface "i2(0) i1(0) i1(1) ...o(p+q)" <<stream selection>> as out
* routing <-up- in1
* routing <-up- in2
* routing <- in0
* out <- routing
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* CONDITION          | **input-0** | @ref atomic_stream "atomic stream" as test condition
* THEN               | **input-1** | @ref atomic_stream "atomic stream" drained if condition is @c true
* ELSE               | **input-2** | @ref atomic_stream "atomic stream" drained if condition is @c false
* "selection stream" | **output**  | @ref atomic_stream "atomic stream" resulting from interleaving of data drained from @c THEN and @c ELSE streams.
* @{
**/


/**
* @brief Creates a placeholder for a @ref switch_if_grp to conditionally drain given input streams.
*
* If the sample value of its first input stream (@c CONDITION stream) is @c true, this node will drain
* the given @c THEN stream or the @c ELSE stream otherwise.
* The resulting output stream has same traits as the @c THEN stream.
*
* @note Both branches on input producing @c THEN and @c ELSE input streams shall have roughly identical @ref stream_specifier.
* A tolerance is granted to output sample type of the @c ELSE stream which is simply required to be convertible
* into the output sample type of the @c THEN stream.
*
* **Usage example**
* @snippet routing_test.cpp switch_if usage example
*
* @todo Implement "variant" node similar to switch-case statement
**/
inline constexpr auto switch_if() /** @cond */ ->
decltype(make_placeholder<detail::switch_if_node>()) /** @endcond */
{ return make_placeholder<detail::switch_if_node>(); }


/**
* @copybrief switch_if()
*
* @note Alias for `switch_if().preset_inputs(std::forward<ElseStreamable>(else_streamable))`
*
* **Usage example**
* @snippet routing_test.cpp switch_if with ELSE branch as parameter
*
* @param else_streamable is either a processing branch or convertible into a processing branch (with help of the @ref const_sample_source)
* which will produce the stream if input @c CONDITION value is false (@c ELSE selection).
* 
* @tparam ElseStreamable type is implicitly deduced from @c else_streamable parameter
**/
template<class ElseStreamable>
inline constexpr auto switch_if(ElseStreamable&& else_streamable) /** @cond */ ->
decltype(switch_if().preset_inputs(std::forward<ElseStreamable>(else_streamable))) /** @endcond */
{ return switch_if().preset_inputs(std::forward<ElseStreamable>(else_streamable)); }


/**
* @copybrief switch_if()
*
* @note Alias for `switch_if().preset_inputs(std::forward<ThenStreamable>(then_streamable), std::forward<ElseStreamable>(else_streamable))`
*
* **Usage example**
* @snippet routing_test.cpp switch_if with THEN and ELSE branches as parameters
*
* @param then_streamable is either a processing branch or convertible into a processing branch (with help of the @ref const_sample_source)
* which will produce the stream if input @c CONDITION value is true (@c THEN selection).
*
* @param else_streamable is either a processing branch or convertible into a processing branch (with help of the @ref const_sample_source)
* which will produce the stream if input @c CONDITION value is false (@c ELSE selection).
* 
* @tparam ThenStreamable type is implicitly deduced from @c then_streamable parameter
* @tparam ElseStreamable type is implicitly deduced from @c else_streamable parameter
**/
template<class ThenStreamable, class ElseStreamable>
inline constexpr auto switch_if(ThenStreamable&& then_streamable, ElseStreamable&& else_streamable) /** @cond */ ->
decltype(switch_if().preset_inputs(std::forward<ThenStreamable>(then_streamable), std::forward<ElseStreamable>(else_streamable))) /** @endcond */
{ return switch_if().preset_inputs(std::forward<ThenStreamable>(then_streamable), std::forward<ElseStreamable>(else_streamable)); }


///@} switch_if_grp
///@} routing_grp
} // namespace nodes
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_SWITCH_IF_HPP
