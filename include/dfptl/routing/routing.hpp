/**
* @file routing/routing.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_ROUTING_HPP
#define DFPTL_ROUTING_ROUTING_HPP


#include "dfptl/routing/hold.hpp"
#include "dfptl/routing/delay.hpp"
#include "dfptl/routing/looper.hpp"
#include "dfptl/routing/convey.hpp"
#include "dfptl/routing/drainer.hpp"
#include "dfptl/routing/switch_if.hpp"
#include "dfptl/routing/channelize.hpp"
#include "dfptl/routing/const_frame.hpp"
#include "dfptl/routing/from_iterator.hpp"
#include "dfptl/routing/synced_buffer.hpp"
#include "dfptl/routing/mutable_frame.hpp"
#include "dfptl/routing/extract_channel.hpp"
#include "dfptl/routing/to_single_sample.hpp"
#include "dfptl/routing/sample_static_cast.hpp"
#include "dfptl/routing/adjust_frame_length.hpp"


namespace dfp
{
/**
* @defgroup routing_grp Routing & Routine processing cells
* @brief Defines processing cells helping in stream routing and routine manipulations.
* @{
*
* @defgroup routing_detail implementation details
* @brief Implementation details and boilerplate for @ref routing_grp
**/

/**
* @brief Contains items related to processing cells for stream Routing & Routine manipulations.
**/
namespace routing
{
/**
* @brief Set of helper functions for Routing & Routine nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for Routing & Routine sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief Set of helper functions for Routing & Routine sinks.
**/
inline namespace sinks {}
namespace trunks = sinks;


/**
* @brief short name of functions in @ref routing_grp namespace
**/
namespace shortname {}


///@}
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_ROUTING_HPP
