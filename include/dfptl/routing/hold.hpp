/**
* @file routing/hold.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_HOLD_HPP
#define DFPTL_ROUTING_HOLD_HPP


#include "dfptl/core/from.hpp"
#include "dfptl/core/probe.hpp"
#include "dfptl/core/subprocessing.hpp"
#include "dfptl/routing/switch_if.hpp"
#include "dfptl/alu/logical_not.hpp"


#define DFP_ROUTING_HOLD routing::hold


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup hold_grp "hold" node
* @brief Drains conditionally its 2nd input branch based on values delivered by its 1st input stream.
*
* A @ref hold_grp is a 2-inputs sample-based node which for each captured sample on its 1st input
* will drain its 2nd input (@c REF stream) if the sample value is worth @c false.
* The resulting stream is a @ref atomic_stream "atomic stream" interleaving drained samples from the @c REF
* or a duplication of the very latest drained sample when 1st input sample is worth @c true.
*
* @warning For now, the @ref hold_grp only accepts @ref atomic_stream "atomic stream" on its inputs
*
* **Node interface**
* @startuml
* rectangle "o(n) = (i0(n)) ? *REF++ : *REF" <<routing::hold()>> as routing
* interface "i1(0) ...i1(p)" <<#1-REF>> as in1
* interface "false false true false ...i0(p+q)" <<#0-CONDITION>> as in0
* interface "i1(0) i1(1) i1(1) i1(2) ...o(p+q)" <<stream selection>> as out
* routing <-up- in1
* routing <- in0
* out <- routing
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* CONDITION          | **input-0** | @ref atomic_stream "atomic stream" as test condition
* REF                | **input-1** | @ref atomic_stream "atomic stream" drained if condition is @c false
* "selection stream" | **output**  | @ref atomic_stream "atomic stream" resulting from interleaving of data drained from @c REF and duplication of the very latest drained sample.
* @{
**/


class hold_processing
{
    struct ref_tag : custom_tag {};

public:
    template<class Input0, class Input1>
    auto operator()(Input0 in0, Input1 in1) const ->
    decltype(
        switch_if() += from(ref_tag{})
                    |= probe(ref_tag{}) <<= in1
                    |= alu::logical_not() <<= in0
    )
    { return
        switch_if() += from(ref_tag{})
                    |= probe(ref_tag{}) <<= in1
                    |= alu::logical_not() <<= in0
        ;
    }
};


/**
* @brief Creates a cell builder for a @ref hold_grp to conditionally drain the given input stream.
*
* If the sample value of its first input stream (@c CONDITION stream) is @c true, this node will drain
* the given @c REF stream and publish it on its output. If not, the very lastest drained sample returned.
* The resulting output stream has same traits as the @c REF stream.
*
* **Usage example**
* @snippet routing_test.cpp hold usage example
**/
inline constexpr auto hold() /** @cond */ ->
decltype(subprocessing(hold_processing{})) /** @endcond */
{ return subprocessing(hold_processing{}); }


///@} hold_grp
///@} routing_grp
} // namespace nodes
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_HOLD_HPP
