/**
* @file routing/const_frame.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_CONST_FRAME_HPP
#define DFPTL_ROUTING_CONST_FRAME_HPP


#include "dfptl/routing/detail/const_frame_source.hpp"
#include "dfptl/core/placeholder.hpp"

#include "stl/tuple.hpp"
#include "stl/experimental/array.hpp"
#include <array>
#include <utility>
#include <initializer_list>


namespace dfp
{
namespace routing
{
inline namespace sources
{
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation-html"
#endif
///@addtogroup routing_grp
///@{
/**
* @defgroup const_frame_grp "const-frame" source
* @todo to be rename into frame_from_value
* @brief Produces a stream of immutable frame.
*
* A @ref const_frame_grp feeds a processing branch with an immutable samples frame over the time.
* Given input frame can be a STL-container or a C-array.
*
* Frame samples can be arranged in different channels, If so they shall be stored interleaved in the given frame,
* e.g. for a 3-channels frame the samples mapping shall be :
* [s00, s10, s20, s01, s11, s21, s02, s12, s22, s03, ..., sXY] where @c X is the channel index and @c Y the sample rank in the channel.
*
* **Source interface**
* @startuml
* rectangle "o(n) = frame[n % frame.size()]" <<const_frame(frame)>> as src
* interface "output: [frame[0] frame[1] ... frame[n-1]] [frame[0] ... o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | <ul><li>a @ref stream_grp with static frame extent conveying the given immutable frame, if the given container is a std::array or C-array</li><li>Otherwise the frame extent of the produced @ref stream_grp is dynamic</li></ul>
*
* @sa mutable_frame_grp, const_sample_grp
* @todo: [std::span](https://en.cppreference.com/w/cpp/container/span) as input container should make the source producing a static frame stream
* if @c Extent is not @c std::dynamic_extent
* @{
**/
#ifdef __clang__
#pragma clang diagnostic pop
#endif


/**
* @brief Creates a @ref const_frame_grp from sequence container with a static extent of channel count whose value is given as a function parameter.
*
* @param container is a [sequence container](https://en.cppreference.com/w/cpp/named_req/SequenceContainer)
* that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed as compile-time integral constant convertible to @ref dfp::core::size_t_const "size_t_const".
* @note Frame length shall be divisible by the @c channel_count value or compile-time error will be raised.
*
* @tparam Container type is implicitly deduced from @c container parameter.
* @tparam ChannelCount type is implicitly deduced from @c channel_count parameter.
*
* **Usage example**
* Producing an immutable mono-channel stream from a @c std::vector
* @snippet routing_test.cpp mono const_frame from vector
* Producing an immutable 2-channels stream from a @c std::array
* @snippet routing_test.cpp 2-channels const_frame from array
**/
template<
    class Container, class Integral = std::size_t, Integral ChannelCount = 1 /** @cond */, DFP_F_REQUIRES((
        !std::is_array<std::remove_cvref_t<Container>>{}
    )) /** @endcond */>
inline constexpr auto
const_frame(Container&& container, integral_const<Integral, ChannelCount> channel_count = integral_const<Integral, ChannelCount>{}) ->
decltype(   make_placeholder<std::tuple<Container, size_t_const<(std::size_t) ChannelCount>>, detail::const_frame_source>(container))
{
    static_assert(channel_count > 0, "channel_count shall be a strictly positive number");
    static_assert(supports_const_iterator_check<Container>{}, "A const iterator shall be available for Container type");
    return  make_placeholder<std::tuple<Container, size_t_const<(std::size_t) ChannelCount>>, detail::const_frame_source>(container);
}


/**
* @brief Creates a @ref const_frame_grp from sequence container with a dynamic extent of channel count.
*
* @param container is a [sequence container](https://en.cppreference.com/w/cpp/named_req/SequenceContainer)
* that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed at runtime.
*
* @tparam Container type is implicitly deduced from @c container parameter.
*
* **Usage example**
* Producing an immutable 2-channels stream whose channel size has been specified at runtime
* @snippet routing_test.cpp 2-channels const_frame specified at runtime
**/
template<class Container, DFP_F_REQUIRES((!std::is_array<std::remove_cvref_t<Container>>{}))>
inline constexpr auto
const_frame(Container&& container, std::size_t channel_count) ->
decltype(   make_placeholder<std::tuple<Container, size_t_const<DYNAMIC_EXTENT>>, detail::const_frame_source>(std::forward<Container>(container), channel_count))
{
    static_assert(supports_const_iterator_check<Container>{}, "A const iterator shall be available for Container type");
    return  make_placeholder<std::tuple<Container, size_t_const<DYNAMIC_EXTENT>>, detail::const_frame_source>(std::forward<Container>(container), channel_count);
}


/**
* @brief Creates a @ref const_frame_grp from C-array with a static extent of channel count whose value is given as a function parameter.
*
* @param c_array is a C-array passed as lvalue const reference that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed as compile-time integral constant convertible to @ref dfp::core::size_t_const "size_t_const".
* @note Frame length shall be divisible by the @c channel_count value or compile-time error will be raised.
*
* @tparam T is implicitly deduced from @c c_array parameter
* @tparam Size is implicitly deduced from @c c_array parameter
* @tparam ChannelCount type is implicitly deduced from @c channel_count parameter.
**/
template<class T, std::size_t Size, class Integral = std::size_t, Integral ChannelCount = 1>
inline constexpr auto const_frame(
    T const(&c_array)[Size],
    integral_const<Integral, ChannelCount> channel_count  = integral_const<Integral, ChannelCount>{}
) ->
decltype(   make_placeholder<std::tuple<std::array<T, Size>, size_t_const<(std::size_t) ChannelCount>>, detail::const_frame_source>(std::experimental::to_array(c_array)))
{
    static_assert(channel_count > 0, "channel_count shall be a strictly positive number");
    return  make_placeholder<std::tuple<std::array<T, Size>, size_t_const<(std::size_t) ChannelCount>>, detail::const_frame_source>(std::experimental::to_array(c_array));
}


/**
* @brief Creates a @ref const_frame_grp from C-array with a dynamic extent of channel count.
*
* @param c_array is a C-array passed as lvalue const reference that contains the samples of the frame to inject in the processing branch.
* @param channel_count is the count of channel expressed at runtime.
*
* @tparam T is implicitly deduced from @c c_array parameter
* @tparam Size is implicitly deduced from @c c_array parameter
**/
template<class T, std::size_t Size>
inline constexpr auto const_frame(T const(&c_array)[Size], std::size_t channel_count) ->
decltype(make_placeholder<std::tuple<std::array<T, Size>, size_t_const<DYNAMIC_EXTENT>>, detail::const_frame_source>(std::experimental::to_array(c_array), channel_count))
{ return make_placeholder<std::tuple<std::array<T, Size>, size_t_const<DYNAMIC_EXTENT>>, detail::const_frame_source>(std::experimental::to_array(c_array), channel_count); }


template<class... Args>
inline constexpr decltype(const_frame(std::array<std::nullptr_t, 0>{}))
const_frame(Args...) noexcept
{
    static_assert(
        always_false<std::tuple<Args...>>{},
        "const_frame source shall be initialized with a STL sequence container or a C-array (value or reference)"
    );

    return const_frame(std::array<std::nullptr_t, 0>{});
}


///@} const_frame_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
//TODO: create alias ffrmval
using routing::sources::const_frame;
} // namespace sources
} // namespace shortname

///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_CONST_FRAME_HPP
