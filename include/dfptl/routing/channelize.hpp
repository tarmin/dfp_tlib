/**
* @file routing/channelize.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_CHANNELIZE_HPP
#define DFPTL_ROUTING_CHANNELIZE_HPP


#include "dfptl/routing/detail/channelize_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup channelize_grp "channelize" node
* @brief Aggregate multiple input streams into a multi-channels output stream.
*
* A @ref channelize_grp is a multiple-inputs (1 to many) frame-based node which aggregates the various channels of its input streams
* and produces a multi-channels @ref stream_grp on its output.
*
* **Node interface**
* @startuml
* rectangle "[o0(n) o1(n) ...oj(n) ...oj(n+N)]\nwhere: \n° j = x+y+... is the channel count\n° j*(N+1) is the frame length" <<routing::channelize()>> as routing
* interface "[a0(0) a1(0) ...ax(0) ...ax(N)] [a0(1) ..." <<IN#0>> as in0
* interface "[b0(0) ...by(0) ...by(N)] [b0(1) ..." <<IN#1>> as in1
* interface "..." <<IN#p>> as inp
* interface "[a0(0) ...ax(0) b0(0) ...by(0) ...a0(N) ...ax(N) b0(N) ... by(N)] ..." <<channelized stream>> as out
* routing <-up- in0
* routing <-up- in1
* routing <-up- inp
* out <-up- routing
* @enduml
*
* stream name          | location    | description
* ---------------------|-------------|------------
* IN#0                 | **input-0** | 1st input @ref stream_grp
* IN#1                 | **input-1** | optional 2nd input @ref stream_grp
* ...                  | ...         | ...
* IN #p                 | **input-p** | optional p-th input @ref stream_grp
* "channelized stream" | **output**  | @ref stream_grp resulting from interleaving of all input channel samples.
*
* @note if only one single input stream feeds this @ref channelize_grp, it is simply forwarded "as-it-is" on the output.
* @{
**/


/**
* @brief Creates a cell builder for a @ref channelize_grp to interleave samples of the input stream(s) 
*
* **Usage example**
* @snippet routing_test.cpp channelize usage example
**/
inline constexpr auto channelize() /** @cond */ ->
decltype(make_placeholder<detail::channelize_node>()) /** @endcond */
{ return make_placeholder<detail::channelize_node>(); }


///@} channelize_grp
///@} routing_grp
} // namespace nodes
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_CHANNELIZE_HPP
