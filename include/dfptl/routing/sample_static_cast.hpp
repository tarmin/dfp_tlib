/**
* @file routing/sample_static_cast.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_SAMPLE_STATIC_CAST_HPP
#define DFPTL_ROUTING_SAMPLE_STATIC_CAST_HPP


#include "dfptl/core/apply.hpp"
#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/sample_static_cast_node.hpp"


namespace dfp
{
namespace routing
{
namespace functions
{

template<class NewType>
struct sample_static_cast
{
    template<class InputType>
    DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
    NewType operator()(InputType const& input) const { return static_cast<NewType>(input); }
};


} // namespace functions


inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup sample_static_cast_grp "sample static cast" node
* @brief Converts stream samples with the @c static_cast
*
* A @ref sample_static_cast_grp converts stream samples with help of the @c static_cast operator of its @ref atomic_stream "atomic input stream"
* and produces the resulting stream on its output.
*
* **Node interface**
* @startuml
* rectangle "o(n) = static_cast<X>(i(n))" <<sample_static_cast<X>()>> as conv
* interface "input: 1 2 ...i(n)]" <<in>> as in0
* interface "output: static_cast<X>(1) static_cast<X>(2) ...o(n)]" <<conversion result>> as out
* conv <- in
* out <- conv
* @enduml
* * <tt>"in" **input**                  </tt> : @ref atomic_stream "atomic stream" to convert
* * <tt>"conversion result" **output**  </tt> : @ref atomic_stream "atomic stream" resulting from static_cast conversion
* @{
**/


/**
* @brief Creates a placeholder for a @ref sample_static_cast_grp to convert stream samples to the given @c New type.
*
* **Usage example**
* @snippet routing_test.cpp sample_static_cast usage example
*
* @tparam New is the destination type of the sample type conversion.
*
* @retval "can be constexpr" with @c clang
* @retval "is never constexpr" with @c gcc
**/
template<class New, DFP_F_REQUIRES((!is_custom_tag<New>{}))>
DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
inline auto sample_static_cast() /** @cond */ ->
decltype(apply<functions::sample_static_cast<New>>()) /** @endcond */
{ return apply<functions::sample_static_cast<New>>(); }


/**
* @brief Creates a placeholder for a @ref sample_static_cast_grp to convert to stream samples to sample type associated to the @c T @ref core::custom_tag.
*
* **Usage example**
* @snippet routing_test.cpp tagger usage example
*
* @tparam T is a tag identifying the stream on which to retrieve the sample type to apply for the conversion.
*
* @retval "can be constexpr" with @c clang
* @retval "is never constexpr" with @c gcc
**/
template<class T, DFP_F_REQUIRES((is_custom_tag<T>{}))>
DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
inline auto sample_static_cast(T = T{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<T>, detail::sample_static_cast_node>()) /** @endcond */
{ return make_placeholder<std::tuple<T>, detail::sample_static_cast_node>(); }


///@} sample_static_cast_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup sample_static_cast_grp
///@{
/**
* @brief shortname for @ref routing::nodes::sample_static_cast "sample_static_cast<T>()"
**/
template<class T>
DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
inline auto sscast(T = T{}) /** @cond */ ->
decltype(sample_static_cast<T>()) /** @endcond */
{ return sample_static_cast<T>(); }


///@} sample_static_cast_grp
} // namespace nodes
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_SAMPLE_STATIC_CAST_HPP
