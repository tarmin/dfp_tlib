/**
* @file routing/detail/sample_static_cast_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_SAMPLE_STATIC_CAST_NODE_HPP
#define DFPTL_ROUTING_SAMPLE_STATIC_CAST_NODE_HPP


#include "dfptl/core/node.hpp"


#define DFP_ROUTING_SAMPLE_STATIC_CAST routing::sample_static_cast


namespace dfp
{
namespace routing
{
namespace detail
{
///@addtogroup routing_detail
///@{


template<class Context, class Tag, class = void>
class sample_static_cast_node
{
    static_assert(is_custom_tag<Tag>{}, "Given Tag template parameter shall be a custom tag");
    static_assert(Context::UPSTREAM_COUNT == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_SAMPLE_STATIC_CAST, 1));
}; // class sample_static_cast_node


template<class Context, class Tag>
class sample_static_cast_node<Context, Tag, DFP_C_REQUIRES((is_custom_tag<Tag>{} && (Context::UPSTREAM_COUNT == 1)))>
    : public node<
                Context,
                typename output_stream_specifier_t<context_upstream_t<0, Context>>::template set_sample_clue_t<
                    sample_t<output_stream_specifier_t<typename Context::template at_tag_t<Tag>>>
                >,
                sample_static_cast_node<Context, Tag>
      >
{
    // alias for the base type
    typedef  node<
                Context,
                typename output_stream_specifier_t<context_upstream_t<0, Context>>::template set_sample_clue_t<
                    sample_t<output_stream_specifier_t<typename Context::template at_tag_t<Tag>>>
                >,
                sample_static_cast_node<Context, Tag>
    > node_type;
    typedef dfp::detail::constify_t<node_type> const_node;

    typedef output_sample_t<node_type> output_sample_type;

public:
    using node_type::node_type;

    template<class T = node_type, DFP_F_REQUIRES((input_stream_t<T>::IS_ATOMIC))>
    DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,)
    output_sample_t<node_type> process_stream(typename input_stream_t<node_type>::to_const::type&& input_stream) const
    { return static_cast<output_sample_type>(*input_stream.cdrain().cbegin()); }

    template<class T = node_type, DFP_F_REQUIRES((!input_stream_t<T>::IS_ATOMIC))>
    DFP_IF(DFP_CPP14_SUPPORT)(DFP_IF_NOT(DFP_BAD_CONSTEXPR_SUPPORT_WITH_STATIC_CAST)(constexpr,),)
    output_frame_t<node_type> process_stream(typename input_stream_t<node_type>::to_const::type&& input_stream) const
    {
        // Note that for now, output_frame cannot be declared as class member to allow constexpr support
        // when moving constexpr process_stream in dedicated partial specialization class, this output_frame should rather be allocated as class member
        output_frame_t<node_type> output_frame;

        for(auto& output_sample : output_frame)
        {
            auto const input_frame = input_stream.cdrain();
            auto input_iterator = input_frame.cbegin();
            std::size_t input_length = input_frame.size();

            for(std::size_t i = 0 ; i < input_length ; i++)
                output_sample = static_cast<output_sample_type>(*input_iterator++);
        }

        return output_frame;
    }
}; // class sample_static_cast_node


///@}
} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_SAMPLE_STATIC_CAST_NODE_HPP
