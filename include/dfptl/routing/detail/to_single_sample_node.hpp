/**
* @file routing/detail/to_single_sample_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_TO_SINGLE_SAMPLE_NODE_HPP
#define DFPTL_ROUTING_TO_SINGLE_SAMPLE_NODE_HPP


#include "dfptl/core/node.hpp"

#include <stl/utility.hpp>
#include <algorithm>


#define DFP_ROUTING_TO_SINGLE_SAMPLE routing::to_single_sample


namespace dfp
{
namespace routing
{
namespace detail
{


template<class Context, class Derived, class = void>
class to_single_sample_node_base
{
    static_assert(Context::UPSTREAM_COUNT == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_TO_SINGLE_SAMPLE, 1));
    static_assert(always_false<to_single_sample_node_base>{}, DFP_UNEXPECTED_ERROR);
}; // class to_single_sample_node_base


// the weird pattern below with various specializations of to_single_sample_node depending on is_copy_constructible<Context::upstreams_tuple> is only required
// because of c++11 for which copy constructor is undefined (not deleted) if parent copy constructor is deleted
template<class Context, class = void>
struct to_single_sample_node
 :  to_single_sample_node_base<Context, to_single_sample_node<Context>>
{
    using to_single_sample_node_base<Context, to_single_sample_node<Context>>::to_single_sample_node_base;
}; // struct to_single_sample_node


template<class Context>
struct to_single_sample_node<Context, DFP_C_REQUIRES((!std::is_copy_constructible<typename Context::upstreams_tuple>{}))>
 :  to_single_sample_node_base<Context, to_single_sample_node<Context>>
{
    using to_single_sample_node_base<Context, to_single_sample_node<Context>>::to_single_sample_node_base;
    to_single_sample_node(to_single_sample_node const&) = delete;
    to_single_sample_node(to_single_sample_node&&) = default;
}; // struct to_single_sample_node<!is_copy_constructible<upstreams_tuple>>


template<class Context, class Derived>
class to_single_sample_node_base<Context, Derived, DFP_C_REQUIRES((
    (Context::UPSTREAM_COUNT == 1) &&
    (output_stream_specifier_t<context_upstream_t<0, Context>>::FRAME_EXTENT != 1)
))>
 :  public  node<Context, atomic_stream_specifier<as_input0_tag>, Derived>
{
    // alias for the base type
    typedef node<Context, atomic_stream_specifier<as_input0_tag>, Derived> node_type;

public:
    to_single_sample_node_base(Context&& context)
     :  node_type(std::forward<Context>(context)), current_(nullptr), end_(current_)
    {}

    to_single_sample_node_base(to_single_sample_node_base const& o)
     :  node_type(o),
        input_frame_(o.input_frame_),
        // note that if (o.current_ != o.end_) then it means that process_stream has been invoked once and input_frame_ has been set
        current_((o.current_ != o.end_) ? get_input_frame().cend() - (o.end_ - o.current_) : o.current_),
        end_((o.current_ != o.end_) ? get_input_frame().cend() : current_)
    {}

    to_single_sample_node_base(to_single_sample_node_base&& o)
     :  node_type(std::move(o)),
        input_frame_(std::move(o.input_frame_)),
        // note that if (o.current_ != o.end_) then it means that process_stream has been invoked once and input_frame_ has been set
        current_((o.current_ != o.end_) ? get_input_frame().cend() - (o.end_ - o.current_) : o.current_),
        end_((o.current_ != o.end_) ? get_input_frame().cend() : current_)
    {}

    output_sample_t<node_type> DFP_MUTABLE_CONSTEXPR process_stream(input_stream_t<node_type>&& input_stream)
    {
        if (current_ == end_) 
        {
            set_input_frame(input_stream.cdrain());
            current_ = get_input_frame().cbegin();
            end_ = get_input_frame().cend();
        }

        return *current_++;
    }

private:
    template<class T = node_type, DFP_F_REQUIRES((std::is_lvalue_reference<decltype(std::declval<input_stream_t<T>>().cdrain())>{}))>
    void set_input_frame(input_frame_t<T> const& frame)
    { input_frame_ = &frame; }

    template<class T = node_type, DFP_F_REQUIRES((std::is_lvalue_reference<decltype(std::declval<input_stream_t<T>>().cdrain())>{}))>
    input_frame_t<T> const& get_input_frame()
    { return *input_frame_; }

    template<class T = node_type, DFP_F_REQUIRES((!std::is_lvalue_reference<decltype(std::declval<input_stream_t<T>>().cdrain())>{}))>
    void set_input_frame(input_frame_t<T>&& frame)
    { input_frame_ = std::move(frame); }

    template<class T = node_type, DFP_F_REQUIRES((!std::is_lvalue_reference<decltype(std::declval<input_stream_t<T>>().cdrain())>{}))>
    input_frame_t<T> const& get_input_frame()
    { return input_frame_; }

    std::conditional_t<
        std::is_lvalue_reference<decltype(std::declval<input_stream_t<node_type>>().cdrain())>{},
        input_frame_t<node_type> const*, input_frame_t<node_type>
    > input_frame_;
    const_iterator_t<input_frame_t<node_type>> current_;
    const_iterator_t<input_frame_t<node_type>> end_;
}; // class to_single_sample_node_base<...FRAME_EXTENT != 1...>



template<class Context, class Derived>
class to_single_sample_node_base<Context, Derived, DFP_C_REQUIRES((
    (Context::UPSTREAM_COUNT == 1) &&
    (output_stream_specifier_t<context_upstream_t<0, Context>>::FRAME_EXTENT == 1)
))>
 :  public  node<Context, atomic_stream_specifier<as_input0_tag>, Derived>
{
    DFP_STATIC_WARNING(always_true<Derived>{}, "input stream is already a sample-based stream so this " DFP_STRINGIFY(DFP_ROUTING_TO_SINGLE_SAMPLE) " is useless at this graph location");
    // alias for the base type
    typedef node<Context, atomic_stream_specifier<as_input0_tag>, Derived> node_type;

public:
    using node_type::node_type;

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) size_t_const<0> forward_sample(input_sample_t<node_type> const& input_sample) const
    { return 0_c; }

}; // class to_single_sample_node_base<...FRAME_EXTENT == 1...>


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_TO_SINGLE_SAMPLE_NODE_HPP
