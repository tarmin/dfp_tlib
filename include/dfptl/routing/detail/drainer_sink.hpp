/**
* @file routing/detail/drainer_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_DRAINER_SINK_HPP
#define DFPTL_ROUTING_DETAIL_DRAINER_SINK_HPP


#include "dfptl/core/sink.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace routing
{
namespace detail
{


#define DFP_ROUTING_DRAINER routing::drainer


template<class Input, class = void>
class drainer_sink
{ static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_DRAINER, 1)); };


template<class Input>
class drainer_sink<Input, DFP_C_REQUIRES((std::tuple_size<Input>{} == 1))>
 :  public  sink<Input, run_while, drainer_sink<Input>>
{
    typedef sink<Input, run_while, drainer_sink> sink_type;

public:
    using sink_type::run;

    drainer_sink(Input&& input, std::function<bool()> continue_condition) noexcept
     :  sink_type(std::forward<Input>(input), continue_condition)
    {}

    void release_sample(input_sample_t<sink_type> const&) const
    {/*nothing to do*/}
}; // class drainer_sink


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_DRAINER_SINK_HPP
