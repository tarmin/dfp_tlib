/**
* @file routing/detail/delay_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_DELAY_NODE_HPP
#define DFPTL_ROUTING_DETAIL_DELAY_NODE_HPP


#include "dfptl/core/node.hpp"

#include <boost/circular_buffer.hpp>


namespace dfp
{
namespace routing
{
namespace detail
{


#define DFP_ROUTING_DELAY routing::delay


/**
* @brief Frame-based or Sample-based node which delays a stream by a given amount of sample count.
*
* @tparam Input is the type of the processing cell (source or node) bound to the input port to this node.
* @tparam DelayLength specifies the type for specifying the length of the delay. It shall be std::size_t or size_t_const template instantiation.
**/
template<class Input, class DelayLength, class InitialValue, class Enable = void>
class delay_node
{
    static_assert(std::disjunction<std::is_same<DelayLength, std::size_t>, is_size_t_const<DelayLength>>{}, DFP_UNEXPECTED_ERROR);
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_DELAY, 1));
    static_assert(output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_ATOMIC, DFP_INPUT_NOT_ATOMIC_STREAM(DFP_ROUTING_DELAY, 0));
};


template<class Input, class InitialValue>
class delay_node<Input, std::size_t, InitialValue, DFP_C_REQUIRES((
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_ATOMIC
))> :
    public  node<Input, as_input0_tag, delay_node<Input, std::size_t, InitialValue>>
{
    // alias for the base type
    typedef node<Input, as_input0_tag, delay_node> node_type;

public:
    /**
    * @brief Creates a new instance of delay_node.
    *
    * @param input is the processing cell bound to the input port of this node.
    **/
    delay_node(Input&& input, std::size_t delay_length, input_sample_t<node_type> const& initial_value)
        :   node_type(std::forward<Input>(input)), delayed_(delay_length, initial_value)
    { if (delay_length < 1) throw std::invalid_argument("delay length cannot be null"); }

    /**
    * @brief Copy the input sample to the destination reference given at construction time.
    *
    * @param input is the sample on which to apply the operation.
    **/
    output_sample_t<node_type> process_sample(input_sample_t<node_type> const& input)
    {
        output_sample_t<node_type> output = delayed_.front();
        delayed_.push_back(input);

        return output;
    }

private:
    boost::circular_buffer<input_sample_t<node_type>> delayed_;
}; // class delay_node<...IS_ATOMIC, delay specified at runtime ...>


template<class Input, class InitialValue>
class delay_node<Input, size_t_const<1>, InitialValue, DFP_C_REQUIRES((
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_ATOMIC
))>
 :  public  node<Input, as_input0_tag, delay_node<Input, size_t_const<1>, InitialValue>>
{
    // alias for the base type
    typedef node<Input, as_input0_tag, delay_node> node_type;

public:
    /**
    * @brief Creates a new instance of delay_node.
    *
    * @param input is the processing cell bound to the input port of this node.
    **/
    constexpr delay_node(Input&& input, input_sample_t<node_type> const& initial_value)
        :   node_type(std::forward<Input>(input)), delayed_(initial_value) {}

    /**
    * @brief Copy the input sample to the destination reference given at construction time.
    *
    * @param input is the sample on which to apply the operation.
    **/
    DFP_MUTABLE_CONSTEXPR output_sample_t<node_type> process_sample(input_sample_t<node_type> const& input)
    {
        output_sample_t<node_type> output = delayed_;
        delayed_ = input;

        return output;
    }

private:
    input_sample_t<node_type> delayed_;
}; // class delay_node<IS_ATOMIC, size_t_const<1>>


template<class Input, std::size_t DelayLength, class InitialValue>
class delay_node<Input, size_t_const<DelayLength>, InitialValue, DFP_C_REQUIRES((
    output_stream_t<typename std::tuple_element<0, Input>::type>::IS_ATOMIC && (DelayLength > 1)
))>
 :  public  node<Input, as_input0_tag, delay_node<Input, size_t_const<DelayLength>, InitialValue>>
{
    static_assert(DelayLength != 0, "DelayLength template parameter cannot be null for node " DFP_STRINGIFY(DFP_ROUTING_DELAY));

    // alias for the base type
    typedef node<Input, as_input0_tag, delay_node> node_type;

public:
    /**
    * @brief Creates a new instance of delay_node.
    *
    * @param input is the processing cell bound to the input port of this node.
    **/
    delay_node(Input&& input, InitialValue const& initial_value)
     :  node_type(std::forward<Input>(input)), delayed_{}, front_(), back_()
    { delayed_.fill(initial_value); }

    /**
    * @brief Copy the input sample to the destination reference given at construction time.
    *
    * @param input is the sample on which to apply the operation.
    **/
    output_sample_t<node_type> process_sample(input_sample_t<node_type> const& input)
    {
        output_sample_t<node_type> output = delayed_[front_++];
        delayed_[back_++] = input;

        if (front_ >= DelayLength)
            front_ = 0;

        if (back_ >= DelayLength)
            back_ = 0;

        return output;
    }

private:
    std::array<input_sample_t<node_type>, DelayLength> delayed_;
    size_t front_;
    size_t back_;
}; // class delay_node<size_t_const<DelayLength>>


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_DELAY_NODE_HPP
