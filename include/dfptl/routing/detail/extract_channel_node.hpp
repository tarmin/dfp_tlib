/**
* @file routing/detail/extract_channel_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_EXTRACT_CHANNEL_NODE_HPP
#define DFPTL_ROUTING_DETAIL_EXTRACT_CHANNEL_NODE_HPP


#include "dfptl/core/node.hpp"


namespace dfp
{
namespace routing
{
namespace detail
{


#define DFP_ROUTING_EXTRACT_CHANNEL routing::extract_channel


template<class Input, class = void>
class extract_channel_node
{ static_assert(always_false<extract_channel_node>{}, DFP_UNEXPECTED_ERROR); };


struct extract_channel_helper
{
    template<class Node>
    static void check_channel_index_inrange(Node const& node, std::size_t channel_index)
    {
        const std::size_t channel_count = node.input_stream_specifier().get_channel_count();

        if (channel_index >= channel_count)
        {
            std::stringstream message;
            
            message << "channel index value (" << channel_index <<") i;s out of range [0.." << channel_count-1 << "]";

            throw std::invalid_argument(message.str());
        }
    }

    template<class Node>
    static output_frame_t<Node> deinterleave(Node const& node, input_frame_t<Node> const& input)
    {
        output_frame_t<Node> output = node.output_stream_specifier().get_frame_builder().build();
        const std::size_t channel_count = node.input_stream_specifier().get_channel_count();
        auto it = input.cbegin() + node.channel_index_;

        for (auto& ot : output)
        { ot = *it; it += channel_count; }

        return output;
    }
}; // struct extract_channel_helper


template<class Input>
class extract_channel_node<Input, DFP_C_REQUIRES((
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_FRAME_LENGTH_STATIC
    &&
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_CHANNEL_COUNT_STATIC
))>
 :  public  node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_sketch_clue_t<void> // enforce selection of default "frame sketch clue"
            // below line ensure that frame is writable 
            ::template set_sample_clue_t<std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>>
            ::template set_frame_extent_t<
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::FRAME_EXTENT / output_stream_specifier_t<std::tuple_element_t<0, Input>>::CHANNEL_EXTENT
            >
            ::template set_channel_extent_t<1>,
        extract_channel_node<Input>
    >
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_EXTRACT_CHANNEL, 1));

    typedef node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_sketch_clue_t<void>
            ::template set_sample_clue_t<std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>>
            ::template set_frame_extent_t<
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::FRAME_EXTENT / output_stream_specifier_t<std::tuple_element_t<0, Input>>::CHANNEL_EXTENT
            >
            ::template set_channel_extent_t<1>,
        extract_channel_node> node_type;

    friend struct extract_channel_helper;

public:
    //TODO: create ctor variant with channel_index as integral const for compile time check 
    extract_channel_node(Input&& input, std::size_t channel_index)
     :  node_type(std::forward<Input>(input)), channel_index_(channel_index)
    { extract_channel_helper::check_channel_index_inrange(*this, channel_index); }

    output_frame_t<node_type> process_frame(input_frame_t<node_type> const& input) const
    { return extract_channel_helper::deinterleave(*this, input); }

private:
    std::size_t const channel_index_;
}; // class extract_channel_node<... input::IS_FRAME_LENGTH_STATIC && input::IS_CHANNEL_COUNT_STATIC ...>


template<class Input>
class extract_channel_node<Input, DFP_C_REQUIRES((
    !output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_FRAME_LENGTH_STATIC
    ||
    !output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_CHANNEL_COUNT_STATIC
))>
 :  public  node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_sketch_clue_t<void> // enforce selection of default "frame sketch clue"
            // below line ensure that frame is writable 
            ::template set_sample_clue_t<std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>>
            ::template set_frame_extent_t<DYNAMIC_EXTENT>
            ::template set_channel_extent_t<1>,
        extract_channel_node<Input>
    >
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_EXTRACT_CHANNEL, 1));

    typedef node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_sketch_clue_t<void> // enforce selection of default "frame sketch clue"
            // below line ensure that frame is writable 
            ::template set_sample_clue_t<std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>>
            ::template set_frame_extent_t<DYNAMIC_EXTENT>
            ::template set_channel_extent_t<1>,
        extract_channel_node> node_type;

    friend struct extract_channel_helper;

public:
    extract_channel_node(Input&& input, std::size_t channel_index)
     :  node_type(
            std::forward<Input>(input),
            output_stream_specifier_t<node_type>{}
                .set_frame_length(
                    (node_type::output_stream_specifier_of(input).get_channel_count() == DYNAMIC_EXTENT)
                    ||
                    (node_type::output_stream_specifier_of(input).get_frame_length() == DYNAMIC_EXTENT)
                    ? DYNAMIC_EXTENT : node_type::output_stream_specifier_of(input).get_frame_length() / node_type::output_stream_specifier_of(input).get_channel_count()
                )
        ),
        channel_index_(channel_index)
    { extract_channel_helper::check_channel_index_inrange(*this, channel_index); }

    void initialize()
    { // handle possibly late definition of the input stream specifier
        if (this->output_stream_specifier().get_frame_length() != DYNAMIC_EXTENT)
            return;

        extract_channel_helper::check_channel_index_inrange(*this, channel_index_);

        this->set_output_stream_specifier(this->output_stream_specifier()
            .set_frame_length(this->input_stream_specifier().get_frame_length() / this->input_stream_specifier().get_channel_count())
        );
    }

    output_frame_t<node_type> process_frame(input_frame_t<node_type> const& input) const
    { return extract_channel_helper::deinterleave(*this, input); }

private:
    std::size_t const channel_index_;
}; // class extract_channel_node<... !input::FRAME_EXTENT || !input::CHANNEL_EXTENT ...>


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_EXTRACT_CHANNEL_NODE_HPP
