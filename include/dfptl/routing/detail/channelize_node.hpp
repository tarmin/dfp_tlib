/**
* @file routing/detail/channelize_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_CHANNELIZE_NODE_HPP
#define DFPTL_ROUTING_DETAIL_CHANNELIZE_NODE_HPP


#include "dfptl/core/node.hpp"


namespace dfp
{
namespace routing
{
namespace detail
{


#define DFP_ROUTING_CHANNELIZE routing::channelize


template<class Input>
class channelize_node;


template<std::size_t... InputIndexes>
struct channelize_helper
{
    static constexpr std::size_t INPUT_COUNT = sizeof...(InputIndexes);

    template<class Node>
    static output_frame_t<Node> interleave(Node const& node, input_frame_t<Node, InputIndexes> const&... inputs)
    {
        output_frame_t<Node> output = node.output_stream_specifier().get_frame_builder().build();
        std::size_t const output_channel_count = node.output_stream_specifier().get_channel_count();

        return interleave_<Node, InputIndexes...>(node, output, output_channel_count, 0, inputs...);
    }

private:
    template<class Node, std::size_t FrontInputIndex, std::size_t ...EndInputIndexes>
    static output_frame_t<Node>& interleave_(
        Node const& node, output_frame_t<Node>& output, std::size_t output_channel_count, std::size_t current_output_channel_id,
        input_frame_t<Node, FrontInputIndex> const& front_input, input_frame_t<Node, EndInputIndexes> const&... end_inputs)
    {
        auto const front_channel_count = node.input_stream_specifier(size_t_const<FrontInputIndex>{}).get_channel_count();
        auto const front_end = front_input.cend();

        for (std::size_t chid = 0 ; chid < front_channel_count ; chid++)
        {
            auto out_it = output.begin() + current_output_channel_id++;
            for(auto in_it = front_input.cbegin()+chid ; in_it < front_end ; in_it += front_channel_count)
            {
                *out_it = *in_it;
                out_it += output_channel_count;
            }
        }

        return interleave_<Node, EndInputIndexes...>(node, output, output_channel_count, current_output_channel_id, end_inputs...);
    }

    template<class Node>
    static output_frame_t<Node>& interleave_(Node const& node, output_frame_t<Node>& output, std::size_t output_channel_count, std::size_t current_output_channel_id)
    { (void) node ; (void) output_channel_count ; (void) current_output_channel_id ; return output; }
}; // struct channelize_helper


template<class Input, class InputIndexSequence>
class channelize_node_impl
{ static_assert(always_false<channelize_node_impl>{}, DFP_UNEXPECTED_ERROR); };


template<class Input, std::size_t ...InputIndexes>
class channelize_node_impl<Input, std::index_sequence<InputIndexes...>>
 :  public  node<
        Input,
        static_stream_specifier<
            //TODO: smarter sample type deduction ?
            std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>,
            pack_or(!output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::IS_FRAME_LENGTH_STATIC...) ?
                DYNAMIC_EXTENT
                :
                pack_add(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::FRAME_EXTENT...),
            pack_or(!output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::IS_CHANNEL_COUNT_STATIC...) ?
                DYNAMIC_EXTENT
                :
                pack_add(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::CHANNEL_EXTENT...)
        >,
        channelize_node_impl<Input, std::index_sequence<InputIndexes...>>
    >
{
    typedef node<
        Input,
        static_stream_specifier<
            //TODO: smarter sample deduction ?
            std::remove_cv_t<typename output_stream_specifier_t<std::tuple_element_t<0, Input>>::sample_clue>,
            pack_and(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::IS_FRAME_LENGTH_STATIC...) ?
                pack_add(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::FRAME_EXTENT...)
                :
                DYNAMIC_EXTENT,
            pack_and(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::IS_CHANNEL_COUNT_STATIC...) ?
                pack_add(output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::CHANNEL_EXTENT...)
                :
                DYNAMIC_EXTENT
        >,
        channelize_node_impl> node_type;

    //TODO: verify all inputs have compatible sample type
    static_assert(
        not(
            pack_and(input_stream_specifier_t<node_type, InputIndexes>::IS_FRAME_LENGTH_STATIC...)
            &&
            pack_and(input_stream_specifier_t<node_type, InputIndexes>::IS_CHANNEL_COUNT_STATIC...)
        ) or pack_equal((
            output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::FRAME_EXTENT
            /
            output_stream_specifier_t<std::tuple_element_t<InputIndexes, Input>>::CHANNEL_EXTENT
        )...),
        "all " DFP_STRINGIFY(DFP_ROUTING_CHANNELIZE) " inputs shall have identical channel chunk length (i.e. frame length / channel count)"
    );

    friend struct channelize_helper<InputIndexes...>;

public:
    //TODO: create ctor variant with channel_index as integral const for compile time check 
    channelize_node_impl(Input&& input)
     :  node_type(std::forward<Input>(input),
            output_stream_specifier_t<node_type>{}
            .set_frame_length(static_if(
                pack_and((this->output_stream_specifier_of(input, size_t_const<InputIndexes>{}).is_frame_length_specified())...),
                pack_add(node_type::template output_stream_specifier_of<InputIndexes>(input).get_frame_length()...),
                DYNAMIC_EXTENT
            ))
            .set_channel_count(static_if(
                pack_and((this->output_stream_specifier_of(input, size_t_const<InputIndexes>{}).is_channel_count_specified())...),
                pack_add(node_type::template output_stream_specifier_of<InputIndexes>(input).get_channel_count()...),
                DYNAMIC_EXTENT
            ))
        )
    {}

    void initialize()
    { // handle possibly late definition of the input stream specifiers
        auto output_specifier = this->output_stream_specifier();

        if (!this->output_stream_specifier().is_frame_length_specified())
            output_specifier = output_specifier
                .set_frame_length(pack_add(this->input_stream_specifier(size_t_const<InputIndexes>{}).get_frame_length()...));

        if (!this->output_stream_specifier().is_channel_count_specified())
            output_specifier = output_specifier
                .set_channel_count(pack_add(this->input_stream_specifier(size_t_const<InputIndexes>{}).get_channel_count()...));

        this->set_output_stream_specifier(output_specifier);
    }

    output_frame_t<node_type> process_frame(input_frame_t<node_type, InputIndexes> const&... inputs) const
    { return channelize_helper<InputIndexes...>::interleave(*this, inputs...); }
}; // class channelize_node_impl


template<class Input>
class channelize_node
 :  public channelize_node_impl<Input, index_sequence_from_t<Input>>
{
public:
    using channelize_node_impl<Input, index_sequence_from_t<Input>>::channelize_node_impl;
}; // class channelize_node


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_CHANNELIZE_NODE_HPP
