/**
* @file routing/detail/convey_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_CONVEY_NODE_HPP
#define DFPTL_ROUTING_DETAIL_CONVEY_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/type_traits.hpp"


#define DFP_ROUTING_CONVEY routing::convey


namespace dfp
{
namespace routing
{
namespace detail
{
template<class Context, class InputIndexSequence, class = void>
class convey_node_impl
{
    static_assert(
        std::tuple_size<typename Context::upstreams_tuple>{} >= 3,
        DFP_STRINGIFY(DFP_ROUTING_CONVEY) " node requires at least 3 inputs (2 alternative streams + 1 selector stream)"
    );

    static_assert(always_false<convey_node_impl>{}, DFP_UNEXPECTED_ERROR);
};


template<class Context, std::size_t... InputIds>
class convey_node_impl<Context, std::index_sequence<InputIds...>, DFP_C_REQUIRES((
        !is_integral_constant<output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>>{}
    &&  (sizeof...(InputIds) >= 3)
))>
 :  public  node<Context, as_input0_tag, convey_node_impl<Context, std::index_sequence<InputIds...>>>
{
    // alias for the base type
    typedef node<Context, as_input0_tag, convey_node_impl> node_type;
    static constexpr std::size_t SELECTOR_INDEX = sizeof...(InputIds)-1;

    static_assert(
        are_same<but_last_t<input_sample_t<node_type, InputIds>...>>{},
        DFP_STRINGIFY(DFP_ROUTING_CONVEY) " node requires all input streams but last to deliver same sample type"
    );

    static_assert(
        std::is_convertible<input_sample_t<node_type, SELECTOR_INDEX>, std::size_t>{},
        DFP_STRINGIFY(DFP_ROUTING_CONVEY) " node requires its last input stream to deliver convertible-to-size_t sample"
    );

    static_assert(pack_and(input_stream_t<node_type, InputIds>::IS_ATOMIC...), DFP_INPUT_NOT_ATOMIC_STREAM(DFP_ROUTING_CONVEY, all));

public:
    using node_type::node_type;

    template<std::size_t S = SELECTOR_INDEX, DFP_F_REQUIRES((S == 2))>
    constexpr output_sample_t<node_type> process_sample(
        input_sample_t<node_type, 0> const& in0,
        input_sample_t<node_type, 1> const& in1,
        input_sample_t<node_type, SELECTOR_INDEX> const& selector
    ) const
    { return selector ? in1 : in0; }

    template<std::size_t S = SELECTOR_INDEX, DFP_F_REQUIRES((S == 3))>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) output_sample_t<node_type> process_sample(
        input_sample_t<node_type, 0> const& in0,
        input_sample_t<node_type, 1> const& in1,
        input_sample_t<node_type, 1> const& in2,
        input_sample_t<node_type, SELECTOR_INDEX> const& selector
    ) const
    {
        switch(selector) {
            case 0: return in0; break;
            case 1: return in1; break;
            default:
            case 2: return in2; break;
        }
    }

    template<std::size_t S = SELECTOR_INDEX, DFP_F_REQUIRES((S > 3))>
    constexpr output_sample_t<node_type> process_sample(
        input_sample_t<node_type, InputIds> const&... inputs
    ) const
    { return process_sample_(inputs...); }

private:
    template<class... TransmittableInputs>
    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,)  output_sample_t<node_type> process_sample_(
        TransmittableInputs const&... transmittable_inputs,
        input_sample_t<node_type, SELECTOR_INDEX> const selector
    ) const
    // suboptimal implementation (as it introduces some array allocation on stack)
    // TODO: consider to create some template specilization for most common case (aka input count = 2, 3, ..., 8) 
    {
        selector = (selector < SELECTOR_INDEX) ? selector : SELECTOR_INDEX-1;
        return std::experimental::make_array(transmittable_inputs...)[selector];
    }
}; // class convey_node_impl <... NOT is_convertible_to_size_t_const< output_sample_t<last input> > ...>


template<class Context, std::size_t... InputIds>
class convey_node_impl<Context, std::index_sequence<InputIds...>, DFP_C_REQUIRES((
        is_integral_constant<output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>>{}
    &&  (output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>{} >= 0)
    &&  (sizeof...(InputIds) >= 3)
))>
 :  public  node<Context, as_input_tag<(size_t) output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>{}>, convey_node_impl<Context, std::index_sequence<InputIds...>>>
{
    // alias for the base type
    typedef node<Context, as_input_tag<(size_t) output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>{}>, convey_node_impl> node_type;
    static constexpr std::size_t SELECTOR_INDEX = sizeof...(InputIds)-1;
    typedef size_t_const<(size_t) output_sample_t<typename context_upstream<SELECTOR_INDEX, Context>::type>{}> selector_const;

public:
    using node_type::node_type;

    template<class... InputSamples>
    selector_const forward_sample(InputSamples const&...)
    {
        static_assert(
            selector_const{} < SELECTOR_INDEX,
            "Value provided by the selector stream of " DFP_STRINGIFY(DFP_ROUTING_CONVEY)  " node shall be less than the index of the selector stream"
        );

        return selector_const{};
    }
}; // class convey_node_impl <... is_convertible_to_size_t_const< output_sample_t<last input> > ...>

template<class Context, std::size_t... InputIds>
class convey_node_impl<Context, std::index_sequence<InputIds...>, DFP_C_REQUIRES((
        is_integral_constant<output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>>{}
    &&  !(output_sample_t<typename context_upstream<sizeof...(InputIds)-1, Context>::type>{} >= 0)
    &&  (sizeof...(InputIds) >= 3)
))>
{
    static constexpr std::size_t SELECTOR_INDEX = sizeof...(InputIds)-1;

        static_assert(
            always_false<convey_node_impl>{},
            "Value provided by the selector stream of " DFP_STRINGIFY(DFP_ROUTING_CONVEY) " node shall be positive"
        );
};


template<class Context>
struct convey_node
 :  public  convey_node_impl<Context, index_sequence_from_t<typename Context::upstreams_tuple>>
{
    typedef convey_node_impl<Context, index_sequence_from_t<typename Context::upstreams_tuple>> base;
    using base::base;
}; // class convey_node


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_CONVEY_NODE_HPP
