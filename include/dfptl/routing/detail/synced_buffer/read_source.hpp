/**
* @file routing/detail/synced_buffer/read_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_READ_SOURCE_HPP
#define DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_READ_SOURCE_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/routing/synced_buffer/share.hpp"


#define DFP_SYNCED_BUFFER_READ routing::synced_buffer::read


namespace dfp
{
namespace routing
{
namespace synced_buffer
{
namespace detail
{


template<class Context, class FrameExtent, class Sample, class BufferMaxLength, class = void>
class read_source
{
    static_assert(
        is_size_t_const<FrameExtent>{},
        "FrameExtent shall be a size_t_const template specialization for "  DFP_STRINGIFY(DFP_SYNCED_BUFFER_READ)
    );

    static_assert(
        is_size_t_const<BufferMaxLength>{},
        "BufferMaxLength shall be a size_t_const template specialization for "  DFP_STRINGIFY(DFP_SYNCED_BUFFER_READ)
    );

    static_assert(
        BufferMaxLength{} != DYNAMIC_EXTENT,
        "BufferLength shall be specified at compile-time for " DFP_STRINGIFY(DFP_SYNCED_BUFFER_READ)
    );
};


template<class Context, class FrameExtent, class Sample, std::size_t BufferMaxLength>
class read_source<Context, FrameExtent, Sample, size_t_const<BufferMaxLength>, DFP_C_REQUIRES((BufferMaxLength != (std::size_t) -1))>
 :  public  source<Context, static_stream_specifier<Sample, FrameExtent{}>, read_source<Context, FrameExtent, Sample, size_t_const<BufferMaxLength>>>
{
    typedef source<Context, static_stream_specifier<Sample, FrameExtent{}>, read_source> source_type;

public:
    read_source(Context const& context, share<Sample, BufferMaxLength>& buffer)
     :  source_type(context), share_(buffer), lock_(share_.mutex, std::defer_lock)
    {}

    read_source(read_source&& src)
     :  source_type(std::move(src)), share_(src.share_), lock_(std::move(src.lock_)), frame_(std::move(src.frame_))
    {}

    //TODO: figure out mechanism to mark this source as copy-constructible before initialization but not after (at tree level)
    read_source(read_source const&) = delete;

    output_frame_t<source_type> const& acquire_frame()
    {
        std::size_t const output_frame_length = this->output_stream_specifier().get_frame_length();
        auto& container = share_.container;
        auto& cv = share_.cv;

        lock_.lock();
        cv.wait(lock_, [output_frame_length, &container]{ return container.size() >= output_frame_length; });

        std::copy(container.cbegin(), container.cbegin() + output_frame_length, frame_.begin());
        container.erase(container.begin(), container.begin() + output_frame_length);

        lock_.unlock();
        cv.notify_one();

        return frame_;
    }

private:
    share<Sample, BufferMaxLength>& share_;
    std::unique_lock<std::mutex> lock_;
    output_frame_t<source_type> frame_;
}; // class read_source


} // namespace detail
} // namespace synced_buffer
} // namespace routing
} // namespace dfp


#endif //DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_READ_SOURCE_HPP
