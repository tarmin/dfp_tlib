/**
* @file routing/detail/synced_buffer/write_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_WRITE_NODE_HPP
#define DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_WRITE_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/exception.hpp"
#include "dfptl/routing/synced_buffer/share.hpp"

#include <iostream>


#define DFP_SYNCED_BUFFER_WRITE routing::synced_buffer::write


namespace dfp
{
namespace routing
{
namespace synced_buffer
{
namespace detail
{


template<class Input, class Sample, class BufferLength>
class write_node
 :  public  node<Input, as_input0_tag, write_node<Input, Sample, BufferLength>>
{
    typedef node<Input, as_input0_tag, write_node> node_type;

    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_SYNCED_BUFFER_WRITE, 1));

public:
    /**
    * @brief Creates a new instance of write_node.
    *
    * @param input is the processing graph bound to input of this node.
    **/
    write_node(Input&& input, share<Sample, BufferLength{}>& buffer)
     :  node_type(std::forward<Input>(input)), share_(buffer)
    {
        if (share_.length < this->input_stream_specifier().get_frame_length())
            throw frame_length_error(DFP_STRINGIFY(DFP_SYNCED_BUFFER_WRITE), 0, "Input frame length shall be less or equal to the shared buffer size");
    }

    size_t_const<0> forward_frame(input_frame_t<node_type> const& input)
    {
        std::size_t const input_frame_length = input.size();
        std::lock_guard<std::mutex> lock(share_.mutex); // need to protect access to container
        auto& container = share_.container;
        std::size_t available_room = share_.length - container.size();

        if (available_room < input_frame_length) //TODO: add some overflow_policy (silently_override, override, block, throw_error)
        {
            std::cerr << DFP_STRINGIFY(DFP_SYNCED_BUFFER_WRITE) << " overflow!!" << std::endl;

            // free missing room
            container.erase(container.begin(), container.begin() + input_frame_length - available_room);
        }

        container.insert(container.end(), input.begin(), input.end());
        share_.cv.notify_one();

        return 0_c;
    }

private:
    share<Sample, BufferLength{}>& share_;
}; // class write_node


} // namespace detail
} // namespace synced_buffer
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_SYNCED_BUFFER_WRITE_NODE_HPP
