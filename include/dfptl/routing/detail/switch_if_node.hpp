/**
* @file routing/detail/switch_if_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_SWITCH_IF_NODE_HPP
#define DFPTL_ROUTING_DETAIL_SWITCH_IF_NODE_HPP


#include "dfptl/core/node.hpp"


#define DFP_ROUTING_SWITCH_IF routing::switch_if


namespace dfp
{
namespace routing
{
namespace detail
{


DFP_INLINE_CONSTEXPR int SWITCH_IF_CONDITION_INPUT_INDEX  = 0;
DFP_INLINE_CONSTEXPR int SWITCH_IF_THEN_INPUT_INDEX       = 1;
DFP_INLINE_CONSTEXPR int SWITCH_IF_ELSE_INPUT_INDEX       = 2;


template<class Inputs, int Index>
struct switch_if_input
{
    static_assert(std::tuple_size<Inputs>{} == 3, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_SWITCH_IF, 3));
    //TODO: check input produce a single sample stream

    typedef std::tuple_element_t<Index, Inputs> type;
};

template<class Inputs, int Index>
using switch_if_input_t = typename switch_if_input<Inputs, Index>::type;

template<class Inputs>
using switch_if_condition_input_t = switch_if_input_t<Inputs, SWITCH_IF_CONDITION_INPUT_INDEX>;

template<class Inputs>
using switch_if_then_input_t      = switch_if_input_t<Inputs, SWITCH_IF_THEN_INPUT_INDEX>;

template<class Inputs>
using switch_if_else_input_t      = switch_if_input_t<Inputs, SWITCH_IF_ELSE_INPUT_INDEX>;


template<class Inputs>
class switch_if_node
 :  public  node<Inputs, output_stream_specifier_t<switch_if_then_input_t<Inputs>>, switch_if_node<Inputs>>
{
    // alias for the base type
    typedef node<Inputs, output_stream_specifier_t<switch_if_then_input_t<Inputs>>, switch_if_node> node_type;

    static_assert(std::is_convertible<input_sample_t<node_type, SWITCH_IF_CONDITION_INPUT_INDEX>, bool>{}, DFP_INVALID_INPUT_SAMPLE(DFP_ROUTING_SWITCH_IF, convertible-to-bool, SWITCH_IF_CONDITION_INPUT_INDEX));
//TODO: assert both THEN and ELSE input have identical stream specifier

public:
    /**
    * @brief Creates a new instance of switch_if_node.
    *
    * @param inputs is the processing cells bound to the input port of this node.
    **/
    constexpr switch_if_node(Inputs&& inputs) : node_type(std::forward<Inputs>(inputs)) {}

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr, ) output_sample_t<node_type> process_stream(
        input_stream_t<node_type, 0>&& condition, input_stream_t<node_type, 1>&& then, input_stream_t<node_type, 2>&& other
    ) /* TODO: should be const method but issue !? */
    {
        // todo: issue as current implementation will only work with sample-based input stream
        if(*condition.drain().begin())
            return *then.drain().begin();
        else
            return static_cast<output_sample_t<node_type>>(*other.drain().begin());
    }
}; // class switch_if_node


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_SWITCH_IF_NODE_HPP
