/**
* @file routing/detail/adjust_frame_length_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_ADJUST_FRAME_LENGTH_NODE_HPP
#define DFPTL_ROUTING_ADJUST_FRAME_LENGTH_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/exception.hpp"

#include <stl/utility.hpp>
#include <algorithm>


namespace dfp
{
namespace routing
{
namespace detail
{
///@addtogroup routing_detail
///@{


#define DFP_ROUTING_ADJUST_FRAME_LENGTH routing::adjust_frame_length


/**
* @sa adjust_frame_length()
**/
template<class Input, class FrameExtent, class = void>
class adjust_frame_length_node
{
    static_assert(is_size_t_const<FrameExtent>{}, "FrameExtent shall be a size_t_const template specialization");

    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_ADJUST_FRAME_LENGTH, 1));

    static_assert(always_false<Input>{}, DFP_UNEXPECTED_ERROR);
}; // class adjust_frame_length_node


//TODO: for optimization purpose create partial specialization for case where FrameLength is equal to frame length of the input
//TODO: for optimization purpose create partial specialization for case where FrameLength shorter than input frame and input frame is given by reference
template<class Input, std::size_t FrameLength>
class adjust_frame_length_node<Input, size_t_const<FrameLength>, DFP_C_REQUIRES((
    (FrameLength != DYNAMIC_EXTENT) && (output_stream_specifier_t<std::tuple_element_t<0, Input>>::FRAME_EXTENT != DYNAMIC_EXTENT)
))>
 :  public  node<Input, static_stream_specifier<as_input0_tag, FrameLength>, adjust_frame_length_node<Input, size_t_const<FrameLength>>>
{
    typedef node<Input, static_stream_specifier<as_input0_tag, FrameLength>, adjust_frame_length_node> node_type;
    typedef dfp::detail::constify_t<node_type> const_node;

    static_assert(
        (FrameLength % input_stream_t<node_type>::FRAME_EXTENT) == 0,
        "FrameLength of the output stream shall be divisible by the frame length of the input stream"
    );

    constexpr static size_t void_value(size_t){ return 0; }; // to avoid "expression result unused" warning

#if (__cplusplus >= 201103L) // if compiler version is at least C++11
        //TODO: must be enabled only if input stream is an atomic_stream
        template<size_t... Is>
        constexpr output_frame_t<const_node> output_frame_initializer(
            typename input_stream_t<node_type>::to_const::type input_stream, std::index_sequence<Is...>
        ) const
        { return {{(void_value(Is), *input_stream.cdrain().cbegin())...}}; }
#endif

public:
    /**
    * @brief Creates a new instance of adjust_frame_length_node.
    *
    * @param input is the processing cell bound to the input port of this node.
    **/
    constexpr adjust_frame_length_node(Input&& input) noexcept : node_type(std::forward<Input>(input)) {}

    /**
    * @brief Converts the @c input_frame into an output frame type with expected @c FrameLength
    *
    * @param input_stream is the input stream conveying the frame to convert.
    **/
    constexpr output_frame_t<const_node>        process_stream(typename input_stream_t<node_type>::to_const::type&& input_stream) const
    {
        /*RVO is expected to apply here*/
        return output_frame_initializer(
            input_stream, std::make_index_sequence<output_frame_t<node_type>{}.size()>()
        );
    }

    DFP_MUTABLE_CONSTEXPR output_frame_t<node_type> process_stream(input_stream_t<node_type>&& input_stream)
    {
        // Note that for now, output_frame cannot be declared as class member to allow constexpr support
        // when moving constexpr process_stream in dedicated partial specialization class, this output_frame should rather be allocated as class member
        output_frame_t<node_type> output_frame = {};
        auto output_iterator = output_frame.begin();
        const std::size_t input_frame_length = node_type::input_stream_specifier().get_frame_length();
        const std::size_t iteration_count =
            node_type::output_stream_specifier().get_frame_length() / input_frame_length;

        for(std::size_t i = 0 ; i < iteration_count ; i++)
        {
            auto const input_frame = input_stream.cdrain();
            auto input_iterator = input_frame.cbegin();

            for(std::size_t j = 0 ; j < input_frame_length ; j++)
                *output_iterator++ = *input_iterator++;
        }

        return output_frame;
    }
}; // class adjust_frame_length_node<... FrameLength != DYNAMIC_EXTENT ...>


template<class Input, std::size_t FrameExtent>
class adjust_frame_length_node<Input, size_t_const<FrameExtent>, DFP_C_REQUIRES((FrameExtent == DYNAMIC_EXTENT))>
 :  public  node<Input, dynamic_stream_specifier<as_input0_tag>, adjust_frame_length_node<Input, size_t_const<FrameExtent>>>
{
    typedef node<Input, dynamic_stream_specifier<as_input0_tag>, adjust_frame_length_node> node_type;

public:
    /**
    * @brief Creates a new instance of adjust_frame_length_node.
    *
    * @param input is the processing cell bound to the input port of this node.
    **/
    adjust_frame_length_node(Input&& input, std::size_t frame_length)
     :  node_type(std::forward<Input>(input), this->output_stream_specifier().set_frame_length(frame_length)), output_frame_(frame_length)
    {
        if((this->output_stream_specifier().get_frame_length() % this->input_stream_specifier().get_frame_length()) != 0)
            throw frame_length_error(DFP_STRINGIFY(DFP_ROUTING_ADJUST_FRAME_LENGTH), 0, "Specified output frame length shall be divisible by the input frame length.");
    }

    output_frame_t<node_type> const& process_stream(input_stream_t<node_type>&& input_stream)
    {
        auto output_iterator = output_frame_.begin();

        while(output_iterator < output_frame_.cend())
        {
            auto const input_frame = input_stream.cdrain();
            auto input_iterator = input_frame.cbegin();

            while(input_iterator < input_frame.cend())
                *output_iterator++ = *input_iterator++;
        }

        return output_frame_;
    }

private:
    output_frame_t<node_type> output_frame_;
}; // class adjust_frame_length_node<... FrameExtent == DYNAMIC_EXTENT ...>


///@}
} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_ADJUST_FRAME_LENGTH_NODE_HPP
