/**
* @file routing/detail/looper_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_DETAIL_LOOPER_SINK_HPP
#define DFPTL_ROUTING_DETAIL_LOOPER_SINK_HPP


#include "dfptl/core/sink.hpp"


namespace dfp
{
namespace routing
{
namespace detail
{


#define DFP_ROUTING_LOOPER routing::looper


template<class Context>
class looper_sink
 :  public  sink<Context, run_for, looper_sink<Context>>
{
    static_assert(Context::UPSTREAM_COUNT == 1, DFP_INVALID_INPUT_COUNT(DFP_ROUTING_LOOPER, 1));

    typedef sink<Context, run_for, looper_sink> sink_type;

public:
    constexpr looper_sink(Context&& context, std::size_t samples_count) noexcept
     :  sink_type(std::forward<Context>(context), samples_count)
    {}

    looper_sink(Context&& context, std::function<std::size_t()> samples_count)
     :  sink_type(std::forward<Context>(context), samples_count())
    {}

    template<class T = sink_type, DFP_F_REQUIRES((input_stream_specifier_t<T>::IS_ATOMIC))>
    void release_sample(input_sample_t<sink_type> const&)
    {/*nothing to do*/}

    template<class T = sink_type, DFP_F_REQUIRES((!input_stream_specifier_t<T>::IS_ATOMIC))>
    void release_frame(input_frame_t<sink_type> const&) const
    {/*nothing to do*/}
}; // class looper_sink


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_LOOPER_SINK_HPP
