/**
* @file routing/detail/const_frame_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**//*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_DETAIL_CONST_FRAME_SOURCE_HPP
#define DFPTL_ROUTING_DETAIL_CONST_FRAME_SOURCE_HPP


#include "dfptl/core/source.hpp"


namespace dfp
{
namespace routing
{
namespace detail
{


template<class Context, class Container, class ChannelExtent>
class const_frame_source
 :  public  source<
        Context, stream_specifier_from_frame<std::remove_cvref_t<Container>, ChannelExtent{}>, const_frame_source<Context, Container, ChannelExtent>
    >
{
    typedef source<
        Context, stream_specifier_from_frame<std::remove_cvref_t<Container>, ChannelExtent{}>, const_frame_source
    > source_type;

public:
    typedef typename source_type::output_specifier_type output_specifier_type;

    template<class T = output_specifier_type, DFP_F_REQUIRES((T::CHANNEL_EXTENT != DYNAMIC_EXTENT))>
    constexpr const_frame_source(Context const& context, Container container)
     :  source_type(context, output_specifier_type{}.set_frame_length(get_length(container))), frame_(std::forward<Container>(container))
    {}

    constexpr const_frame_source(Context const& context, Container container, std::size_t channel_count)
     :  source_type(context, output_specifier_type{}.set_frame_length(get_length(container)).set_channel_count(channel_count)),
        frame_(std::forward<Container>(container))
    {}

    constexpr output_frame_t<source_type> const& acquire_frame() const
    { return frame_; }

    const Container frame_;
};


} // namespace detail
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_DETAIL_CONST_FRAME_SOURCE_HPP
