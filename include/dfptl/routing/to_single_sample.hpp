/**
* @file routing/to_single_sample.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_TO_SINGLE_SAMPLE_HPP
#define DFPTL_ROUTING_TO_SINGLE_SAMPLE_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/to_single_sample_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup to_single_sample_grp "convert to single-sample-stream" node
* @brief Converts its single input stream into a @ref atomic_stream "atomic stream".
*
* Sample-based processing node or sink requires some @ref atomic_stream "atomic stream" on its inputs.
* This node helps to convert any @ref core::stream "stream" into a @ref atomic_stream "atomic stream".
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n)" <<to_single_sample()>> as conv
* interface "input: [1 2][3 4][5 6] ...[i(n-1) i(n)]" <<in>> as in
* interface "output: 1 2 3 4 5 6 ...i(n)" <<conversion result>> as out
* conv <- in
* out <- conv
* @enduml
* * <tt>"in" **input**              </tt> : @ref core::stream "stream" to convert
* * <tt>"conversion result" **output**</tt> : @ref atomic_stream "atomic stream" resulting for conversion
*
* @note Such a node implies some amount of memory consumption as one input frame shall be buffered.
* Its usage in processing branch should be considered carefully.
* @{
**/


/**
* @brief Creates a placeholder for a @ref to_single_sample_grp.
*
* **Usage example**
* @snippet routing_test.cpp to_single_sample usage example
**/
inline auto constexpr to_single_sample() /** @cond */ ->
decltype(make_placeholder<std::tuple<>, detail::to_single_sample_node>()) /** @endcond */
{ return make_placeholder<std::tuple<>, detail::to_single_sample_node>(); }


///@} to_single_sample_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup to_single_sample_grp
///@{
/**
* @brief shortname for @ref routing::nodes::to_single_sample()
**/
inline auto constexpr toss() /** @cond */ ->
decltype(to_single_sample()) /** @endcond */
{ return to_single_sample(); }


///@} to_single_sample_grp
} // namespace nodes
} // namespace shortname


///@} routing_grp
} // namespace routing_grp
} // namespace dfp


#endif // DFPTL_ROUTING_TO_SINGLE_SAMPLE_HPP
