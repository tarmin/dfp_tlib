/**
* @file routing/from_iterator.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_FROM_ITERATOR_HPP
#define DFPTL_ROUTING_FROM_ITERATOR_HPP


#include <dfptl/core/apply.hpp>


namespace dfp
{
namespace routing
{
namespace detail
{


template<class T, class Derived>
class iterate_with_base
{
protected:
    typedef typename std::iterator_traits<T>::value_type value_type;

public:
    iterate_with_base(T const& iterator) : value_(*iterator)
    {}

    value_type operator()()
    {
        auto const current = value_;

        value_ = *++derived().iterator_;
        return current;
    }

private:
    Derived& derived()
    { return *static_cast<Derived* const>(this); }

    value_type value_;
}; //struct iterate_with_base


template<class T>
class iterate_with_value
 :  public  iterate_with_base<T, iterate_with_value<T>>
{
    typedef iterate_with_base<T, iterate_with_value<T>> base;
    friend base;

public:
    iterate_with_value(T const& iterator)
     :  base(iterator), iterator_(iterator)
    {}

private:
    T iterator_;
}; // class iterate_with_value


template<class T>
class iterate_with_ref
 :  public  iterate_with_base<T, iterate_with_ref<T>>
{
    typedef iterate_with_base<T, iterate_with_ref<T>> base;
    friend base;

public:
    iterate_with_ref(std::reference_wrapper<T> const& ref)
     :  base(ref.get()), iterator_(ref.get())
    {}

protected:
    T& iterator_;
}; // class iterate_with_ref


} // namespace detail
inline namespace sources
{
///@addtogroup routing_grp
///@{
///@{
/** @defgroup from_iterator_grp "from-iterator" source
* @brief A @ref from_iterator_grp feeds a processing branch with samples resulting from seeking with some iterator.
*
* **Source interface**
* @startuml
* rectangle "o(n) = *it++" <<from_iterator(it)>> as src
* interface "output: *it++ *it++ ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
*
* stream name | location    | description
* ------------|-------------|------------
* "produced"  | **output**  | a @ref atomic_stream "atomic stream" conveying the value resulting from seeking with the iterator
**/
///@{


/**
* @brief Creates a placeholder for a @ref from_iterator_grp to feed a processing branch with some values resulting from seeking with @c iterator.
*
* Note that with this variant of `from_iterator()` the given iterator is passed by value.
*
* **Usage example**
* @snippet routing_test.cpp from_iterator usage example
*
* @param iterator to seek the data
*
* @tparam T type is implicity deduced from @c iterator argument.
**/
//TODO: add static check for T models iterator
template<class T, DFP_F_REQUIRES((!std::detail::is_reference_wrapper<T>{}))>
inline constexpr auto from_iterator(T const& iterator) /** @cond */ ->
decltype(apply(detail::iterate_with_value<T>(iterator))) /** @endcond */
{ return apply(detail::iterate_with_value<T>(iterator)); }


/**
* @brief Creates a placeholder for a @ref from_iterator_grp to feed a processing branch with some values resulting from seeking with @c iterator reference.
*
* Note that with this variant of `from_iterator()` the given iterator is passed by reference.
*
* **Usage example**
* @snippet routing_test.cpp from_iterator usage example with ref
*
* @param ref `std::reference_wrapper` wrapping some iterator `ref`erence involved in the data seeking
*
* @tparam T type is implicity deduced from @c ref argument.
**/
//TODO: add static check for T models iterator
//TODO: to be turned into sink "observer_over(range) -- cpp17" and "observer_over(begin, end)" & item source
//TODO: create also observer_for(range/iterator/container, count) & observer_while(range/iterator/container, condition)
//TODO: turn current from_iterator() into item(iterator)
//TODO: observer_for is actually drainer() <<= iterate_for(range, count)
template<class T>
inline constexpr auto from_iterator(std::reference_wrapper<T> const& ref) /** @cond */ ->
decltype(apply(detail::iterate_with_ref<T>(ref))) /** @endcond */
{ return apply(detail::iterate_with_ref<T>(ref)); }


} // namespace sources


namespace shortname
{
inline namespace sources
{
///@addtogroup from_iterator_grp
///@{
/**
* @brief shortname for @ref routing::sources::from_iterator()
**/
template<class T>
inline auto constexpr fitr(T const& i) /** @cond */ ->
decltype(from_iterator(i)) /** @endcond */
{ return from_iterator(i); }
///@} from_iterator_grp
} // namespace sources
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif //DFPTL_ROUTING_FROM_ITERATOR_HPP
