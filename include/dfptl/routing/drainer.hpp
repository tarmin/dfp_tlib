/**
* @file routing/drainer.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_DRAINER_HPP
#define DFPTL_ROUTING_DRAINER_HPP


#include "dfptl/routing/detail/drainer_sink.hpp"
#include "dfptl/core/placeholder.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace routing
{
inline namespace sinks
{
///@addtogroup routing_grp
///@{
/**
* @defgroup drainer_grp "drainer" sink
* @brief Fires the input processing branch while given condition is true.
*
* A @ref drainer_grp schedules the execution of its single input processing branch in current thread
* and drains samples from the input stream while given condition is true.
* It does not apply any transformation on captured samples.
*
* @note With @ref drainer_grp the terminal captured samples are not released outside of the processing tree scope.
* So they will be "lost" if not consumed within the processing tree.
*
* **Sink interface**
* @startuml
* rectangle "while(condition) {captures} i(n);" <<drainer(condition)>> as loop
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* loop <- in
* @enduml
* * <tt>"in" **input**             </tt> : @ref atomic_stream "atomic stream" conveying samples to capture
* @{
**/
//TODO: drainer is an alias for release_sample_while
//TODO: looper is an alias for release_sample_for
//TODO: apply is an alias for acquire_sample
//TODO: apply is also an alias for process_sample
//TODO: const_sample is an alias for acquire_sample
//TODO: from is an alias for acquire_sample
//TODO: execute is also an alias for forward_sample
//TODO: execute is also an alias for sample_releaser

/**
* @brief Creates a placeholder for a @ref drainer_grp.
*
* @param continue_condition is a std::function which indicates that this sink shall continue to drain sample while it is evaluated as true.
*
* **Usage example**
* @snippet routing_test.cpp Drain while given function is true
**/
inline auto drainer(std::function<bool()> continue_condition) noexcept /** @cond */ ->
decltype(make_placeholder<detail::drainer_sink>(continue_condition)) /** @endcond */
{ return make_placeholder<detail::drainer_sink>(continue_condition); }


/**
* @brief Creates a placeholder for a @ref drainer_grp.
*
* @param continue_condition is a lvalue reference which indicates that this sink shall continue to drain sample while it is evaluated as true.
*
* **Usage example**
* @snippet routing_test.cpp Drain while given variable is true
**/
template<class T, DFP_F_REQUIRES((std::is_convertible<T, bool>{} && !std::is_invocable<T>{}))>
inline auto drainer(T const& continue_condition) noexcept /** @cond */ ->
decltype(drainer(std::function<bool()>())) /** @endcond */
{ return drainer([&continue_condition]{ return static_cast<bool>(continue_condition); }); }


/**
* @brief Creates a placeholder for a @ref drainer_grp and auto start execution of the processing.
*
* @param continue_condition is a std::function which indicates that this sink shall continue to drain sample while it is evaluated as true.
*
* **Usage example**
* @snippet routing_test.cpp Draining while given function is true
**/
inline auto draining(std::function<bool()> continue_condition) noexcept /** @cond */ ->
decltype(make_placeholder<detail::drainer_sink>(continue_condition).implicit_run()) /** @endcond */
{ return make_placeholder<detail::drainer_sink>(continue_condition).implicit_run(); }


/**
* @brief Creates a placeholder for a @ref drainer_grp and auto start execution of the processing.
*
* @param continue_condition is a lvalue reference which indicates that this sink shall continue to drain sample while it is evaluated as true.
*
* **Usage example**
* @snippet routing_test.cpp Draining while given variable is true
**/
template<class T, DFP_F_REQUIRES((std::is_convertible<T, bool>{} && !std::is_invocable<T>{}))>
inline auto draining(T const& continue_condition) noexcept /** @cond */ ->
decltype(draining(std::function<bool()>())) /** @endcond */
{ return draining([&continue_condition]{ return static_cast<bool>(continue_condition); }); }


///@} drainer_grp
} // namespace sinks


namespace shortname
{
inline namespace sinks
{
///@addtogroup drainer_grp
///@{
using routing::sinks::drainer;
using routing::sinks::draining;
///@} drainer_grp
} // namespace sinks
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif //DFPTL_ROUTING_DRAINER_HPP
