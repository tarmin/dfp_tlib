/**
* @file routing/extract_channel.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ROUTING_EXTRACT_CHANNEL_HPP
#define DFPTL_ROUTING_EXTRACT_CHANNEL_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/routing/detail/extract_channel_node.hpp"


namespace dfp
{
namespace routing
{
inline namespace nodes
{
///@addtogroup routing_grp
///@{
/**
* @defgroup extract_channel_grp "extract channel" node
* @brief Extracts a single channel from a multi-channels input stream.
*
* A @ref extract_channel_grp produces to its output the stream resulting from single channel extraction from its SINGLE input.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n,j) | 0<=j<k (k as channel count, N as frame length)" <<extract_channel(j)>> as extract
* interface "input: [i(0,0) i(0,1) ... i(0,k) i(1,0) i(1,1) ... i(N,k)] ..." <<INPUT>> as in
* interface "output: [i(0,j) i(1,j) ... i(N,j)" <<operation result>> as out
* extract <-up- in
* out <-up- extract
* @enduml
*
* stream name         | location    | description
* --------------------|-------------|------------
* INPUT               | **input-0** | @ref a stream conveying multiple channels frame
* "operation result"  | **output**  | @ref a stream resulting extraction of a single channel
* @{
**/


/**
* @brief Creates a cell builder for a @ref extract_channel_grp.
*
* **Usage example**
* @snippet routing_test.cpp extract-channel node usage
*
* @param channel_index is index of the channel to extract
**/
auto extract_channel(std::size_t channel_index) /** @cond */ ->
decltype(make_placeholder<detail::extract_channel_node>(channel_index)) /** @endcond */
{ return make_placeholder<detail::extract_channel_node>(channel_index); }


///@} extract_channel_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using routing::nodes::extract_channel;
} // namespace nodes
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_EXTRACT_CHANNEL_HPP
