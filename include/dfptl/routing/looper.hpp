/**
* @file routing/looper.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_ROUTING_LOOPER_HPP
#define DFPTL_ROUTING_LOOPER_HPP


#include "dfptl/routing/detail/looper_sink.hpp"
#include "dfptl/core/placeholder.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace routing
{
inline namespace sinks
{
///@addtogroup routing_grp
///@{
/**
* @defgroup looper_grp "looper" sink
* @brief Fires in loop its input processing branch.
*
* A @ref looper_grp schedules in loop the execution of its single input processing branch in current thread
* and drains the given count of samples from its input stream. It does not apply any transformation on captured samples.
*
* @note With @ref looper_grp the captured samples are not released outside of the processing tree scope.
* So they will be "lost" if not consumed within the processing tree.
*
* @warning frame-based processing can be bound to a @ref looper_grp but take attention that the count of processing fired will be adjusted to at least
* drain the count of samples given as parameter to the sink. Thus @code looping(5) <<= const_frame({1, 2, 3}; @endcode will trigger 2 iterations of processing
* as 5 samples are required by the @ref looper_grp (So actually 2 frames of 3 samples will be drained by those iterations).
*
* **Sink interface**
* @startuml
* rectangle "for(n=0;n<N;n++) {captures} i(n);" <<looper(N)>> as loop
* interface "input-0: i(0) i(1) ...i(n)" <<INPUT>> as in
* loop <-up- in
* @enduml
* stream name/result | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | any kind of @ref stream_grp "DFP stream"
* @{
**/


/**
* @brief Creates a builder for a @ref looper_grp.
*
* @param samples_count is the count of samples to drain.
*
* **Usage example**
* @snippet routing_test.cpp Drain in loop samples from processing branch
**/
//TODO: add optimized implementation for size_t_const
template<class T, DFP_F_REQUIRES((std::is_convertible<T, std::size_t>{}))>
inline auto looper(T samples_count)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<detail::looper_sink>((std::size_t)samples_count))
#endif
{ return make_placeholder<detail::looper_sink>((std::size_t)samples_count); }


/**
* @brief Creates a builder for a @ref looper_grp.
*
* @param samples_count is a std::function providing the count of samples to drain.
* This std::function is evaluated once for all at beginning of the loop.
*
* **Usage example**
* @snippet routing_test.cpp Drain in loop with iteration count as result of the given function
*
* @sa the @ref drainer_grp for a sink whose run condition is evaluated at each iteration
**/
//TODO: add optimized implementation for size_t_const
inline auto looper(std::function<std::size_t()> samples_count)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<detail::looper_sink>(samples_count))
#endif
{ return make_placeholder<detail::looper_sink>(samples_count); }


/**
* @brief Creates a builder for a @ref looper_grp and auto start execution of the processing.
*
* This builder is actually an alias for looper(...).implicitly_run() 
*
* @param samples_count is the count of samples to drain. It can be a value convertible to std::size_t or a function object producting std::size_t
*
* @tparam T is implictly deduced from parameter @c samples_count
*
* **Usage example**
* @snippet routing_test.cpp Draining in loop samples from processing branch
* @snippet routing_test.cpp Draining in loop samples from frame-based processing branch
**/
template<class T>
constexpr inline auto looping(T&& samples_count)
#if not DFP_CPP14_SUPPORT
->
decltype(looper(std::forward<T>(samples_count)).implicit_run())
#endif
{ return looper(std::forward<T>(samples_count)).implicit_run(); }


///@} looper_grp
} // namespace sinks


namespace shortname
{
inline namespace sinks
{
///@addtogroup looper_grp
///@{
using routing::sinks::looper;
using routing::sinks::looping;
///@} looper_grp
} // namespace sinks
} // namespace shortname


///@} routing_grp
} // namespace routing
} // namespace dfp


#endif // DFPTL_ROUTING_LOOPER_HPP
