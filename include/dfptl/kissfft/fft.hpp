/**
* @file kissfft/fft.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_KISSFFT_FFT_HPP
#define DFPTL_KISSFFT_FFT_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/kissfft/detail/fft_node.hpp"


namespace dfp
{
/**
* @defgroup kissfft_grp kissFFT node
* @brief Applies Fast Fourier Transform of its SINGLE input stream based on [kissfft](https://github.com/mborgerding/kissfft) library.
* @{
*
* A @ref kissfft_grp can applies FFT or inverse FFT of a given length on frame reaching its SINGLE input.
* If the input frame does not have the same length as the FFT length the input frame length is automatically adjusted
* (by inserting an @ref adjust_frame_length_grp at the input).
*
* **Node interface**
* @startuml
* rectangle "o(n) = fft([i(n) ...i(n+LENGTH-1)])" <<kissfft::fft(LENGTH)>> as fft
* interface "...[i(n) ...i(n+N-1)]" <<INPUT>> as in
* interface "...o(n)" <<"spectrum frame">> as out
* fft <-down- in
* out <-down- fft
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | frame stream of sample to transform
* "spectrum frame"   | **output**  | spectrum frame stream resulting from the FFT
**/


/**
* @namespace dfp::kissfft
* @brief Contains items related to processing cells wrapping the FFT function.
**/
///@}
namespace kissfft
{
/**
* @addtogroup kissfft_grp
* @{
* @namespace dfp::kissfft::nodes
* @brief Set of helper functions for kissFFT nodes.
* @}
**/
inline namespace nodes
{
///@addtogroup kissfft_grp
///@{
/**
* @brief Creates a placeholder for a @ref kissfft_grp.
*
* @param fft_length length of the FFT transform.
* @param inverse if `true` apply inverse FFT.
**/
inline auto fft(size_t fft_length, bool inverse = false) /** @cond */ ->
decltype(make_placeholder<std::tuple<decltype(DYNAMIC_EXTENT)>, detail::versatile_fft_node>(fft_length, inverse)) /** @endcond */
{ return make_placeholder<std::tuple<decltype(DYNAMIC_EXTENT)>, detail::versatile_fft_node>(fft_length, inverse); }


/**
* @brief Creates a placeholder for an inverse @ref kissfft_grp.
*
* @param fft_length length of the FFT transform.
**/
inline auto ifft(size_t fft_length) /** @cond */ ->
decltype(make_placeholder<std::tuple<decltype(DYNAMIC_EXTENT)>, detail::versatile_fft_node>(fft_length, true)) /** @endcond */
{ return make_placeholder<std::tuple<decltype(DYNAMIC_EXTENT)>, detail::versatile_fft_node>(fft_length, true); }


/**
* @brief Creates a placeholder for a @ref kissfft_grp.
*
* @param fft_length length of the FFT transform.
* @param inverse if `true` apply inverse FFT.
*
* @tparam Integral is implicitly deduced from @c fft_length parameter.
* @tparam FftLength is implicitly deduced from @c fft_length parameter.
*
* **Usage example**
* @snippet kissfft_test.cpp fft usage example
**/
template<class Integral, Integral FftLength, DFP_F_REQUIRES((FftLength != DYNAMIC_EXTENT))>
inline auto fft(integral_const<Integral, FftLength> fft_length, bool inverse = false) /** @cond */ ->
decltype(make_placeholder<std::tuple<size_t_const< FftLength>>, detail::versatile_fft_node>(inverse)) /** @endcond */
{ static_assert(fft_length > 0, "fft_length shall be a strictly positive number");
  return make_placeholder<std::tuple<size_t_const<fft_length>>, detail::versatile_fft_node>(inverse); }


/**
* @brief Creates a placeholder for an inverse @ref kissfft_grp.
*
* @param fft_length length of the FFT transform.
*
* @tparam Integral is implicitly deduced from @c fft_length parameter.
* @tparam FftLength is implicitly deduced from @c fft_length parameter.
*
* **Usage example**
* @snippet kissfft_test.cpp fft usage example
**/
template<class Integral, Integral FftLength, DFP_F_REQUIRES((FftLength != DYNAMIC_EXTENT))>
inline auto ifft(integral_const<Integral, FftLength> fft_length) /** @cond */ ->
decltype(make_placeholder<std::tuple<size_t_const< FftLength>>, detail::versatile_fft_node>(true)) /** @endcond */
{ static_assert(fft_length > 0, "fft_length shall be a strictly positive number");
  return make_placeholder<std::tuple<size_t_const<fft_length>>, detail::versatile_fft_node>(true); }


///@} kissfft_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using kissfft::fft;
using kissfft::ifft;
} // namespace nodes
} // namespace shortname


} // namespace kissfft
} // namespace dfp


#endif // DFPTL_KISSFFT_FFT_HPP
