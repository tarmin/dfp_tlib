/**
* @file kissfft/detail/fft.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_KISSFFT_DETAIL_FFT_HPP
#define DFPTL_KISSFFT_DETAIL_FFT_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/subprocessing.hpp"
#include "dfptl/routing/adjust_frame_length.hpp"
#include "dfptl/math/complex.hpp"

#include <kissfft.hh>


using namespace dfp::routing;


namespace dfp
{
namespace kissfft
{
namespace detail
{


#define DFP_KISSFFT_FFT kissfft::fft


template<class Input>
struct fft_input
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_KISSFFT_FFT, 1));

    typedef std::tuple_element_t<0, Input> type;
};

template<class Input>
using fft_input_t = typename fft_input<Input>::type;


template<class Input, class = void>
class fft_node
{ static_assert(always_false<Input>{}, DFP_UNEXPECTED_ERROR); };


template<std::size_t FrameExtent, class = void>
class versatile_fft_processing
{ static_assert(always_false<size_t_const<FrameExtent>>{}, DFP_UNEXPECTED_ERROR); };

template<std::size_t FrameExtent>
class versatile_fft_processing<FrameExtent, DFP_C_REQUIRES((FrameExtent != DYNAMIC_EXTENT))>
{
private:
    bool const inverse_;
    static constexpr size_t_const<FrameExtent> FRAME_EXTENT = size_t_const<FrameExtent>{};

public:
    versatile_fft_processing(bool inverse) :    inverse_(inverse)
    {}

    template<class Input>
    auto operator()(Input in) const ->
    decltype(make_placeholder<fft_node>(FrameExtent, this->inverse_) <<= adjust_frame_length(FRAME_EXTENT) <<= in)
    { return make_placeholder<fft_node>(FrameExtent, this->inverse_) <<= adjust_frame_length(FRAME_EXTENT) <<= in; }
}; //class versatile_fft_processing< ... FrameExtent != DYNAMIC_EXTENT ...>

template<std::size_t FrameExtent>
class versatile_fft_processing<FrameExtent, DFP_C_REQUIRES((FrameExtent == DYNAMIC_EXTENT))>
{

    std::size_t const frame_length_;
    bool const inverse_;

public:
    versatile_fft_processing(std::size_t frame_length, bool inverse)
     :  frame_length_(frame_length), inverse_(inverse)
    {}

    template<class Input>
    auto operator()(Input in) const ->
    decltype(make_placeholder<fft_node>(this->frame_length_, this->inverse_) <<= adjust_frame_length(this->frame_length_) <<= in)
    { return make_placeholder<fft_node>(this->frame_length_, this->inverse_) <<= adjust_frame_length(this->frame_length_) <<= in; }
}; //class versatile_fft_processing< ... FrameExtent == DYNAMIC_EXTENT ...>


template<class Input, class FrameExtent>
class versatile_fft_node
 :  public  core::detail::subprocessing_node<Input, versatile_fft_processing<FrameExtent::value>>
{
    static_assert(is_size_t_const<FrameExtent>{}, "FrameExtent shall be a size_t_const template specialization for "  DFP_STRINGIFY(DFP_KISSFFT_FFT));

    typedef core::detail::subprocessing_node<Input, versatile_fft_processing<FrameExtent::value>> node_type;

public:
    template<std::size_t FrameExtentValue = FrameExtent::value, DFP_F_REQUIRES((FrameExtentValue == DYNAMIC_EXTENT))>
    versatile_fft_node(Input&& input, std::size_t frame_length, bool inverse)
    :   node_type(std::forward<Input>(input), frame_length, inverse)
    {}

    template<std::size_t FrameExtentValue = FrameExtent::value, DFP_F_REQUIRES((FrameExtentValue != DYNAMIC_EXTENT))>
    versatile_fft_node(Input&& input, bool inverse)
    :   node_type(std::forward<Input>(input), inverse)
    {}
}; //class versatile_fft_node


template<class Input>
class fft_node<Input, DFP_C_REQUIRES((!math::is_complex<output_sample_t<fft_input_t<Input>>>{}))>
 :  public node<Input, as_input0_tag, fft_node<Input>>
{
    static_assert(always_false<Input>{}, "Support of non complex type in input for fft node has not yet been implemented.");
}; // class fft_node<... !is_complex ...>


template<class Input>
class fft_node<Input, DFP_C_REQUIRES((math::is_complex<output_sample_t<fft_input_t<Input>>>{}))>
 :  public node<Input, as_input0_tag, fft_node<Input>>
{
    // alias for the base type
    typedef node<Input, as_input0_tag, fft_node> node_type;
    typedef output_frame_t<node_type> frame_type;

public:
    fft_node(Input&& input, std::size_t length, bool inverse)
     :  node_type(std::forward<Input>(input)), kissfft_(length, inverse), output_(frame_builder<frame_type>{}.build())
    {}

    output_frame_t<node_type> const& process_frame(input_frame_t<node_type> const& input)
    {
        //TODO: verify that frame has data() operation
        kissfft_.transform(input.data(), output_.data());

        return output_;
    }

private:
    ::kissfft<typename input_sample_t<node_type>::value_type> kissfft_;
    frame_type output_;
}; // class fft_node< ... is_complex ...>



} // namespace detail
} // namespace kissfft
} // namespace dfp


#endif //DFPTL_KISSFFT_DETAIL_FFT_HPP
