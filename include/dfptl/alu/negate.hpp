/**
* @file alu/negate.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_NEGATE_HPP
#define DFPTL_ALU_NEGATE_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup negate_grp "negate" node
* @brief Performs negation of its input samples.
*
* A @ref negate_grp performs an element-wise negation of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from the input sample type and the C++ standard rule for this arithmetic operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = -i0(n)" <<negate()>> as neg
* interface "input-0: 1 2 3 4 ...i0(n)" <<in>> as in0
* interface "output: -1 -2 -3 -4 ...o(n)" <<operation result>> as out
* neg <- in0
* out <- neg
* @enduml
* * <tt>"in" **input-0**             </tt> : @ref atomic_stream "atomic stream" conveying sample to negate
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a placeholder for a @ref negate_grp.
*
* **Usage example**
* @snippet alu_test.cpp negate usage example
*
* @sa neg()
**/
inline auto constexpr negate() /** @cond */ ->
decltype(apply<upg::negate<>>()) /** @endcond */
{ return apply<upg::negate<>>(); }


///@} negate_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup negate_grp
///@{
/**
* @brief shortname for @ref negate()
**/
inline auto constexpr neg() /** @cond */ ->
decltype(negate()) /** @endcond */
{ return negate(); }
///@} negate_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_NEGATE_HPP
