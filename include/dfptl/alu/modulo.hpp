/**
* @file alu/modulo.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_MODULO_HPP
#define DFPTL_ALU_MODULO_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup modulo_grp "modulo" node
* @brief Performs modulo operation of its both input samples.
*
* A @ref modulo_grp performs an element-wise division of sample on its 1st input
* with the one on its 2nd input and produces the resulting remainder on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this arithmetic operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i0(n) % i1(n)" <<modulo()>> as mod
* interface "input-0: 1 2 3 4 5 ...i0(n)" <<lhs>> as in0
* interface "input-1: 4 4 4 4 6 ...i1(n)" <<rhs>> as in1
* interface "output: 1 2 3 0 5 ...o(n)" <<modulo result>> as out
* in0 -down-> mod
* mod <- in1
* out <- mod
* @enduml
* * <tt>"lhs" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying left-hand side operand of the operation
* * <tt>"rhs" **input-1**            </tt> : @ref atomic_stream "atomic stream" conveying right-hand side operand of the operation
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a placeholder for a @ref modulo_grp.
*
* **Usage example**
* @code{.cpp}
* auto processing_branch = alu::modulo().build(
*       5,
*       2
* );
* assert(*processing_branch.begin() == 5%2);
* @endcode
*
* @sa mod()
**/
//TODO: add check for input stream of integral sample
inline auto constexpr modulo() /** @cond */ ->
decltype(apply<upg::modulus<>>()) /** @endcond */
{ return apply<upg::modulus<>>(); }


/**
* @copybrief modulo()
*
* @note Alias for modulo().preset_inputs(std::forward<Rhs>(rhs))
*
* **Usage example**
* @code{.cpp}
* auto processing_branch = alu::modulo(2) <<= 5;
* assert(*processing_branch.begin() == 5%2);
* auto processing_branch2 = alu::modulo(processing_branch) <<= 3;
* assert(*processing_branch.begin() == (5%2)%3);
* @endcode

* @param rhs replaces the 2nd input stream (*Input-1*). This is the right hand operand of this arithmetic operation.
* It can have any type suitable with this operation against the given sample type conveyed by *Input-0* stream
* or a processing branch conveying sample suitable with this operation.
*
* @tparam Rhs is implicitly deduced from @c rhs argument.
*
* @sa modulo() mod()
**/
template<class Rhs>
inline constexpr auto modulo(Rhs&& rhs) /** @cond */ ->
decltype(apply<upg::modulus<>>().preset_inputs(std::forward<Rhs>(rhs))) /** @endcond */
{ return apply<upg::modulus<>>().preset_inputs(std::forward<Rhs>(rhs)); }


///@} modulo_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup modulo_grp
///@{
/**
* @brief shortname for @ref modulo(Rhs&&)
**/
template<class Rhs>
inline constexpr auto mod(Rhs&& rhs) /** @cond */ ->
decltype(modulo(std::forward<Rhs>(rhs))) /** @endcond */
{ return modulo(std::forward<Rhs>(rhs)); }


/**
* @brief shortname for @ref modulo()
**/
inline constexpr auto mod() /** @cond */ ->
decltype(modulo()) /** @endcond */
{ return modulo(); }


///@} modulo_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_MODULO_HPP
