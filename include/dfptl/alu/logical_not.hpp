/**
* @file alu/logical_not.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_LOGICAL_NOT_HPP
#define DFPTL_ALU_LOGICAL_NOT_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup logical_not_grp "logical-NOT" node
* @brief Performs logical negation of its input sample.
*
* A @ref logical_not_grp performs an element-wise logical negation of sample on its single input
* produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this logical operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = !i0(n)" <<logical_not()>> as oper
* interface "input-0: 1 0 2 ...i0(n)" <<operand>> as in0
* interface "output: false true false ...o(n)" <<operation result>> as out
* oper <- in0
* out <- oper
* @enduml
* * <tt>"operand" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying the single operand of the operation
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/

/**
* @brief Creates a placeholder for a @ref logical_not_grp.
*
* **Usage example**
* @code{.cpp}
* auto processing_branch = alu::logical_not() <<= true
* assert(*processing_branch.begin() == !true);
* @endcode
*
* @sa lnot()
**/
inline auto constexpr logical_not() /** @cond */ ->
decltype(apply<upg::logical_not<>>()) /** @endcond */
{ return apply<upg::logical_not<>>(); }


///@} logical_not_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup logical_not_grp
///@{
/**
* @brief shortname for @ref logical_not()
**/
inline auto constexpr lnot() /** @cond */ ->
decltype(logical_not()) /** @endcond */
{ return logical_not(); }


///@} logical_not_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_LOGICAL_NOT_HPP
