/**
* @file alu/alu.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_ALU_HPP
#define DFPTL_ALU_ALU_HPP


#include "dfptl/alu/add.hpp"
#include "dfptl/alu/negate.hpp"
#include "dfptl/alu/divide.hpp"
#include "dfptl/alu/modulo.hpp"
#include "dfptl/alu/counter.hpp"
#include "dfptl/alu/multiply.hpp"
#include "dfptl/alu/subtract.hpp"
#include "dfptl/alu/equal_to.hpp"
#include "dfptl/alu/not_equal.hpp"
#include "dfptl/alu/less_than.hpp"
#include "dfptl/alu/less_equal.hpp"
#include "dfptl/alu/accumulate.hpp"
#include "dfptl/alu/logical_or.hpp"
#include "dfptl/alu/accumulator.hpp"
#include "dfptl/alu/logical_and.hpp"
#include "dfptl/alu/logical_not.hpp"
#include "dfptl/alu/greater_than.hpp"
#include "dfptl/alu/greater_equal.hpp"
#include "dfptl/alu/schmitt_trigger.hpp"
#include "dfptl/alu/average_channels.hpp"


namespace dfp
{
/**
* @defgroup alu_grp Arithmetic & Logic processing cells
* @brief Defines processing nodes applying standard C++ arithmetic and logic operations and some usual sources only depending on those nodes.
*
* Please take a look at the @ref triangular_waveform_generator.cpp for an example of processing tree only involving @ref alu_grp.
* @{
*
* @defgroup alu_detail implementation details
* @brief Implementation details and boilerplate for @ref alu_grp
**/


/**
* @brief Contains items related to processing cells for C++ Arithmetic & Logic operations
**/
namespace alu
{
/**
* @brief Set of helper functions for Arithmetic & Logic nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for Arithmetic & Logic sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief short name of functions in @ref alu_grp namespace
**/
namespace shortname {}


///@}
} // namespace alu
} // namespace dfp


/** @example triangular_waveform_generator.cpp
* An example of sawtooth function generator mainly involving @ref alu_grp.
*
* Here is the processing tree implemented:
* @startuml
* rectangle "alu::counter(1)" <<source>> as counter
* rectangle "alu::modulo(PERIOD_LENGTH)" <<node>> as mod_length
* rectangle "alu::less_than(PERIOD_LENGTH/2)" <<node>> as less
* rectangle "alu::multiply(2)" <<node>> as mult
* rectangle "alu::subtract(1)" <<node>> as subst
* rectangle "alu::accumulate()" <<node>> as acc
* rectangle "print_2_periods" <<custom sink>> as looper
*
* mod_length <- counter
* less <- mod_length
* mult <- less
* subst <- mult
* acc <- subst
* looper <- acc
* @enduml
**/


#endif // DFPTL_ALU_ALU_HPP
