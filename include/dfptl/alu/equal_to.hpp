/**
* @file alu/equal_to.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_EQUAL_TO_HPP
#define DFPTL_ALU_EQUAL_TO_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup equal_to_grp "equal-to" node
* @brief Tests equality of its both input samples.
*
* A @ref equal_to_grp performs an element-wise equality-test of sample on its 1st input
* with the one on its 2nd input and produces the comparison result on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this logical operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i0(n) == i1(n)" <<equal_to()>> as test
* interface "input-0: 4 5 ...i0(n)" <<lhs>> as in0
* interface "input-1: 2 5 ...i1(n)" <<rhs>> as in1
* interface "output: false true ...o(n)" <<operation result>> as out
* in0 -down-> test
* test <- in1
* out <- test
* @enduml
* * <tt>"lhs" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying left-hand side operand of the operation
* * <tt>"rhs" **input-1**            </tt> : @ref atomic_stream "atomic stream" conveying right-hand side operand of the operation
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a placeholder for a @ref equal_to_grp.
*
* **Usage example**
* @snippet alu_test.cpp equal_to usage example
*
* @sa eq()
**/
inline auto constexpr equal_to() /** @cond */ ->
decltype(apply<upg::equal_to<>>()) /** @endcond */
{ return apply<upg::equal_to<>>(); }


/**
* @copybrief equal_to()
*
* @note Alias for equal_to().preset_inputs(std::forward<Rhs>(rhs))
*
* **Usage example**
* @snippet alu_test.cpp equal_to usage example with preset

* @param rhs replaces the 2nd input stream (*Input-1*). This is the right hand operand of this logic operation.
* It can have any type suitable with this operation against the given sample type conveyed by *Input-0* stream
* or a processing branch conveying sample suitable with this operation.
*
* @tparam Rhs is implicitly deduced from @c rhs argument.
*
* @sa equal_to() eq()
**/
template<class Rhs>
inline auto constexpr equal_to(Rhs&& rhs) /** @cond */ ->
decltype(apply<upg::equal_to<>>().preset_inputs(std::forward<Rhs>(rhs))) /** @endcond */
{ return apply<upg::equal_to<>>().preset_inputs(std::forward<Rhs>(rhs)); }


///@} equal_to_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup equal_to_grp
///@{
/**
* @brief shortname for @ref equal_to(Rhs&&)
**/
template<class Rhs>
inline auto constexpr eq(Rhs&& rhs) /** @cond */ ->
decltype(equal_to(std::forward<Rhs>(rhs))) /** @endcond */
{ return equal_to(std::forward<Rhs>(rhs)); }


/**
* @brief shortname for @ref equal_to()
**/
inline auto constexpr eq() /** @cond */ ->
decltype(equal_to()) /** @endcond */
{ return equal_to(); }


///@} equal_to_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_EQUAL_TO_HPP
