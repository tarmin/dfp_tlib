/**
* @file alu/accumulate.hpp
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_ACCUMULATOR_HPP
#define DFPTL_ALU_ACCUMULATOR_HPP


#include "dfptl/alu/detail/accumulator_sink.hpp"
#include "dfptl/core/placeholder.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace sinks
{
///@addtogroup alu_grp
///@{
/**
* @defgroup accumulator_grp "accumulator" sink
* @brief Accumulates a given count of samples from its single input and returns result.
*
* An @ref accumulator_grp accumulates samples from its SINGLE input.
* When `CumulOperation` is provided it serves to accumulate elements otherwise `std::plus<>` is used by defaut.
* Sample resulting from cumulation of all past input samples is produced as the result of the sink run after a given count of samples has been drained.
* The sink result type is same as the `CumulOperation` (or `std::plus<>`) reult type.
*
* **Sink interface**
* * with usage of `std::plus<>`
* @startuml
* rectangle "o(n) = o(n-1) + i0(n) | o(0) = initial + i(0)" <<accumulator(samples_count, initial)>> as acc
* interface "input-0: 1 3 2 ...i0(n)" <<INPUT>> as in0
* interface "result: initial+1+3+2+ ... +.o(n)" <<operation result>> as out
* acc <-down- in0
* out <.down. acc
* @enduml
* * with custom `op`eration
* @startuml
* rectangle "o(n) = op(o(n-1), i0(n-1)) | o(0) = initial" <<accumulator(samples_count, op, initial)>> as acc_
* interface "input-0: 1 3 2 ...i0(n)" <<INPUT>> as in0_
* interface "result: op(...op(initial, i(0)), o(n)...)" <<operation result>> as out_
* acc_ <-down- in0_
* out_ <.down. acc_
* @enduml
*
* stream name    | location    | description
* ---------------|-------------|------------
* INPUT          | **input-0** | any @ref stream_grp conveying the samples to accumulate
* "cumul result" | **result**  | cumul result over samples count
* @{
**/


/**
* @brief Creates a builder for an @ref accumulator_grp accumulating samples reaching its input with the given `CumulOperation` over a given samples count
*
* @param samples_count specifies the count of samples reaching the sink input to accumulate before providing a result.
* @param initial is the initial value of the cumul. Default value is @c Initial{}. Any type for this parameter is acceptable
* as long as it is compatible as left operand of the `CumulOperation` with the sample type of the input stream.
* @param cumul_op is the cumul operation to apply on each sample and the intermediate cumul result.
*
* @tparam Initial can be implicitly deduced from the @c initial parameter.
* @tparam CumulOperation can be implicitly deduced from the @c cumul_op parameter.
*
* **Usage examples**
* @snippet alu_test.cpp 'accumulator sink' usage example with frame-based stream
* @snippet alu_test.cpp 'accumulator sink' usage example with custom cumul operation
*
* @sa accumulate_grp
**/
template<class CumulOperation = upg::plus<>, class Initial = int>
inline auto constexpr accumulator(
    std::size_t samples_count,
    Initial&& initial = Initial{},
    CumulOperation&& cumul_op = CumulOperation{}
)
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<std::tuple<Initial, CumulOperation>, detail::accumulator_sink>(samples_count, std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)))
#endif
{ return make_placeholder<std::tuple<Initial, CumulOperation>, detail::accumulator_sink>(samples_count, std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)); }


/**
* @brief alias for accumulator().implicit_run()
*
* Creates a builder for @ref accumulator() "accumulator sink" and automatically drains the required samples so that cumul result is immediately available.
*
* @param samples_count specifies the count of samples reaching the sink input to accumulate before providing a result.
* @param initial is the initial value of the cumul. Default value is @c Initial{}. Any type for this parameter is acceptable
* as long as it is compatible as left operand of the `CumulOperation` with the sample type of the input stream.
* @param cumul_op is the cumul operation to apply on each sample and the intermediate cumul result.
*
* @tparam Initial can be implicitly deduced from the @c initial parameter.
* @tparam CumulOperation can be implicitly deduced from the @c cumul_op parameter.
*
* **Usage example**
* @snippet alu_test.cpp 'accumulating sink' usage example with sample-based stream
*
* @sa accumulate_grp
**/
template<class CumulOperation = upg::plus<>, class Initial = int>
inline auto constexpr accumulating(
    std::size_t samples_count,
    Initial&& initial = Initial{},
    CumulOperation&& cumul_op = CumulOperation{}
)
#if not DFP_CPP14_SUPPORT
->
decltype(accumulator(samples_count, std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)).implicit_run())
#endif
{ return accumulator(samples_count, std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)).implicit_run(); }


///@} accumulator_grp
///@} alu_grp
} // inline namespace sinks
} // namespace alu
} // namespace dfp


#endif //DFPTL_ALU_ACCUMULATOR_HPP
