/**
* @file alu/less_than.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_LESS_THAN_HPP
#define DFPTL_ALU_LESS_THAN_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup less_than_grp "less-than" node
* @brief Tests for less-than comparison of its both input samples.
*
* A @ref less_than_grp performs an element-wise less-than comparison of sample on its 1st input
* with the one on its 2nd input and produces the comparison result on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this logical operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i0(n) < i1(n)" <<less_than()>> as test
* interface "input-0: 4 3 ...i0(n)" <<lhs>> as in0
* interface "input-1: 2 5 ...i1(n)" <<rhs>> as in1
* interface "output: false true ...o(n)" <<operation result>> as out
* in0 -down-> test
* test <- in1
* out <- test
* @enduml
* * <tt>"lhs" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying left-hand side operand of the operation
* * <tt>"rhs" **input-1**            </tt> : @ref atomic_stream "atomic stream" conveying right-hand side operand of the operation
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a placeholder for a @ref less_than_grp.
*
* **Usage example**
* @snippet alu_test.cpp less_than usage example
*
* @sa lt()
**/
inline auto constexpr less_than() /** @cond */ ->
decltype(apply<upg::less<>>()) /** @endcond */
{ return apply<upg::less<>>(); }


/**
* @copybrief less_than()
*
* @note Alias for less_than().preset_inputs(std::forward<Rhs>(rhs))
*
* **Usage example**
* @snippet alu_test.cpp less_than usage example with preset
*
* @param rhs replaces the 2nd input stream (*Input-1*). This is the right hand operand of this logic operation.
* It can have any type suitable with this operation against the given sample type conveyed by *Input-0* stream
* or a processing branch conveying sample suitable with this operation.
*
* @tparam Rhs is implicitly deduced from @c rhs argument.
*
* @sa less_than() lt()
**/
template<class Rhs>
inline auto constexpr less_than(Rhs&& rhs) /** @cond */ ->
decltype(apply<upg::less<>>().preset_inputs(std::forward<Rhs>(rhs))) /** @endcond */
{ return apply<upg::less<>>().preset_inputs(std::forward<Rhs>(rhs)); }


///@} less_than_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup less_than_grp
///@{
/**
* @brief shortname for @ref less_than(Rhs&&)
**/
template<class Rhs>
inline auto constexpr lt(Rhs&& rhs) /** @cond */ ->
decltype(less_than(std::forward<Rhs>(rhs))) /** @endcond */
{ return less_than(std::forward<Rhs>(rhs)); }


/**
* @brief shortname for @ref less_than()
**/
inline auto constexpr lt() /** @cond */ ->
decltype(less_than()) /** @endcond */
{ return less_than(); }


///@} less_than_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_LESS_THAN_HPP
