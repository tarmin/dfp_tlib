/**
* @file alu/logical_or.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_LOGICAL_OR_HPP
#define DFPTL_ALU_LOGICAL_OR_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
namespace functions
{
struct logical_or
{
    template<class Arg0, class Arg1, class... Args>
    constexpr auto operator()(Arg0&& arg0, Arg1&& arg1, Args&&... args) const ->
    decltype(std::declval<logical_or>() (upg::logical_or<>{}(std::forward<Arg0>(arg0), std::forward<Arg1>(arg1)), std::forward<Args>(args)...))
    { return logical_or{}               (upg::logical_or<>{}(std::forward<Arg0>(arg0), std::forward<Arg1>(arg1)), std::forward<Args>(args)...); }

    template<class Arg0, class Arg1>
    constexpr auto operator()(Arg0&& arg0, Arg1&& arg1) const ->
    decltype(upg::logical_or<>{}(std::forward<Arg0>(arg0), std::forward<Arg1>(arg1)))
    { return upg::logical_or<>{}(std::forward<Arg0>(arg0), std::forward<Arg1>(arg1)); }

//    template<class... Args>
//    constexpr bool operator()(Args&&...) const
//    { static_assert(sizeof...(Args) >= 2, "logical_or expects at least 2 inputs"); return true; }
}; //struct logical_or


} //namespace functions


inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup logical_or_grp "logical-OR" node
* @brief Applies logical OR operation on sample of its 2 or more input streams.
*
* A @ref logical_or_grp performs an element-wise logical-OR of sample on its 1st input with the sample on its 2nd input.
* If more inputs are present the result of the first 2-terms operation is used as 1st input to operate
* a subsequent logical-OR operation with the next input.
* The process is iterated until consuming of the following inputs.
* The result of the last operation is produces on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this logical operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i0(n) || i1(n) || ..." <<logical_or()>> as op
* interface "input-0: true false ...i0(n)" <<IN0>> as in0
* interface "input-1: true true ...i1(n)" <<IN1>> as in1
* interface "input-.." as ini
* interface "input-p: true true ...ip(n)" <<INp>> as inp
* interface "output: (i0(0)||i1(0)||...ip(0))  ...o(n)" <<operation result>> as out
* in0 -down-> op
* in1 -down-> op
* ini -down-> op
* inp -down-> op
* out <-up- op
* @enduml
* stream name        | location    | description
* -------------------|-------------|------------
* IN0                | **input-0** | @ref atomic_stream "atomic stream" conveying left-hand side operand of the operation
* IN1                | **input-1** | @ref atomic_stream "atomic stream" conveying right-hand side operand of the operation
* IN..               | **input-..**| @ref atomic_stream "atomic stream" conveying possible subsequent input(s) of the operation
* INp                | **input-p** | @ref atomic_stream "atomic stream" conveying the last input **[ p >= 1 ]**
* "operation result" | **output**  | the @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a cell builder for a @ref logical_or_grp.
*
* this node can accept 2 or more input sample-based streams
*
* **Usage example**
* @snippet alu_test.cpp logical_or usage example
*
* @sa lor()
**/
inline auto constexpr logical_or() /** @cond */ ->
decltype(apply<functions::logical_or>()) /** @endcond */
{ return apply<functions::logical_or>(); }


/**
* @copybrief logical_or()
*
* @note Alias for `logical_or().preset_inputs(std::forward<Rhs>(rhs))`
*
* **Usage example**
* @snippet alu_test.cpp logical_or usage example with preset

* @param rhs replaces the 2nd input stream (*Input-1*). This is the right hand operand of this logic operation.
* It can have any type suitable with this operation against the given sample type conveyed by *Input-0* stream
* or a processing branch conveying sample suitable with this operation.
*
* @tparam Rhs is implicitly deduced from @c rhs argument.
*
* @sa logical_or() lor()
**/
template<class Rhs>
inline auto constexpr logical_or(Rhs&& rhs) /** @cond */ ->
decltype(logical_or().preset_inputs(std::forward<Rhs>(rhs))) /** @endcond */
{ return logical_or().preset_inputs(std::forward<Rhs>(rhs)); }


///@} logical_or_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup logical_or_grp
///@{
/**
* @brief shortname for @ref logical_or(Rhs&&)
**/
template<class Rhs>
inline constexpr auto lor(Rhs&& rhs) /** @cond */ ->
decltype(logical_or(std::forward<Rhs>(rhs))) /** @endcond */
{ return logical_or(std::forward<Rhs>(rhs)); }

/**
* @brief shortname for @ref logical_or()
**/
template<class Rhs>
inline constexpr auto lor() /** @cond */ ->
decltype(logical_or()) /** @endcond */
{ return logical_or(); }


///@} logical_and_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp

#endif // DFPTL_ALU_LOGICAL_OR_HPP
