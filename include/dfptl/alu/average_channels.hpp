/**
* @file alu/sum_channels.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_AVERAGE_CHANNELS_HPP
#define DFPTL_ALU_AVERAGE_CHANNELS_HPP


#include "dfptl/alu/detail/average_channels_node.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup average_channels_grp "average-channels" node
* @brief Performs average of channels samples reaching its SINGLE input.
*
* A @ref average_channels_grp performs an element-wise average of all channels samples on its 1st input
*
* The output stream is identical to input one except that its channel count is worth one and its frame length is divided by the input channel count.
*
* **Node interface**
* @startuml
* rectangle "o(n) = (i(n,0)+...+i(n,j)+...+i(n,k))/k | 0<=j<k (k as channel count" <<average_channels()>> as average_channels
* interface "input-0: 1|1|1 1|2|3 2|2|5 ...i(n,k)" <<INPUT>> as in
* interface "output: 1 2 3 ...o(n,k)" <<operation result>> as out
* in -down-> average_channels
* out <-up- average_channels
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | multi-channels @ref stream_grp "stream"
* "operation result" | **output**  | single-channel @ref stream_grp "stream" conveying the operation result
* @{
**/


/**
* @brief Creates a cell builder for a @ref average_channels_grp.
*
* **Usage example**
* @snippet alu_test.cpp average_channels usage example
**/
inline auto constexpr average_channels() /** @cond */ ->
decltype(make_placeholder<detail::average_channels_node>()) /** @endcond */
{ return make_placeholder<detail::average_channels_node>(); }


///@} average_channels_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{


using alu::average_channels;


} // namespace nodes
} // namespace shortname

///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_AVERAGE_CHANNELS_HPP
