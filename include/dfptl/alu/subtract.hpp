/**
* @file alu/subtract.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_SUBTRACT_HPP
#define DFPTL_ALU_SUBTRACT_HPP


#include "dfptl/core/apply.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup subtract_grp "subtract" node
* @brief Performs subtraction of its both input samples.
*
* A @ref subtract_grp performs an element-wise subtraction of sample on its 1st input
* with the one on its 2nd input and produces the resulting difference on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this arithmetic operator.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i0(n) - i1(n)" <<subtract()>> as sub
* interface "input-0: 4 5 2 ...i0(n)" <<lhs>> as in0
* interface "input-1: 2 3 1 ...i1(n)" <<rhs>> as in1
* interface "output: 2 2 1 ...o(n)" <<operation result>> as out
* in0 -down-> sub
* sub <- in1
* out <- sub
* @enduml
* * <tt>"lhs" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying left-hand side operand of the operation
* * <tt>"rhs" **input-1**            </tt> : @ref atomic_stream "atomic stream" conveying right-hand side operand of the operation
* * <tt>"operation result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a placeholder for a @ref subtract_grp.
*
* **Usage example**
* @code{.cpp}
* auto processing_branch = alu::subtract().build(
*       5,
*       2
* );
* assert(*processing_branch.begin() == 5-2);
* @endcode
*
* @sa sub()
**/
inline auto constexpr subtract() /** @cond */ ->
decltype(apply<upg::minus<>>()) /** @endcond */
{ return apply<upg::minus<>>(); }


/**
* @copybrief subtract()
*
* @note Alias for subtract().preset_inputs(std::forward<Rhs>(rhs))
*
* **Usage example**
* @code{.cpp}
* auto processing_branch = alu::subtract(2) <<= 5;
* assert(*processing_branch.begin() == 5-2);
* auto processing_branch2 = alu::subtract(processing_branch) <<= 3;
* assert(*processing_branch.begin() == (5-2)-3);
* @endcode

* @param rhs replaces the 2nd input stream (*Input-1*). This is the right hand operand of this arithmetic operation.
* It can have any type suitable with this operation against the given sample type conveyed by *Input-0* stream
* or a processing branch conveying sample suitable with this operation.
*
* @tparam Rhs is implicitly deduced from @c rhs argument.
*
* @sa subtract() sub()
**/
template<class Rhs>
inline auto constexpr subtract(Rhs&& rhs) /** @cond */ ->
decltype(apply<upg::minus<>>().preset_inputs(std::forward<Rhs>(rhs))) /** @endcond */
{ return apply<upg::minus<>>().preset_inputs(std::forward<Rhs>(rhs)); }


///@} subtract_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup subtract_grp
///@{
/**
* @brief shortname for @ref subtract(Rhs&&)
**/
template<class Rhs>
inline auto constexpr sub(Rhs&& rhs) /** @cond */ ->
decltype(subtract(std::forward<Rhs>(rhs))) /** @endcond */
{ return subtract(std::forward<Rhs>(rhs)); }


/**
* @brief shortname for @ref subtract()
**/
inline auto constexpr sub() /** @cond */ ->
decltype(subtract()) /** @endcond */
{ return subtract(); }


///@} subtract_grp
} // namespace nodes
} // namespace shortname


///@} alu_grp
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_SUBTRACT_HPP
