/**
* @file alu/detail/accumulate_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_DETAIL_ACCUMULATE_NODE_HPP
#define DFPTL_ALU_DETAIL_ACCUMULATE_NODE_HPP

#define DFP_ACCUMULATX accumulate
#include "dfptl/alu/detail/accumulat_verifier.hxx"
#undef DFP_ACCUMULATX
#include "dfptl/core/node.hpp"

#include <utility>


namespace dfp
{
namespace alu
{
namespace detail
{


template<class Context, class Initial, class CumulOperation>
class accumulate_node
    :   accumulate_class_verifier<Context, Initial, CumulOperation>,
        constructor_parameters_type_check<Context, Initial, CumulOperation>,
        public node<
            Context,
            atomic_stream_specifier<std::remove_cvref_t<std::invoke_result_t<
                CumulOperation, Initial, sample_t<output_stream_specifier_t<context_upstream_t<0, Context>>>
            >>>,
            accumulate_node<Context, Initial, CumulOperation>
        >,
        core::detail::ebco_member<CumulOperation>
{
    typedef constructor_parameters_type_check<Context, Initial, CumulOperation> constructor_check_type;
    typedef node<Context,
        atomic_stream_specifier<std::remove_cvref_t<std::invoke_result_t<
            CumulOperation, Initial, sample_t<output_stream_specifier_t<context_upstream_t<0, Context>>>
        >>>,
        accumulate_node> node_type;
    typedef core::detail::ebco_member<CumulOperation> cumul_operation_member;

public:
    template <class InitialArg, class CumulOperationArg>
    constexpr accumulate_node(Context&& context, InitialArg&& initial, CumulOperationArg&& cumul_op_arg)
     :  constructor_check_type(accumulate_ctor_verifier<InitialArg, CumulOperationArg>{}),
        node_type(std::forward<Context>(context)),
        cumul_operation_member(std::forward<CumulOperationArg>(cumul_op_arg)),
        cumul_result_(std::forward<InitialArg>(initial))
    {}

    DFP_MUTABLE_CONSTEXPR output_sample_t<node_type> process_sample(input_sample_t<node_type> const& input)
    { return cumul_result_ = cumul_operation_member::get()(cumul_result_, input); }

private:
    output_sample_t<node_type> cumul_result_;
}; // class accumulate_node


} // namespace detail
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_DETAIL_ACCUMULATE_NODE_HPP
