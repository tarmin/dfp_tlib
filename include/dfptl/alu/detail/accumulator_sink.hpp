/**
* @file alu/detail/accumulator_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_DETAIL_ACCUMULATOR_SINK_HPP
#define DFPTL_ALU_DETAIL_ACCUMULATOR_SINK_HPP


#define DFP_ACCUMULATX accumulator
#include "dfptl/alu/detail/accumulat_verifier.hxx"
#undef DFP_ACCUMULATX
#include "dfptl/core/sink.hpp"
#include "dfptl/core/detail/ebco_member.hpp"

#include <numeric>


namespace dfp
{
namespace alu
{
namespace detail
{


template<class Context, class Initial, class CumulOperation>
class accumulator_sink
 :  accumulator_class_verifier<Context, Initial, CumulOperation>,
    constructor_parameters_type_check<Context, Initial, CumulOperation>,
    public  sink<Context, run_for, accumulator_sink<Context, Initial, CumulOperation>>,
    core::detail::ebco_member<CumulOperation>
{
    typedef constructor_parameters_type_check<Context, Initial, CumulOperation> constructor_check_type;
    typedef sink<Context, run_for, accumulator_sink> sink_type;
    typedef core::detail::ebco_member<CumulOperation> cumul_op_member;
    typedef std::remove_cvref_t<std::invoke_result_t<CumulOperation, Initial, input_sample_t<sink_type>>> cumul_type;

public:
    template<class InitialArg, class CumulOperationArg>
    constexpr accumulator_sink(Context&& context, std::size_t samples_count, InitialArg&& initial, CumulOperationArg&& cumul_op)
     :  constructor_check_type(accumulator_ctor_verifier<InitialArg, CumulOperationArg>{}),
        sink_type(std::forward<Context>(context), samples_count),
        cumul_op_member(std::forward<CumulOperationArg>(cumul_op)),
        cumul_(std::forward<InitialArg>(initial))
    {}

#if DFP_CONSTRAINTS_SUPPORT
    template<class T = sink_type> requires input_stream_specifier_t<T>::IS_ATOMIC
#else
    template<class T = sink_type, DFP_F_REQUIRES((input_stream_specifier_t<T>::IS_ATOMIC))>
#endif
    void release_sample(input_sample_t<sink_type> const& in)
    { cumul_ = cumul_op_member::get()(cumul_, in); }

#if DFP_CONSTRAINTS_SUPPORT
    template<class T = sink_type> requires (!input_stream_specifier_t<T>::IS_ATOMIC)
#else
    template<class T = sink_type, DFP_F_REQUIRES((!input_stream_specifier_t<T>::IS_ATOMIC))>
#endif
    void release_frame(input_frame_t<sink_type> const& in)
    { cumul_ = std::accumulate(in.cbegin(), in.cend(), cumul_, cumul_op_member::get()); }

    DFP_MUTABLE_CONSTEXPR cumul_type const& run()
    { sink_type::run(); return cumul_; }

private:
    cumul_type cumul_; 
}; // class accumulator_sink


} // namespace detail
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_DETAIL_ACCUMULATOR_SINK_HPP
