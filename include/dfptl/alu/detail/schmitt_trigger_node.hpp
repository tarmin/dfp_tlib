/**
* @file alu/detail/schmitt_trigger_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_DETAIL_SCHMITT_TRIGGER_NODE_HPP
#define DFPTL_ALU_DETAIL_SCHMITT_TRIGGER_NODE_HPP


#include <dfptl/core/node.hpp>

#include <utility>
#include <stl/type_traits.hpp>
#include <boost/type_traits/has_greater_equal.hpp>
#include <boost/type_traits/has_less_equal.hpp>


#define DFP_ALU_SCHMITT_TRIGGER alu::schmitt_trigger


namespace dfp
{
namespace alu
{
namespace detail
{
//TODO Provides arugmetn to specify initial state ? (or generic mechanism ?)


template<class Input>
class schmitt_trigger_node
 :  public  node<Input, atomic_stream_specifier<bool>, schmitt_trigger_node<Input>>
{
    static_assert(std::tuple_size<Input>{} == 2, DFP_INVALID_INPUT_COUNT(DFP_ALU_SCHMITT_TRIGGER, 2));

    // alias for the base type
    typedef node<Input, atomic_stream_specifier<bool>, schmitt_trigger_node> node_type;

    static_assert(
        boost::has_greater_equal<input_sample_t<node_type, 0>, input_sample_t<node_type, 1>>{},
        "both sample types of " DFP_STRINGIFY(DFP_ALU_SCHMITT_TRIGGER) " input streams shall be comparable with >="
    );

    static_assert(
        boost::has_less_equal<input_sample_t<node_type, 0>, input_sample_t<node_type, 1>>{},
        "both sample types of " DFP_STRINGIFY(DFP_ALU_SCHMITT_TRIGGER) " input streams shall be comparable with <="
    );

public:
    constexpr schmitt_trigger_node(Input&& input) : node_type(std::forward<Input>(input)), state_(false)
    {}

    DFP_MUTABLE_CONSTEXPR bool process_sample(
        input_sample_t<node_type, 0> const& input,
        input_sample_t<node_type, 1> const& threshold
    )
    {
        if (input >= threshold)
            state_ = true;

        if (input <= -threshold)
            state_ = false;

        return state_;
    }

private:
    bool state_;
}; // class schmitt_trigger_node


} // namespace detail
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_DETAIL_SCHMITT_TRIGGER_NODE_HPP
