/**
* @file alu/detail/average_channels_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_AVERAGE_CHANNELS_NODE_HPP
#define DFPTL_ALU_AVERAGE_CHANNELS_NODE_HPP


#include "dfptl/core/node.hpp"


namespace dfp
{
namespace alu
{
namespace detail
{


#define DFP_ALU_AVERAGE_CHANNELS alu::average_channels


template<class Input, class = void>
class average_channels_node
{ static_assert(always_false<average_channels_node>{}, DFP_UNEXPECTED_ERROR); };


struct average_channels_helper
{
    template<class Node, DFP_F_REQUIRES((std::is_floating_point<output_sample_t<Node>>{}))>
    static DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) output_frame_t<Node> average_channels(Node const& node, input_frame_t<Node> const& input)
    {
        output_frame_t<Node> output = node.output_stream_specifier().get_frame_builder().build();
        auto const channel_count = node.input_stream_specifier().get_channel_count();
        auto it = input.cbegin();

        for(output_sample_t<Node>& o : output)
        {
            for(std::size_t ch = 0 ; ch < channel_count ; ch++)
                o += *it++;
            o /= channel_count;
        }

        return output;
    }

    template<class Node, DFP_F_REQUIRES((!std::is_floating_point<output_sample_t<Node>>{}))>
    static DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) output_frame_t<Node> average_channels(Node const& node, input_frame_t<Node> const& input)
    {
        //TODO: provide optimized implementation for non floating type
        output_frame_t<Node> output = node.output_stream_specifier().get_frame_builder().build();
        auto const channel_count = node.input_stream_specifier().get_channel_count();
        auto it = input.cbegin();

        for(output_sample_t<Node>& o : output)
        {
            double o_as_double = 0;

            for(std::size_t ch = 0 ; ch < channel_count ; ch++)
                o_as_double += (double) *it++;
            o_as_double /= channel_count;
            o = (output_sample_t<Node>) o_as_double;
        }

        return output;
    }
}; // average_channels_helper


template<class Input>
class average_channels_node<Input, DFP_C_REQUIRES((
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_FRAME_LENGTH_STATIC
    &&
    output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_CHANNEL_COUNT_STATIC
))>
 :  public  node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_extent_t<
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::FRAME_EXTENT
                /
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::CHANNEL_EXTENT
            >
            ::template set_channel_extent_t<1>,
        average_channels_node<Input>
    >
{
    typedef node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_extent_t<
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::FRAME_EXTENT
                /
                output_stream_specifier_t<std::tuple_element_t<0, Input>>::CHANNEL_EXTENT
            >
            ::template set_channel_extent_t<1>,
        average_channels_node<Input>
    > node_type;

public:
    using node_type::node_type;

    DFP_IF(DFP_CPP14_SUPPORT)(constexpr,) output_frame_t<node_type> process_frame(input_frame_t<node_type> const& input_frame) const
    { return average_channels_helper::average_channels(*this, input_frame); }
}; // class average_channels_node<IS_FRAME_LENGTH_STATIC && IS_CHANNEL_COUNT_STATIC>


template<class Input>
class average_channels_node<Input, DFP_C_REQUIRES((
    !output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_FRAME_LENGTH_STATIC
    ||
    !output_stream_specifier_t<std::tuple_element_t<0, Input>>::IS_CHANNEL_COUNT_STATIC
))>
 :  public  node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_extent_t<DYNAMIC_EXTENT>
            ::template set_channel_extent_t<1>,
        average_channels_node<Input>
    >
{
    typedef node<
        Input,
        typename output_stream_specifier_t<std::tuple_element_t<0, Input>>
            ::template set_frame_extent_t<DYNAMIC_EXTENT>
            ::template set_channel_extent_t<1>,
        average_channels_node<Input>
    > node_type;

public:
    average_channels_node(Input&& input)
     :  node_type(
            std::forward<Input>(input),
            output_stream_specifier_t<node_type>{}
                .set_frame_length(
                    !node_type::output_stream_specifier_of(input).is_channel_count_specified()
                    ||
                    !node_type::output_stream_specifier_of(input).is_frame_length_specified()
                    ? DYNAMIC_EXTENT : node_type::output_stream_specifier_of(input).get_frame_length() / node_type::output_stream_specifier_of(input).get_channel_count()
                )
        )
    {}

    void initialize()
    { // handle possibly late definition of the input stream specifier
        if (this->output_stream_specifier().get_frame_length() != DYNAMIC_EXTENT)
            return;

        this->set_output_stream_specifier(this->output_stream_specifier()
            .set_frame_length(this->input_stream_specifier().get_frame_length() / this->input_stream_specifier().get_channel_count())
        );
    }

    output_frame_t<node_type> process_frame(input_frame_t<node_type> const& input_frame) const
    { return average_channels_helper::average_channels(*this, input_frame); }
}; // class average_channels_node<!IS_FRAME_LENGTH_STATIC || !IS_CHANNEL_COUNT_STATIC>


} // namespace detail
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_AVERAGE_CHANNELS_NODE_HPP
