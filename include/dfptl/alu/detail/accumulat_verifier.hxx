/**
* @file alu/detail/accumulat_ctor_verifier.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2024 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


// skip this template fiel if DFP_ACCUMULATX not defiend
#ifdef DFP_ACCUMULATX

#include <dfptl/core/cplusplus.hpp>

namespace dfp
{
namespace alu
{
namespace detail
{

#define DFP_ALU_ACCUMULATX alu:: DFP_ACCUMULATX

template<class Context, class Initial, class CumulOperation>
struct DFP_CONCAT(DFP_ACCUMULATX,_class_verifier)
{
    static_assert(Context::UPSTREAM_COUNT == 1, DFP_INVALID_INPUT_COUNT(DFP_ALU_ACCUMULATX, 1));

    typedef output_sample_t<typename context_upstream<0, Context>::type> input_sample_type;

    // note for developper: one might thing that an unary function object might be more accurate than an binary function for the CumulOperation.
    // Working with a binary function is actually a subtle design decision to avoid need for storage of the cumul result in the CumulOperation and so
    // allow data-less class function as lambda.
    static_assert(
        std::is_invocable<CumulOperation, Initial, input_sample_type>{},
        "given 'CumulOperation' type argument of the " DFP_STRINGIFY(DFP_ALU_ACCUMULATX) " shall be a binary function accepting 'Initial' value as 1st parameter and sample type of input stream as 2nd parameter"
    );

    typedef std::remove_cvref_t<std::invoke_result_t<CumulOperation, Initial, input_sample_type>> cumul_type;

    static_assert(
        !std::is_void<cumul_type>{},
        "given 'CumulOperation' type argument of the " DFP_STRINGIFY(DFP_ALU_ACCUMULATX) " shall return a (non-void) result"
    );


    static_assert(
        std::is_assignable<cumul_type&, Initial>{},
        "given 'Initial' type argument of the " DFP_STRINGIFY(DFP_ALU_ACCUMULATX) " sink shall be assignable to cumul result"
    );
}; // struct DFP_CONCAT(DFP_ACCUMULATX,_class_verifier)


template<class InitialArg, class CumulOperationArg>
struct DFP_CONCAT(DFP_ACCUMULATX,_ctor_verifier)
{
    template<class Context, class Initial, class CumulOperation>
    struct check_type
    {
//TODO        static_assert(std::is_constructible<Initial, InitialArg>{}, DFP_UNEXPECTED_ERROR);
        static_assert(std::is_constructible<std::remove_cvref_t<Initial>, InitialArg>{}, DFP_UNEXPECTED_ERROR);
        static_assert(std::is_constructible<CumulOperation, CumulOperationArg>{}, DFP_UNEXPECTED_ERROR);
    }; // struct check_type
}; // struct DFP_CONCAT(DFP_ACCUMULATX,_ctor_verifier)

} // namespace detail
} // namespace alu
} // namespace dfp


#endif // DFP_ACCUMULATX