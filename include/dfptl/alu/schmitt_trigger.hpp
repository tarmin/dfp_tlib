/**
* @file alu/schmitt_trigger.hpp
**/
/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_SCHMITT_TRIGGER_HPP
#define DFPTL_ALU_SCHMITT_TRIGGER_HPP


#include "dfptl/alu/detail/schmitt_trigger_node.hpp"
#include "dfptl/core/placeholder.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup schmitt_trigger_grp Schmitt trigger node
* @brief Implements a comparator with hysteresis commonly known as ["Schmitt trigger"](https://en.wikipedia.org/wiki/Schmitt_trigger)
*
* Whose transfer function of the processing node follows the following graph:
* ![Hysteresis_sharp_curve](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Hysteresis_sharp_curve.svg/270px-Hysteresis_sharp_curve.svg.png) 
*
* The "Schmitt trigger" node compares each sample reaching its 1st input with sample on its 2nd input (representing the **T**-hreshold value).
*
* **Node interface**
* @startuml
* rectangle "o(n) = comparison boolean status ..." <<schmitt_trigger()>> as op
* interface "input-0: 0 1 2 3 1 0 -2 -3 ...i0(n)" <<INPUT>> as input
* interface "input-1: 2 2 2 2 2 2  2  2 ...i1(n)" <<THRESHOLD>> as threshold
* interface "output:  F F T T T T  F  F ...o(n)" <<operation result>> as out
* input -down-> op
* threshold -down-> op
* out <-up- op
* @enduml
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying the sample to compare
* THRESHOLD          | **input-1** | @ref atomic_stream "atomic stream" conveying the threshold value
* "operation result" | **output**  | the @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/

/**
* @brief Creates a cell builder for @ref schmitt_trigger_grp
*
* **Usage example**
* @snippet alu_test.cpp schmitt_trigger usage example
**/
constexpr auto schmitt_trigger() /** @cond */ ->
decltype(make_placeholder<std::tuple<>, detail::schmitt_trigger_node>()) /** @endcond */
{ return make_placeholder<std::tuple<>, detail::schmitt_trigger_node>(); }


/**
* @brief Alias for `schmitt_trigger().preset_inputs(std::forward<Threshold>(threshold)`
*
* @param threshold replaces the 2nd input stream (*Input-1*).
* This is the value of the switching threshold (**T** in the graph above)
*
* @tparam Threshold is implicitly deduced from @c switch_threshold and threshold parameter. This type shall be
* suitable for creation of a stream whose samples are comparable against the samples conveyed by *Input-0* stream.
*
* **Usage example**
* @snippet alu_test.cpp schmitt_trigger usage example with preset
**/
template<class Threshold>
constexpr auto schmitt_trigger(Threshold&& threshold) /** @cond */ ->
decltype(schmitt_trigger().preset_inputs(std::forward<Threshold>(threshold))) /** @endcond */
{ return schmitt_trigger().preset_inputs(std::forward<Threshold>(threshold)); }


///@}
///@}
} // namespace nodes
} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_SCHMITT_TRIGGER_HPP
