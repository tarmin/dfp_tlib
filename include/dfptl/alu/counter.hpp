/**
* @file alu/counter.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_COUNTER_HPP
#define DFPTL_ALU_COUNTER_HPP


#include "dfptl/core/const_sample.hpp"
#include "dfptl/core/default_settings.hpp"
#include "dfptl/alu/accumulate.hpp"
#include "dfptl/alu/subtract.hpp"


namespace dfp
{
namespace alu
{
inline namespace sources
{
///@addtogroup alu_grp
///@{
/**
* @defgroup counter_grp "counter" source
* @brief Produces incremental values flow.
*
* A @ref counter_grp produces new values incrementing previous one with the given step.
*
* **Source interface**
* @startuml
* rectangle "o(n) = o(n-1) + step | o(0) = offset" <<counter(step, offset)>> as src
* interface "output: offset (offset+step) (offet+2*step) ...o(n)" <<count result>> as out
* out <-down- src
* @enduml
* stream name        | location    | description
* -------------------|-------------|------------
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the incremental values samples.
* @{
**/


/**
* @brief Creates a builder for a @ref counter_grp.
*
* @param step is the progress step of the counter
* @param initial is the first value returned by the counter
*
* @tparam OutSample is the type of the output sample. It can be implicitly deduced from @c step or @c initial parameters if they are provided.
*
* @snippet alu_test.cpp counter usage example
**/
template<class OutSample = default_sample_t<>, DFP_F_REQUIRES((!is_type_meta<OutSample>{}))>
inline auto constexpr counter(OutSample const& step = 1, OutSample const& initial = OutSample{})
#if not DFP_CPP14_SUPPORT
->
decltype(accumulate(initial-step) <<= const_sample(step))
#endif
{ return accumulate(initial-step) <<= const_sample(step); }


/**
* @brief Creates a builder for a @ref counter_grp.
*
* @param type_meta the @ref type_meta "type metadata" value helping to specify the output sample type.
* @param step is the progress step of the counter
* @param initial is the first value returned by the counter
*
* @tparam OutSampleTypeMeta is implictly deduced from @c type_meta parameter
* @snippet alu_test.cpp counter type_meta usage example
**/
template<class OutSampleTypeMeta, DFP_F_REQUIRES((is_type_meta<OutSampleTypeMeta>{}))>
inline auto constexpr counter(OutSampleTypeMeta type_meta, typename OutSampleTypeMeta::type const& step = 1, typename OutSampleTypeMeta::type const& initial = 0)
#if not DFP_CPP14_SUPPORT
->
decltype(counter<typename OutSampleTypeMeta::type>(step, initial))
#endif
{ return counter<typename OutSampleTypeMeta::type>(step, initial); }


///@} alu_grp
///@} counterOut_grp
} // namespace sources



namespace shortname
{
inline namespace sources
{
using alu::sources::counter;
} // namespace sources
} // namespace shortname


} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_COUNTER_HPP
