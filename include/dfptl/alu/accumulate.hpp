/**
* @file alu/accumulate.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALU_ACCUMULATE_HPP
#define DFPTL_ALU_ACCUMULATE_HPP


#include "dfptl/alu/detail/accumulate_node.hpp"
#include "dfptl/core/placeholder.hpp"

#include "stl/functional.hpp"


namespace dfp
{
namespace alu
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
/**
* @defgroup accumulate_grp "accumulate" node
* @brief Accumulates all samples from its single input.
*
* An @ref accumulate_grp accumulates samples on its SINGLE input.
* When `CumulOperation` is provided it serves to accumulate elements otherwise `std::plus<>` is used by defaut.
* Sample resulting from cumulation of all past input samples is produced on the output at every new input sample arrival.
* The output sample type is same as the `CumulOperation` (or `std::plus<>`) result type.
*
* **Node interface**
* * with usage of `std::plus<>`
* @startuml
* rectangle "o(n) = o(n-1) + i0(n) | o(0) = initial + i(0)" <<accumulate(initial)>> as acc
* interface "input-0: 1 3 2 ...i0(n)" <<INPUT>> as in0
* interface "output: (initial+1) (initial+4) (initial+6) ...o(n)" <<operation result>> as out
* acc <-down- in0
* out <-down- acc
* @enduml
* * with custom `op`eration
* @startuml
* rectangle "o(n) = op(o(n-1), i0(n-1)) | o(0) = initial" <<accumulate(op, initial)>> as acc_
* interface "input-0: 1 3 2 ...i0(n)" <<INPUT>> as in0_
* interface "output: op(initial, i(0)) ...o(n)" <<operation result>> as out_
* acc_ <-down- in0_
* out_ <-down- acc_
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | any @ref stream_grp conveying the samples to accumulate
* "operation result" | **output**  | a @ref stream_grp conveying the cumulation result since creation of the node
* @{
**/


/**
* @brief Creates a builder for an @ref accumulate_grp accumulating samples reaching its input with the given `CumulOperation`
*
* @param initial is the initial value of the cumulation. Default value is @c 0. Any type for this parameter is acceptable
* as long as it is compatible as left operand of the `CumulOperation` with the sample type of the input stream.
* @param cumul_op is a bynary function to apply to accumulate previous cumulation result with the new input sample.
*
* @tparam CumulOperation can be implicity deduced from the @c cumul_op parameter.
* @tparam Initial can be implicity deduced from the @c initial parameter.
*
* **Usage example**
* @snippet alu_test.cpp accumulate with max usage example
*
* @sa shortname:: @ref shortname::acc() "acc(Initial&&, CumulOperation&&)"
**/
template<class CumulOperation = upg::plus<>, class Initial = int>
inline auto constexpr accumulate(Initial&& initial = Initial{}, CumulOperation&& cumul_op = CumulOperation{})
#if not DFP_CPP14_SUPPORT
->
decltype(make_placeholder<std::tuple<Initial, CumulOperation>, detail::accumulate_node>(std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)))
#endif
{ return make_placeholder<std::tuple<Initial, CumulOperation>, detail::accumulate_node>(std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)); }


///@} accumulate_grp
///@} alu_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup alu_grp
///@{
///@addtogroup accumulate_grp
///@{


/**
* @brief shortname for @ref accumulate(Initial&&, CumulOperation&&)
*
* **Usage example**
* @snippet alu_test.cpp acc usage example
**/
template<class CumulOperation = upg::plus<>, class Initial = int>
constexpr auto acc(Initial&& initial = Initial{}, CumulOperation&& cumul_op = CumulOperation{})
#if not DFP_CPP14_SUPPORT
->
decltype(accumulate(std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)))
#endif
{ return accumulate(std::forward<Initial>(initial), std::forward<CumulOperation>(cumul_op)); }


///@} accumulate_grp
///@} alu_grp
} // namespace nodes
} // namespace shortname


} // namespace alu
} // namespace dfp


#endif // DFPTL_ALU_ACCUMULATE_HPP
