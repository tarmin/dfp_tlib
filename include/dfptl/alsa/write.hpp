/**
* @file alsa/write.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALSA_WRITE_HPP
#define DFPTL_ALSA_WRITE_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/alsa/detail/write_node.hpp"


#include <zita-alsa-pcmi.h>


namespace dfp
{
namespace alsa
{
inline namespace nodes
{
///@defgroup alsa_grp ALSA I/O cells
///@addtogroup alsa_grp
///@{
/**
* @defgroup alsa_write_grp "ALSA-play" node
* @brief Plays to ALSA device sound signal from its single input @ref core::stream "stream".
*
* A @ref alsa_write_grp is a frame-based processing node which plays PCM-samples from its input stream to some ALSA device.
* Its input stream is forwarded to its output port without modification.
* Processing in this node are based on functions provided by the [zita-alsa-pcmi](https://directory.fsf.org/wiki/Zita-alsa-pcmi/) library.
*
* @note current limitations:
* * only mono-playback supported (input signal played equally on all device channels)
* * only one single ALSA cell can be inserted in a processing tree.
*
* @warning As per current implementation, the min acceptable frame rate at input of this node is 1 frame per second
* otherwise inner poll mechanism in zita-alsa-pcmi lib will fall in timeout.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n) | play i(n) to ALSA device" <<alsa::write(device)>> as write
* interface "input: ...[i(n-L) i(n-L+1) ... i(n)]" <<in>> as in
* interface "output: ...[i(n-L) i(n-L+1) ... i(n)]" <<operation result>> as out
* write <- in
* out <- write
* @enduml
* * <tt>"in" **input**              </tt> : @ref core::stream "stream" to play to ALSA device
* * <tt>"operation result" **output**</tt> : the same stream as the input one.
* @{
**/


/**
* @brief Creates a placeholder for a @ref alsa_write_grp.
*
* **Usage example**
* @snippet alsa_test.cpp alsa::write usage example
*
* @param alsa_device is the Alsa device to play on.
**/
inline auto write(Alsa_pcmi& alsa_device) /** @cond */ ->
decltype(make_placeholder<detail::write_node>(std::ref(alsa_device))) /** @endcond */
{ return make_placeholder<detail::write_node>(std::ref(alsa_device)); }


///@} alsa_write_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using alsa::write;
} // namespace nodes
} // namespace shortname


///@} alsa_grp
} // namespace alsa
} // namespace dfp


#endif // DFPTL_ALSA_WRITE_HPP
