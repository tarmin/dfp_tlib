/**
* @file alsa/detail/write_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALSA_DETAIL_WRITE_NODE_HPP
#define DFPTL_ALSA_DETAIL_WRITE_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/alsa/detail/read_source.hpp"

#include "stl/type_traits.hpp"

#include <zita-alsa-pcmi.h>


#define DFP_ALSA_WRITE alsa::write


namespace dfp
{
namespace alsa
{
namespace detail
{


template<class Input>
class write_node
 :  public  node<Input, decltype(output_stream_specifier_t<std::tuple_element_t<0, Input>>{}.set_tag_type(ALSA_CELL_TAG)), write_node<Input>>
{
    typedef node<Input, decltype(output_stream_specifier_t<std::tuple_element_t<0, Input>>{}.set_tag_type(ALSA_CELL_TAG)), write_node> node_type;

    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_ALSA_WRITE, 1));
    static_assert(
        !input_stream_t<node_type>::template has_tag<alsa_cell_tag>::value,
        "A processing tree can only ship one single ALSA cell for now"
    );
    static_assert(
        std::is_same<input_sample_t<node_type>, float>{}, DFP_INVALID_INPUT_SAMPLE(DFP_ALSA_WRITE, float, 0)
    );

public:
    typedef typename node_type::output_specifier_type output_specifier_type;

    /**
    * @brief Creates a new instance of write_node.
    *
    * @param input is the processing graph bound to input of this node.
    **/
    template<class T = output_specifier_type, DFP_F_REQUIRES((T::IS_STATIC))>
    write_node(Input&& input, Alsa_pcmi& alsa_device)
     :  node_type(std::forward<Input>(input)), alsa_device_(&alsa_device)
    {}

    template<class T = output_specifier_type, DFP_F_REQUIRES((!T::IS_STATIC))>
    write_node(Input&& input, Alsa_pcmi& alsa_device)
     :  node_type(std::forward<Input>(input), std::get<0>(input).output_stream_specifier().set_tag_type(ALSA_CELL_TAG)), alsa_device_(&alsa_device)
    {}

    write_node(write_node&& it) : node_type(std::move(it)), alsa_device_(it.alsa_device_)
    { it.alsa_device_ = nullptr; }

    // don't want different clients playing sound to the same device
    write_node(write_node const&) = delete;

    ~write_node()
    { if (alsa_device_ != nullptr) alsa_device_->pcm_stop(); }


    void initialize()
    {
        int const alsa_error = alsa_device_->pcm_start();

        if (alsa_error)
            throw new std::runtime_error("failed to start ALSA device for playback");
    }


    size_t_const<0> forward_frame(input_frame_t<node_type> const& input)
    {
        snd_pcm_sframes_t const available_room = alsa_device_->pcm_wait();
        std::size_t const input_frame_length = input.size();
        int const channel_count = alsa_device_->nplay();
        int const required_frame_length = input_frame_length;

        if ((available_room > 0) && (available_room >= required_frame_length))
        {
            int given = alsa_device_->play_init(required_frame_length);
            assert(given == required_frame_length); (void) given;
            for (int i = 0 ; i < channel_count ; i++)
                alsa_device_->play_chan(i, input.data(), input_frame_length);

            alsa_device_->play_done(given);
        }
        else
        { /*TODO: raise warning as pcm_wait() returned unexpectedly */ }

        return 0_c;
    }

private:
    Alsa_pcmi* alsa_device_;
}; // class write_node


} // namespace detail
} // namespace alsa
} // namespace dfp


#endif // DFPTL_ALSA_DETAIL_WRITE_NODE_HPP
