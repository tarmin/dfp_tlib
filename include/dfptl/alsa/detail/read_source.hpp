/**
* @file alsa/detail/read_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALSA_DETAIL_READ_SOURCE_HPP
#define DFPTL_ALSA_DETAIL_READ_SOURCE_HPP


#include "dfptl/core/source.hpp"

#include "stl/type_traits.hpp"
#include <vector>

#include <zita-alsa-pcmi.h>


#define DFP_ALSA_READ alsa::read


namespace dfp
{
namespace alsa
{
namespace detail
{


struct alsa_cell_tag : custom_tag {};
DFP_INLINE_CONSTEXPR type_meta<alsa_cell_tag> ALSA_CELL_TAG = type_meta<alsa_cell_tag>{};


template<class Context>
class read_source
//TODO: stream tag to be replaced by initialization storage
 :  public  source<Context, dynamic_stream_specifier<float>::set_tag_clue_t<alsa_cell_tag>, read_source<Context>>
{
    typedef source<Context, dynamic_stream_specifier<float>::set_tag_clue_t<alsa_cell_tag>, read_source> source_type;

public:
    read_source(Context const& context, Alsa_pcmi& alsa_device)
     :  source_type(context, typename source_type::output_specifier_type{}.set_frame_length(alsa_device.fsize() / alsa_device.ncapt())),
        alsa_device_(&alsa_device), frame_(alsa_device.fsize() / alsa_device.ncapt())
    {}

    void initialize()
    {
        int const alsa_error = alsa_device_->pcm_start();

        if (alsa_error)
            throw new std::runtime_error("failed to start ALSA device for capture");

        frame_.resize(frame_.capacity());
    }

    read_source(read_source&& src) : source_type(std::move(src)), frame_(std::move(src.frame_))
    {
        alsa_device_ = src.alsa_device_;

        src.alsa_device_ = nullptr;
    }

    // don't want for now different clients recording sound from the same device
    read_source(read_source const&) = delete; 

    ~read_source()
    { if (alsa_device_ != nullptr) alsa_device_->pcm_stop(); }


    output_frame_t<source_type> const& acquire_frame()
    {
        snd_pcm_sframes_t const available_samples = alsa_device_->pcm_wait();
        snd_pcm_sframes_t const frame_length = frame_.size();

        if ((available_samples > 0) && (available_samples >= (frame_length*alsa_device_->ncapt())))
        {
            int given = alsa_device_->capt_init(frame_length);
            assert(given == frame_length); (void) given;
            alsa_device_->capt_chan(0, frame_.data(), frame_length);
            alsa_device_->capt_done(given);
        }
        else
        { /*TODO: raise warning as pcm_wait() returned unexpectedly */ }

        return frame_;
    }

private:
    Alsa_pcmi* alsa_device_;
    std::vector<float> frame_;
}; // class read_source


} // namespace detail
} // namespace alsa
} // namespace dfp


#endif //DFPTL_ALSA_DETAIL_READ_SOURCE_HPP
