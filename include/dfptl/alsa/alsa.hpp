/**
* @file alsa/alsa.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALSA_ALSA_HPP
#define DFPTL_ALSA_ALSA_HPP


#include "dfptl/alsa/read.hpp"
#include "dfptl/alsa/write.hpp"


namespace dfp
{
/**
* @defgroup alsa_grp Sound device I/O cells
* @brief Defines Processing sources and nodes for handling [ALSA](https://alsa-project.org/) devices.
*
* @ref alsa_grp are realized by wrapping functions of the [zita-alsa-pcmi](https://directory.fsf.org/wiki/Zita-alsa-pcmi) library
*
* @note Processing cells defined in this namespace simply wrap functions provided by the [zita-alsa-pcmi](https://directory.fsf.org/wiki/Zita-alsa-pcmi) library
* which shall be available on your building host for your target. The @c bootstrap script can be used to install required dependency
* when building host and target is Ubuntu 16.04 (or higher revision)
*
* @{
*
* @defgroup alsa_detail implementation details
* @brief Implementation details and boilerplate for @ref alsa_grp
**/


/**
* @brief Contains items related to processing cells wrapping the ALSA functions.
**/
namespace alsa
{
/**
* @brief Set of helper functions for ALSA device handling nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for ALSA device handling sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief short name of functions in @ref alsa_grp namespace
**/
namespace shortname {}


///@} alsa_grp
} // namespace alsa
} // namespace dfp


#endif // DFPTL_ALSA_ALSA_HPP
