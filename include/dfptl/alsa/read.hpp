/**
* @file alsa/read.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_ALSA_READ_HPP
#define DFPTL_ALSA_READ_HPP


#include "dfptl/alsa/detail/read_source.hpp"
#include "dfptl/core/placeholder.hpp"

#include <zita-alsa-pcmi.h>


namespace dfp
{
namespace alsa
{
inline namespace sources
{
///@addtogroup alsa_grp
///@{
/**
* @defgroup alsa_read_grp "ALSA-capture" source
* @brief Captures from an ALSA device the sound signal and produces it on its output.
*
* A @ref alsa_read_grp is a frame-based processing source which captures PCM-samples from a given ALSA device.
* It then produces output stream of frames with the captured samples.
* Processing in this source are based on functions provided by the [zita-alsa-pcmi](https://directory.fsf.org/wiki/Zita-alsa-pcmi/) library.
*
* @note current limitations:
* * only support for 1st-channel recording (captured only 1st channel of the alsa device)
* * only one single ALSA cell can be inserted in a processing tree.
*
* * @warning As per current implementation, the min acceptable frame rate at output of this source is 1 frame per second
* otherwise inner poll mechanism in zita-alsa-pcmi lib will fall in timeout.
*
* **Source interface**
* @startuml
* rectangle "o(n) = frame captured from ALSA device" <<alsa::read(device)>> as read
* interface "output: ...[o(n-L) o(n-L+1) ... o(n)]" <<captured>> as out
* out <-up- read
* @enduml
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "captured"   | **output** | @ref stream_grp                | delivers frames captured from the ALSA-device
* @{
**/


/**
* @brief Creates a cell builder for a @ref alsa_read_grp.
*
* **Usage example**
* @snippet alsa_test.cpp alsa::read usage example
*
* @param alsa_device is the Alsa device to capture from.
**/
inline auto read(Alsa_pcmi& alsa_device) /** @cond */ ->
decltype(make_placeholder<detail::read_source>(std::ref(alsa_device))) /** @endcond */
{ return make_placeholder<detail::read_source>(std::ref(alsa_device)); }


///@} alsa_read_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
using alsa::read;
} // namespace sources
} // namespace shortname


///@} alsa_grp
} // namespace alsa
} // namespace dfp


#endif // DFPTL_ALSA_READ_HPP
