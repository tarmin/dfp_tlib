/**
* @file speex/audio_process.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_AUDIO_PROCESS_HPP
#define DFPTL_SPEEX_AUDIO_PROCESS_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/speex/detail/audio_process_node.hpp"


namespace dfp
{
namespace speex
{
inline namespace nodes
{
///@addtogroup speex_grp
///@{
/**
* @defgroup speex_audio_process_grp SPEEX audio processing node
* @brief Applies some audio processing due to SPEEX DSP library on its single input @ref core::stream "stream".
*
* A @ref speex_audio_process_grp can apply some audio processing (denoising, residual echo reduction) on its single input of `int16_t` frame
* and produces a @ref stream_grp of `int16_t` frames.
* Processing in this node is based on functions provided by the [speex](https://www.speex.org/) library.
*
* **Node interface**
* @startuml
* rectangle "[o(n)...] = process([i(n)...])" <<speex::audio_process_builder()...build()>> as audio_process
* interface "...[i(n) ...i(n+N-1)]" <<INPUT>> as in
* interface "...[o(n) ...o(n+N-1)]" <<"processed frame">> as out
* audio_process <-up- in
* out <-up- audio_process
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | a @ref stream_grp conveying frame to process
* "processed frame"  | **output**  | the @ref stream_grp of processed `int16_t` frames.
* @{
**/


/**
* @brief Defines default settings for SPEEX audio processing.
**/
struct audio_process_settings
{
    /**
    * @brief default value in dB of the maximal denoise attenuation
    **/
    constexpr static int DEFAULT_MAX_NOISE_ATTENUATION = -15;
    /**
    * @brief default value in dB of the maximal residual echo attenuation
    **/
    constexpr static int DEFAULT_RESIDUAL_ECHO_ATTENUATION = -40;
    /**
    * @brief default value in dB of the maximal residual echo attenuation in dual talk condition
    **/
    constexpr static int DEFAULT_DUALTALK_RESIDUAL_ECHO_ATTENUATION = -15;
};


/**
* @brief the class of helper for building of a @ref speex_audio_process_grp.
**/
template<bool HasSharedState>
struct audio_process_builder : audio_process_settings
{
private:
    union process_initializer
    {
        int sampling_rate_;
        SpeexPreprocessState* processing_state_;

        process_initializer(int sampling_rate) : sampling_rate_(sampling_rate) {}
        process_initializer(SpeexPreprocessState* state) : processing_state_(state) {}

        template<bool T = HasSharedState, DFP_F_REQUIRES((!T))>
        int get() const
        { return sampling_rate_; }

        template<bool T = HasSharedState, DFP_F_REQUIRES((T))>
        SpeexPreprocessState* get() const
        { return processing_state_; }
    } const initializer;
    int max_noise_attenuation_{ 0 };
    SpeexEchoState const* echo_state_{ nullptr };
    int max_residual_echo_attenuation_{ 0 };
    int max_dualtalk_residual_echo_attenuation_ { 0 };

public:
    /**
    * @brief Creates a builder (of builder) to create a @ref speex_audio_process_grp.
    *
    * @param sampling_rate is the sampling rate in Hz of the audio input signal.
    **/
    template<bool T = HasSharedState, DFP_F_REQUIRES((!T))>
    audio_process_builder(int sampling_rate) : initializer(sampling_rate)
    {}

    /**
    * @brief Creates a builder (of builder) to create a @ref speex_audio_process_grp given a SPEEX processing state.  
    *
    * @param processing_state a processing state managed out of the processing graph with help of `speex_preprocess_state_init` and `speex_preprocess_state_destroy`.
    **/
    template<bool T = HasSharedState, DFP_F_REQUIRES((T))>
    audio_process_builder(SpeexPreprocessState* processing_state) : initializer(processing_state)
    {}

    /**
    * @brief Enables the denoising processing
    *
    * @param max_noise_attenuation the max level in dB of noise attenuation. Value `0` disables denoising.
    * @return instance of builder with denoising enabled.
    **/
    audio_process_builder& enable_denoise(int max_noise_attenuation = DEFAULT_MAX_NOISE_ATTENUATION)
    {
        max_noise_attenuation_ = max_noise_attenuation;
        return *this;
    }

    /**
    * @brief Enables the residual echo reduction
    *
    * Residual echo reduction shall be placed downstream of a @ref aec_grp given sharing the same state variable of echo processing.
    *
    * @param echo_state state of the `aec` processing. Value `nullptr` disables residual echo reduction.
    * @param max_residual_echo_attenuation the max level in dB of echo attenuation.
    * @param max_dualtalk_residual_echo_attenuation the max level in dB of noise attenuation in dual talk condition.
    * @return instance of builder with residual echo attenuation configured.
    **/
    audio_process_builder& enable_residual_echo_reduction(
        SpeexEchoState const* echo_state,
        int max_residual_echo_attenuation = DEFAULT_RESIDUAL_ECHO_ATTENUATION,
        int max_dualtalk_residual_echo_attenuation = DEFAULT_DUALTALK_RESIDUAL_ECHO_ATTENUATION
    )
    {
        echo_state_ = echo_state;
        max_residual_echo_attenuation_ = max_residual_echo_attenuation;
        max_dualtalk_residual_echo_attenuation_ = max_dualtalk_residual_echo_attenuation;
        return *this;
    }

    /**
    * @brief Creates the builder of a SPEEX audio processing node
    * @return instance of builder of a SPEEX audio processing node.
    **/
    auto build()  /** @cond */ ->
    decltype(make_placeholder<std::tuple<std::bool_constant<HasSharedState>>, detail::audio_process_node>(
        initializer.get(), max_noise_attenuation_, echo_state_, max_residual_echo_attenuation_, max_dualtalk_residual_echo_attenuation_
    )) /** @endcond */
    { return make_placeholder<std::tuple<std::bool_constant<HasSharedState>>, detail::audio_process_node>(
        initializer.get(), max_noise_attenuation_, echo_state_, max_residual_echo_attenuation_, max_dualtalk_residual_echo_attenuation_
    ); }
};


/**
* @brief Creates an `audio_process_builder` given the sample rate of the input signal.
*
* **Usage example**
* @snippet speex_test.cpp speex::audio_process usage example
**/
audio_process_builder<false> make_audio_process_builder(int sampling_rate)
{ return audio_process_builder<false>(sampling_rate); }


/**
* @brief Creates an `audio_process_builder` given a shared processing state.
**/
audio_process_builder<true> make_audio_process_builder(SpeexPreprocessState* process_state)
{ return audio_process_builder<true>(process_state); }


/**
* @brief Creates a builder of denoising processing.
*
* This function is simply an alias for `audio_process_builder(sampling_rate).enable_denoise(max_denoise_attenuation).build()`
* It is convenient when the denoising processing is the only one of interest. However it would be suboptimal to apply it on a stream
* where another SPEEX processing is also required as it would imply consumption of double resources.
* In such a case the `make_audio_process_builder` helper shall be used.
*
* @param sampling_rate is the sampling rate in Hz of the audio input signal.
* @param max_noise_attenuation the max level in dB of noise attenuation. Value `0` disables denoising.
*
* **Usage example**
* @snippet speex_test.cpp speex::denoise usage example
**/
inline auto denoise(int sampling_rate, int max_denoise_attenuation = audio_process_settings::DEFAULT_MAX_NOISE_ATTENUATION) /** @cond */ ->
decltype(make_audio_process_builder(sampling_rate).enable_denoise(max_denoise_attenuation).build()) /** @endcond */
{ return make_audio_process_builder(sampling_rate).enable_denoise(max_denoise_attenuation).build(); }


/**
* @brief Creates a builder of processing for residual echo reduction.
*
* This function is simply an alias for `audio_process_builder(sampling_rate).enable_residual_echo_reduction(echo_state, max_residual_echo_attenuation).build()`
* It is convenient when the residual echo reduction is the only one of interest. However it would be suboptimal to apply it on a stream
* where another SPEEX processing is also required as it would imply consumption of double resources.
* In such a case the `make_audio_process_builder` helper shall be used.
*
* @param sampling_rate is the sampling rate in Hz of the audio input signal.
* @param echo_state state of the `aec` processing. Value `nullptr` disables residual echo reduction.
* @param max_residual_echo_attenuation the max level in dB of echo attenuation.
* @param max_dualtalk_residual_echo_attenuation the max level in dB of noise attenuation in dual talk condition.
*
* **Usage example**
* @snippet speex_test.cpp speex::reduce_residual_echo usage example
**/
inline auto reduce_residual_echo(int sampling_rate,
    SpeexEchoState const* echo_state,
    int max_residual_echo_attenuation = audio_process_settings::DEFAULT_RESIDUAL_ECHO_ATTENUATION,
    int max_dualtalk_residual_echo_attenuation = audio_process_settings::DEFAULT_DUALTALK_RESIDUAL_ECHO_ATTENUATION
) /** @cond */ ->
decltype(make_audio_process_builder(sampling_rate)
    .enable_residual_echo_reduction(echo_state, max_residual_echo_attenuation, max_dualtalk_residual_echo_attenuation)
    .build()) /** @endcond */
{ return make_audio_process_builder(sampling_rate)
    .enable_residual_echo_reduction(echo_state, max_residual_echo_attenuation, max_dualtalk_residual_echo_attenuation)
    .build(); }


///@} speex_vad_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using speex::denoise;
using speex::reduce_residual_echo;
} // namespace nodes
} // namespace shortname


///@} speex_grp
} // namespace speex
} // namespace dfp


#endif // DFPTL_SPEEX_AUDIO_PROCESS_HPP
