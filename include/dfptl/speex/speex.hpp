/**
* @file speex/speex.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_SPEEX_HPP
#define DFPTL_SPEEX_SPEEX_HPP


#include "dfptl/speex/aec.hpp"
#include "dfptl/speex/vad.hpp"
#include "dfptl/speex/audio_process.hpp"


namespace dfp
{
/**
* @defgroup speex_grp SPEEX processing cells
* @brief Defines Processing sources and nodes for handling of the [speex](https://www.speex.org/) codec.
*
* @ref speex_grp are realized by wrapping features brough by the [speex](https://www.speex.org/) codec.
*
* @note Processing cells defined in this namespace mainly wrap functions provided by the [speex](https://www.speex.org/) library
* which shall be available on your building host for your target. The @c bootstrap script can be used to install required dependency
* when building host and target is Ubuntu 16.04 (or higher revision)
*
* @{
*
* @defgroup speex_detail implementation details
* @brief Implementation details and boilerplate for @ref speex_grp
**/


/**
* @brief Contains items related to processing cells wrapping the speex functions.
**/
namespace speex
{
/**
* @brief Set of helper functions for speex codec handling nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for speex codec handling sources.
**/
inline namespace sources {}
namespace leaves = sources;

/**
* @brief short name of functions in @ref speex_grp namespace
**/
namespace shortname {}


///@} speex_grp
} // namespace speex
} // namespace dfp


#endif // DFPTL_SPEEX_SPEEX_HPP
