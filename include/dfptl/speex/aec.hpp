/**
* @file speex/aec.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_AEC_HPP
#define DFPTL_SPEEX_AEC_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/speex/detail/aec_node.hpp"


namespace dfp
{
namespace speex
{
inline namespace nodes
{
///@addtogroup speex_grp
///@{
/**
* @defgroup speex_aec_grp SPEEX "AEC" node
* @brief Cancels echo on the input audio signal given the echo reference signal.
*
* The processing of Acoustic Echo Cancellation applied by this node relies on the AEC function of the [speex](https://www.speex.org/) library.
* A SPEEX AEC node shall be fed by 2 separate input streams of **fixed-point** samples (`int16_t`).
*
* @note This node requires its both input streams to have **identical frame lengths**
*
* **Node interface**
* @startuml
* rectangle "[o(n) ... o(n+L-1)] | L as filter length" <<speex::aec(L)>> as aec
* interface "...[i(n) ...i(n+L-1)]" <<#0-AUDIO_COMPOSITE>> as polluted
* interface "...[r(n) ...r(n+L-1)]" <<#1-ECHO_REF>> as ref
* interface "...[o(n) ... o(n+L-1)]" <<"audio w/o echo">> as cleaned
* aec <-up- polluted
* aec <-up- ref
* cleaned <-up- aec
* @enduml
*
* stream name      | location    | description
* -----------------|-------------|------------
* AUDIO_COMPOSITE  | **input-0** | @ref stream_grp conveying the audio signal polluted with echo.
* ECHO_REF         | **input-1** | @ref stream_grp conveying the echo reference signal.
* "audio w/o echo" | **output**  | @ref stream_grp conveying the audio signal cleaned from echo.
*
* @todo add support for dynamic frame extent
* @{
**/


/**
* @brief Creates a cell builder for a @ref speex_aec_grp.
*
* @param filter_length length of the cancelation filter.
*
* **Usage example**
* @snippet speex_test.cpp speex::aec usage example
**/
inline auto aec(int filter_length) /** @cond */ ->
decltype(make_placeholder<std::tuple<std::false_type>, detail::aec_node>(filter_length)) /** @endcond */
{ return make_placeholder<std::tuple<std::false_type>, detail::aec_node>(filter_length); }


/**
* @brief Creates a cell builder for a @ref speex_aec_grp given the speex echo state.
*
* @param aec_state an echo state managed out of the processing graph with help of `speex_echo_state_init` and `speex_echo_state_destroy`.
*
* **Usage example**
* @snippet speex_test.cpp speex::reduce_residual_echo usage example
**/
inline auto aec(SpeexEchoState* aec_state) /** @cond */ ->
decltype(make_placeholder<std::tuple<std::true_type>, detail::aec_node>(aec_state)) /** @endcond */
{ return make_placeholder<std::tuple<std::true_type>, detail::aec_node>(aec_state); }


///@} speex_aec_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using speex::aec;
} // namespace nodes
} // namespace shortname


///@} speex_grp
} // namespace speex
} // namespace dfp


#endif // DFPTL_SPEEX_AEC_HPP
