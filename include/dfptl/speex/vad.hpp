/**
* @file speex/vad.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_VAD_HPP
#define DFPTL_SPEEX_VAD_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/speex/detail/vad_node.hpp"


namespace dfp
{
namespace speex
{
inline namespace nodes
{
///@addtogroup speex_grp
///@{
/**
* @defgroup speex_vad_grp SPEEX "VAD" node
* @brief Detects voice activity in its single input @ref core::stream "stream".
*
* A @ref speex_vad_grp detects voice activity in its input stream and produces a @ref atomic_stream "atomic stream" of boolean values
* to indicate presence of voice in incoming frame.
* Processing in this node is based on functions provided by the [speex](https://www.speex.org/) library.
*
* @note SPEEX VAD seems to have very poor accuracy. Consider to work with @ref fvad_grp instead for voice activity detection.
*
* **Node interface**
* @startuml
* rectangle "o(n) = ([i(n) ...i(n+N-1)] is voice frame)" <<speex::vad()>> as vad
* interface "...[i(n) ...i(n+N-1)]" <<INPUT>> as in
* interface "...o(n)" <<"detection status">> as out
* vad <- in
* out <- vad
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | a @ref stream_grp conveying frame to to analyze for voice detection
* "detection status" | **output**  | the @ref atomic_stream "atomic stream" of boolean indicating status of voice detection
* @{
**/


/**
* @brief Creates a placeholder for a @ref speex_vad_grp.
*
* @param sampling_rate is the sampling rate in Hz of the audio input signal.
*
* **Usage example**
* @snippet speex_test.cpp speex::vad usage example
**/
inline auto vad(int sampling_rate) /** @cond */ ->
decltype(make_placeholder<detail::vad_node>(sampling_rate)) /** @endcond */
{ return make_placeholder<detail::vad_node>(sampling_rate); }


///@} speex_vad_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using speex::vad;
} // namespace nodes
} // namespace shortname


///@} speex_grp
} // namespace speex
} // namespace dfp


#endif // DFPTL_SPEEX_VAD_HPP
