/**
* @file speex/detail/aec_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_DETAIL_AEC_NODE_HPP
#define DFPTL_SPEEX_DETAIL_AEC_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/exception.hpp"

#include "stl/experimental/array.hpp"

#include <speex/speex_echo.h>


#define DFP_SPEEX_AEC speex::aec


namespace dfp
{
namespace speex
{
namespace detail
{
///@addtogroup speex
///@{
///@addtogroup speex_detail
///@{


DFP_INLINE_CONSTEXPR auto AEC_MIC_INPUT_INDEX      = 0_c;
DFP_INLINE_CONSTEXPR auto AEC_ECHOREF_INPUT_INDEX   = 1_c;


template<class Input, int Index>
struct aec_input
{
    static_assert(std::tuple_size<Input>{} == 2, DFP_INVALID_INPUT_COUNT(DFP_SPEEX_AEC, 2));

    typedef std::tuple_element_t<Index, Input> type;
};

template<class Input, int Index>
using aec_input_t = typename aec_input<Input, Index>::type;

template<class Input>
using aec_mic_input_t       = aec_input_t<Input, AEC_MIC_INPUT_INDEX>;

template<class Input>
using aec_echoref_input_t   = aec_input_t<Input, AEC_ECHOREF_INPUT_INDEX>;


template<class Input, class = void>
class aec_output_specifier
{
public:
    typedef dynamic_stream_specifier<int16_t>
        ::template set_channel_extent_t<1> // whatever is the input channel_extent the AEC only supports single channel
        //TODO check that channel extent is 1 if not dynamic
        type;
}; // class aec_output_specifier<...>


template<class Input>
class aec_output_specifier<Input, DFP_C_REQUIRES((
    output_stream_specifier_t<aec_mic_input_t<Input>>::IS_FRAME_LENGTH_STATIC
    ||
    output_stream_specifier_t<aec_echoref_input_t<Input>>::IS_FRAME_LENGTH_STATIC
))>
{
    typedef output_stream_specifier_t<aec_mic_input_t<Input>> mic_specifier;
    typedef output_stream_specifier_t<aec_echoref_input_t<Input>> echoref_specifier;

    // in case both input stream specifiers have static extent it shall be identical 
    static_assert(
        (mic_specifier::IS_FRAME_LENGTH_STATIC xor echoref_specifier::IS_FRAME_LENGTH_STATIC)
        ||
        (mic_specifier::FRAME_EXTENT == echoref_specifier::FRAME_EXTENT),
        DFP_INVALID_INPUT_FRAME_EXTENT(DFP_SPEEX_AEC, to be identical, 0 && 1)
    );

    typedef std::conditional_t<mic_specifier::IS_FRAME_LENGTH_STATIC, mic_specifier, echoref_specifier> input_static_specifier;

public:
    typedef typename input_static_specifier
        //removing possible cv qualifier on input sample type
        ::template set_sample_clue_t<std::remove_cv_t<typename input_static_specifier::sample_clue>>
        // whatever is the input channel_extent the AEC only supports single channel
        ::template set_channel_extent_t<1>
        //TODO check that channel extent is 1 if not dynamic
    type;
}; // class aec_output_specifier<... one of the input provides a constant-flow stream ...>

template<class Input>
using aec_output_specifier_t = typename aec_output_specifier<Input>::type;

template<class Inputs, class Derived>
class aec_node_base;


template<class Inputs, class HAS_SHARED_STATE>
class aec_node
 :  public  aec_node_base<Inputs, aec_node<Inputs, std::false_type>>
{
    typedef aec_node_base<Inputs, aec_node> base;
    friend base;

public:
    using base::base;

    /**
    * @brief Creates a new instance of aec_node.
    *
    * @param inputs is tuple of processing cells bound to the input port of this node.
    **/
    aec_node(Inputs&& inputs, int filter_length)
     :  base(std::forward<Inputs>(inputs)),
        state_(nullptr), filter_length_(filter_length)
    {}

    template<class T = base, DFP_F_REQUIRES((std::is_copy_constructible<T>{}))>
    aec_node(aec_node const& it)
    :   base(it), state_(it.state_), filter_length_(it.filter_length_)
    {
        if (it.state_ != nullptr)
            throw cell_error(DFP_STRINGIFY(DFP_SPEEX_AEC), "echo state has already been initialized so node is not copyable anymore");
    }

    aec_node(aec_node&& it) : base(std::move(it)), state_(it.state_), filter_length_(it.filter_length_)
    { it.state_ = nullptr; }

    ~aec_node()
    { if(state_ != nullptr) speex_echo_state_destroy(state_); }

    void initialize()
    {
        base::initialize();

        state_ = speex_echo_state_init(this->output_stream_specifier().get_frame_length(), filter_length_);
    }

private:
    SpeexEchoState* get_state()
    { return state_; }

    SpeexEchoState* state_;
    int filter_length_;
};


template<class Inputs>
class aec_node<Inputs, std::true_type>
 :  public  aec_node_base<Inputs, aec_node<Inputs, std::true_type>>
{
    typedef aec_node_base<Inputs, aec_node> base;
    friend base;

public:
    using base::base;

    aec_node(Inputs&& inputs, SpeexEchoState* aec_state)
     :  base(std::forward<Inputs>(inputs)), state_(aec_state)
    {}

private:
    SpeexEchoState* get_state()
    { return state_; }

    SpeexEchoState* state_;
};


template<class Inputs, class Derived>
class aec_node_base
 :  public  node<Inputs, aec_output_specifier_t<Inputs>, Derived>
{
    typedef output_stream_specifier_t<aec_mic_input_t<Inputs>> mic_specifier;
    typedef output_stream_specifier_t<aec_echoref_input_t<Inputs>> echoref_specifier;

    // alias for the base type
    typedef node<Inputs, aec_output_specifier_t<Inputs>, Derived> node_type;

    static_assert(
        std::is_same<std::remove_cv_t<sample_t<mic_specifier>>, int16_t>{},
        DFP_INVALID_INPUT_SAMPLE(DFP_SPEEX_AEC, int16_t, 0)
    );
    static_assert(
        std::is_same<std::remove_cv_t<sample_t<echoref_specifier>>, int16_t>{},
        DFP_INVALID_INPUT_SAMPLE(DFP_SPEEX_AEC, int16_t, 1)
    );

public:
    typedef typename node_type::output_specifier_type output_specifier_type;

    template<class S = output_specifier_type, class T = mic_specifier, class U = echoref_specifier, DFP_F_REQUIRES((
        T::IS_FRAME_LENGTH_STATIC && U::IS_FRAME_LENGTH_STATIC && S::IS_STATIC
    ))>
    aec_node_base(Inputs&& inputs)
     :  node_type(std::forward<Inputs>(inputs))
    {}

    template<class T = mic_specifier, class U = echoref_specifier, DFP_F_REQUIRES((
        T::IS_FRAME_LENGTH_STATIC && !U::IS_FRAME_LENGTH_STATIC
    ))>
    aec_node_base(Inputs&& inputs)
     :  node_type(
            std::forward<Inputs>(inputs),
            output_specifier_type{}.set_frame_length(std::get<AEC_MIC_INPUT_INDEX>(inputs).output_stream_specifier().get_frame_length())
        )
    {}

    template<class T = mic_specifier, class U = echoref_specifier, DFP_F_REQUIRES((
        !T::IS_FRAME_LENGTH_STATIC && U::IS_FRAME_LENGTH_STATIC
    ))>
    aec_node_base(Inputs&& inputs)
     :  node_type(
            std::forward<Inputs>(inputs),
            output_specifier_type{}.set_frame_length(std::get<AEC_ECHOREF_INPUT_INDEX>(inputs).output_stream_specifier().get_frame_length())
        )
    {}

    template<class T = mic_specifier, class U = echoref_specifier, DFP_F_REQUIRES((
        !T::IS_FRAME_LENGTH_STATIC && !U::IS_FRAME_LENGTH_STATIC
//        ||
//        ((T::FRAME_EXTENT != DYNAMIC_EXTENT) && (U::FRAME_EXTENT != DYNAMIC_EXTENT) && !S::IS_STATIC) 
    ))>
    aec_node_base(Inputs&& inputs)
     :  node_type(
            std::forward<Inputs>(inputs),
            output_specifier_type{} // in case where both intputs have DYNAMIC_EXTENT for frame extent, output frame length might be lately specified in initialize() method
        )
    {
        // just in case output frame length could already be determined
        set_frame_length();
    }

    output_frame_t<node_type> process_frame(input_frame_t<node_type, 0> const& mic_input, input_frame_t<node_type, 1> const& echoref_input)
    {
        auto out_frame = node_type::output_stream_specifier().get_frame_builder().build();

        //TODO: check that data() exists due to help of some frame_traits
        speex_echo_cancellation(get_state(), mic_input.data(), echoref_input.data(), out_frame.data());

        return out_frame;
    }


    template<class T=node_type, DFP_F_REQUIRES((!output_stream_specifier_t<T>::IS_FRAME_LENGTH_STATIC))>
    void initialize()
    {
        node_type::initialize();
        set_frame_length();
        check_after_initialize();
    }

    template<class T=node_type, DFP_F_REQUIRES((output_stream_specifier_t<T>::IS_FRAME_LENGTH_STATIC))>
    void initialize()
    {
        node_type::initialize();
        check_after_initialize();
    }

private:
    SpeexEchoState* get_state()
    { return static_cast<Derived* const>(this)->get_state(); }

    void set_frame_length()
    {
        std::size_t const mic_input_frame_length = this->input_stream_specifier(AEC_MIC_INPUT_INDEX).get_frame_length();
        std::size_t const echoref_input_frame_length = this->input_stream_specifier(AEC_ECHOREF_INPUT_INDEX).get_frame_length();

        if (mic_input_frame_length != DYNAMIC_EXTENT)
            this->set_output_stream_specifier(
                this->output_stream_specifier().set_frame_length(mic_input_frame_length)
            );
        else
            this->set_output_stream_specifier(
                this->output_stream_specifier().set_frame_length(echoref_input_frame_length)
            );
            
    }

    void check_after_initialize()
    {
        using namespace std::experimental;

        std::size_t const mic_input_frame_length = this->input_stream_specifier(AEC_MIC_INPUT_INDEX).get_frame_length();
        std::size_t const echoref_input_frame_length = this->input_stream_specifier(AEC_ECHOREF_INPUT_INDEX).get_frame_length();

        if (mic_input_frame_length == DYNAMIC_EXTENT)
            throw frame_length_error(
                DFP_STRINGIFY(DFP_SPEEX_AEC), AEC_MIC_INPUT_INDEX,
                "Frame length is \"DYNAMIC_EXTENT\" but ought have been specified at this stage."
            );

        if(echoref_input_frame_length == DYNAMIC_EXTENT)
            throw frame_length_error(
                DFP_STRINGIFY(DFP_SPEEX_AEC), AEC_ECHOREF_INPUT_INDEX,
                "Frame length is \"DYNAMIC_EXTENT\" but ought have been specified at this stage."
            );

        if (mic_input_frame_length != echoref_input_frame_length)
            throw frame_length_error(DFP_STRINGIFY(DFP_SPEEX_AEC), make_array(0, 1), "Both input frame lengths shall be identical.");

        if (
            (this->input_stream_specifier(AEC_MIC_INPUT_INDEX).get_channel_count() != 1)
            ||
            (this->input_stream_specifier(AEC_ECHOREF_INPUT_INDEX).get_channel_count() != 1)
        )
            throw channel_count_error(DFP_STRINGIFY(DFP_SPEEX_AEC), make_array(0, 1), "Both input streams shall provide one single channel.");
    }
}; // class aec_node_base


///@}
///@}
} // namespace detail
} // namespace speex
} // namespace dfp

#endif // DFPTL_SPEEX_DETAIL_AEC_NODE_HPP
