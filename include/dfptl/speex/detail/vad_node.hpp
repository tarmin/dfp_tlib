/**
* @file speex/detail/vad_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_DETAIL_VAD_NODE_HPP
#define DFPTL_SPEEX_DETAIL_VAD_NODE_HPP


#include "dfptl/core/node.hpp"

#include <speex/speex_preprocess.h>


#define DFP_SPEEX_VAD speex::vad


namespace dfp
{
namespace speex
{
namespace detail
{
///@addtogroup speex
///@{
///@addtogroup speex_detail
///@{


template<class Input>
class vad_node
 :  public  node<Input, atomic_stream_specifier<bool>, vad_node<Input>>
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_SPEEX_VAD, 1));

    typedef node<Input, atomic_stream_specifier<bool>, vad_node> node_type;

    static_assert(std::is_same<input_sample_t<node_type>, int16_t>{}, DFP_INVALID_INPUT_SAMPLE(DFP_SPEEX_VAD, int16_t, 0));

public:
    vad_node(Input&& input, int sampling_rate)
     :  node_type(std::forward<Input>(input)),
        state_(speex_preprocess_state_init(this->input_stream_specifier().get_frame_length(), sampling_rate))
    {
        spx_int32_t turnon = 1;
        speex_preprocess_ctl(state_, SPEEX_PREPROCESS_SET_VAD, &turnon);
    }

    vad_node(vad_node&& it) : node_type(std::move(it)), state_(it.state_)
    { it.state_ = nullptr; }

//    template<class InStream = input_stream_specifier_t<node_type>, DFP_F_REQUIRES((InStream::FRAME_EXTENT  != DYNAMIC_EXTENT))>
    output_sample_t<node_type> process_stream(input_stream_t<node_type>&& in)
    {
        const std::size_t frame_length = this->input_stream_specifier().get_frame_length();
        const auto data = in.drain().data(); // assuming that frame exposes some data() method (might be provided by frame_traits)
        //TODO: if data() method is not provided frame should be copied in a temporary buffer.
        //TODO: add in-place processing support
        spx_int16_t buffer[frame_length];

        for(std::size_t i = 0 ; i < frame_length ; i++)
            buffer[i] = data[i];

        return speex_preprocess_run(state_, &buffer[0]);
    }

    ~vad_node()
    { if (state_ != nullptr) speex_preprocess_state_destroy(state_); }

private:
    SpeexPreprocessState* state_;
};


///@} speex_detail
///@} speex
} // namespace detail
} // namespace speex
} // namespace dfp

#endif // DFPTL_SPEEX_DETAIL_VAD_NODE_HPP
