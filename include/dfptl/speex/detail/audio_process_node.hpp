/**
* @file speex/detail/audio_process_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_SPEEX_DETAIL_AUDIO_PROCESS_NODE_HPP
#define DFPTL_SPEEX_DETAIL_AUDIO_PROCESS_NODE_HPP


#include "dfptl/core/node.hpp"

#include <speex/speex_preprocess.h>


#define DFP_SPEEX_AUDIO_PROCESS speex::audio_process


namespace dfp
{
namespace speex
{
namespace detail
{
///@addtogroup speex
///@{
///@addtogroup speex_detail
///@{


template<class Input, class Derived>
class audio_process_node_base;


template<class Input, class HasSharedState = std::false_type>
class audio_process_node
 :  public  audio_process_node_base<Input, audio_process_node<Input, HasSharedState>>
{
    typedef audio_process_node_base<Input, audio_process_node> base;

public:
    audio_process_node(
        Input&& input, int sampling_rate,
        int max_noise_attenuation,
        SpeexEchoState const* echo_state,
        int max_residual_echo_attenuation,
        int max_dualtalk_residual_echo_attenuation
    )
     :  base(std::forward<Input>(input), max_noise_attenuation, echo_state, max_residual_echo_attenuation, max_dualtalk_residual_echo_attenuation),
        sampling_rate_(sampling_rate), state_(nullptr)
    {}

    audio_process_node(audio_process_node&& it)
     :  base(std::move(it)), sampling_rate_(it.sampling_rate_), state_(it.state_)
    { it.state_ = nullptr; }

    audio_process_node(audio_process_node const&) = default;

    ~audio_process_node()
    { if (state_ != nullptr) speex_preprocess_state_destroy(state_); }

    void initialize()
    {
        state_ = speex_preprocess_state_init(this->input_stream_specifier().get_frame_length(), sampling_rate_);

        base::initialize();
    }

    SpeexPreprocessState* get_state()
    { return state_; }

private:
    int const sampling_rate_;
    SpeexPreprocessState* state_;
};


template<class Input>
class audio_process_node<Input, std::true_type>
 :  public  audio_process_node_base<Input, audio_process_node<Input, std::true_type>>
{
    typedef audio_process_node_base<Input, audio_process_node> base;

public:
    audio_process_node(
        Input&& input, SpeexPreprocessState* processing_state,
        int max_noise_attenuation,
        SpeexEchoState const* echo_state,
        int max_residual_echo_attenuation,
        int max_dualtalk_residual_echo_attenuation
    )
     :  base(std::forward<Input>(input), max_noise_attenuation, echo_state, max_residual_echo_attenuation, max_dualtalk_residual_echo_attenuation),
        state_(processing_state)
    {}

    SpeexPreprocessState* get_state()
    { return state_; }

private:
    SpeexPreprocessState* state_;
};


template<class Input, class Derived>
class audio_process_node_base
 :  public  node<Input, as_input0_tag, Derived>
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_SPEEX_AUDIO_PROCESS, 1));

    typedef node<Input, as_input0_tag, Derived> node_type;

    static_assert(std::is_same<input_sample_t<node_type>, int16_t>{}, DFP_INVALID_INPUT_SAMPLE(DFP_SPEEX_AUDIO_PROCESS, int16_t, 0));

    typedef output_frame_t<node_type> frame_type;

public:
    audio_process_node_base(
        Input&& input,
        int max_noise_attenuation,
        SpeexEchoState const* echo_state,
        int max_residual_echo_attenuation,
        int max_dualtalk_residual_echo_attenuation
    )
     :  node_type(std::forward<Input>(input)),
        max_noise_attenuation_(max_noise_attenuation),
        echo_state_(echo_state),
        max_residual_echo_attenuation_(max_residual_echo_attenuation),
        max_dualtalk_residual_echo_attenuation_(max_dualtalk_residual_echo_attenuation),
        output_(frame_builder<frame_type>{}.set_frame_length(this->output_stream_specifier().get_frame_length()).build())
    {}

    void initialize()
    {
        spx_int32_t turnon = 1;

        // output frame length may have been specified lately
        update_length(output_, this->output_stream_specifier().get_frame_length());

        // all const_cast below required because of speex lib missing for dedicated signature for setter
        if (max_noise_attenuation_ != 0)
        {
            speex_preprocess_ctl(get_state(), SPEEX_PREPROCESS_SET_NOISE_SUPPRESS, const_cast<int*>(&max_noise_attenuation_));
            speex_preprocess_ctl(get_state(), SPEEX_PREPROCESS_SET_DENOISE, &turnon);
        }
        if (echo_state_ != nullptr)
        {
            speex_preprocess_ctl(get_state(), SPEEX_PREPROCESS_SET_ECHO_SUPPRESS, const_cast<int*>(&max_residual_echo_attenuation_));
            speex_preprocess_ctl(get_state(), SPEEX_PREPROCESS_SET_ECHO_SUPPRESS_ACTIVE, const_cast<int*>(&max_dualtalk_residual_echo_attenuation_));
            speex_preprocess_ctl(get_state(), SPEEX_PREPROCESS_SET_ECHO_STATE, const_cast<SpeexEchoState*>(echo_state_)); // non null value residual echo reduction
        }
    }

    output_frame_t<node_type> const& process_frame(input_frame_t<node_type> const& in)
    {
        //TODO: add support in-place processing support if possible

        std::copy(in.cbegin(), in.cend(), output_.begin());

        speex_preprocess_run(get_state(), &output_.data()[0]);

        return output_;
    }

    SpeexPreprocessState* get_state() const
    { return static_cast<Derived const* const>(this)->get_state(); }

private:
    SpeexPreprocessState* get_state()
    { return static_cast<Derived* const>(this)->get_state(); }

    // max noise attenuation level expressed in dB. If null or positive, denoise processing is disabled
    int const max_noise_attenuation_;
    SpeexEchoState_ const* const echo_state_; // if nullptr, residual echo reduction is disabled
    // max residual echo attenuation level expressed in dB.
    int const max_residual_echo_attenuation_;
    // max residual echo attenuation level expressed in dB in dual talk condition.
    int const max_dualtalk_residual_echo_attenuation_;
    frame_type output_;
};


///@} speex_detail
///@} speex
} // namespace detail
} // namespace speex
} // namespace dfp

#endif // DFPTL_SPEEX_DETAIL_AUDIO_PROCESS_NODE_HPP
