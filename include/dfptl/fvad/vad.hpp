/**
* @file fvad/fvad.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_FVAD_VAD_HPP
#define DFPTL_FVAD_VAD_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/fvad/detail/vad_node.hpp"


namespace dfp
{
/**
* @defgroup fvad_grp FVAD node
* @brief Detects voice activity on its single input @ref core::stream "stream".
*
* A @ref fvad_grp detects voice activity on its input stream and produces a @ref atomic_stream "atomic stream" of boolean values
* to indicate presence of voice in the incoming frame.
* Processing in this node is based on functions provided by the [fvad](https://github.com/dpirch/libfvad) library.
*
* @note
* * input sample type shall be int16_t.
* * Only frames with a length of 10, 20 or 30 ms are supported, so for example at 8 kHz, input frame length (aka flow width) must be either 80, 160 or 240.
*
* **Node interface**
* @startuml
* rectangle "o(n) = ([i(n) ...i(n+N-1)] is voice frame)" <<fvad::fvad()>> as vad
* interface "...[i(n) ...i(n+N-1)]" <<INPUT>> as in
* interface "...o(n)" <<"detection status">> as out
* vad <- in
* out <- vad
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | a @ref stream_grp conveying frame to to analyze for voice detection
* "detection status" | **output**  | the @ref atomic_stream "atomic stream" of boolean indicating status of voice detection
*
* @note @ref fvad_grp mainly wraps functions provided by the [fvad](https://github.com/dpirch/libfvad) library
* which shall be available on your building host for your target. The @c bootstrap script can be used to install required dependency
* when building host and target is Ubuntu 16.04 (or higher revision)
*
* @{
*
* @defgroup fvad_detail implementation details
* @brief Implementation details and boilerplate for @ref fvad_grp
**/


/**
* @brief Contains items related to processing cells wrapping the FVAD function.
**/
namespace fvad
{
inline namespace nodes
{
/**
* @brief Creates a cell builder for a @ref fvad_grp.
*
* @param sampling_rate specifies the input sample rate in Hz for a VAD instance.
* Valid values are 8000, 16000, 32000 and 48000. 
* @param mode indicates the VAD operating ("aggressiveness") mode of a the VAD.
* more aggressive (higher mode) VAD is more restrictive in reporting speech.
* Put in other words the probability of being speech when the VAD returns 1 is
* increased with increasing mode. As a consequence also the missed detection
* rate goes up.
* Valid modes are 0 ("quality"), 1 ("low bitrate"), 2 ("aggressive"), and 3
* ("very aggressive"). The default mode is 0.
*
* **Usage example**
* @snippet fvad_test.cpp fvad usage example
**/
inline auto vad(int sampling_rate, int mode = 0) /** @cond */ ->
decltype(make_placeholder<std::tuple<Fvad*>, detail::vad_node>(sampling_rate, mode)) /** @endcond */
{ return make_placeholder<std::tuple<Fvad*>, detail::vad_node>(sampling_rate, mode); }


template<int SAMPLING_RATE>
inline auto vad(int_const<SAMPLING_RATE> sampling_rate, int mode = 0) /** @cond */ ->
decltype(make_placeholder<std::tuple<Fvad*>, detail::vad_node>(sampling_rate, mode)) /** @endcond */
{ return make_placeholder<std::tuple<Fvad*>, detail::vad_node>(sampling_rate, mode); }


/**
* @brief Creates a cell builder for a @ref fvad_grp.
*
* @param state_ref a reference on an existing `Fvad` state managed outside of the processing tree
*
* **Usage example**
* @snippet fvad_test.cpp fvad usage example2
**/
inline auto vad(std::reference_wrapper<Fvad> state_ref) /** @cond */ ->
decltype(make_placeholder<std::tuple<std::reference_wrapper<Fvad>>, detail::vad_node>(state_ref)) /** @endcond */
{ return make_placeholder<std::tuple<std::reference_wrapper<Fvad>>, detail::vad_node>(state_ref); }


} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using fvad::vad;
} // namespace nodes
} // namespace shortname


///@} fvad_grp
} // namespace fvad
} // namespace dfp


#endif // DFPTL_FVAD_VAD_HPP
