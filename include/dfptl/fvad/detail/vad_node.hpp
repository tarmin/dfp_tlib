/**
* @file fvad/detail/vad_node.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_FVAD_DETAIL_VAD_NODE_HPP
#define DFPTL_FVAD_DETAIL_VAD_NODE_HPP


#include "dfptl/core/node.hpp"
#include "dfptl/core/exception.hpp"

#include <fvad.h>


#define DFP_FVAD_VAD fvad::vad


namespace dfp
{
namespace fvad
{
namespace detail
{
///@addtogroup fvad
///@{
///@addtogroup fvad_detail
///@{


template<class Input, class StateWrapper>
class vad_node
{
    static_assert(
        std::is_same<StateWrapper, Fvad*>{} || std::is_same<StateWrapper, std::reference_wrapper<Fvad>>{},
        DFP_UNEXPECTED_ERROR
    );
};

template<class Input, class Derived>
class vad_node_base;

template<class Input>
class vad_node<Input, Fvad*>
 :  public  vad_node_base<Input, vad_node<Input, Fvad*>>
{
    typedef vad_node_base<Input, vad_node> base;

public:
    vad_node(Input&& input, int sampling_rate, int mode)
     :  base(std::forward<Input>(input)), sampling_rate_(sampling_rate), mode_(mode)
    { check_frame_extent(sampling_rate); }

    template<int SAMPLING_RATE, class T = typename base::node_type, DFP_F_REQUIRES((input_stream_specifier_t<T>::FRAME_EXTENT != DYNAMIC_EXTENT))>
    vad_node(Input&& input, int_const<SAMPLING_RATE> sampling_rate, int mode)
     :  base(std::forward<Input>(input)), sampling_rate_(sampling_rate), mode_(mode)
    { check_frame_extent(sampling_rate); }

    void initialize()
    {
        if (state_ == nullptr)
        {
            state_ = fvad_new();
            check_state_build(state_);
            init(sampling_rate_, mode_);
        }
    }

    vad_node(vad_node const&) = default;

    vad_node(vad_node&& it)
     :  base(std::move(it)), state_(it.state_), sampling_rate_(it.sampling_rate_), mode_(it.mode_)
    { it.state_ = nullptr; }

    ~vad_node()
    {
        if (state_ != nullptr)
            fvad_free(state_);
    }

    Fvad& get_state()
    { return *state_; }

private:
    static void check_state_build(Fvad const* state)
    {
        if(state == nullptr)
            throw cell_error(DFP_STRINGIFY(DFP_FVAD_VAD), "Out-of-memory error");
    }

    template<int SAMPLING_RATE, class T = typename base::node_type, DFP_F_REQUIRES((input_stream_specifier_t<T>::FRAME_EXTENT != DYNAMIC_EXTENT))>
    static void check_frame_extent(int_const<SAMPLING_RATE>)
    {
        static_assert(
            ((SAMPLING_RATE*10%1000 == 0) && (SAMPLING_RATE*10/1000 == input_stream_t<typename base::node_type>::FRAME_EXTENT)) || // 10ms
            ((SAMPLING_RATE*20%1000 == 0) && (SAMPLING_RATE*20/1000 == input_stream_t<typename base::node_type>::FRAME_EXTENT)) || // 20ms
            ((SAMPLING_RATE*30%1000 == 0) && (SAMPLING_RATE*30/1000 == input_stream_t<typename base::node_type>::FRAME_EXTENT)),   // 30ms
            DFP_INVALID_INPUT_FRAME_EXTENT(DFP_FVAD_VAD, equal to 10ms || 20ms || 30ms, 0)
        );
    }

    void check_frame_extent(int sampling_rate)
    {
        const size_t frame_length = this->input_stream_specifier().get_frame_length();
        if(
            (sampling_rate*0.01 != frame_length) && // 10ms
            (sampling_rate*0.02 != frame_length) && // 20ms
            (sampling_rate*0.03 != frame_length)    // 30ms
        )
            throw frame_length_error(DFP_STRINGIFY(DFP_FVAD_VAD), 0, "It only supports input frame length of 10ms, 20ms or 30ms");
    }

    void init(int sampling_rate, int mode)
    {
        fvad_set_mode(state_, mode);
        fvad_set_sample_rate(state_, sampling_rate);
    }

    Fvad* state_ {};
    int sampling_rate_;
    int mode_;
}; // class vad_node<...Fvad*...>


template<class Input>
class vad_node<Input, std::reference_wrapper<Fvad>>
 :  public  vad_node_base<Input, vad_node<Input, std::reference_wrapper<Fvad>>>
{
    typedef vad_node_base<Input, vad_node> base;

public:
    vad_node(Input&& input, std::reference_wrapper<Fvad> state)
     :  base(std::forward<Input>(input)), state_(state.get())
    {}

    Fvad& get_state()
    { return state_; }

private:
    Fvad& state_;
}; // class vad_node<...std::reference_wrapper<Fvad>...>


template<class Input, class Derived>
class vad_node_base : public node<Input, atomic_stream_specifier<bool>, Derived>
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_FVAD_VAD, 1));

protected:
    typedef node<Input, atomic_stream_specifier<bool>, Derived> node_type;

    static_assert(std::is_same<input_sample_t<node_type>, int16_t>{}, DFP_INVALID_INPUT_SAMPLE(DFP_FVAD_VAD, int16_t, 0));

public:
    using node_type::node_type;

//    template<class InStream = input_stream_specifier_t<node_type>, DFP_F_REQUIRES((InStream::FRAME_EXTENT  != DYNAMIC_EXTENT))>
    output_sample_t<node_type> process_stream(input_stream_t<node_type>&& in)
    {
        const auto frame = in.drain();

        // assuming that frame exposes some data() method (might be provided by frame_traits)
        //TODO: if data() method is not provided frame should be copied in a temporary buffer.

        int result = fvad_process(&state(), frame.data(), frame.size());

        if (result == -1)
            throw cell_error(DFP_STRINGIFY(DFP_FVAD_VAD));

        return result == 1;
    }

private:
    Fvad& state()
    { return static_cast<Derived* const>(this)->get_state(); }
}; // class vad_node_base


///@} fvad_detail
///@} fvad
} // namespace detail
} // namespace fvad
} // namespace dfp


#endif // DFPTL_FVAD_DETAIL_VAD_NODE_HPP
