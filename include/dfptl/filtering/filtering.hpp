/**
* @file filtering/filtering.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_FILTERING_FILTERING_HPP
#define DFPTL_FILTERING_FILTERING_HPP


#include "dfptl/filtering/finite_difference.hpp"

namespace dfp
{
/**
* @defgroup filtering_grp Filtering cells
* @brief Defines processing nodes applying signal filtering nodes.
* @{
**/
/**
* @defgroup filtering_detail implementation details
* @brief Implementation details and boilerplate for @ref filtering_grp
**/
/**
* @brief Contains items related to processing cells for signal filtering
**/
namespace filtering
{

/**
* @brief Set of helper functions for nodes applying signal filtering.
**/
inline namespace nodes {}
namespace knots = nodes;

/** @brief short name of functions in @ref filtering_grp namespace **/
namespace shortname {}


///@}
} // namespace filtering
} // namespace dfp


#endif // DFPTL_FILTERING_FILTERING_HPP
