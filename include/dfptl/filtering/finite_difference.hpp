/**
* @file filtering/finite_difference.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_FILTERING_FINITE_DIFFERENCE_HPP
#define DFPTL_FILTERING_FINITE_DIFFERENCE_HPP


#include "dfptl/core/from.hpp"
#include "dfptl/core/probe.hpp"
#include "dfptl/core/subprocessing.hpp"
#include "dfptl/routing/delay.hpp"
#include "dfptl/alu/subtract.hpp"


#define DFP_FILTERING_FINITE_DIFFERENCE filtering::finite_difference


namespace dfp
{
namespace filtering
{
inline namespace nodes
{
///@addtogroup filtering_grp
///@{
/**
* @defgroup finite_difference_grp "finite-difference" node
* @brief c.f. [finite difference](https://en.wikipedia.org/wiki/Finite_difference)
* @{
* @defgroup backward_difference_grp "backward-difference" node
* A @ref backward_difference_grp is a 1-input sample-based node which for each captured sample on its single input
* computes the discrete backward difference given by `i(n) - i(n-1)` and will provides on its output.
* The resulting stream is a, @ref atomic_stream "atomic stream" delivering the computation result.
*
* **Node interface**
* @startuml
* rectangle "o(n) = i(n) - i(n-1)" <<filtering::backward_difference(init)>> as filtering
* interface "2 1 1 0 ..." <<#0-SIGNAL>> as in
* interface "(2-init) 1 0 -1 ..." <<filtered signal>> as out
* filtering <- in
* out <- filtering
* @enduml
*
* stream name       | location    | description
* ------------------|-------------|------------
* SIGNAL            | **input-0** | @ref atomic_stream "atomic stream" as input data signal
* "filtered signal" | **output**  | @ref atomic_stream "atomic stream" resulting from filtering computation on input `SIGNAL`
* @{
**/


template<class T>
class backward_difference_processing
{
    T const initial_;
    static constexpr struct : custom_tag {} IN {};

public:
    constexpr backward_difference_processing(T const& initial) :  initial_(initial)
    {}

    template<class Input>
    auto operator()(Input in) const
#if not DFP_CPP14_SUPPORT
    ->
    decltype(
        alu::subtract() += routing::delay(1, this->initial_) <<= from(IN)
                        |= unsafe_probe(IN) <<= in
    )
#endif
    { return
        alu::subtract() += routing::delay(1, this->initial_) <<= from(IN)
                        |= unsafe_probe(IN) <<= in
    ;}
}; // backward_difference_processing


/**
* @brief Creates a placeholder for a @ref backward_difference_grp to apply backward difference.
*
* **Usage example**
* @snippet filtering_test.cpp backward_difference usage example
*
* @param[in] initial is the left operand for first difference computation.
* @tparam T implicitly deduced from @c initial parameter
**/
template<class T = int>
inline constexpr auto backward_difference(T const& initial = T{}) /** @cond */ ->
decltype(subprocessing(backward_difference_processing<T>{initial})) /** @endcond */
{ return subprocessing(backward_difference_processing<T>{initial}); }


///@} backward_difference_grp
///@} finite_difference_grp
///@} filtering_grp
} // namespace nodes

namespace shortname
{
inline namespace nodes
{
/**
* @brief shortname for @ref backward_difference()
**/
template<class T = int>
inline auto constexpr bdiff(T const& initial = T{}) /** @cond */ ->
decltype(backward_difference(initial)) /** @endcond */
{ return backward_difference(initial); }
} // nodes
} // shortname
} // namespace filtering
} // namespace dfp


#endif // DFPTL_FILTERING_FINITE_DIFFERENCE_HPP
