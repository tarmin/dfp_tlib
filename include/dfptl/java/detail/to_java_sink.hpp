/**
* @file java/detail/to_java_sink.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_JAVA_DETAIL_TO_JAVA_SINK_HPP
#define DFPTL_JAVA_DETAIL_TO_JAVA_SINK_HPP


#include "dfptl/core/sink.hpp"
#include "dfptl/java/detail/processor_descriptor.hpp"

#include <boost/fusion/include/transform_view.hpp>
#include <boost/fusion/include/filter_view.hpp>
#include <boost/fusion/include/front.hpp>
#include <boost/fusion/include/pair.hpp>

#include <functional>
#include <sstream>
#include <map>

#include <jni.h>

#include <iostream>

namespace dfp
{
namespace java
{
namespace detail
{


#define DFP_JAVA_TO_JAVA java::to_java


template<class StreamTagPair, class = void>
struct contains_from_java_descriptor_impl : std::false_type {};


template<class StreamTagPair>
struct contains_from_java_descriptor_impl<StreamTagPair, DFP_C_REQUIRES((
    always_true<typename boost::fusion::result_of::first<StreamTagPair>::type::from_java_descriptor>{}
))> : std::true_type {};


template<class StreamTagPair>
struct contains_from_java_descriptor : contains_from_java_descriptor_impl<StreamTagPair> {};


template<class StreamTagMap>
struct from_java_descriptor_streamtags
{
    typedef boost::fusion::filter_view<StreamTagMap const,
        boost::mpl::quote1<contains_from_java_descriptor>::apply<boost::mpl::_>
    > type;
};


struct extract_from_java_descriptor
{
    template<class TagPair>
    struct result
    { typedef typename boost::fusion::result_of::first<TagPair>::type::from_java_descriptor type; };

    template <typename TagPair>
    typename result<TagPair>::type operator()(TagPair) const
    { return typename result<TagPair>::type{}; }
};


template<class StreamTagMap>
struct from_java_descriptors_view
{
    typedef boost::fusion::transform_view<
        typename boost::mpl::quote1<from_java_descriptor_streamtags>::apply<StreamTagMap>::type,
        extract_from_java_descriptor
    > type;
};


template<class FromJavaDescriptor, class Rank, class = void>
struct from_java_descriptor_has_rank_impl : std::false_type {};

template<class FromJavaDescriptor, class Rank>
struct from_java_descriptor_has_rank_impl<FromJavaDescriptor, Rank, DFP_C_REQUIRES((
    typename FromJavaDescriptor::first_type{} == Rank{}
))> : std::true_type {};


template<class FromJavaDescriptor, class Rank>
struct from_java_descriptor_has_rank : from_java_descriptor_has_rank_impl<FromJavaDescriptor, Rank> {};


template<class InputRank, class StreamTagMap>
struct input_jsample
{
    typedef typename std::remove_reference<
        typename boost::fusion::result_of::front<boost::fusion::filter_view<
            typename boost::mpl::quote1<from_java_descriptors_view>::apply<StreamTagMap>::type,
            boost::mpl::quote2<from_java_descriptor_has_rank>::apply<boost::mpl::_, InputRank>
        >>::type
    >::type::second_type type;
};


template<class Jsample> struct java_array;
template<class Jsample> using java_array_t = typename java_array<Jsample>::type;

template<> struct java_array<jchar>
{ typedef jcharArray type; };

template<> struct java_array<jint>
{ typedef jintArray type; };

template<> struct java_array<jshort>
{ typedef jshortArray type; };

template<> struct java_array<jlong>
{ typedef jlongArray type; };

template<> struct java_array<jfloat>
{ typedef jfloatArray type; };

template<> struct java_array<jdouble>
{ typedef jdoubleArray type; };


template<class Jarray> struct jelement;
template<class Jarray> using jelement_t = typename jelement<Jarray>::type;

template<> struct jelement<jcharArray>
{ typedef jchar type; };

template<> struct jelement<jshortArray>
{ typedef jshort type; };

template<> struct jelement<jintArray>
{ typedef jint type; };

template<> struct jelement<jlongArray>
{ typedef jlong type; };

template<> struct jelement<jfloatArray>
{ typedef jfloat type; };

template<> struct jelement<jdoubleArray>
{ typedef jdouble type; };


template<class Jelement> jarray new_jarray(JNIEnv* env, jsize length);
template<class Jarray> jelement_t<Jarray>* get_jarray_elements(JNIEnv* env, Jarray jarr);


template<class Input, class OutputJsample, class = void>
class to_java_sink
{
    static_assert(std::tuple_size<Input>{} == 1, DFP_INVALID_INPUT_COUNT(DFP_JAVA_TO_JAVA, 1));
};


template<class Input, class OutputJsample>
class to_java_sink<Input, OutputJsample, DFP_C_REQUIRES((std::tuple_size<Input>{} == 1))>
 :  public  sink<Input, run_once, to_java_sink<Input, OutputJsample>,
                boost::fusion::pair<processor_descriptor, processor_descriptor>
            >
{
    typedef sink<Input, run_once, to_java_sink,
                boost::fusion::pair<processor_descriptor, processor_descriptor>
            > sink_type;

    static_assert(
        typename boost::fusion::result_of::size<
            typename boost::mpl::quote1<from_java_descriptor_streamtags>::apply<typename input_stream_t<sink_type>::tag_map::type>::type
        >::type{} == 1,
        "expecting one single from_java source at edge of the processing tree"
    );

    typedef typename boost::mpl::quote2<input_jsample>
        ::apply<int_const<0>, typename input_stream_t<sink_type>::tag_map::type>::type input_jsample_type;

public:
    using processor_context_map = std::map<jobject, processor_context, std::function<bool(jobject, jobject)>>;

    to_java_sink(Input&& input, type_meta<OutputJsample>) noexcept
     :  sink_type(std::forward<Input>(input))
    {}

    template<class InitStorageView>
    void initialize_upwards(InitStorageView init_storage)
    {
        processor_descriptor descriptor = boost::fusion::at_key<processor_descriptor>(init_storage);
        processor_context& context = *descriptor.context;
        to_java_sink const& processing_tree = *reinterpret_cast<to_java_sink*>(context.processing_tree_ptr);
        const std::size_t input_frame_length = processing_tree.sink_type::input_stream_specifier().get_frame_length();
        JNIEnv& jenv = *descriptor.jenv;

        if (input_frame_length != (std::size_t) descriptor.output_jframe_length)
        {
            std::stringstream message_stream;
            message_stream << "input stream of to_java sink has unexpected frame length " << input_frame_length << " while " << descriptor.output_jframe_length << " was expected";
            throw std::length_error(message_stream.str());
        }

        // allocate the java output array
        context.output_jframe = static_cast<jarray>(
            jenv.NewGlobalRef(new_jarray<OutputJsample>(&jenv, descriptor.output_jframe_length))
        );
    }

    void release_frame(input_frame_t<sink_type> const& input) const
    {
        auto input_it = input.cbegin();
        auto output_it = output_frame_;

        for(jsize i = get_length(input) ; i > 0 ; i--)
            *output_it++ = *input_it++;
    }

    static void initialize(
        JNIEnv *env, jobject jobj,
        std::function<to_java_sink(std::size_t, std::vector<std::size_t> const&)> const& tree_builder,
        jsize output_jframe_length,
        jintArray input_jframe_lengths
    )
    {
        // if first call to this initialize method, need to create the context_map_
        // note that the `env` given by the JAVA runtime is unique for a given process
        if(context_map_ == nullptr)
        {
            // TODO: usage of nonstd::optional should be preferred to avoid unecessary dynamic allocation
            context_map_ = new processor_context_map(
                [env](jobject const& ref1, jobject const& ref2)
                { return env->IsSameObject(ref1, ref2) != JNI_TRUE; }
            );            
        }

        const jsize input_count = env->GetArrayLength(input_jframe_lengths);
        std::vector<std::size_t> jframe_lengths_as_vector(input_count);
        jsize const* jframe_lengths_ptr = get_jarray_elements(env, input_jframe_lengths);

        for(int i = 0 ; i < input_count ; i++)
            jframe_lengths_as_vector[i] = (std::size_t) jframe_lengths_ptr[i];
        // const_cast required to workaround bad definition of releas_jarray_elements
        release_jarray_elements(env, input_jframe_lengths, const_cast<jsize*>(jframe_lengths_ptr));

        processor_context* const context = &(*context_map_)[env->NewGlobalRef(jobj)];
        to_java_sink* const processing_tree = new to_java_sink(tree_builder(output_jframe_length, jframe_lengths_as_vector));
        processor_descriptor descriptor
        {
            output_jframe_length,
            env,
            context,
            0 // next_source_rank
        };

        context->input_spans = std::vector<upg::span<processor_context::any_sample>*>(input_count);
        context->input_jframe_lengths = jframe_lengths_as_vector;
        context->processing_tree_ptr = processing_tree;

        processing_tree->initialize_tree(descriptor);
    }

    static void dispose(JNIEnv* env, jobject jobj)
    {
        if(context_map_ != nullptr)
        {
            auto const pair_it = context_map_->find(jobj);

            if(pair_it != context_map_->cend())
            {
                processor_context& context = pair_it->second;
                jobject const jprocessor_obj = pair_it->first;

                env->DeleteGlobalRef(context.output_jframe);
                context_map_->erase(pair_it);
                env->DeleteGlobalRef(jprocessor_obj);
            }
        }

        if(context_map_->empty())
        {
            delete context_map_;
            context_map_ = nullptr;
        }
    }

    static jarray run(JNIEnv *env, jobject jobj, jobject jinputs)
    {
        processor_context& context = (*context_map_)[env->NewGlobalRef(jobj)];
        to_java_sink& processing_tree = *reinterpret_cast<to_java_sink*>(context.processing_tree_ptr);
        jclass const inputs_jclass = env->GetObjectClass(jinputs);
        jsize const jtuple_size = env->CallIntMethod(jinputs, env->GetMethodID(inputs_jclass, "size", "()I"));

        if (jtuple_size != 1)
            throw std::invalid_argument("input java tuple of INativeProcessor.process() has invalid size, on single input supported for now");

        jarray input_jarray = (jarray) env->CallObjectMethod(jinputs, env->GetMethodID(inputs_jclass, "get", "(I)Ljava/lang/Object;"), 0);

        auto input_jframe = static_cast<java_array_t<input_jsample_type>>(input_jarray);
        const std::size_t input0_jframe_length = env->GetArrayLength(input_jframe);
        java_array_t<OutputJsample>& joutput =
            reinterpret_cast<java_array_t<OutputJsample>&>((*context_map_)[jobj].output_jframe);

        using any_sample = processor_context::any_sample;
        input_jsample_type* const input_jframe_ptr = get_jarray_elements(env, input_jframe);

        if (input0_jframe_length != context.input_jframe_lengths[0])
        {
            std::stringstream message_stream;
            message_stream << "java input frame for from_java source #0 has unexpected frame length " << input0_jframe_length << " while " << context.input_jframe_lengths[0] << " was expected";
            throw std::length_error(message_stream.str());
        }

        *context.input_spans[0] = upg::span<any_sample>((any_sample*) input_jframe_ptr, context.input_jframe_lengths[0]);

        // output_frame_ shall be assigned before `run` invocation as it will be used in `release_frame`
        processing_tree.output_frame_ = get_jarray_elements(env, joutput);
        processing_tree.sink_type::run();
        release_jarray_elements(env, input_jframe, input_jframe_ptr);
        release_jarray_elements(env, joutput, processing_tree.output_frame_);
        processing_tree.output_frame_ = nullptr;
        *context.input_spans[0] = upg::span<any_sample>();

        return joutput;
    }

    static processor_context_map* context_map_;

    // default initialize method shall be overloaded as another initialize overloading is defined
    void initialize()
    { sink_type::initialize(); }

private:
    OutputJsample* output_frame_;
}; // class to_java_sink


template<> jarray new_jarray<jchar>(JNIEnv* env, jsize length)
{ return env->NewCharArray(length); }

template<> jarray new_jarray<jint>(JNIEnv* env, jsize length)
{ return env->NewIntArray(length); }

template<> jarray new_jarray<jlong>(JNIEnv* env, jsize length)
{ return env->NewLongArray(length); }

template<> jarray new_jarray<jfloat>(JNIEnv* env, jsize length)
{ return env->NewFloatArray(length); }

template<> jarray new_jarray<jdouble>(JNIEnv* env, jsize length)
{ return env->NewDoubleArray(length); }


template<> jchar* get_jarray_elements<>(JNIEnv* env, jcharArray arr)
{ return env->GetCharArrayElements(arr, NULL); }

template<> jint* get_jarray_elements<>(JNIEnv* env, jintArray arr)
{ return env->GetIntArrayElements(arr, NULL); }

template<> jshort* get_jarray_elements<>(JNIEnv* env, jshortArray arr)
{ return env->GetShortArrayElements(arr, NULL); }

template<> jlong* get_jarray_elements<>(JNIEnv* env, jlongArray arr)
{ return env->GetLongArrayElements(arr, NULL); }

template<> jfloat* get_jarray_elements<>(JNIEnv* env, jfloatArray arr)
{ return env->GetFloatArrayElements(arr, NULL); }

template<> jdouble* get_jarray_elements<>(JNIEnv* env, jdoubleArray arr)
{ return env->GetDoubleArrayElements(arr, NULL); }


} // namespace detail
} // namespace java
} // namespace dfp


#endif // DFPTL_JAVA_DETAIL_TO_JAVA_SINK_HPP
