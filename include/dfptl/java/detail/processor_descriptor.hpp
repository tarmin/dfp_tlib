/**
* @file java/detail/processor_descriptor.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_JAVA_DETAIL_PROCESSOR_DESCRIPTOR_HPP
#define DFPTL_JAVA_DETAIL_PROCESSOR_DESCRIPTOR_HPP


#include <vector>

#include <jni.h>


namespace dfp
{
namespace java
{
namespace detail
{


struct processor_context
{
    struct any_sample {};
    jarray output_jframe;
    void* processing_tree_ptr;
    std::vector<std::size_t> input_jframe_lengths;
    std::vector<upg::span<any_sample>*> input_spans;
};


struct processor_descriptor
{
    jsize output_jframe_length;
    JNIEnv *jenv;
    processor_context* context;
    uint8_t next_source_rank;
};

void release_jarray_elements(JNIEnv* env, jfloatArray arr, jfloat* const sam)
{ env->ReleaseFloatArrayElements(arr, sam, 0); }

void release_jarray_elements(JNIEnv* env, jintArray arr, jint* const sam)
{ env->ReleaseIntArrayElements(arr, sam, 0); }

void release_jarray_elements(JNIEnv* env, jshortArray arr, jshort* const sam)
{ env->ReleaseShortArrayElements(arr, sam, 0); }


} // namespace detail
} // namespace java
} // namespace dfp


#endif // DFPTL_JAVA_DETAIL_PROCESSOR_DESCRIPTOR_HPP
