/**
* @file java/java.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_JAVA_JAVA_HPP
#define DFPTL_JAVA_JAVA_HPP


#include "dfptl/core/utility.hpp"
#include "dfptl/java/to_java.hpp"
#include "dfptl/java/from_java.hpp"

#include <stl/type_traits.hpp>


#define DFP_JAVA_ASSIGN_PROCESSING_TREE(JavaProcessorClass, ProcessingTreeBuilder)\
    template<>\
    typename std::invoke_result_t<decltype(ProcessingTreeBuilder), std::size_t, std::vector<std::size_t> const&>::processor_context_map*\
    std::invoke_result_t<decltype(ProcessingTreeBuilder), std::size_t, std::vector<std::size_t> const&>::terminal_cell_type::context_map_ = nullptr;\
    extern "C" JNIEXPORT void JNICALL\
    DFP_CONCAT(Java_, DFP_CONCAT(JavaProcessorClass, _initialize))(JNIEnv *env, jobject jobj, jint output_jframe_length, jintArray input_jframe_lengths)\
    { std::invoke_result_t<decltype(ProcessingTreeBuilder), std::size_t, std::vector<std::size_t> const&>::initialize(\
        env, jobj, ProcessingTreeBuilder, output_jframe_length, input_jframe_lengths\
    ); }\
    extern "C" JNIEXPORT jarray JNICALL\
    DFP_CONCAT(Java_, DFP_CONCAT(JavaProcessorClass, _process))(JNIEnv *env, jobject jobj, jobject jinputs)\
    { return std::invoke_result_t<decltype(ProcessingTreeBuilder), std::size_t, std::vector<std::size_t> const>::run(\
        env, jobj, jinputs\
    ); }\
    extern "C" JNIEXPORT void JNICALL\
    DFP_CONCAT(Java_, DFP_CONCAT(JavaProcessorClass, _dispose))(JNIEnv *env, jobject jobj)\
    { std::invoke_result_t<decltype(ProcessingTreeBuilder), std::size_t, std::vector<std::size_t> const&>::dispose(env, jobj); }


namespace dfp
{
/**
* @defgroup java_grp JAVA cells
* @brief Defines processing nodes applying signal filtering nodes.
* @{
**/
/**
* @defgroup java_detail implementation details
* @brief Implementation details and boilerplate for @ref filtering_grp
**/
/**
* @brief Contains items related to processing cells for signal filtering
**/
namespace java
{


DFP_INLINE_CONSTEXPR int32_t_meta<> JINT;
DFP_INLINE_CONSTEXPR float_meta<>   JFLOAT;


/**
* @brief Set of helper functions for JAVA sources.
**/
inline namespace sources {}
namespace leaves = sources;

/** @brief short name of functions in @ref java_grp namespace **/
namespace shortname {}


///@}
} // namespace java
} // namespace dfp


#endif // DFPTL_JAVA_JAVA_HPP
