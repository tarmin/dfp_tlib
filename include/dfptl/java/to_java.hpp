/**
* @file java/to_java.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_JAVA_TO_JAVA_HPP
#define DFPTL_JAVA_TO_JAVA_HPP


#include "dfptl/java/detail/to_java_sink.hpp"


namespace dfp
{
namespace java
{
inline namespace sinks
{
///@addtogroup java_grp
///@{
/**
* @defgroup release_sample_grp "release sample" sink
* @brief Releases one single sample from its single input.
*
* A @ref release_sample_grp pulls one single sample from its single input port when its \c run() method is invoked.
* It does not apply any transformation on captured samples.
*
* **Sink interface**
* @startuml
* rectangle "i(k-1) <- run() | with k the count of run() calls" <<sample_releaser()>> as sink
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* sink <-up- in
* @enduml

 * port name    | location    | stream specification          | description
* --------------|-------------|-------------------------------|------------
* "in"          | **input-0** | @ref atomic_stream            | consumes the samples to release
* @{
**/


/**
* @brief Creates a placeholder for a @ref release_sample_grp.
*
* **Usage example**
* @snippet routing_test.cpp sample_releaser usage example
**/
template<class OutputJsample>
constexpr inline auto to_java(type_meta<OutputJsample> output_jsample_meta) /** @cond */ ->
decltype(make_placeholder<std::tuple<OutputJsample>, detail::to_java_sink>(output_jsample_meta)) /** @endcond */
{ return make_placeholder<std::tuple<OutputJsample>, detail::to_java_sink>(output_jsample_meta); }


///@} to_java_grp
} // namespace sinks


namespace shortname
{
inline namespace sinks
{
///@addtogroup to_java_grp
///@{
using java::sinks::to_java;
///@} to_java_grp
} // namespace sinks
} // namespace shortname


///@} java_grp
} // namespace java
} // namespace dfp


#endif // DFPTL_JAVA_TO_JAVA_HPP
