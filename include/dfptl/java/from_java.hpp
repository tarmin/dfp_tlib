/**
* @file java/from_java.hpp
**/
/*
* Copyright (C) 2022 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/



#ifndef DFPTL_JAVA_FROM_JAVA_HPP
#define DFPTL_JAVA_FROM_JAVA_HPP


#include "dfptl/core/subprocessing.hpp"
#include "dfptl/routing/mutable_frame.hpp"
#include "dfptl/java/detail/processor_descriptor.hpp"

#include <stl/span.hpp>


namespace dfp
{
namespace java
{
inline namespace sources
{
///@addtogroup java_grp
///@{
/**
* @defgroup release_sample_grp "release sample" sink
* @brief Releases one single sample from its single input.
*
* A @ref release_sample_grp pulls one single sample from its single input port when its \c run() method is invoked.
* It does not apply any transformation on captured samples.
*
* **Sink interface**
* @startuml
* rectangle "i(k-1) <- run() | with k the count of run() calls" <<sample_releaser()>> as sink
* interface "input-0: i(0) i(1) ...i(n)" <<in>> as in
* sink <-up- in
* @enduml
*
* port name    | location    | stream specification          | description
* -------------|-------------|-------------------------------|------------
* "in"         | **input-0** | @ref atomic_stream            | consumes the samples to release
* @{
**/


#define DFP_JAVA_FROM_JAVA java::from_java


template<int InputRank, class InputJsample>
class from_java_processing
{
    upg::span<InputJsample> input_span_;

public:
    struct tag_type : custom_tag
    { struct from_java_descriptor : std::pair<int_const<InputRank>, InputJsample> {}; };

    template<class InitStorageView>
    void initialize_upwards(InitStorageView init_storage)
    {
        static_assert(
            boost::fusion::has_key<detail::processor_descriptor>(init_storage),
            "Usage of a `" DFP_STRINGIFY(DFP_JAVA_FROM_JAVA) "` source requires a `java::to_java` sink to be present in the processing tree."
        );

        detail::processor_descriptor& descriptor = boost::fusion::at_key<detail::processor_descriptor>(init_storage);
        detail::processor_context& context = *descriptor.context;
        std::size_t const source_rank = descriptor.next_source_rank++;
        using any_sample = detail::processor_context::any_sample;

        context.input_spans[source_rank] = reinterpret_cast<upg::span<any_sample>*>(&input_span_);
    }

    auto operator()() ->
    decltype(routing::mutable_frame(input_span_).build())
    { return routing::mutable_frame(input_span_).build(); }
}; // class from_java_processing

/**
* @brief Creates a placeholder for a @ref release_sample_grp.
*
* **Usage example**
* @snippet routing_test.cpp sample_releaser usage example
**/
template<std::size_t InputRank, class InputJsample>
constexpr inline auto from_java(size_t_const<InputRank> input_rank, type_meta<InputJsample> input_jsample_meta = type_meta<InputJsample>{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<from_java_processing<InputRank, InputJsample>>, core::detail::subprocessing_source>(from_java_processing<InputRank, InputJsample>{})) /** @endcond */
{ return make_placeholder<std::tuple<from_java_processing<InputRank, InputJsample>>, core::detail::subprocessing_source>(from_java_processing<InputRank, InputJsample>{}); }


template<std::size_t InputRank, class InputJsample>
constexpr inline auto from_java(type_meta<InputJsample> input_jsample_meta = type_meta<InputJsample>{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<from_java_processing<InputRank, InputJsample>>, core::detail::subprocessing_source>(from_java_processing<InputRank, InputJsample>{})) /** @endcond */
{ return make_placeholder<std::tuple<from_java_processing<InputRank, InputJsample>>, core::detail::subprocessing_source>(from_java_processing<InputRank, InputJsample>{}); }


///@} from_java_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
///@addtogroup from_java_grp
///@{
using java::sources::from_java;
///@} to_java_grp
} // namespace sources
} // namespace shortname


///@} java_grp
} // namespace java
} // namespace dfp


#endif //DFPTL_JAVA_FROM_JAVA_HPP
