/**
* @file math/abs.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_ABS_HPP
#define DFPTL_MATH_ABS_HPP


#include "dfptl/core/apply.hpp"

#include <cmath>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup abs_grp
///@{
///@defgroup abs_functor absolute-value function-object
///@{

/**
* @anchor tpl_abs
* @brief Function-object computing absolute value of its @c arg input. Effectively calls
* [std::abs(int)](https://en.cppreference.com/w/cpp/numeric/math/abs), [std::abs(float)](https://en.cppreference.com/w/cpp/numeric/math/fabs) or
* [std::abs(complex)](https://en.cppreference.com/w/cpp/numeric/complex/abs) depending of the input type
*
* @sa **partial specializations**
* @li @ref void_abs "whether T is void"
*
* @tparam T is the type of the absolute function argument. It shall
* [be an arithmetic](https://en.cppreference.com/w/cpp/types/is_arithmetic) type or @ref is_complex "be a complex type".
**/
template<class T = void, class = void>
struct abs
{
    /**
    * @brief Returns the absolute value of @c arg
    *
    * @param arg input value
    **/
    auto operator()(T const& arg) const /**@cond */ ->
    decltype(std::abs(arg)) /** @endcond */
    { return std::abs(arg); }
};


/**
* @anchor void_abs
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa @ref tpl_abs "abs template class"
**/
template<>
struct abs<void>
{
    /**
    * @brief Returns the absolute value of @c arg
    *
    * @param arg input value
    **/
    template<class U>
    auto operator()(U const& arg) const /** @cond */ ->
    decltype(abs<U>()(arg)) /** @endcond */
    { return abs<U>()(arg); }
};


///@} abs_functor
///@} abs_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup abs_grp "abs" node
* @brief Computes absolute value of its input samples.
*
* A @ref abs_grp computes absolute value of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::abs(i0(n))" <<abs()>> as op
* interface "input-0: 1 -2 ...i0(n)" <<INPUT>> as in0
* interface "output: 1 2 ...o(n)" <<operation result>> as out
* op <-up- in0
* out <-up- op
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to compute absolute values
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a builder for a @ref abs_grp computing absolute value.
*
* **Usage example**
* @snippet math_test.cpp abs usage example
**/
inline auto abs() /** @cond */ ->
decltype(apply<functions::abs<>>()) /** @endcond */
{ return apply<functions::abs<>>(); }


///@} abs_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::abs;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_ABS_HPP
