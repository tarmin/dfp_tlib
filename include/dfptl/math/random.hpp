/**
* @file math/random.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_RANDOM_HPP
#define DFPTL_MATH_RANDOM_HPP


#include "dfptl/core/placeholder.hpp"
#include "dfptl/core/default_settings.hpp"
#include "dfptl/math/detail/random_source.hpp"

#include <random>
#include "stl/type_traits.hpp"
#include "stl/experimental/type_traits.hpp"


namespace dfp
{
namespace math
{
///@addtogroup math_grp
///@{
/**
* @defgroup random_grp "random" source
* @brief Produces a @ref atomic_stream "atomic stream" of random numerical values.
*
* **Source interface**
* @startuml
* rectangle "o(n) = random value" <<random<>()>> as src
* interface "output: o(0) o(1) ...o(n)" <<produced>> as out
* out <-up- src
* @enduml
* * @c "produced" **output** :
*   * a @ref atomic_stream "atomic stream" conveying the random sample
* @{
**/


/**
* @brief Provides typedef type with the uniform distribution applied on random number generator
* for the given @c Sample type.
*
* the type is set as follow depending on the @c Sample template parameter:
* | @c Sample      | uniform distribution |
* |----------------|----------------------|
* | Integer types except bool and char | std::uniform_int_distribution |
* | Real types     | std::uniform_real_distribution |
* | Boolean types  | std::bernoulli_distribution    |
*
* @remark @c Sample template parameter shall be an arithmetic but not char type or compile-time failure will happen.
*
* @sa random()
**/
template<class Sample, class = void>
struct uniform_random_distribution
{
    static_assert(
        std::is_arithmetic<Sample>{} && !is_char<Sample>{},
        "Sample template parameter shall be an arithmetic type but not a char or unsigned char"
    );
};

template<class Sample>
struct uniform_random_distribution<Sample, DFP_C_REQUIRES((std::is_floating_point<Sample>{}))>
{ typedef std::uniform_real_distribution<Sample> type; };

template<class Sample>
struct uniform_random_distribution<Sample,DFP_C_REQUIRES((std::is_integral<Sample>{} && !is_bool<Sample>{} && !is_char<Sample>{}))>
{ typedef std::uniform_int_distribution<Sample> type; };

template<class Sample>
struct uniform_random_distribution<Sample, DFP_C_REQUIRES((is_bool<Sample>{}))>
{ typedef std::bernoulli_distribution type; };


/**
* @brief alias for `typename uniform_random_distribution<Sample>::type`
**/
template<class Sample>
using uniform_random_distribution_t = typename uniform_random_distribution<Sample>::type;


/**
* @brief Provides the default seed value associated to the given random generator engine or engine adaptor.
*
* The default seed value is given as a compile-time constant by making this class inheriting from
* [integral_constant](https://en.cppreference.com/w/cpp/types/integral_constant)
**/
template<class Engine, class = void>
struct default_seed
{ static_assert(always_false<Engine>{}, "unexpected issue, please raise it at https://gitlab.com/tarmin/dfp_tlib/-/issues"); };

// if Engine is actually an Engine adaptor, the default_seed can be found in base engine
template<class Engine>
struct default_seed<Engine, DFP_C_REQUIRES((always_true<decltype(std::remove_cvref_t<decltype(std::declval<Engine>().base())>::default_seed)>{}))>
 :  std::integral_constant<typename Engine::result_type, std::remove_cvref_t<decltype(std::declval<Engine>().base())>::default_seed> {};

template<class Engine>
struct default_seed<Engine, DFP_C_REQUIRES((always_true<decltype(Engine::default_seed)>{}))>
 :  std::integral_constant<typename Engine::result_type, Engine::default_seed> {};


///@} random_grp
///@} math_grp


inline namespace sources
{
///@addtogroup math_grp
///@{
///@addtogroup random_grp
///@{


/**
* @brief Creates a placeholder for a @ref random_grp given some engine initialization parameter and number distribution.
*
* **Usage example**
* @snippet math_test.cpp random source usage example
*
* @param engine_param is parameter for random generator engine initialization with its constructor
* @param distribution specifies the statistical probability density function of the random samples
*
* @tparam Engine is type of the random generator engine.
* @tparam EngineParam type is implicitly deduced from @c engine_param parameter.
* @tparam Distribution type is implicity deduced from @c distribution parameter.
*
* @sa shortname::rand()
**/
template<
    class Engine = std::default_random_engine,
    class EngineParam = typename default_seed<Engine>::value_type,
    class Distribution = uniform_random_distribution_t<default_sample_t<>>,
    DFP_F_REQUIRES((always_true<decltype(Engine(std::declval<EngineParam>()))>{}))
>
inline auto random(EngineParam&& engine_param, Distribution&& distribution = Distribution{}) /** @cond */ ->
decltype(make_placeholder<std::tuple<Engine, EngineParam, Distribution>, detail::random_source>(std::forward<EngineParam>(engine_param), std::forward<Distribution>(distribution))) /** @endcond */
{ return make_placeholder<std::tuple<Engine, EngineParam, Distribution>, detail::random_source>(std::forward<EngineParam>(engine_param), std::forward<Distribution>(distribution)); }


/**
* @brief Alias for @c random<Engine>(default_seed<Engine>::value, std::forward<Distribution>(distribution))
*
* @remark @c <> may be required when using @c random<>() to avoid confusion with the `random` function of stdlib.h
*
* @sa shortname::rand()
**/
template<
    class Engine = std::default_random_engine,
    class Distribution = uniform_random_distribution_t<default_sample_t<>>,
    // shaky code below : aims at enabling function if Distribution looks like a random number distribution type
    // should rather fully defines a random distribution concept
    DFP_F_REQUIRES((always_true<typename Distribution::param_type>{}))
>
inline auto random(Distribution&& distribution = Distribution{}) /** @cond */ ->
decltype(random<Engine>(default_seed<Engine>::value, std::forward<Distribution>(distribution))) /** @endcond */
{ return random<Engine>(default_seed<Engine>::value, std::forward<Distribution>(distribution)); }


///@} random_grp
///@} math_grp
} // namespace sources


namespace shortname
{
inline namespace sources
{
///@addtogroup math_grp
///@{
///@addtogroup random_grp
///@{


/**
* @brief Alias for @c random<Engine>(engine_param, std::forward<Distribution>(distribution))
*
* @sa random()
**/
template<
    class Engine = std::default_random_engine,
    class EngineParam = typename default_seed<Engine>::value_type,
    class Distribution = uniform_random_distribution_t<default_sample_t<>>
>
constexpr auto rand(
    EngineParam&& engine_param, Distribution&& distribution = Distribution{}
) /** @cond */ ->
decltype(random<Engine>(engine_param, std::forward<Distribution>(distribution))) /** @endcond */
{ return random<Engine>(engine_param, std::forward<Distribution>(distribution)); }


/**
* @brief Alias for @c random<Engine>(std::forward<Distribution>(distribution))
*
* @remark @c <> may be required when using @c rand<>() to avoid confusion with the @c std::rand() function of cstdlib
*
* @sa random()
**/
template<
    class Engine = std::default_random_engine,
    class Distribution = uniform_random_distribution_t<default_sample_t<>>,
    // shaky code below : aims at enabling function if Distribution looks like a random number distribution type
    // should rather fully defines a random distribution concept
    DFP_F_REQUIRES((always_true<typename Distribution::param_type>{}))
>
constexpr auto rand(Distribution&& distribution = Distribution{}) /** @cond */ ->
decltype(random<Engine>(std::forward<Distribution>(distribution))) /** @endcond */
{ return random<Engine>(std::forward<Distribution>(distribution)); }


///@} random_grp
///@} math_grp
} // namespace sources
} // namespace shortname
} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_RANDOM_HPP
