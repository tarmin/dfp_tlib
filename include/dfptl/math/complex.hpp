/**
* @file math/complex.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_COMPLEX_HPP
#define DFPTL_MATH_COMPLEX_HPP


#include "dfptl/core/apply.hpp"

#include <complex>


namespace dfp
{
namespace math
{


/**
* @brief Provides constant member @c value equal to @c true if given @c T is a std::complex object.
**/
template<class T, class = void>
struct is_complex : std::false_type {};

template<class T>
struct is_complex<T, DFP_C_REQUIRES((std::is_base_of<T, std::complex<typename T::value_type>>{}))> : std::true_type {};


namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup complex_grp
///@{
///@defgroup complex_functor complex number function-object
///@{

/**
* @anchor tpl_complex
* @brief Function-object for creation of complex number given real and imaginary parts or another complex number.
* Effectively calls [std::complex](https://en.cppreference.com/w/cpp/numeric/complex)
*
* @sa **partial specializations**
*   * @ref floating_p_complex "whether T is floating point type"
*   * @ref void_complex "whether T is void"
*
* @tparam T is the type of the complex function argument. It shall be a
* [floating-point](https://en.cppreference.com/w/cpp/types/is_floating_point)
* or a [complex](https://en.cppreference.com/w/cpp/numeric/complex) type.
**/
template<class T = void, class = void> struct complex
{ static_assert(
    std::is_void<T>{} || std::is_floating_point<T>{} || is_complex<T>{},
    "template parameter T of the complex class shall be a floating-point type, a complex number type or void"
);};

/**
* @anchor floating_p_complex
* @brief partial specialization whether template argument @c T
* [is a floating point](https://en.cppreference.com/w/cpp/types/is_floating_point) type.
* @sa the @ref tpl_complex "complex template function-object"
**/
template<class T>
struct complex<T, DFP_C_REQUIRES((std::is_floating_point<T>{}))>
{
    /**
    * @brief Returns a complex number object with the given `real` part and `imag`inary part.
    **/
    std::complex<T> operator()(T const& real, T const& imag)
    { return std::complex<T>(real, imag); }

    /**
    * @brief Returns a complex number object resulting from the conversion of the given input `value`
    **/
    template<class U>
    std::complex<T> operator()(std::complex<U> const& value)
    { return std::complex<T>{value}; }
};


/**
* @anchor void_complex
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_cos "complex template function-object"
**/
template<>
struct complex<void>
{
    /**
    * @brief Returns a complex number object with the given `real` part and `imag`inary part.
    **/
    template<class T>
    auto operator()(T const& real, T const& imag) const ->
    decltype(complex<T>()(real, imag))
    { return complex<T>()(real, imag); }

    /**
    * @brief Returns a complex number object resulting from the conversion of the given input `value`
    **/
    template<class T>
    auto operator()(std::complex<T> const& value) const ->
    decltype(complex<T>()(value))
    { return complex<T>()(value); }
}; // struct complex<void>


///@addtogroup cos_functor
///@{
template<class, class> struct cos;

/**
* @anchor floating_p_cos
* @brief partial specialization whether template argument @c T
* [is a complex](https://en.cppreference.com/w/cpp/numeric/complex) type.
* @sa the @ref tpl_cos "cos template function-object"
**/
template<class T>
struct cos<T, DFP_C_REQUIRES((is_complex<T>{}))>
{
    /**
    * @brief Returns the cosine of @c angle (expressed in radians)
    **/
    T operator()(T const& angle) const
    { return std::cos(angle); }
}; // struct cos<complex>
///@} cos_functor


///@addtogroup abs_functor
///@{
template<class, class> struct abs;

/**
* @anchor complex_abs
* @brief partial specialization whether template argument @c T
* @ref is_complex "is a complex" type.
* @sa @ref tpl_abs "abs template class"
**/
template<class T>
struct abs<T, DFP_C_REQUIRES((is_complex<T>{}))>
{
    /**
    * @brief Returns the absolute value of @c arg
    **/
    typename T::value_type operator()(T const& arg) const
    { return std::abs(arg); }
}; // abs<is_complex>
///@} abs_functor


///@} complex_functor
///@} complex_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup complex_grp "complex number" node
* @brief Creates complex number given real and imaginary part on its inputs.
*
* A @ref complex_grp creates a complex given the real part on its 1st input and the imaginary part on its 2nd
* **or** converts complex number on its single input to the given complex type.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* * for complex number creation
* @startuml
* rectangle "o(n) = std::complex<std::common_type<...>>(i0(n), i1(n))" <<complex()>> as ope
* interface "input-0: 1 3 ...i0(n)" <<REAL_PART>> as in0
* interface "input-1: 0 2 ...i1(n)" <<IMAG_PART>> as in1
* interface "output: 1 3+2i ..o(n)" <<operation result>> as out
* ope <-up- in0
* ope <-up- in1
* out <-up- ope
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* REAL_PART          | **input-0** | @ref atomic_stream "atomic stream" conveying sample representing the real part of the number
* IMAG_PART          | **input-1** | @ref atomic_stream "atomic stream" conveying sample representing the imaginary part of the number
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the complex number composed by the real and the imaginary parts provided
*
* * for complex number conversion
* @startuml
* rectangle "o(n) = std::complex<T>(i0(n))" <<complex<T>()>> as ope
* interface "input-0: 1.0f+2.0f*i 3.0f ...i0(n)" <<in>> as in0
* interface "output: 1.0+2.0*i 3.0 ...o0(n)" <<operation result>> as out
* ope <-up- in0
* out <-up- ope
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying the complex number to convert
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the complex number resulting from conversion
*
* @sa real_grp, imag_grp
* @{
**/


/**
* @brief Creates a builder for a @ref complex_grp.
*
* **Usage example**
* @snippet math_test.cpp complex usage example
**/
template<class T = void>
inline auto complex() /** @cond */ ->
decltype(apply<functions::complex<T>>()) /** @endcond */
{ return apply<functions::complex<T>>(); }


///@} to_complex_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup complex_grp
///@{
/**
* @brief shortname for @ref complex<T>()
* @return 
**/
template<class T = void>
inline auto cplx() /** @cond */ ->
decltype(complex<T>()) /** @endcond */
{ return complex<T>(); }
///@}
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_COMPLEX_HPP
