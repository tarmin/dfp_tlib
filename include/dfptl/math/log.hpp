/**
* @file math/log.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_LOG_HPP
#define DFPTL_MATH_LOG_HPP


#include "dfptl/core/apply.hpp"

#include <cmath>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup log_grp
///@{
///@defgroup log_functor logarithm function-object
///@{

/**
* @anchor tpl_log
* @brief Function-object computing logarithm of its @c arg input. Effectively calls
* [std::log](https://en.cppreference.com/w/cpp/numeric/math/log)
*
* @sa **partial specializations**
* @li @ref void_log "whether T is void"
* @li @ref integral_log "whether T is integral type"
*
* @tparam T is the type of the logarithm function argument. It shall
* [be an arithmetic](https://en.cppreference.com/w/cpp/types/is_arithmetic) type.
**/
template<class T = void, class = void>
struct log
{
    /**
    * @brief Returns the logarithm of @c arg
    *
    * @param arg input value
    **/
    auto operator()(T arg) const /** @cond */ ->
    decltype(std::log(arg)) /** @endcond */
    { return std::log(arg); }
};


/**
* @anchor void_log
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa @ref tpl_log "log template class"
**/
template<>
struct log<void>
{
    /**
    * @brief Returns the logarithm of @c arg
    *
    * @param arg input value
    **/
    template<class U>
    auto operator()(U arg) const /** @cond */ ->
    decltype(log<U>()(arg)) /** @endcond */
    { return log<U>()(arg); }
};


///@} log_functor
///@} log_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup log_grp "log" node
* @brief Computes natural logarithm of its input samples.
*
* A @ref log_grp performs a logarithm of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::log(i0(n))" <<log()>> as op
* interface "input-0: 1 e e*e ...i0(n)" <<INPUT>> as in0
* interface "output: 0 1 2 ...o(n)" <<operation result>> as out
* op <-up- in0
* out <-up- op
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to compute the logarithm
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a builder for a @ref log_grp computing a natural (base @c e) logarithm.
*
* **Usage example**
* @snippet math_test.cpp log usage example
**/
inline auto log() /** @cond */ ->
decltype(apply<functions::log<>>()) /** @endcond */
{ return apply<functions::log<>>(); }


///@} log_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::log;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_LOG_HPP
