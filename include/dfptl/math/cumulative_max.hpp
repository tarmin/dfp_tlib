/**
* @file math/cumulative_max.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_CUMULATIVE_MAX_HPP
#define DFPTL_MATH_CUMULATIVE_MAX_HPP


#include "dfptl/math/max.hpp"
#include "dfptl/alu/accumulate.hpp"
#include "dfptl/core/subprocessing.hpp"


namespace dfp
{
namespace math
{
inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup cummax_grp "cumulative max" node
* @brief Returns the cumulative maximum value over all samples reaching its SINGLE input.
*
* A @ref cummax_grp computes the maximum value over all samples reaching its SINGLE input produces the result on its output.
*
* **Node interface**
* @startuml
* rectangle "o(n) = cumulative max of i0(n)" <<cumulative_max()>> as op
* interface "input-0: -1 -2 2 3 1 ...i0(n)" <<INPUT0>> as in0
* interface "output: -1 -1 2 3 3 ...o(n)" <<operation result>> as out
* op <-up- in0
* out <-up- op
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT0             | **input-0** | @ref atomic_stream "atomic stream" conveying the samples onto extract the maximum value
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
*
* @sa max()
*
* @warning @ref cummax_grp instance is not "moveable" (not move-constructible) when runnable
* @{
**/


//TODO: add support for generic resetable pattern
struct cummax_processing
{
    template<class Input>
    auto operator()(Input in) const
#if not DFP_CPP14_SUPPORT
    ->
    decltype(alu::accumulate<functions::max>(std::numeric_limits<output_sample_t<Input>>::min()) <<= in)
#endif
    { return alu::accumulate<functions::max>(std::numeric_limits<output_sample_t<Input>>::min()) <<= in; }
};


/**
* @brief Creates a builder for a @ref cummax_grp returning the max value over all samples.
*
* **Usage example**
* @snippet math_test.cpp cummax usage example
**/
inline decltype(subprocessing(cummax_processing{})) cumulative_max()
{ return        subprocessing(cummax_processing{}); }


///@} cummax_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
///@addtogroup math_grp
///@{
///@addtogroup cummax_grp
///@{


/**
* @brief shortname for @ref cumulative_max()
**/
inline decltype(cumulative_max()) cummax()
{ return cumulative_max(); }


///@} cummax_grp
///@} math_grp
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_CUMULATIVE_MAX_HPP
