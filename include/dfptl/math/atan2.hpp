/**
* @file math/atan2.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_ATAN2_HPP
#define DFPTL_MATH_ATAN2_HPP


#include "dfptl/core/apply.hpp"

#include <cmath>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup atan2_grp
///@{
///@defgroup atan2_functor 2-argument arc tangent function-object
///@{

/**
* @anchor tpl_atan2
* @brief Function-object computing arc tangent of its 2 arguments ratio `x/y`. Effectively calls
* [std::atan2](https://en.cppreference.com/w/cpp/numeric/math/atan2)
*
* @sa **partial specializations**
*   * @ref void_atan2 "whether X and Y are void"
*
* @tparam Y is type of 1st @c std::atan2 argument
* @tparam X is type of 2nd @c std::atan2 argument
**/
template<class Y = void, class X = void, class = void> struct atan2
{
    /**
    * @brief Returns the 2-argument arc tangent of `x/y`
    **/
    auto operator()(Y y, X x) const /** @cond */ ->
    decltype(std::atan2(y, x)) /** @endcond */
    { return std::atan2(y, x); }
};


/**
* @anchor void_atan2
* @brief partial specialization whether template argument @c Y and @c X are @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_atan2 "atan2 template function-object"
**/
template<>
struct atan2<void, void>
{
    /**
    * @brief Returns the 2-argument arc tagent of `y/x`
    *
    * @param x
    * @param y
    **/
    template<class Y, class X>
    auto operator()(Y y, X x) const /** @cond */ ->
    decltype(atan2<Y, X>()(y, x)) /** @endcond */
    { return atan2<Y, X>()(y, x); }
};


///@} atan2_functor
///@} atan2_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup atan2_grp "2-argument arc tangent" node
* @brief Computes 2-argument arc tagent of ratio of its input samples (`y/x`).
*
* A @ref atan2_grp performs a 2-argumetn arc tagent of sample on its both inputs and produces the resulting angle on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::atan2(i0(n), i1(n))" <<atan2()>> as atan2
* interface "input-0: 1 0 -1 ...i0(n)" <<Y>> as in0
* interface "input-1: 0 1 0 ...i1(n)" <<X>> as in1
* interface "output: pi/2 0 -pi/2 ...o(n)" <<operation result>> as out
* atan2 <-up- in0
* atan2 <-up- in1
* out <-up- atan2
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* Y                  | **input-0** | @ref atomic_stream "atomic stream" conveying 1st argument `y` of  2-argument arc tagent
* X                  | **input-1** | @ref atomic_stream "atomic stream" conveying 2nd argument `x` of  2-argument arc tagent
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a cell builder for a @ref atan2_grp.
*
* **Usage example**
* @snippet math_test.cpp atan2 usage example
**/
inline auto atan2() /** @cond */ ->
decltype(apply<functions::atan2<>>()) /** @endcond */
{ return apply<functions::atan2<>>(); }


///@} atan2_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::atan2;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_ATAN2_HPP
