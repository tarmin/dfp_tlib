/**
* @file math/max.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_MAX_HPP
#define DFPTL_MATH_MAX_HPP


#include "dfptl/core/apply.hpp"

#include <algorithm>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup max_grp
///@{
///@defgroup max_functor greater-value function-object
///@{

/**
* @brief Function-object computing greater value of its both inputs. Effectively calls
* [std::max](https://en.cppreference.com/w/cpp/algorithm/max)
**/
struct max
{
    template<class... T, DFP_F_REQUIRES((sizeof...(T) != 2))>
    void operator()(T const&... a) const
    { static_assert(always_false<std::tuple<T...>>{}, "max functor expects exactly 2 input values"); }

    /**
    * @brief Returns the greater value of @c a and @c b
    **/
    template<class T>
    constexpr T const& operator()(T const& a, T const& b) const
    { return std::max(a, b); }
};


///@} max_functor
///@} max_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup max_grp "max" node
* @brief Computes greater value of samples reaching its both inputs.
*
* A @ref max_grp computes greater value of sample on its first and second input and produces the result on its output.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::max(i0(n), i1(n))" <<max()>> as op
* interface "input-0: 2 -2 ...i0(n)" <<INPUT0>> as in0
* interface "input-0: 1 2 ...i1(n)" <<INPUT1>> as in1
* interface "output: 2 2 ...o(n)" <<operation result>> as out
* op <-up- in1
* op <-up- in0
* out <-up- op
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT0             | **input-0** | @ref atomic_stream "atomic stream" conveying 1st sample of comparison for greater
* INPUT1             | **input-1** | @ref atomic_stream "atomic stream" conveying 2nd sample of comparison for greater
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
*
* @sa cumulative_max()
*
* @{
**/


/**
* @brief Creates a builder for a @ref max_grp computing greater value.
*
* **Usage example**
* @snippet math_test.cpp max usage example
**/
inline auto max() /** @cond */ ->
decltype(apply<functions::max>()) /** @endcond */
{ return apply<functions::max>(); }


///@} max_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::max;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_MAX_HPP
