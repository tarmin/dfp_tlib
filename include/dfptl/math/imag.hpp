/**
* @file math/imag.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_IMAG_HPP
#define DFPTL_MATH_IMAG_HPP


#include "dfptl/core/apply.hpp"

#include <complex>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup imag_grp
///@{
///@defgroup imag_functor imag function-object
///@{

/**
* @anchor tpl_imag
* @brief Function-object providing imag component of a complex number. Effectively calls
* [std::imag](https://en.cppreference.com/w/cpp/numeric/complex/imag2)
*
* @sa **partial specializations**
*   * @ref void_imag "whether T is void"
*
* @tparam T is the type of the imag function argument.
**/
template<class T = void, class = void> struct imag
{
    /**
    * @brief Returns the imag component of complex @c z
    *
    * @param z input value
    **/
    auto operator()(T const& z) const /** @cond */ ->
    decltype(std::imag(z)) /** @endcond */
    { return std::imag(z); }
};


/**
* @anchor void_imag
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_imag "imag template function-object"
**/
template<>
struct imag<void>
{
    /**
    * @brief Returns the imag component of complex @c z
    *
    * @param z input value
    **/
    template<class U>
    auto operator()(U const& z) const /** @cond */ ->
    decltype(imag<U>()(z)) /** @endcond */
    { return imag<U>()(z); }
};


///@} imag_functor
///@} imag_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup imag_grp "imag" node
* @brief Returns imag component of its input samples.
*
* A @ref imag_grp extracts the imaginary component of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::imag(i0(n))" <<imag()>> as imag
* interface "input-0: 0+i -1 1-i ...i0(n)" <<in>> as in0
* interface "output: 1 -0 -1 ...o(n)" <<operation result>> as out
* imag <-up- in0
* out <-up- imag
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to extract the imaginary component
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
*
* @sa real_grp, complex_grp
*
* @{
**/


/**
* @brief Creates a builder for a @ref imag_grp.
*
* **Usage example**
* @snippet math_test.cpp imag usage example
**/
inline auto imag() /** @cond */ ->
decltype(apply<functions::imag<>>()) /** @endcond */
{ return apply<functions::imag<>>(); }


///@} imag_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::imag;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_IMAG_HPP
