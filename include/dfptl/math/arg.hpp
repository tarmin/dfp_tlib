/**
* @file math/arg.hpp
**/
/*
* Copyright (C) 2023 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_ARG_HPP
#define DFPTL_MATH_ARG_HPP


#include "dfptl/core/apply.hpp"

#include <complex>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup arg_grp
///@{
///@defgroup arg_functor arg function-object
///@{

/**
* @anchor tpl_arg
* @brief Function-object providing the phase angle (in radians) of a complex number. Effectively calls
* [std::arg](https://en.cppreference.com/w/cpp/numeric/complex/arg)
*
* @sa **partial specializations**
*   * @ref void_arg "whether T is void"
*
* @tparam T is the type of the phase angle function argument.
**/
template<class T = void, class = void> struct arg
{
    /**
    * @brief Returns the phase angle of complex @c z
    *
    * @param z input value
    **/
    auto operator()(T const& z) const /** @cond */ ->
    decltype(std::arg(z)) /** @endcond */
    { return std::arg(z); }
};


/**
* @anchor void_arg
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_arg "arg template function-object"
**/
template<>
struct arg<void>
{
    /**
    * @brief Returns the phae angle of complex @c z
    *
    * @param z input value
    **/
    template<class U>
    auto operator()(U const& z) const /** @cond */ ->
    decltype(arg<U>()(z)) /** @endcond */
    { return arg<U>()(z); }
};


///@} arg_functor
///@} arg_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup arg_grp "arg" node
* @brief Returns the phase angle (in radians) of its input samples.
*
* A @ref arg_grp extracts the phase angle of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::arg(i0(n))" <<arg()>> as arg
* interface "input-0: a*(cos(b)+sin(b)i) ...i0(n)" <<in>> as in0
* interface "output: b ...o(n)" <<operation result>> as out
* arg <-up- in0
* out <-up- arg
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to extract the phase angle
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
*
* @sa polar_grp, complex_grp
*
* @{
**/


/**
* @brief Creates a cell builder for a @ref arg_grp.
*
* **Usage example**
* @snippet math_test.cpp arg usage example
**/
inline auto arg() /** @cond */ ->
decltype(apply<functions::arg<>>()) /** @endcond */
{ return apply<functions::arg<>>(); }


///@} arg_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::arg;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_ARG_HPP
