/**
* @file math/real.hpp
**/
/*
* Copyright (C) 2021 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_REAL_HPP
#define DFPTL_MATH_REAL_HPP


#include "dfptl/core/apply.hpp"
#include "dfptl/math/complex.hpp"


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup real_grp
///@{
///@defgroup real_functor real function-object
///@{

/**
* @anchor tpl_real
* @brief Function-object providing real component of a complex number. Effectively calls
* [std::real](https://en.cppreference.com/w/cpp/numeric/complex/real2)
*
* @sa **partial specializations**
*   * @ref complex_real "whether T is complex type"
*   * @ref floating_p_real "whether T is floating point type"
*   * @ref integral_real "whether T is integral type"
*   * @ref void_real "whether T is void"
*
* @tparam T is the type of the real function argument.
**/
template<class T = void, class = void> struct real
{ static_assert(
    std::is_void<T>{} || is_complex<T>{} || std::is_arithmetic<T>{},
    "template parameter T of the real class shall be a complex type, an arithmetic type or void"
);};

/**
* @anchor complex_real
* @brief partial specialization whether template argument @c T @ref is_complex "is a complex type"
* @sa the @ref tpl_real "real template function-object"
**/
template<class T>
struct real<T, DFP_C_REQUIRES((is_complex<T>{}))>
{
    /**
    * @brief Returns the real component of complex @c z
    **/
    typename T::value_type operator()(T const& z) const
    { return std::real(z); }
};

/**
* @anchor floating_p_real
* @brief partial specialization whether template argument @c T
* [is a floating point](https://en.cppreference.com/w/cpp/types/is_floating_point) type.
* @sa the @ref tpl_real "real template function-object"
**/
template<class T>
struct real<T, DFP_C_REQUIRES((std::is_floating_point<T>{}))>
{
    /**
    * @brief Returns the real component of complex @c z
    **/
    T operator()(T const& z) const
    { return std::real(z); }
};

/**
* @anchor integral_real
* @brief partial specialization whether template argument @c T
* [is a integral](https://en.cppreference.com/w/cpp/types/is_integral) type.
* @sa the @ref tpl_real "real template function-object"
**/
template<class T>
struct real<T, DFP_C_REQUIRES((std::is_integral<T>{}))>
{
    /**
    * @brief Returns the real component of complex @c z
    **/
    double operator()(T const& z) const
    { return std::real(z); }
};

/**
* @anchor void_real
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_real "real template function-object"
**/
template<>
struct real<void>
{
    /**
    * @brief Returns the real component of complex @c z
    **/
    template<class U>
    auto operator()(U const& z) const /** @cond */ ->
    decltype(real<U>()(z)) /** @endcond */
    { return real<U>()(z); }
};


///@} real_functor
///@} real_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup real_grp "real" node
* @brief Returns real component of its input samples.
*
* A @ref real_grp extracts the real component of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = real(i0(n))" <<real()>> as real
* interface "input-0: 0+i -1 1+i ...i0(n)" <<in>> as in0
* interface "output: 0 -1 1 ...o(n)" <<operation result>> as out
* real <-up- in0
* out <-up- real
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to extract the real component
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
*
* * @sa imag_grp, complex_grp
*
* @{
**/


/**
* @brief Creates a builder for a @ref real_grp.
*
* **Usage example**
* @snippet math_test.cpp real usage example
**/
inline auto real() /** @cond */ ->
decltype(apply<functions::real<>>()) /** @endcond */
{ return apply<functions::real<>>(); }


///@} real_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::real;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_REAL_HPP
