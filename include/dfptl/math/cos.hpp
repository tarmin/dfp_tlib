/**
* @file math/cos.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_COS_HPP
#define DFPTL_MATH_COS_HPP


#include "dfptl/core/apply.hpp"

#include <cmath>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup cos_grp
///@{
///@defgroup cos_functor cosine function-object
///@{

/**
* @anchor tpl_cos
* @brief Function-object computing cosine of its @c angle input. Effectively calls
* [std::cos](https://en.cppreference.com/w/cpp/numeric/math/cos)
*
* @sa **partial specializations**
*   * @ref void_cos "whether T is void"
*
* @tparam T is the type of the cosine function argument. It shall
* [be an arithmetic](https://en.cppreference.com/w/cpp/types/is_arithmetic) type.
**/
template<class T = void, class = void>
struct cos
{
    /**
    * @brief Returns the cosine of @c angle (expressed in radians)
    *
    * @param angle input value
    **/
    auto operator()(T angle) const /** @cond */ ->
    decltype(std::cos(angle)) /** @endcond */
    { return std::cos(angle); }
};


/**
* @anchor void_cos
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa the @ref tpl_cos "cos template function-object"
**/
template<>
struct cos<void>
{
    /**
    * @brief Returns the cosine of @c angle (expressed in radians)
    *
    * @param angle input value
    **/
    template<class U>
    auto operator()(U const& angle) const /** @cond */ ->
    decltype(cos<U>()(angle)) /** @endcond */
    { return cos<U>()(angle); }
};


///@} cos_functor
///@} cos_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup cos_grp "cosine" node
* @brief Computes cosine of its input samples (measured in radians).
*
* A @ref cos_grp performs a cosine of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from its input sample type and the C++ standard rule for this mathematical operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::cos(i0(n))" <<cos()>> as cos
* interface "input-0: 0 pi/2 pi ...i0(n)" <<in>> as in0
* interface "output: 1 0 -1 ...o(n)" <<operation result>> as out
* cos <-up- in0
* out <-up- cos
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to compute the cosine
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a builder for a @ref cos_grp.
*
* **Usage example**
* @snippet math_test.cpp cos usage example
**/
inline auto cos() /** @cond */ ->
decltype(apply<functions::cos<>>()) /** @endcond */
{ return apply<functions::cos<>>(); }


///@} cos_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::cos;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_COS_HPP
