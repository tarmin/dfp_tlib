/**
* @file random_source.hpp
* This is an internal header file, included by other library headers. Do not attempt to use it directly.
**/
/**
* Copyright (C) 2018 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_DETAIL_RANDOM_SOURCE_HPP
#define DFPTL_MATH_DETAIL_RANDOM_SOURCE_HPP


#include "dfptl/core/source.hpp"

#include "stl/type_traits.hpp"


namespace dfp
{
namespace math
{
namespace detail
{
///@addtogroup math_detail
///@{


/**
* @brief Sample-based source which generates some pseudo random number
*
* @param OutSample is the type of the produced samples.
**/
//TODO: define Engine and Distribution concept and static_assert
template<class Context, class Engine, class EngineParam, class Distribution>
class random_source
 :  public  source<
        Context, atomic_stream_specifier<typename Distribution::result_type>, random_source<Context, Engine, EngineParam, Distribution>
    >
{
    typedef source<
        Context, atomic_stream_specifier<typename Distribution::result_type>, random_source
    > source_type;

public:
    template<class DistributionF>
    random_source(Context const& context, EngineParam const& engine_param, DistributionF&& distribution)
        :   source_type(context), engine_(engine_param), distribution_(std::forward<DistributionF>(distribution)) {}

    output_sample_t<source_type> acquire_sample() noexcept
    { return distribution_(engine_); }

private:
    Engine engine_;
    Distribution distribution_;
}; // class random_source


///@} math_detail
} // namespace detail
} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_DETAIL_RANDOM_SOURCE_HPP
