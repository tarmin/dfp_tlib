/**
* @file math/sin.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_SIN_HPP
#define DFPTL_MATH_SIN_HPP


#include "dfptl/core/apply.hpp"

#include <cmath>


namespace dfp
{
namespace math
{
namespace functions
{
///@addtogroup math_grp
///@{
///@addtogroup sin_grp
///@{
///@defgroup sin_functor sine function-object
///@{


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#endif
/**
* @anchor tpl_sin
* @brief Function-object computing sine of its @c angle input. Effectively calls
* [std::sin](https://en.cppreference.com/w/cpp/numeric/math/sin)
*
* @sa **partial specializations**
*   * @ref void_sin "whether T is void"
*
* @tparam T is the type of the sine function argument. It shall
* [be an arithmetic](https://en.cppreference.com/w/cpp/types/is_arithmetic) type.
**/
template<class T = void, class = void>
struct sin
{
    /**
    * @brief Returns the sine of @c angle (expressed in radians)
    *
    * @param angle input value
    **/
    auto operator()(T const& angle) const /** @cond */ ->
    decltype(std::sin(angle)) /** @endcond */
    { return std::sin(angle); }
};


/**
* @anchor void_sin
* @brief partial specialization whether template argument @c T is @c void.
*
* Automatically deduced output type based on input argument of @c operator() method
* @sa @ref tpl_sin "sin template class"
**/
template<>
struct sin<void>
{
    /**
    * @brief Returns the sine of @c angle (expressed in radians)
    *
    * @param angle input value
    **/
    template<class U>
    auto operator()(U const& angle) const /** @cond */ ->
    decltype(sin<U>()(angle)) /** @endcond */
    { return sin<U>()(angle); }
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif


///@} sin_functor
///@} sin_grp
///@} math_grp
} // namespace functions


inline namespace nodes
{
///@addtogroup math_grp
///@{
/**
* @defgroup sin_grp "sine" node
* @brief Computes sine of its input samples (measured in radians).
*
* A @ref sin_grp performs a sine of sample on its single input and produces the result on its output.
*
* The output sample type is automatically inferred from the input sample types and the C++ standard rule for this arithmetic operator applies.
*
* **Node interface**
* @startuml
* rectangle "o(n) = std::sin(i0(n))" <<sin()>> as sin
* interface "input-0: 0 pi/2 pi ...i0(n)" <<in>> as in0
* interface "output: 0 1 0 ...o(n)" <<operation result>> as out
* sin <-up- in0
* out <-up- sin
* @enduml
*
* stream name        | location    | description
* -------------------|-------------|------------
* INPUT              | **input-0** | @ref atomic_stream "atomic stream" conveying sample onto to compute the sine
* "operation result" | **output**  | @ref atomic_stream "atomic stream" conveying the operation result
* @{
**/


/**
* @brief Creates a builder for a @ref sin_grp.
*
* **Usage example**
* @snippet math_test.cpp sin usage example
**/
inline auto sin() /** @cond */ ->
decltype(apply<functions::sin<>>()) /** @endcond */
{ return apply<functions::sin<>>(); }


///@} sin_grp
///@} math_grp
} // namespace nodes


namespace shortname
{
inline namespace nodes
{
using math::nodes::sin;
} // namespace nodes
} // namespace shortname


} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_SIN_HPP
