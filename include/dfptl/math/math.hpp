/**
* @file math/math.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_MATH_MATH_HPP
#define DFPTL_MATH_MATH_HPP


#include "dfptl/math/cos.hpp"
#include "dfptl/math/sin.hpp"
#include "dfptl/math/log.hpp"
#include "dfptl/math/abs.hpp"
#include "dfptl/math/max.hpp"
#include "dfptl/math/arg.hpp"
#include "dfptl/math/real.hpp"
#include "dfptl/math/imag.hpp"
#include "dfptl/math/atan2.hpp"
#include "dfptl/math/random.hpp"
#include "dfptl/math/complex.hpp"
#include "dfptl/math/cumulative_max.hpp"


namespace dfp
{
/**
* @defgroup math_grp Mathematical processing cells
* @brief Defines processing nodes applying classic mathematical operations and some usual sources only depending on those nodes.
* @{
**/
/**
* @defgroup math_detail implementation details
* @brief Implementation details and boilerplate for @ref math_grp
**/
/**
* @brief Contains items related to processing cells for classic mathematical operations
**/
namespace math
{
/**
* @brief Namespace of function-objects computing the mathematical operations.
**/
namespace functions {}

/**
* @brief Set of helper functions for nodes applying Mathematical operations.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for sources producing waveform from Mathematical functions.
**/
inline namespace sources {}
namespace leaves = sources;

/** @brief short name of functions in @ref math_grp namespace **/
namespace shortname {}


///@}
} // namespace math
} // namespace dfp


#endif // DFPTL_MATH_MATH_HPP
