/**
* @file windowing/windowing.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_WINDOWING_WINDOWING_HPP
#define DFPTL_WINDOWING_WINDOWING_HPP


#include "dfptl/windowing/hann.hpp"
#include "dfptl/windowing/blackman_nuttall.hpp"


namespace dfp
{
/**
* @defgroup windowing_grp Windowing processing cells
* @brief Defines processing nodes applying signal windowing and sources producing frame of window functions.
*
* @{
*
* @defgroup windowing_detail implementation details
* @brief Implementation details and boilerplate for @ref windowing_grp
**/


/**
* @brief Contains items related to processing cells for signal windowning.
**/
namespace windowing
{
/**
* @brief Set of helper functions for windowing nodes.
**/
inline namespace nodes {}
namespace knots = nodes;

/**
* @brief Set of helper functions for sources of window-function frame.
**/
inline namespace sources {}
namespace leaves = sources;


///@} windowing_grp
} // namespace windowing
} // namespace dfp


#endif // DFPTL_WINDOWING_WINDOWING_HPP
