/**
* @file windowing/hann.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_WINDOWING_HANN_HPP
#define DFPTL_WINDOWING_HANN_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/alu/multiply.hpp"
#include "dfptl/routing/to_single_sample.hpp"
#include "dfptl/routing/const_frame.hpp"

#include <cmath>


namespace dfp
{
namespace windowing
{
namespace detail
{


template<class Iterator, class TapsCount, class = DFP_CONCEPT_ASSERT((is_input_iterator<Iterator>))>
inline void initialize_hann_frame(Iterator begin_it, TapsCount taps_count)
{
    auto const N = taps_count - 1;

    for(std::size_t i = 0 ; i < taps_count ; i++)
    { *begin_it++ = 0.5 * ( 1 - std::cos(2*M_PI*i/N) ); };
}


} // namespace detail


///@addtogroup windowing_grp
///@{
/**
* @defgroup hann_grp Hann function
*
* @brief Provides processing source and node related to signal windowing with the
* [Hann function](https://en.wikipedia.org/wiki/Hann_function).
* @{
**/
inline namespace sources
{
///@addtogroup hann_grp
///@{
/**
* @defgroup hann_frame_grp "Hann frame" source
* @brief Produces frame of the Hann function signal.
*
* A @ref hann_frame_grp is a frame-based source producing on its output a frame of the Hann function.
*
* **Source interface**
* @startuml
* rectangle "o(n) = hann_f(n % N)" <<hann_frame(N)>> as fct
* interface "output: [o(0) ...o(N-1)][o(0)..." <<function result>> as out
* out <- fct
* @enduml
* * <tt>"function result" **output**</tt> : @ref core::detail::stream "stream" conveying a frame of 'double'-type sample
*
* @sa hann_window_grp
* @{
**/


template<class T>
inline decltype(routing::const_frame({0, 0})) hann_frame(T taps_count)
{
    static_assert(always_false<T>{}, "taps_count argument has invalid type. It shall be std::size_t or size_t_const");

    return routing::const_frame({0, 0});
}


/**
* @brief Creates a @ref hann_frame_grp given a frame length as integral constant.
*
* Sample type for the produced frame is @c double.
* As the frame length is resolved at compile-time, the created source has an optimized usage of the memory ressource.
*
* @param taps_count is the function length (resolved at compile-time).
*
* @tparam TapsCount is function length (usually deduced) from @c taps_count parameter.
*
* **Usage example**
* @snippet windowing_test.cpp hann_frame usage example with integral constant
*
* @todo add support for float output type.
**/
template<size_t TapsCount>
inline auto hann_frame(size_t_const<TapsCount> = size_t_const<TapsCount>{}) /** @cond */ ->
decltype(routing::const_frame(std::declval<std::array<double, TapsCount> const&>())) /** @endcond */
{
    //TODO: make it constexpr if TapsCount is not too large (e.g. < 256). Consider to dev a "constexpr for-loop" for this
    static std::array<double, TapsCount> const taps
    { [] {
        size_t_const<TapsCount> taps_count;
        //TODO: should be allocated on heap if too large (e.g. > 1024)
        std::array<double, taps_count> taps;

        detail::initialize_hann_frame(begin(taps), taps_count);

        return taps;
    }() };

    return routing::const_frame(taps);
} //hann_frame(size_t_const<TapsCount>)


/**
* @brief Creates a @ref hann_frame_grp given a frame length as @c std::size_t.
*
* Sample type for the produced frame is @c double.
*
* @param taps_count is the function length.
*
* **Usage example**
* @snippet windowing_test.cpp hann_frame usage example with std::size_t
*
* @todo add support for float output type.
**/
inline auto hann_frame(std::size_t taps_count) /** @cond */ ->
decltype(routing::const_frame(std::declval<std::vector<double>&&>())) /** @endcond */
{
    //TODO: should be allocated on heap if too large (e.g. > 1024)
    std::vector<double> taps(taps_count);

    detail::initialize_hann_frame(begin(taps), taps_count);

    return routing::const_frame(std::move(taps));
} //hann_frame(std::size_t)


///@} hann_frame_grp
} //namespace sources


inline namespace nodes
{
///@addtogroup hann_grp
///@{
/**
* @defgroup hann_window_grp "Hann windowing" node
* @brief Applies Hann windowing on its single-sample input stream.
*
* A @ref hann_window_grp "multiplies" the Hann window signal with the samples on its single input
* and produces the result sample on its output.
*
* **Node interface**
* @startuml
* rectangle "o(n) = hann_frame(N)[n%N] * i(n)" <<hann(N)>> as win
* interface "input-0: i(0)... i(n)" <<in>> as in
* interface "output: o(0)... o(n)" <<windowing result>> as out
* win <- in0
* out <- win
* @enduml
* * <tt>"in" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying the signal to window
* * <tt>"windowing result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the windowing result
*
* @sa hann_frame_grp
* @{
**/


template<class WindowLength, DFP_F_REQUIRES((!std::is_convertible<WindowLength, std::size_t>{}))>
// note that usage of routing::to_single_sample() here is just to get a function return of type placeholder
inline decltype(routing::to_single_sample()) hann(WindowLength window_length)
{
    static_assert(always_false<WindowLength>{}, "window_length argument has invalid type. It shall be std::size_t or size_t_const");
    return routing::to_single_sample();
}


/**
* @brief Creates a placeholder for a @ref hann_window_grp given a window length as integral constant.
*
* As the window length is resolved at compile-time, the created node has an optimized usage of the memory ressource.
*
* Resulting sample type is deduced from multiplication of the input sample type with @c double.
*
* @param window_length is the window length (resolved at compile-time).
*
* @tparam WindowLength is window length (usually deduced) from @c window_length parameter.
*
* **Usage example**
* @snippet windowing_test.cpp hann usage example with integral constant
*
* @todo add support for float output type.
**/
template<size_t WindowLength>
inline auto hann(size_t_const<WindowLength> window_length = size_t_const<WindowLength>{}) /** @cond */ ->
decltype(alu::multiply(routing::to_single_sample() <<= hann_frame(window_length))) /** @endcond */
{ return alu::multiply(routing::to_single_sample() <<= hann_frame(window_length)); }


/**
* @brief Creates a placeholder for a @ref hann_window_grp given a window length as @c std::size_t.
*
* Resulting sample type is deduced from multiplication of the input sample type with @c double.
*
* @param window_length is the window length.
*
* @todo add support for float output type.
**/
inline auto hann(std::size_t window_length) /** @cond */ ->
decltype(alu::multiply(routing::to_single_sample() <<= hann_frame(window_length))) /** @endcond */
{ return alu::multiply(routing::to_single_sample() <<= hann_frame(window_length)); }


///@} hann_window_grp
} // namespace nodes
///@} hann_grp
///@} windowing_grp
} // namespace windowning
} // namespace dfp


#endif // DFPTL_WINDOWING_HANN_HPP
