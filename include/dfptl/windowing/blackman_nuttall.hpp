/**
* @file windowing/blackman_nuttall.hpp
**/
/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more detail.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef DFPTL_WINDOWING_BLACKMAN_NUTTALL_HPP
#define DFPTL_WINDOWING_BLACKMAN_NUTTALL_HPP


#include "dfptl/core/source.hpp"
#include "dfptl/alu/multiply.hpp"
#include "dfptl/routing/to_single_sample.hpp"
#include "dfptl/routing/const_frame.hpp"

#include <cmath>


namespace dfp
{
namespace windowing
{
namespace detail
{


template<class Iterator, class TapsCount, class = DFP_CONCEPT_ASSERT((is_input_iterator<Iterator>))>
inline void initialize_blackman_nuttall_frame(Iterator begin_it, TapsCount taps_count)
{
    double const a0 = 0.3635819;
    double const a1 = 0.4891775;
    double const a2 = 0.1365995;
    double const a3 = 0.0106411;
    auto const N = taps_count - 1;

    for(std::size_t i = 0 ; i < taps_count ; i++)
    { *begin_it++ = a0 - a1*std::cos(2*M_PI*i/N) + a2*std::cos(4*M_PI*i/N) - a3*std::cos(6*M_PI*i/N); };
}


} // namespace detail


///@addtogroup windowing_grp
///@{
/**
* @defgroup blackman_nuttall_grp Blackman-Nuttall function
*
* @brief Provides processing source and node related to signal windowing with the
* [Blackman-Nuttall function](https://en.wikipedia.org/wiki/Window_function#Blackman%E2%80%93Nuttall_window).
* @{
**/
inline namespace sources
{
///@addtogroup blackman_nuttall_grp
///@{
/**
* @defgroup blackman_nuttall_frame_grp "Blackman-Nuttall frame" source
* @brief Produces frame of the Blackman-Nuttall function signal.
*
* A @ref blackman_nuttall_frame_grp is a frame-based source producing on its output a frame of the Blackman-Nuttall function.
*
* **Source interface**
* @startuml
* rectangle "o(n) = blackman_nuttall_f(n % N)" <<blackman_nuttall_frame(N)>> as fct
* interface "output: [o(0) ...o(N-1)][o(0)..." <<function result>> as out
* out <- fct
* @enduml
* * <tt>"function result" **output**</tt> : @ref core::detail::stream "stream" conveying a frame of 'double'-type sample
*
* @sa blackman_nuttall_window_grp
* @{
**/


template<class T>
[[noreturn]]
inline decltype(routing::const_frame(std::array<std::nullptr_t, 0>{})) blackman_nuttall_frame(T taps_count)
{ static_assert(always_false<T>{}, "taps_count argument has invalid type. It shall be std::size_t or size_t_const"); }


/**
* @brief Creates a @ref blackman_nuttall_frame_grp given a frame length as integral constant.
*
* Sample type for the produced frame is @c double.
* As the frame length is resolved at compile-time, the created source has an optimized usage of the memory ressource.
*
* @param taps_count is the function length (resolved at compile-time).
*
* @tparam TapsCount is function length (usually deduced) from @c taps_count parameter.
*
* **Usage example**
* @snippet windowing_test.cpp blackman_nuttall_frame usage example with integral constant
*
* @todo add support for float output type.
**/
template<size_t TapsCount>
inline auto blackman_nuttall_frame(size_t_const<TapsCount> = size_t_const<TapsCount>{}) /** @cond */ ->
decltype(routing::const_frame(std::declval<std::array<double, TapsCount> const&>())) /** @endcond */
{
    //TODO: make it constexpr if TapsCount is not too large (e.g. < 256). Consider to dev a "constexpr for-loop" for this
    static std::array<double, TapsCount> const taps
    { [] {
        size_t_const<TapsCount> taps_count;
        //TODO: should be allocated on heap if too large (e.g. > 1024)
        std::array<double, taps_count> taps;

        detail::initialize_blackman_nuttall_frame(begin(taps), taps_count);

        return taps;
    }() };

    return routing::const_frame(taps);
} //blackman_nuttall_frame(size_t_const<TapsCount>)


/**
* @brief Creates a @ref blackman_nuttall_frame_grp given a frame length as @c std::size_t.
*
* Sample type for the produced frame is @c double.
*
* @param taps_count is the function length.
*
* **Usage example**
* @snippet windowing_test.cpp blackman_nuttall_frame usage example with std::size_t
*
* @todo add support for float output type.
**/
inline auto blackman_nuttall_frame(std::size_t taps_count) /** @cond */ ->
decltype(routing::const_frame(std::declval<std::vector<double> const&>())) /** @endcond */
{
    static std::vector<double> const taps
    { [taps_count] {
        //TODO: should be allocated on heap if too large (e.g. > 1024)
        std::vector<double> taps(taps_count);

        detail::initialize_blackman_nuttall_frame(begin(taps), taps_count);

        return taps;
    }() };

    return routing::const_frame(taps);
} //blackman_nuttall_frame(std::size_t)


///@} blackman_nuttall_frame_grp
} //namespace sources


inline namespace nodes
{
///@addtogroup blackman_nuttall_grp
///@{
/**
* @defgroup blackman_nuttall_window_grp "Blackman-Nuttall windowing" node
* @brief Applies Blackman-Nuttall windowing on its single-sample input stream.
*
* A @ref blackman_nuttall_window_grp "multiplies" the Blackman-Nuttall window signal with the samples on its single input
* and produces the result sample on its output.
*
* **Node interface**
* @startuml
* rectangle "o(n) = blackman_nuttall_frame(N)[n%N] * i(n)" <<backman_nuttall(N)>> as win
* interface "input-0: i(0)... i(n)" <<in>> as in
* interface "output: o(0)... o(n)" <<windowing result>> as out
* win <- in0
* out <- win
* @enduml
* * <tt>"in" **input-0**            </tt> : @ref atomic_stream "atomic stream" conveying the signal to window
* * <tt>"windowing result" **output**</tt> : @ref atomic_stream "atomic stream" conveying the windowing result
*
* @sa blackman_nuttall_frame_grp
* @{
**/


template<class WindowLength>
// note that usage of routing::to_single_sample() here is just to get a function return of type placeholder
inline decltype(routing::to_single_sample()) blackman_nuttall(WindowLength window_length)
{
    static_assert(always_false<WindowLength>{}, "window_length argument has invalid type. It shall be std::size_t or size_t_const");
    return routing::to_single_sample();
}


/**
* @brief Creates a placeholder for a @ref blackman_nuttall_window_grp given a window length as integral constant.
*
* As the window length is resolved at compile-time, the created node has an optimized usage of the memory ressource.
*
* Resulting sample type is deduced from multiplication of the input sample type with @c double.
*
* @param window_length is the window length (resolved at compile-time).
*
* @tparam WindowLength is window length (usually deduced) from @c window_length parameter.
*
* **Usage example**
* @snippet windowing_test.cpp blackman_nuttall usage example with integral constant
*
* @todo add support for float output type.
**/
template<size_t WindowLength>
inline auto blackman_nuttall(size_t_const<WindowLength> window_length = size_t_const<WindowLength>{}) /** @cond */ ->
decltype(alu::multiply(routing::to_single_sample() <<= blackman_nuttall_frame(window_length))) /** @endcond */
{ return alu::multiply(routing::to_single_sample() <<= blackman_nuttall_frame(window_length)); }


/**
* @brief Creates a placeholder for a @ref blackman_nuttall_window_grp given a window length as @c std::size_t.
*
* Resulting sample type is deduced from multiplication of the input sample type with @c double.
*
* @param window_length is the window length.
*
* @todo add support for float output type.
**/
inline auto blackman_nuttall(std::size_t window_length) /** @cond */ ->
decltype(alu::multiply(routing::to_single_sample() <<= blackman_nuttall_frame(window_length))) /** @endcond */
{ return alu::multiply(routing::to_single_sample() <<= blackman_nuttall_frame(window_length)); }


///@} blackman_nuttall_window_grp
} // namespace nodes


///@} blackman_nuttall_grp
///@} windowing_grp
} // namespace windowning
} // namespace dfp


#endif // DFPTL_WINDOWING_BLACKMAN_NUTTALL_HPP
