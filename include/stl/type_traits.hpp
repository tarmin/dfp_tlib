/*
* Copyright (C) 2020 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_TYPE_TRAITS_HPP
#define STL_TYPE_TRAITS_HPP


#include "stl/detail/invoke_result.hpp"

#include <type_traits>
#include <utility>


// if compiler version is C++11 or below
#if (__cplusplus <= 201103L)

namespace std
{


template<class T>
using add_const_t               = typename std::add_const<T>            ::type;

template<class T>
using add_cv_t                  = typename std::add_cv<T>               ::type;

template<class T>
using add_lvalue_reference_t    = typename std::add_lvalue_reference<T> ::type;

template<class T>
using add_rvalue_reference_t    = typename std::add_rvalue_reference<T> ::type;

template<class T>
using add_volatile_t            = typename std::add_volatile<T>         ::type;

template<bool B, class T, class F>
using conditional_t             = typename std::conditional<B,T,F>      ::type;

template<class T>
using decay_t                   = typename std::decay<T>                ::type;

template<class T>
using make_signed_t             = typename std::make_signed<T>          ::type;

template<class T>
using make_unsigned_t           = typename std::make_unsigned<T>        ::type;

template<class T>
using remove_const_t            = typename std::remove_const<T>         ::type;

template<class T>
using remove_cv_t               = typename std::remove_cv<T>            ::type;

template<class T>
using remove_pointer_t          = typename std::remove_pointer<T>       ::type;

template<class T>
using remove_reference_t        = typename std::remove_reference<T>     ::type;

template<class T>
using result_of_t               = typename std::result_of<T>            ::type;

} // namespace std

#endif // if compiler version is C++11 or below


// if compiler version is C++14 or below
#if (__cplusplus <= 201402L)

namespace std
{
#include <functional>


template <bool B>
using bool_constant = integral_constant<bool, B>;

template<typename... Ts> struct make_void { typedef void type;};
template<typename... Ts> using void_t = typename make_void<Ts...>::type;


template<class...> struct conjunction : std::true_type { };
template<class B1> struct conjunction<B1> : B1 { };
template<class B1, class... Bn>
struct conjunction<B1, Bn...> 
    : std::conditional_t<bool(B1::value), conjunction<Bn...>, B1> {};


template<class...> struct disjunction : std::false_type { };
template<class B1> struct disjunction<B1> : B1 { };
template<class B1, class... Bn>
struct disjunction<B1, Bn...> 
    : std::conditional_t<bool(B1::value), B1, disjunction<Bn...>>  { };

template<class B>
struct negation : std::bool_constant<!bool(B::value)> { };


template <class F, class... ArgTypes>
struct invoke_result : detail::invoke_result<void, F, ArgTypes...> {};

template< class F, class... ArgTypes>
using invoke_result_t = typename invoke_result<F, ArgTypes...>::type;

template <typename F, typename... Args>
struct is_invocable :
    std::is_constructible<
        std::function<void(Args ...)>,
        std::reference_wrapper<typename std::remove_reference<F>::type>
    >
{
};

template <typename R, typename F, typename... Args>
struct is_invocable_r :
    std::is_constructible<
        std::function<R(Args ...)>,
        std::reference_wrapper<typename std::remove_reference<F>::type>
    >
{
};


} // namespace std


#endif // if compiler version is C++14 or below


#if (__cplusplus <= 201703L)


namespace std
{


template<class T>
struct remove_cvref { typedef std::remove_cv_t<std::remove_reference_t<T>> type; };

template<class T>
using remove_cvref_t = typename remove_cvref<T>::type;


template <class T>
struct unwrap_reference { using type = T; };

template <class U>
struct unwrap_reference<std::reference_wrapper<U>> { using type = U&; };


template<class T>
using unwrap_reference_t = typename unwrap_reference<T>::type;

template< class T >
struct unwrap_ref_decay : unwrap_reference<std::decay_t<T>> {};

} // namespace std


#endif // if compiler version is C++17 or below

#endif // STL_TYPE_TRAITS_HPP
