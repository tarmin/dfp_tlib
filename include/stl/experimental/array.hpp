/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_EXPERIMENTAL_ARRAY_HPP
#define STL_EXPERIMENTAL_ARRAY_HPP


#include "stl/type_traits.hpp"
#include <array>


namespace std
{
namespace experimental
{
namespace detail
{


template<class> struct is_ref_wrapper : std::false_type {};
template<class T> struct is_ref_wrapper<std::reference_wrapper<T>> : std::true_type {};
 
template<class T>
using not_ref_wrapper = std::negation<is_ref_wrapper<std::decay_t<T>>>;
 
template <class D, class...> struct return_type_helper
{ typedef D type; };

template <class... Types>
struct return_type_helper<void, Types...> : std::common_type<Types...>
{ static_assert(
    std::conjunction<not_ref_wrapper<Types>...>::value, "Types cannot contain reference_wrappers when D is void"
); };

template <class D, class... Types>
using return_type = std::array<typename return_type_helper<D, Types...>::type, sizeof...(Types)>;


template <class T, std::size_t N, std::size_t... I>
constexpr std::array<std::remove_cv_t<T>, N> to_array_impl(T (&a)[N], std::index_sequence<I...>)
{ return { {a[I]...} }; }


} // namespace detail


template < class D = void, class... Types>
constexpr detail::return_type<D, Types...> make_array(Types&&... t)
{ return {{std::forward<Types>(t)...}}; }

 
template <class T, std::size_t N>
constexpr std::array<std::remove_cv_t<T>, N> to_array(T (&a)[N])
{ return detail::to_array_impl(a, std::make_index_sequence<N>{}); }


} // namespace experimental
} // std


#endif // STL_EXPERIMENTAL_ARRAY_HPP
