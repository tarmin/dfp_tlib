/*
* Copyright (C) 2018 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_TUPLE_HPP
#define STL_TUPLE_HPP


#include <tuple>
#include "stl/utility.hpp"
#include "stl/type_traits.hpp"


// if compiler version is C++11 or below
#if (__cplusplus <= 201103L)


namespace std
{


template <std::size_t I, class T>
using tuple_element_t = typename tuple_element<I, T>::type;


} // namespace std


#endif // if compiler version is C++11 or below


// if compiler version is C++14 or below
#if (__cplusplus <= 201402L)


namespace std
{
namespace detail
{


template<class T, class Tuple, std::size_t... I>
constexpr T make_from_tuple_impl(Tuple&& t, std::index_sequence<I...>)
{ return T(std::get<I>(std::forward<Tuple>(t))...); }


} // namespace detail


template<class T, class Tuple>
constexpr T make_from_tuple(Tuple&& t)
{ return detail::make_from_tuple_impl<T>(
    std::forward<Tuple>(t), std::make_index_sequence<std::tuple_size<std::remove_reference_t<Tuple>>::value>{}
); }


} // namespace std


#endif // if compiler version is C++14 or below


#endif // STL_TUPLE_HPP
