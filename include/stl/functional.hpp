/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_FUNCTIONAL_HPP
#define STL_FUNCTIONAL_HPP


#include <functional>

#include "stl/type_traits.hpp"


// if compiler version is C++11 or below
#if (__cplusplus <= 201103L)


namespace upg // upgraded STD lib
{


template<class T = void> struct multiplies : std::multiplies<T> {};

template<> struct multiplies<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs * rhs)
    { return lhs * rhs; }
};


template<class T = void> struct divides;

template<> struct divides<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs / rhs)
    { return lhs / rhs; }
};


template<class T = void> struct modulus;

template<> struct modulus<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs % rhs)
    { return lhs % rhs; }
};


template<class T = void> struct plus : std::plus<T> {};

template<> struct plus<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs + rhs)
    { return lhs + rhs; }
};


template<class T = void> struct minus;

template<> struct minus<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs - rhs)
    { return lhs - rhs; }
};


template<class T = void> struct negate;

template<> struct negate<void>
{
    template<class T>
    constexpr auto
    operator()(T&& arg) const -> decltype(-arg)
    { return -arg; }
};


template<class T = void> struct bit_and;

template<> struct bit_and<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs & rhs)
    { return lhs & rhs; }
};


template<class T = void> struct bit_or;

template<> struct bit_or<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const -> decltype(lhs | rhs)
    { return lhs | rhs; }
};


template<class T = void> struct bit_xor;

template<> struct bit_xor<void>
{
    template<class T, class U>
    constexpr auto operator()(T&& lhs, U&& rhs) const ->  decltype(lhs ^ rhs)
    { return lhs ^ rhs; }
};


template<class T = void> struct logical_not;

template<> struct logical_not<void>
{
    template<class T>
    constexpr auto operator()(T&& arg) const -> decltype(!arg)
    { return !arg; }
};


template<class T = void> struct less;

template<> struct less<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs < lhs)
    { return rhs < lhs; }
};


template<class T = void> struct less_equal;

template<> struct less_equal<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs <= lhs)
    { return rhs <= lhs; }
};


template<class T = void> struct greater;

template<> struct greater<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs > lhs)
    { return rhs > lhs; }
};


template<class T = void> struct greater_equal;

template<> struct greater_equal<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs >= lhs)
    { return rhs >= lhs; }
};


template<class T = void> struct equal_to;

template<> struct equal_to<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs == lhs)
    { return rhs == lhs; }
};


template<class T = void> struct not_equal_to;

template<> struct not_equal_to<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs != lhs)
    { return rhs != lhs; }
};


template<class T = void> struct logical_and;

template<> struct logical_and<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs && lhs)
    { return rhs && lhs; }
};


template<class T = void> struct logical_or;

template<> struct logical_or<void>
{
    template<class Rhs, class Lhs>
    constexpr auto operator()(Rhs&& rhs, Lhs&& lhs) const -> decltype(rhs || lhs)
    { return rhs || lhs; }
};


} // namespace upg


#else  // if compiler version is C++11 or below

namespace upg
{
    using std::multiplies;
    using std::divides;
    using std::modulus;
    using std::plus;
    using std::minus;
    using std::negate;
    using std::bit_and;
    using std::bit_or;
    using std::bit_xor;
    using std::logical_not;
    using std::less;
    using std::less_equal;
    using std::greater;
    using std::greater_equal;
    using std::equal_to;
    using std::not_equal_to;
    using std::logical_and;
    using std::logical_or;
}

#endif // if compiler version is C++11 or below


#endif // STL_FUNCTIONAL_HPP
