/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_SPAN_HPP
#define STL_SPAN_HPP


// if compiler version is C++17 or below
#if (__cplusplus <= 201703L)

#define span_CONFIG_SIZE_TYPE std::size_t
#define span_FEATURE_MAKE_SPAN 1


#include <nonstd/span.hpp>
#include <utility>


namespace upg
{
    using nonstd::span;
    using nonstd::make_span;
    using nonstd::dynamic_extent;
}


#else //if (__cplusplus <= 201703L)

#include <span>


namespace upg
{
    using std::span;
    using std::dynamic_extent;
    //WIP
}


#endif //if (__cplusplus <= 201703L)


#endif //STL_SPAN_HPP
