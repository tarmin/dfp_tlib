/*
* Copyright (C) 2019 Tarmin Rehve, <tarmin.rehve@gmail.com>
*
* This file is part of DFP template library.
*
* DFP template library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DFP template library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with DFP template library.  If not, see <http://www.gnu.org/licenses/>.
**/


#ifndef STL_OPTIONAL_HPP
#define STL_OPTIONAL_HPP


// if compiler version is C++14 or below
#if (__cplusplus <= 201402L)

    
#include <cl/optional.h>


namespace upg
{
    using cl::optional;
}


#else // if compiler version is C++14 or below


#include <optional>
namespace upg
{
    using std::optional;
}


#endif // if compiler version is C++14 or below


#endif //STL_OPTIONAL_HPP
