package org.dfp.api;

import com.mmnaseri.utils.tuples.Tuple;

public interface INativeProcessor<OutputFrame, InputFrames extends Tuple<?>> {
    void initialize(int outputFrameLength, int[] inputFrameLengths);
    OutputFrame process(InputFrames inputs);
    void dispose();
}
