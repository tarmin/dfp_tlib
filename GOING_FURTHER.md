[//]: # (/**)

# Multi-channels stream
## Manipulating multi-channels stream
DFPTL allows to organize samples of stream into channels. Multi-channels stream can be seem as an aggregation of several mono channel streams whose sample and frame traits are all similar.
Some dedicated cells help in handling channels in a processing graph:
* [const_frame source] can channelize samples of a given immutable frame into multiple channels
* [mutable_frame source] can channelize samples of a given mutable frame reference into multiple channels
* [extract_channel node] can extract a single channel from a multi-channel stream
* [channelize node] assembles the various streams on its input into a multi-channels stream
* TODO:
* [dechannelize sink]() splits all channels of stream. Individual channel can be retrieved with the [dechannelize::get source]()
* [select node]() restricts processing of the downstream cells to only the selected channel
* [deselect node]() cancels effect of a previous `select` node, so all channels are again considered in downstream processing

## Handling multi-channels in custom cells
* When implementating custom [nodes] or [sinks] the channel count of an input stream
can be retrieved with help of the dfp::core::downstream_cell::input_stream_specifier() member method.
* When implementating custom [nodes] or [sources] the channel count of the output stream
can be specified either at construction time by passing the value to base class or during class definition, by passing constant integral to base class specialization.

# subprocessing cell
Chain of processing created with DFPTL can be packaged into re-usable [subprocessing cells]. Thus subprocessing cells can be 
used as `source` or `node` and placed in new processing graph where such unit processing shall be applied.

Below is a C++14 example highlighting the usage and power of such [subprocessing cells]:
```cpp
using namespace dfp;
using namespace dfp::routing;
...
// create square subprocessing
struct square_processing
{
    template<class Cloneable>
    auto operator()(Cloneable& in) const
    { return
        alu::multiply() += in.dup()
                        |= in.ref()
    ;}
};
auto square = [](){ return subprocessing(square_processing{}); };

// create countdown subprocessing
template<class Value>
struct countdown_processing
{
    countdown_processing(Value start_value)
     :  start_value_(start_value) {}

    auto operator()() const
    { return
        alu::subtract() += alu::counter<Value>()
                        |= start_value_
    ;}

private:
    Value const start_value_;
};
auto countdown = [](int start_value){ return subprocessing(countdown_processing{start_value}); };
...

// usage in new processing graph of subprocessing node and source created just above
auto nonlinear_countdown = releasing_sample() <<= square() <<= countdown(10);

assert(*nonlinear_countdown == 100);
assert(*++nonlinear_countdown == 81);
assert(*++nonlinear_countdown == 64);
assert(*++nonlinear_countdown == 49);
assert(*++nonlinear_countdown == 36);
assert(*++nonlinear_countdown == 25);
assert(*++nonlinear_countdown == 16);
assert(*++nonlinear_countdown == 9);
```

# implicit conversions
## callable to `apply source` or `apply` node
## callable to `execute` node
## souce cell builder to source
## value to processing `const_sample` source

[const_frame source]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__const__frame__grp.html
[mutable_frame source]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__mutable__frame__grp.html
[extract_channel node]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__extract__channel__grp.html
[channelize node]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__channelize__grp.html
[subprocessing cells]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__subprocessing__grp.html
[nodes]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__node__grp.html
[sinks]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__sink__grp.html
[sources]: https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__source__grp.html

[//]: # (
[const_frame source]: @ref const_frame_grp
[mutable_frame source]: @ref mutable_frame_grp
[extract_channel node]: @ref extract_channel_grp
[channelize node]: @ref channelize_grp
[nodes]: @ref node_grp
[sinks]: @ref sink_grp
[sources]: @ref source_grp
[subprocessing cells]: @ref subprocessing_grp
@anchor)

[//]: # (**/)
