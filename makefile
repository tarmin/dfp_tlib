THIS_MAKEFILE:= $(realpath $(firstword $(MAKEFILE_LIST)))
ROOT_DIR     := $(shell dirname $(THIS_MAKEFILE))
CXXSTD       ?= c++11
OUT_DIR      := $(ROOT_DIR)/test/build/$(CXXSTD)
TEST_SRC_DIR := $(ROOT_DIR)/test
VPATH        := $(TEST_SRC_DIR) $(ROOT_DIR)/example
CURRENT_DIR  := $(shell pwd)

INC_PATH := -I$(ROOT_DIR)/include -I$(ROOT_DIR)/dep/span-lite/include -I$(ROOT_DIR)/dep/speexdsp/include -I$(ROOT_DIR)/dep/libfvad/include -I$(ROOT_DIR)/dep/kissfft -I$(ROOT_DIR)/dep/optional/lib
CXXFLAGS += $(INC_PATH) -g -O0 -std=$(CXXSTD) -Wall

STANDALONE_EXAMPLES_SRC  := main.cpp dfptl_overview_example.cpp const_sample_overview_example.cpp triangular_waveform_generator.cpp\
 placeholder_creation_example.cpp placeholder_preset_inputs_example.cpp operators_overview_example.cpp
STANDALONE_TESTS_SRC     := core_test.cpp core_cell_test.cpp stl_test.cpp alu_test.cpp default_settings_test.cpp math_test.cpp routing_test.cpp container_test.cpp io_test.cpp windowing_test.cpp filtering_test.cpp kissfft_test.cpp
ALL_TESTS_SRC            := $(STANDALONE_TESTS_SRC) sndfile_test.cpp fvad_test.cpp speex_test.cpp
STANDALONE_EXAMPLES_OBJ  := $(addprefix $(OUT_DIR)/, $(STANDALONE_EXAMPLES_SRC:.cpp=.o))
STANDALONE_TESTS_OBJ     := $(addprefix $(OUT_DIR)/, $(STANDALONE_TESTS_SRC:.cpp=.o))
ALL_TESTS_OBJ            := $(addprefix $(OUT_DIR)/, $(ALL_TESTS_SRC:.cpp=.o))
STANDALONE_EXAMPLES_EXEC := $(addprefix $(OUT_DIR)/, standalone_examples)
STANDALONE_TESTS_EXEC    := $(addprefix $(OUT_DIR)/, standalone_tests)
ALL_TESTS_EXEC           := $(addprefix $(OUT_DIR)/, all_tests)
STANDALONE_TESTS_LIB     := -lboost_test_exec_monitor -lpthread
ALL_TESTS_LIB            := $(STANDALONE_TESTS_LIB) -lsndfile -lspeexdsp -lfvad -L$(ROOT_DIR)/dep/install/lib -Wl,-rpath -Wl,$(ROOT_DIR)/dep/install/lib

.PHONY: standalone_tests run_standalone_tests clean_standalone_tests \
        all_tests run_all_tests clean_all_tests \
		standalone_examples run_standalone_examples clean_standalone_examples \
		all_examples run_all_examples clean_all_examples \
		default

default: run_standalone_tests


run_all_examples: run_standalone_examples

run_all_tests: all_tests
	@echo
	@echo
	cd $(TEST_SRC_DIR) ; $(ALL_TESTS_EXEC) ; cd $(CURRENT_DIR)


run_standalone_examples: standalone_examples
	@echo
	@echo
	cd $(TEST_SRC_DIR) ; $(STANDALONE_EXAMPLES_EXEC) ; cd $(CURRENT_DIR)

run_standalone_tests: standalone_tests
	@echo
	@echo
	cd $(TEST_SRC_DIR) ; $(STANDALONE_TESTS_EXEC) ; cd $(CURRENT_DIR)


all_examples : standalone_examples

all_tests: $(ALL_TESTS_EXEC)


standalone_examples: $(STANDALONE_EXAMPLES_EXEC)

standalone_tests: $(STANDALONE_TESTS_EXEC)


$(STANDALONE_EXAMPLES_EXEC): $(THIS_MAKEFILE) $(STANDALONE_EXAMPLES_OBJ)
	$(CXX) -o $@ $(STANDALONE_EXAMPLES_OBJ)

$(STANDALONE_TESTS_EXEC): $(THIS_MAKEFILE) $(STANDALONE_TESTS_OBJ) $(OUT_DIR)/test_main.o
	$(CXX) -o $@ $(OUT_DIR)/test_main.o $(STANDALONE_TESTS_OBJ) $(STANDALONE_TESTS_LIB)

$(ALL_TESTS_EXEC): $(THIS_MAKEFILE) $(ALL_TESTS_OBJ) $(OUT_DIR)/test_main.o
	$(CXX) -o $@ $(OUT_DIR)/test_main.o $(ALL_TESTS_OBJ) $(ALL_TESTS_LIB)

$(OUT_DIR)/test_main.o: | $(OUT_DIR)
	echo "#include <boost/test/unit_test.hpp>" > $(OUT_DIR)/test_main.cpp
	echo >> $(OUT_DIR)/test_main.cpp
	$(CXX) -c -o $(OUT_DIR)/test_main.o $(OUT_DIR)/test_main.cpp $(CXXFLAGS) -DBOOST_TEST_MODULE="DFP TLIB unit tests"


$(STANDALONE_EXAMPLES_OBJ) $(ALL_TESTS_OBJ): $(OUT_DIR)/%.o : %.cpp | $(OUT_DIR)
		$(CXX) -c -o $@ $< $(CXXFLAGS)


$(OUT_DIR):
	mkdir -p $(OUT_DIR)

clean_all_examples: clean_standalone_examples

clean_standalone_examples:
	-rm $(STANDALONE_EXAMPLES_OBJ) 2> /dev/null

clean_all_tests:
	-rm $(OUT_DIR)/test_main.o $(ALL_TESTS_OBJ) 2> /dev/null

clean_standalone_tests:
	-rm $(OUT_DIR)/test_main.o $(STANDALONE_TESTS_OBJ) 2> /dev/null
