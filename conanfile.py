import os
from conan import ConanFile
from conan.tools.files import copy


class DfpTlibRecipe(ConanFile):
    name = "dfp_tlib"
    version = "0.1"

    # Optional metadata
    license = "GNU Lesser General Public License v3.0"
    author = "Tarmin Rehve tarmin.rehve@gmail.com"
    url = ""
    homepage = "https://gitlab.com/tarmin/dfp_tlib"
    description = "Template Library for DataFlow Programming in C++"
    topics = ("dfp_tlib" "dataflow" "dfptl")

    # Header only configuration
    no_copy_source = True
    default_options = {"boost/*:header_only": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "include/stl/*", "include/dfptl/core/*", "include/dfptl/alu/*", "include/dfptl/math/*", "include/dfptl/routing/*", "dep/optional/lib/*"

    def package(self):
         # This will also copy the "include" folder
         copy(self, "include/*", self.source_folder, self.package_folder)
         copy(self, "*", os.path.join(self.source_folder, "dep/optional/lib"), os.path.join(self.package_folder, "include"))

    def package_info(self):
         # For header-only packages, libdirs and bindirs are not used
         # so it's necessary to set those as empty.
         self.cpp_info.bindirs = []
         self.cpp_info.libdirs = []

    def requirements(self):
        self.requires("span-lite/[>=0.10.3]")
        self.requires("boost/[>=1.73.0]")

    def layout(self):
        self.cpp.source.includedirs = "dep/optional/lib"
