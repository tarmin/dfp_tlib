package org.dfp.example;

import com.mmnaseri.utils.tuples.impl.OneTuple;

import org.junit.Test;

import static org.junit.Assert.*;

public class DfpJavaExampleTest {
    @Test
    public void multiply() {
        float[] out;
        final float[] in0 = {0, 1, 2, 3, 4};
        final float[] in1 = {-1, -2, -3, -4};

        AmplifyProcessor ap1 = new AmplifyProcessor();
        AmplifyProcessor ap2 = new AmplifyProcessor();

        ap1.open(5, new int[]{in0.length} );
        ap2.open(4, new int[]{in1.length} );
        out = ap1.process(OneTuple.of(in0));
        assertEquals(5, out.length);
        assertArrayEquals(new float[]{0, 2.1f, 4.2f, 6.3f, 8.4f}, out, 0);
        ap1.close();
        out = ap2.process(OneTuple.of(in1));
        assertEquals(4, out.length);
        assertArrayEquals(new float[]{-2.1f, -4.2f, -6.3f, -8.4f}, out, 0);
        ap2.close();
    }
}
