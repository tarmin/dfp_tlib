package org.dfp.example;

import com.mmnaseri.utils.tuples.Tuple;

import org.dfp.api.INativeProcessor;

import java.io.Closeable;

public class AmplifyProcessor implements INativeProcessor<float[], Tuple<float[]>>, Closeable {
    private boolean isClosed = false;

    static {
        System.loadLibrary("dfp_jni_example");
    }

    public void open(int outputFrameLength, int[] inputFrameLengths) {
        initialize(outputFrameLength, inputFrameLengths);
    }

    @Override
    public native void initialize(int outputFrameLength, int[] inputFrameLengths);

    @Override
    public native float[] process(Tuple<float[]> inputs);

    @Override
    public native void dispose();


    @Override
    public void close() {
        if (!isClosed) {
            isClosed = true;
            dispose();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
            close();
    }
}
