#include <dfptl/core/core.hpp>
#include <dfptl/java/java.hpp>
#include <dfptl/alu/multiply.hpp>
#include <dfptl/io/write_csv.hpp>
#include <dfptl/routing/to_single_sample.hpp>
#include <dfptl/routing/adjust_frame_length.hpp>


using namespace dfp;
using namespace dfp::io;
using namespace dfp::alu;
using namespace dfp::java;
using namespace dfp::routing;


auto amplify_processing_builder = [](std::size_t output_frame_length, std::vector<std::size_t> input_frame_lengths) {
    (void) input_frame_lengths;
    return
        to_java(JFLOAT) <<= cout_csv() <<= adjust_frame_length(output_frame_length) <<= multiply(2.1) <<= to_single_sample() <<= from_java<0>(JFLOAT);
};

DFP_JAVA_ASSIGN_PROCESSING_TREE(org_dfp_example_AmplifyProcessor, amplify_processing_builder);
