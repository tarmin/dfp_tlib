# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.10.2)

# Declares and names the project.

project("dfp_jni_example")

set(CMAKE_CXX_STANDARD 17)

set(ANDROID_STL "c++_shared")

# Download automatically, you can also just copy the conan.cmake file
if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
            "${CMAKE_BINARY_DIR}/conan.cmake")
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)

conan_cmake_configure(
        REQUIRES boost/[>=1.70.0]
        OPTIONS boost:header_only=True
        GENERATORS cmake_find_package)

conan_cmake_install(
        PATH_OR_REFERENCE .
        BUILD never
        REMOTE conan-center)

set(CMAKE_MODULE_PATH ${CMAKE_BINARY_DIR})
find_package(Boost MODULE REQUIRED)

include_directories (
        ${CMAKE_SOURCE_DIR}/../../../../../../include
        ${CMAKE_SOURCE_DIR}/../../../../../../dep/span-lite/include
        ${Boost_INCLUDE_DIR}
)

# hack to allow JUnit test with native lib
if (CMAKE_SYSTEM STREQUAL CMAKE_HOST_SYSTEM)
    find_package(JNI REQUIRED)
    include_directories(${JNI_INCLUDE_DIRS})
endif()

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
        dfp_jni_example

        # Sets the library as a shared library.
        SHARED

        # Provides a relative path to your source file(s).
        amplify_processing_example.cpp)
