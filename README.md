[//]: # (@if 0 @anchor)
# DFP_TLIB overview
[//]: # (@endif @anchor)

**DFP_TLIB** is a C++ template library which aims at bringing dataflow programming to C++ without sacrificing performance.

 Thus processing defined with help of DFP_TLIB are expected to consume similar amount of memory and CPU than equivalent implementation with classical C++ programming. So, if compiler optimization flag is turned on, the object code produced will not include any overhead for the management of the processing network.

 To achieve its performance objective, DFP_TLIB takes leverage of meta-programming techniques based on C++ template so it **requires support for C+11** or higher revision.

In DFP_TLIB, computation is based on demand-driven principle (requirement for a data triggers the operation that will generate it). The Domain-Specific Language brought by DFP_TLIB allows to define some tree of processing cells where stream lines between them are represented with help of overloading of the `operator<<=`.

Thus for example, getting a STL container with a sequence of 10-first square values will with DFP_TLIB look like:
```cpp
auto frame_wrapper =
    routing::releasing_frame() <<= routing::adjust_frame_length(10) <<= [](int const& in){ return in*in; } <<= alu::counter(INT32_T);
// frame_wrapper.get() now contains the following sequence of int : {0, 1, 4, 9, 16, 25, 36, 49, 64, 81}
```

> DFP_TLIB is still in early stage of development, some parts of the API may still change without prior notice.


[//]: # (
@if 0 @anchor)
# Table of contents

<!-- MDTOC maxdepth:6 firsth1:1 numbering:0 flatten:0 bullets:1 updateOnSave:1 -->

- [DFP_TLIB overview](#dfp_tlib-overview)
- [Table of contents](#table-of-contents)
- [Features](#features)
- [Compilation & Build](#compilation--build)
- [Getting started](#getting-started)
  - [Core concepts](#core-concepts)
    - [The processing cells](#the-processing-cells)
    - [The stream lines](#the-stream-lines)
  - [Creating a first processing tree](#creating-a-first-processing-tree)
  - [Directory structure](#directory-structure)
  - [Namespace organization](#namespace-organization)
  - [API](#api)
  - [External dependency table](#external-dependency-table)
- [Going further](#going-further)

<!-- /MDTOC -->
[//]: # (
@endif @anchor)


# Features

* Dataflow programing DSL
* demand-driven model
* declarative programming
* no overhead for processing network management
* suitable for low-end CPU, DSP or microcontroller
* minimal C++ requirement is C++11, supports also C++14 and C++17
* support for compile-time computation with constexpr processing tree (starting with C++14 and at least version 12 if compiler is GCC)
* support for frame-based and sample-based processing
* support of [multi-channels stream](#multi-channels-stream)
* support of [subprocessing cell](#subprocessing-cell) to create reusable processing stream


# Compilation & Build

DFP_TLIB is a template library. Its source code repository only contains source code as C++ header files mainly dependent on the C++ STL and C++ BOOST library header files.

However, note that the library may wrap functionalities brought by some external libraries whose the build artifacts will need to be downloaded or built and installed specifically for your building host.
Dependencies on external libraries are summarized in later [table](#external-dependency-table).

Moreover, while the library supports `C++11` standard, some types defined by DFP_TLIB can be specific to this C++ version.
So it is highly recommended to start new development with `C++14` or above.

Getting the source code:
```sh
git clone https://gitlab.com/tarmin/dfp_tlib.git --recursive
cd dfp_tlib
```
Building and running the examples or tests:
```sh
make standalone_examples standalone_tests
make run_standalone_examples run_standalone_tests
make all_examples all_tests
make run_all_examples run_all_tests
```
*standalone_* prefix refers to examples or tests which do not depend on external libraries.
On the contrary, *all_* prefix includes also examples or tests which have external dependencies.

> Note that when building host and target is Ubuntu16.04 or higher version, some `bootstrap` script can be applied to install all dependencies required to run non-standalone tests or examples.
> if so, before building tests or examples, please source the bootstrap script:
```sh
. bootstrap
```


# Getting started

With DFP_TLIB, data flow programming consists in chaining several **processing cells** which will each apply a dedicated processing on data streams transiting between them.


## Core concepts
### The processing cells

3 different types of processing cells exist in DFP_TLIB: the **sources** (also named **leaves**), the **nodes** (or **knots**) and the **sinks** (or **trunks**).

**Sources** and **nodes** always produce data stream on their **single output**. They are referred as **upstream processing cells**.

**Sinks** and **nodes** always accept data stream(s) on their **input(s)**. They are referred as **downstream processing cells**.


### The stream lines and flow operators

The stream lines linking ports of processing cells together and indicating the data flow direction are represented with help of the <b>`operator<<=`</b>. Such an operator actually
<b>extend</b>s the upstream part on its right hand side with the given node or sink on its left hand side.
Processing graph can be <b>expand</b>ed which help of the <b>`operator|=`</b>. Due to this operator, a new branch is initiated in an existing processing graph.
The independent branches of an upstream graph can be <b>collect</b>ed all together with the <b>`operator+=`</b> to bind a multiple-input processing cells with its inputs.

The <b>`operator<<=`</b>, <b>`operator|=`</b> and <b>`operator+=`</b> are flow operators applicable on **sink, node or source** processing cells builders.
However as long as one of the operand of those flow operators is a processing cell some implicit conversions may happen.
Please refer to dedicated "[implicit conversions](GOING_FURTHER.md#implicit-conversions)" chapter.

Moreover as the upstream cells only have one single output, any complete processing network (with one sink and no open input) designed with DFP_TLIB is actually a **processing tree**.

At last, any part in a processing tree not including a sink is called a **processing branch**.


## Creating a first processing tree

[//]: # (
@if 0 @anchor)
```plantuml
'@endif

@startuml
rectangle "counter(time_step)" <<source>> as count
rectangle "const_sample(2*PI/frequency)" <<source>> as angular_freq
rectangle "multiply()" <<node>> as mult
rectangle "sin()" <<node>> as sin
rectangle "csv_print()" <<node>> as print
rectangle "looping(frequency/time_step)" <<sink>> as loop


note top of count
  source can only have
  one single output no input
endnote

count -down-> mult
angular_freq -> mult
mult -> sin
sin -> print
print -> loop

note bottom of loop
sink or node can accept
one or multiple inputs
but sink has no output
endnote
@enduml

'@if 0
```
[//]: # (
@endif @n @anchor)


The diagram above represents a simple processing tree which perform computation of the `sin(2*PI/frequency*counter(time_step))` function on `frequency/time_step` iterations and print the result in comma-separated-value format.

> Note that processing trees programmed with DFP_TLIB are actually defined from sink to source(s) so they shall be read from right to left. Next diagrams will be presented with respect of this convention.


[//]: # (
@if 0 @anchor)
```plantuml
'@endif

@startuml
rectangle "counter(time_step)" <<source>> as count
rectangle "const_sample(2*PI/frequency)" <<source>> as angular_freq
rectangle "multiply()" <<node>> as mult
rectangle "sin()" <<node>> as sin
rectangle "csv_print()" <<node>> as print
rectangle "looping(frequency/time_step)" <<sink>> as loop

loop <- print
print <- sin
sin <- mult
mult <- angular_freq
mult <-down- count
@enduml

'@if 0
```
[//]: # (
@endif @n @anchor)

The piece of code below presents the corresponding implementation using DFP_TLIB DSL. Full source code can be found in file [dfptl_overview_example.cpp](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/dfptl_overview_example_8cpp-example.html).
```cpp
// create a processing tree printing values of 1 sinewave period
double frequency = 440;
double time_step = 10;

routing::looping(frequency/time_step) <<= csv_print <<= math::sin() <<= alu::multiply() += 2*M_PI/frequency
                                                                                        |= alu::counter(time_step);
```

The processing given above could classically be implemented as follow:
```cpp
// create a waterfall of functions printing values of 1 sinewave period
double frequency = 440;
double time_step = 10;
counter_function counter(time_step);

for( int i = 0 ; i < frequency/time_step ; i++)
{
    csv_print( sin( (2*M_PI/frequency)*counter() ) );
}
```


## Directory structure

Development or usage of processing cells will require to include files in folder **include/dfptl/core/**. For simplication purpose, header file **include/dfptl/core/core.hpp** can be included for both cases.
Note that any files under a **detail/** folder are not intended to be included by end-user source files. So there is no effort to keep design of those parts stable and it may significantly change from one release to another without prior notice.

+ *include/* : all header source files part of DFP template library.
  + **include/dfptl/** : header source files for the DFP DSL and pre-defined processing cells.
    + **include/dfptl/core/** : header source files required to manage a processing tree and implement custom processing cells.
    + *include/dfptl/core/detail/* : header files for implementation details of the library core. It should not be included by anything else than header files from  include/dfptl/core/.
    + **include/dfptl/alu/** : header source files defining processing cells applying arithmetic or logic operations.
    + *include/dfptl/alu/detail/* : header files for implementation details of alu module.
    + **include/dfptl/math/** : header source files defining processing cells applying classic mathematical operations.
    + *include/dfptl/math/detail/* : header files for implementation details of math module.
    + **include/dfptl/routing/** : header source files defining processing cells helping in stream routing and routine manipulations.
    + *include/dfptl/routing/detail/* : header files for implementation details of routing module.
    + **include/dfptl/container/** : header source files defining processing cells helping in container manipulations.
    + *include/dfptl/container/detail/* : header files for implementation details of container module.
    + **include/dfptl/sndfile/** : header source files defining processing cells helping in sound file handling.
    + *include/dfptl/sndfile/detail/* : header files for implementation details of sndfile module.
    + **include/dfptl/io/** : header source files defining processing cells based on standard io stream manipulation.
    + *include/dfptl/io/detail/* : header files for implementation details of io module.
    + **include/dfptl/filtering/** : header source files defining processing cells applying signal filtering.
    + *include/dfptl/filtering/detail/* : header files for implementation details of filtering module.
    + **include/dfptl/windowing/** : header source files defining processing cells for signal windowing application.
    + *include/dfptl/io/detail/* : header files for implementation details of windowing module.
    + **include/dfptl/kissfft/** : header source files defining processing cells wrapping FFT computation by kissfft library.
    + *include/dfptl/kissfft/detail/* : header files for implementation details of kissfft module.
    + **include/dfptl/fvad/** : header source files defining processing cells bringing out VAD.
    + *include/dfptl/fvad/detail/* : header files for implementation details of fvad module.
    + **include/dfptl/speex/** : header source files defining processing cells bringing out speex codec processing.
    + *include/dfptl/speex/detail/* : header files for implementation details of speex module.
    + **include/dfptl/alsa/** : header source files defining processing cells helping in sound playback and capture through ALSA devices.
    + *include/dfptl/alsa/detail/* : header files for implementation details of alsa module.
    + **include/dfptl/java/** : header source files defining processing cells helping in interfacing DFP_TLIB processing tree with JAVA.
    + *include/dfptl/java/detail/* : header files for implementation details of java module.
  + *include/stl/* : backport of newest C++ STL features to C++11 release.
+ *example/* : all sources files for examples.
+ *test/* : all sources files for library testing.
+ *dep/* : source code of some dependent projects.
+ *codelite/* : codelilte project files.


## Namespace organization

As a general comment, anything inside an inner **detail** namespace is not intended to be directly used by end-user implementation. Elements of such namespaces are implementation details and may change or disappear without prior notice.

+ **[dfp](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/namespacedfp.html)** : root namespace of the DFP template library.
  + **[core](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__core.html)** : namespace of the DFPTL library core. It contains all classes and functions definitions required to manage a processing tree and implement custom processing cells.
    + *[dfp::core::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/details/html/group__core__detail.html)* : implementation details of library core.
  + **[alu](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__alu__grp.html)** : namespace of the DFPTL alu module. It contains all classes and functions definitions required to bring arithmetic and logic operations in processing tree.
    + *[dfp::alu::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__alu__detail.html)* : implementation details of alu module.
  + **[math](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__math__grp.html)** : namespace of the DFPTL math module. It contains all classes and functions definitions required to bring classic mathematical operations in processing tree.
    + *[dfp::math::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__math__detail.html)* : implementation details of math module.
  + **[routing](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__routing__grp.html)** : namespace of the DFPTL routing module. It contains all classes and functions definitions required to bring stream routing operations and routine manipulations in processing tree.
    + *[dfp::routing::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__routing__detail.html)* : implementation details of routing module.
  + **[container](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__container__grp.html)** : namespace of the DFPTL container module. It contains all classes and functions definitions required to bring container manipulations in processing tree.
    + *[dfp::container::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__container__detail.html)* : implementation details of container module.
  + **[io](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__io__grp.html)** : namespace of the DFPTL io module. It contains all classes and functions definitions required to bind dfp stream with [standard io stream](https://en.cppreference.com/w/cpp/io).
    + *[dfp::io::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__io__detail.html)* : implementation details of io module.
  + **[sndfile](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__sndfile__grp.html)** : namespace of the DFPTL sndfile module. It contains all classes and functions definitions required to bring sndfile handling (based on the [libsndfile](http://www.mega-nerd.com/libsndfile/) library) in processing tree.
    + *[dfp::sndfile::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__sndfile__detail.html)* : implementation details of sndfile module.
  + **[filtering](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__filtering__grp.html)** : namespace of the DFPTL filtering module. It contains all classes and functions definitions required to apply signal filtering in processing tree.
    + *[dfp::filtering::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__filtering__detail.html)* : implementation details of filtering module.
  + **[windowing](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__windowing__grp.html)** : namespace of the DFPTL windowing module. It contains all classes and functions definitions required to apply signal windowing in processing tree.
    + *[dfp::windowing::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__windowing__detail.html)* : implementation details of windowing module.
  + **[kissfft](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__kissfft__grp.html)** : namespace of the DFPTL kissfft module. It contains all classes and functions definitions required to perform Fast Fourier Transform in processing tree based on the [kissfft library](https://github.com/mborgerding/kissfft).
    + *[dfp::kissfft::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__kissfft__detail.html)* : implementation details of kissfft module.
  + **[fvad](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__fvad__grp.html)** : namespace of the DFPTL fvad module. It contains all classes and functions definitions required to bring VAD (based on the [libfvad](https://github.com/dpirch/libfvad) library) in processing tree.
    + *[dfp::fvad::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__fvad__detail.html)* : implementation details of fvad module.
  + **[speex](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__speex__grp.html)** : namespace of the DFPTL speex module. It contains all classes and functions definitions required to bring speex codec processing (based on the [speex](https://www.speex.org/) library) in processing tree. For now, only provides VAD capability.
    + *[dfp::speex::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__speex__detail.html)* : implementation details of speex module.
  + **[alsa](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__alsa__grp.html)** : namespace of the DFPTL alsa module. It contains all classes and functions definitions required to bring handling of ALSA PCM devices in processing tree.
    + *[dfp::alsa::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__alsa__detail.html)* : implementation details of alsa module.
  + **[java](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__java__grp.html)** : namespace of the DFPTL java module. It contains all classes and functions definitions required to easily interface processing tree with JAVA.
    + *[dfp::java::detail](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/group__java__detail.html)* : implementation details of java module.


## API

Complete [DFP TLIB API](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/) is described in a [doxygen-autogenerated document](https://tarmin.gitlab.io/dfp_tlib/doxygen/api/html/).


## External dependency table

directory  | namespace      | dependency**    | ubuntu package* | purpose
-----------|----------------|-----------------|-----------------|--------
core       | dfp::core      | boost headers   | libboost-dev    | core definition for processing tree or custom cell creation
alu        | dfp::alu       | none            | N/A             | definition of arithmetic and logic processing cells
math       | dfp::math      | none            | N/A             | definition of processing cells applying classic mathematical operations
routing    | dfp::routing   | none            | N/A             | definition of processing cells for stream routing and routine manipulations
container  | dfp::container | none            | N/A             | definition of processing cells for manipulations of sample containers
io         | dfp::io        | none            | N/A             | definition of processing cells for DFPTL stream binding with std IO stream.
filtering  | dfp::filtering | dfp::routing,<br> dfp::alu | N/A  | definition of processing cells for signal filtering
windowing  | dfp::windowing | dfp::routing,<br> dfp::alu | N/A  | definition of processing cells for signal windowing
kissfft    | dfp::kissfft   | none            | src on github   | definition of processing cells for FFT operations
sndfile    | dfp::sndfile   | libsndfile      | libsndfile1-dev | definition of processing cells for sound file handling
fvad       | dfp::fvad      | libfvad         | src on github   | definition of processing cells for Voice Activity Detection with libfvad
speex      | dfp::speex     | libspeexdsp     | src on github   | definition of processing cells for processing based on SPEEX codec
alsa       | dfp::alsa      | libzita-alsa-pcmi,<br> libasound2 | libzita-alsa-pcmi-dev,<br> libasound2-dev  | definition of processing cells for PCM audio streaming with ALSA driver
java       | dfp::java      | jni             | openjdk-8-jdk   | definition of processing cells for easy interfacing with JAVA

ubuntu package*: As a matter of example, in this colomn are given the names of Ubuntu 16.04 APT-packages which can provide the dependencies.<br>
dependency**: all modules but core implicitly depend also on dfp::core module.

[//]: # (
@if 0 @anchor)
# Going further
* #### [multi-channels stream](GOING_FURTHER.md#multi-channels-stream)
* #### [subprocessing cell](GOING_FURTHER.md#subprocessing-cell)
* #### [implicit conversions](GOING_FURTHER.md#implicit-conversions)

[//]: # (
@endif @n @anchor)
